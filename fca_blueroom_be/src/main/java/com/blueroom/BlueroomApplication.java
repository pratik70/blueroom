package com.blueroom;

import javax.persistence.EntityManagerFactory;

import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
@EnableScheduling
public class BlueroomApplication {


	@Autowired
	private EntityManagerFactory entityManagerFactory;

	
	public static void main(String[] args) {
		SpringApplication.run(BlueroomApplication.class, args);
		System.out.println("Here");
	}
	

	@Bean(name="sessionFactory")
    public SessionFactory sessionFactory() {
        return ((HibernateEntityManagerFactory) this.entityManagerFactory).getSessionFactory();
    }
}
