package com.blueroom.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.UserTechnicalToolSkillSet;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.entities.auth.Role;
import com.blueroom.repository.UserDao;
import com.blueroom.repository.UserRoleRepository;
import com.blueroom.repository.UserTechnicalToolSkillSetRepository;
import com.blueroom.vm.FileStorageException;
import com.blueroom.vm.FileStorageProperties;
import com.blueroom.vm.UserFormVm;
import com.blueroom.vm.UserSkillSetVM;
import com.blueroom.vm.UserSkillTechnicalToolVM;

@Service
public class UserGetservice {
    
    private Path fileStorageLocation;
    
    @Autowired
    FileStorageProperties fileStorageProperties;
    
    @Autowired
    UserDao userdao;
    
    @Autowired
    UserRoleRepository userRoleRepository;
    
    @Autowired
    UserTechnicalToolSkillSetRepository userTechnicalToolSkillSetRepository;
    
    public AuthUser setusers(UserFormVm userform) {
        AuthUser user = new AuthUser();
        
        user.setAge(userform.getuserAge());
        user.setGender(userform.getuserGender());
        user.setFirstName(userform.getuserFname());
        user.setLastName(userform.getuserLname());
        user.setEmployeeId(userform.getEmployeeId());
        /*		userDao.save(user);
		return masterRepository.save(user);
         */
        
        return userdao.save(user);
    }
    
    public AuthUser saveUser(AuthUser userform) {
        AuthUser user1 = new AuthUser();
        if (Objects.nonNull(userform.id)) {
            user1 = userdao.findOne(userform.id);
        }
        
        user1.setAge(userform.getAge());
        user1.setGender(userform.getGender());
        user1.setFirstName(userform.getFirstName());
        user1.setLastName(userform.getLastName());
        user1.setEmployeeId(userform.getEmployeeId());
        user1.setUsername(userform.getEmployeeId().toString().concat("_" + userform.getType()));//employee id as a username
        user1.setPassword(userform.getPassword());
        user1.setEmail(userform.getEmail());
        user1.setGender(userform.getGender());
        user1.setEmployeeId(userform.getEmployeeId());
        user1.setNationality(userform.getNationality());
        user1.setCity(userform.getCity());
        user1.setName(userform.getName());
        user1.setProfileImg(userform.getProfileImg());
        user1.setEtuLevel(userform.getEtuLevel());
        user1.setCreatedDate(userform.getCreatedDate());
        user1.setRoles(userform.getRoles());
        user1.setPermissions(userform.getPermissions());
        user1.setStatus(userform.getStatus());
        user1.setEnabled(userform.isEnabled());
        user1.setEtuList(userform.getEtuList());
        user1.setPillarList(userform.getPillarList());
        user1.setApproval(userform.getApproval());
        user1.setFirst_joining_date(userform.getFirst_joining_date());
        user1.setFiapl_joining_date(userform.getFiapl_joining_date());
        user1.setEducational_background(userform.getEducational_background());
        user1.setInternal_trainer(userform.getInternal_trainer());
        user1.setPreffered_learning_style(userform.getPreffered_learning_style());
        user1.setType(userform.getType());
        user1.setEnabled(true);
        
        return userdao.save(user1);
        
    }
    
    public List<AuthUser> getall() {
        return userdao.findAllBlueroomUser();
    }
    
    public void deleteusers(UserFormVm userform) {
        AuthUser user1 = userdao.getOne(userform.getuserId());
        if (Objects.nonNull(user1)) {
            userdao.delete(user1);
        }
    }
    
    public AuthUser findById(long uid) { 
        return userdao.findOne(uid);
    }
    
    public AuthUser findByEmployeeId(long uid) { 
         return userdao.findOneByemployeeId(uid);
    }
    
    
    
    public String storeFile(MultipartFile file, String userFolderPath) {
        
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        
        this.fileStorageLocation = Paths.get(userFolderPath)
                .toAbsolutePath().normalize();
        
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
        
        try {
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! file contains invalid path.......");
            }
            
            Path targetLocation = fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("could not store file" + fileName + ". please try again", ex);
        }
        
    }
    
    public List<Role> getAllRoles() {
        return userRoleRepository.findAllBlueroomRole();
    }
    
    public List<String> getLearningStyle(){
    	  return userRoleRepository.findAllLearingStyle();
    	
    }
    
    public List<AuthUser> findAllByEmployeeId(Long employeeId) {
        return userdao.findAllByEmployeeIdAndStatus(employeeId,"Active");
        
    }
    
    public List<AuthUser> findAllByEmail(String email) {
        return userdao.findAllByEmailAndStatus(email,"Active");
    }

	public List<AuthUser> getAllPDUsers() {
		
		return userdao.findAllPDUser();
	}

	public Object addUserSkillTechnicalTools(UserSkillTechnicalToolVM userSkillTechnicalToolVM) {
        UserTechnicalToolSkillSet technicalToolSkillSet = new UserTechnicalToolSkillSet();
        technicalToolSkillSet.setActualSkill(userSkillTechnicalToolVM.getActualSkill());
        technicalToolSkillSet.setTechnicalTool(userSkillTechnicalToolVM.getTechnicalTool());
        technicalToolSkillSet.setUser(userSkillTechnicalToolVM.getUser());
        userTechnicalToolSkillSetRepository.save(technicalToolSkillSet);
        return technicalToolSkillSet;
	}

	public List<UserTechnicalToolSkillSet> getUserSkillTechnicalTools(Long userId) {
		return userTechnicalToolSkillSetRepository.findAllByUserId(userId);
	}
	public List<Role> getAllPDRoles() {
		return userRoleRepository.findAllPDRole();
	}

	public List<AuthUser> getPlantHeadTeam() {
		return userdao.findPlantHeadTeam();
	}
    
}
