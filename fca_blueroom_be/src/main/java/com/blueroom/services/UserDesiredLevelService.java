package com.blueroom.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.UserCompetencyProgress;
import com.blueroom.entities.UserDesiredLevel;
import com.blueroom.repository.UserDesiredLevelRepository;

@Service
public class UserDesiredLevelService {

	@Autowired
	UserDesiredLevelRepository userdesiredlevelRepository;
	
	@Autowired
	UserCompetencyProgressService userCompetencyProgressService;

	public UserDesiredLevel getUserDesiredLevelById(Long id) {
		return userdesiredlevelRepository.findOne(id);
	}

	public UserDesiredLevel addUserDesiredLevel(UserDesiredLevel userdesiredlevel) {
		return userdesiredlevelRepository.save(userdesiredlevel);
	}

	public List<UserDesiredLevel> getAllUserDesiredLevel() {
		return userdesiredlevelRepository.findAll();
	}

	public List<Map<String, Object>> getAllUserDesiredLevelV2() {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for (UserDesiredLevel userDesiredLevel : userdesiredlevelRepository.findAll()) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("competency", userDesiredLevel.getCompetencies().getName());
			map.put("user", userDesiredLevel.getUser().getFirstName());
			map.put("level", userDesiredLevel.getActualSkill());
			map.put("year", userDesiredLevel.getYear());
			result.add(map);
		}
		return result;
	}

	public UserDesiredLevel updateUserDesiredLevel(UserDesiredLevel userDesiredlevel) {
		UserDesiredLevel existing = userdesiredlevelRepository.findOneByUserIdCompetencyIDYear(
				userDesiredlevel.getUser().getId(), userDesiredlevel.getCompetencies().getId(),
				userDesiredlevel.getYear());
		if (Objects.isNull(existing)) {
			userDesiredlevel.setStatus(true);
			return userdesiredlevelRepository.save(userDesiredlevel);
		} else {
			existing.setRequiredSkill(userDesiredlevel.getRequiredSkill());
			existing.setActualSkill(userDesiredlevel.getActualSkill());
			existing.setPlanStatus(userDesiredlevel.getPlanStatus());
			existing.setStatus(true);
			return userdesiredlevelRepository.save(existing);
		}
	}

	public Object updateUserCurrentLevel(UserDesiredLevel userDesiredlevel) {
		UserDesiredLevel existing = userdesiredlevelRepository.findOneByUserIdCompetencyIDYear(
				userDesiredlevel.getUser().getId(), userDesiredlevel.getCompetencies().getId(),
				userDesiredlevel.getYear());

		UserCompetencyProgress userCompetencyProgress = new UserCompetencyProgress();
		userCompetencyProgress.setModifiedAt(new Date());
		userCompetencyProgress.setCompetencyId(userDesiredlevel.getCompetencies());
		userCompetencyProgress.setUpdatedLevel(userDesiredlevel.getActualSkill());
		userCompetencyProgress.setUser(userDesiredlevel.getUser());
		;

		if (Objects.isNull(existing)) {
			userCompetencyProgress.setPreviousLevel(0L);
			userCompetencyProgressService.addUserCompetencyProgress(userCompetencyProgress);
			userDesiredlevel.setStatus(true);
			return userdesiredlevelRepository.save(userDesiredlevel);
		} else {
			if(userDesiredlevel.getActualSkill() != existing.getActualSkill()) {
				userCompetencyProgress.setPreviousLevel(existing.getActualSkill());
				userCompetencyProgressService.addUserCompetencyProgress(userCompetencyProgress);
			}
			existing.setRequiredSkill(userDesiredlevel.getRequiredSkill());
			existing.setActualSkill(userDesiredlevel.getActualSkill());
			existing.setPlanStatus(userDesiredlevel.getPlanStatus());
			existing.setStatus(true);
			return userdesiredlevelRepository.save(existing);
		}
	}

}
