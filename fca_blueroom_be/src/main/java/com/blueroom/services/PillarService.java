package com.blueroom.services;

import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.CompetencyPillar;
import com.blueroom.entities.Pillar;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.repository.CompetenciesRepository;
import com.blueroom.repository.CompetencyPillarRepository;
import com.blueroom.repository.PillarRepository;
import com.blueroom.repository.UserDao;
import com.blueroom.vm.IDVM;
import com.blueroom.vm.PillarVM;

@Service
public class PillarService {

	@Autowired
	PillarRepository pillarRepository;
	
	@Autowired
	AuthUserRepository authUserRepository;

	@Autowired
	CompetenciesRepository competenciesRepository;
	
	@Autowired
	CompetencyPillarRepository competencyPillarRepository;

	@Autowired
	UserDao userDao;
	
	public Pillar getPillarById(Long id) {
		return pillarRepository.findOne(id);
	}

	public List<Pillar> getAllPillars() {
		return pillarRepository.getAllByStatus(true);
	}

	public Pillar addPillar(PillarVM pillarVM) {
		Pillar existing = pillarRepository.findOneByCode(pillarVM.getCode());
		if(Objects.isNull(existing)) {
			Pillar pillar = new Pillar(); 	
			pillar.setPillerName(pillarVM.getName());
			pillar.setCode(pillarVM.getCode());
			pillar.setManager(pillarVM.getManagerID());
			pillar.setStatus(pillarVM.getStatus());
			pillar = pillarRepository.save(pillar);
			for (IDVM userid : pillarVM.getMemberID()) {
				AuthUser user = authUserRepository.findOne(userid.getId());
				user.getPillarList().add(pillar);
				authUserRepository.save(user);
			}
			for (IDVM competencyid : pillarVM.getCompetencyID()) {
				CompetencyPillar existingCP =competencyPillarRepository.findOneByCompetencyIdAndPillarId(pillar.getId(), competencyid.getId());
				if(Objects.isNull(existingCP)) {
					Competencies competency = competenciesRepository.findOne(competencyid.getId());
				    CompetencyPillar pc = new CompetencyPillar();
				    pc.setCompetency(competency);
				    pc.setPillar(pillar);
				    competencyPillarRepository.save(pc);
				}
			}
			return pillar;
			}
		    else {
		    	throw new IllegalArgumentException("code already exist");
		    	}
		}

	public void deletePillar(Long id) {
		Pillar existingPillar = getPillarById(id);
		existingPillar.setStatus(false);
		pillarRepository.deletePillarTeam(id);
		pillarRepository.save(existingPillar);
	}

	
	public List<AuthUser> pillarTeam(Long id) { 
//		Pillar pill = getPillarById(id);
//		return pill.getTeams(); 
		return authUserRepository.getTeamByPillar(id); 
	}
	 
	public AuthUser pillarManager(Long id) {
		Pillar pill = getPillarById(id);
		return pill.getManager();
	}

	public List<Pillar> getPillarByName(String name) {
		return pillarRepository.getAllByName(name);
	}

	public Pillar updatePillar(PillarVM pillarVM) {
		 long[] memberId=authUserRepository.findByteamMember(pillarVM.getId());
		 authUserRepository.deletebyTeam(pillarVM.getId());
		
		 competencyPillarRepository.deletebycompetency(pillarVM.getId());
		 
		Pillar existingPillar = getPillarById(pillarVM.getId());
		existingPillar.setPillerName(pillarVM.getName());
		existingPillar.setCode(pillarVM.getCode());
		existingPillar.setManager(pillarVM.getManagerID());
		existingPillar.setStatus(pillarVM.getStatus());
		existingPillar=pillarRepository.save(existingPillar);
		
		
		for (IDVM userid : pillarVM.getMemberID()) {
			AuthUser user = authUserRepository.findOne(userid.getId());
			user.getPillarList().add(existingPillar);
			authUserRepository.save(user);
		}


		for (IDVM competencyid : pillarVM.getCompetencyID()) {
		
				Competencies competency = competenciesRepository.findOne(competencyid.getId());
				CompetencyPillar competencyPillar=competencyPillarRepository.findOneByCompetencyIdAndPillarId(competency.getId(),pillarVM.getId());
				if(Objects.nonNull(competencyPillar))
				{
					competencyPillar.setStatus(true);
					 competencyPillarRepository.save(competencyPillar);	
				}
				else
				{
				 CompetencyPillar pc = new CompetencyPillar();
					pc.setCompetency(competency);
					pc.setPillar(existingPillar);
					pc.setStatus(true);
					pc.setCurrent_level(0L);
					pc.setLast_year_level(0L);
					pc.setRequired_level(0L);
					 competencyPillarRepository.save(pc);	
				}
		}

		return existingPillar ;
	}

	public AuthUser setPillarUser(Long id, Pillar pillarId) {
		AuthUser existingAuth = authUserRepository.findOne(id);
		Pillar existingPillar = getPillarById(pillarId.getId());
		existingAuth.getPillarList().add(existingPillar);
//	    existingPillar.getTeams().add(member);
//		return pillarRepository.save(existingPillar);
		return authUserRepository.save(existingAuth);
	}

	public AuthUser setPillarUsers(Long id, List<Pillar> pillars) {
		AuthUser exitstingAuthUser = authUserRepository.findOne(id);
		// Pillar existingPillar = getPillarById(id);
		for(Pillar pillar : pillars) {
			exitstingAuthUser.getPillarList().add(getPillarById(pillar.getId()));
		}
		AuthUser user = authUserRepository.save(exitstingAuthUser);
		return user;
	}

	@Transactional
	public void pillarTeamDeleteUser(Long pillarId, Long userId) {
//	     pillarRepository.deletePillarTeamUser(pillarId,userId);
		authUserRepository.deleteUserPiller(pillarId,userId);
	}

	public Pillar getPillarByUserId(Long userId) {
		Pillar pillar = pillarRepository.getPillarByUserId(userId);
		
		if(Objects.isNull(pillar)) {
			pillar = pillarRepository.getPillarByPillarLeader(userId);
		}
		
		return pillar;
	}
	
	public List<Pillar>  getPillarByUser(Long id) { 
		return authUserRepository.getPillarByuser(id); 
	}

	public List<AuthUser> getAllPillarLeaders() {
		return userDao.findPlantHeadTeam();
	}

	public List<AuthUser> getAllTeamMembers() {
		return userDao.findAllTeamMembers();
	}
	
}