package com.blueroom.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.DevelopmentMethod;
import com.blueroom.entities.DevelopmentTool;
import com.blueroom.entities.UserDesiredLevel;
import com.blueroom.repository.DevelopmentMethodsRepository;
import com.blueroom.repository.DevelopmentToolRepository;
import com.blueroom.vm.DevelopmentMethodVM;
import com.blueroom.vm.IDVM;

@Service
public class DevelopmentMethodsService {

	@Autowired
	DevelopmentMethodsRepository developmentMethodsRepository;
	
	@Autowired
	DevelopmentToolRepository  developmentToolRepository ;
	
	public Map<String, Object> getDevelopmentByIdAndStatus(Long id) {
		DevelopmentMethod method = developmentMethodsRepository.findOneByIdAndStatus(id);
		Map<String, Object> map = new HashMap<String, Object>();
	    map.put("method", method);
	    map.put("tools", developmentToolRepository.findByDevelopmentMethodId(method.getId()));
	    return map;
	}

	public List<Map<String, Object>> getAllDevelopmentMethods() {
		List<DevelopmentMethod> methods = developmentMethodsRepository.findAllByStatus(true);
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for(DevelopmentMethod method:methods) {
			Map<String, Object> map = new HashMap<String, Object>();
			 map.put("method", method);
	        map.put("tools", developmentToolRepository.findByDevelopmentMethodId(method.getId()));
	        result.add(map);
		}
		
		return result;
	}

	public DevelopmentMethodVM addDevelopment(DevelopmentMethodVM developmentMethodVM) {
		DevelopmentMethod existing = developmentMethodsRepository.findByName(developmentMethodVM.getName());
		if(Objects.isNull(existing)) {
			DevelopmentMethod developmentMethod = new DevelopmentMethod();
			developmentMethod.setName(developmentMethodVM.getName());
			developmentMethod = developmentMethodsRepository.save(developmentMethod);
			for (IDVM toolFromJson: developmentMethodVM.getTools()) {
				DevelopmentTool tool = developmentToolRepository.findOne(toolFromJson.getId());
				tool.setDevelopmentMethod(developmentMethod);
				developmentToolRepository.save(tool);
			}
			developmentMethodVM.setId(developmentMethod.getId());
			return developmentMethodVM;
		} else {
	    	throw new IllegalArgumentException("code already exist");
	    }
	}
	

	public void deleteDevelopment(Long id) {
		DevelopmentMethod developmentMethod = developmentMethodsRepository.findOne(id);
		developmentMethod.setStatus(false);
		developmentMethod = developmentMethodsRepository.save(developmentMethod);
		List<DevelopmentTool> tools= developmentToolRepository.findByDevelopmentMethodId(id);
		for (DevelopmentTool tool : tools) {
			tool.setDevelopmentMethod(null);
			developmentToolRepository.save(tool);
		}
	}

	public DevelopmentMethodVM updateDevelopment(DevelopmentMethodVM developmentMethodVM) {
		DevelopmentMethod developmentMethod = developmentMethodsRepository.findOne(developmentMethodVM.getId());
		developmentMethod.setName(developmentMethodVM.getName());
		developmentMethod = developmentMethodsRepository.save(developmentMethod);
		List<DevelopmentTool> tools = developmentToolRepository.findByDevelopmentMethodId(developmentMethod.getId());
		for(DevelopmentTool tool :tools) {
			tool.setDevelopmentMethod(null);
		}
		for (IDVM toolFromJson: developmentMethodVM.getTools()){
			DevelopmentTool tool = developmentToolRepository.findOne(toolFromJson.getId());
			tool.setStatus(true);
			tool.setDevelopmentMethod(developmentMethod);
			developmentToolRepository.save(tool);
		}
		return developmentMethodVM;
	}
}
