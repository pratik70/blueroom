package com.blueroom.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.DevelopmentMethod;
import com.blueroom.entities.DevelopmentTool;
import com.blueroom.entities.UserDesiredLevel;
import com.blueroom.repository.CompetenciesRepository;
import com.blueroom.repository.CompetencyPillarRepository;
import com.blueroom.repository.DevelopmentMethodsRepository;
import com.blueroom.repository.DevelopmentToolRepository;
import com.blueroom.repository.UserDesiredLevelRepository;
import com.blueroom.vm.MessageVM;
@Service
public class DevelopmentToolService {

	@Autowired
	DevelopmentToolRepository developmentToolRepository;
	
	@Autowired
	SkillService skillService;
	
	@Autowired 
	UserDesiredLevelRepository userDesiredLevelRepository;
	
	@Autowired
	CompetenciesRepository competenciesRepository;
	
	@Autowired
	DevelopmentMethodsRepository developmentMethodsRepository;
	
	@Autowired
	CompetencyPillarRepository competencyPillarRepository;
		
	public DevelopmentTool getDevelopmentToolById(Long id) {
		return developmentToolRepository.findOne(id);
	}
                                                                                    

	public List<DevelopmentTool> getAllDevelopmentTool() {
		return developmentToolRepository.findByStatus(true);
	}


	public DevelopmentTool addDevelopmentTool(DevelopmentTool developmentTool) {
		DevelopmentTool existing = developmentToolRepository.findByName(developmentTool.getName());
		if(Objects.isNull(existing)) {	
			return developmentToolRepository.save(developmentTool);
		}else {
	    	throw new IllegalArgumentException("code already exist");
	    }
	}


	public void deleteDevelopmentTool(Long id) {
		DevelopmentTool existingTool= getDevelopmentToolById(id);
		existingTool.setStatus(false);
		developmentToolRepository.save(existingTool);
		
	}


	public DevelopmentTool updateDevelopmentTool(DevelopmentTool developmentTool) {
		DevelopmentTool existing = developmentToolRepository.findOne(developmentTool.getId());
		existing.setName(developmentTool.getName());
		return developmentToolRepository.save(developmentTool);
	}


	public List<Map<String, String>> getDevelopmentPlan(HashMap<String, Long> data) {
		Map<String,Boolean> calc = new HashMap<String,Boolean>();
	    List<DevelopmentTool> tools=  developmentToolRepository.findByDevelopmentMethodId(data.get("methodId"));
	    for(DevelopmentTool tool:tools) {
	    	calc.put(tool.getName(),false);
	    }
		List<DevelopmentTool> plantools= developmentToolRepository.getPlan(data.get("userId"), data.get("competencyId"), data.get("year"), data.get("methodId"));
		for(DevelopmentTool plantool:plantools ) {
			calc.put(plantool.getName(),true);
		} 
		List<Map<String, String>> result = new ArrayList<Map<String,String>>();
		for(String key : calc.keySet()) {
			Map<String, String> row = new HashMap<String, String>();
			row.put("name", key);
			if(calc.get(key)) {
				row.put("selected", "True");
			} else {
				row.put("selected", "False");
			}
			result.add(row);
		}
		return result;
	}
	
}
