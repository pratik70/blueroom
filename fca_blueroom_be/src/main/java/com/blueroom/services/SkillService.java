package com.blueroom.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.SkillInventryEntity;
import com.blueroom.entities.TechnicalTool;
import com.blueroom.entities.UpgradeMethodToolsSkillHistory;
import com.blueroom.entities.UpgradeTechnicalToolsSkillHistory;
import com.blueroom.entities.UserDesiredLevel;
import com.blueroom.entities.UserTechnicalToolSkillSet;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.repository.ETURepository;
import com.blueroom.repository.MethodsAndToolsRepository;
import com.blueroom.repository.TechnicalToolRepository;
import com.blueroom.repository.UpgradeMethodToolsSkillHistoryRepository;
import com.blueroom.repository.UpgradeTechnicalToolsSkillHistoryRepository;
import com.blueroom.repository.UserDesiredLevelRepository;
import com.blueroom.repository.UserSkillSetRepository;
import com.blueroom.repository.UserTechnicalToolSkillSetRepository;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;

@Service
public class SkillService {

    @Autowired
    AuthUserRepository userRepository;
    
    @Autowired
    ETURepository eTURepository;

    @Autowired
    MethodsAndToolsRepository methodsAndToolsRepository;

    @Autowired
    UserSkillSetRepository userSkillSetRepository;

    @Autowired
    TechnicalToolRepository technicalToolRepository;

    @Autowired
    UserTechnicalToolSkillSetRepository userTechnicalToolSkillSetRepository;

    @Autowired
    UpgradeMethodToolsSkillHistoryRepository upgradeMethodToolsSkillHistoryRepository;

    @Autowired
    UpgradeTechnicalToolsSkillHistoryRepository upgradeTechnicalToolsSkillHistoryRepository;
    
    @Autowired
    UserDesiredLevelRepository userDesiredLevelRepository;

    public MessageVM getEtuUsersSkill(MatrixFilterVM filterVM) {
        MessageVM messageVM = new MessageVM();
        HashMap<String, Object> skillInventoryData = new HashMap<>();
        List<AuthUser> userList = new ArrayList<>();
        List<BigInteger> userids = new ArrayList();
        List<Long> useridstoretrive=new ArrayList();
        if (filterVM.getEtuList().isEmpty() || filterVM.getEtuList().size() < 1) {
            userList = userRepository.findAllByStatus("Active");
        } else {
            List<Long> etuIdss = filterVM.getEtuList().stream().map(mapper -> mapper.getId()).collect(Collectors.toList());
            
            for(Long etuIds:etuIdss){
            	userids = eTURepository.findUserIdsByETUId(etuIds);
            }
            for(BigInteger b:userids){
            	Long userid=b.longValue();
            	useridstoretrive.add(userid);
            }
            userList=userRepository.findAllByIdIn(useridstoretrive);
        }
		List<MethodsAndTools> methodsAndToolsList = methodsAndToolsRepository.findAll();
		HashMap<String, String> methodsAndToolsMap = new HashMap<>();
		for (MethodsAndTools methodsAndTools :methodsAndToolsList) {
			if (methodsAndTools.getReprortPriority() != null && methodsAndTools.getReprortPriority() > 0 ) 
				if (methodsAndTools.getLevelSecondTool() != null ) {
					methodsAndToolsMap.put( "method_tool_" + methodsAndTools.getId() , methodsAndTools.getLevelFirstTool() + " "+ methodsAndTools.getLevelSecondTool());

				} else {
					methodsAndToolsMap.put( "method_tool_" + methodsAndTools.getId() , methodsAndTools.getLevelFirstTool());
				}
		}

        skillInventoryData.put("methodsAndToolHeaderList", methodsAndToolsMap);
        HashMap<String, String> technicalToolMap = new HashMap<>();
        List<TechnicalTool> technicalTools = technicalToolRepository.findAll();
        for (TechnicalTool technicalTool : technicalTools) {
            technicalToolMap.put("technical_tool_" + technicalTool.getId(), technicalTool.getTechnicalToolDescription());
        }
        skillInventoryData.put("technicalToolList", technicalToolMap);

        List<HashMap<String, Object>> userSkill = new ArrayList<>();
        for (AuthUser user : userList) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("userId", user.getId());
            hashMap.put("userName", user.getName());
            List<SkillInventryEntity> skillSetsList = userSkillSetRepository.findAllByUser(user.getId());
            for (SkillInventryEntity skillSet : skillSetsList) {
                hashMap.put("method_tool_" + skillSet.getMethodsTools().getId(), skillSet.getActual_skill_id());
            }
            List<UserTechnicalToolSkillSet> userTechnicalToolSkillSetList = userTechnicalToolSkillSetRepository.findAllByUser(user);
            for (UserTechnicalToolSkillSet technicalToolSkillSet : userTechnicalToolSkillSetList) {
                hashMap.put("technical_tool_" + technicalToolSkillSet.getTechnicalTool().getId(), technicalToolSkillSet.getActualSkill());
            }
            userSkill.add(hashMap);
        }
        skillInventoryData.put("userList", userSkill);
//                Set<String> keys = userSkill.stream().map(l -> l.keySet()).collect(Collectors.toList()); 
        messageVM.setData(skillInventoryData);
        return messageVM;
    }

    public MessageVM getUserSkillReport(Integer year) {
        MessageVM messageVM = new MessageVM();
        List<String> updatedMethodsIds = upgradeMethodToolsSkillHistoryRepository.getUpdatedMethodsIdsUsingYear(year);
        HashMap<String, Object> skillInventoryReportData = new HashMap<>();
        List<MethodsAndTools> methodsAndToolsList = new ArrayList<>();
        if (!updatedMethodsIds.isEmpty()) {
            methodsAndToolsList = methodsAndToolsRepository.findAll(updatedMethodsIds);
        }
        HashMap<String, String> methodsAndToolsMap = new HashMap<>();
        for (MethodsAndTools methodsAndTools : methodsAndToolsList) {
            if (methodsAndTools.getLevelSecondTool() != null) {
                methodsAndToolsMap.put("method_tool_" + methodsAndTools.getId(), methodsAndTools.getLevelFirstTool() + " " + methodsAndTools.getLevelSecondTool());
            } else {
                methodsAndToolsMap.put("method_tool_" + methodsAndTools.getId(), methodsAndTools.getLevelFirstTool());
            }
        }
        skillInventoryReportData.put("methodsAndToolHeaderList", methodsAndToolsMap);
        List<String> updatedTechnicalToolIds = upgradeTechnicalToolsSkillHistoryRepository.getUpdatedToolIdsUsingYear(year);
        HashMap<String, String> technicalToolMap = new HashMap<>();
        List<TechnicalTool> technicalTools = new ArrayList<>();
        if (!updatedTechnicalToolIds.isEmpty()) {
            technicalTools = technicalToolRepository.findAll(updatedTechnicalToolIds);
        }
        for (TechnicalTool technicalTool : technicalTools) {
            technicalToolMap.put("technical_tool_" + technicalTool.getId(), technicalTool.getTechnicalToolDescription());
        }
        skillInventoryReportData.put("technicalToolList", technicalToolMap);
        List<String> upgradedUsersList = upgradeMethodToolsSkillHistoryRepository.findAllUpdatedUsersByYear(year);
        upgradedUsersList.addAll(upgradeTechnicalToolsSkillHistoryRepository.findAllUpdatedUsersByYear(year));
        List<AuthUser> userList = new ArrayList<>();
        if (!upgradedUsersList.isEmpty()) {
            userList = userRepository.findAll(upgradedUsersList);
        }
        List<HashMap<String, Object>> userSkill = new ArrayList<>();
        for (AuthUser user : userList) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("userId", user.getId());
            hashMap.put("userName", user.getName());

            List<UpgradeMethodToolsSkillHistory> upgradeMethodToolsSkillHistories = upgradeMethodToolsSkillHistoryRepository.findAllByUserAndYear(user.getId(), year);
            for (UpgradeMethodToolsSkillHistory toolsSkillHistory : upgradeMethodToolsSkillHistories) {
                hashMap.put("method_tool_" + toolsSkillHistory.getMethodsTools().getId(), toolsSkillHistory.getNewSkill());
            }

            List<UpgradeTechnicalToolsSkillHistory> technicalToolsSkillHistories = upgradeTechnicalToolsSkillHistoryRepository.findAllByUserAndYear(user.getId(), year);
            for (UpgradeTechnicalToolsSkillHistory upgradeTechnicalToolsSkillHistory : technicalToolsSkillHistories) {
                hashMap.put("technical_tool_" + upgradeTechnicalToolsSkillHistory.getTechnicalTool().getId(), upgradeTechnicalToolsSkillHistory.getNewSkill());
            }
            userSkill.add(hashMap);
        }
        skillInventoryReportData.put("userList", userSkill);

        messageVM.setData(skillInventoryReportData);
        return messageVM;
    }

	public MessageVM getUserDesiredSkillByUserId(Integer userId, Integer year) {
		MessageVM vm = new MessageVM();
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for(UserDesiredLevel userDesiredLevel: userDesiredLevelRepository.getUserDesiredSkillByUserIdAndYear(userId, year)) {
			Map<String, Object> map = new HashMap<String, Object>();
	        map.put("competency_name", userDesiredLevel.getCompetencies().getName() );
	        map.put("competency_area", userDesiredLevel.getCompetencies().getArea() );
	        map.put("competency_type", userDesiredLevel.getCompetencies().getType() );
	        map.put("actual_skill", userDesiredLevel.getActualSkill());
	        map.put("required_skill", userDesiredLevel.getRequiredSkill());
	        map.put("last_year_skill", userDesiredLevelRepository.getUserDesiredSkillByUserIdAndYearAndComptencyId(userId, year-1, userDesiredLevel.getCompetencies().getId()));
	        result.add(map);
		}
		vm.setCode("200");
		vm.setData(result);
		return vm;
	}

	public MessageVM getUserDesiredSkillByUserIdAndYear(Integer userId, Integer year, String area) {
		MessageVM vm = new MessageVM();
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for(UserDesiredLevel userDesiredLevel: userDesiredLevelRepository.getUserDesiredSkillByUserIdAndYear(userId, year, area)) {
			Map<String, Object> map = new HashMap<String, Object>();
	        map.put("competency_name", userDesiredLevel.getCompetencies().getName() );
	        map.put("competency_id", userDesiredLevel.getCompetencies().getId());
	        map.put("user_name", userDesiredLevel.getUser().getName());
	        map.put("year", userDesiredLevel.getYear());
	        map.put("actual_skill", userDesiredLevel.getActualSkill());
	        map.put("required_skill", userDesiredLevel.getRequiredSkill());
	        result.add(map);
		}
		vm.setCode("200");
		vm.setData(result);
		return vm;
	}
	
	public MessageVM getTeamMaxSkillByPillarIdAndCompetencyArea(Long pillarId, String competency_area) {
		MessageVM vm = new MessageVM();
		List<Object[]> objectList=userDesiredLevelRepository.getTeamMaxSkillByPillarIdAndCompetencyArea(pillarId, competency_area);
		vm.setCode("200");
		vm.setData(objectList);
		return vm;
	}

	public MessageVM getTeamAverageSkillByPillarIdAndCompetencyArea(Long pillarId, String competency_area) {
		List<Object[]> resultList = new ArrayList<Object[]>();
		MessageVM vm = new MessageVM();
		List<Object[]> objectList = userDesiredLevelRepository.getTeamAverageSkillByPillarIdAndCompetencyArea(pillarId,
				competency_area);
		for (Object[] objArray : objectList) {
			objArray[2] = Integer.parseInt(objArray[2].toString()) / Integer.parseInt(objArray[4].toString());
			objArray[3] = Integer.parseInt(objArray[3].toString()) / Integer.parseInt(objArray[4].toString());
			resultList.add(objArray);
		}
		vm.setCode("200");
		vm.setData(resultList);
		return vm;
	}

	public MessageVM getPlantHeadTeamMaxSkill(String competency_area) {
		MessageVM vm = new MessageVM();
		List<Object[]> objectList=userDesiredLevelRepository.getPlantHeadTeamMaxSkill(competency_area);
		vm.setCode("200");
		vm.setData(objectList);
		return vm;
	}
	
	public MessageVM getPlantHeadTeamAverageSkill(String competency_area) {
		List<Object[]> resultList = new ArrayList<Object[]>();
		MessageVM vm = new MessageVM();
		List<Object[]> objectList = userDesiredLevelRepository.getPlantHeadTeamAverage(competency_area);
		for (Object[] objArray : objectList) {
			objArray[2] = Integer.parseInt(objArray[2].toString()) / Integer.parseInt(objArray[4].toString());
			objArray[3] = Integer.parseInt(objArray[3].toString()) / Integer.parseInt(objArray[4].toString());
			resultList.add(objArray);
		}
		vm.setCode("200");
		vm.setData(resultList);
		return vm;
	}

}
