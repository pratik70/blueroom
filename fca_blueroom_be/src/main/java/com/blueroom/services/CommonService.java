package com.blueroom.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.ETU;
import com.blueroom.entities.Loss;
import com.blueroom.entities.MachineLoss;
import com.blueroom.entities.MachineOperation;
import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.TechnicalTool;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.BenifitRepository;
import com.blueroom.repository.CDYearRepository;
import com.blueroom.repository.ETURepository;
import com.blueroom.repository.KPIRepository;
import com.blueroom.repository.LossRepository;
import com.blueroom.repository.LossTypeRepository;
import com.blueroom.repository.MachineLossRepository;
import com.blueroom.repository.MachineOperationRepository;
import com.blueroom.repository.MachineRepository;
import com.blueroom.repository.MethodsAndToolsRepository;
import com.blueroom.repository.PillarRepository;
import com.blueroom.repository.ProcessRepository;
import com.blueroom.repository.ProjectRepository;
import com.blueroom.repository.TechnicalToolRepository;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.vm.MachineOperationVM;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;
import com.blueroom.vm.TechnicalToolVM;
import com.blueroom.vm.UserVM;
import com.blueroom.vm.UserWorkingProjectCountVM;

@Service
public class CommonService {

    @Autowired
    LossRepository lossRepository;

    @Autowired
    LossTypeRepository lossTypeRepository;

    @Autowired
    MachineRepository machineRepository;

    @Autowired
    MachineOperationRepository machineOperationRepository;

    @Autowired
    MethodsAndToolsRepository methodsAndToolsRepository;

    @Autowired
    ETURepository etuRepository;

    @Autowired
    ProcessRepository processRepository;

    @Autowired
    PillarRepository pillarRepository;

    @Autowired
    AuthUserRepository userRepository;

    @Autowired
    BenifitRepository benifitRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    MachineLossRepository machineLossRepository;

    @Autowired
    TechnicalToolRepository technicalToolRepository;

    @Autowired
    CDYearRepository cdYearRepository;

    @Autowired
    KPIRepository kpiRepository;

    public MessageVM getAllLossGroup() {
        MessageVM messageVM = new MessageVM();
        messageVM.setData(lossRepository.findAllByOrderByLossType());
        return messageVM;
    }

    public MessageVM getAllMachine(long etu_id) {
        MessageVM messageVM = new MessageVM();
        if (Objects.isNull(etu_id)) {
            messageVM.setData(machineOperationRepository.findAll());
        } else {
            messageVM.setData(machineOperationRepository.findOperationByETU(etu_id));
        }
        return messageVM;
    }

    public MessageVM getAllMachine() {
        MessageVM messageVM = new MessageVM();
        messageVM.setData(machineOperationRepository.findAll());
        return messageVM;
    }

    public MessageVM getAllETUs() {
        MessageVM messageVM = new MessageVM();
        messageVM.setData(etuRepository.findAll());
        return messageVM;
    }

    public MessageVM updateETU(ETU etu) {
        MessageVM messageVM = new MessageVM();
        etuRepository.save(etu);
        messageVM.setData(etuRepository.findAll());
        return messageVM;
    }

    public MessageVM getAllProcess() {
        MessageVM messageVM = new MessageVM();
        messageVM.setData(processRepository.findAll());
        return messageVM;
    }

    public MessageVM getAllPillars() {
        MessageVM messageVM = new MessageVM();
        messageVM.setData(pillarRepository.findAll());
        return messageVM;
    }

    public MessageVM getAllUserList() {
        List<Object[]> data = projectRepository.getAllUserWorkingProjectCount();
        HashMap<Long, UserWorkingProjectCountVM> methodToolsCol = new HashMap<>();
        for (Object[] a : data) {
            UserWorkingProjectCountVM countVM = new UserWorkingProjectCountVM();
            countVM.setTotalWorkingProject(Integer.parseInt(a[1] + ""));
            countVM.setTotalProjectLeader(Integer.parseInt(a[2] + ""));
            countVM.setTotalProjectCoach(Integer.parseInt(a[3] + ""));
            countVM.setTotalProjectMember(Integer.parseInt(a[4] + ""));
            methodToolsCol.put(Long.parseLong(a[0] + ""), countVM);

        }
        List<AuthUser> userList = userRepository.findAllByStatus("Active");
        List<UserVM> userWorkingList = new ArrayList<>();
        for (AuthUser user : userList) {
            UserVM userVM = new UserVM();
            userVM.setId(user.getId());
            userVM.setName(user.getName());

            if (methodToolsCol.get(user.getId()) != null) {
                userVM.setProjectWorkingCount(methodToolsCol.get(user.getId()));
            } else {
                userVM.setWorkingProject(0);
            }
            userWorkingList.add(userVM);
        }
        MessageVM messageVM = new MessageVM();
        messageVM.setData(userWorkingList);
        return messageVM;
    }

    public MessageVM getAllBenifitTypeList() {
        MessageVM messageVM = new MessageVM();
        messageVM.setData(benifitRepository.findAll());
        return messageVM;
    }

    public MessageVM getAllMachineLossList() {
        MessageVM messageVM = new MessageVM();
        HashMap<String, Object> responseObj = new HashMap<>();
//        responseObj.put("machineLossList", machineLossRepository.findAll());
        responseObj.put("sourceLossList", machineLossRepository.getUniqueSoureceLosses());
        responseObj.put("impactedLossList", machineLossRepository.getUniqueImpactedLosses());
        responseObj.put("activeSourceInpactedLoss", machineLossRepository.getActiveSourceInpactedLoss());

        messageVM.setData(responseObj);
        return messageVM;
    }

    public MessageVM changeBMatrixColor(MachineLoss machineLoss) {
        MessageVM messageVM = new MessageVM();
        String id = machineLossRepository.findIdByCategories(machineLoss.getSourceOp(), machineLoss.getSourceOp().replace(" ", "-"), machineLoss.getSourceLoss(), machineLoss.getImpactedOp(), machineLoss.getImpactedOp().replace(" ", "-"), machineLoss.getImpactedLoss());
        if (id != null) {
            long machineId = Long.parseLong(id);
            MachineLoss machineLosses = machineLossRepository.findOne(machineId);
            machineLosses.setValueGreen(machineLoss.isValueGreen());
            machineLosses.setValueRed(machineLoss.isValueRed());
            machineLosses.setValueYellow(machineLoss.isValueYellow());
            machineLossRepository.save(machineLosses);
            messageVM.setData(machineLossRepository.getActiveSourceInpactedLoss());
            return messageVM;
        } else {
            return null;
        }

    }

    public MessageVM getAllTechnicalToolList() {
        MessageVM messageVM = new MessageVM();
        List<TechnicalToolVM> technicalToolList = new ArrayList<>();
        List<TechnicalTool> list = technicalToolRepository.findAll();
        for (TechnicalTool tool : list) {
            TechnicalToolVM toolVM = new TechnicalToolVM();
            toolVM.setTechnicalToolId(tool.getId());
            toolVM.setProfileName(tool.getProfileName());
            toolVM.setTechnicalToolDescription(tool.getTechnicalToolDescription());
            technicalToolList.add(toolVM);
        }
        messageVM.setData(technicalToolList);
        return messageVM;
    }

    public MessageVM getCDYearList() {
        MessageVM messageVM = new MessageVM();
        messageVM.setData(cdYearRepository.findAll());
        return messageVM;
    }

    public MessageVM getKPIsList() {
        MessageVM messageVM = new MessageVM();
        messageVM.setData(kpiRepository.findAll());
        return messageVM;
    }

    public MessageVM getEtuUserList(MatrixFilterVM filterVM) {
        List<AuthUser> userList = new ArrayList<>();
        List<BigInteger> userids = new ArrayList();
        List<Long> useridstoretrive = new ArrayList();
        if (filterVM.getEtuList().isEmpty() || filterVM.getEtuList().size() < 1) {
            userList = userRepository.findAllByStatus("Active");
        } else {
            List<Long> etuIdss = filterVM.getEtuList().stream().map(mapper -> mapper.getId()).collect(Collectors.toList());
            for (Long etuIds : etuIdss) {
                userids = etuRepository.findUserIdsByETUId(etuIds);
            }
            for (BigInteger b : userids) {
                Long userid = b.longValue();
                useridstoretrive.add(userid);
            }
            userList = userRepository.findAllByIdIn(useridstoretrive);
        }
        List<UserVM> userWorkingList = new ArrayList<>();
        for (AuthUser user : userList) {
            UserVM userVM = new UserVM();
            userVM.setId(user.getId());
            userVM.setName(user.getName());
            userVM.setEtuLevel(user.getEtuLevel());
            userWorkingList.add(userVM);
        }
        MessageVM messageVM = new MessageVM();
        messageVM.setData(userWorkingList);
        return messageVM;
    }

    @Transactional
    public MessageVM updateLoss(Loss loss) {
        MessageVM messageVM = new MessageVM();
        lossRepository.updateLoss(loss.getPriority(), loss.getSeverity(), loss.getId());
        messageVM.setData(lossRepository.findAll());
        return messageVM;
    }

    public ArrayList<Loss> getLoss() {
        ArrayList<Loss> loss = (ArrayList<Loss>) lossRepository.findAll();
        return loss;
    }

    public ArrayList<TechnicalTool> getTheTechnicalTools() {
        ArrayList<TechnicalTool> technicalTools = (ArrayList<TechnicalTool>) technicalToolRepository.findAll();
        return technicalTools;
    }

    public ArrayList<MethodsAndTools> getWcmMethods() {
        ArrayList<MethodsAndTools> methodsAndToolsSet = (ArrayList<MethodsAndTools>) methodsAndToolsRepository.findAllcommonandspecificMethods();
        return methodsAndToolsSet;

    }

    public ArrayList<MethodsAndTools> getWcmTools() {
        ArrayList<MethodsAndTools> methodsAndToolsSet = (ArrayList<MethodsAndTools>) methodsAndToolsRepository.findAllcommonandspecificTools();
        return methodsAndToolsSet;

    }

    public ArrayList<MachineOperation> getOperations(Long etu_id) {

        if (etu_id == 0) {
            ArrayList<MachineOperation> machineOperationset = (ArrayList<MachineOperation>) machineOperationRepository.findAll();
            return machineOperationset;
        } else {
            ArrayList<MachineOperation> machineOperationset = (ArrayList<MachineOperation>) machineOperationRepository.findOperationByETU(etu_id);

            return machineOperationset;
        }
    }

    public MethodsAndTools addWcmTool(MethodsAndTools methodsAndTools) {
        methodsAndToolsRepository.save(methodsAndTools);
        return methodsAndTools;
    }

    public MethodsAndTools updateWcmTool(Long id, MethodsAndTools methodsAndTools) {
        MethodsAndTools tool = methodsAndToolsRepository.findOne(id != null ? id : methodsAndTools.getId());

        tool.setLevelFirstTool(methodsAndTools.getLevelFirstTool());
        tool.setLevelSecondTool(methodsAndTools.getLevelSecondTool());
        tool.setReprortPriority(methodsAndTools.getReprortPriority());
        tool.setType(methodsAndTools.getType());
        tool.setPillar_approach(methodsAndTools.getPillar_approach());

        methodsAndToolsRepository.save(tool);
        return methodsAndTools;
    }

    public MethodsAndTools addWcmMethod(MethodsAndTools methodsAndTools) {
        methodsAndToolsRepository.save(methodsAndTools);
        return methodsAndTools;
    }

    public MethodsAndTools updateWcmMethod(Long id, MethodsAndTools methodsAndTools) {

        MethodsAndTools method = methodsAndToolsRepository.findOne(id != null ? id : methodsAndTools.getId());
        method.setId(methodsAndTools.getId());
        method.setLevelFirstTool(methodsAndTools.getLevelFirstTool());
        method.setLevelSecondTool(methodsAndTools.getLevelSecondTool());
        method.setReprortPriority(methodsAndTools.getReprortPriority());
        method.setType(methodsAndTools.getType());
        method.setPillar_approach(methodsAndTools.getPillar_approach());
        methodsAndToolsRepository.save(method);

        return methodsAndTools;
    }

    public Loss addLossType(Loss loss) {
        lossRepository.save(loss);
        return loss;
    }

    public TechnicalTool addTechnicalTool(TechnicalTool technicalTool) {
        technicalToolRepository.save(technicalTool);
        return technicalTool;
    }

    public MachineOperation addOperations(MachineOperationVM machineOperationVM) {
        ETU etu = etuRepository.findOne(machineOperationVM.getEtu_Id());
        MachineOperation machineOperation = new MachineOperation();
        machineOperation.setEtu(etu);
        machineOperation.setMachineCode(machineOperationVM.getMachineCode());
        machineOperation.setMachineName(machineOperationVM.getMachineName());
        machineOperationRepository.save(machineOperation);

        return machineOperation;
    }

    public MachineOperation deleteOperations(Long id) {
        MachineOperation machineOperation = machineOperationRepository.findOne(id);
        machineOperationRepository.delete(machineOperation);
        return machineOperation;
    }

}
