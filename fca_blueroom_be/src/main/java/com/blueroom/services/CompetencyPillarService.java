package com.blueroom.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.CompetencyPillar;
import com.blueroom.entities.Pillar;
import com.blueroom.repository.CompetenciesRepository;
import com.blueroom.repository.CompetencyPillarRepository;
import com.blueroom.vm.CompetencyPillarVM;

@Service
public class CompetencyPillarService {
	
	@Autowired
	CompetencyPillarRepository competencypillarRepository;

	@Autowired
	CompetenciesRepository competencyRepository;
	

	public List<CompetencyPillar> getAllCompetencyPillar() {
		return competencypillarRepository.findAll();
	}


	public 	CompetencyPillar addCompetencyPillar(CompetencyPillar competencypillar) {
		return competencypillarRepository.save(competencypillar);
	}


	public void deleteCompetencyPillar(Long id) {
		CompetencyPillar cp = competencypillarRepository.findOne(id);
        cp.setStatus(false);
		
	}
	public List<CompetencyPillar> getCompetencyPillar(Long id) {
		return competencypillarRepository.getAllById(id);
		
	}


	public CompetencyPillar updateCompetencyPillar(CompetencyPillar competencypillar) {
	
		return null;
	}


	public CompetencyPillarVM addCompetencyPillarv2(CompetencyPillarVM competencypillarVM) {
		
		/*
		 * Competencies competency = competencypillarVM.getCompetency();
		 * competencyRepository.save(competency); for(Pillar pillar :
		 * competencypillarVM.getPillars()) { CompetencyPillar cp = new
		 * CompetencyPillar(); cp.setPillar(pillar); cp.setCompetency(competency);
		 * competencypillarRepository.save(cp); }
		 */
		return competencypillarVM;
	}

}
