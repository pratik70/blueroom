package com.blueroom.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.DevelopmentMethod;
import com.blueroom.entities.DevelopmentPlan;
import com.blueroom.entities.DevelopmentTool;
import com.blueroom.entities.UserDesiredLevel;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.repository.CompetenciesRepository;
import com.blueroom.repository.DevelopmentMethodsRepository;
import com.blueroom.repository.DevelopmentPlanRepository;
import com.blueroom.repository.DevelopmentToolRepository;
import com.blueroom.repository.UserDesiredLevelRepository;
import com.blueroom.vm.DevelopmentPlanVM;
import com.blueroom.vm.IDVM;

@Service
public class DevelopmentPlanService {
	@Value("${userFolderPath}")
	String userFolderPath;

	@Autowired
	DevelopmentPlanRepository developmentPlanRepository;

	@Autowired
	DevelopmentToolRepository developmentToolRepository;

	@Autowired
	CompetenciesRepository competenciesRepository;

	@Autowired
	AuthUserRepository authUserRepository;

	@Autowired
	DevelopmentMethodsService developmentMethodsService;

	@Autowired
	DevelopmentToolService developmentToolService;

	@Autowired
	UserDesiredLevelRepository userDesiredLevelRepository;

	@Autowired
	DevelopmentMethodsRepository developmentMethodsRepository;

	@Autowired
	PillarService pillarService;

	public DevelopmentPlanVM updateDevelopmentPlan(DevelopmentPlanVM developmentPlanVM) {

		Long methodId = developmentPlanVM.getMethodId();

		List<DevelopmentPlan> existing = developmentPlanRepository.getDevelopmentPlan(developmentPlanVM.getUserId(),
				developmentPlanVM.getCompetencyId(), developmentPlanVM.getYear(), developmentPlanVM.getType(), methodId);
		developmentPlanRepository.delete(existing);
		Competencies competency = competenciesRepository.findOne(developmentPlanVM.getCompetencyId());
		AuthUser user = authUserRepository.findOne(developmentPlanVM.getUserId());
		Integer year = developmentPlanVM.getYear();
		for (IDVM id : developmentPlanVM.getToolId()) {
			DevelopmentPlan developmentPlan = new DevelopmentPlan();
			developmentPlan.setCompetency(competency);
			developmentPlan.setUser(user);
			developmentPlan.setYear(year);
			developmentPlan.setTool(developmentToolRepository.findOne(id.getId()));
			developmentPlan.setType(developmentPlanVM.getType());
			developmentPlanRepository.save(developmentPlan);
		}

		return developmentPlanVM;
	}

	public ByteArrayInputStream exportDevelopmentPlan(Long userId, Long year, Long pillarId) throws IOException {
		ClassPathResource resource = new ClassPathResource("TeamCompetencyRadarChart.xlsx");
		File file = resource.getFile();
		FileInputStream inputstream = new FileInputStream(file);
		try (

				XSSFWorkbook workbook = new XSSFWorkbook(inputstream);
				ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			Sheet sheetBehavioral = createSheetAndSetHeaders(workbook, "Behavioral");
			Sheet sheetTechnical = createSheetAndSetHeaders(workbook, "Technical");
			Sheet sheetWCM = createSheetAndSetHeaders(workbook, "WCM");

			sheetBehavioral = writeDevelopmentPlanToExcel(workbook, sheetBehavioral, userId, "Behavioral", year,
					pillarId);
			sheetTechnical = writeDevelopmentPlanToExcel(workbook, sheetTechnical, userId, "Technical", year, pillarId);
			sheetWCM = writeDevelopmentPlanToExcel(workbook, sheetWCM, userId, "WCM", year, pillarId);

			workbook.write(out);

			return new ByteArrayInputStream(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException("fail to export data to Excel file: " + e.getMessage());
		}

	}

	static int sheetIndex1 = 0;

	private Sheet createSheetAndSetHeaders(Workbook workbook, String sheetName) {

		Sheet sheet = workbook.getSheetAt(sheetIndex1);
		if (sheetIndex1 == 0) {
			workbook.setSheetName(sheetIndex1, "Behaviour Sheet");
		} else if (sheetIndex1 == 1) {
			workbook.setSheetName(sheetIndex1, "Technical Sheet");
		} else if (sheetIndex1 == 2) {
			workbook.setSheetName(sheetIndex1, "WCM Sheet");
		}

		sheetIndex1++;
		if (sheetIndex1 == 3) {
			sheetIndex1 = 0;
		}

		XSSFCellStyle headerStyle = (XSSFCellStyle) workbook.createCellStyle();
		headerStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(184, 204, 228)));
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 8);
		font.setBold(true);
		headerStyle.setFont(font);

		Row rowhead = sheet.createRow((short) 2);
		Cell headerCell = rowhead.createCell(0);
		headerCell.setCellValue("Levels");
		headerCell.setCellStyle(headerStyle);

		headerCell = rowhead.createCell(1);
		headerCell.setCellValue("Competency");
		headerCell.setCellStyle(headerStyle);

		headerCell = rowhead.createCell(2);
		headerCell.setCellValue("Desired Level");
		headerCell.setCellStyle(headerStyle);

		headerCell = rowhead.createCell(3);
		headerCell.setCellValue("Current Level");
		headerCell.setCellStyle(headerStyle);

		headerCell = rowhead.createCell(4);
		headerCell.setCellValue("Last Year Level");
		headerCell.setCellStyle(headerStyle);

		XSSFCellStyle headerStyle1 = (XSSFCellStyle) workbook.createCellStyle();
		headerStyle1.setFillForegroundColor(new XSSFColor(new java.awt.Color(184, 204, 228)));
		headerStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle1.setFont(font);

		Cell headerCell1 = rowhead.createCell(5);
		headerStyle1.setAlignment(HorizontalAlignment.CENTER);
		rowhead.createCell(5).setCellValue("Gap");
		headerCell1.setCellStyle(headerStyle1);

		List<Map<String, Object>> developmentMethods = developmentMethodsService.getAllDevelopmentMethods();

		XSSFCellStyle headerStyle2 = (XSSFCellStyle) workbook.createCellStyle();
		headerStyle2.setFillForegroundColor(new XSSFColor(new java.awt.Color(184, 204, 228)));
		headerStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle2.setFont(font);

		XSSFFont font2 = ((XSSFWorkbook) workbook).createFont();
		font2.setFontName("Arial");
		font2.setFontHeightInPoints((short) 8);
		font2.setBold(true);
		headerStyle2.setFont(font);
		for (int cnt = 0; cnt < developmentMethods.size(); cnt++) {
			DevelopmentMethod developmentMethod = (DevelopmentMethod) developmentMethods.get(cnt).get("method");
			headerCell = rowhead.createCell(6 + cnt);
			String developmentMethodName =developmentMethod.getName();
			
			if(developmentMethodName.length()>=38)
			{
			headerCell.setCellValue(developmentMethodName.substring(0,38));
			System.out.println(developmentMethodName.substring(0,38));
			}
			else
			{
				headerCell.setCellValue(developmentMethod.getName());
			}
			headerCell.setCellStyle(headerStyle2);

		}

		return sheet;
	}

	private Sheet writeDevelopmentPlanToExcel(Workbook workbook, Sheet sheet, Long userId, String competencyArea,
			Long year, Long pillarId) {
		List<Map<String, Object>> developmentMethods = developmentMethodsService.getAllDevelopmentMethods();
		String[] methodsoFDevelopmentName = new String[developmentMethods.size()];

		for (int cnt = 0; cnt < developmentMethods.size(); cnt++) {
			DevelopmentMethod developmentMethod = (DevelopmentMethod) developmentMethods.get(cnt).get("method");
			methodsoFDevelopmentName[cnt] = developmentMethod.getName();
		}

		List<Map<String, Object>> developmentPlan = this.getDevelopmentPlanByUserIdAndCompetencyAreaAndYear(userId,
				competencyArea, year, pillarId);

		int rowCnt = 3;
		for (Map<String, Object> planObj : developmentPlan) {
			Row row = sheet.createRow((short) rowCnt);

			XSSFCellStyle headerStyle2 = (XSSFCellStyle) workbook.createCellStyle();
			headerStyle2.setFillForegroundColor(new XSSFColor(new java.awt.Color(230, 184, 183)));
			headerStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			XSSFCellStyle headerStyle3 = (XSSFCellStyle) workbook.createCellStyle();
			headerStyle3.setFillForegroundColor(new XSSFColor(new java.awt.Color(254, 255, 102)));
			headerStyle3.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			XSSFCellStyle headerStyle4 = (XSSFCellStyle) workbook.createCellStyle();
			headerStyle4.setFillForegroundColor(new XSSFColor(new java.awt.Color(152, 252, 102)));
			headerStyle4.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			Cell headerCell0 = row.createCell(0);
			if (planObj.get("levels").equals("Leading Change") || planObj.get("levels").equals("Technical Competencies")
					|| planObj.get("levels").equals("Reactive")) {
				row.createCell(0).setCellValue((String) planObj.get("levels"));
				headerCell0.setCellStyle(headerStyle2);
			}
			if (planObj.get("levels").equals("Leading People") || planObj.get("levels").equals("Preventive")) {
				row.createCell(0).setCellValue((String) planObj.get("levels"));
				headerCell0.setCellStyle(headerStyle3);
			}
			if (planObj.get("levels").equals("Proactive")) {
				row.createCell(0).setCellValue((String) planObj.get("levels"));
				headerCell0.setCellStyle(headerStyle4);
			}

			Competencies competency = (Competencies) planObj.get("competency");
			row.createCell(1).setCellValue(competency.getName());
			row.createCell(2).setCellValue((Long) planObj.get("desired_level"));
			row.createCell(3).setCellValue((Long) planObj.get("current_level"));
			row.createCell(4).setCellValue((Long) planObj.get("last_year_level"));

			CellStyle headerStyle1 = workbook.createCellStyle();
			headerStyle1.setAlignment(HorizontalAlignment.CENTER);
			headerStyle1.setFillForegroundColor(IndexedColors.RED.getIndex());
			headerStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle headerStyle5 = workbook.createCellStyle();
			headerStyle5.setAlignment(HorizontalAlignment.CENTER);
			headerStyle5.setFillForegroundColor(IndexedColors.GREEN.getIndex());
			headerStyle5.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			XSSFFont font = ((XSSFWorkbook) workbook).createFont();
			font.setColor(IndexedColors.WHITE.getIndex());
			headerStyle1.setFont(font);

			Long gap = (Long) planObj.get("desired_level") - (Long) planObj.get("current_level");
			Cell headerCell5 = row.createCell(5);
			if(gap>0 || gap<0)
			{
			row.createCell(5).setCellValue(gap);
			headerCell5.setCellStyle(headerStyle1);
			}
			if(gap==0)
			{
				row.createCell(5).setCellValue(gap);
				headerCell5.setCellStyle(headerStyle5);	
			}

			int cellCnt = 6;
			for (String methodName : methodsoFDevelopmentName) {
				String tools = "";
				List<Map<String, Object>> toolList = (List<Map<String, Object>>) planObj.get(methodName);

				for (Map<String, Object> tool : toolList) {
					boolean isSelected = (boolean) tool.get("selected");
					if (isSelected) {
						tools = tools + tool.get("tool_name") + ", ";
					}
				}

				if (tools != "") {
					tools = tools.substring(0, tools.length() - 2);
				}

				row.createCell(cellCnt).setCellValue(tools);

				cellCnt++;
			}

			rowCnt++;
		}

		return sheet;

	}

	public ByteArrayInputStream exportTeamMemberDevelopmentPlan(Long userId, Long year, Long pillarId)
			throws IOException {
		ClassPathResource resource = new ClassPathResource("CompetencyRadarChart.xlsx");
		File file = resource.getFile();
		FileInputStream inputstream = new FileInputStream(file);
		try (XSSFWorkbook workbook = new XSSFWorkbook(inputstream);
				ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			Sheet sheetBehavioral = createSheetAndSetHeaders_TeamMember(workbook, "Behavioral");
			Sheet sheetTechnical = createSheetAndSetHeaders_TeamMember(workbook, "Technical");
			Sheet sheetWCM = createSheetAndSetHeaders_TeamMember(workbook, "WCM");

			sheetBehavioral = writeDevelopmentPlanToExcel_TeamMember(workbook, sheetBehavioral, userId, "Behavioral",
					year, pillarId);
			sheetTechnical = writeDevelopmentPlanToExcel_TeamMember(workbook, sheetTechnical, userId, "Technical", year,
					pillarId);
			sheetWCM = writeDevelopmentPlanToExcel_TeamMember(workbook, sheetWCM, userId, "WCM", year, pillarId);

			workbook.write(out);

			return new ByteArrayInputStream(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException("fail to export data to Excel file: " + e.getMessage());
		}
	}

	static int sheetIndex = 0;

	private Sheet createSheetAndSetHeaders_TeamMember(Workbook workbook, String sheetName) throws IOException {

		Sheet sheet = workbook.getSheetAt(sheetIndex);
		if (sheetIndex == 0) {
			workbook.setSheetName(sheetIndex, "Behaviour Sheet");
		} else if (sheetIndex == 1) {
			workbook.setSheetName(sheetIndex, "Technical Sheet");
		} else if (sheetIndex == 2) {
			workbook.setSheetName(sheetIndex, "WCM Sheet");
		}
		sheetIndex++;
		if (sheetIndex == 3) {
			sheetIndex = 0;
		}

		XSSFCellStyle headerStyle = (XSSFCellStyle) workbook.createCellStyle();
		headerStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(184, 204, 228)));
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 8);
		font.setBold(true);
		headerStyle.setFont(font);
		
		XSSFCellStyle headerStyle2 = (XSSFCellStyle) workbook.createCellStyle();
		headerStyle2.setFillForegroundColor(new XSSFColor(new java.awt.Color(228, 109, 10)));
		headerStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		XSSFCellStyle headerStyle3 = (XSSFCellStyle) workbook.createCellStyle();
		headerStyle3.setFillForegroundColor(new XSSFColor(new java.awt.Color(31, 73, 125)));
		headerStyle3.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle3.setAlignment(HorizontalAlignment.CENTER);
	

		XSSFFont font3 = ((XSSFWorkbook) workbook).createFont();
		font3.setFontName("Arial");
		font3.setFontHeightInPoints((short) 10);
		font3.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));
		font3.setBold(true);
		headerStyle3.setFont(font3);

		

		Row rowhead1 = sheet.createRow((short) 3);
		Row rowhead2 = sheet.createRow((short) 2);
		CellStyle cellStyle = workbook.createCellStyle();

		Cell headerCell = rowhead1.createCell(0);
		headerCell.setCellValue("Levels");
		headerCell.setCellStyle(headerStyle);

		headerCell = rowhead1.createCell(1);
		headerCell.setCellValue("Competency");
		headerCell.setCellStyle(headerStyle);
		sheet.autoSizeColumn(1);

		headerCell = rowhead1.createCell(2);
		headerCell.setCellValue("Desired L");
		headerCell.setCellStyle(headerStyle);
		sheet.autoSizeColumn(2);

		headerCell = rowhead1.createCell(3);
		headerCell.setCellValue("Current L");
		headerCell.setCellStyle(headerStyle);
		sheet.autoSizeColumn(3);

		headerCell = rowhead1.createCell(4);
		headerCell.setCellValue("Last Year L");
		headerCell.setCellStyle(headerStyle);
		sheet.autoSizeColumn(4);

		XSSFCellStyle headerStyle1 = (XSSFCellStyle) workbook.createCellStyle();
		headerStyle1.setFillForegroundColor(new XSSFColor(new java.awt.Color(184, 204, 228)));
		headerStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle1.setFont(font);

		Cell headerCell1 = rowhead1.createCell(5);
		headerStyle1.setAlignment(HorizontalAlignment.CENTER);
		rowhead1.createCell(5).setCellValue("Gap");
		headerCell1.setCellStyle(headerStyle1);

		
		XSSFCellStyle headerStyle4 = (XSSFCellStyle) workbook.createCellStyle();
		headerStyle4.setFillForegroundColor(new XSSFColor(new java.awt.Color(184, 204, 228)));
		headerStyle4.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle4.setFont(font);
		headerStyle4.setAlignment(HorizontalAlignment.LEFT);
		
		List<Map<String, Object>> developmentMethods = developmentMethodsService.getAllDevelopmentMethods();
		
		for(int i=0;i<=5;i++)
		{
			headerCell = rowhead2.createCell(i);
			headerCell.setCellValue("");
			headerCell.setCellStyle(headerStyle2);
			sheet.autoSizeColumn(1);
		}

		for (int cnt = 0, index = 6; cnt < developmentMethods.size(); cnt++, index = index + 1) {
			DevelopmentMethod developmentMethod = (DevelopmentMethod) developmentMethods.get(cnt).get("method");
			headerCell = rowhead2.createCell(index);
		    headerCell.setCellValue(developmentMethod.getName());	
			headerCell.setCellStyle(headerStyle3);
			sheet.autoSizeColumn(1);
//			headerCell = rowhead2.createCell(index + 1);
//			headerCell.setCellValue(developmentMethod.getName() + "_actual");
//			headerCell.setCellStyle(headerStyle3);
//			sheet.autoSizeColumn(1);
		}
		for (int cnt = 0, index = 6; cnt < developmentMethods.size(); cnt++, index = index + 1) {
			DevelopmentMethod developmentMethod = (DevelopmentMethod) developmentMethods.get(cnt).get("method");
			headerCell = rowhead1.createCell(index);
			headerCell.setCellStyle(headerStyle4);
			headerCell.setCellValue("Recommended                                                                        Actual");
//			headerCell.setCellStyle(headerStyle1);
//			headerCell.setCellValue("actual");
			sheet.autoSizeColumn(1);
//			headerCell = rowhead1.createCell(index + 1);
//			headerCell.setCellValue(developmentMethod.getName() + "actual");
//			headerCell.setCellStyle(headerStyle1);
			sheet.autoSizeColumn(1);
		}
		return sheet;
	}

	private Sheet writeDevelopmentPlanToExcel_TeamMember(Workbook workbook, Sheet sheet, Long userId,
			String competencyArea, Long year, Long pillarId) {
		List<Map<String, Object>> developmentMethods = developmentMethodsService.getAllDevelopmentMethods();
		String[] methodsoFDevelopmentName = new String[developmentMethods.size()];
		CellStyle headerStyle = workbook.createCellStyle();

		for (int cnt = 0; cnt < developmentMethods.size(); cnt++) {
			DevelopmentMethod developmentMethod = (DevelopmentMethod) developmentMethods.get(cnt).get("method");
			methodsoFDevelopmentName[cnt] = developmentMethod.getName();
		}

		List<Map<String, Object>> developmentPlan = this
				.getTeamMemberDevelopmentPlanByUserIdAndCompetencyAreaAndYear(userId, competencyArea, year, pillarId);

		int rowCnt = 4;
		for (Map<String, Object> planObj : developmentPlan) {
			Row row = sheet.createRow((short) rowCnt);

			XSSFCellStyle headerStyle2 = (XSSFCellStyle) workbook.createCellStyle();
			headerStyle2.setFillForegroundColor(new XSSFColor(new java.awt.Color(230, 184, 183)));
			headerStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			XSSFCellStyle headerStyle3 = (XSSFCellStyle) workbook.createCellStyle();
			headerStyle3.setFillForegroundColor(new XSSFColor(new java.awt.Color(254, 255, 102)));
			headerStyle3.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			XSSFCellStyle headerStyle4 = (XSSFCellStyle) workbook.createCellStyle();
			headerStyle4.setFillForegroundColor(new XSSFColor(new java.awt.Color(152, 252, 102)));
			headerStyle4.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			
			CellStyle headerStyle5 = workbook.createCellStyle();
			headerStyle5.setAlignment(HorizontalAlignment.CENTER);
			headerStyle5.setFillForegroundColor(IndexedColors.GREEN.getIndex());
			headerStyle5.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			Cell headerCell0 = row.createCell(0);
			if (planObj.get("levels").equals("Leading Change") || planObj.get("levels").equals("Technical Competencies")
					|| planObj.get("levels").equals("Reactive")) {
				row.createCell(0).setCellValue((String) planObj.get("levels"));
				headerCell0.setCellStyle(headerStyle2);
			}
			if (planObj.get("levels").equals("Leading People") || planObj.get("levels").equals("Preventive")) {
				row.createCell(0).setCellValue((String) planObj.get("levels"));
				headerCell0.setCellStyle(headerStyle3);
			}
			if (planObj.get("levels").equals("Proactive")) {
				row.createCell(0).setCellValue((String) planObj.get("levels"));
				headerCell0.setCellStyle(headerStyle4);
			}

			headerStyle.setAlignment(HorizontalAlignment.CENTER);
			Competencies competency = (Competencies) planObj.get("competency");
			row.createCell(1).setCellValue(competency.getName());
			Cell headerCell2 = row.createCell(2);
			row.createCell(2).setCellValue((Long) planObj.get("desired_level"));
			headerCell2.setCellStyle(headerStyle);
			Cell headerCell3 = row.createCell(3);
			row.createCell(3).setCellValue((Long) planObj.get("current_level"));
			headerCell3.setCellStyle(headerStyle);
			Cell headerCell4 = row.createCell(4);
			row.createCell(4).setCellValue((Long) planObj.get("last_year_level"));
			headerCell4.setCellStyle(headerStyle);

			CellStyle headerStyle1 = workbook.createCellStyle();
			headerStyle1.setAlignment(HorizontalAlignment.CENTER);
			headerStyle1.setFillForegroundColor(IndexedColors.RED.getIndex());
			headerStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			XSSFFont font = ((XSSFWorkbook) workbook).createFont();
			font.setColor(IndexedColors.WHITE.getIndex());
			headerStyle1.setFont(font);

			Long gap = (Long) planObj.get("desired_level") - (Long) planObj.get("current_level");
			Cell headerCell5 = row.createCell(5);
			if(gap>0 || gap<0)
			{
			row.createCell(5).setCellValue(gap);
			headerCell5.setCellStyle(headerStyle1);
			}
			if(gap==0)
			{
				row.createCell(5).setCellValue(gap);
				headerCell5.setCellStyle(headerStyle5);	
			}

			int cellCnt = 6;
			for (String methodName : methodsoFDevelopmentName) {
				String tools = "";
				List<Map<String, Object>> recommendedToolList = (List<Map<String, Object>>) planObj.get(methodName + "_recommended");

				for (Map<String, Object> tool : recommendedToolList) {
					boolean isSelected = (boolean) tool.get("selected");
					if (isSelected) {
						tools = tools + tool.get("tool_name") + ", ";
					}
				}

				if (tools != "") {
					tools = tools.substring(0, tools.length() - 2);
				}

				row.createCell(cellCnt).setCellValue(tools);

				cellCnt++;

				tools = "";

				List<Map<String, Object>> actualToolList = (List<Map<String, Object>>) planObj
						.get(methodName + "_actual");

				for (Map<String, Object> tool : actualToolList) {
					boolean isSelected = (boolean) tool.get("selected");
					if (isSelected) {
						tools = tools + tool.get("tool_name") + ", ";
					}
				}

				if (tools != "") {
					tools = tools.substring(0, tools.length() - 2);
				}

				row.createCell(cellCnt).setCellValue(tools);

				cellCnt++;
			}

			rowCnt++;
		}

		return sheet;
	}

	public void createDevelopmentPlan(Map<String, Object> data) {
		Integer userId = (Integer) data.get("userId");
		@SuppressWarnings("unchecked")
		List<Long> competencyIds = (List<Long>) data.get("competencyIds");
		Integer year = (Integer) data.get("year");

		userDesiredLevelRepository.createDevelopmentPlan(userId, competencyIds, year);
	}

	public void submitDevelopmentPlan(Map<String, Object> data) {
		Integer userId = (Integer) data.get("userId");
		@SuppressWarnings("unchecked")
		List<Long> competencyIds = (List<Long>) data.get("competencyIds");
		Integer year = (Integer) data.get("year");

		userDesiredLevelRepository.submitDevelopmentPlan(userId, competencyIds, year);
	}

	public void approveDevelopmentPlan(Map<String, Object> data) {
		Integer userId = (Integer) data.get("userId");
		@SuppressWarnings("unchecked")
		List<Long> competencyIds = (List<Long>) data.get("competencyIds");
		Integer year = (Integer) data.get("year");

		userDesiredLevelRepository.approveDevelopmentPlan(userId, competencyIds, year);
	}

	public List<Map<String, Object>> getDevelopmentPlanByUserIdAndCompetencyAreaAndYear(Long userId,
			String competencyArea, Long year, Long pillarId) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		List<Competencies> competencies = null;

		if (competencyArea.equals("WCM")) {
			competencies = (List<Competencies>) competenciesRepository.getCompetenciesByPillarId(pillarId);
		}else if(competencyArea.equals("Technical")){
			competencies = competenciesRepository.findByAreaAndUser(competencyArea, userId);
		}else{
			competencies = competenciesRepository.findByArea(competencyArea);
		}

		for (Competencies competency : competencies) {
			Map<String, Object> map = new HashMap<String, Object>();

			map.put("levels", competency.getType());
			map.put("competency", competency);

			List<UserDesiredLevel> userDesiredLevels = userDesiredLevelRepository
					.getUserDesiredSkillByUserIdAndCompetencyIdAndYear(userId, competency.getId(), year);

			if (userDesiredLevels.size() > 0) {
				map.put("current_level", userDesiredLevels.get(0).getActualSkill());
				map.put("desired_level", userDesiredLevels.get(0).getRequiredSkill());
				map.put("planStatus", userDesiredLevels.get(0).getPlanStatus());
				map.put("gap", userDesiredLevels.get(0).getRequiredSkill() - userDesiredLevels.get(0).getActualSkill());
			} else {
				map.put("current_level", 0L);
				map.put("desired_level", 0L);
				map.put("planStatus", null);
				map.put("gap", 0);
			}

			List<UserDesiredLevel> lastYearUserDesiredLevels = userDesiredLevelRepository
					.getUserDesiredSkillByUserIdAndCompetencyIdAndYear(userId, competency.getId(), year - 1);

			if (lastYearUserDesiredLevels.size() > 0) {
				map.put("last_year_level", lastYearUserDesiredLevels.get(0).getActualSkill());
			} else {
				map.put("last_year_level", 0L);
			}

			List<DevelopmentMethod> developmentMethods = developmentMethodsRepository.findAll();

			for (DevelopmentMethod developmentMethod : developmentMethods) {

				List<DevelopmentTool> developmentTools = developmentToolRepository
						.findByDevelopmentMethodId(developmentMethod.getId());

				List<Object> objList = developmentPlanRepository.getToolIdsByUserIdAndCompetencyIdAndYearAndType(userId,
						competency.getId(), year, "recommended");

				List<Long> toolIds = new ArrayList<Long>();

				for (Object obj : objList) {
					toolIds.add(((BigInteger) obj).longValue());
				}

				List<Map<String, Object>> toolList = new ArrayList<Map<String, Object>>();
				for (DevelopmentTool developmentTool : developmentTools) {
					Map<String, Object> developmentToolMap = new HashMap<String, Object>();
					developmentToolMap.put("tool_id", developmentTool.getId());
					developmentToolMap.put("tool_name", developmentTool.getName());

					if (toolIds.contains(developmentTool.getId())) {
						developmentToolMap.put("selected", true);
					} else {
						developmentToolMap.put("selected", false);
					}
					toolList.add(developmentToolMap);
				}

				map.put(developmentMethod.getName(), toolList);

			}

			result.add(map);
		}

		return result;

	}

	public List<Map<String, Object>> getTeamMemberDevelopmentPlanByUserIdAndCompetencyAreaAndYear(Long userId,
			String competencyArea, Long year, Long pillarId) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

		List<Competencies> competencies = null;

		if (competencyArea.equals("WCM")) {
			competencies = (List<Competencies>) competenciesRepository.getCompetenciesByPillarId(pillarId);
		}else if(competencyArea.equals("Technical")){
			competencies = competenciesRepository.findByAreaAndUser(competencyArea, userId);
		}else {
			competencies = competenciesRepository.findByArea(competencyArea);
		}

		for (Competencies competency : competencies) {
			Map<String, Object> map = new HashMap<String, Object>();

			map.put("levels", competency.getType());
			map.put("competency", competency);

			List<UserDesiredLevel> userDesiredLevels = userDesiredLevelRepository
					.getUserDesiredSkillByUserIdAndCompetencyIdAndYear(userId, competency.getId(), year);

			if (userDesiredLevels.size() > 0) {
				map.put("current_level", userDesiredLevels.get(0).getActualSkill());
				map.put("desired_level", userDesiredLevels.get(0).getRequiredSkill());
				map.put("planStatus", userDesiredLevels.get(0).getPlanStatus());
				map.put("gap", userDesiredLevels.get(0).getRequiredSkill() - userDesiredLevels.get(0).getActualSkill());
			} else {
				map.put("current_level", 0L);
				map.put("desired_level", 0L);
				map.put("planStatus", null);
				map.put("gap", 0);
			}

			List<UserDesiredLevel> lastYearUserDesiredLevels = userDesiredLevelRepository
					.getUserDesiredSkillByUserIdAndCompetencyIdAndYear(userId, competency.getId(), year - 1);

			if (lastYearUserDesiredLevels.size() > 0) {
				map.put("last_year_level", lastYearUserDesiredLevels.get(0).getActualSkill());
			} else {
				map.put("last_year_level", 0L);
			}

			List<DevelopmentMethod> developmentMethods = developmentMethodsRepository.findAll();

			for (DevelopmentMethod developmentMethod : developmentMethods) {

				List<DevelopmentTool> developmentTools = developmentToolRepository
						.findByDevelopmentMethodId(developmentMethod.getId());

				List<Object> objListRecommended = developmentPlanRepository
						.getToolIdsByUserIdAndCompetencyIdAndYearAndType(userId, competency.getId(), year,
								"recommended");

				List<Long> toolIdsRecommended = new ArrayList<Long>();

				for (Object obj : objListRecommended) {
					toolIdsRecommended.add(((BigInteger) obj).longValue());
				}

				List<Object> objListActual = developmentPlanRepository
						.getToolIdsByUserIdAndCompetencyIdAndYearAndType(userId, competency.getId(), year, "actual");

				List<Long> toolIdsActual = new ArrayList<Long>();

				for (Object obj : objListActual) {
					toolIdsActual.add(((BigInteger) obj).longValue());
				}

				List<Map<String, Object>> recommendedToolList = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> actualToolList = new ArrayList<Map<String, Object>>();

				for (DevelopmentTool developmentTool : developmentTools) {
					Map<String, Object> recommendedDevelopmentToolMap = new HashMap<String, Object>();
					recommendedDevelopmentToolMap.put("tool_id", developmentTool.getId());
					recommendedDevelopmentToolMap.put("tool_name", developmentTool.getName());

					if (toolIdsRecommended.contains(developmentTool.getId())) {
						recommendedDevelopmentToolMap.put("selected", true);
					} else {
						recommendedDevelopmentToolMap.put("selected", false);
					}
					recommendedToolList.add(recommendedDevelopmentToolMap);

					Map<String, Object> actualDevelopmentToolMap = new HashMap<String, Object>();
					actualDevelopmentToolMap.put("tool_id", developmentTool.getId());
					actualDevelopmentToolMap.put("tool_name", developmentTool.getName());

					if (toolIdsActual.contains(developmentTool.getId())) {
						actualDevelopmentToolMap.put("selected", true);
					} else {
						actualDevelopmentToolMap.put("selected", false);
					}
					actualToolList.add(actualDevelopmentToolMap);
				}

				map.put(developmentMethod.getName() + "_recommended", recommendedToolList);
				map.put(developmentMethod.getName() + "_actual", actualToolList);

			}

			result.add(map);
		}

		return result;
	}

	public Map<String, Map<String, Long>> getTeamMemberGapClosure(Long userId, Long year) {
		Map<String, Map<String, Long>> map = new HashMap<String, Map<String, Long>>();
		List<Object[]> list = userDesiredLevelRepository.getGapClosureByUserId(userId, year);
		String type;
		for (Object[] obj : list) {
			Map<String, Long> typeRecordMap = new HashMap<String, Long>();
			typeRecordMap.put("actualSkill", ((BigDecimal) obj[0]).longValue());
			typeRecordMap.put("requiredSkill", ((BigDecimal) obj[1]).longValue());
			typeRecordMap.put("gap", (((BigDecimal) obj[1]).longValue() - ((BigDecimal) obj[0]).longValue()));
			type = (String) obj[2];

			map.put(type.replaceAll("\\s", ""), typeRecordMap);
		}
		return map;
	}

	public Object getPillarTeamGapClosure(Long pillarId, Long year) {
		Map<String, Map<String, Long>> map = new HashMap<String, Map<String, Long>>();
		List<Object[]> list = userDesiredLevelRepository.getGapClosureByPillarId(pillarId, year);
		String type;
		for (Object[] obj : list) {
			Map<String, Long> typeRecordMap = new HashMap<String, Long>();
			typeRecordMap.put("actualSkill", ((BigDecimal) obj[0]).longValue());
			typeRecordMap.put("requiredSkill", ((BigDecimal) obj[1]).longValue());
			typeRecordMap.put("gap", (((BigDecimal) obj[1]).longValue() - ((BigDecimal) obj[0]).longValue()));
			type = (String) obj[2];

			map.put(type.replaceAll("\\s", ""), typeRecordMap);
		}
		return map;
	}

	public Object getPlantHeadTeamGapClosure(Long year) {
		Map<String, Map<String, Long>> map = new HashMap<String, Map<String, Long>>();
		List<Object[]> list = userDesiredLevelRepository.getPlantHeadTeamGapClosure(year);
		String type;
		for (Object[] obj : list) {
			Map<String, Long> typeRecordMap = new HashMap<String, Long>();
			typeRecordMap.put("actualSkill", ((BigDecimal) obj[0]).longValue());
			typeRecordMap.put("requiredSkill", ((BigDecimal) obj[1]).longValue());
			typeRecordMap.put("gap", (((BigDecimal) obj[1]).longValue() - ((BigDecimal) obj[0]).longValue()));
			type = (String) obj[2];

			map.put(type.replaceAll("\\s", ""), typeRecordMap);
		}
		return map;
	}
}
