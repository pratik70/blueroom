package com.blueroom.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.CMatrices;
import com.blueroom.entities.ETU;
import com.blueroom.entities.KPI;
import com.blueroom.entities.LossType;
import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.Pillar;
import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectBenefitIndents;
import com.blueroom.entities.ProjectBenefitIndentsEmatrix;
import com.blueroom.entities.ProjectFMatrixSavings;
import com.blueroom.entities.ProjectKIP;
import com.blueroom.entities.ProjectMethodsAndTools;
import com.blueroom.entities.ProjectPlanRemarks;
import com.blueroom.entities.ProjectSupportingPillars;
import com.blueroom.entities.ProjectTeamMember;
import com.blueroom.entities.ProjectTechnicalTools;
import com.blueroom.entities.SkillInventryEntity;
import com.blueroom.entities.UserTechnicalToolSkillSet;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.CMatricesRepository;
import com.blueroom.repository.ETURepository;
import com.blueroom.repository.KPIRepository;
import com.blueroom.repository.LossTypeRepository;
import com.blueroom.repository.MachineRepository;
import com.blueroom.repository.MethodsAndToolsRepository;
import com.blueroom.repository.PillarRepository;
import com.blueroom.repository.PillarsMethodsAndToolsRepository;
import com.blueroom.repository.ProcessRepository;
import com.blueroom.repository.ProjectBenefitIndentsEmatrixRepository;
import com.blueroom.repository.ProjectBenefitIndentsRepository;
import com.blueroom.repository.ProjectFMatrixSavingsRepository;
import com.blueroom.repository.ProjectKIPRepository;
import com.blueroom.repository.ProjectMethodsAndToolsRepository;
import com.blueroom.repository.ProjectPlanRemarksRepository;
import com.blueroom.repository.ProjectRepository;
import com.blueroom.repository.ProjectSupportingPillarsRepository;
import com.blueroom.repository.ProjectTeamMemberRepository;
import com.blueroom.repository.ProjectTechnicalToolsRepository;
import com.blueroom.repository.RoleRepository;
import com.blueroom.repository.UserPillarRepository;
import com.blueroom.repository.UserSkillSetRepository;
import com.blueroom.repository.UserTechnicalToolSkillSetRepository;
import com.blueroom.vm.CMatricesVM;
import com.blueroom.vm.CMatrixFilterVM;
import com.blueroom.vm.DMatricesVM;
import com.blueroom.vm.EMatricesVM;
import com.blueroom.vm.EtuSavingsVM;
import com.blueroom.vm.MatricesVM;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;
import com.blueroom.vm.PillarSAvingsVM;
import com.blueroom.vm.ProjectFMatrixSavingsVM;

@Service
public class MatricesService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ProjectTeamMemberRepository projectTeamMemberRepository;

    @Autowired
    PillarRepository pillarRepository;

    @Autowired
    ProjectSupportingPillarsRepository supportingPillarsRepository;

    @Autowired
    ProjectMethodsAndToolsRepository projectMethodsAndToolsRepository;

    @Autowired
    PillarsMethodsAndToolsRepository pillarsMethodsAndToolsRepository;

    @Autowired
    MethodsAndToolsRepository methodsAndToolsRepository;

    @Autowired
    ProjectBenefitIndentsRepository projectBenefitIndentsRepository;

    @Autowired
    ProjectBenefitIndentsEmatrixRepository projectBenefitIndentsEmatrixRepository;

    @Autowired
    ProjectFMatrixSavingsRepository projectFMatrixSavingsRepository;

    @Autowired
    CMatricesRepository cMatricesRepository;

    @Autowired
    LossTypeRepository lossTypeRepository;

    @Autowired
    ProjectPlanRemarksRepository projectPlanRemarksRepository;

    @Autowired
    ProjectTechnicalToolsRepository projectTechnicalToolsRepository;

    @Autowired
    UserSkillSetRepository userSkillSetRepository;

    @Autowired
    UserTechnicalToolSkillSetRepository userTechnicalToolSkillSetRepository;

    @Autowired
    UserPillarRepository userPillarRepository;

    @Autowired
    KPIRepository kPIRepository;

    @Autowired
    ProjectKIPRepository projectKIPRepository;

    @Autowired
    MachineRepository machineRepository;

    @Autowired
    ETURepository etuRepository;

    @Autowired
    ProcessRepository processRepository;

    public MessageVM getAllMatricesData() {
        MessageVM messageVM = new MessageVM();
        List<Project> projectList = projectRepository.findAll();
        MatricesVM matricesVM = new MatricesVM();

        matricesVM.setPillarColumns(getUniquePillars());

        //start Unique Methods And Tools List Find Out
        HashMap<String, HashMap<String, String>> methodToolsCol = getUniqueMethodsTool();
        matricesVM.setMethodColumns(methodToolsCol.get("methodColumns"));
        matricesVM.setToolColumns(methodToolsCol.get("toolColumns"));
        //start Unique Methods And Tools List Find Out

        if (!projectList.isEmpty()) {
            List<HashMap<String, Object>> matricesProjectList = new ArrayList<>();
            int maxMembers = 1;
            for (Project project : projectList) {
                HashMap<String, Object> projectData = new HashMap<>();
                projectData.put("projectId", "" + project.getId());
                projectData.put("projectName", project.getProjectName());
                projectData.put("yearlyPotential", "" + project.getYearlyPotential());
                projectData.put("projectUIN", project.getProjectCode());

                ProjectSupportingPillars basePillar = supportingPillarsRepository.findOneByProjectAndPrimary(project, true);
                if (basePillar != null && basePillar.getPillar() != null) {
                    projectData.put("basePillar", basePillar.getPillar().getCode());
                }
                List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProject(project);

                List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
                for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                    if (projectMethodsAndTools.getMethodsAndTools() != null) {
                        MethodsAndTools methodsAndTools = projectMethodsAndTools.getMethodsAndTools();
                        if (methodsAndTools.getType() != null && methodsAndTools.getType().endsWith("METHODS")) {
                            projectData.put("method_" + methodsAndTools.getId(), "" + projectMethodsAndTools.getRequiredSkill());
                        }
                        if (methodsAndTools.getType() != null && methodsAndTools.getType().endsWith("TOOLS")) {
                            projectData.put("tool_" + methodsAndTools.getId(), "" + projectMethodsAndTools.getRequiredSkill());
                        }
                    }
                }

                int memberCount = 1;
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember != null && projectTeamMember.getUser() != null) {
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 1) {
                            projectData.put("personResponsible", projectTeamMember.getUser().getName());
                        }
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 2) {
                            projectData.put("coach", projectTeamMember.getUser().getName());
                        }
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 3) {
                            projectData.put("team_member_" + memberCount, projectTeamMember.getUser().getName());
                            memberCount++;
                        }
                    }
                }

                if (memberCount > maxMembers) {
                    maxMembers = memberCount;
                }

                ProjectBenefitIndentsEmatrix benefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
                if (benefitIndentsEmatrix != null) {
                    projectData.put("startDate", benefitIndentsEmatrix.getStartDate());
                    projectData.put("targetDate", benefitIndentsEmatrix.getTargetDate());

                    projectData.put("totalProjectCost", benefitIndentsEmatrix.getTotalProjectCost());
                    projectData.put("confidenceLevel", benefitIndentsEmatrix.getConfidenceLevel());
                    projectData.put("bc", benefitIndentsEmatrix.getBc());

                    projectData.put("pPlannedStartDate", benefitIndentsEmatrix.getpPlannedStartDate());
                    projectData.put("pPlannedEndDate", benefitIndentsEmatrix.getpPlannedEndDate());

                    projectData.put("dPlannedStartDate", benefitIndentsEmatrix.getdPlannedStartDate());
                    projectData.put("dPlannedEndDate", benefitIndentsEmatrix.getdPlannedEndDate());

                    projectData.put("cPlannedStartDate", benefitIndentsEmatrix.getcPlannedStartDate());
                    projectData.put("cPlannedEndDate", benefitIndentsEmatrix.getcPlannedEndDate());

                    projectData.put("pActualStartDate", benefitIndentsEmatrix.getpActualStartDate());
                    projectData.put("pActualEndDate", benefitIndentsEmatrix.getpActualEndDate());

                    projectData.put("dActualStartDate", benefitIndentsEmatrix.getdActualStartDate());
                    projectData.put("dActualEndDate", benefitIndentsEmatrix.getdActualEndDate());

                    projectData.put("cActualStartDate", benefitIndentsEmatrix.getcActualStartDate());
                    projectData.put("cActualEndDate", benefitIndentsEmatrix.getcActualEndDate());

                    projectData.put("aActualStartDate", benefitIndentsEmatrix.getaActualStartDate());
                    projectData.put("aActualEndDate", benefitIndentsEmatrix.getaActualEndDate());

                    projectData.putAll(getProjectBenifiteData(project));

                }

                if (project.getHorizontalExpansion() != null && project.getHorizontalExpansion()) {
                    projectData.put("horizontalExpansion", "Yes");
                } else {
                    projectData.put("horizontalExpansion", "No");
                }

//                if (project.getProcess() != null) {
//                    projectData.put("process", project.getProcess().getProcessName());
//                }
                if (project.getOptMachine() != null) {
                    projectData.put("operationNo", project.getOptMachine().getMachineName());
                }
                if (project.getEtu() != null) {
                    projectData.put("etu", project.getEtu().getEtuNo());
                    projectData.put("process", project.getEtu().getEtuName());
                }
                if (project.getLossType() != null) {
                    projectData.put("loss", project.getLossType().getLoss());
                }

                //projectData.putAll(getProjectBenifiteData(project));
                matricesProjectList.add(projectData);
            }

            matricesVM.setTotalTeamMemberCount(maxMembers);
            matricesVM.setTotalTeamMemberCount(maxMembers);
            matricesVM.setProjectList(matricesProjectList);
            messageVM.setData(matricesVM);
        }
        return messageVM;
    }

    public MessageVM getDMatricesData(MatrixFilterVM matrixFilterVM) {
        MessageVM messageVM = new MessageVM();
        List<Project> projectList = new ArrayList<>();
        if (matrixFilterVM.getEtuList().isEmpty() || matrixFilterVM.getEtuList().size() < 1) {
            projectList = projectRepository.findAllByOrderByCreatedAtDescForD();
        } else {
            List<Long> etuIds = matrixFilterVM.getEtuList().stream().map(mapper -> mapper.getId()).collect(Collectors.toList());
            projectList = projectRepository.findAllByOrderByCreatedAtDescForDWhereETUIn(etuIds);
        }
        DMatricesVM dMatricesVM = new DMatricesVM();

        dMatricesVM.setPillarColumns(getUniquePillars());

        //start Unique Methods And Tools List Find Out
        HashMap<String, HashMap<String, String>> methodToolsCol = getUniqueMethodsTool();
        dMatricesVM.setMethodColumns(methodToolsCol.get("methodColumns"));
        dMatricesVM.setToolColumns(methodToolsCol.get("toolColumns"));
        //start Unique Methods And Tools List Find Out
        dMatricesVM.setKipColumns(getProjectkPIsHeader());
        int maxMembers = 0;
        if (!projectList.isEmpty()) {
            List<HashMap<String, String>> dMatricesProjectList = new ArrayList<>();
            for (Project project : projectList) {
//            	if( matrixFilterVM.getIsEmployee().equals(null)){
//            	}
                if (matrixFilterVM.getIsEmployee().equals("ADMIN") && project.isProjectHide()) {
                    continue;
                }

                HashMap<String, String> projectData = new HashMap<>();
                projectData.put("projectId", "" + project.getId());
                projectData.put("projectName", project.getProjectName());
                projectData.put("yearlyPotential", "" + project.getYearlyPotential());
                projectData.put("projectUIN", project.getProjectCode());
                projectData.put("kpi", project.getProjectCode());

                List<ProjectSupportingPillars> projectSupportingPillars = supportingPillarsRepository.findByProject(project);
                for (ProjectSupportingPillars supportingPillars : projectSupportingPillars) {
                    if (supportingPillars != null && supportingPillars.getPillar() != null && supportingPillars.isPrimary()) {
                        projectData.put("basePillar", supportingPillars.getPillar().getCode());
                    } else if (supportingPillars.getPillar() != null) {
                        projectData.put("pillar_" + supportingPillars.getPillar().getId(), "X");
                    }
                }

                List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
                for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                    if (projectMethodsAndTools.getMethodsAndTools() != null) {
                        MethodsAndTools methodsAndTools = projectMethodsAndTools.getMethodsAndTools();
                        if (methodsAndTools.getType() != null && methodsAndTools.getType().endsWith("METHODS")) {
                            projectData.put("method_" + methodsAndTools.getId(), "X");
                        }
                        if (methodsAndTools.getType() != null && methodsAndTools.getType().endsWith("TOOLS")) {
                            projectData.put("tool_" + methodsAndTools.getId(), "X");
                        }
                    }
                }

                List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProject(project);

                int memberCount = 1;
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember != null && projectTeamMember.getUser() != null) {
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 1) {
                            projectData.put("personResponsible", projectTeamMember.getUser().getName());
                        }
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 2) {
                            projectData.put("personCoach", projectTeamMember.getUser().getName());
                        }
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 3) {
                            projectData.put("team_member_" + memberCount, projectTeamMember.getUser().getName());
                            memberCount++;
                        }
                    }
                }
                if (memberCount > maxMembers) {
                    maxMembers = memberCount;
                }

                if (project.getOptMachine() != null) {
                    projectData.put("operationNo", project.getOptMachine().getMachineName());
                }
                if (project.getEtu() != null) {
                    projectData.put("etu", project.getEtu().getEtuNo());
                    projectData.put("process", project.getEtu().getEtuName());
                }
                if (project.getLossType() != null) {
                    projectData.put("loss", project.getLossType().getLoss());
                }
                dMatricesProjectList.add(projectData);

                List<ProjectKIP> projectKIPs = projectKIPRepository.findAllByProject(project);
                projectKIPs.stream().filter((projectKIP) -> (projectKIP.getKpi() != null)).forEachOrdered((projectKIP) -> {
                    projectData.put("KPI_" + projectKIP.getKpi().getId(), "X");
                });
            }
            dMatricesVM.setTotalTeamMemberCount(maxMembers);
            dMatricesVM.setProjectList(dMatricesProjectList);
            messageVM.setData(dMatricesVM);
        }
        return messageVM;
    }

    public Project getProjectStatus(Project project) {

        Project projectEntity = projectRepository.findOne(project.getId());
        projectEntity.setProjectHide(!projectEntity.isProjectHide());

        projectRepository.save(projectEntity);

        return projectEntity;
    }

    public MessageVM getEMatricesData(MatrixFilterVM matrixFilterVM) {
        DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
        DateFormat excelDateFormater = new SimpleDateFormat("dd.MM.yyyy");

        Calendar reportStartDate = Calendar.getInstance();
        reportStartDate.set(Calendar.DATE, 1);
        reportStartDate.set(Calendar.YEAR, matrixFilterVM.getStartOfYear());
        reportStartDate.set(Calendar.MONTH, matrixFilterVM.getStartOfMonth() - 1);

        Calendar reportEndDate = Calendar.getInstance();
        reportEndDate.set(Calendar.DATE, 31);
        reportEndDate.set(Calendar.MONTH, matrixFilterVM.getEndOfMonth() - 1);
        reportEndDate.set(Calendar.YEAR, matrixFilterVM.getEndOfYear());

        MessageVM messageVM = new MessageVM();
        List<Project> projectList = new ArrayList<>();
        if (matrixFilterVM.getEtuList().isEmpty() || matrixFilterVM.getEtuList().size() < 1) {
            projectList = projectRepository.findProjectUsingStartDate(formater.format(reportStartDate.getTime()), formater.format(reportEndDate.getTime()));
        } else {
            List<Long> etuIds = matrixFilterVM.getEtuList().stream().map(mapper -> mapper.getId()).collect(Collectors.toList());
            projectList = projectRepository.findProjectUsingStartDateAndEtu(formater.format(reportStartDate.getTime()), formater.format(reportEndDate.getTime()), etuIds);
        }
        EMatricesVM eMatricesVM = new EMatricesVM();

        if (!projectList.isEmpty()) {
            List<HashMap<String, Object>> eMatricesProjectList = new ArrayList<>();
            int maxMembers = 1;
            for (Project project : projectList) {
                if ((matrixFilterVM.getIsEmployee().equals("ADMIN") || matrixFilterVM.getIsEmployee().equals("SUPER_EMPLOYEE")) && project.isProjectHide()) {
                    continue;
                }
                HashMap<String, Object> projectData = new HashMap<>();
                projectData.put("projectId", project.getId());
                projectData.put("projectName", project.getProjectName());
                projectData.put("yearlyPotential", project.getYearlyPotential());
                projectData.put("projectUIN", project.getProjectCode());

                projectData.put("direct", project.getDirectLoss());
                projectData.put("indirect", project.getIndirectLoss());
                projectData.put("staff", project.getStaffLoss());
                projectData.put("indirectMaterials", project.getIndirectMaterialsLoss());
                projectData.put("maintenance", project.getMaintenanceLoss());
                projectData.put("scrap", project.getScrapLoss());
                projectData.put("energy", project.getEnergyLoss());
                projectData.put("generalExpenses", project.getExpensessLoss());
                projectData.put("isAdminHide", project.isProjectHide());

                if (project.getCompletionDate() != null) {
                    projectData.put("projectCloserDate", excelDateFormater.format(project.getCompletionDate()));
                }

                projectData.put("horizontalExpansion", project.getHorizontalExpansion() != null && project.getHorizontalExpansion() ? "Yes" : "No");
                projectData.put("standardisation", project.getStandardisation() != null && project.getStandardisation() ? "Yes" : "No");
                projectData.put("epmInfo", project.getEpmInfo() != null && project.getEpmInfo() ? "Yes" : "No");
                projectData.put("executionStarted", project.getExecutionStarted());
                projectData.put("remark", project.getRemark());
                projectData.put("standardisationRemark", project.getStandardisationRemark());
                projectData.put("epmInfoRemark", project.getEpmInfoRemark());

                ProjectSupportingPillars basePillar = supportingPillarsRepository.findOneByProjectAndPrimary(project, true);
                if (basePillar != null && basePillar.getPillar() != null) {
                    projectData.put("basePillar", basePillar.getPillar().getCode());
                }
                List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProject(project);

                int memberCount = 1;
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember != null && projectTeamMember.getUser() != null) {
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 1) {
                            projectData.put("personResponsible", projectTeamMember.getUser().getName());
                        }
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 2) {
                            projectData.put("personCoach", projectTeamMember.getUser().getName());
                        }
                        if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 3) {
                            projectData.put("team_member_" + memberCount, projectTeamMember.getUser().getName());
                            memberCount++;
                        }
                    }
                }
                if (memberCount > maxMembers) {
                    maxMembers = memberCount;
                }

                projectData.putAll(getProjectBenifiteData(project));
                if (project.getOptMachine() != null) {
                    projectData.put("operationNo", project.getOptMachine().getMachineName());
                }
                if (project.getEtu() != null) {
                    projectData.put("etu", project.getEtu().getEtuNo());
                    projectData.put("process", project.getEtu().getEtuName());
                }
                if (project.getLossType() != null) {
                    projectData.put("loss", project.getLossType().getLoss());
                }

                ProjectBenefitIndentsEmatrix benefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
                if (benefitIndentsEmatrix != null) {
                    Calendar cal = Calendar.getInstance();
                    projectData.put("startDate", benefitIndentsEmatrix.getStartDate());
                    if (Objects.nonNull(benefitIndentsEmatrix.getStartDate())) {
                        try {
                            cal.setTime(formater.parse(benefitIndentsEmatrix.getStartDate()));
                        } catch (ParseException ex) {

                        }
                        projectData.put("startDateFormated", excelDateFormater.format(cal.getTime()));
                    } else {
                        projectData.put("startDateFormated", "");
                    }
                    if (Objects.nonNull(benefitIndentsEmatrix.getTargetDate())) {
                        try {
                            cal.setTime(formater.parse(benefitIndentsEmatrix.getTargetDate()));
                        } catch (ParseException ex) {

                        }
                        projectData.put("targetDateFormated", excelDateFormater.format(cal.getTime()));

                    } else {
                        projectData.put("targetDateFormated", "");
                    }
                    projectData.put("targetDate", benefitIndentsEmatrix.getTargetDate());

                    projectData.put("totalProjectCost", benefitIndentsEmatrix.getTotalProjectCost());
                    projectData.put("confidenceLevel", benefitIndentsEmatrix.getConfidenceLevel());
                    projectData.put("bc", benefitIndentsEmatrix.getBc());

                    projectData.put("benefitIndentsEmatrix", benefitIndentsEmatrix);

                    projectData.putAll(getPlanMonthDate("PLAN", "P", benefitIndentsEmatrix.getpPlannedStartDate(), benefitIndentsEmatrix.getpPlannedEndDate()));
                    projectData.putAll(getPlanMonthDate("PLAN", "D", benefitIndentsEmatrix.getdPlannedStartDate(), benefitIndentsEmatrix.getdPlannedEndDate()));
                    projectData.putAll(getPlanMonthDate("PLAN", "C", benefitIndentsEmatrix.getcPlannedStartDate(), benefitIndentsEmatrix.getcPlannedEndDate()));
                    projectData.putAll(getPlanMonthDate("PLAN", "A", benefitIndentsEmatrix.getaPlannedStartDate(), benefitIndentsEmatrix.getaPlannedEndDate()));
                    projectData.putAll(getPlanMonthDate("ACTUAL", "P", benefitIndentsEmatrix.getpActualStartDate(), benefitIndentsEmatrix.getpActualEndDate()));
                    projectData.putAll(getPlanMonthDate("ACTUAL", "D", benefitIndentsEmatrix.getdActualStartDate(), benefitIndentsEmatrix.getdActualEndDate()));
                    projectData.putAll(getPlanMonthDate("ACTUAL", "C", benefitIndentsEmatrix.getcActualStartDate(), benefitIndentsEmatrix.getcActualEndDate()));
                    projectData.putAll(getPlanMonthDate("ACTUAL", "A", benefitIndentsEmatrix.getaActualStartDate(), benefitIndentsEmatrix.getaActualEndDate()));

                    projectData.putAll(getProjectBenifiteData(project));

                    projectData.put("remarkList", projectPlanRemarksRepository.findAllByProject(project));
                }

                eMatricesProjectList.add(projectData);
            }

            eMatricesVM.setTotalTeamMemberCount(maxMembers);
            eMatricesVM.setMembersColumns(getMembersColumns(maxMembers));
            eMatricesVM.setProjectList(eMatricesProjectList);
            messageVM.setMessage("Project list for E Matrices.");
            messageVM.setData(eMatricesVM);
        } else {
            messageVM.setMessage("Project not present with this criteria for E Matrix.");
            eMatricesVM.setProjectList(new ArrayList<>());
            messageVM.setData(eMatricesVM);
        }
        return messageVM;
    }

    public MessageVM getPillarSavings(Integer factorYear) {
        MessageVM messageVM = new MessageVM();

        List<Object[]> projectFMatrixSavingsList = projectFMatrixSavingsRepository.getPillarSavings(factorYear);
        List<Pillar> pillarList1 = pillarRepository.findAll();
        List<Pillar> pillarList = pillarList1.stream().sorted(Comparator.comparing(Pillar::getId)).collect(Collectors.toList());
        Set<PillarSAvingsVM> pillarSAvingsVMList = new HashSet<PillarSAvingsVM>();
        //	Set<PillarSAvingsVM> pillarSAvingsVMList = new TreeSet<PillarSAvingsVM>();
        PillarSAvingsVM pillarSAvingsVM = null;

        //pillarList.sort(Comparator.comparing((Pillar::getId())).reversed());
        for (Pillar p : pillarList) {
            List<Long> projectIdsbyPillar = supportingPillarsRepository.getProjectIdsFromPillarId(p.getId());
            boolean notFound = true;
            if (!projectIdsbyPillar.isEmpty()) {
                BigInteger projectFMatrixActualSaving = projectFMatrixSavingsRepository.getSumOfActualSavingsByProjectIds(projectIdsbyPillar);

                for (int i = 0; i < projectFMatrixSavingsList.size(); i++) {
                    Object[] objectData = projectFMatrixSavingsList.get(i);
                    BigInteger pillarId = (BigInteger) objectData[0];
                    if (p.getId().equals(pillarId.longValue())) {
                        BigDecimal actuals = (BigDecimal) objectData[1];
                        BigInteger actualSavings = actuals.toBigInteger();
                        notFound = false;
                        pillarSAvingsVM = new PillarSAvingsVM(pillarId, actualSavings, p.getCode());
                    }

                }
            }

            if (notFound) {
                pillarSAvingsVMList.add(new PillarSAvingsVM(BigInteger.valueOf(p.getId().intValue()), BigInteger.ZERO, p.getCode()));
            } else {
                pillarSAvingsVMList.add(pillarSAvingsVM);
            }
            pillarSAvingsVMList.add(pillarSAvingsVM);
            messageVM.setData(pillarSAvingsVMList);
        }
        return messageVM;
    }

    public MessageVM getETUSavings(Integer factorYear) {
        MessageVM messageVM = new MessageVM();
        List<Object[]> projectFMatrixSavingsList = projectFMatrixSavingsRepository.getETUSavings(factorYear);
        List<ETU> etulists = etuRepository.findAll();
        List<ETU> etulist = etulists.stream().sorted(Comparator.comparing(ETU::getId)).collect(Collectors.toList());
        Set<EtuSavingsVM> EtuSavingsVMList = new HashSet<EtuSavingsVM>();
        EtuSavingsVM etuSavingsVM = null;
        BigInteger actualSavings = null;
        for (ETU etu : etulist) {
            boolean notFound = false;
            for (int i = 0; i <= projectFMatrixSavingsList.size() - 1; i++) {
                Object[] objectData = projectFMatrixSavingsList.get(i);
                BigInteger etuId = (BigInteger) objectData[0];
                if (Objects.equals(etu.getId(), etuId.longValue())) {
                    BigDecimal actuals = (BigDecimal) objectData[1];
                    actualSavings = actuals.toBigInteger();
                    etuSavingsVM = new EtuSavingsVM(etu.getId(), actualSavings, etu.getProjectAbbrevation());
                    EtuSavingsVMList.add(etuSavingsVM);
                } else {
                    notFound = true;
                }
            }
            if (notFound) {
                double z = 000.000;
                actualSavings = BigInteger.valueOf((long) z);
                etuSavingsVM = new EtuSavingsVM(etu.getId(), actualSavings, etu.getProjectAbbrevation());
                EtuSavingsVMList.add(etuSavingsVM);
            }
        }

        messageVM.setData(EtuSavingsVMList);

        return messageVM;
    }

    public MessageVM getFMatricesData(MatrixFilterVM matrixFilterVM) {
        MessageVM messageVM = new MessageVM();

        MatricesVM matricesVM = new MatricesVM();
        DateFormat formater = new SimpleDateFormat("MMM_yy");
        DateFormat formater2 = new SimpleDateFormat("MM-dd-yyyy");
        DateFormat excelDateFormater = new SimpleDateFormat("MM.dd.yyyy");
        Calendar reportStartDate = Calendar.getInstance();
        reportStartDate.set(Calendar.DATE, 01);
        if (matrixFilterVM.getStartOfMonth() != null) {
            reportStartDate.set(Calendar.MONTH, matrixFilterVM.getStartOfMonth() - 1);
        }

        if (matrixFilterVM.getStartOfYear() != null) {
            reportStartDate.set(Calendar.YEAR, matrixFilterVM.getStartOfYear());
        }

        Calendar reportEndDate = Calendar.getInstance();
        reportEndDate.set(Calendar.DATE, 31);
        if (matrixFilterVM.getEndOfMonth() != null) {
            reportEndDate.set(Calendar.MONTH, matrixFilterVM.getEndOfMonth() - 1);
        }
        if (matrixFilterVM.getEndOfYear() != null) {
            reportEndDate.set(Calendar.YEAR, matrixFilterVM.getEndOfYear());
        }
        List<Project> projectList = new ArrayList<>();
        if (matrixFilterVM.getEtuList().isEmpty() || matrixFilterVM.getEtuList().size() < 1) {
            projectList = projectRepository.findProjectByProjectEndDated(formater2.format(reportStartDate.getTime()), formater2.format(reportEndDate.getTime()));
        } else {
            List<Long> etuIds = matrixFilterVM.getEtuList().stream().map(mapper -> mapper.getId()).collect(Collectors.toList());
            projectList = projectRepository.findProjectByProjectEndDated(formater2.format(reportStartDate.getTime()), formater2.format(reportEndDate.getTime()), etuIds);

        }

        if (!projectList.isEmpty()) {
            List<HashMap<String, Object>> matricesProjectList = new ArrayList<>();
            for (Project project : projectList) {
                if (Objects.nonNull(matrixFilterVM.getIsEmployee())) {

                    if ((matrixFilterVM.getIsEmployee().equals("ADMIN") || matrixFilterVM.getIsEmployee().equals("SUPER_EMPLOYEE")) && project.isProjectHide()) {
                        continue;
                    }
                    HashMap<String, Object> projectData = new HashMap<>();
                    Calendar projectStartDate = Calendar.getInstance();
                    projectData.put("projectId", project.getId());
                    projectData.put("projectName", project.getProjectName());
                    projectData.put("yearlyPotential", project.getYearlyPotential());
                    projectData.put("projectUIN", project.getProjectCode());

                    ProjectSupportingPillars basePillar = supportingPillarsRepository.findOneByProjectAndPrimary(project, true);
                    if (basePillar != null && basePillar.getPillar() != null) {
                        projectData.put("basePillar", basePillar.getPillar().getCode());
                    }

                    if (project.getEtu() != null) {
                        projectData.put("etu", project.getEtu().getEtuNo());
                        projectData.put("process", project.getEtu().getEtuName());
                    }
                    if (project.getLossType() != null) {
                        projectData.put("loss", project.getLossType().getLoss());
                    }

                    if (project.getOptMachine() != null) {
                        projectData.put("operationNo", project.getOptMachine().getMachineName());
                    }

                    ProjectBenefitIndentsEmatrix benefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
                    if (benefitIndentsEmatrix != null) {
                        try {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(formater2.parse(benefitIndentsEmatrix.getStartDate()));
                            projectStartDate.setTime(formater2.parse(benefitIndentsEmatrix.getStartDate()));
                            projectData.put("startDate", excelDateFormater.format(cal.getTime()));
                            cal.setTime(formater2.parse(benefitIndentsEmatrix.getTargetDate()));
                            projectData.put("targetDate", excelDateFormater.format(cal.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    List<ProjectFMatrixSavings> projectFMatrixSavingsList = projectFMatrixSavingsRepository.findByProject(project);
                    Calendar savingDate = Calendar.getInstance();
                    Double totalSaving = 0d;
                    Double attSaving = 0d;
                    Calendar upToReportSavingDate = Calendar.getInstance();
                    if (upToReportSavingDate.getTime().equals(reportEndDate.getTime()) || upToReportSavingDate.getTime().after(reportEndDate.getTime())) {
                        upToReportSavingDate = reportEndDate;
                    }

                    if (!projectFMatrixSavingsList.isEmpty()) {
                        for (ProjectFMatrixSavings projectFMatrixSavings : projectFMatrixSavingsList) {
                            if (projectFMatrixSavings != null && projectFMatrixSavings.getMonth() != null && projectFMatrixSavings.getYear() != null) {
                                savingDate.set(Calendar.MONTH, projectFMatrixSavings.getMonth() - 1);
                                savingDate.set(Calendar.YEAR, projectFMatrixSavings.getYear());
                                if ((savingDate.getTime().equals(projectStartDate.getTime()) || savingDate.getTime().after(projectStartDate.getTime()))
                                        && (savingDate.getTime().equals(reportStartDate.getTime()) || savingDate.getTime().after(reportStartDate.getTime()))
                                        && (savingDate.getTime().equals(upToReportSavingDate.getTime()) || savingDate.getTime().before(upToReportSavingDate.getTime()))) {
                                    String date = formater.format(savingDate.getTime()).toUpperCase();
                                    projectData.put("SAVING_ATT_" + date, projectFMatrixSavings.getProjectedSavingValue());
                                    projectData.put("SAVING_ACT_" + date, projectFMatrixSavings.getActualSavingValue());
                                    totalSaving += projectFMatrixSavings.getActualSavingValue();
                                    attSaving += projectFMatrixSavings.getProjectedSavingValue();
                                }
                            }
                        }
                        projectData.put("totalACTSaving", totalSaving);
                        projectData.put("totalATTSaving", attSaving);

                    }

                    matricesProjectList.add(projectData);
                }
            }
            matricesVM.setProjectList(matricesProjectList);
            messageVM.setData(matricesVM);
        } else {
            matricesVM.setProjectList(new ArrayList<>());
            messageVM.setData(matricesVM);
        }
        return messageVM;
    }

    public HashMap<String, String> getPlanMonthDate(String type, String action, String startDate, String endDate) {
        HashMap<String, String> plan = new HashMap<>();
        DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
        DateFormat formater2 = new SimpleDateFormat("dd_MMM_yy");
        if (startDate != null && endDate != null) {
            Calendar beginCalendar = Calendar.getInstance();
            Calendar finishCalendar = Calendar.getInstance();
            try {
                beginCalendar.setTime(formater.parse(startDate));
                finishCalendar.setTime(formater.parse(endDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            while (beginCalendar.before(finishCalendar) || beginCalendar.equals(finishCalendar)) {
                String date = formater2.format(beginCalendar.getTime()).toUpperCase();
                beginCalendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
                plan.put(type + "_" + date, action);
                beginCalendar.add(Calendar.DATE, 7);
            }
        }

        return plan;
    }

    public HashMap<String, String> getMembersColumns(int count) {
        HashMap<String, String> membersColumns = new HashMap<>();
        for (int i = 0; i <= count; i++) {
            membersColumns.put("team_member_" + i, "Team Member(" + (i + 1) + ")");
        }
        return membersColumns;
    }

    public HashMap<String, String> getUniquePillars() {
        List<Long> uniquePillarList = supportingPillarsRepository.getUniquePillarList();
        List<Pillar> pillarList = new ArrayList<>();
        if (!uniquePillarList.isEmpty()) {
            pillarList = pillarRepository.findAll(uniquePillarList);
        }
        HashMap<String, String> pillarMap = new HashMap<>();
        pillarList.forEach(pillar -> pillarMap.put("pillar_" + pillar.getId(), pillar.getCode()));
        return pillarMap;
    }

    public HashMap<String, HashMap<String, String>> getUniqueMethodsTool() {
        HashMap<String, HashMap<String, String>> res = new HashMap<>();
        List<String> uniqueMethodToolList = projectMethodsAndToolsRepository.getUniqueMethodsAndToolList();
        List<MethodsAndTools> methodAndToolList = new ArrayList<>();
        if (!uniqueMethodToolList.isEmpty()) {
            methodAndToolList = methodsAndToolsRepository.findAll(uniqueMethodToolList);
        }

        HashMap<String, String> methodColumns = new HashMap<>();
        HashMap<String, String> toolColumns = new HashMap<>();
        for (MethodsAndTools methodsAndTools : methodAndToolList) {
            if (methodsAndTools.getType() != null && methodsAndTools.getType().endsWith("METHODS")) {
                if (methodsAndTools.getLevelSecondTool() != null) {
                    methodColumns.put("method_" + methodsAndTools.getId(), methodsAndTools.getLevelFirstTool() + " - " + methodsAndTools.getLevelSecondTool());
                } else {
                    methodColumns.put("method_" + methodsAndTools.getId(), methodsAndTools.getLevelFirstTool());
                }
            }

            if (methodsAndTools.getType() != null && methodsAndTools.getType().endsWith("TOOLS")) {
                if (methodsAndTools.getLevelSecondTool() != null) {
                    toolColumns.put("tool_" + methodsAndTools.getId(), methodsAndTools.getLevelFirstTool() + " - " + methodsAndTools.getLevelSecondTool());
                } else {
                    toolColumns.put("tool_" + methodsAndTools.getId(), methodsAndTools.getLevelFirstTool());
                }
            }

        }

        res.put("methodColumns", methodColumns);
        res.put("toolColumns", toolColumns);

        return res;
    }

    public HashMap<String, Object> getProjectBenifiteData(Project project) {
        HashMap<String, Object> projectData = new HashMap<>();
        List<ProjectBenefitIndents> projectBenefitIndentsList = projectBenefitIndentsRepository.findByProject(project);
        for (ProjectBenefitIndents projectBenefitIndents : projectBenefitIndentsList) {

            if (projectBenefitIndents.getBenifit() != null && projectBenefitIndents.getBenifit().getBenifitName() != null) {
                String benifitName = projectBenefitIndents.getBenifit().getBenifitName();
                switch (benifitName) {
                    case "DIRECT LABOUR COST":
                        if (projectBenefitIndents.getBenifitValue() != null && projectBenefitIndents.getBenifitValue() != 0) {
                            projectData.put("direct_cost", projectBenefitIndents.getBenifitValue());
                        }
                        break;
                    case "INDIRECT LABOUR COST":
                        if (projectBenefitIndents.getBenifitValue() != null && projectBenefitIndents.getBenifitValue() != 0) {
                            projectData.put("indirect_cost", projectBenefitIndents.getBenifitValue());
                        }
                        break;
                    case "STAFF SALARY COST":
                        if (projectBenefitIndents.getBenifitValue() != null && projectBenefitIndents.getBenifitValue() != 0) {
                            projectData.put("staff_cost", projectBenefitIndents.getBenifitValue());
                        }
                        break;
                    case "INDIRECT MATERIALS COST":
                        if (projectBenefitIndents.getBenifitValue() != null && projectBenefitIndents.getBenifitValue() != 0) {
                            projectData.put("indirectMaterials_cost", projectBenefitIndents.getBenifitValue());
                        }
                        break;
                    case "MAINTENANCE SPARES COST":
                        if (projectBenefitIndents.getBenifitValue() != null && projectBenefitIndents.getBenifitValue() != 0) {
                            projectData.put("maintenance_cost", projectBenefitIndents.getBenifitValue());
                        }
                        break;
                    case "SCRAP":
                        if (projectBenefitIndents.getBenifitValue() != null && projectBenefitIndents.getBenifitValue() != 0) {
                            projectData.put("scrap_cost", projectBenefitIndents.getBenifitValue());
                        }
                        break;
                    case "ENERGY COST":
                        if (projectBenefitIndents.getBenifitValue() != null && projectBenefitIndents.getBenifitValue() != 0) {
                            projectData.put("energy_cost", projectBenefitIndents.getBenifitValue());
                        }
                        break;
                    case "GENERAL EXPENSES COST":
                        if (projectBenefitIndents.getBenifitValue() != null && projectBenefitIndents.getBenifitValue() != 0) {
                            projectData.put("generalExpenses_cost", projectBenefitIndents.getBenifitValue());
                        }
                        break;

                }
            }
        }
        return projectData;
    }

    public MessageVM getDEFMatricesData(MatrixFilterVM matrixFilterVM) {
        MessageVM messageVM = new MessageVM();
        List<Project> projectList = new ArrayList<>();
        List<Long> etuIds = new ArrayList<>();
        if (Objects.nonNull(matrixFilterVM.getEtuList()) && matrixFilterVM.getEtuList().size() > 0) {
            etuIds = matrixFilterVM.getEtuList().stream().map(mapper -> mapper.getId()).collect(Collectors.toList());
        }
        boolean etuIdsEmpty = etuIds.isEmpty() || etuIds.size() < 1;
        if (matrixFilterVM.getCurrentWorkingState() != null && matrixFilterVM.getCurrentWorkingState().equalsIgnoreCase("p")) {
            projectList = etuIdsEmpty ? projectRepository.findAllByStatePOrderByCreatedAtDesc() : projectRepository.findAllByStatePOrderByCreatedAtDesc(etuIds);
        } else if (matrixFilterVM.getCurrentWorkingState() != null && matrixFilterVM.getCurrentWorkingState().equalsIgnoreCase("d")) {
            projectList = etuIdsEmpty ? projectRepository.findAllByStateDOrderByCreatedAtDesc() : projectRepository.findAllByStateDOrderByCreatedAtDesc(etuIds);
        } else if (matrixFilterVM.getCurrentWorkingState() != null && matrixFilterVM.getCurrentWorkingState().equalsIgnoreCase("c")) {
            projectList = etuIdsEmpty ? projectRepository.findAllByStateCOrderByCreatedAtDesc() : projectRepository.findAllByStateCOrderByCreatedAtDesc(etuIds);
        } else if (matrixFilterVM.getCurrentWorkingState() != null && matrixFilterVM.getCurrentWorkingState().equalsIgnoreCase("a")) {
            projectList = etuIdsEmpty ? projectRepository.findAllByStateAOrderByCreatedAtDesc() : projectRepository.findAllByStateAOrderByCreatedAtDesc(etuIds);
        } else {
            projectList = etuIdsEmpty ? projectRepository.findAllByOrderByCreatedAtDesc() : projectRepository.findAllByOrderByCreatedAtDesc(etuIds);
        }

        MatricesVM matricesVM = new MatricesVM();
        DateFormat formater = new SimpleDateFormat("MMM_yy");
        DateFormat formater2 = new SimpleDateFormat("MM-dd-yyyy");
        DateFormat excelDateFormater = new SimpleDateFormat("MM.dd.yyyy");
        DateFormat excelDateFormater2 = new SimpleDateFormat("dd.MM.yyyy");

        Calendar reportStartDate = Calendar.getInstance();
        reportStartDate.set(Calendar.DATE, 01);
        reportStartDate.set(Calendar.MONTH, 0);

        Calendar reportEndDate = Calendar.getInstance();
        reportEndDate.set(Calendar.DATE, 31);
        reportEndDate.set(Calendar.MONTH, 11);

        matricesVM.setPillarColumns(getUniquePillars());

        //start Unique Methods And Tools List Find Out
        HashMap<String, HashMap<String, String>> methodToolsCol = getUniqueMethodsTool();
        matricesVM.setMethodColumns(methodToolsCol.get("methodColumns"));
        matricesVM.setToolColumns(methodToolsCol.get("toolColumns"));
        //start Unique Methods And Tools List Find Out

        if (!projectList.isEmpty()) {
            List<HashMap<String, Object>> matricesProjectList = new ArrayList<>();
            int maxMembers = 1;
            for (Project project : projectList) {
                if ((matrixFilterVM.getIsEmployee().equals("ADMIN") || matrixFilterVM.getIsEmployee().equals("SUPER_EMPLOYEE")) && project.isProjectHide()) {
                    continue;
                }
                HashMap<String, Object> projectData = new HashMap<>();
                projectData.put("projectId", "" + project.getId());
                projectData.put("projectName", project.getProjectName());
                projectData.put("yearlyPotential", "" + project.getYearlyPotential());
                projectData.put("projectUIN", project.getProjectCode());
                projectData.put("preliminaryAnalysis", project.getPreliminaryAnalysis());
                projectData.put("executionStarted", project.getExecutionStarted());

                if (project.getCompletionDate() != null) {
                    projectData.put("projectCloserDate", excelDateFormater2.format(project.getCompletionDate()));
                }
                projectData.put("standardisation", project.getStandardisation() != null && project.getStandardisation() ? "Yes" : "No");
                projectData.put("epmInfo", project.getEpmInfo() != null && project.getEpmInfo() ? "Yes" : "No");
                projectData.put("kDocument", project.getDocument() != null ? project.getDocument() : "Document Not Available");
                projectData.put("ewoDocument", project.getEwDocument() != null ? project.getEwDocument() : "Document Not Available");
                projectData.put("remark", project.getRemark());
                projectData.put("standardisationRemark", project.getStandardisationRemark());
                projectData.put("epmInfoRemark", project.getEpmInfoRemark());

                Calendar projectStartDate = Calendar.getInstance();

                List<ProjectSupportingPillars> projectSupportingPillars = supportingPillarsRepository.findByProject(project);
                for (ProjectSupportingPillars supportingPillars : projectSupportingPillars) {
                    if (supportingPillars.getPillar() != null && supportingPillars.isPrimary()) {
                        projectData.put("basePillar", supportingPillars.getPillar().getCode());
                    }
                    if (supportingPillars.getPillar() != null && !supportingPillars.isPrimary()) {
                        projectData.put("pillar_" + supportingPillars.getPillar().getId(), "X");
                    }
                }

                List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProject(project);
                HashMap<Long, Long> toolListSkill = new HashMap<>();
                List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
                for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                    if (projectMethodsAndTools.getMethodsAndTools() != null) {
                        MethodsAndTools methodsAndTools = projectMethodsAndTools.getMethodsAndTools();
                        if (methodsAndTools.getType() != null && methodsAndTools.getType().endsWith("METHODS")) {
                            toolListSkill.put(methodsAndTools.getId(), 0l);
                            projectData.put("method_" + methodsAndTools.getId(), "" + projectMethodsAndTools.getRequiredSkill());
                        }
                        if (methodsAndTools.getType() != null && methodsAndTools.getType().endsWith("TOOLS")) {
                            toolListSkill.put(methodsAndTools.getId(), 0l);
                            projectData.put("tool_" + methodsAndTools.getId(), "" + projectMethodsAndTools.getRequiredSkill());
                        }
                    }
                }

                int memberCount = 1;
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember != null && projectTeamMember.getUser() != null && projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() != null) {
                        switch (projectTeamMember.getRole().getId()) {
                            case 1:
                                projectData.put("personResponsible", projectTeamMember.getUser().getName());
                                toolListSkill = getMaxUserProjectToolSkill(projectTeamMember.getUser(), project, projectMethodsAndToolsList, toolListSkill);
                                break;
                            case 2:
                                projectData.put("coach", projectTeamMember.getUser().getName());
                                toolListSkill = getMaxUserProjectToolSkill(projectTeamMember.getUser(), project, projectMethodsAndToolsList, toolListSkill);
                                break;
                            case 3:
                                projectData.put("team_member_" + memberCount, projectTeamMember.getUser().getName());
                                toolListSkill = getMaxUserProjectToolSkill(projectTeamMember.getUser(), project, projectMethodsAndToolsList, toolListSkill);
                                memberCount++;
                                break;
                            default:
                                break;
                        }
                    }
                }

                for (Map.Entry maxSkillMapItem : toolListSkill.entrySet()) {
                    if (projectData.containsKey("method_" + maxSkillMapItem.getKey())) {
                        projectData.replace("method_" + maxSkillMapItem.getKey(), maxSkillMapItem.getValue());
                    } else if (projectData.containsKey("tool_" + maxSkillMapItem.getKey())) {
                        projectData.replace("tool_" + maxSkillMapItem.getKey(), maxSkillMapItem.getValue());
                    }
                }

                if (memberCount > maxMembers) {
                    maxMembers = memberCount;
                }

                ProjectBenefitIndentsEmatrix benefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
                if (benefitIndentsEmatrix != null) {
                    try {
                        Calendar cal = Calendar.getInstance();
                        if (Objects.nonNull(benefitIndentsEmatrix.getStartDate())) {
                            cal.setTime(formater2.parse(benefitIndentsEmatrix.getStartDate()));
                            projectStartDate.setTime(formater2.parse(benefitIndentsEmatrix.getStartDate()));
                            projectData.put("startDate", excelDateFormater.format(cal.getTime()));
                            projectData.put("startDateFormated", excelDateFormater2.format(cal.getTime()));
                        } else {
                            projectData.put("startDate", null);
                            projectData.put("startDateFormated", null);
                        }
                        if (Objects.nonNull(benefitIndentsEmatrix.getTargetDate())) {
                            cal.setTime(formater2.parse(benefitIndentsEmatrix.getTargetDate()));
                            projectData.put("targetDate", excelDateFormater.format(cal.getTime()));
                            projectData.put("targetDateFormated", excelDateFormater2.format(cal.getTime()));
                        } else {
                            projectData.put("targetDate", null);
                            projectData.put("targetDateFormated", null);
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    projectData.put("benefitIndentsEmatrix", benefitIndentsEmatrix);
                    projectData.put("totalProjectCost", benefitIndentsEmatrix.getTotalProjectCost());
                    projectData.put("confidenceLevel", benefitIndentsEmatrix.getConfidenceLevel());
                    projectData.put("bc", benefitIndentsEmatrix.getBc());
                    if (benefitIndentsEmatrix.getSavingsType() != null) {
                        if (benefitIndentsEmatrix.getSavingsType().equals("Hard Savings(Rs)")) {
                            projectData.put("hardSavings", "X");
                        } else if (benefitIndentsEmatrix.getSavingsType().equals("Virtual Savings(Rs)")) {
                            projectData.put("virtualSavings", "X");
                        } else if (benefitIndentsEmatrix.getSavingsType().equals("Cost Avoidance(Rs)")) {
                            projectData.put("costAvoidance", "X");
                        } else if (benefitIndentsEmatrix.getSavingsType().equals("Soft Savings(Rs)")) {
                            projectData.put("softSavings", "X");
                        }
                    }
                    projectData.putAll(getPlanMonthDate("PLAN", "P", benefitIndentsEmatrix.getpPlannedStartDate(), benefitIndentsEmatrix.getpPlannedEndDate()));
                    projectData.putAll(getPlanMonthDate("PLAN", "D", benefitIndentsEmatrix.getdPlannedStartDate(), benefitIndentsEmatrix.getdPlannedEndDate()));
                    projectData.putAll(getPlanMonthDate("PLAN", "C", benefitIndentsEmatrix.getcPlannedStartDate(), benefitIndentsEmatrix.getcPlannedEndDate()));
                    projectData.putAll(getPlanMonthDate("PLAN", "A", benefitIndentsEmatrix.getaPlannedStartDate(), benefitIndentsEmatrix.getaPlannedEndDate()));
                    projectData.putAll(getPlanMonthDate("ACTUAL", "P", benefitIndentsEmatrix.getpActualStartDate(), benefitIndentsEmatrix.getpActualEndDate()));
                    projectData.putAll(getPlanMonthDate("ACTUAL", "D", benefitIndentsEmatrix.getdActualStartDate(), benefitIndentsEmatrix.getdActualEndDate()));
                    projectData.putAll(getPlanMonthDate("ACTUAL", "C", benefitIndentsEmatrix.getcActualStartDate(), benefitIndentsEmatrix.getcActualEndDate()));
                    projectData.putAll(getPlanMonthDate("ACTUAL", "A", benefitIndentsEmatrix.getaActualStartDate(), benefitIndentsEmatrix.getaActualEndDate()));
                    projectData.putAll(getProjectBenifiteData(project));
                }

                projectData.put("horizontalExpansion", project.getHorizontalExpansion() != null && project.getHorizontalExpansion() ? "Yes" : "No");
                projectData.put("projectProgress", project.getCompletionDate() == null ? "InComplete" : "Complete");

                if (project.getOptMachine() != null) {
                    projectData.put("operationNo", project.getOptMachine().getMachineName());
                }
                if (project.getEtu() != null) {
                    projectData.put("etu", project.getEtu().getEtuNo());
                    projectData.put("process", project.getEtu().getEtuName());
                }
                if (project.getLossType() != null) {
                    projectData.put("loss", project.getLossType().getLoss());
                }

                List<ProjectFMatrixSavings> projectFMatrixSavingsList = projectFMatrixSavingsRepository.findByProject(project);
                Calendar savingDate = Calendar.getInstance();
                Double totalSaving = 0d;
                Double attSaving = 0d;
                Calendar upToReportSavingDate = Calendar.getInstance();
                if (upToReportSavingDate.getTime().equals(reportEndDate.getTime()) || upToReportSavingDate.getTime().after(reportEndDate.getTime())) {
                    upToReportSavingDate = reportEndDate;
                }
                if (!projectFMatrixSavingsList.isEmpty()) {
                    for (ProjectFMatrixSavings projectFMatrixSavings : projectFMatrixSavingsList) {
                        if (projectFMatrixSavings != null && projectFMatrixSavings.getMonth() != null && projectFMatrixSavings.getYear() != null) {
                            savingDate.set(Calendar.MONTH, projectFMatrixSavings.getMonth() - 1);
                            savingDate.set(Calendar.YEAR, projectFMatrixSavings.getYear());
                            if ((savingDate.getTime().equals(projectStartDate.getTime()) || savingDate.getTime().after(projectStartDate.getTime()))
                                    && (savingDate.getTime().equals(reportStartDate.getTime()) || savingDate.getTime().after(reportStartDate.getTime()))
                                    && (savingDate.getTime().equals(upToReportSavingDate.getTime()) || savingDate.getTime().before(upToReportSavingDate.getTime()))) {
                                String date = formater.format(savingDate.getTime()).toUpperCase();
                                projectData.put("SAVING_ATT_" + date, projectFMatrixSavings.getProjectedSavingValue());
                                projectData.put("SAVING_ACT_" + date, projectFMatrixSavings.getActualSavingValue());
                                totalSaving += projectFMatrixSavings.getActualSavingValue();
                                attSaving += projectFMatrixSavings.getProjectedSavingValue();
                            }
                        }
                    }
                    projectData.put("totalACTSaving", totalSaving);
                    projectData.put("totalATTSaving", attSaving);
                }

                projectData.put("remarkList", projectPlanRemarksRepository.findAllByProject(project));
                matricesProjectList.add(projectData);
            }

            matricesVM.setTotalTeamMemberCount(maxMembers);
            matricesVM.setProjectList(matricesProjectList);
            messageVM.setData(matricesVM);
        } else {
            matricesVM.setProjectList(new ArrayList<>());
            messageVM.setData(matricesVM);
        }
        return messageVM;
    }

    public MessageVM getAllPendingPDCAProjectListByETU(Long etuId) {
        MessageVM messageVM = new MessageVM();
        if (Objects.isNull(etuId)) {
            messageVM.setData(projectRepository.findAllProjectNames());
        } else {
            messageVM.setData(projectRepository.findAllProjectNamesByEtuIds(etuId));
        }
        return messageVM;
    }

    @SuppressWarnings("deprecation")
    public MessageVM uploadMatrices(MultipartFile file) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        try {
            @SuppressWarnings("resource")
            XSSFWorkbook wb = new XSSFWorkbook(file.getInputStream());
            System.err.println("Upload Start..");
            int totalRecordsInserted = 0;
            int totalRecordsUpdated = 0;
            for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                Sheet datatypeSheet = wb.getSheetAt(i);
                totalRecordsInserted = 0;
                totalRecordsUpdated = 0;
                for (int j = 2; j < datatypeSheet.getLastRowNum(); j++) {
                    CMatrices cMatric = new CMatrices();
                    Row row = datatypeSheet.getRow(j);
                    Cell cell = null;

                    if (row == null || row.getCell(0) == null) {
                        continue;
                    }
                    //data date
                    cell = row.getCell(0);
                    if (Objects.nonNull(cell) && !cell.getStringCellValue().equals("")) {
                        java.util.Date date = sdf.parse(cell.getStringCellValue());
                        cMatric.setDataDate(date);
                    } else {
                        continue;
                    }

                    //loss type  
                    cell = row.getCell(1);
                    if (Objects.nonNull(cell)) {
                        String lossValue = cell.getStringCellValue();
                        lossValue = lossValue.trim();
                        cMatric.setLossType(lossTypeRepository.findByLossName(lossValue));
                    }

                    //etu 
                    cell = row.getCell(2);
                    if (Objects.nonNull(cell)) {
                        cMatric.setEtu(etuRepository.findOne(1L));
                    }

                    cell = row.getCell(4);
                    if (Objects.nonNull(cell)) {
                        DecimalFormat df2 = new DecimalFormat(".#");
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        String val = cell.getStringCellValue();
                        if (val != null && val.toLowerCase().contains("op")) {
                            val = val.toLowerCase().replace("op", "");

                        }
                        if (val != null) {
                            val = val.trim();
                        }
                        boolean hasNonLetters = false;
                        for (char ch : val.toCharArray()) {
                            if (!Character.isLetter(ch)) {
                                hasNonLetters = true;
                                break;
                            }
                        }
                        if (hasNonLetters) {
                            if (val.contains(".")) {
                                val = df2.format(Double.parseDouble(val));
                            }
                        }
                        cMatric.setMachine(machineRepository.findByMachinCode(val));
                    }

                    //description
                    cell = row.getCell(5);
                    if (Objects.nonNull(cell)) {
                        cMatric.setDescription(cell.getStringCellValue());
                    }

                    cell = row.getCell(6);
                    if (Objects.nonNull(cell)) {
                        //total loss value
                        Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                        cMatric.setTotalLossValue(truncatedDouble);
                    } else {
                        cMatric.setTotalLossValue(0);
                    }

                    cell = row.getCell(7);
                    if (Objects.nonNull(cell)) {
                        //direct labour
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (cell.getNumericCellValue() == 26.0) {//26.0 represents '-' string from Excel
                            cMatric.setDirectLabour(0);
                        } else {
                            Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                            cMatric.setDirectLabour(truncatedDouble);
                        }
                    } else {
                        cMatric.setDirectLabour(0);
                    }

                    cell = row.getCell(8);
                    if (Objects.nonNull(cell)) {
                        //indirect labour
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (cell.getNumericCellValue() == 26.0) {//26.0 represents '-' string from Excel
                            cMatric.setIndirectLabour(0);
                        } else {
                            Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                            cMatric.setIndirectLabour(truncatedDouble);
                        }
                    } else {
                        cMatric.setIndirectLabour(0);
                    }

                    cell = row.getCell(9);
                    if (Objects.nonNull(cell)) {
                        //sallaried Payroll
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (cell.getNumericCellValue() == 26.0) {//26.0 represents '-' string from Excel
                            cMatric.setSalariedPayroll(0);
                        } else {
                            Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                            cMatric.setSalariedPayroll(truncatedDouble);
                        }
                    } else {
                        cMatric.setSalariedPayroll(0);
                    }

                    cell = row.getCell(10);
                    if (Objects.nonNull(cell)) {
                        //indirect material
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (cell.getNumericCellValue() == 26.0) {//26.0 represents '-' string from Excel
                            cMatric.setIndirectMaterial(0);
                        } else {
                            Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                            cMatric.setIndirectMaterial(truncatedDouble);
                        }
                    } else {
                        cMatric.setIndirectMaterial(0);
                    }

                    cell = row.getCell(11);
                    if (Objects.nonNull(cell)) {
                        //maintenace material
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (cell.getNumericCellValue() == 26.0) {//26.0 represents '-' string from Excel
                            cMatric.setManintenanceMaterial(0);
                        } else {
                            Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                            cMatric.setManintenanceMaterial(truncatedDouble);
                        }
                    } else {
                        cMatric.setManintenanceMaterial(0);
                    }

                    cell = row.getCell(12);
                    if (Objects.nonNull(cell)) {
                        //scrap
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (cell.getNumericCellValue() == 26.0) {//26.0 represents '-' string from Excel
                            cMatric.setScrap(0);
                        } else {
                            Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                            cMatric.setScrap(truncatedDouble);
                        }
                    } else {
                        cMatric.setScrap(0);
                    }

                    cell = row.getCell(13);
                    if (Objects.nonNull(cell)) {
                        //energy
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (cell.getNumericCellValue() == 26.0) {//26.0 represents '-' string from Excel
                            cMatric.setEnergy(0);
                        } else {
                            Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                            cMatric.setEnergy(truncatedDouble);

                        }
                    } else {
                        cMatric.setEnergy(0);
                    }

                    cell = row.getCell(14);
                    if (Objects.nonNull(cell)) {
                        //other services
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (cell.getNumericCellValue() == 26.0) {//26.0 represents '-' string from Excel
                            cMatric.setOtherServices(0);
                        } else {

                            Integer truncatedDouble = BigDecimal.valueOf(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_UP).intValue();
                            cMatric.setOtherServices(truncatedDouble);
                        }
                    } else {
                        cMatric.setOtherServices(0);
                    }

                    CMatrices cmatrix = cMatricesRepository.findByDescriptionAndMachineAndEtuAndDataDate(cMatric.getDescription(), cMatric.getMachine(), cMatric.getEtu(), cMatric.getDataDate());
                    if (Objects.nonNull(cmatrix)) {
                        cmatrix.setTotalLossValue(cMatric.getTotalLossValue());
                        cmatrix.setDirectLabour(cMatric.getDirectLabour());
                        cmatrix.setIndirectLabour(cMatric.getIndirectLabour());
                        cmatrix.setSalariedPayroll(cMatric.getSalariedPayroll());
                        cmatrix.setIndirectMaterial(cMatric.getIndirectMaterial());
                        cmatrix.setManintenanceMaterial(cMatric.getManintenanceMaterial());
                        cmatrix.setScrap(cMatric.getScrap());
                        cmatrix.setEnergy(cMatric.getEnergy());
                        cmatrix.setOtherServices(cMatric.getOtherServices());
                        cMatricesRepository.save(cmatrix);
                        totalRecordsUpdated++;
                    }

                    if (Objects.isNull(cmatrix) && Objects.nonNull(cMatric.getDataDate()) && Objects.nonNull(cMatric.getMachine()) && Objects.nonNull(cMatric.getEtu())
                            && Objects.nonNull(cMatric.getDescription()) && Objects.nonNull(cMatric.getLossType()) && Objects.nonNull(cMatric.getTotalLossValue())) {
                        cMatric.setCreatedAt(new Date());
                        cMatric.setUpdatedAt(new Date());
                        cMatricesRepository.save(cMatric);
                        totalRecordsInserted++;
                    }
                }
                System.err.println("Total Record Inserted..  " + totalRecordsInserted);
                System.err.println("Total Record Updated..  " + totalRecordsUpdated);
                totalRecordsInserted = 0;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public MessageVM getCMatricesParetoData(CMatrixFilterVM cMatrixFilterVM) {
        MessageVM messageVM = new MessageVM();
        HashMap<Long, CMatricesRes> allLossTypeMap = new HashMap<>();
        List<LossType> lossTypeList = lossTypeRepository.findAll();
        for (LossType lossType : lossTypeList) {
            CMatricesRes obj = new CMatricesRes();
            obj.setId(lossType.getId());
            obj.setLossType(lossType.getLossType());
            obj.setTotalLossValue(0);
            allLossTypeMap.put(lossType.getId(), obj);
        }

        List<Object[]> dataList = new ArrayList<>();
        if (cMatrixFilterVM.getEtuType() != null && cMatrixFilterVM.getYear() > 0 && cMatrixFilterVM.getMonth() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETU(cMatrixFilterVM.getEtuType(), cMatrixFilterVM.getYear(), cMatrixFilterVM.getMonth());
        } else if (cMatrixFilterVM.getEtuType() != null && cMatrixFilterVM.getYear() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETUUsingTypeAndYear(cMatrixFilterVM.getEtuType(), cMatrixFilterVM.getYear());
        } else if (cMatrixFilterVM.getEtuType() != null && cMatrixFilterVM.getMonth() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETUUsingTypeAndMonth(cMatrixFilterVM.getEtuType(), cMatrixFilterVM.getMonth());
        } else if (cMatrixFilterVM.getYear() > 0 && cMatrixFilterVM.getMonth() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETU(cMatrixFilterVM.getYear(), cMatrixFilterVM.getMonth());
        } else if (cMatrixFilterVM.getEtuType() != null) {
            dataList = cMatricesRepository.getTotalLossValueForETU(cMatrixFilterVM.getEtuType());
        } else if (cMatrixFilterVM.getYear() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETUUsingYear(cMatrixFilterVM.getYear());
        } else if (cMatrixFilterVM.getMonth() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETUUsingMonth(cMatrixFilterVM.getMonth());
        }
        List<CMatricesRes> chartData = new ArrayList<>();
        for (Object[] a : dataList) {
            if (a[0] + "" != null) {
                CMatricesRes obj = allLossTypeMap.get(Long.parseLong(a[0] + ""));
                obj.setTotalLossValue(Integer.parseInt(a[1] + ""));
                allLossTypeMap.replace(Long.parseLong(a[0] + ""), obj);
            }
        }
        allLossTypeMap.forEach((key, value) -> {
            chartData.add(value);
        });

        messageVM.setData(chartData);
        return messageVM;
    }

    public MessageVM getCMatricesData(CMatrixFilterVM cMatrixFilterVM) {
        MessageVM messageVM = new MessageVM();
        DateFormat excelDateFormater = new SimpleDateFormat("MM.dd.yyyy");
        List<CMatrices> cMatricesList = new ArrayList<>();
        List<CMatricesVM> data = new ArrayList<>();
        if (cMatrixFilterVM.getEtuType() != null && cMatrixFilterVM.getYear() > 0 && cMatrixFilterVM.getMonth() > 0) {
            cMatricesList = cMatricesRepository.getCMatricesDataForETU(cMatrixFilterVM.getEtuType(), cMatrixFilterVM.getYear(), cMatrixFilterVM.getMonth());
        }
        for (CMatrices cMatrices : cMatricesList) {
            CMatricesVM cMatricesVM = new CMatricesVM();
            cMatricesVM.setId(cMatrices.getId());
            cMatricesVM.setDescription(cMatrices.getDescription());
            if (cMatrices.getEtu() != null) {
                cMatricesVM.setEtu(cMatrices.getEtu().getEtuNo());
                cMatricesVM.setProcess(cMatrices.getEtu().getProjectAbbrevation());
            }
            if (cMatrices.getDataDate() != null) {
                cMatricesVM.setDataDate(excelDateFormater.format(cMatrices.getDataDate()));
            }
            if (cMatrices.getLossType() != null) {
                cMatricesVM.setLossType(cMatrices.getLossType().getLossType());
            }

            cMatricesVM.setDirectLabour(cMatrices.getDirectLabour());
            cMatricesVM.setIndirectLabour(cMatrices.getIndirectLabour());
            cMatricesVM.setSalariedPayroll(cMatrices.getSalariedPayroll());
            cMatricesVM.setIndirectMaterial(cMatrices.getIndirectMaterial());
            cMatricesVM.setManintenanceMaterial(cMatrices.getManintenanceMaterial());
            cMatricesVM.setScrap(cMatrices.getScrap());
            cMatricesVM.setEnergy(cMatrices.getEnergy());
            cMatricesVM.setOtherServices(cMatrices.getOtherServices());

//            if (cMatrices.getProcess() != null) {
//                cMatricesVM.setProcess(cMatrices.getProcess().getProcessName());
//            }
            cMatricesVM.setTotalLossValue(cMatrices.getTotalLossValue());
            if (cMatrices.getMachine() != null) {
                cMatricesVM.setMachine(cMatrices.getMachine().getMachineName());
            }
            data.add(cMatricesVM);
        }
        messageVM.setData(data);
        return messageVM;
    }

    public MessageVM getCMatricesData1(CMatrixFilterVM cMatrixFilterVM) {
        MessageVM messageVM = new MessageVM();
        HashMap<Long, CMatricesRes> allLossTypeMap = new HashMap<>();
        List<LossType> lossTypeList = lossTypeRepository.findAll();
        for (LossType lossType : lossTypeList) {
            CMatricesRes obj = new CMatricesRes();
            obj.setId(lossType.getId());
            obj.setLossType(lossType.getLossType());
            allLossTypeMap.put(lossType.getId(), obj);
        }

        List<Object[]> dataList = new ArrayList<>();
        if (cMatrixFilterVM.getEtuType() != null && cMatrixFilterVM.getYear() > 0 && cMatrixFilterVM.getMonth() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETU(cMatrixFilterVM.getEtuType(), cMatrixFilterVM.getYear(), cMatrixFilterVM.getMonth());
        } else if (cMatrixFilterVM.getEtuType() != null && cMatrixFilterVM.getYear() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETUUsingTypeAndYear(cMatrixFilterVM.getEtuType(), cMatrixFilterVM.getYear());
        } else if (cMatrixFilterVM.getEtuType() != null && cMatrixFilterVM.getMonth() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETUUsingTypeAndMonth(cMatrixFilterVM.getEtuType(), cMatrixFilterVM.getMonth());
        } else if (cMatrixFilterVM.getYear() > 0 && cMatrixFilterVM.getMonth() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETU(cMatrixFilterVM.getYear(), cMatrixFilterVM.getMonth());
        } else if (cMatrixFilterVM.getEtuType() != null) {
            dataList = cMatricesRepository.getTotalLossValueForETU(cMatrixFilterVM.getEtuType());
        } else if (cMatrixFilterVM.getYear() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETUUsingYear(cMatrixFilterVM.getYear());
        } else if (cMatrixFilterVM.getMonth() > 0) {
            dataList = cMatricesRepository.getTotalLossValueForETUUsingMonth(cMatrixFilterVM.getMonth());
        }
        List<CMatricesRes> chartData = new ArrayList<>();
        for (Object[] a : dataList) {
            if (a[0] + "" != null) {
                LossType lossType = lossTypeRepository.getOne(Long.parseLong(a[0] + ""));
                CMatricesRes obj = new CMatricesRes();
                obj.setLossType(lossType.getLossType());
                obj.setTotalLossValue(Integer.parseInt(a[1] + ""));
                chartData.add(obj);
            }

        }
        messageVM.setData(chartData);
        return messageVM;
    }

    public MessageVM getProjectDetails(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        HashMap<String, Object> projectDetails = new HashMap<>();
        if (project != null) {
            projectDetails.put("projectInfo", project);
            ProjectBenefitIndentsEmatrix benefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
            projectDetails.put("benefitIndentsEMatrix", benefitIndentsEmatrix);

            List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProject(project);
            List<Object> teamMember = new ArrayList<>();
            List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
            List<ProjectTechnicalTools> projectTechnicalToolsList = projectTechnicalToolsRepository.findByProject(project);
            for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                if (projectTeamMember != null && projectTeamMember.getUser() != null) {
                    if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 1) {
                        projectDetails.put("personTeamLeader", getRadarchartData(projectTeamMember.getUser(), project, projectMethodsAndToolsList, projectTechnicalToolsList));
                    }
                    if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 2) {
                        //projectDetails.put("personCoach", projectTeamMember.getUser());
                        projectDetails.put("personCoach", getRadarchartData(projectTeamMember.getUser(), project, projectMethodsAndToolsList, projectTechnicalToolsList));
                    }
                    if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 3) {
                        //teamMember.add(projectTeamMember.getUser());
                        teamMember.add(getRadarchartData(projectTeamMember.getUser(), project, projectMethodsAndToolsList, projectTechnicalToolsList));

                    }
                }
            }
            projectDetails.put("teamMember", teamMember);
            List<ProjectFMatrixSavingsVM> projectFMatrixSavingsVMList = new ArrayList<>();
            List<ProjectFMatrixSavings> projectFMatrixSavingsList = projectFMatrixSavingsRepository.findByProject(project);
            if (!projectFMatrixSavingsList.isEmpty()) {
                for (ProjectFMatrixSavings savings : projectFMatrixSavingsList) {
                    ProjectFMatrixSavingsVM projectFMatrixSavings = new ProjectFMatrixSavingsVM();
                    projectFMatrixSavings.setId(savings.getId());
                    projectFMatrixSavings.setProject_id(project.getId());
                    projectFMatrixSavings.setMonth(savings.getMonth());
                    projectFMatrixSavings.setYear(savings.getYear());
                    projectFMatrixSavings.setActualSavingValue(savings.getActualSavingValue());
                    projectFMatrixSavings.setActualProjectedValue(savings.getProjectedSavingValue());
                    projectFMatrixSavingsVMList.add(projectFMatrixSavings);
                }
            }
            projectDetails.put("projectSavings", projectFMatrixSavingsVMList);
            List<String> pillar = new ArrayList<>();
            List<ProjectSupportingPillars> projectSupportingPillars = supportingPillarsRepository.findByProject(project);
            for (ProjectSupportingPillars supportingPillars : projectSupportingPillars) {
                if (supportingPillars != null && supportingPillars.getPillar() != null && supportingPillars.isPrimary()) {
                    projectDetails.put("basePillar", supportingPillars.getPillar().getCode());
                } else if (supportingPillars.getPillar() != null) {
                    pillar.add(supportingPillars.getPillar().getCode());
                }
            }
            projectDetails.put("supportingPillars", pillar);
            List<ProjectPlanRemarks> projectPlanRemarkList = projectPlanRemarksRepository.findAllByProject(project);
            projectDetails.put("projectPlanRemarkList", projectPlanRemarkList);
        }
        messageVM.setData(projectDetails);
        return messageVM;
    }

    public RadarData getRadarchartData(AuthUser user, Project project, List<ProjectMethodsAndTools> projectMethodsAndToolsList, List<ProjectTechnicalTools> projectTechnicalToolsList) {
        List<RadarAxis> radarAxisData = new ArrayList<>();
        List<String> allTool = new ArrayList<>();
        RadarData data = new RadarData();
        data.setUser(user);
        for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
            if (projectMethodsAndTools.getMethodsAndTools() != null) {
                if (projectMethodsAndTools.getMethodsAndTools() != null) {
                    if (projectMethodsAndTools.getMethodsAndTools().getLevelSecondTool() != null && projectMethodsAndTools.getMethodsAndTools().getLevelSecondTool().trim().length() > 0) {
                        allTool.add(projectMethodsAndTools.getMethodsAndTools().getLevelFirstTool() + " - " + projectMethodsAndTools.getMethodsAndTools().getLevelSecondTool().trim());
                    } else {
                        allTool.add(projectMethodsAndTools.getMethodsAndTools().getLevelFirstTool());
                    }
                }

                List<String> userBelongPillarlist = userPillarRepository.getAllUserPillar(user.getId(), projectMethodsAndTools.getMethodsAndTools().getId());
                if (userBelongPillarlist.size() <= 0) {
                    continue;
                }
                List<SkillInventryEntity> skillSets = userSkillSetRepository.findAllByMethodsToolsAndUser(projectMethodsAndTools.getMethodsAndTools().getId(), user.getId());
                if (!skillSets.isEmpty()) {
                    RadarAxis axis = new RadarAxis();
                    if (projectMethodsAndTools.getMethodsAndTools() != null) {
                        if (projectMethodsAndTools.getMethodsAndTools().getLevelSecondTool() != null && projectMethodsAndTools.getMethodsAndTools().getLevelSecondTool().trim().length() > 0) {
                            axis.setAxis(projectMethodsAndTools.getMethodsAndTools().getLevelFirstTool() + " - " + projectMethodsAndTools.getMethodsAndTools().getLevelSecondTool().trim());
                        } else {
                            axis.setAxis(projectMethodsAndTools.getMethodsAndTools().getLevelFirstTool());
                        }
                    }
                    //   axis.setAxis(projectMethodsAndTools.getMethodsAndTools().getLevelFirstTool());
                    axis.setValue(skillSets.get(0).getActual_skill_id());
                    radarAxisData.add(axis);
                }
            }
        }
        for (ProjectTechnicalTools projectTechnicalTools : projectTechnicalToolsList) {
            if (projectTechnicalTools.getTechnicalTool() != null) {
                allTool.add(projectTechnicalTools.getTechnicalTool().getTechnicalToolDescription());
                List<UserTechnicalToolSkillSet> skillSets = userTechnicalToolSkillSetRepository.findAllByTechnicalToolAndUser(projectTechnicalTools.getTechnicalTool(), user);
                if (!skillSets.isEmpty()) {
                    RadarAxis axis = new RadarAxis();
                    axis.setAxis(projectTechnicalTools.getTechnicalTool().getTechnicalToolDescription());
                    axis.setValue(skillSets.get(0).getActualSkill());
                    radarAxisData.add(axis);
                }
            }
        }
        data.setAllTool(allTool);
        data.setRadarAxisData(radarAxisData);
        return data;
    }

    public HashMap<String, String> getProjectkPIsHeader() {
        List<KPI> kpis = kPIRepository.findAll();
        HashMap<String, String> hashMap = new HashMap<>();
        kpis.forEach(kpi -> hashMap.put("KPI_" + kpi.getId(), kpi.getKpiName()));
        return hashMap;
    }

    public HashMap<Long, Long> getMaxUserProjectToolSkill(AuthUser user, Project project, List<ProjectMethodsAndTools> projectMethodsAndToolsList, HashMap<Long, Long> toolMaxSkill) {
        projectMethodsAndToolsList.stream().filter((projectMethodsAndTools) -> (projectMethodsAndTools.getMethodsAndTools() != null)).forEachOrdered((projectMethodsAndTools) -> {
            List<String> userBelongPillarlist = userPillarRepository.getAllUserPillar(user.getId(), projectMethodsAndTools.getMethodsAndTools().getId());
            if (!(userBelongPillarlist.size() <= 0)) {
                List<SkillInventryEntity> skillSets = userSkillSetRepository.findAllByMethodsToolsAndUser(projectMethodsAndTools.getMethodsAndTools().getId(), user.getId());
                if (!skillSets.isEmpty() && toolMaxSkill.containsKey(projectMethodsAndTools.getMethodsAndTools().getId())) {
                    if (toolMaxSkill.get(projectMethodsAndTools.getMethodsAndTools().getId()) < skillSets.get(0).getActual_skill_id()) {
                        toolMaxSkill.replace(projectMethodsAndTools.getMethodsAndTools().getId(), skillSets.get(0).getActual_skill_id());
                    }
                }
            }
        });
        return toolMaxSkill;
    }

}

class RadarData {

    private AuthUser user;
    private List<RadarAxis> radarAxisData = new ArrayList<>();
    private List<String> allTool = new ArrayList<>();

    public AuthUser getUser() {
        return user;
    }

    public void setUser(AuthUser user) {
        this.user = user;
    }

    public List<RadarAxis> getRadarAxisData() {
        return radarAxisData;
    }

    public void setRadarAxisData(List<RadarAxis> radarAxisData) {
        this.radarAxisData = radarAxisData;
    }

    public List<String> getAllTool() {
        return allTool;
    }

    public void setAllTool(List<String> allTool) {
        this.allTool = allTool;
    }

}

class RadarAxis {

    private String axis;
    private Long value;

    public String getAxis() {
        return axis;
    }

    public void setAxis(String axis) {
        this.axis = axis;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long long1) {
        this.value = long1;
    }

}

class CMatricesRes {

    private Long id;
    private String lossType;
    private Integer totalLossValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLossType() {
        return lossType;
    }

    public void setLossType(String lossType) {
        this.lossType = lossType;
    }

    public Integer getTotalLossValue() {
        return totalLossValue;
    }

    public void setTotalLossValue(Integer totalLossValue) {
        this.totalLossValue = totalLossValue;
    }

}
