package com.blueroom.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.UserCompetencyProgress;
import com.blueroom.repository.UserCompetencyProgressRepository;

@Service
public class UserCompetencyProgressService {

	@Autowired
	UserCompetencyProgressRepository userCompetencyProgressRepository;

	public List<Map<String, Object>> getUserCompetencyProgress(long userId, long competencyId) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for (UserCompetencyProgress userCompetencyProgress : userCompetencyProgressRepository
				.getUserCompetencyProgressByUserIdAndComepetencyID(userId, competencyId)) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("userId", userCompetencyProgress.getUser().getId());
			map.put("competency", userCompetencyProgress.getCompetencyId().getName());
			map.put("previousLevel", userCompetencyProgress.getPreviousLevel());
			map.put("updatedLevel", userCompetencyProgress.getUpdatedLevel());
			map.put("modifiedAt", userCompetencyProgress.getModifiedAt());
			map.put("modefiedBy", userCompetencyProgress.getModefiedBy().getName());
			result.add(map);
		}
		return result;
	}
	
	public  UserCompetencyProgress addUserCompetencyProgress(UserCompetencyProgress userCompetencyProgress) {	
		return userCompetencyProgressRepository.save(userCompetencyProgress);		
	}

}
