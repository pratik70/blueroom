package com.blueroom.services;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.blueroom.entities.Benifit;
import com.blueroom.entities.CDYear;
import com.blueroom.entities.ETU;
import com.blueroom.entities.Factorization;
import com.blueroom.entities.KPI;
import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectBenefitIndents;
import com.blueroom.entities.ProjectBenefitIndentsEmatrix;
import com.blueroom.entities.ProjectFMatrixSavings;
import com.blueroom.entities.ProjectKIP;
import com.blueroom.entities.ProjectMethodsAndTools;
import com.blueroom.entities.ProjectPlanRemarks;
import com.blueroom.entities.ProjectSupportingPillars;
import com.blueroom.entities.ProjectTeamMember;
import com.blueroom.entities.ProjectTechnicalTools;
import com.blueroom.entities.SkillInventryEntity;
import com.blueroom.entities.StageFilter;
import com.blueroom.entities.TeamRole;
import com.blueroom.entities.TechnicalTool;
import com.blueroom.entities.UpgradeMethodToolsSkillHistory;
import com.blueroom.entities.UpgradeTechnicalToolsSkillHistory;
import com.blueroom.entities.UserTechnicalToolSkillSet;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.repository.BenifitRepository;
import com.blueroom.repository.CDYearRepository;
import com.blueroom.repository.ETURepository;
import com.blueroom.repository.FactorizationRepository;
import com.blueroom.repository.KPIRepository;
import com.blueroom.repository.LossRepository;
import com.blueroom.repository.MachineRepository;
import com.blueroom.repository.MethodsAndToolsRepository;
import com.blueroom.repository.PillarRepository;
import com.blueroom.repository.PillarsMethodsAndToolsRepository;
import com.blueroom.repository.ProcessRepository;
import com.blueroom.repository.ProjectBenefitIndentsEmatrixRepository;
import com.blueroom.repository.ProjectBenefitIndentsRepository;
import com.blueroom.repository.ProjectFMatrixSavingsRepository;
import com.blueroom.repository.ProjectKIPRepository;
import com.blueroom.repository.ProjectMethodsAndToolsRepository;
import com.blueroom.repository.ProjectPlanRemarksRepository;
import com.blueroom.repository.ProjectRepository;
import com.blueroom.repository.ProjectSupportingPillarsRepository;
import com.blueroom.repository.ProjectTeamMemberRepository;
import com.blueroom.repository.ProjectTechnicalToolsRepository;
import com.blueroom.repository.RoleRepository;
import com.blueroom.repository.StageFilterRepository;
import com.blueroom.repository.TechnicalToolRepository;
import com.blueroom.repository.UpgradeMethodToolsSkillHistoryRepository;
import com.blueroom.repository.UpgradeTechnicalToolsSkillHistoryRepository;
import com.blueroom.repository.UserSkillSetRepository;
import com.blueroom.repository.UserTechnicalToolSkillSetRepository;
import com.blueroom.utility.SendEmail;
import com.blueroom.vm.BenifitVM;
import com.blueroom.vm.EmailVM;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MemberVM;
import com.blueroom.vm.MessageVM;
import com.blueroom.vm.MethodToolsUserOperator;
import com.blueroom.vm.MethodsAndToolsVM;
import com.blueroom.vm.ProjectBenefitVM;
import com.blueroom.vm.ProjectFMatrixSavingsVM;
import com.blueroom.vm.ProjectMembersVM;
import com.blueroom.vm.ProjectPlanRemarkVM;
import com.blueroom.vm.ProjectPlanningVM;
import com.blueroom.vm.ProjectSupportingPillarVM;
import com.blueroom.vm.ProjectVM;
import com.blueroom.vm.ProjectWCMToolVM;
import com.blueroom.vm.StageFilterVM;
import com.blueroom.vm.TechnicalToolVM;
import com.blueroom.vm.TrainingMailVM;
import com.blueroom.vm.UpgradeSkillVM;
import com.blueroom.vm.UserSkillSetVM;
import com.blueroom.vm.UserSkillTechnicalToolVM;
import com.blueroom.vm.UserVM;
import com.blueroom.vm.UserWorkingProjectCountVM;

@Service
public class ProjectService {

    @Autowired
    private JavaMailSender sender;

    @Autowired
    LossRepository lossRepository;

    @Autowired
    MachineRepository machineRepository;

    @Autowired
    ETURepository etuRepository;

    @Autowired
    ProcessRepository processRepository;

    @Autowired
    PillarRepository pillarRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    ProjectSupportingPillarsRepository supportingPillarsRepository;

    @Autowired
    PillarsMethodsAndToolsRepository pillarsMethodsAndToolsRepository;

    @Autowired
    MethodsAndToolsRepository methodsAndToolsRepository;

    @Autowired
    ProjectMethodsAndToolsRepository projectMethodsAndToolsRepository;

    @Autowired
    UserSkillSetRepository userSkillSetRepository;

    @Autowired
    AuthUserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ProjectTeamMemberRepository projectTeamMemberRepository;

    @Autowired
    ProjectBenefitIndentsRepository projectBenefitIndentsRepository;

    @Autowired
    ProjectBenefitIndentsEmatrixRepository projectBenefitIndentsEmatrixRepository;

    @Autowired
    BenifitRepository benifitRepository;

    @Autowired
    ProjectFMatrixSavingsRepository projectFMatrixSavingsRepository;

    @Autowired
    FactorizationRepository factorizationRepository;

    @Autowired
    ProjectPlanRemarksRepository projectPlanRemarksRepository;

    @Autowired
    ProjectTechnicalToolsRepository projectTechnicalToolsRepository;

    @Autowired
    UserTechnicalToolSkillSetRepository userTechnicalToolSkillSetRepository;

    @Autowired
    TechnicalToolRepository technicalToolRepository;

    @Autowired
    KPIRepository kPIRepository;

    @Autowired
    ProjectSupportingPillarsRepository projectSupportingPillarsRepository;

    @Autowired
    CDYearRepository cdYearRepository;

    @Autowired
    ProjectKIPRepository projectKIPRepository;

    @Autowired
    UpgradeMethodToolsSkillHistoryRepository upgradeMethodToolsSkillHistoryRepository;

    @Autowired
    UpgradeTechnicalToolsSkillHistoryRepository upgradeTechnicalToolsSkillHistoryRepository;

    @Autowired
    StageFilterRepository stageFilterRepository;

    @Autowired
    SendEmail sendEmail;

    TeamRole role1 = new TeamRole();
    TeamRole role2 = new TeamRole();
    TeamRole role3 = new TeamRole();

    public ProjectService() {
        super();
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MessageVM createProject(ProjectVM projectVM) {
        MessageVM messageVM = new MessageVM();
        Project projectEntity = new Project();
        if (projectVM != null) {
            projectEntity.setProjectName(projectVM.getProjectName());
            projectEntity.setPreliminaryAnalysis(projectVM.getPreliminaryAnalysis());
            projectEntity.setYearlyPotential(projectVM.getYearlyPotential());
            ETU etu = etuRepository.findOne(projectVM.getEtu());
            projectEntity.setEtu(etu);
            Long previousProjectCode = projectRepository.getPreviousProjectCodeByETU(etu.getProjectAbbrevation() + "-") + 1;
            String updatedProjectCode = etu.getProjectAbbrevation() + "-" + previousProjectCode.toString();
            projectEntity.setProjectCode(updatedProjectCode);

            CDYear cdYear = cdYearRepository.findOne(projectVM.getProjectCD());
            projectEntity.setcDYear(cdYear);
            if (projectVM.getLossType() != null) {
                projectEntity.setLossType(lossRepository.findOne(projectVM.getLossType()));
            }
            if (projectVM.getOptMachine() != null) {
                projectEntity.setOptMachine(machineRepository.findOne(projectVM.getOptMachine()));
            }
            if (projectVM.getProcess() != null) {
                projectEntity.setProcess(processRepository.findOne(projectVM.getProcess()));
            }
            projectEntity.setCompleteStage(1);
            projectEntity.setDescription(projectVM.getDescription());

            projectEntity.setDirectLoss(projectVM.getDirectLoss());
            projectEntity.setIndirectLoss(projectVM.getIndirectLoss());
            projectEntity.setStaffLoss(projectVM.getStaffLoss());
            projectEntity.setEnergyLoss(projectVM.getEnergyLoss());
            projectEntity.setExpensessLoss(projectVM.getExpensessLoss());
            projectEntity.setScrapLoss(projectVM.getScrapLoss());
            projectEntity.setIndirectMaterialsLoss(projectVM.getIndirectMaterialsLoss());
            projectEntity.setMaintenanceLoss(projectVM.getMaintenanceLoss());

            projectEntity.setCreatedAt(new Date());
            projectEntity.setUpdatedAt(new Date());
            projectEntity.setEwDocument(projectVM.getEwDocument());
            projectRepository.save(projectEntity);
            projectVM.setProjectId(projectEntity.getId());
            messageVM.setData(projectVM);
        }
        return messageVM;
    }

    public Project changeETUApproval(Long id) {
        Project project = new Project();
        if (Objects.nonNull(id)) {

            project = projectRepository.findOne(id);
            project.setEtuStatus(!project.isEtuStatus());
            projectRepository.save(project);

        }

        return project;

    }

    public Project changeFinanceApproval(Long id) {
        Project project = new Project();
        if (Objects.nonNull(id)) {

            project = projectRepository.findOne(id);
            project.setFinanceStatus(!project.isFinanceStatus());
            projectRepository.save(project);
        }

        return project;

    }

    public MessageVM deleteProject(Long id) {
        MessageVM messageVM = new MessageVM();

        List<ProjectBenefitIndents> projectBenefitIndents = new ArrayList<>();
        projectBenefitIndents = projectBenefitIndentsRepository.findAllByProjectId(id);
        for (ProjectBenefitIndents pbi : projectBenefitIndents) {
            projectBenefitIndentsRepository.delete(pbi.getId());
        }

        List<ProjectBenefitIndentsEmatrix> projectBenefitIndentsEmatrix = new ArrayList<>();
        projectBenefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findAllByProjectId(id);
        for (ProjectBenefitIndentsEmatrix pbie : projectBenefitIndentsEmatrix) {
            projectBenefitIndentsEmatrixRepository.delete(pbie.getId());
        }

        List<ProjectFMatrixSavings> projectFMatrixSavings = new ArrayList<>();
        projectFMatrixSavings = projectFMatrixSavingsRepository.findAllByProjectId(id);
        for (ProjectFMatrixSavings pfs : projectFMatrixSavings) {
            projectFMatrixSavingsRepository.delete(pfs.getId());
        }

        List<ProjectKIP> projectKIP = new ArrayList<>();
        projectKIP = projectKIPRepository.findAllByProjectId(id);
        for (ProjectKIP pkip : projectKIP) {
            projectKIPRepository.delete(pkip.getId());
        }

        List<ProjectMethodsAndTools> projectMethodsAndTools = new ArrayList<>();
        projectMethodsAndTools = projectMethodsAndToolsRepository.findAllByProjectId(id);
        for (ProjectMethodsAndTools pmt : projectMethodsAndTools) {
            projectMethodsAndToolsRepository.delete(pmt.getId());
        }

        List<ProjectPlanRemarks> projectPlanRemarks = new ArrayList<>();
        projectPlanRemarks = projectPlanRemarksRepository.findAllByProjectId(id);
        for (ProjectPlanRemarks ppr : projectPlanRemarks) {
            projectPlanRemarksRepository.delete(ppr.getId());
        }

        List<ProjectSupportingPillars> projectSupportingPillars = new ArrayList<>();
        projectSupportingPillars = projectSupportingPillarsRepository.findAllByProjectId(id);
        for (ProjectSupportingPillars psp : projectSupportingPillars) {
            projectSupportingPillarsRepository.delete(psp.getId());
        }

        List<ProjectTeamMember> projectTeamMember = new ArrayList<>();
        projectTeamMember = projectTeamMemberRepository.findAllByProjectId(id);
        for (ProjectTeamMember ptm : projectTeamMember) {
            projectTeamMemberRepository.delete(ptm.getId());
        }

        List<ProjectTechnicalTools> projectTechnicalTools = new ArrayList<>();
        projectTechnicalTools = projectTechnicalToolsRepository.findAllByProjectId(id);
        for (ProjectTechnicalTools ptt : projectTechnicalTools) {
            projectTechnicalToolsRepository.delete(ptt.getId());
        }

        Project project = new Project();
        project = projectRepository.findOne(id);
        projectRepository.delete(project.getId());
        return messageVM;

    }

    public MessageVM updatedProject(Long id, ProjectVM projectVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(id);
        boolean isUpdateSavingData = false;
        if (project != null) {
            project.setProjectName(projectVM.getProjectName());
            project.setPreliminaryAnalysis(projectVM.getPreliminaryAnalysis());
            if (project.getYearlyPotential() != projectVM.getYearlyPotential()) {
                isUpdateSavingData = true;
            }
            project.setYearlyPotential(projectVM.getYearlyPotential());
            //project.setProjectCode(projectVM.getProjectCode());

            CDYear cdYear = cdYearRepository.findOne(projectVM.getProjectCD());
            project.setcDYear(cdYear);

            if (projectVM.getEtu() != null) {
                project.setEtu(etuRepository.findOne(projectVM.getEtu()));
            }
            if (projectVM.getLossType() != null) {
                project.setLossType(lossRepository.findOne(projectVM.getLossType()));
            }
            if (projectVM.getOptMachine() != null) {
                project.setOptMachine(machineRepository.findOne(projectVM.getOptMachine()));
            }
            if (projectVM.getProcess() != null) {
                project.setProcess(processRepository.findOne(projectVM.getProcess()));
            }

            project.setDirectLoss(projectVM.getDirectLoss());
            project.setIndirectLoss(projectVM.getIndirectLoss());
            project.setStaffLoss(projectVM.getStaffLoss());
            project.setEnergyLoss(projectVM.getEnergyLoss());
            project.setExpensessLoss(projectVM.getExpensessLoss());
            project.setScrapLoss(projectVM.getScrapLoss());
            project.setIndirectMaterialsLoss(projectVM.getIndirectMaterialsLoss());
            project.setMaintenanceLoss(projectVM.getMaintenanceLoss());

            project.setDescription(projectVM.getDescription());
            project.setUpdatedAt(new Date());
            if (project.getCompleteStage() == null || project.getCompleteStage() <= 0) {
                project.setCompleteStage(1);
            }
            project.setEwDocument(projectVM.getEwDocument());
            projectRepository.save(project);
            if (isUpdateSavingData) {
                updateSavingData(project);
                if (project.getCompleteStage() != null && project.getCompleteStage() >= 5) {
                    updateProjectCosting(project);
                }
            }
            projectVM.setProjectId(project.getId());
            messageVM.setData(projectVM);
        }

        return messageVM;
    }

    public MessageVM addSupportingPillars(ProjectSupportingPillarVM pillarVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(pillarVM.getProjectId());
        if (project != null) {
            List<ProjectSupportingPillars> pillerList = supportingPillarsRepository.findByProject(project);
            if (pillarVM.getMainPillar() != null) {
                boolean isPillarAdd = false;
                for (int i = 0; i < pillerList.size(); i++) {
                    if (pillerList.get(i).getPillar() != null && pillerList.get(i).getPillar().getId().equals(pillarVM.getMainPillar())) {
                        ProjectSupportingPillars projectMainPillar = pillerList.get(i);
                        projectMainPillar.setPrimary(true);
                        supportingPillarsRepository.save(projectMainPillar);
                        pillerList.remove(i);
                        isPillarAdd = true;
                        break;
                    }
                }
                if (!isPillarAdd) {
                    ProjectSupportingPillars projectMainPillar = new ProjectSupportingPillars();
                    projectMainPillar.setProject(project);
                    projectMainPillar.setPillar(pillarRepository.getOne(pillarVM.getMainPillar()));
                    projectMainPillar.setPrimary(true);
                    supportingPillarsRepository.save(projectMainPillar);
                }
            }
            if (pillarVM.getSupportingPillars().size() > 0) {
                for (Long pillarId : pillarVM.getSupportingPillars()) {
                    if (pillarId != null) {
                        boolean isPillarAdd = false;
                        for (int i = 0; i < pillerList.size(); i++) {
                            if (pillerList.get(i).getPillar() != null && pillerList.get(i).getPillar().getId().equals(pillarId)) {
                                ProjectSupportingPillars projectMainPillar = pillerList.get(i);
                                projectMainPillar.setPrimary(false);
                                supportingPillarsRepository.save(projectMainPillar);
                                pillerList.remove(i);
                                isPillarAdd = true;
                                break;
                            }
                        }
                        if (!isPillarAdd) {
                            ProjectSupportingPillars projectMainPillar = new ProjectSupportingPillars();
                            projectMainPillar.setProject(project);
                            projectMainPillar.setPillar(pillarRepository.getOne(pillarId));
                            projectMainPillar.setPrimary(false);
                            supportingPillarsRepository.save(projectMainPillar);
                        }
                    }
                }

            }

            if (pillarVM.getKpis().size() > 0) {
                List<ProjectKIP> projectKIPList = projectKIPRepository.findAllByProject(project);
                for (Long kpiId : pillarVM.getKpis()) {
                    if (kpiId != null) {
                        KPI kpi = kPIRepository.findOne(kpiId);
                        boolean isProjectKpiSelected = false;
                        for (ProjectKIP projectKIP : projectKIPList) {
                            if (projectKIP != null && projectKIP.getKpi() == kpi) {
                                isProjectKpiSelected = true;
                                projectKIPList.remove(projectKIP);
                                break;
                            }
                        }
                        if (!isProjectKpiSelected) {
                            ProjectKIP projectKIP = new ProjectKIP();
                            projectKIP.setKpi(kpi);
                            projectKIP.setProject(project);
                            projectKIPRepository.save(projectKIP);
                        }
                    }
                }
                for (ProjectKIP projectKIP : projectKIPList) {
                    projectKIPRepository.delete(projectKIP);;
                }
            } else {
                List<ProjectKIP> projectKIPList = projectKIPRepository.findAllByProject(project);
                for (ProjectKIP projectKIP : projectKIPList) {
                    projectKIPRepository.delete(projectKIP);;
                }
            }

            if (project.getCompleteStage() == null || project.getCompleteStage() <= 1) {
                project.setCompleteStage(2);
                projectRepository.save(project);
            }
            for (int i = 0; i < pillerList.size(); i++) {
                ProjectSupportingPillars projectMainPillar = pillerList.get(i);
                supportingPillarsRepository.delete(projectMainPillar);
            }
            messageVM.setMessage("Successfully saved project pillars.");
            messageVM.setCode("200");
        } else {
            messageVM.setMessage("Error : Add project pillar failed: Project not found!");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public MessageVM getProjectPillars(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            List<ProjectSupportingPillars> pillerList = supportingPillarsRepository.findByProject(project);
            ProjectSupportingPillarVM projectSupportingPillarVM = new ProjectSupportingPillarVM();
            List<Long> list = new ArrayList<>();
            projectSupportingPillarVM.setProjectId(projectId);
            for (ProjectSupportingPillars supportingPillar : pillerList) {
                if (supportingPillar.isPrimary()) {
                    projectSupportingPillarVM.setMainPillar(supportingPillar.getPillar().getId());
                } else {
                    list.add(supportingPillar.getPillar().getId());
                }
            }
            List<Long> kpis = new ArrayList<>();
            List<ProjectKIP> projectKIPList = projectKIPRepository.findAllByProject(project);
            for (ProjectKIP projectKIP : projectKIPList) {
                kpis.add(projectKIP.getKpi().getId());
            }
            projectSupportingPillarVM.setKpis(kpis);
            projectSupportingPillarVM.setSupportingPillars(list);
            messageVM.setData(projectSupportingPillarVM);

        }
        return messageVM;
    }

    @Transactional
    public MessageVM getAllSelectedPillarMethodsAndTools(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            List<Long> pillerIdList = supportingPillarsRepository.getAllSelectedPillarByProjectId(project.getId());
            List<Object[]> objectList = methodsAndToolsRepository.findAllMethodsToolsAndPillarInfo(pillerIdList);
            List<MethodsAndToolsVM> resultList = new ArrayList<>();
            objectList.stream().forEach((action) -> {
                MethodsAndToolsVM newVm = new MethodsAndToolsVM(
                        (Objects.nonNull(action[0]) ? ((BigInteger) action[0]).longValue() : 0l),
                        Objects.nonNull(action[1]) ? (String) action[1] : "",
                        Objects.nonNull(action[2]) ? (String) action[2] : "",
                        Objects.nonNull(action[3]) ? (String) action[3] : "",
                        Objects.nonNull(action[4]) ? (String) action[4] : "");
                resultList.add(newVm);
            });
            messageVM.setData(resultList);
        }
        return messageVM;
    }

    public MessageVM saveMethodsAndTools(Long projectId, ProjectWCMToolVM projectWCMToolVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
            List<ProjectTechnicalTools> projectTechnicalToolsList = projectTechnicalToolsRepository.findByProject(project);
            try {
                HashMap<Long, MethodsAndToolsVM> map = new HashMap<>();
                for (MethodsAndToolsVM vm : projectWCMToolVM.getMethodsAndTools()) {
                    map.put(vm.getId(), vm);
                }
                for (Long i : map.keySet()) {
                    MethodsAndToolsVM vm = map.get(i);
                    if (vm.getId() != null && vm.getRequiredSkill() != null) {
                        MethodsAndTools methodsAndTools = methodsAndToolsRepository.getOne(vm.getId());
                        if (methodsAndTools != null) {
                            boolean isInserted = false;
                            for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                                if (projectMethodsAndTools != null && projectMethodsAndTools.getMethodsAndTools() != null && projectMethodsAndTools.getMethodsAndTools() == methodsAndTools) {
                                    projectMethodsAndToolsList.remove(projectMethodsAndTools);
                                    //projectMethodsAndTools = projectMethodsAndToolsList.get(0);
                                    projectMethodsAndTools.setRequiredSkill(vm.getRequiredSkill());
                                    projectMethodsAndToolsRepository.save(projectMethodsAndTools);
                                    isInserted = true;
                                    break;
                                }
                            }

                            if (!isInserted) {
                                List<ProjectMethodsAndTools> projectMethodsAndToolsPresent = projectMethodsAndToolsRepository.findByProjectAndMethodsTools(project, methodsAndTools);
                                if (projectMethodsAndToolsPresent.isEmpty()) {
                                    ProjectMethodsAndTools projectMethodsAndTools = new ProjectMethodsAndTools();
                                    projectMethodsAndTools.setProject(project);
                                    projectMethodsAndTools.setMethodsAndTools(methodsAndTools);
                                    projectMethodsAndTools.setRequiredSkill(vm.getRequiredSkill());
                                    projectMethodsAndToolsRepository.save(projectMethodsAndTools);
                                }

                            }
                        }

                    }
                }

                for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                    projectMethodsAndToolsRepository.delete(projectMethodsAndTools);
                }
                for (TechnicalToolVM technicalToolVM : projectWCMToolVM.getTechnicalTools()) {
                    if (technicalToolVM.getTechnicalToolId() != null && technicalToolVM.getRequiredSkill() != null) {
                        TechnicalTool technicalTool = technicalToolRepository.getOne(technicalToolVM.getTechnicalToolId());
                        boolean isInserted = false;
                        if (technicalTool != null) {
                            for (ProjectTechnicalTools projectTechnicalTools : projectTechnicalToolsList) {
                                if (projectTechnicalTools != null && projectTechnicalTools.getTechnicalTool() != null && projectTechnicalTools.getTechnicalTool() == technicalTool) {
                                    projectTechnicalToolsList.remove(projectTechnicalTools);
                                    projectTechnicalTools.setRequiredSkill(technicalToolVM.getRequiredSkill());
                                    projectTechnicalToolsRepository.save(projectTechnicalTools);
                                    isInserted = true;
                                    break;
                                }
                            }
                            if (!isInserted) {
                                ProjectTechnicalTools projectTechnicalTools = new ProjectTechnicalTools();
                                projectTechnicalTools.setProject(project);
                                projectTechnicalTools.setTechnicalTool(technicalTool);
                                projectTechnicalTools.setRequiredSkill(technicalToolVM.getRequiredSkill());
                                projectTechnicalToolsRepository.save(projectTechnicalTools);
                            }
                        }
                    }
                }
                for (ProjectTechnicalTools projectTechnicalTools : projectTechnicalToolsList) {
                    projectTechnicalToolsRepository.delete(projectTechnicalTools);
                }
                if (project.getCompleteStage() == null || project.getCompleteStage() <= 2) {
                    project.setCompleteStage(3);
                    projectRepository.save(project);
                }
                messageVM.setMessage("Successfully saved project methods and tools.");
                messageVM.setCode("200");
            } catch (Exception e) {
                e.printStackTrace();
                messageVM.setMessage("Error : saved project methods and tools.");
                messageVM.setCode("500");
            }

        }
        return messageVM;
    }

    public MessageVM getProjectMethodsAndTools(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        List<MethodsAndToolsVM> methodsAndToolsVMs = new ArrayList<>();
        if (project != null) {
            try {
                List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
                for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                    if (projectMethodsAndTools.getMethodsAndTools() != null) {
                        MethodsAndTools methodsAndTools = projectMethodsAndTools.getMethodsAndTools();
                        MethodsAndToolsVM toolsVM = new MethodsAndToolsVM();
                        toolsVM.setId(methodsAndTools.getId());
                        toolsVM.setLevelFirstTool(methodsAndTools.getLevelFirstTool());
                        toolsVM.setLevelSecondTool(methodsAndTools.getLevelSecondTool());
                        toolsVM.setType(methodsAndTools.getType());
                        toolsVM.setRequiredSkill(projectMethodsAndTools.getRequiredSkill());

                        methodsAndToolsVMs.add(toolsVM);
                    }
                }
                messageVM.setData(methodsAndToolsVMs);
                messageVM.setMessage("Project methods and tools list.");
                messageVM.setCode("200");
            } catch (Exception e) {
                e.printStackTrace();
                messageVM.setMessage("Error : get project methods and tools.");
                messageVM.setCode("500");
            }

        }
        return messageVM;
    }

    public MessageVM getProjectQualifiedUsers(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        List<MethodToolsUserOperator> operatorsList = new ArrayList<>();
        if (project != null) {
            List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
            for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                MethodToolsUserOperator methodToolsUserOperator = new MethodToolsUserOperator();

                MethodsAndToolsVM methodsAndToolsVM = new MethodsAndToolsVM();
                if (projectMethodsAndTools.getMethodsAndTools() != null) {
                    methodsAndToolsVM.setId(projectMethodsAndTools.getMethodsAndTools().getId());
                    methodsAndToolsVM.setLevelFirstTool(projectMethodsAndTools.getMethodsAndTools().getLevelFirstTool());
                    methodsAndToolsVM.setLevelSecondTool(projectMethodsAndTools.getMethodsAndTools().getLevelSecondTool());
                    methodsAndToolsVM.setType(projectMethodsAndTools.getMethodsAndTools().getType());
                }
                methodToolsUserOperator.setMethodsAndToolsVM(methodsAndToolsVM);

                operatorsList.add(methodToolsUserOperator);

            }
            messageVM.setData(operatorsList);
        }

        return messageVM;
    }

    public MessageVM getProjectSkillQualifiedUsers(Long projectId, Boolean usersmode) {

        MessageVM messageVM = new MessageVM();
        List<Object[]> stageWorkingCount = new ArrayList<>();

        stageWorkingCount = projectRepository.getstagescounts();

        for (Object[] b : stageWorkingCount) {
            StageFilter stageFilter = new StageFilter();
            stageFilter.setProjectId((long) Integer.parseInt(b[0] + ""));
            stageFilter.setUserId((long) Integer.parseInt(b[1] + ""));
            stageFilter.setRoleId((long) Integer.parseInt(b[2] + ""));
            stageFilter.setpWorkingStage((long) Integer.parseInt(b[3] + ""));
            stageFilter.setdWorkingStage((long) Integer.parseInt(b[4] + ""));
            stageFilter.setcWorkingStage((long) Integer.parseInt(b[5] + ""));
            stageFilter.setaWorkingStage((long) Integer.parseInt(b[6] + ""));

            if (stageFilter.getpWorkingStage() != 0) {
                stageFilterRepository.save(stageFilter);
            }
            if (stageFilter.getdWorkingStage() != 0) {
                stageFilterRepository.save(stageFilter);
            }
            if (stageFilter.getcWorkingStage() != 0) {
                stageFilterRepository.save(stageFilter);
            }
            if (stageFilter.getaWorkingStage() != 0) {
                stageFilterRepository.save(stageFilter);
            }

        }
        List<Long> onGoingProjects = stageFilterRepository.getAllonGoingProjects();

        List<Object[]> data = projectRepository.getAllPreferedUserWorkingProjectCount(onGoingProjects);

        HashMap<Long, UserWorkingProjectCountVM> methodToolsCol = new HashMap<>();
        for (Object[] a : data) {
            UserWorkingProjectCountVM countVM = new UserWorkingProjectCountVM();
            countVM.setTotalWorkingProject(Integer.parseInt(a[1] + ""));
            countVM.setTotalProjectLeader(Integer.parseInt(a[2] + ""));
            countVM.setTotalProjectCoach(Integer.parseInt(a[3] + ""));
            countVM.setTotalProjectMember(Integer.parseInt(a[4] + ""));
            methodToolsCol.put(Long.parseLong(a[0] + ""), countVM);

        }
        Project project = projectRepository.findOne(projectId);
        if (project != null) {

            List<BigInteger> userIdsList = new ArrayList<>();
            List<BigInteger> deleteUserIdsList = new ArrayList<>();
            List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
            List<Long> pillarids = new ArrayList<Long>();
            List<ProjectSupportingPillars> pillerList = supportingPillarsRepository.findByProject(project);

            for (ProjectSupportingPillars pillar : pillerList) {
                long pillarId = pillar.getPillar().getId();
                pillarids.add(pillarId);
            }
            //projectMethodsAndToolsList get all those user 
            for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                if (projectMethodsAndTools.getMethodsAndTools() == null) {
                    continue;
                }
                deleteUserIdsList.addAll(userSkillSetRepository.getUserListBySkillWithoutUser(projectMethodsAndTools.getMethodsAndTools().getId(), projectMethodsAndTools.getRequiredSkill()/*,project.getEtu().getId()*/));
                userIdsList.addAll(deleteUserIdsList);
                deleteUserIdsList.clear();
            }
            List<ProjectTechnicalTools> projectTechnicalToolsList = projectTechnicalToolsRepository.findByProject(project);

            for (ProjectTechnicalTools projectTechnicalTools : projectTechnicalToolsList) {
                if (projectTechnicalTools.getTechnicalTool() == null) {
                    continue;
                }
                if (userIdsList.isEmpty()) {
                    userIdsList.addAll(userTechnicalToolSkillSetRepository.getUserListBySkillWithoutUser(projectTechnicalTools.getTechnicalTool().getId(), projectTechnicalTools.getRequiredSkill()/*, pillarids*/));
                } else {
                    userIdsList.addAll(userTechnicalToolSkillSetRepository.getUserListBySkill(projectTechnicalTools.getTechnicalTool().getId(), projectTechnicalTools.getRequiredSkill(), userIdsList/*, pillarids*/));
                }
            }
            //Userlist add filter for etu
            /**
             * *****************
             */
            List<BigInteger> etuUserList = new ArrayList<>();

            List<Long> filteredUserIdsList = new ArrayList<>();
            if (usersmode) {
                filteredUserIdsList = userIdsList.stream().map(mapper -> mapper.longValue()).collect(Collectors.toList());
            } else {
                etuUserList.addAll(etuRepository.findUserIdsByETUId(project.getEtu().getId()));
                filteredUserIdsList = userIdsList.stream().filter(u -> etuUserList.contains(u)).map(u -> u.longValue()).collect(Collectors.toList());
            }
            /**
             * *****************
             */
            if (!filteredUserIdsList.isEmpty()) {
                List<AuthUser> userList = userRepository.findAll(filteredUserIdsList);
                List<UserVM> userWorkingList = new ArrayList<>();
                for (AuthUser user : userList) {
                    UserVM userVM = new UserVM();
                    userVM.setId(user.getId());
                    userVM.setName(user.getName());
                    userVM.setEtuLevel(user.getEtuLevel() == null ? 1l : user.getEtuLevel());
                    if (methodToolsCol.get(user.getId()) != null) {
                        userVM.setProjectWorkingCount(methodToolsCol.get(user.getId()));
                    } else {
                        userVM.setWorkingProject(0);
                    }
                    userWorkingList.add(userVM);
                }
                messageVM.setData(userWorkingList);
            } else {
                messageVM.setData(new ArrayList<>());
            }
        }
        stageFilterRepository.deleteAll();
        return messageVM;

    }

    public MessageVM addProjectMembers(Long projectId, ProjectMembersVM projectMembersVM) {
        MessageVM messageVM = new MessageVM();

        List<ProjectTeamMember> getProjectTeamMember = (List<ProjectTeamMember>) new ArrayList<ProjectTeamMember>();

        getProjectTeamMember = projectTeamMemberRepository.findOneByProjectId(projectId);
        if (getProjectTeamMember.size() >= 0) {
            for (ProjectTeamMember ptm : getProjectTeamMember) {
                projectTeamMemberRepository.delete(ptm.getId());
            }
        }

        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            role1 = roleRepository.getOne(1);
            role2 = roleRepository.getOne(2);
            role3 = roleRepository.getOne(3);
            List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProject(project);
            if (projectMembersVM.getTeamLeader() != null) {
                addMember(projectMembersVM.getTeamLeader(), project, role1, projectMembersVM.isTeamLeaderFiltered());
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 1) {
                        projectTeamMembersList.remove(projectTeamMember);
                        break;
                    }
                }
            }
            if (projectMembersVM.getTeamCoach() != null) {
                addMember(projectMembersVM.getTeamCoach(), project, role2, projectMembersVM.isTeamCoachFiltered());
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 2) {
                        projectTeamMembersList.remove(projectTeamMember);
                        break;
                    }
                }
            }

            if (projectMembersVM.getFirstTeamMember() != null) {
                addMember(projectMembersVM.getFirstTeamMember(), project, role3, projectMembersVM.isTeamLeaderFiltered());
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 3) {
                        projectTeamMembersList.remove(projectTeamMember);
                        break;
                    }
                }
            }
            if (projectMembersVM.getSecondTeamMember() != null) {
                addMember(projectMembersVM.getSecondTeamMember(), project, role3, projectMembersVM.isTeamLeaderFiltered());
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 3) {
                        projectTeamMembersList.remove(projectTeamMember);
                        break;
                    }
                }
            }
            if (projectMembersVM.getThirdTeamMember() != null) {
                addMember(projectMembersVM.getThirdTeamMember(), project, role3, projectMembersVM.isTeamLeaderFiltered());
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember.getRole() != null && projectTeamMember.getRole().getId() == 3) {
                        projectTeamMembersList.remove(projectTeamMember);
                        break;
                    }
                }
            }

            for (MemberVM memberVM : projectMembersVM.getTeamMembers()) {
                addMember(memberVM.getUserId(), project, role3, memberVM.isFilteredUser());
                for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                    if (projectTeamMember.getUser() != null && memberVM.getUserId() == projectTeamMember.getUser().getId()) {
                        projectTeamMembersList.remove(projectTeamMember);
                        break;
                    }
                }
            }
            for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                projectTeamMemberRepository.delete(projectTeamMember);
            }
            if (project.getCompleteStage() == null || project.getCompleteStage() <= 3) {
                project.setCompleteStage(4);
                projectRepository.save(project);
            }
            messageVM.setMessage("Successfully project members saved.");
            messageVM.setCode("200");
        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public void addMember(Long memberUserId, Project project, TeamRole role, boolean isFiltered) {
        AuthUser memberUser = userRepository.getOne(memberUserId);
        if (memberUser != null) {
            List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProjectAndUserAndRole(project, memberUser, role);
            if (!projectTeamMembersList.isEmpty()) {
                ProjectTeamMember member = projectTeamMembersList.get(0);
                member.setFilteredUser(isFiltered);
                projectTeamMemberRepository.save(member);
            } else {
                ProjectTeamMember member = new ProjectTeamMember();
                member.setProject(project);
                member.setUser(memberUser);
                member.setRole(role);
                member.setFilteredUser(isFiltered);
                projectTeamMemberRepository.save(member);
            }

        }
    }

    public MessageVM getProject(Long id) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(id);
        if (project != null) {
            messageVM.setData(project);
            messageVM.setCode("200");
            messageVM.setMessage("Project Information.");
        } else {
            messageVM.setCode("500");
            messageVM.setMessage("Error :: Project not found!!.");
        }
        return messageVM;
    }

    public MessageVM getProject(String projectCode) {
        MessageVM messageVM = new MessageVM();
        List<Project> projectList = projectRepository.findByProjectCode(projectCode);
        if (!projectList.isEmpty()) {
            messageVM.setData(projectList.get(0));
            messageVM.setCode("200");
            messageVM.setMessage("Project Information.");
        } else {
            messageVM.setCode("500");
            messageVM.setMessage("Error :: Project not found!!.");
        }
        return messageVM;
    }

    public MessageVM getProjectTeamMembers(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        ProjectMembersVM projectMembersVM = new ProjectMembersVM();
        List<MemberVM> memberVMs = new ArrayList<>();
        if (project != null) {
            int i = 0;
            List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProject(project);
            for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                if (projectTeamMember.getRole() != null) {

                    if (projectTeamMember.getRole().getId() == 1) {
                        projectMembersVM.setTeamLeader(projectTeamMember.getUser().getId());
                        projectMembersVM.setTeamLeaderFiltered(projectTeamMember.isFilteredUser());
                    } else if (projectTeamMember.getRole().getId() == 2) {
                        projectMembersVM.setTeamCoach(projectTeamMember.getUser().getId());
                        projectMembersVM.setTeamCoachFiltered(projectTeamMember.isFilteredUser());
                    } else if (projectTeamMember.getRole().getId() == 3) {

                        if (i == 0) {
                            projectMembersVM.setFirstTeamMember(projectTeamMember.getUser().getId());

                        }
                        if (i == 1) {
                            projectMembersVM.setSecondTeamMember(projectTeamMember.getUser().getId());
                        }
                        if (i == 2) {
                            projectMembersVM.setThirdTeamMember(projectTeamMember.getUser().getId());
                        }
                        i++;

//                        MemberVM memberVM = new MemberVM();
//                        memberVM.setUserId(projectTeamMember.getUser().getId());
//                        memberVM.setFilteredUser(projectTeamMember.isFilteredUser());
//                        memberVMs.add(memberVM);
                    }
                }
            }
            projectMembersVM.setTeamMembers(memberVMs);
            messageVM.setData(projectMembersVM);
            messageVM.setMessage("project members list.");
            messageVM.setCode("200");
        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public MessageVM manageProjectCosting(Long projectId, ProjectBenefitVM projectBenefitVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            //        	project.setEtuMailSend(true);
//        	projectRepository.save(project);
            List<AuthUser> authuser = userRepository.findAllByApproval(projectId);

            ProjectBenefitIndentsEmatrix projectBenefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
            if (projectBenefitIndentsEmatrix != null) {
                projectBenefitIndentsEmatrix.setProject(project);
                projectBenefitIndentsEmatrix.setBc(projectBenefitVM.getBc());
                projectBenefitIndentsEmatrix.setConfidenceLevel(projectBenefitVM.getConfidenceLevel());
                projectBenefitIndentsEmatrix.setTotalPotentialBenefit(projectBenefitVM.getTotalPotentialBenefit());
                projectBenefitIndentsEmatrix.setTotalProjectCost(projectBenefitVM.getTotalProjectCost());
                projectBenefitIndentsEmatrix.setSavingsType(projectBenefitVM.getSavingsType());
                projectBenefitIndentsEmatrix.setUpdatedAt(new Date());
            } else {
                projectBenefitIndentsEmatrix = new ProjectBenefitIndentsEmatrix();
                projectBenefitIndentsEmatrix.setProject(project);
                projectBenefitIndentsEmatrix.setBc(projectBenefitVM.getBc());
                projectBenefitIndentsEmatrix.setConfidenceLevel(projectBenefitVM.getConfidenceLevel());
                projectBenefitIndentsEmatrix.setTotalPotentialBenefit(projectBenefitVM.getTotalPotentialBenefit());
                projectBenefitIndentsEmatrix.setTotalProjectCost(projectBenefitVM.getTotalProjectCost());
                projectBenefitIndentsEmatrix.setSavingsType(projectBenefitVM.getSavingsType());
                projectBenefitIndentsEmatrix.setCreatedAt(new Date());
                projectBenefitIndentsEmatrix.setUpdatedAt(new Date());
            }
            projectBenefitIndentsEmatrixRepository.save(projectBenefitIndentsEmatrix);
            for (BenifitVM benifitVM : projectBenefitVM.getBenifitList()) {
                Benifit benifit = benifitRepository.getOne(benifitVM.getId());
                ProjectBenefitIndents projectBenefitIndents = projectBenefitIndentsRepository.findOneByProjectAndBenifit(project, benifit);
                if (projectBenefitIndents != null) {
                    projectBenefitIndents.setProject(project);
                    projectBenefitIndents.setBenifit(benifit);
                    projectBenefitIndents.setBenifitValue(benifitVM.getBenifitValue());
                } else {
                    projectBenefitIndents = new ProjectBenefitIndents();
                    projectBenefitIndents.setProject(project);
                    projectBenefitIndents.setBenifit(benifit);
                    projectBenefitIndents.setBenifitValue(benifitVM.getBenifitValue());
                }
                projectBenefitIndentsRepository.save(projectBenefitIndents);
            }

            if (project.getCompleteStage() == null || project.getCompleteStage() <= 4) {
                project.setCompleteStage(5);
            }
            boolean flat = sendEmailsToETUAndFinanceApprovals(project);
            if (flat) {
                project.setEtuMailSend(sendEmailsToETUAndFinanceApprovals(project));
            } else {
                project.setEtuMailSend(true);
            }
            project = projectRepository.save(project);
            projectBenefitIndentsEmatrix.setProject(project);
            /////////////
            messageVM.setData(projectBenefitIndentsEmatrix);
            messageVM.setMessage("project benefit list.");
            messageVM.setCode("200");

        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public boolean sendEmailsToETUAndFinanceApprovals(Project project) {
        MessageVM messageVM = new MessageVM();
        boolean flag = false;
        if (project.getCompleteStage() != null && project.getCompleteStage() >= 5 && !project.isEtuMailSend()) {
            List<AuthUser> authuser = userRepository.findAllByApproval(project.getId());
            List<String> emps = new ArrayList<String>();
            for (AuthUser user : authuser) {
                if (Objects.nonNull(user)) {
                    MimeMessage message = sender.createMimeMessage();

                    MimeMessageHelper helper = new MimeMessageHelper(message);
                    try {
                        helper.setTo(user.getEmail());
                        helper.setText("Blueroom: B/C Details = " + " Hi User Your Project Name is " + project.getProjectName() + "Your Project Code is " + project.getProjectCode());
                        helper.setSubject("Blueroom: B/C Details");
                        sender.send(message);

                        emps.add(user.getName());
                        flag = true;
                    } catch (MessagingException e) {
                        // TODO: handle exception
                        e.printStackTrace();
                        // messageVM.setData("message not sent  ");
                    } catch (Exception e) {
                        // TODO: handle exception
                        //	messageVM.setData("message not sent  ");
                    }
                }
            }
            messageVM.setData(project);
        }
        return flag;
    }

    public MessageVM getProjectCostingData(Long projectId) {
        MessageVM messageVM = new MessageVM();
        ProjectBenefitVM projectBenefitVM = new ProjectBenefitVM();
        List<BenifitVM> benifitList = new ArrayList<>();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            ProjectBenefitIndentsEmatrix projectBenefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
            if (projectBenefitIndentsEmatrix != null) {
                projectBenefitVM.setBc(projectBenefitIndentsEmatrix.getBc());
                projectBenefitVM.setConfidenceLevel(projectBenefitIndentsEmatrix.getConfidenceLevel());
                projectBenefitVM.setTotalPotentialBenefit(projectBenefitIndentsEmatrix.getTotalPotentialBenefit());
                projectBenefitVM.setTotalProjectCost(projectBenefitIndentsEmatrix.getTotalProjectCost());
                projectBenefitVM.setCreatedAt(projectBenefitIndentsEmatrix.getCreatedAt());
                projectBenefitVM.setUpdatedAt(projectBenefitIndentsEmatrix.getUpdatedAt());
                projectBenefitVM.setSavingsType(projectBenefitIndentsEmatrix.getSavingsType());
                List<ProjectBenefitIndents> projectBenefitIndentsList = projectBenefitIndentsRepository.findByProject(project);
                for (ProjectBenefitIndents projectBenefitIndents : projectBenefitIndentsList) {
                    if (projectBenefitIndents.getBenifit() != null) {
                        BenifitVM benifitVM = new BenifitVM();
                        benifitVM.setBenifitName(projectBenefitIndents.getBenifit().getBenifitName());
                        benifitVM.setBenifitValue(projectBenefitIndents.getBenifitValue());
                        benifitVM.setId(projectBenefitIndents.getBenifit().getId());
                        benifitList.add(benifitVM);
                    }
                }
                projectBenefitVM.setBenifitList(benifitList);
                messageVM.setData(projectBenefitVM);

            }
            messageVM.setMessage("Project benifit list.");
            messageVM.setCode("200");
        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public MessageVM savePlanningDates(Long projectId, ProjectPlanningVM projectPlanningVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            ProjectBenefitIndentsEmatrix projectBenefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
            if (projectBenefitIndentsEmatrix != null) {
                projectBenefitIndentsEmatrix.setStartDate(projectPlanningVM.getStartDate());
                projectBenefitIndentsEmatrix.setTargetDate(projectPlanningVM.getTargetDate());

                //p status date 
                projectBenefitIndentsEmatrix.setpPlannedStartDate(projectPlanningVM.getpPlannedStartDate());
                projectBenefitIndentsEmatrix.setpPlannedEndDate(projectPlanningVM.getpPlannedEndDate());

                //d status date 
                projectBenefitIndentsEmatrix.setdPlannedStartDate(projectPlanningVM.getdPlannedStartDate());
                projectBenefitIndentsEmatrix.setdPlannedEndDate(projectPlanningVM.getdPlannedEndDate());

                //c status date 
                projectBenefitIndentsEmatrix.setcPlannedStartDate(projectPlanningVM.getcPlannedStartDate());
                projectBenefitIndentsEmatrix.setcPlannedEndDate(projectPlanningVM.getcPlannedEndDate());

                //a status date 
                projectBenefitIndentsEmatrix.setaPlannedStartDate(projectPlanningVM.getaPlannedStartDate());
                projectBenefitIndentsEmatrix.setaPlannedEndDate(projectPlanningVM.getaPlannedEndDate());
                projectBenefitIndentsEmatrixRepository.save(projectBenefitIndentsEmatrix);

                if (project.getCompleteStage() == null || project.getCompleteStage() <= 5) {
                    project.setCompleteStage(6);
                    projectRepository.save(project);
                }
                messageVM.setMessage("Successfully Project planning data saved.");
                messageVM.setCode("200");
            } else {
                messageVM.setMessage("Error : Project plan not possible with this data.");
                messageVM.setCode("500");
            }
        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public MessageVM saveProjectExecutionDates(Long projectId, ProjectPlanningVM projectPlanningVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            ProjectBenefitIndentsEmatrix projectBenefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
            if (projectBenefitIndentsEmatrix != null) {

                //p status date 
                projectBenefitIndentsEmatrix.setpActualStartDate(projectPlanningVM.getpActualStartDate());
                projectBenefitIndentsEmatrix.setpActualEndDate(projectPlanningVM.getpActualEndDate());

                //d status date 
                projectBenefitIndentsEmatrix.setdActualStartDate(projectPlanningVM.getdActualStartDate());
                projectBenefitIndentsEmatrix.setdActualEndDate(projectPlanningVM.getdActualEndDate());

                //c status date 
                projectBenefitIndentsEmatrix.setcActualStartDate(projectPlanningVM.getcActualStartDate());
                projectBenefitIndentsEmatrix.setcActualEndDate(projectPlanningVM.getcActualEndDate());

                //a status date 
                projectBenefitIndentsEmatrix.setaActualStartDate(projectPlanningVM.getaActualStartDate());
                projectBenefitIndentsEmatrix.setaActualEndDate(projectPlanningVM.getaActualEndDate());

                projectBenefitIndentsEmatrix.setCompletionDate(projectPlanningVM.getCompletionDate());
                projectBenefitIndentsEmatrixRepository.save(projectBenefitIndentsEmatrix);

                if (projectPlanningVM.isSaveCompletionDate()) {
                    DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
                    Date completionDate;
                    try {
                        if (Objects.nonNull(projectPlanningVM.getaActualEndDate())) {
                            completionDate = formater.parse(projectPlanningVM.getaActualEndDate());

                            Calendar completionDateCalendar = Calendar.getInstance();
                            completionDateCalendar.setTime(completionDate);
                            //						while (completionDateCalendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
                            //							completionDateCalendar.add(Calendar.DATE, 1);
                            //					    }
                            //						completionDateCalendar.add(Calendar.DATE, -1);
                            project.setCompletionDate(completionDateCalendar.getTime());
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    updateSavingData(project);
                }

                if (project.getCompleteStage() == null || project.getCompleteStage() <= 5) {
                    project.setCompleteStage(6);
                }
                projectRepository.save(project);
                messageVM.setMessage("Successfully Project planning data saved.");
                messageVM.setCode("200");
            } else {
                messageVM.setMessage("Error : Project plan not possible with this data.");
                messageVM.setCode("500");
            }
        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public void updateSavingData(Project project) {
        if (project.getCompletionDate() == null) {
            return;
        }
        DateFormat formater = new SimpleDateFormat("MM");
        DateFormat formater2 = new SimpleDateFormat("yyyy");
        Calendar beginCalendar = Calendar.getInstance();
        beginCalendar.setTime(project.getCompletionDate());
        beginCalendar.add(Calendar.DATE, 1);

        Calendar finishCalendar = Calendar.getInstance();
        finishCalendar.setTime(beginCalendar.getTime());
        finishCalendar.add(Calendar.YEAR, 1);

        if (finishCalendar.after(new Date())) {
            finishCalendar.setTime(new Date());
            finishCalendar.add(Calendar.DATE, -1);
        }
        Integer totalDaysWorking = getdaysBetweenTwoDates(beginCalendar.getTime(), finishCalendar.getTime());
        List<ProjectFMatrixSavings> projectFMatrixSavingsList = projectFMatrixSavingsRepository.findByProject(project);
        Integer savingPerDay = (int) (project.getYearlyPotential() / 365);
        if (totalDaysWorking > 0) {
            while (beginCalendar.before(finishCalendar)) {
                String month = formater.format(beginCalendar.getTime());
                String year = formater2.format(beginCalendar.getTime());
                ProjectFMatrixSavings projectFMatrixSavings = new ProjectFMatrixSavings();
                for (ProjectFMatrixSavings fMatrixSavings : projectFMatrixSavingsList) {
                    if (fMatrixSavings.getMonth() == Integer.parseInt(month) && fMatrixSavings.getYear() == Integer.parseInt(month)) {
                        projectFMatrixSavings = fMatrixSavings;
                        projectFMatrixSavingsList.remove(fMatrixSavings);
                        break;
                    }
                }
                projectFMatrixSavings.setMonth(Integer.parseInt(month));
                projectFMatrixSavings.setYear(Integer.parseInt(year));

                Factorization factorization = factorizationRepository.findOneByFactorYearAndFactorMonth(projectFMatrixSavings.getYear(), projectFMatrixSavings.getMonth());
                Calendar endOfMonth = Calendar.getInstance();
                endOfMonth.setTime(beginCalendar.getTime());
                endOfMonth.set(Calendar.DAY_OF_MONTH, endOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
                if (endOfMonth.getTimeInMillis() > (new Date()).getTime()) {
                    if (month.equals(formater.format(new Date())) && year.equals(formater2.format(new Date()))) {
                        endOfMonth.setTime(new Date());
                    } else {
                        endOfMonth.setTime(beginCalendar.getTime());
                    }

                }

                Integer monthDaysWorking = getdaysBetweenTwoDates(beginCalendar.getTime(), endOfMonth.getTime());

                projectFMatrixSavings.setProjectedSavingValue((int) (savingPerDay * monthDaysWorking));
                if (factorization != null && factorization.getFactorValue() != 0) {
                    projectFMatrixSavings.setActualSavingValue((int) (savingPerDay * monthDaysWorking * factorization.getFactorValue()));
                } else {
                    projectFMatrixSavings.setActualSavingValue((int) (savingPerDay * monthDaysWorking));
                }
                projectFMatrixSavings.setProject(project);
                projectFMatrixSavingsRepository.save(projectFMatrixSavings);
                beginCalendar.add(Calendar.MONTH, 1);
                beginCalendar.set(Calendar.DAY_OF_MONTH, beginCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            }
        }
        if (projectFMatrixSavingsList.size() < 0) {
            for (ProjectFMatrixSavings fMatrixSavings : projectFMatrixSavingsList) {
                projectFMatrixSavingsRepository.delete(fMatrixSavings);
            }
        }
    }

    public int getdaysBetweenTwoDates(Date date1, Date date2) {
        int daysdiff = 0;
        long diff = date2.getTime() - date1.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        daysdiff = (int) diffDays;
        return daysdiff;
    }

    public MessageVM getProjectPlanningDates(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        ProjectPlanningVM projectPlanningVM = new ProjectPlanningVM();
        if (project != null) {
            ProjectBenefitIndentsEmatrix projectBenefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
            if (projectBenefitIndentsEmatrix != null) {
                projectPlanningVM.setStartDate(projectBenefitIndentsEmatrix.getStartDate());
                projectPlanningVM.setTargetDate(projectBenefitIndentsEmatrix.getTargetDate());

                //p status date 
                projectPlanningVM.setpPlannedStartDate(projectBenefitIndentsEmatrix.getpPlannedStartDate());
                projectPlanningVM.setpPlannedEndDate(projectBenefitIndentsEmatrix.getpPlannedEndDate());
                projectPlanningVM.setpActualStartDate(projectBenefitIndentsEmatrix.getpActualStartDate());
                projectPlanningVM.setpActualEndDate(projectBenefitIndentsEmatrix.getpActualEndDate());

                //d status date 
                projectPlanningVM.setdPlannedStartDate(projectBenefitIndentsEmatrix.getdPlannedStartDate());
                projectPlanningVM.setdPlannedEndDate(projectBenefitIndentsEmatrix.getdPlannedEndDate());
                projectPlanningVM.setdActualStartDate(projectBenefitIndentsEmatrix.getdActualStartDate());
                projectPlanningVM.setdActualEndDate(projectBenefitIndentsEmatrix.getdActualEndDate());

                //c status date 
                projectPlanningVM.setcPlannedStartDate(projectBenefitIndentsEmatrix.getcPlannedStartDate());
                projectPlanningVM.setcPlannedEndDate(projectBenefitIndentsEmatrix.getcPlannedEndDate());
                projectPlanningVM.setcActualStartDate(projectBenefitIndentsEmatrix.getcActualStartDate());
                projectPlanningVM.setcActualEndDate(projectBenefitIndentsEmatrix.getcActualEndDate());

                //a status date 
                projectPlanningVM.setaPlannedStartDate(projectBenefitIndentsEmatrix.getaPlannedStartDate());
                projectPlanningVM.setaPlannedEndDate(projectBenefitIndentsEmatrix.getaPlannedEndDate());
                projectPlanningVM.setaActualStartDate(projectBenefitIndentsEmatrix.getaActualStartDate());
                projectPlanningVM.setaActualEndDate(projectBenefitIndentsEmatrix.getaActualEndDate());

                projectPlanningVM.setTotalPotentialBenefit(projectBenefitIndentsEmatrix.getTotalPotentialBenefit());
                messageVM.setData(projectPlanningVM);
                messageVM.setCode("200");
            } else {
                messageVM.setMessage("Error : Project plan not possible with this data.");
                messageVM.setCode("500");
            }
        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public MessageVM saveProjectActualSavings(Long projectId, List<ProjectFMatrixSavingsVM> projectFMatrixSavingsVMList) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            for (ProjectFMatrixSavingsVM savingsVM : projectFMatrixSavingsVMList) {
                ProjectFMatrixSavings projectFMatrixSavings = projectFMatrixSavingsRepository.findOneByProjectAndMonthAndYear(project, savingsVM.getMonth(), savingsVM.getYear());
                if (projectFMatrixSavings != null) {
                    projectFMatrixSavings.setYear(savingsVM.getYear());
                    //projectFMatrixSavings.setActualSavingValue(savingsVM.getActualSavingValue());
                    projectFMatrixSavingsRepository.save(projectFMatrixSavings);

                } else {
                    projectFMatrixSavings = new ProjectFMatrixSavings();
                    projectFMatrixSavings.setProject(project);
                    projectFMatrixSavings.setMonth(savingsVM.getMonth());
                    projectFMatrixSavings.setYear(savingsVM.getYear());
                    //projectFMatrixSavings.setActualSavingValue(savingsVM.getActualSavingValue());
                    projectFMatrixSavingsRepository.save(projectFMatrixSavings);
                }
            }
            if (project.getCompleteStage() == null || project.getCompleteStage() <= 7) {
                project.setCompleteStage(8);
                projectRepository.save(project);
            }
            messageVM.setData(projectFMatrixSavingsVMList);
            messageVM.setMessage("Successfully project saving saved.");
            messageVM.setCode("200");
        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public MessageVM getProjectActualSavings(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        List<ProjectFMatrixSavingsVM> projectFMatrixSavingsVMList = new ArrayList<>();
        if (project != null) {
            List<ProjectFMatrixSavings> projectFMatrixSavingsList = projectFMatrixSavingsRepository.findByProject(project);
            if (!projectFMatrixSavingsList.isEmpty()) {
                for (ProjectFMatrixSavings savings : projectFMatrixSavingsList) {
                    ProjectFMatrixSavingsVM projectFMatrixSavings = new ProjectFMatrixSavingsVM();
                    projectFMatrixSavings.setId(savings.getId());
                    projectFMatrixSavings.setProject_id(project.getId());
                    projectFMatrixSavings.setMonth(savings.getMonth());
                    projectFMatrixSavings.setYear(savings.getYear());
                    projectFMatrixSavings.setActualSavingValue(savings.getActualSavingValue());
                    projectFMatrixSavings.setActualProjectedValue(savings.getProjectedSavingValue());
                    projectFMatrixSavingsVMList.add(projectFMatrixSavings);
                }
            }
            messageVM.setData(projectFMatrixSavingsVMList);
            messageVM.setMessage("Successfully get project saving list.");
            messageVM.setCode("200");
        }
        return messageVM;
    }

    public MessageVM saveProjectComplete(Long projectId, ProjectVM projectVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {

            List<AuthUser> authuser = userRepository.findAllByFinanceApproval(projectId);

            boolean setFinancemail = false;
            boolean setMessage = false;
            if (authuser.size() > 0) {
                setMessage = false;
            } else {
                setMessage = true;
            }
            for (AuthUser user : authuser) {

                MimeMessage message = sender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(message);
                try {
                    helper.setTo(user.getEmail());
                    helper.setText("Bluroom: Project Completion Details = "
                            + " Hi Please provide finance approval for this  Project whiich Name is "
                            + project.getProjectName()
                            + "Your Project Code is "
                            + project.getProjectCode());
                    helper.setSubject("Bluroom: Project Completion Details");
                    sender.send(message);
                    // project.setFinanceMailSend(true);
                    setFinancemail = true;
                } catch (MessagingException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }

            project.setCompleteStage(7);
            project.setHorizontalExpansion(projectVM.isHorizontalExpansion());
            project.setCompletionDate(projectVM.getCompletionDate());
            project.setRemark(projectVM.getRemark());
            project.setUpdatedAt(new Date());
            project.setStandardisation(projectVM.getStandardisation());
            project.setStandardisationRemark(projectVM.getStandardisationRemark());
            project.setEpmInfo(projectVM.getEpmInfo());
            project.setEpmInfoRemark(projectVM.getEpmInfoRemark());
            project.setDocument(projectVM.getDocument());
            project.setEwDocument(projectVM.getEwDocument());

            projectVM.setProjectId(project.getId());

            messageVM.setData(projectVM);

            if (setMessage) {
                messageVM.setMessage("user not found");
            } else {
                messageVM.setMessage("Project updated successfully.");
            }
            messageVM.setCode("200");
            projectVM.setFinance_mail_send(project.financeMailSend);

            if (setFinancemail) {
                project.setFinanceMailSend(true);

            }
            projectRepository.save(project);

        } else {
            messageVM.setMessage("Error : Project not found.");
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public MessageVM startExecutionPlan(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            project.setExecutionStarted(true);
            project.setUpdatedAt(new Date());
            projectRepository.save(project);
            messageVM.setData(project);
            messageVM.setCode("200");
        }
        return messageVM;
    }

    public MessageVM addProjectRemark(Long projectId, ProjectPlanRemarkVM projectPlanRemarkVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            List<ProjectPlanRemarks> projectPlanRemarkList = projectPlanRemarksRepository.findByProjectAndWeekStartDate(project, projectPlanRemarkVM.getWeekStartDate());
            if (!projectPlanRemarkList.isEmpty()) {
                ProjectPlanRemarks planRemarks = projectPlanRemarkList.get(0);
                planRemarks.setRemarkText(projectPlanRemarkVM.getRemarkText());
                projectPlanRemarksRepository.save(planRemarks);
            } else {
                ProjectPlanRemarks planRemarks = new ProjectPlanRemarks();
                planRemarks.setProject(project);
                planRemarks.setWeekStartDate(projectPlanRemarkVM.getWeekStartDate());
                planRemarks.setRemarkText(projectPlanRemarkVM.getRemarkText());
                projectPlanRemarksRepository.save(planRemarks);
                projectPlanRemarkVM.setId(planRemarks.getId());
            }
            messageVM.setData(projectPlanRemarkVM);
            messageVM.setCode("200");
        }
        return messageVM;
    }

    public MessageVM getProjectRemark(Long projectId, ProjectPlanRemarkVM projectPlanRemarkVM) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            List<ProjectPlanRemarks> projectPlanRemarkList = projectPlanRemarksRepository.findByProjectAndWeekStartDate(project, projectPlanRemarkVM.getWeekStartDate());
            if (!projectPlanRemarkList.isEmpty()) {
                ProjectPlanRemarks planRemarks = projectPlanRemarkList.get(0);
                projectPlanRemarkVM.setRemarkText(planRemarks.getRemarkText());
            }
            messageVM.setData(projectPlanRemarkVM);
            messageVM.setCode("200");
        }
        return messageVM;
    }

    public MessageVM getProjectTechnicalTool(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);

        if (project != null) {
            try {
                List<TechnicalToolVM> technicalToolList = new ArrayList<>();
                List<ProjectTechnicalTools> projectTechnicalToolsList = projectTechnicalToolsRepository.findByProject(project);
                for (ProjectTechnicalTools projectTechnicalTools : projectTechnicalToolsList) {
                    if (projectTechnicalTools.getTechnicalTool() != null) {
                        TechnicalToolVM technicalToolVM = new TechnicalToolVM();
                        technicalToolVM.setTechnicalToolId(projectTechnicalTools.getTechnicalTool().getId());
                        technicalToolVM.setRequiredSkill(projectTechnicalTools.getRequiredSkill());
                        technicalToolList.add(technicalToolVM);
                    }
                }
                messageVM.setData(technicalToolList);
                messageVM.setMessage("Project methods and tools list.");
                messageVM.setCode("200");
            } catch (Exception e) {
                e.printStackTrace();
                messageVM.setMessage("Error : get project methods and tools.");
                messageVM.setCode("500");
            }

        }
        return messageVM;
    }

    public MessageVM getSkillUpdationData(Long projectId) {
        MessageVM messageVM = new MessageVM();
        Project project = projectRepository.findOne(projectId);
        if (project != null) {
            List<ProjectMethodsAndTools> projectMethodsAndToolsList = projectMethodsAndToolsRepository.findByProject(project);
            List<ProjectTechnicalTools> projectTechnicalToolsList = projectTechnicalToolsRepository.findByProject(project);
            List<ProjectTeamMember> projectTeamMembersList = projectTeamMemberRepository.findByProject(project);
            List<Object> userList = new ArrayList<>();
            for (ProjectTeamMember projectTeamMember : projectTeamMembersList) {
                HashMap< String, Object> hashMap = new HashMap<>();
                List<UserSkillSetVM> skillSetVMList = new ArrayList<>();
                for (ProjectMethodsAndTools projectMethodsAndTools : projectMethodsAndToolsList) {
                    UserSkillSetVM skillSetVM = new UserSkillSetVM();
                    skillSetVM.setId(projectMethodsAndTools.getId());
                    List<SkillInventryEntity> userSkillSetList = userSkillSetRepository.findAllByMethodsToolsAndUser(projectMethodsAndTools.getMethodsAndTools().getId(), projectTeamMember.getUser().getId());
                    if (!userSkillSetList.isEmpty()) {
                        SkillInventryEntity userSkillSet = userSkillSetList.get(0);
                        skillSetVM.setActualSkill(userSkillSet.getActual_skill_id());
                        skillSetVM.setUser(projectTeamMember.getUser());
                        skillSetVM.setMethodsTools(projectMethodsAndTools.getMethodsAndTools());
                    }/* else {
						skillSetVM.setActualSkill(0);
						skillSetVM.setUser(projectTeamMember.getUser());
						skillSetVM.setMethodsTools(projectMethodsAndTools.getMethodsAndTools());
					}*/
                    skillSetVMList.add(skillSetVM);
                }
                List<UserSkillTechnicalToolVM> userSkillTechnicalToolVMList = new ArrayList<>();
                for (ProjectTechnicalTools projectTechnicalTools : projectTechnicalToolsList) {
                    UserSkillTechnicalToolVM skillSetVM = new UserSkillTechnicalToolVM();
                    List<UserTechnicalToolSkillSet> userTechnicalToolSkillSetList = userTechnicalToolSkillSetRepository.findAllByTechnicalToolAndUser(projectTechnicalTools.getTechnicalTool(), projectTeamMember.getUser());
                    skillSetVM.setId(projectTechnicalTools.getId());
                    if (!userTechnicalToolSkillSetList.isEmpty()) {
                        UserTechnicalToolSkillSet userSkillSet = userTechnicalToolSkillSetList.get(0);
                        skillSetVM.setActualSkill(userSkillSet.getActualSkill());
                        skillSetVM.setUser(projectTeamMember.getUser());
                        skillSetVM.setTechnicalTool(projectTechnicalTools.getTechnicalTool());
                    } else {
                        skillSetVM.setActualSkill((long) 0);
                        skillSetVM.setUser(projectTeamMember.getUser());
                        skillSetVM.setTechnicalTool(projectTechnicalTools.getTechnicalTool());
                    }
                    userSkillTechnicalToolVMList.add(skillSetVM);
                }

                hashMap.put("user", projectTeamMember.getUser());
                hashMap.put("methodAndTools", skillSetVMList);
                hashMap.put("technicalTools", userSkillTechnicalToolVMList);
                userList.add(hashMap);
            }
            messageVM.setData(userList);
            messageVM.setCode("200");
        } else {
            messageVM.setData(new ArrayList<>());
            messageVM.setCode("500");
        }
        return messageVM;
    }

    public MessageVM getSatgeCount(MatrixFilterVM filterVM) {
        MessageVM messageVM = new MessageVM();
        ProjectWorkingState projectWorkingState = new ProjectWorkingState();
        List<Object[]> stageWorkingCount = new ArrayList<>();

        stageWorkingCount = projectRepository.getSatgeCount();
        if (stageWorkingCount.size() > 0) {
            for (Object[] a : stageWorkingCount) {
                projectWorkingState.setpWorkingStages(Integer.parseInt(a[0] + ""));

                if (Integer.parseInt(a[1] + "") > 0) {
                    projectWorkingState.setdWorkingStages(Integer.parseInt(a[1] + ""));
                }

                if (Integer.parseInt(a[2] + "") > 0) {
                    projectWorkingState.setcWorkingStages(Integer.parseInt(a[2] + ""));
                }

                if (Integer.parseInt(a[3] + "") > 0) {
                    projectWorkingState.setaWorkingStages(Integer.parseInt(a[3] + ""));
                }
            }
        }
        messageVM.setData(projectWorkingState);
        return messageVM;
    }

    public MessageVM sendTrainingMail(Long projectId, TrainingMailVM trainingMailVM) {
        MessageVM messageVM = new MessageVM();
        EmailVM emailVM = new EmailVM();
        emailVM.setFrom("mindnervesdemo@gmail.com");
        emailVM.setTo("ranajitmahakunde2010@gmail.com");
        emailVM.setSubject(trainingMailVM.getMailSubject());
        emailVM.setMailBody("<div> Hi </div>");
        //sendEmail.sendEmail(emailVM);
        messageVM.setMessage("Mail send successfully.");
        return messageVM;
    }

    //TODO User pillar skill set
    public MessageVM upgradeUserSkill(List<UpgradeSkillVM> upgradeSkillList) {

        for (UpgradeSkillVM upgradeSkillVM : upgradeSkillList) {
            AuthUser user = userRepository.findOne(upgradeSkillVM.getUserId());
            if (user != null) {
                for (MethodsAndToolsVM methodsAndToolsVM : upgradeSkillVM.getMethodAndTools()) {
                    MethodsAndTools methodsAndTools = methodsAndToolsRepository.findOne(methodsAndToolsVM.getId());
                    List<SkillInventryEntity> userSkillSet = userSkillSetRepository.findAllByMethodsToolsAndUser(methodsAndTools.getId(), user.getId());
                    UpgradeMethodToolsSkillHistory methodToolsSkillHistory = new UpgradeMethodToolsSkillHistory();
                    SkillInventryEntity skillSet = new SkillInventryEntity();
                    if (!userSkillSet.isEmpty()) {
                        skillSet = userSkillSet.get(0);
                        methodToolsSkillHistory.setOldSkill(skillSet.getActual_skill_id());
                        methodToolsSkillHistory.setNewSkill(methodsAndToolsVM.getUpdatedSkill());
                        skillSet.setActual_skill_id(methodsAndToolsVM.getUpdatedSkill());
                        skillSet.setMethodsTools(methodsAndTools);
                        //skillSet.setUser(user);
                        userSkillSetRepository.saveData(user.getId(), methodsAndTools.getId(), methodsAndToolsVM.getUpdatedSkill());

                        methodToolsSkillHistory.setComment(methodsAndToolsVM.getComment());
                        methodToolsSkillHistory.setUser(user);
                        methodToolsSkillHistory.setMethodsTools(methodsAndTools);
                        methodToolsSkillHistory.setCreatedAt(new Date());
                        methodToolsSkillHistory.setUpdatedAt(new Date());
                        upgradeMethodToolsSkillHistoryRepository.save(methodToolsSkillHistory);
                    }/* else {
						System.err.println("User Id (" + user.getId() + ")Not found for Method Id ("+ methodsAndTools.getId() +") for update skill.");
						methodToolsSkillHistory.setOldSkill(0);
						methodToolsSkillHistory.setNewSkill(methodsAndToolsVM.getUpdatedSkill());
					}*/
                }

                for (TechnicalToolVM technicalToolVM : upgradeSkillVM.getTechnicalTools()) {
                    UpgradeTechnicalToolsSkillHistory technicalToolsSkillHistory = new UpgradeTechnicalToolsSkillHistory();
                    TechnicalTool technicalTool = technicalToolRepository.findOne(technicalToolVM.getTechnicalToolId());
                    List<UserTechnicalToolSkillSet> technicalToolList = userTechnicalToolSkillSetRepository.findAllByTechnicalToolAndUser(technicalTool, user);
                    UserTechnicalToolSkillSet technicalToolSkillSet = new UserTechnicalToolSkillSet();
                    if (!technicalToolList.isEmpty()) {
                        technicalToolSkillSet = technicalToolList.get(0);
                        technicalToolsSkillHistory.setNewSkill(technicalToolVM.getUpdatedSkill());
                        technicalToolsSkillHistory.setOldSkill(technicalToolSkillSet.getActualSkill());
                    } else {
                        technicalToolsSkillHistory.setNewSkill(technicalToolVM.getUpdatedSkill());
                        technicalToolsSkillHistory.setOldSkill((long) 0.0);
                        System.err.println("User Id (" + user.getId() + ")Not found for technical Id (" + technicalTool.getId() + ") for update skill.");
                    }
                    technicalToolSkillSet.setActualSkill(technicalToolVM.getUpdatedSkill());
                    technicalToolSkillSet.setTechnicalTool(technicalTool);
                    technicalToolSkillSet.setUser(user);
                    userTechnicalToolSkillSetRepository.save(technicalToolSkillSet);

                    technicalToolsSkillHistory.setComment(technicalToolVM.getComment());
                    technicalToolsSkillHistory.setUser(user);
                    technicalToolsSkillHistory.setTechnicalTool(technicalTool);
                    technicalToolsSkillHistory.setCreatedAt(new Date());
                    technicalToolsSkillHistory.setUpdatedAt(new Date());
                    upgradeTechnicalToolsSkillHistoryRepository.save(technicalToolsSkillHistory);
                }

            }
        }
        return null;
    }

    private void updateProjectCosting(Project project) {
        ProjectBenefitIndentsEmatrix projectBenefitIndentsEmatrix = projectBenefitIndentsEmatrixRepository.findOneByProject(project);
        if (projectBenefitIndentsEmatrix != null) {
            projectBenefitIndentsEmatrix.setProject(project);
            projectBenefitIndentsEmatrix.setTotalPotentialBenefit(project.getYearlyPotential().intValue());
            double per = Objects.nonNull(projectBenefitIndentsEmatrix.getConfidenceLevel()) && projectBenefitIndentsEmatrix.getConfidenceLevel() > 0 ? projectBenefitIndentsEmatrix.getConfidenceLevel() * 1.0 / 100 : 1;
            Double benifitCost = ((project.getYearlyPotential() / projectBenefitIndentsEmatrix.getTotalProjectCost()) * per);
            DecimalFormat numberFormat = new DecimalFormat("#.00");
            //BigDecimal b = new BigDecimal(benifitCost.);
            //BigDecimal bigDecimal = BigDecimal.valueOf(benifitCost).setScale(2, RoundingMode.HALF_UP);
            projectBenefitIndentsEmatrix.setBc(Double.parseDouble(numberFormat.format(benifitCost)));
            projectBenefitIndentsEmatrix.setUpdatedAt(new Date());
            projectBenefitIndentsEmatrixRepository.save(projectBenefitIndentsEmatrix);
        }
    }

}

class ProjectWorkingState {

    private Integer pWorkingStages = 0;
    private Integer dWorkingStages = 0;
    private Integer cWorkingStages = 0;
    private Integer aWorkingStages = 0;

    public Integer getpWorkingStages() {
        return pWorkingStages;
    }

    public void setpWorkingStages(Integer pWorkingStages) {
        this.pWorkingStages = pWorkingStages;
    }

    public Integer getdWorkingStages() {
        return dWorkingStages;
    }

    public void setdWorkingStages(Integer dWorkingStages) {
        this.dWorkingStages = dWorkingStages;
    }

    public Integer getcWorkingStages() {
        return cWorkingStages;
    }

    public void setcWorkingStages(Integer cWorkingStages) {
        this.cWorkingStages = cWorkingStages;
    }

    public Integer getaWorkingStages() {
        return aWorkingStages;
    }

    public void setaWorkingStages(Integer aWorkingStages) {
        this.aWorkingStages = aWorkingStages;
    }

}
