package com.blueroom.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import com.blueroom.entities.Factorization;
import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectFMatrixSavings;
import com.blueroom.repository.FactorizationRepository;
import com.blueroom.repository.ProjectFMatrixSavingsRepository;
import com.blueroom.repository.ProjectRepository;

@Controller
public class SchedulerServices {
	
	@Autowired
	ProjectFMatrixSavingsRepository projectFMatrixSavingsRepository;
	
	@Autowired
	FactorizationRepository factorizationRepository;
	
	@Autowired
	ProjectRepository projectRepository;  
	
	
	@Scheduled(cron = "0 1 0 * * ?")
	public void updateAllProjectSavingSchedular ( ) {
		System.err.println("Schedular Runing @" + ( new Date() ).toString() );
		List<Project> projectList = projectRepository.findAllInSavingMonitor();
		for (Project project: projectList) {
			updateSavingData(project);
		}
		System.err.println("Schedular End @" + ( new Date() ).toString() );
	}
	
	public void updateSavingData(Project project) {
		if (project.getCompletionDate() == null ) {
			return ;
		}
		DateFormat formater = new SimpleDateFormat("MM");
		DateFormat formater2 = new SimpleDateFormat("yyyy");
		Calendar beginCalendar = Calendar.getInstance();
		beginCalendar.setTime(project.getCompletionDate());
		beginCalendar.add(Calendar.DATE, 1);
		System.out.println("Date :: " + beginCalendar.getTime()  );
		Calendar finishCalendar = Calendar.getInstance();
		finishCalendar.setTime(beginCalendar.getTime());
		finishCalendar.add(Calendar.YEAR, 1);
		System.out.println("Date :: " + finishCalendar.getTime()  );
		if (finishCalendar.after(new Date())) {
			finishCalendar.setTime(new Date());
			finishCalendar.add(Calendar.DATE, -1);
		}
		Integer totalDaysWorking = getdaysBetweenTwoDates(beginCalendar.getTime(), finishCalendar.getTime() );
		List<ProjectFMatrixSavings> projectFMatrixSavingsList = projectFMatrixSavingsRepository.findByProject(project);
		Integer savingPerDay = (int) (project.getYearlyPotential() / 365);
		if (totalDaysWorking > 0) {
			while (beginCalendar.before(finishCalendar)) {
				String month = formater.format(beginCalendar.getTime());
				String year = formater2.format(beginCalendar.getTime());
				ProjectFMatrixSavings projectFMatrixSavings = new ProjectFMatrixSavings();
				for (ProjectFMatrixSavings fMatrixSavings : projectFMatrixSavingsList ) {
					if ( fMatrixSavings.getMonth() == Integer.parseInt(month) && fMatrixSavings.getYear() == Integer.parseInt(month)) {
						projectFMatrixSavings = fMatrixSavings;
						projectFMatrixSavingsList.remove(fMatrixSavings);
						break;
					}
				}
				projectFMatrixSavings.setMonth(Integer.parseInt(month));
				projectFMatrixSavings.setYear(Integer.parseInt(year));

				Factorization factorization = factorizationRepository.findOneByFactorYearAndFactorMonth(projectFMatrixSavings.getYear(), projectFMatrixSavings.getMonth());
				Calendar endOfMonth = Calendar.getInstance();
				endOfMonth.setTime(beginCalendar.getTime());
				endOfMonth.set(Calendar.DAY_OF_MONTH, endOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
				if ( endOfMonth.getTimeInMillis() > (new Date()).getTime() ) {
					if (month.equals(formater.format(new Date())) && year.equals(formater2.format(new Date())) ){
						endOfMonth.setTime(new Date());
					} else {
						endOfMonth.setTime(beginCalendar.getTime());
					}

				}

				Integer monthDaysWorking = getdaysBetweenTwoDates(beginCalendar.getTime(), endOfMonth.getTime() );

				projectFMatrixSavings.setProjectedSavingValue((int) (savingPerDay * monthDaysWorking));
				if (factorization != null && factorization.getFactorValue() != 0 ) {
					projectFMatrixSavings.setActualSavingValue((int)( savingPerDay * monthDaysWorking * factorization.getFactorValue()));	
				}	else {
					projectFMatrixSavings.setActualSavingValue((int) (savingPerDay * monthDaysWorking) );
				}
				projectFMatrixSavings.setProject(project);
				projectFMatrixSavingsRepository.save(projectFMatrixSavings);
				beginCalendar.add(Calendar.MONTH, 1);
				beginCalendar.set(Calendar.DAY_OF_MONTH,beginCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
			}
		}
		for (ProjectFMatrixSavings fMatrixSavings : projectFMatrixSavingsList ) {
			projectFMatrixSavingsRepository.delete(fMatrixSavings);
		}		

	}
	
	public int getdaysBetweenTwoDates (Date date1, Date date2) {
		int daysdiff = 0;
		long diff = date2.getTime() - date1.getTime();
		long diffDays = diff / (24 * 60 * 60 * 1000);
		daysdiff = (int) diffDays;
		return daysdiff;
	}
}
