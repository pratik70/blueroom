package com.blueroom.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.blueroom.entities.Competencies;

import com.blueroom.repository.CompetenciesRepository;

@Component
public class CompetenciesService {

	@Autowired
	CompetenciesRepository competenciesRepository;

	public Competencies addCompetencies(Competencies competencies) {
		return competenciesRepository.save(competencies);
	}

	public List<Competencies> getCompetencies() {
		return competenciesRepository.findAllByStatus(true);
	}

	public Competencies deleteCompetencieUser(Long id) {
		Competencies com = competenciesRepository.findOne(id);
		com.setStatus(false);
		return competenciesRepository.save(com);

	}

	public Competencies getCompetencie(Long id) {
		return competenciesRepository.findOne(id);

	}
	
	public List<Competencies> getCompetencieArea(String type)
	{
		return competenciesRepository.findAllByAreaAndStatus(type, true);
	}

}
