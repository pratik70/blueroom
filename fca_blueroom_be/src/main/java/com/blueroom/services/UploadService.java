package com.blueroom.services;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Objects;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.CDYear;
import com.blueroom.entities.ETU;
import com.blueroom.entities.LayoutImage;
import com.blueroom.entities.Project;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.CDYearRepository;
import com.blueroom.repository.ETURepository;
import com.blueroom.repository.LayoutImageRepository;
import com.blueroom.repository.ProjectRepository;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.vm.MessageVM;
import java.util.List;

@Service
public class UploadService {

    @Value("${layoutPath}")
    private String layoutFolderPath;

    @Value("${documentFilePath}")
    private String documentFilePath;

    @Value("${userFolderPath}")
    private String userFolderPath;

    @Autowired
    LayoutImageRepository layoutImageRepository;

    @Autowired
    CDYearRepository cdYearRepository;

    @Autowired
    ETURepository etuRepository;

    @Autowired
    AuthUserRepository userRepository;

    @Autowired
    ProjectRepository projectRepository;

    public MessageVM uploadLayout(MultipartFile multipartFile, String layoutType, Long cdYear, Long etu) throws Exception {
        if (multipartFile.isEmpty()) {
            throw new Exception("File empty exception!!");
        }
        try {
            File directory = new File(layoutFolderPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            File file = new File(layoutFolderPath + File.separator + multipartFile.getOriginalFilename());
            multipartFile.transferTo(file);
            CDYear year = cdYearRepository.getOne(cdYear);
            if (layoutType != null && layoutType != "" && year != null) {

                LayoutImage layoutImage = new LayoutImage();
                if (etu != null) {
                    ETU etu2 = etuRepository.getOne(etu);
                    layoutImage = layoutImageRepository.findOneByLayoutTypeAndCDYearAndEtu(layoutType, year, etu2);
                    if (layoutImage == null) {
                        layoutImage = new LayoutImage();
                        layoutImage.setEtu(etu2);
                        layoutImage.setCreatedAt(new Date());
                    }

                } else {
                    layoutImage = layoutImageRepository.findOneByLayoutTypeAndCDYear(layoutType, year);
                    if (layoutImage != null) {
                        layoutImage = new LayoutImage();
                    }
                }

                if (layoutImage == null) {
                    layoutImage = new LayoutImage();
                    layoutImage.setCreatedAt(new Date());
                }
                layoutImage.setcDYear(year);
                layoutImage.setLayoutType(layoutType);
                layoutImage.setName(multipartFile.getOriginalFilename());
                layoutImage.setUpdatedAt(new Date());
                layoutImage.setFilePath("/");
                layoutImageRepository.save(layoutImage);
            } else {
                throw new Exception("Something wents wrong.");
            }
        } catch (Exception e) {
            System.err.println("File Not Found Exception!");
            throw new Exception("Something wents wrong.");
        }
        return null;
    }

    public FileSystemResource getLayoutImage(String layoutType) {
        File f;
        try {
            LayoutImage layoutImage = layoutImageRepository.findOneByLayoutType(layoutType);
            if (layoutImage != null) {
                f = new File(layoutFolderPath + File.separator + layoutImage.getName());
                return new FileSystemResource(f);
            }
            return null;
        } catch (Exception e) {
            System.err.println("File Not Found Exception!");
            return null;
        }
    }

    public FileSystemResource getImageLayoutPreview(HttpServletResponse response, String layoutType, Long etuId, Long yearId) {
        File f;
        try {
            LayoutImage layoutImage = null;
            List<LayoutImage> layoutImages = layoutImageRepository.findAllByLayoutTypeAndEtuId(layoutType, etuId);
            if (layoutImages != null && layoutImages.size() > 1) {
                if (yearId != null && yearId != 0) {
                    layoutImages = layoutImageRepository.findAllByLayoutTypeAndCDYearIdAndEtuId(layoutType, yearId, etuId);
                }
            }
            layoutImage = layoutImages == null || layoutImages.isEmpty() ? null : layoutImages.get(0);
            if (layoutImage != null) {
                f = new File(layoutFolderPath + File.separator + layoutImage.getName());
                BufferedImage previewImage = ImageIO.read(f);
                OutputStream out = response.getOutputStream();
                ImageIO.write(previewImage, "jpg", out);
                out.close();
            }
            return null;
        } catch (Exception e) {
            System.err.println("File Not Found Exception!");
            return null;
        }
    }

    public MessageVM uploadDocumnet(MultipartFile multipartFile) throws Exception {
        MessageVM messageVM = new MessageVM();
        if (multipartFile.isEmpty()) {
            throw new Exception("File empty exception!!");
        }
        try {
            File directory = new File(documentFilePath);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            File file = new File(documentFilePath + File.separator + multipartFile.getOriginalFilename());
            multipartFile.transferTo(file);

            messageVM.setData(file.getName());
            messageVM.setCode("200");
            messageVM.setMessage("Document Uploaded Successfully.");
            return messageVM;
        } catch (Exception e) {
            e.printStackTrace();
        }
        messageVM.setCode("400");
        messageVM.setData(null);
        messageVM.setMessage("Document Uploaded Failed.");
        return messageVM;
    }

    public FileSystemResource getImagePreviewByETUAndCdYear(HttpServletResponse response, String layoutType, Long etuId, Long cdId) {
        File f;
        try {
            CDYear year = cdYearRepository.getOne(cdId);
            ETU etu = etuRepository.getOne(etuId);
            if (Objects.nonNull(etu) && Objects.nonNull(year) && Objects.nonNull(layoutType)) {
                LayoutImage layoutImage = layoutImageRepository.findOneByLayoutTypeAndCDYearAndEtu(layoutType, year, etu);
                if (layoutImage != null) {
                    f = new File(layoutFolderPath + File.separator + layoutImage.getName());
                    BufferedImage previewImage = ImageIO.read(f);
                    OutputStream out = response.getOutputStream();
                    ImageIO.write(previewImage, "jpg", out);
                    out.close();
                }
            }
            return null;
        } catch (Exception e) {
            System.err.println("Can't read input file!");
            return null;
        }
    }

    public FileSystemResource getUserImage(HttpServletResponse response, Long userId) {
        File f;
        try {
            AuthUser user = userRepository.findOne(userId);
            String profile = userRepository.getProfileImage(userId);
            if (Objects.nonNull(user)) {
                if (profile != null) {
                    f = new File(userFolderPath + File.separator + profile);
                    BufferedImage previewImage = ImageIO.read(f);
                    OutputStream out = response.getOutputStream();
                    ImageIO.write(previewImage, "jpg", out);
                    out.close();
                }
            }
            return null;
        } catch (Exception e) {
            System.err.println("File Not Found Exception!");
            return null;
        }
    }

    public FileSystemResource getProjectKaizenDoc(HttpServletResponse response, Long projectId) {
        File f;
        try {
            Project project = projectRepository.getOne(projectId);
            if (Objects.nonNull(project)) {
                if (project.getDocument() != null) {
                    f = new File(documentFilePath + File.separator + project.getDocument());
                    InputStream is = new FileInputStream(f.getPath());
                    FileCopyUtils.copy(is, response.getOutputStream());
                    response.flushBuffer();
                }
            }
            return null;
        } catch (Exception e) {
            System.err.println("File Not Found Exception!");
            return null;
        }

    }

    public FileSystemResource getProjectEntireWorkDoc(HttpServletResponse response, Long projectId) {
        File f;
        try {
            Project project = projectRepository.getOne(projectId);
            if (Objects.nonNull(project)) {
                if (project.getEwDocument() != null) {
                    f = new File(documentFilePath + File.separator + project.getEwDocument());
                    InputStream is = new FileInputStream(f.getPath());
                    FileCopyUtils.copy(is, response.getOutputStream());
                    response.flushBuffer();
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
