/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.services.masterdata;

import com.blueroom.entities.masterdata.MasterData;
import com.blueroom.repository.masterdata.MasterDataRepository;
import com.blueroom.vm.MessageVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author govind
 */
@Service
public class MasterDataService {

    @Autowired
    MasterDataRepository masterDataRepository;

    public MessageVM getMasterDataByETU(Long etuID) {
        MessageVM result = new MessageVM();
        result.setData(masterDataRepository.findByEtuId(etuID));
        result.setCode("200");
        return result;
    }

    public MessageVM findById(Long masterDataId) {
        MessageVM result = new MessageVM();
        result.setData(masterDataRepository.findOne(masterDataId));
        result.setCode("200");
        return result;
    }

    public MessageVM saveMasterData(MasterData masterData) {
        MessageVM result = new MessageVM();
        result.setData(masterDataRepository.save(masterData));
        result.setCode("200");
        return result;
    }

    public MasterData findByEtuId(Long etuId) {
        return masterDataRepository.findByEtuId(etuId);
    }

}
