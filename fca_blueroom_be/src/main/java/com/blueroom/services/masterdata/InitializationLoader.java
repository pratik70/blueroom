/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.services.masterdata;

import com.blueroom.entities.masterdata.DailyBasisLossType;
import com.blueroom.repository.masterdata.DailyBasisLossTypeRepository;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author govind
 */
@Component
public class InitializationLoader {

    @Autowired
    DailyBasisLossTypeRepository dailyBasisLossTypeRepository;

    @PostConstruct
    public void run() {
        if (dailyBasisLossTypeRepository.count() < 1) {
            loadDailyBasisLossTypes();
        }
    }

    private void loadDailyBasisLossTypes() {
        String[] result = getDailyBasisLossType();
        for (String dailyBasisLossName : result) {
            DailyBasisLossType dailyBasisLossType = new DailyBasisLossType();
            dailyBasisLossType.setLossName(dailyBasisLossName);
            dailyBasisLossTypeRepository.save(dailyBasisLossType);
        }
    }

    private String[] getDailyBasisLossType() {
        String[] dailyBasisLossType = {"Breakdown", "Microstoppages", "stand still", "Tool change", "Rework", "Hydulic/Chemical Over sumption", "Tool Over consumption", "startup-shutdown", "machine scrap", "material scrap", "Quality mDI", "Type Change"};
        return dailyBasisLossType;
    }
}
