/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.services.masterdata;

import com.blueroom.entities.masterdata.DailyBasisLossType;
import com.blueroom.entities.masterdata.MachineData;
import com.blueroom.repository.masterdata.MachineDataRepository;
import com.blueroom.vm.MessageVM;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author govind
 */
@Service
public class MachineDataService {

    @Autowired
    MachineDataRepository machineDataRepository;

    public MessageVM getMachineDataByEtu(Long etuId) {
        MessageVM result = new MessageVM();
        result.setData(machineDataRepository.findByEtuId(etuId));
        result.setCode("200");
        return result;
    }

    public MessageVM saveMachineData(List<MachineData> machineData) {
        MessageVM result = new MessageVM();
        result.setData(machineDataRepository.save(machineData));
        result.setCode("200");
        return result;
    }

    public MessageVM findById(Long machineDataId) {
        MessageVM result = new MessageVM();
        result.setData(machineDataRepository.findOne(machineDataId));
        result.setCode("200");
        return result;
    }

    public List<MachineData> getMachineDataByEtuId(Long etuId) {
        return machineDataRepository.findByEtuId(etuId);
    }
}
