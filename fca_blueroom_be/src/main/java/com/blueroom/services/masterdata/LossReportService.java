/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.services.masterdata;

import com.blueroom.entities.masterdata.DailyBasisLoss;
import com.blueroom.entities.masterdata.DailyBasisLossType;
import com.blueroom.entities.masterdata.LossReportVM;
import com.blueroom.entities.masterdata.MachineData;
import com.blueroom.entities.masterdata.MasterData;
import com.blueroom.repository.masterdata.DailyBasisLossTypeRepository;
import com.blueroom.vm.MessageVM;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author govind
 */
@Component
public class LossReportService {

    @Autowired
    DailyBasisLossService dailyBasisLossService;

    @Autowired
    MachineDataService machineDataService;

    @Autowired
    MasterDataService masterDataService;

    @Autowired
    DailyBasisLossTypeRepository dailyBasisLossTypeRepository;

    public MessageVM getLossReport(Long etuId) {
        MessageVM result = new MessageVM();

        List<LossReportVM> reportLossList = new ArrayList<>();
        List<DailyBasisLossType> lossTypes = dailyBasisLossTypeRepository.findAll();
        MasterData masterData = masterDataService.findByEtuId(etuId);
        List<MachineData> machineData = machineDataService.getMachineDataByEtuId(etuId);
//        Double D6 = 0.0;
//        Long E6 = 0L;
//        Long F6 = 0L;
//        if (machineData.size() > 0) {
        Double D6 = machineData.stream().mapToDouble(mapper -> mapper.getFactor()).sum();
        Long E6 = machineData.stream().mapToLong(mapper -> mapper.getNoOfOperator()).sum();
        Long F6 = machineData.stream().mapToLong(mapper -> mapper.getNoOfKWh()).sum();
//        }
        lossTypes.forEach(lossType -> {
            LossReportVM reportVM = new LossReportVM();
            reportVM.setDailyBasisLossType(lossType);
            Map<String, Object> casualLossResult = new HashMap<>();
            List<DailyBasisLoss> dailyLoss = dailyBasisLossService.findByEtuIdAndDailyBasisLossType(etuId, lossType);
            DailyBasisLoss dl = dailyLoss.size() > 0 ? dailyLoss.get(0) : null;

            Double B4 = 0.0, C4 = 0.0, D4 = 0.0, E4 = 0.0, F4 = 0.0, H4 = 0.0, I4 = 0.0, J4 = 0.0, K4 = 0.0, M4 = 0.0;
            Long F18 = 0L, G18 = 0L, H18 = 0L, I18 = 0L, K18 = 0L, G4 = 0L, L4 = 0L;
            if (Objects.nonNull(masterData)) {
                B4 = masterData.getDirectLabourCost();
                B4 = B4 != null ? B4 : 0.0;
                C4 = masterData.getIndirectLabourCost();
                C4 = C4 != null ? C4 : 0.0;
                D4 = masterData.getDirectLabourCost();
                D4 = D4 != null ? D4 : 0.0;
                E4 = masterData.getEnergyCost();
                E4 = E4 != null ? E4 : 0.0;
                F4 = masterData.getEnergyFactor();
                F4 = F4 != null ? F4 : 0.0;
                G4 = masterData.getNoOfLabour();
                G4 = G4 != null ? G4 : 0L;
                H4 = masterData.getTotalConnecedLoad();
                H4 = H4 != null ? H4 : 0.0;
                I4 = masterData.getCycleTime();
                I4 = I4 != null ? I4 : 0.0;
                J4 = masterData.getMachineScrap();
                J4 = J4 != null ? J4 : 0.0;
                K4 = masterData.getMaterialScrap();
                K4 = K4 != null ? K4 : 0.0;
                L4 = masterData.getNoOfWhiteCollar();
                L4 = L4 != null ? L4 : 0L;
                M4 = masterData.getToolCostPerPart();
                M4 = M4 != null ? M4 : 0.0;
            }

            if (Objects.nonNull(dl)) {
                F18 = dl.getLossInMinutes();
                F18 = F18 != null ? F18 : 0L;
                G18 = dl.getScrapQuantitySameOP();
                G18 = G18 != null ? G18 : 0L;
                H18 = dl.getIndirectMaterialCost();
                H18 = H18 != null ? H18 : 0L;
                I18 = dl.getMaintenanceMaterialCost();
                I18 = I18 != null ? I18 : 0L;
                K18 = dl.getScrapQuantityOtherOP();
                K18 = K18 != null ? K18 : 0L;
            }

            switch (lossType.getLossName()) {
                case "stand still":
                    casualLossResult.put("directLabourCost", F18 / 60 * B4 * G4);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", F18 / 60 * D4 * L4);
                    casualLossResult.put("indirectMaterialCost", null);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", null);
                    casualLossResult.put("energyCost", null);
                    casualLossResult.put("other", null);
                    break;
                case "Quality mDI":
                    casualLossResult.put("directLabourCost", F18 / 60 * D4 * L4);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", F18 / 60 * B4 * 2);
                    casualLossResult.put("indirectMaterialCost", null);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", null);
                    casualLossResult.put("energyCost", null);
                    casualLossResult.put("other", null);
                    break;
                case "Breakdown":
                    casualLossResult.put("directLabourCost", F18 / 60 * B4 * E6 * D6);
                    casualLossResult.put("indirectLabourCost", F18 / 60 * C4 * 2);
                    casualLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", H18);
                    casualLossResult.put("maintenanceMaterialCost", I18);
                    casualLossResult.put("scrapCost", G18 * J4);
                    casualLossResult.put("energyCost", F18 / 60 * E4 * F4 * F6);
                    casualLossResult.put("other", null);
                    break;
                case "Microstoppages":
                    casualLossResult.put("directLabourCost", F18 / 60 * B4 * E6 * D6);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", H18);
                    casualLossResult.put("maintenanceMaterialCost", I18);
                    casualLossResult.put("scrapCost", G18 * J4);
                    casualLossResult.put("energyCost", F18 / 60 * E4 * F4 * F6);
                    casualLossResult.put("other", null);
                    break;
                case "Tool change":
                    casualLossResult.put("directLabourCost", F18 / 60 * B4 * E6 * D6);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", H18);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", G18 * J4);
                    casualLossResult.put("energyCost", F18 / 60 * E4 * F4 * F6);
                    casualLossResult.put("other", null);
                    break;
                case "startup-shutdown":
                    casualLossResult.put("directLabourCost", F18 / 60 * B4 * E6 * D6);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", null);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", null);
                    casualLossResult.put("energyCost", F18 / 60 * E4 * F4 * F6);
                    casualLossResult.put("other", null);
                    break;
                case "machine scrap":
                    casualLossResult.put("directLabourCost", K18 * I4 / 60 * B4 * E6);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", K18 * 20 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", K18 * M4);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", K18 * J4);
                    casualLossResult.put("energyCost", K18 * I4 / 60 * F6 * E4 * F4);
                    casualLossResult.put("other", null);
                    break;

                case "material scrap":
                    casualLossResult.put("directLabourCost", K18 * I4 / 60 * B4 * E6);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", K18 * 15 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", K18 * M4);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", K18 * K4);
                    casualLossResult.put("energyCost", K18 * I4 / 60 * F6 * E4 * F4);
                    casualLossResult.put("other", null);
                    break;
                case "Type Change":
                    casualLossResult.put("directLabourCost", F18 / 60 * B4 * E6 * D6);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", H18);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", G18 * J4);
                    casualLossResult.put("energyCost", F18 / 60 * E4 * F4 * F6);
                    casualLossResult.put("other", null);
                    break;
                case "Tool Over consumption":
                    casualLossResult.put("directLabourCost", F18 / 60 * B4 * E6 * D6);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", H18);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", null);
                    casualLossResult.put("energyCost", F18 / 60 * E4 * F4 * F6);
                    casualLossResult.put("other", null);
                    break;
                case "Rework":
                    casualLossResult.put("directLabourCost", K18 * I4 / 60 * B4 * E6);
                    casualLossResult.put("indirectLabourCost", null);
                    casualLossResult.put("salariedCost", K18 * 20 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", K18 * M4);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", null);
                    casualLossResult.put("energyCost", K18 * I4 / 60 * F6 * E4 * F4);
                    casualLossResult.put("other", null);
                    break;
                case "Hydulic/Chemical Over sumption":
                    casualLossResult.put("directLabourCost", F18 / 60 * B4 * E6 * D6);
                    casualLossResult.put("indirectLabourCost", F18 / 60 * C4 * 1);
                    casualLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    casualLossResult.put("indirectMaterialCost", H18);
                    casualLossResult.put("maintenanceMaterialCost", null);
                    casualLossResult.put("scrapCost", G18 * J4);
                    casualLossResult.put("energyCost", F18 / 60 * E4 * F4 * F6);
                    casualLossResult.put("other", null);
                    break;
            }

            Map<String, Object> resultantLossResult = new HashMap<>();
            switch (lossType.getLossName()) {
                case "stand still":
                case "Quality mDI":
                    resultantLossResult.put("directLabourCost", null);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", null);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", null);
                    resultantLossResult.put("energyCost", null);
                    resultantLossResult.put("other", null);
                    break;
                case "Breakdown":
                    resultantLossResult.put("directLabourCost", F18 / 60 * B4 * G4 * D6);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", G18 * J4);
                    resultantLossResult.put("energyCost", F18 / 60 * E4 * F4 * H4);
                    resultantLossResult.put("other", null);
                    break;
                case "Microstoppages":
                    resultantLossResult.put("directLabourCost", F18 / 60 * B4 * G4 * D6);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", G18 * J4);
                    resultantLossResult.put("energyCost", F18 / 60 * E4 * F4 * H4);
                    resultantLossResult.put("other", null);
                    break;
                case "Tool change":
                    resultantLossResult.put("directLabourCost", F18 / 60 * B4 * G4 * D6);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", G18 * J4);
                    resultantLossResult.put("energyCost", F18 / 60 * E4 * F4 * H4);
                    resultantLossResult.put("other", null);
                    break;
                case "startup-shutdown":
                    resultantLossResult.put("directLabourCost", F18 / 60 * B4 * G4 * D6);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", null);
                    resultantLossResult.put("energyCost", F18 / 60 * E4 * F4 * H4);
                    resultantLossResult.put("other", null);
                    break;
                case "machine scrap":
                    resultantLossResult.put("directLabourCost", K18 * I4 / 60 * B4 * G4);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", K18 * 20 / 60 * D4 * 3);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", K18 * J4);
                    resultantLossResult.put("energyCost", K18 * I4 / 60 * H4 * E4 * F4);
                    resultantLossResult.put("other", null);
                    break;
                case "material scrap":
                    resultantLossResult.put("directLabourCost", K18 * I4 / 60 * B4 * G4);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", K18 * 15 / 60 * D4 * 3);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", K18 * K4);
                    resultantLossResult.put("energyCost", K18 * I4 / 60 * H4 * E4 * F4);
                    resultantLossResult.put("other", null);
                    break;
                case "Type Change":
                    resultantLossResult.put("directLabourCost", F18 / 60 * B4 * G4 * D6);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", G18 * J4);
                    resultantLossResult.put("energyCost", F18 / 60 * E4 * F4 * H4);
                    resultantLossResult.put("other", null);
                    break;

                case "Tool Over consumption":
                    resultantLossResult.put("directLabourCost", F18 / 60 * B4 * G4 * D6);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", null);
                    resultantLossResult.put("energyCost", F18 / 60 * E4 * F4 * H4);
                    resultantLossResult.put("other", null);
                    break;
                case "Rework":
                    resultantLossResult.put("directLabourCost", K18 * I4 / 60 * B4 * G4);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", K18 * 20 / 60 * D4 * 3);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", null);
                    resultantLossResult.put("energyCost", K18 * I4 / 60 * H4 * E4 * F4);
                    resultantLossResult.put("other", null);
                    break;
                case "Hydulic/Chemical Over sumption":
                    resultantLossResult.put("directLabourCost", F18 / 60 * B4 * G4 * D6);
                    resultantLossResult.put("indirectLabourCost", null);
                    resultantLossResult.put("salariedCost", F18 / 60 * D4 * 1);
                    resultantLossResult.put("indirectMaterialCost", null);
                    resultantLossResult.put("maintenanceMaterialCost", null);
                    resultantLossResult.put("machineScrap", null);
                    resultantLossResult.put("energyCost", F18 / 60 * E4 * F4 * H4);
                    resultantLossResult.put("other", null);
                    break;

            }
            Double total = 0.0;
            for (Entry<String, Object> entry : casualLossResult.entrySet()) {
                if (Objects.nonNull(entry.getValue())) {
                    Object s = (Object) entry.getValue();
                    if (s instanceof Long) {
                        total += ((Long) s).doubleValue();
                    } else {
                        total += (Double) s;
                    }
                }
            }
            for (Entry<String, Object> entry : resultantLossResult.entrySet()) {
                if (Objects.nonNull(entry.getValue())) {
                    Object s = (Object) entry.getValue();
                    if (s instanceof Long) {
                        total += ((Long) s).doubleValue();
                    } else {
                        total += (Double) s;
                    }
                }
            }
            reportVM.setTotal(total);
            reportVM.setCausulLoss(casualLossResult);
            reportVM.setResultantLoss(resultantLossResult);
            reportLossList.add(reportVM);
        });

        result.setData(reportLossList);
        return result;
    }

}
