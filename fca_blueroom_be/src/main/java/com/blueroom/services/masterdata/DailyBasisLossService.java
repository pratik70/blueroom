/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.services.masterdata;

import com.blueroom.controllers.masterdata.GridFilter;
import com.blueroom.entities.ETU;
import com.blueroom.entities.masterdata.DailyBasisLoss;
import com.blueroom.entities.masterdata.DailyBasisLossType;
import com.blueroom.repository.masterdata.DailyBasisLossRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author govind
 */
@Service
public class DailyBasisLossService {

    @Autowired
    DailyBasisLossRepository dailyBasisLossRepository;

    public List<DailyBasisLoss> findByEtuId(Long etuId) {
        return dailyBasisLossRepository.findByEtuId(etuId);
    }

    public DailyBasisLoss findById(Long dailyBasisLossId) {
        return dailyBasisLossRepository.findOne(dailyBasisLossId);
    }

    public DailyBasisLoss save(DailyBasisLoss dailyBasisLoss) {
        return dailyBasisLossRepository.save(dailyBasisLoss);
    }

    public List<DailyBasisLoss> getAllListByFilter(GridFilter gridFilter) {
        ETU etu = gridFilter.getEtu();
        if (Objects.nonNull(etu)) {
            List<DailyBasisLossType> lossTypes = gridFilter.getDailyBasisLossTypes();
            if (lossTypes.size() > 0 && Objects.isNull(gridFilter.getStartDate()) && Objects.isNull(gridFilter.getEndDate())) {
                return dailyBasisLossRepository.findByEtuAndDailyBasisLossTypeIn(etu, lossTypes);
            } else if (lossTypes.size() > 0 && Objects.nonNull(gridFilter.getStartDate()) && Objects.isNull(gridFilter.getEndDate())) {
                return dailyBasisLossRepository.findByEtuAndDailyBasisLossTypeInAndLossDateGreaterThanEqual(etu, lossTypes, gridFilter.getEndDate());
            } else if (lossTypes.size() > 0 && Objects.isNull(gridFilter.getStartDate()) && Objects.nonNull(gridFilter.getEndDate())) {
                return dailyBasisLossRepository.findByEtuAndDailyBasisLossTypeInAndLossDateLessThanEqual(etu, lossTypes, gridFilter.getEndDate());
            } else if (lossTypes.size() > 0 && Objects.nonNull(gridFilter.getStartDate()) && Objects.nonNull(gridFilter.getEndDate())) {
                return dailyBasisLossRepository.findByEtuAndDailyBasisLossTypeInAndLossDateGreaterThanEqualAndLossDateLessThanEqual(etu, lossTypes, gridFilter.getStartDate(), gridFilter.getEndDate());
            } else if (Objects.nonNull(gridFilter.getStartDate()) && Objects.nonNull(gridFilter.getEndDate())) {
                return dailyBasisLossRepository.findByEtuAndLossDateGreaterThanEqualAndLossDateLessThanEqual(etu, gridFilter.getStartDate(), gridFilter.getEndDate());
            } else if (Objects.nonNull(gridFilter.getStartDate()) && Objects.isNull(gridFilter.getEndDate())) {
                return dailyBasisLossRepository.findByEtuAndLossDateGreaterThanEqual(etu, gridFilter.getStartDate());
            } else if (Objects.isNull(gridFilter.getStartDate()) && Objects.nonNull(gridFilter.getEndDate())) {
                return dailyBasisLossRepository.findByEtuAndLossDateLessThanEqual(etu, gridFilter.getEndDate());
            }
            return dailyBasisLossRepository.findByEtuId(etu.getId());
        }
        return new ArrayList<>();
    }

    public List<DailyBasisLoss> findByEtuIdAndDailyBasisLossType(Long etuId, DailyBasisLossType lossType) {
        return dailyBasisLossRepository.findByEtuIdAndDailyBasisLossType(etuId, lossType);
    }

}
