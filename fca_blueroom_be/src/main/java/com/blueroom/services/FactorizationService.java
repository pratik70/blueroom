package com.blueroom.services;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.ETU;
import com.blueroom.entities.Factorization;
import com.blueroom.entities.FactorizationProjectSaving;
import com.blueroom.entities.FactorizationSavingProject;
import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectFMatrixSavings;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.ETURepository;
import com.blueroom.repository.FactorizationProjectSavingRepository;
import com.blueroom.repository.FactorizationRepository;
import com.blueroom.repository.FactorizationSavingProjectRepository;
import com.blueroom.repository.ProjectFMatrixSavingsRepository;
import com.blueroom.repository.ProjectRepository;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.vm.FactorizationProjectSavingVM;
import com.blueroom.vm.FactorizationSavingAndProjectVM;
import com.blueroom.vm.FactorizationVM;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;
import com.blueroom.vm.UserProjectSavingVM;
import com.blueroom.vm.UserVM;
import java.util.stream.Collectors;

@Service
public class FactorizationService {

    @Autowired
    FactorizationRepository factorizationRepository;

    @Autowired
    ETURepository etuRepository;

    @Autowired
    AuthUserRepository userRepository;

    @Autowired
    FactorizationSavingProjectRepository factorizationSavingProjectRepository;

    @Autowired
    ProjectFMatrixSavingsRepository projectFMatrixSavingsRepository;

    @Autowired
    FactorizationProjectSavingRepository factorizationProjectSavingRepository;

    @Autowired
    ProjectRepository projectRepository;

    public MessageVM addFactorizationData(Integer factorYear, List<FactorizationVM> factorList) {
        MessageVM messageVM = new MessageVM();
        ETU etu = null;
        for (FactorizationVM factorizationVM : factorList) {
            etu = etuRepository.findOne(factorizationVM.getEtuId());
            if (etu != null) {
                Factorization factorization = factorizationRepository.findOneByFactorYearAndFactorMonth(factorYear, factorizationVM.getFactorMonth());
                if (factorization == null) {
                    factorization = new Factorization();
                    factorization.setCreatedAt(new Date());
                }
                factorization.setEtu(etu);
                factorization.setActualValue(factorizationVM.getActualValue());
                factorization.setPlanValue(factorizationVM.getPlanValue());
                factorization.setFactorValue(factorizationVM.getFactorValue());
                factorization.setFactorYear(factorYear);
                factorization.setFactorMonth(factorizationVM.getFactorMonth());
                factorization.setUpdatedAt(new Date());
                factorizationRepository.save(factorization);
            }
        }

        if (etu != null) {
            List<Integer> yearList = new ArrayList<>();
            yearList.add(factorYear - 1);
            yearList.add(factorYear);
            List<Project> projectList = projectRepository.findAllInEtuAndFactorYear(etu.getId(), yearList);
            for (Project project : projectList) {
                updateSavingData(project);
            }
        }
        messageVM.setCode("200");
        return messageVM;
    }

    public void updateSavingData(Project project) {
        if (project.getCompletionDate() == null) {
            return;
        }
        DateFormat formater = new SimpleDateFormat("MM");
        DateFormat formater2 = new SimpleDateFormat("yyyy");
        Calendar beginCalendar = Calendar.getInstance();
        beginCalendar.setTime(project.getCompletionDate());
        beginCalendar.add(Calendar.DATE, 1);
        //  System.out.println("Date :: " + beginCalendar.getTime());
        Calendar finishCalendar = Calendar.getInstance();
        finishCalendar.setTime(beginCalendar.getTime());
        finishCalendar.add(Calendar.YEAR, 1);
        // System.out.println("Date :: " + finishCalendar.getTime());
        if (finishCalendar.after(new Date())) {
            finishCalendar.setTime(new Date());
            finishCalendar.add(Calendar.DATE, -1);
        }
        Integer totalDaysWorking = getdaysBetweenTwoDates(beginCalendar.getTime(), finishCalendar.getTime());
        List<ProjectFMatrixSavings> projectFMatrixSavingsList = projectFMatrixSavingsRepository.findByProject(project);
        Integer savingPerDay = (int) (project.getYearlyPotential() / 365);
        if (totalDaysWorking > 0) {
            while (beginCalendar.before(finishCalendar)) {
                String month = formater.format(beginCalendar.getTime());
                String year = formater2.format(beginCalendar.getTime());
                ProjectFMatrixSavings projectFMatrixSavings = new ProjectFMatrixSavings();
                for (ProjectFMatrixSavings fMatrixSavings : projectFMatrixSavingsList) {
                    if (fMatrixSavings.getMonth() == Integer.parseInt(month) && fMatrixSavings.getYear() == Integer.parseInt(month)) {
                        projectFMatrixSavings = fMatrixSavings;
                        projectFMatrixSavingsList.remove(fMatrixSavings);
                        break;
                    }
                }
                projectFMatrixSavings.setMonth(Integer.parseInt(month));
                projectFMatrixSavings.setYear(Integer.parseInt(year));

                Factorization factorization = factorizationRepository.findOneByFactorYearAndFactorMonth(projectFMatrixSavings.getYear(), projectFMatrixSavings.getMonth());
                Calendar endOfMonth = Calendar.getInstance();
                endOfMonth.setTime(beginCalendar.getTime());
                endOfMonth.set(Calendar.DAY_OF_MONTH, endOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
                if (endOfMonth.getTimeInMillis() > (new Date()).getTime()) {
                    if (month.equals(formater.format(new Date())) && year.equals(formater2.format(new Date()))) {
                        endOfMonth.setTime(new Date());
                    } else {
                        endOfMonth.setTime(beginCalendar.getTime());
                    }

                }

                Integer monthDaysWorking = getdaysBetweenTwoDates(beginCalendar.getTime(), endOfMonth.getTime());

                projectFMatrixSavings.setProjectedSavingValue((int) (savingPerDay * monthDaysWorking));
                if (factorization != null && factorization.getFactorValue() != 0) {
                    projectFMatrixSavings.setActualSavingValue((int) (savingPerDay * monthDaysWorking * factorization.getFactorValue()));
                } else {
                    projectFMatrixSavings.setActualSavingValue((int) (savingPerDay * monthDaysWorking));
                }
                projectFMatrixSavings.setProject(project);
                projectFMatrixSavingsRepository.save(projectFMatrixSavings);
                beginCalendar.add(Calendar.MONTH, 1);
                beginCalendar.set(Calendar.DAY_OF_MONTH, beginCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            }
        }
        for (ProjectFMatrixSavings fMatrixSavings : projectFMatrixSavingsList) {
            projectFMatrixSavingsRepository.delete(fMatrixSavings);
        }

    }

    public int getdaysBetweenTwoDates(Date date1, Date date2) {
        int daysdiff = 0;
        long diff = date2.getTime() - date1.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        daysdiff = (int) diffDays;
        return daysdiff;
    }

    public MessageVM getFactorizationData(Integer factorYear, Long etuId) {
        MessageVM messageVM = new MessageVM();
        List<FactorizationVM> factorizationVMList = new ArrayList<>();
        ETU etu = etuRepository.findOne(etuId);
        if (etu != null) {
            List<Factorization> factorizationList = factorizationRepository.findAllByFactorYearAndEtu(factorYear, etu);
            for (Factorization factorization : factorizationList) {
                FactorizationVM factorizationVM = new FactorizationVM();
                factorizationVM.setId(factorization.getId());
                factorizationVM.setActualValue(factorization.getActualValue());
                factorizationVM.setPlanValue(factorization.getPlanValue());
                factorizationVM.setFactorValue(factorization.getFactorValue());
                factorizationVM.setFactorYear(factorization.getFactorYear());
                factorizationVM.setFactorMonth(factorization.getFactorMonth());
                factorizationVM.setCreatedAt(factorization.getCreatedAt());
                factorizationVM.setUpdatedAt(factorization.getUpdatedAt());
                if (factorization.getEtu() != null) {
                    factorizationVM.setEtuId(factorization.getEtu().getId());
                }
                factorizationVMList.add(factorizationVM);
            }
        }

        messageVM.setData(factorizationVMList);
        messageVM.setCode("200");
        return messageVM;
    }

    public MessageVM getUserSavingsWithProject(Integer factorYear, MatrixFilterVM filterVM) {
        MessageVM messageVM = new MessageVM();
        List<AuthUser> userList = new ArrayList<>();
        List<BigInteger> userids = new ArrayList();
        List<Long> useridstoretrive = new ArrayList();
        if (filterVM.getEtuList().isEmpty() || filterVM.getEtuList().size() < 1) {
            userList = userRepository.findAllByStatus("Active");
        } else {
            List<Long> etuIdss = filterVM.getEtuList().stream().map(mapper -> mapper.getId()).collect(Collectors.toList());
            for (Long etuIds : etuIdss) {
                userids = etuRepository.findUserIdsByETUId(etuIds);
            }
            for (BigInteger b : userids) {
                Long userid = b.longValue();
                useridstoretrive.add(userid);
            }

            userList = userRepository.findAllByIdIn(useridstoretrive);
            //userList = userRepository.findAllByEtuLevel(etuIds);
        }

        HashMap<Long, UserProjectSavingVM> userHashMap = new HashMap<>();
        for (AuthUser user : userList) {
            UserVM userVM = new UserVM();
            userVM.setId(user.getId());
            userVM.setEmployeeId(user.getEmployeeId());
            userVM.setName(user.getName());
            userVM.setWorkingProject(0);
            userVM.setProfileImage(user.getProfileImg());
            UserProjectSavingVM savingVM = new UserProjectSavingVM();
            savingVM.setUser(userVM);
            savingVM.setUserId(user.getId());
            savingVM.setProjectSavings(0);
            savingVM.setTeamMemberSize(0);
            savingVM.setCompleteNoProject(0);
            userHashMap.put(user.getId(), savingVM);
        }
        List<Object[]> projectCountWorkingAsTL = factorizationRepository.findProjectCountWorkingAsTL();
        for (Object[] a : projectCountWorkingAsTL) {
            if (userHashMap.get(Long.parseLong(a[0] + "")) != null) {
                UserProjectSavingVM projectSavingVM = userHashMap.get(Long.parseLong(a[0] + ""));
                UserVM userVM = projectSavingVM.getUser();
                userVM.setWorkingProject(Integer.parseInt(a[1] + ""));
                projectSavingVM.setUser(userVM);
                userHashMap.put(Long.parseLong(a[0] + ""), projectSavingVM);
            }
        }
        /*List<Object[]> dataList  = factorizationRepository.findAllUserWithProjectIdAndSaving(factorYear);
		for (Object[] a : dataList) {
			if (userHashMap.get(Long.parseLong(a[0]+"")) != null) {
				UserProjectSavingVM savingVM = userHashMap.get(Long.parseLong(a[0]+""));
				savingVM.setUserId(Long.parseLong(a[0]+""));
				savingVM.setProjectId(Long.parseLong(a[1]+""));
				savingVM.setProjectSavings((int)Double.parseDouble(a[2]+""));
				savingVM.setCompleteNoProject(savingVM.getCompleteNoProject()+ 1 );
				savingVM.setTeamMemberSize(Integer.parseInt(a[3]+""));
				savingVM.setProjectSavingInPercentage((int)Double.parseDouble(a[4]+""));
				userHashMap.put(Long.parseLong(a[0]+""), savingVM);
			}
			
		} [ 41, 737, 583725.0, 5, 233490.0, 40% ] */
        List<Object[]> dataList = factorizationRepository.findAllUserWithProjectIdAndSaving(factorYear);
        for (Object[] a : dataList) {
            if (userHashMap.get(Long.parseLong(a[0] + "")) != null) {
                UserProjectSavingVM savingVM = userHashMap.get(Long.parseLong(a[0] + ""));

                savingVM.setUserId(Long.parseLong(a[0] + ""));
                savingVM.setProjectId(Long.parseLong(a[2] + ""));
                if (Long.parseLong(a[1] + "") == 1l) {
                    savingVM.setCompleteNoProject(savingVM.getCompleteNoProject() + 1);
                }
                savingVM.setProjectSavings(savingVM.getProjectSavings() + (int) Double.parseDouble(a[5] + ""));

                //savingVM.setTeamMemberSize(Integer.parseInt(a[3]+""));
                //savingVM.setProjectSavingInPercentage((int)Double.parseDouble(a[4]+""));
                userHashMap.put(Long.parseLong(a[0] + ""), savingVM);
            }

        }
        messageVM.setData(userHashMap);
        messageVM.setCode("200");
        return messageVM;
    }

    public MessageVM addUserSavingsAndProjectFactors(Integer factorYear, List<FactorizationSavingAndProjectVM> factorizationSavingAndProjectList) {
        MessageVM messageVM = new MessageVM();
        for (FactorizationSavingAndProjectVM factorizationSavingAndProjectVM : factorizationSavingAndProjectList) {
            AuthUser user = userRepository.findOne(factorizationSavingAndProjectVM.getUserId());
            if (user != null) {
                List<FactorizationSavingProject> factorizationSavingProjectList = factorizationSavingProjectRepository.findAllByUserAndFactorYear(user, factorYear);
                FactorizationSavingProject factorizationSavingProject = new FactorizationSavingProject();
                if (!factorizationSavingProjectList.isEmpty()) {
                    factorizationSavingProject = factorizationSavingProjectList.get(0);
                }
                factorizationSavingProject.setUser(user);
                factorizationSavingProject.setProjectFactor(factorizationSavingAndProjectVM.getProjectFactor());
                factorizationSavingProject.setSavingFactor(factorizationSavingAndProjectVM.getSavingFactor());
                factorizationSavingProject.setFactorYear(factorYear);
                factorizationSavingProjectRepository.save(factorizationSavingProject);
            }
        }
        messageVM.setMessage("Factor saving and project data successfully saved.");
        return messageVM;
    }

    public MessageVM getUserSavingsAndProjectFactors(Integer factorYear) {
        MessageVM messageVM = new MessageVM();
        List<FactorizationSavingAndProjectVM> factorizationSavingAndProjectList = new ArrayList<>();
        List<FactorizationSavingProject> factorizationSavingProjectList = factorizationSavingProjectRepository.findAllByFactorYear(factorYear);
        for (FactorizationSavingProject factorizationSavingProject : factorizationSavingProjectList) {
            FactorizationSavingAndProjectVM factorizationSavingAndProjectVM = new FactorizationSavingAndProjectVM();
            factorizationSavingAndProjectVM.setUserId(factorizationSavingProject.getUser().getId());
            factorizationSavingAndProjectVM.setEmployeeId(factorizationSavingProject.getUser().getEmployeeId());
            factorizationSavingAndProjectVM.setSavingFactor(factorizationSavingProject.getSavingFactor());
            factorizationSavingAndProjectVM.setProjectFactor(factorizationSavingProject.getProjectFactor());
            factorizationSavingAndProjectVM.setFactorYear(factorizationSavingProject.getFactorYear());
            factorizationSavingAndProjectList.add(factorizationSavingAndProjectVM);
        }
        messageVM.setData(factorizationSavingAndProjectList);
        return messageVM;
    }

    public MessageVM getProjectMonthlySavingFactor(Integer factorYear, Long etuID) {
        List<BigInteger> projectlist = new ArrayList<BigInteger>();
        List<BigInteger> projectIds = new ArrayList<BigInteger>();
        if (etuID > 0) {

            ETU etu = etuRepository.findOne(etuID);
            projectlist = projectRepository.findAllProjectIdsByEtu(etu);
        } else {
            List<ETU> etulist = etuRepository.findAll();
            for (ETU etu : etulist) {
                projectIds = projectRepository.findAllProjectIdsByEtu(etu);
                projectlist.addAll(projectIds);
            }
            // projectlist = projectRepository.findAllProjectIdsByEtuId(etulist);

        }
        MessageVM messageVM = new MessageVM();
        HashMap<Integer, FactorizationProjectSavingVM> hashMap = new HashMap<>();
        for (int i = 1; i <= 13; i++) {
            hashMap.put(i, new FactorizationProjectSavingVM(i, factorYear));
        }
        if (projectlist.size() >= 1) {
            List<Object[]> dataList = projectFMatrixSavingsRepository.findAllProjectMonthlySaving(factorYear, projectlist);

            for (Object a[] : dataList) {
                if (hashMap.containsKey(Integer.parseInt(a[0] + ""))) {
                    FactorizationProjectSavingVM projectSavingVM = hashMap.get(Integer.parseInt(a[0] + ""));
                    projectSavingVM.setMonthlyProjectSaving((int) Double.parseDouble(a[1] + ""));
                    hashMap.replace(Integer.parseInt(a[0] + ""), projectSavingVM);
                }
            }
        } else {

        }

//		List<FactorizationProjectSavingVM> factorizationProjectSavingVMList = new ArrayList<>();
        List<FactorizationProjectSaving> factorizationProjectSavingsList = factorizationProjectSavingRepository.findAllByFactorYear(factorYear);
        for (FactorizationProjectSaving projectMonthlySaving : factorizationProjectSavingsList) {
//			FactorizationProjectSavingVM projectSavingVM = hashMap.get(projectMonthlySaving.getMonth());
//			projectSavingVM.setFactorYear(projectMonthlySaving.getFactorYear());
//			projectSavingVM.setMonth(projectMonthlySaving.getMonth());
//			projectSavingVM.setMonthFactor(projectMonthlySaving.getMonthFactor());
//			factorizationProjectSavingVMList.add(projectSavingVM);
            if (hashMap.containsKey(projectMonthlySaving.getMonth())) {
                FactorizationProjectSavingVM projectSavingVM = hashMap.get(projectMonthlySaving.getMonth());
                projectSavingVM.setMonthFactor(projectMonthlySaving.getMonthFactor());
                hashMap.replace(projectMonthlySaving.getMonth(), projectSavingVM);
            }
        }
        List<FactorizationProjectSavingVM> factorizationProjectSavingVMList = new ArrayList<FactorizationProjectSavingVM>(hashMap.values());
        messageVM.setData(factorizationProjectSavingVMList);
        return messageVM;
    }

    public MessageVM saveProjectSavingsFactor(Integer factorYear, List<FactorizationProjectSavingVM> factorizationProjectSavingVMs) {
        MessageVM messageVM = new MessageVM();
        for (FactorizationProjectSavingVM factorizationProjectSavingVM : factorizationProjectSavingVMs) {
            List<FactorizationProjectSaving> factorizationProjectSavingsList = factorizationProjectSavingRepository.findAllByFactorYearAndMonth(factorYear, factorizationProjectSavingVM.getMonth());
            FactorizationProjectSaving factorizationProjectSaving = new FactorizationProjectSaving();
            if (!factorizationProjectSavingsList.isEmpty()) {
                factorizationProjectSaving = factorizationProjectSavingsList.get(0);
            }
            factorizationProjectSaving.setMonth(factorizationProjectSavingVM.getMonth());
            factorizationProjectSaving.setFactorYear(factorYear);
            factorizationProjectSaving.setMonthFactor(factorizationProjectSavingVM.getMonthFactor());
            factorizationProjectSavingRepository.save(factorizationProjectSaving);
        }
        messageVM.setMessage("Project monthly saving factor's updated");
        return messageVM;
    }

}
