package com.blueroom.importdata;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.Pillar;
import com.blueroom.entities.SkillInventryEntity;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.repository.MethodsAndToolsRepository;
import com.blueroom.repository.PillarRepository;
import com.blueroom.repository.SkillInventryEntityRepository;
import com.blueroom.services.SkillService;
import com.blueroom.vm.MessageVM;

@Service
public class IntermediatePillarSpecificService {

    @Autowired
    PillarRepository pillarRepository;

    @Autowired
    AuthUserRepository authUserRepository;

    @Autowired
    MethodsAndToolsRepository methodsAndToolsRepository;

    @Autowired
    SkillInventryEntityRepository skillInventryEntityRepository;

    static Connection connection = null;
    static PreparedStatement preparedstatement = null;
    String filePath = "/home/mnt/govind/Skill_Inventory_Updated.xlsx";

    public MessageVM loadIntermediateData() {
        MessageVM messageVM = new MessageVM();
        long startTime = System.nanoTime();
        messageVM = updateExcelPath(filePath);
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        return messageVM;
    }

    private MessageVM updateExcelPath(String excelfilePath) {
        try {
            FileInputStream file = new FileInputStream(new File(excelfilePath));
            Workbook workbook = WorkbookFactory.create(new FileInputStream(excelfilePath));
            if (workbook instanceof HSSFWorkbook) {
                workbook = new HSSFWorkbook(file);
            } else if (workbook instanceof XSSFWorkbook) {
                workbook = new XSSFWorkbook(file);
            } else {
                throw new IllegalArgumentException("The specified file is not a Excel file");
            }
            XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(1);
            long count = 18375;
            Row headerRow = sheet.getRow(0);
            Row secondRow = sheet.getRow(1);
            for (int i = 2; i <= sheet.getLastRowNum(); i++) {
                Row row;
                String pillarCode = null;

                row = sheet.getRow(i);
                SkillInventryEntity skillInventryEntity = null;
                String methodAndToolsType = row.getCell(5).toString();
                if (methodAndToolsType.equals("Actual")) {
                    Pillar pillar = null;
                    pillarCode = row.getCell(3).toString();
                    if (pillarCode == null || pillarCode.equals("")) {
                        continue;
                    }
                    if (pillarCode.equals("EEM") || pillarCode.equals("EPM")) {
                        pillarCode = "EPM / EEM";
                    } else if (pillarCode.equals("Energy & PM")) {
                        pillarCode = "PM";
                    }

                    pillar = pillarRepository.findOneByCode(pillarCode);

                    if (Objects.isNull(pillar)) {
                        pillar = pillarRepository.findOneByPillarName(pillarCode);
                    }
                    /////////////////////////////////////////////////////////////
                    long employeeId;
                    String employeeName = null;
                    employeeId = (long) row.getCell(6).getNumericCellValue();
                    employeeName = row.getCell(7).toString();

                    AuthUser authuser = null;
                    authuser = authUserRepository.findOneByemployeeId(employeeId);
                    if (Objects.isNull(authuser)) {
                        authuser = !authUserRepository.findOneByName(employeeName).isEmpty() && authUserRepository.findOneByName(employeeName).size() == 1 ? authUserRepository.findOneByName(employeeName).get(0) : null;
                    }

                    if (Objects.nonNull(authuser) && Objects.nonNull(pillar)) {

                        for (int j = 8; j <= 121; j++) {
                            MethodsAndTools methodsAndTools = null;
                            String levelFirstTool;
                            String levelSecondTool;

                            if (headerRow.getCell(j) == null) {
                                levelFirstTool = null;
                            } else {
                                levelFirstTool = headerRow.getCell(j).toString();
                                if (Objects.isNull(levelFirstTool) || levelFirstTool.trim().equals("")) {
                                    levelSecondTool = secondRow.getCell(j).toString();
                                    methodsAndTools = methodsAndToolsRepository.findOneByLevelSecondTool(levelSecondTool);
                                } else if (methodsAndToolsRepository.findOneByLevelFirstTool(levelFirstTool).size() == 1) {
                                    methodsAndTools = methodsAndToolsRepository.findOneByLevelFirstTool(levelFirstTool).get(0);
                                } else if (methodsAndToolsRepository.findOneByLevelFirstTool(levelFirstTool).size() > 1) {
                                    levelSecondTool = secondRow.getCell(j).toString();
                                    methodsAndTools = methodsAndToolsRepository.findOneByLevelFirstToolAndLevelSecondTool(levelFirstTool, levelSecondTool);
                                }
                            }

                            //actual skill
                            long actual_skill;
                            if (row.getCell(j) == null) {
                                actual_skill = 0;
                            } else {
                                actual_skill = (long) row.getCell(j).getNumericCellValue();
                            }
                            if (Objects.nonNull(authuser) && Objects.nonNull(pillar) && Objects.nonNull(methodsAndTools)) {
                                /*							 skillInventryEntity= new SkillInventryEntity();
							skillInventryEntity.setAuthUser(authuser);
							skillInventryEntity.setMethodsTools(methodsAndTools);
							skillInventryEntity.setPillar(pillar);
							skillInventryEntity.setActual_skill_id(actual_skill);
							skillInventryEntityRepository.save(skillInventryEntity);
                                 */
                                skillInventryEntity = new SkillInventryEntity();
                                // skillInventryEntity.setId(count);
                                skillInventryEntity.setActual_skill_id(actual_skill);
                                //skillInventryEntity.setEmployeeId(methodsAndTools.getId());
                                skillInventryEntity.setMethodsTools(methodsAndTools);

                                //skillInventryEntity.setUserPillarId(pillar.getId());
                                skillInventryEntity.setPillar(pillar);

                                //skillInventryEntity.setUserId(authuser.getId());
                                skillInventryEntity.setAuthUser(authuser);
                                skillInventryEntityRepository.save(skillInventryEntity);

                                count++;

                            }

                        }
                    }
                }
            }//For i

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
