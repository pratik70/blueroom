package com.blueroom.importdata;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.Pillar;
import com.blueroom.entities.SkillInventryEntity;
import com.blueroom.entities.UserSkillSet;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.repository.MethodsAndToolsRepository;
import com.blueroom.repository.PillarRepository;
import com.blueroom.repository.SkillInventryEntityRepository;
import com.blueroom.repository.UserDao;
import com.blueroom.vm.MessageVM;

@Service
public class SkillInventoryService {

    @Autowired
    PillarRepository pillarRepository;

    @Autowired
    AuthUserRepository authUserRepository;

    @Autowired
    SkillInventryEntityRepository skillInventryEntityRepository;

    @Autowired
    UserDao userDao;

    @Autowired
    MethodsAndToolsRepository methodsAndToolsRepository;

    static Connection connection = null;
    static PreparedStatement preparedStatement = null;
    String filePath = "/home/mnt/govind/Skill_Inventory_Updated.xlsx";

    public MessageVM loadSkillInventoryData() {
        MessageVM vm = new MessageVM();
        long startTime = System.nanoTime();
        vm = uploadExcelFile(filePath);
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        return vm;
    }

    private MessageVM uploadExcelFile(String excelfilePath) {

        try {

            FileInputStream file = new FileInputStream(new File(excelfilePath));
            Workbook workbook = WorkbookFactory.create(new FileInputStream(excelfilePath));

            if (workbook instanceof HSSFWorkbook) {
                workbook = new XSSFWorkbook(file);
            } else if (workbook instanceof XSSFWorkbook) {
                workbook = new XSSFWorkbook(file);
            } else {
                throw new IllegalArgumentException("The specified file is not a Excel file");
            }

            XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
            long count = 1;

            if (true) {
                Row headerRow = sheet.getRow(0);
                Row secondRow = sheet.getRow(1);

                for (int i = 2; i <= sheet.getLastRowNum(); i++) {
                    Row row;
                    String pillarCode = null;
                    String employeeName = null;
                    long employeeId;
                    row = (Row) sheet.getRow(i);
                    SkillInventryEntity skillInventryEntity = null;
                    String methodsAndToolsType = row.getCell(5).toString();
                    if (methodsAndToolsType.equals("Actual")) { //checkk actual
                        Pillar pillar = null;
                        pillarCode = row.getCell(3).toString();
                        if (pillarCode == null || pillarCode.equals("")) {
                            continue;
                        }
                        if (pillarCode.equals("EEM") || pillarCode.equals("EPM")) {
                            pillarCode = "EPM / EEM";
                        } else if (pillarCode.equals("Energy & PM")) {
                            pillarCode = "PM";
                        }
                        pillar = pillarRepository.findOneByCode(pillarCode);
                        if (Objects.isNull(pillar)) {
                            pillar = pillarRepository.findOneByPillarName(pillarCode);
                        }

                        employeeId = (long) row.getCell(6).getNumericCellValue();
                        employeeName = row.getCell(7).toString();

                        AuthUser authuser = null;
                        authuser = authUserRepository.findOneByemployeeId(employeeId);
                        if (Objects.isNull(authuser)) {

                            authuser = !authUserRepository.findOneByName(employeeName).isEmpty() && authUserRepository.findOneByName(employeeName).size() == 1 ? authUserRepository.findOneByName(employeeName).get(0) : null;

                        }

                        if (Objects.nonNull(authuser) && Objects.nonNull(pillar)) {
                            for (int j = 8; j <= 169; j++) {
                                MethodsAndTools methodsAndTools = null;
                                String levelFirstTool;
                                String levelSecondTool;

                                if (headerRow.getCell(j) == null) {
                                    levelFirstTool = null;
                                } else {
                                    levelFirstTool = headerRow.getCell(j).toString();
                                    if (Objects.isNull(levelFirstTool) || levelFirstTool.trim().equals("")) {
                                        levelSecondTool = secondRow.getCell(j).toString();
                                        methodsAndTools = methodsAndToolsRepository.findOneByLevelSecondTool(levelSecondTool);
                                    } else if (methodsAndToolsRepository.findOneByLevelFirstTool(levelFirstTool).size() == 1) {
                                        methodsAndTools = methodsAndToolsRepository.findOneByLevelFirstTool(levelFirstTool).get(0);
                                    } else if (methodsAndToolsRepository.findOneByLevelFirstTool(levelFirstTool).size() > 1) {
                                        levelSecondTool = secondRow.getCell(j).toString();
                                        methodsAndTools = methodsAndToolsRepository.findOneByLevelFirstToolAndLevelSecondTool(levelFirstTool, levelSecondTool);
                                    }
                                }

                                long actual_skill;
                                if (row.getCell(j) == null) {
                                    actual_skill = 0;
                                } else {
                                    actual_skill = (long) row.getCell(j).getNumericCellValue();
                                }

                                //method tool id
                                if (Objects.nonNull(pillar) && Objects.nonNull(authuser) && Objects.nonNull(methodsAndTools)) {
                                    skillInventryEntity = new SkillInventryEntity();
                                    skillInventryEntity.setActual_skill_id(actual_skill);
                                    skillInventryEntity.setMethodsTools(methodsAndTools);
                                    skillInventryEntity.setPillar(pillar);
                                    skillInventryEntity.setAuthUser(authuser);
                                    skillInventryEntityRepository.save(skillInventryEntity);
                                    count++;
                                }
                            }
                        }
                    }

                }

            }

            for (int i = 2; i <= 0; i++) {
                UserSkillSet userSkillSet = null;
                long employeeId;
                String actual_skill;
                long actual_skill_no[];
                double user_pillar_id;
                Row row;
                Row headerRow = sheet.getRow(0);
                Row secondRow = sheet.getRow(1);
                Row thirdRow = sheet.getRow(2);
                String pillarCode = null;
                String employeeName = null;
                row = (Row) sheet.getRow(i);
                List<String> skippedPillars = null;
                String methodsAndTools = row.getCell(5).toString();
                Pillar pillar = null;
                AuthUser authuser = null;
                SkillInventryEntity skillInventryEntity = null;
//				if(row.getCell(1)==null){
//					methodToolId = 0.00;
//				}
//				else
//				{
//						if(i>=2)
//						{
                employeeId = (long) row.getCell(6).getNumericCellValue();
                pillarCode = row.getCell(3).toString();
                if (pillarCode == null || pillarCode.equals("")) {

                    continue;
                }
                if (pillarCode != null) {
                    if (pillarCode.equals("EEM") || pillarCode.equals("EPM")) {
                        pillarCode = "EPM / EEM";
                    } else if (pillarCode.equals("Energy & PM")) {
                        pillarCode = "PM";
                    }
                    if (pillarCode.equals("")) {
                        pillarCode = "EPM / EEM";
                    }
                    pillar = pillarRepository.findOneByCode(pillarCode);
                    if (Objects.isNull(pillar)) {
                        pillar = pillarRepository.findOneByPillarName(pillarCode);
                    }

                }
                employeeName = row.getCell(7).toString();

                if (methodsAndTools.equals("Actual")) {
                    authuser = authUserRepository.findOneByemployeeId(employeeId);
                    if (Objects.isNull(authuser)) {
                        authuser = !authUserRepository.findOneByName(employeeName).isEmpty() && authUserRepository.findOneByName(employeeName).size() == 1 ? authUserRepository.findOneByName(employeeName).get(0) : null;
                    }
                }
                if (row == headerRow) {
                    for (int j = 6; j <= 166; j++) {
                        if (row.getCell(j) == null) {
                            actual_skill = null;
                        } else {
                            actual_skill = row.getCell(j).toString();
                        }
                    }//End of j*/
                } else {

                    for (int j = 6; j <= 166; j++) {
                        if (i >= 6) {
                            if (row.getCell(8) == null) {
                                user_pillar_id = 0;
                            } else {
                                double actual_skill_double;
                                actual_skill_double = row.getCell(j).getNumericCellValue();
                                long actual_skill_id = (long) actual_skill_double;
                            }
                        }

                    }//End of j

                }

                if (row == thirdRow) {
                    for (int j = 8; j <= 169; j++) {

                        if (row.getCell(j) == null) {
                            actual_skill = null;
                        } else {
                            double actual_skill_double;
                            actual_skill_double = row.getCell(j).getNumericCellValue();
                            long actual_skill_id = (long) actual_skill_double;
                            try {

                                if ((Objects.nonNull(pillar.getId())) && (Objects.nonNull(authuser.getId()))) {
                                    try {
                                    } catch (NullPointerException np) {
                                        np.printStackTrace();
                                    }
                                    count++;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }//End of j*/
                    //}
                }
            }//End of i	

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
