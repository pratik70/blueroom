package com.blueroom.importdata;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.Benifit;
import com.blueroom.entities.ETU;
import com.blueroom.entities.Factorization;
import com.blueroom.entities.KPI;
import com.blueroom.entities.Loss;
import com.blueroom.entities.Pillar;
import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectBenefitIndents;
import com.blueroom.entities.ProjectBenefitIndentsEmatrix;
import com.blueroom.entities.ProjectFMatrixSavings;
import com.blueroom.entities.ProjectKIP;
import com.blueroom.entities.ProjectSupportingPillars;
import com.blueroom.entities.ProjectTeamMember;
import com.blueroom.entities.TeamRole;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.BenifitRepository;
import com.blueroom.repository.ETURepository;
import com.blueroom.repository.FactorizationRepository;
import com.blueroom.repository.KPIRepository;
import com.blueroom.repository.LossRepository;
import com.blueroom.repository.MachineRepository;
import com.blueroom.repository.PillarRepository;
import com.blueroom.repository.ProjectBenefitIndentsEmatrixRepository;
import com.blueroom.repository.ProjectBenefitIndentsRepository;
import com.blueroom.repository.ProjectFMatrixSavingsRepository;
import com.blueroom.repository.ProjectKIPRepository;
import com.blueroom.repository.ProjectRepository;
import com.blueroom.repository.ProjectSupportingPillarsRepository;
import com.blueroom.repository.ProjectTeamMemberRepository;
import com.blueroom.repository.RoleRepository;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.vm.MessageVM;

//import excel.excelA.Cell;
//import excel.excelA.HSSFWorkbook;
//import excel.excelA.MatrixAEntity;
//import excel.excelA.Sheet;
//import excel.excelA.Workbook;
//import excel.excelA.XSSFWorkbook;
@Service
public class ProjectImportService {

    @Autowired
    LossRepository lossRepository;

    @Autowired
    ETURepository etuRepository;

    @Autowired
    MachineRepository machineRepository;

    @Autowired
    PillarRepository pillarRepository;

    @Autowired
    AuthUserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ProjectBenefitIndentsEmatrixRepository projectBenefitIndentsEmatrixRepository;

    @Autowired
    BenifitRepository benifitRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    ProjectSupportingPillarsRepository projectSupportingPillarsRepository;

    @Autowired
    ProjectTeamMemberRepository projectTeamMemberRepository;

    @Autowired
    ProjectBenefitIndentsRepository projectBenefitIndentsRepository;

    @Autowired
    KPIRepository kpiRepository;

    @Autowired
    ProjectKIPRepository projectKIPRepository;

    @Autowired
    ProjectFMatrixSavingsRepository projectFMatrixSavingsRepository;

    @Autowired
    FactorizationRepository factorizationRepository;

    static Connection connection = null;
    static PreparedStatement preparedStatement = null;
    String FILEPATH = "D:\\dev\\workspace\\Blue Room\\Assets\\Blueroom.xlsx";
    DateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
    DateFormat formater1 = new SimpleDateFormat("MM-dd-yyyy");
    DecimalFormat df2 = new DecimalFormat(".##");

    public MessageVM loadProjectMatrixData() {
        MessageVM vm = new MessageVM();
        long startTime = System.nanoTime();
        vm = uploadExcelFile(FILEPATH);

        //		HashMap<Integer, ProjectWeek> hashMap = new HashMap<>();
        //		hashMap.putAll(getPlanMonthDate(1,"Jan-18"));
        //		hashMap.putAll(getPlanMonthDate(5, "Feb-18"));
        //		hashMap.putAll(getPlanMonthDate(9,"Mar-18"));
        //		hashMap.putAll(getPlanMonthDate(13,"Apr-18"));
        //		hashMap.putAll(getPlanMonthDate(17,"May-19"));
        //		hashMap.putAll(getPlanMonthDate(21,"Jun-21"));
        //		vm.setData(hashMap);
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        return vm;
    }

    static void save(int rowIndex) {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MessageVM uploadExcelFile1(String excelFilePath) {
        MessageVM messageVM = new MessageVM();
        HashMap<Integer, ProjectWeek> hashMap = new HashMap<>();
        try {
            //List<Project> projectList = new ArrayList<>();
            Workbook workbook = WorkbookFactory.create(new FileInputStream(excelFilePath));
            if (workbook instanceof HSSFWorkbook) {
                workbook = (HSSFWorkbook) workbook;
            } else if (workbook instanceof XSSFWorkbook) {
                workbook = (XSSFWorkbook) workbook;
            } else {
                throw new IllegalArgumentException("The specified file is not a Excel file");
            }
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Row headerRow = datatypeSheet.getRow(0);
            for (Cell headerCell : headerRow) {
                if (headerCell.getCellTypeEnum() == CellType.STRING && headerCell.getStringCellValue().equalsIgnoreCase("Project Completion Date")) {
                    break;
                }
                if (headerCell.getColumnIndex() > 60 && headerCell.getCellTypeEnum() == CellType.NUMERIC && Objects.nonNull(headerCell.getNumericCellValue())) {
                    hashMap.putAll(getPlanMonthDate(headerCell.getColumnIndex(), headerCell.getDateCellValue()));
                }
            }

            messageVM.setData(hashMap);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return messageVM;
    }

    public MessageVM uploadExcelFile(String excelFilePath) {
        MessageVM messageVM = new MessageVM();
        HashMap<Integer, ProjectWeek> hashMap = new HashMap<>();
        try {

            List<Project> projectList = new ArrayList<>();
            Workbook workbook = WorkbookFactory.create(new FileInputStream(excelFilePath));
            if (workbook instanceof HSSFWorkbook) {
                workbook = (HSSFWorkbook) workbook;
            } else if (workbook instanceof XSSFWorkbook) {
                workbook = (XSSFWorkbook) workbook;
            } else {
                throw new IllegalArgumentException("The specified file is not a Excel file");
            }

            Sheet datatypeSheet = workbook.getSheetAt(0);
            Row headerRow = datatypeSheet.getRow(0);
            Cell cell = null;
            Cell cell2 = null;
            String cellType = null;
            Integer PROJECT_CODE_IND = 1;
            Integer LOSS_TYPE_IND = 2;
            Integer ETU_IND = 3;
            Integer OPERATION_NO_IDE = 5;
            Integer PROJECT_NAME_IND = 6;
            Integer PRELIMINARY_IND = 7;

            Integer YEARLY_POTENTIAL_IND = 8;

            Integer DIRECT_LABOUR_IND = 9;
            Integer INDIRECT_LABOUR_IND = 10;
            Integer SALARY_PAYROLL_IND = 11;
            Integer INDIRECT_MATERIALS_IND = 12;
            Integer MAINTENANCE_MATERIALS_IND = 13;
            Integer SCRAP_IND = 14;
            Integer ENERGY_IND = 15;
            Integer GENERAL_EXPENSES_IND = 16;

            Integer BASE_PILLAR_IND = 17;

            Integer SAF_IND = 18;
            Pillar pillarSAF = pillarRepository.findOne(1l);

            Integer CD_IND = 19;
            Pillar pillarCD = pillarRepository.findOne(2l);

            Integer FI_IND = 20;
            Pillar pillarFI = pillarRepository.findOne(3l);

            Integer AM_IND = 21;
            Pillar pillarAM = pillarRepository.findOne(4l);

            Integer PM_IND = 22;
            Pillar pillarPM = pillarRepository.findOne(6l);

            Integer QC_IND = 23;
            Pillar pillarQC = pillarRepository.findOne(7l);

            Integer LOG_IND = 24;
            Pillar pillarLOG = pillarRepository.findOne(8l);

            Integer EEM_IND = 25;
            Pillar pillarEEM = pillarRepository.findOne(9l);

            Integer PD_IND = 26;
            Pillar pillarPD = pillarRepository.findOne(10l);

            Integer ENV_IND = 27;
            Pillar pillarENV = pillarRepository.findOne(11l);

            Integer ENG_IND = 28;
            Pillar pillarENG = pillarRepository.findOne(12l);

            Integer WO_IND = 29;
            Pillar pillarWO = pillarRepository.findOne(5l);

            //KPI START
            Integer LWDC_IND = 30;
            KPI kpiLWDC = kpiRepository.findOne(1l);

            Integer IPTV_IND = 31;
            KPI kpiIPTV = kpiRepository.findOne(2l);

            Integer FTQ_IND = 32;
            KPI kpiFTQ = kpiRepository.findOne(3l);

            Integer UNIT_COST_IND = 33;
            KPI kpiUNIT_COST = kpiRepository.findOne(4l);

            Integer LS_IND = 34;
            KPI kpiLS = kpiRepository.findOne(5l);

            Integer AH_IND = 35;
            KPI kpiAH = kpiRepository.findOne(6l);

            Integer UPTIME_IND = 36;
            KPI kpiUPTIME = kpiRepository.findOne(7l);

            Integer ABSENTEEISM_IND = 37;
            KPI kpiABSENTEEISM = kpiRepository.findOne(8l);

            Integer OEE_IND = 38;
            KPI kpiOEE = kpiRepository.findOne(9l);
            //KPI END

            Integer TEAM_LEADER_IND = 39;
            Integer TEAM_COACH_IND = 40;
            Integer MEMBER_START_IND = 41;
            Integer MEMBER_END_IND = 44;

            //cost
            Integer DIRECT_LABOUR_COST_IND = 45;
            Integer INDIRECT_LABOUR_COST_IND = 46;
            Integer SALARY_PAYROLL_COST_IND = 47;
            Integer INDIRECT_MATERIALS_COST_IND = 48;
            Integer MAINTENANCE_MATERIALS_COST_IND = 49;
            Integer GENERAL_EXPENSES_COST_IND = 50;

            Integer TOTAL_PROJECT_COST_IND = 51;
            Integer CONFIDENCE_LEVEL_IND = 52;
            Integer BC_IND = 53;
            Integer PROJECT_START_IND = 54;
            Integer PROJECT_START_DATE_IND = 55;
            Integer PROJECT_END_DATE_IND = 56;

            //savings type
            Integer HARD_SAVING_IND = 57;
            Integer SOFT_SAVING_IND = 58;
            Integer COST_AVOIDANCE_IND = 59;
            Integer VIRTUAL_SAVING_IND = 60;

            Integer PLANNING_START_DATE_IND = 62;
            Integer PLANNING_END_DATE_IND = 62;

            for (Cell headerCell : headerRow) {
                if (headerCell.getCellTypeEnum() == CellType.STRING && headerCell.getStringCellValue().equalsIgnoreCase("Project Completion Date")) {
                    PLANNING_END_DATE_IND = headerCell.getColumnIndex() - 1;
                    break;
                }
                if (headerCell.getColumnIndex() > 60 && headerCell.getCellTypeEnum() == CellType.NUMERIC && Objects.nonNull(headerCell.getNumericCellValue())) {
                    hashMap.putAll(getPlanMonthDate(headerCell.getColumnIndex(), headerCell.getDateCellValue()));
                }

            }

            Integer PROJECT_COMPLETEION_IND = 0;
            for (Cell headerCell : headerRow) {
                if (headerCell.getCellTypeEnum() == CellType.STRING && headerCell.getStringCellValue().length() > 0 && headerCell.getStringCellValue().equalsIgnoreCase("Project Completion Date")) {
                    PROJECT_COMPLETEION_IND = headerCell.getColumnIndex();
                }
            }

            Integer STANDARDISATOIN_IND = PROJECT_COMPLETEION_IND + 1;
            Integer HORZIONTAL_IND = PROJECT_COMPLETEION_IND + 2;
            Integer MP_INFO_IND = PROJECT_COMPLETEION_IND + 3;

            //role list
            TeamRole role1 = roleRepository.findOne(1);
            TeamRole role2 = roleRepository.findOne(2);
            TeamRole role3 = roleRepository.findOne(3);
            List<Benifit> benifits = benifitRepository.findAll();
            for (int rowIndex = 1; rowIndex < datatypeSheet.getLastRowNum(); rowIndex += 2) {
                //for (int rowIndex = 1; rowIndex < 5; rowIndex+= 2) {
                //			for ( int rowIndex = 29; rowIndex < 30; rowIndex+= 2 ) {
                try {

                    Row row = datatypeSheet.getRow(rowIndex);
                    Row row2 = datatypeSheet.getRow(rowIndex + 1);

                    Project project = new Project();
                    List<ProjectSupportingPillars> pillarList = new ArrayList<>();
                    List<ProjectTeamMember> projectTeamMembers = new ArrayList<>();
                    ProjectBenefitIndentsEmatrix projectBenefitIndentsEmatrix = new ProjectBenefitIndentsEmatrix();
                    List<ProjectBenefitIndents> projectBenefitIndentList = new ArrayList<>();
                    //              MatrixAEntity entity = new MatrixAEntity();
                    // Project Code                           
                    cell = row.getCell(PROJECT_CODE_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            cellType = cell.getStringCellValue();
                        }
                        project.setProjectCode(cellType);
                    } else {
                        continue;
                    }
                    //loss type              
                    cell = row.getCell(LOSS_TYPE_IND);
                    if (Objects.nonNull(cell)) {
                        String lossValue = "";
                        if (cell.getStringCellValue().length() > 0) {
                            lossValue = cell.getStringCellValue();
                            lossValue = lossValue.trim();
                            Loss loss = lossRepository.findAllByLoss(lossValue);
                            project.setLossType(loss);
                        }

                    }

                    cell = row.getCell(ETU_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String etuNo = cell.getStringCellValue().trim();
                            ETU etu = etuRepository.findOneByEtuNo(etuNo);
                            if (etu != null) {
                                project.setEtu(etu);
                            }
                        }

                    }
                    //				Machine Operation Number 
                    cell = row.getCell(OPERATION_NO_IDE);
                    if (Objects.nonNull(cell)) {

                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        String val = cell.getStringCellValue();
                        if (val != null && val.toLowerCase().contains("op")) {
                            val = val.toLowerCase().replace("op", "");

                        }
                        if (val != null) {
                            val = val.trim();
                        }
                        boolean hasNonLetters = false;
                        for (char ch : val.toCharArray()) {
                            if (!Character.isLetter(ch)) {
                                hasNonLetters = true;
                                break;
                            }
                        }
                        if (hasNonLetters) {
                            if (val.contains(".")) {
                                val = df2.format(Double.parseDouble(val));
                            }
                        }
                        project.setOptMachine(machineRepository.findByMachinCode(val));
                    }
                    //project title
                    cell = row.getCell(PROJECT_NAME_IND);
                    if (Objects.nonNull(cell)) {
                        String projectName = "";
                        if (cell.getStringCellValue().length() > 0) {
                            projectName = cell.getStringCellValue();
                        }
                        project.setProjectName(projectName);
                    }

                    //PRELIMINARY Analysis
                    cell = row.getCell(PRELIMINARY_IND);
                    if (Objects.nonNull(cell)) {
                        String preliminaryAnalysis = "";
                        if (cell.getStringCellValue().length() > 0) {
                            preliminaryAnalysis = cell.getStringCellValue();
                        }
                        project.setPreliminaryAnalysis(preliminaryAnalysis);
                    }

                    //DIRECT LABOUR 
                    cell = row.getCell(DIRECT_LABOUR_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            project.setDirectLoss((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                        }
                    }

                    //INDIRECT LABOUR 
                    cell = row.getCell(INDIRECT_LABOUR_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            project.setIndirectLoss((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                        }
                    }

                    //SALARY PAYROLL
                    cell = row.getCell(SALARY_PAYROLL_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            project.setStaffLoss((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                        }
                    }

                    //INDIRECT MATERIALS  
                    cell = row.getCell(INDIRECT_MATERIALS_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            project.setIndirectMaterialsLoss((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                        }
                    }

                    //MAINTENANCE MATERIALS  
                    cell = row.getCell(MAINTENANCE_MATERIALS_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            project.setMaintenanceLoss((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                        }
                    }

                    //SCRAP
                    cell = row.getCell(SCRAP_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            project.setScrapLoss((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                        }
                    }

                    //ENERGY IND 
                    cell = row.getCell(ENERGY_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            project.setEnergyLoss((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                        }
                    }

                    //GENERAL EXPENSES
                    cell = row.getCell(GENERAL_EXPENSES_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            project.setExpensessLoss((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                        }
                    }

                    // YEARLY POTENTIAL 
                    cell = row.getCell(YEARLY_POTENTIAL_IND);
                    if (Objects.nonNull(cell)) {
                        //try catch added by g
                        try {
                            if (cell.getNumericCellValue() > 0) {
                                Double yearlyPotential = Double.parseDouble(df2.format(Double.parseDouble(cell.getNumericCellValue() + "")));
                                project.setYearlyPotential(yearlyPotential);
                            }
                        } catch (Exception e) {
                            project.setYearlyPotential(0.0);
                        }

                    }
                    projectRepository.save(project);

                    //Project Pillar 
                    cell = row.getCell(BASE_PILLAR_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            Pillar pillar = pillarRepository.findOneByCode(code);
                            if (pillar != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillar);
                                projectBasePillar.setPrimary(true);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //SAF_IND
                    cell = row.getCell(SAF_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarSAF);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //CD_IND
                    cell = row.getCell(CD_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarCD);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //FI_IND
                    cell = row.getCell(FI_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarFI);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //AM_IND
                    cell = row.getCell(AM_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarAM);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //PM_IND
                    cell = row.getCell(PM_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarPM);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //QC_IND
                    cell = row.getCell(QC_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarQC);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //LOG_IND
                    cell = row.getCell(LOG_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarLOG);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //EEM_IND
                    cell = row.getCell(EEM_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarEEM);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //PD_IND
                    cell = row.getCell(PD_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarPD);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //ENV_IND
                    cell = row.getCell(ENV_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarENV);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //ENG_IND
                    cell = row.getCell(ENG_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarENG);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }

                        }

                    }

                    //WO_IND
                    cell = row.getCell(WO_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectSupportingPillars projectBasePillar = new ProjectSupportingPillars();
                                projectBasePillar.setPillar(pillarWO);
                                projectBasePillar.setPrimary(false);
                                projectBasePillar.setProject(project);
                                projectSupportingPillarsRepository.save(projectBasePillar);
                                pillarList.add(projectBasePillar);
                            }
                        }
                    }

                    //LWDC_IND
                    cell = row.getCell(LWDC_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiLWDC);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //IPTV_IND
                    cell = row.getCell(IPTV_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiIPTV);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //FTQ_IND
                    cell = row.getCell(FTQ_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiFTQ);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //UNIT_COST_IND
                    cell = row.getCell(UNIT_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiUNIT_COST);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //LS_IND
                    cell = row.getCell(LS_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiLS);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //AH_IND
                    cell = row.getCell(AH_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiAH);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //UPTIME_IND
                    cell = row.getCell(UPTIME_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiUPTIME);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //ABSENTEEISM_IND
                    cell = row.getCell(ABSENTEEISM_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiABSENTEEISM);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //OEE_IND
                    cell = row.getCell(OEE_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String code = cell.getStringCellValue().trim();
                            if (code != null) {
                                ProjectKIP projectKIP = new ProjectKIP();
                                projectKIP.setProject(project);
                                projectKIP.setKpi(kpiOEE);
                                projectKIPRepository.save(projectKIP);
                            }
                        }
                    }

                    //Team Leader 
                    cell = row.getCell(TEAM_LEADER_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String leaderName = cell.getStringCellValue().trim();
                            List<AuthUser> userList = userRepository.findOneByName(leaderName);
                            if (!userList.isEmpty()) {
                                ProjectTeamMember member = new ProjectTeamMember();
                                member.setUser(userList.get(0));
                                member.setFilteredUser(true);
                                member.setRole(role1);
                                member.setProject(project);
                                projectTeamMemberRepository.save(member);
                                projectTeamMembers.add(member);
                            }
                        }
                    }

                    //Team COACH 
                    cell = row.getCell(TEAM_COACH_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            String leaderName = cell.getStringCellValue().trim();
                            List<AuthUser> userList = userRepository.findOneByName(leaderName);
                            if (!userList.isEmpty()) {
                                ProjectTeamMember member = new ProjectTeamMember();
                                member.setUser(userList.get(0));
                                member.setFilteredUser(true);
                                member.setRole(role2);
                                member.setProject(project);
                                projectTeamMemberRepository.save(member);
                                projectTeamMembers.add(member);
                            }
                        }
                    }

                    //Team member 
                    for (int i = MEMBER_START_IND; i <= MEMBER_END_IND; i++) {
                        cell = row.getCell(i);
                        if (Objects.nonNull(cell)) {
                            if (cell.getStringCellValue().length() > 0) {
                                String memberName = cell.getStringCellValue().trim();
                                List<AuthUser> userList = userRepository.findOneByName(memberName);
                                if (!userList.isEmpty()) {
                                    ProjectTeamMember member = new ProjectTeamMember();
                                    member.setUser(userList.get(0));
                                    member.setFilteredUser(true);
                                    member.setRole(role3);
                                    member.setProject(project);
                                    projectTeamMemberRepository.save(member);
                                    projectTeamMembers.add(member);
                                }
                            }
                        }
                    }

                    //DIRECT LABOUR COST
                    cell = row.getCell(DIRECT_LABOUR_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            ProjectBenefitIndents benefitIndents = new ProjectBenefitIndents();
                            Integer directLoss = (int) Double.parseDouble(df2.format(Double.parseDouble(cell.getNumericCellValue() + "")));
                            benefitIndents.setBenifitValue(directLoss);
                            benefitIndents.setProject(project);
                            benefitIndents.setBenifit(benifits.get(0));
                            projectBenefitIndentsRepository.save(benefitIndents);
                            projectBenefitIndentList.add(benefitIndents);
                        }
                    }

                    //INDIRECT LABOUR COST
                    cell = row.getCell(INDIRECT_LABOUR_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            ProjectBenefitIndents benefitIndents = new ProjectBenefitIndents();
                            benefitIndents.setBenifitValue((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                            benefitIndents.setProject(project);
                            benefitIndents.setBenifit(benifits.get(1));
                            projectBenefitIndentsRepository.save(benefitIndents);
                            projectBenefitIndentList.add(benefitIndents);
                        }
                    }

                    //SALARY PAYROLL COST
                    cell = row.getCell(SALARY_PAYROLL_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            ProjectBenefitIndents benefitIndents = new ProjectBenefitIndents();
                            benefitIndents.setBenifitValue((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                            benefitIndents.setProject(project);
                            benefitIndents.setBenifit(benifits.get(2));
                            projectBenefitIndentsRepository.save(benefitIndents);
                            projectBenefitIndentList.add(benefitIndents);
                        }
                    }

                    //INDIRECT MATERIALS  COST
                    cell = row.getCell(INDIRECT_MATERIALS_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            ProjectBenefitIndents benefitIndents = new ProjectBenefitIndents();
                            benefitIndents.setBenifitValue((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                            benefitIndents.setProject(project);
                            benefitIndents.setBenifit(benifits.get(3));
                            projectBenefitIndentsRepository.save(benefitIndents);
                            projectBenefitIndentList.add(benefitIndents);
                        }
                    }

                    //MAINTENANCE MATERIALS  
                    cell = row.getCell(MAINTENANCE_MATERIALS_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            ProjectBenefitIndents benefitIndents = new ProjectBenefitIndents();
                            benefitIndents.setBenifitValue((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                            benefitIndents.setProject(project);
                            benefitIndents.setBenifit(benifits.get(4));
                            projectBenefitIndentsRepository.save(benefitIndents);
                            projectBenefitIndentList.add(benefitIndents);
                        }
                    }

                    //SCRAP
                    cell = row.getCell(GENERAL_EXPENSES_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            ProjectBenefitIndents benefitIndents = new ProjectBenefitIndents();
                            benefitIndents.setBenifitValue((int) Double.parseDouble(cell.getNumericCellValue() + ""));
                            benefitIndents.setProject(project);
                            benefitIndents.setBenifit(benifits.get(7));
                            projectBenefitIndentsRepository.save(benefitIndents);
                            projectBenefitIndentList.add(benefitIndents);
                        }
                    }

                    // TOTAL PROJECT COST
                    cell = row.getCell(TOTAL_PROJECT_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            projectBenefitIndentsEmatrix.setTotalProjectCost((int) (Double.parseDouble(cell.getNumericCellValue() + "")));
                        }
                    }

                    // TOTAL PROJECT COST
                    cell = row.getCell(TOTAL_PROJECT_COST_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getNumericCellValue() > 0) {
                            projectBenefitIndentsEmatrix.setTotalProjectCost((int) (Double.parseDouble(cell.getNumericCellValue() + "")));
                        }
                    }

                    //CONFIDENCE LEVEL
                    cell = row.getCell(CONFIDENCE_LEVEL_IND);
                    if (Objects.nonNull(cell)) {
                        if (Objects.nonNull(cell.getNumericCellValue())) {
                            projectBenefitIndentsEmatrix.setConfidenceLevel((int) (Double.parseDouble((cell.getNumericCellValue() * 100) + "")));
                        }
                    }

                    // BC Value
                    cell = row.getCell(BC_IND);
                    if (Objects.nonNull(cell)) {
                        //if-else and try catch added by g
                        if (cell.getCellTypeEnum() == CellType.NUMERIC || cell.getCellTypeEnum() == CellType.FORMULA) {
                            try {
                                if (cell.getNumericCellValue() > 0) {
                                    Double bc = Double.parseDouble(df2.format(Double.parseDouble(cell.getNumericCellValue() + "")));
                                    projectBenefitIndentsEmatrix.setBc(bc);
                                }
                            } catch (Exception e) {
                                projectBenefitIndentsEmatrix.setBc(0.0);
                                e.printStackTrace();
                            }
                        } else {
                            cell.setCellType(CellType.STRING);
                            String data = cell.getStringCellValue();
                            if (data == null || data.length() == 0 || data.equalsIgnoreCase("-") || data.equalsIgnoreCase("") || data.trim().equalsIgnoreCase("")) {
                                projectBenefitIndentsEmatrix.setBc(0.0);
                            } else {
                                Double bc = Double.parseDouble(df2.format(Double.parseDouble(data)));
                                projectBenefitIndentsEmatrix.setBc(bc > 0 ? bc : 0.0);
                            }
                        }
                    }

                    // Project Start or not 
                    cell = row.getCell(PROJECT_START_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            project.setExecutionStarted(cell.getStringCellValue().equalsIgnoreCase("YES"));
                        } else {
                            project.setExecutionStarted(false);
                        }
                    }

                    // Project Start DATE
                    cell = row.getCell(PROJECT_START_DATE_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            Date date = formater.parse(cell.getStringCellValue());
                            projectBenefitIndentsEmatrix.setStartDate(formater1.format(date));
                        }
                    }

                    // Project Target DATE
                    cell = row.getCell(PROJECT_END_DATE_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            Date date = formater.parse(cell.getStringCellValue());
                            projectBenefitIndentsEmatrix.setTargetDate(formater1.format(date));

                        }
                    }

                    // Project Saving Type Hard Savings(Rs)
                    cell = row.getCell(HARD_SAVING_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            projectBenefitIndentsEmatrix.setSavingsType("Hard Savings(Rs)");
                        }
                    }

                    // Project Saving Type Soft Savings(Rs)
                    cell = row.getCell(SOFT_SAVING_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            projectBenefitIndentsEmatrix.setSavingsType("Soft Savings(Rs)");
                        }
                    }

                    // Project Saving Type Cost Avoidance(Rs)
                    cell = row.getCell(COST_AVOIDANCE_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            projectBenefitIndentsEmatrix.setSavingsType("Cost Avoidance(Rs)");
                        }
                    }

                    // Project Saving Type Virtual Savings(Rs)
                    cell = row.getCell(VIRTUAL_SAVING_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            projectBenefitIndentsEmatrix.setSavingsType("Virtual Savings(Rs)");
                        }
                    }

                    String state = "";
                    for (int i = PLANNING_START_DATE_IND; i <= PLANNING_END_DATE_IND; i++) {
                        cell = row.getCell(i);
                        cell2 = row2.getCell(i);
                        //					if (cell.getCellTypeEnum()  ==  CellType.STRING &&  cell.getStringCellValue().length()  > 0 ) {
                        //						ProjectWeek projectWeek =  hashMap.get(cell.getColumnIndex());
                        //						if (cell.getStringCellValue().equalsIgnoreCase("p") ) {
                        //							if ( projectBenefitIndentsEmatrix.getpPlannedStartDate() == null) {
                        //								projectBenefitIndentsEmatrix.setpPlannedStartDate(projectWeek.getStartWeekDateStr());
                        //							} 
                        //							projectBenefitIndentsEmatrix.setpPlannedEndDate(projectWeek.getEndWeekDateStr());
                        //						} else if (cell.getStringCellValue().equalsIgnoreCase("d") ) {
                        //							if ( projectBenefitIndentsEmatrix.getdPlannedStartDate() == null) {
                        //								projectBenefitIndentsEmatrix.setdPlannedStartDate(projectWeek.getStartWeekDateStr());
                        //							} 
                        //							projectBenefitIndentsEmatrix.setdPlannedEndDate(projectWeek.getEndWeekDateStr());
                        //						} else if (cell.getStringCellValue().equalsIgnoreCase("c") ) {
                        //							if ( projectBenefitIndentsEmatrix.getcPlannedStartDate() == null) {
                        //								projectBenefitIndentsEmatrix.setcPlannedStartDate(projectWeek.getStartWeekDateStr());
                        //							} 
                        //							projectBenefitIndentsEmatrix.setcPlannedEndDate(projectWeek.getEndWeekDateStr());
                        //						} else if (cell.getStringCellValue().equalsIgnoreCase("a") ) {
                        //							if ( projectBenefitIndentsEmatrix.getaPlannedStartDate() == null) {
                        //								projectBenefitIndentsEmatrix.setaPlannedStartDate(projectWeek.getStartWeekDateStr());
                        //							} 
                        //							projectBenefitIndentsEmatrix.setaPlannedEndDate(projectWeek.getEndWeekDateStr());
                        //						}
                        //						
                        //					}

                        if (cell.getCellTypeEnum() == CellType.STRING && cell.getStringCellValue().length() > 0) {
                            ProjectWeek projectWeek = hashMap.get(cell.getColumnIndex());
                            if (cell.getStringCellValue().equalsIgnoreCase("p")) {
                                if (projectBenefitIndentsEmatrix.getpPlannedStartDate() == null) {
                                    projectBenefitIndentsEmatrix.setpPlannedStartDate(projectWeek.getStartWeekDateStr());
                                }
                                projectBenefitIndentsEmatrix.setpPlannedEndDate(projectWeek.getEndWeekDateStr());
                            } else if (cell.getStringCellValue().equalsIgnoreCase("d")) {
                                if (projectBenefitIndentsEmatrix.getdPlannedStartDate() == null) {
                                    projectBenefitIndentsEmatrix.setdPlannedStartDate(projectWeek.getStartWeekDateStr());
                                }
                                projectBenefitIndentsEmatrix.setdPlannedEndDate(projectWeek.getEndWeekDateStr());
                            } else if (cell.getStringCellValue().equalsIgnoreCase("c")) {
                                if (projectBenefitIndentsEmatrix.getcPlannedStartDate() == null) {
                                    projectBenefitIndentsEmatrix.setcPlannedStartDate(projectWeek.getStartWeekDateStr());
                                }
                                projectBenefitIndentsEmatrix.setcPlannedEndDate(projectWeek.getEndWeekDateStr());
                            } else if (cell.getStringCellValue().equalsIgnoreCase("a")) {
                                if (projectBenefitIndentsEmatrix.getaPlannedStartDate() == null) {
                                    projectBenefitIndentsEmatrix.setaPlannedStartDate(projectWeek.getStartWeekDateStr());
                                }
                                projectBenefitIndentsEmatrix.setaPlannedEndDate(projectWeek.getEndWeekDateStr());
                            }

                        }
                        if (cell2.getCellTypeEnum() == CellType.STRING && cell2.getStringCellValue().length() > 0) {
                            ProjectWeek projectWeek = hashMap.get(cell.getColumnIndex());
                            if (cell2.getStringCellValue().equalsIgnoreCase("p")) {
                                if (projectBenefitIndentsEmatrix.getpActualStartDate() == null) {
                                    projectBenefitIndentsEmatrix.setpActualStartDate(projectWeek.getStartWeekDateStr());
                                }
                                projectBenefitIndentsEmatrix.setpActualEndDate(projectWeek.getEndWeekDateStr());
                            } else if (cell2.getStringCellValue().equalsIgnoreCase("d")) {
                                if (projectBenefitIndentsEmatrix.getdActualStartDate() == null) {
                                    projectBenefitIndentsEmatrix.setdActualStartDate(projectWeek.getStartWeekDateStr());
                                }
                                projectBenefitIndentsEmatrix.setdActualEndDate(projectWeek.getEndWeekDateStr());
                            } else if (cell2.getStringCellValue().equalsIgnoreCase("c")) {
                                if (projectBenefitIndentsEmatrix.getcActualStartDate() == null) {
                                    projectBenefitIndentsEmatrix.setcActualStartDate(projectWeek.getStartWeekDateStr());
                                }
                                projectBenefitIndentsEmatrix.setcActualEndDate(projectWeek.getEndWeekDateStr());
                            } else if (cell2.getStringCellValue().equalsIgnoreCase("a")) {
                                if (projectBenefitIndentsEmatrix.getaActualStartDate() == null) {
                                    projectBenefitIndentsEmatrix.setaActualStartDate(projectWeek.getStartWeekDateStr());
                                }
                                projectBenefitIndentsEmatrix.setaActualEndDate(projectWeek.getEndWeekDateStr());
                            }
                        }
                    }

                    //				Integer PLANNING_START_DATE_IND = 61;
                    //				Integer PLANNING_END_DATE_IND = 60;
                    //				
                    // Project Completion Date
                    cell = row.getCell(PROJECT_COMPLETEION_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            Date date = formater.parse(cell.getStringCellValue());
                            project.setCompletionDate(date);
                            project.setCompleteStage(7);
                        }
                    }

                    // PROJECT STANDARDISATOIN
                    cell = row.getCell(STANDARDISATOIN_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            project.setStandardisation(cell.getStringCellValue().equalsIgnoreCase("yes"));
                        }
                    }

                    cell = row.getCell(HORZIONTAL_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            project.setHorizontalExpansion(cell.getStringCellValue().equalsIgnoreCase("yes"));
                        }
                    }

                    cell = row.getCell(MP_INFO_IND);
                    if (Objects.nonNull(cell)) {
                        if (cell.getStringCellValue().length() > 0) {
                            project.setEpmInfo(cell.getStringCellValue().equalsIgnoreCase("yes"));
                        }
                    }
                    if (Objects.nonNull(project.getYearlyPotential())) {
                        projectBenefitIndentsEmatrix.setTotalPotentialBenefit((int) Double.parseDouble(project.getYearlyPotential() + ""));
                    } else {
                        projectBenefitIndentsEmatrix.setTotalPotentialBenefit(0);
                    }
                    projectBenefitIndentsEmatrix.setProject(project);
                    projectBenefitIndentsEmatrixRepository.save(projectBenefitIndentsEmatrix);
                    project.setCreatedAt(new Date());
                    project.setUpdatedAt(new Date());
                    projectRepository.save(project);
                    updateSavingData(project);
                    projectList.add(project);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            messageVM.setData(projectList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageVM;
    }

    public HashMap<Integer, ProjectWeek> getPlanMonthDate(int colNumber, Date month) {
        HashMap<Integer, ProjectWeek> plan = new HashMap<>();
        DateFormat monthFormater = new SimpleDateFormat("MM");
        DateFormat formater = new SimpleDateFormat("MMM-yy");
        DateFormat formater2 = new SimpleDateFormat("MM-dd-yyyy");
        try {
            //			Date month = formater.parse(monthDate);
            Calendar beginCalendar = Calendar.getInstance();
            beginCalendar.setTime(month);
            Calendar finishCalendar = (Calendar) beginCalendar.clone();
            finishCalendar.set(Calendar.DAY_OF_MONTH, finishCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            int count = 0;
            while (beginCalendar.before(finishCalendar)) {

                Calendar satrtWeekCal = (Calendar) beginCalendar.clone();
                satrtWeekCal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                while (satrtWeekCal.before(beginCalendar)) {
                    satrtWeekCal.add(Calendar.DATE, 7);
                    satrtWeekCal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                }
                Calendar endWeekCal = Calendar.getInstance();
                endWeekCal.setTime(satrtWeekCal.getTime());
                endWeekCal.set(Calendar.DAY_OF_WEEK, endWeekCal.getActualMaximum(Calendar.DAY_OF_WEEK));
                while (endWeekCal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
                    endWeekCal.add(Calendar.DATE, 1);
                }
                endWeekCal.add(Calendar.DATE, -1);
                if (!monthFormater.format(endWeekCal.getTime()).equals(monthFormater.format(satrtWeekCal.getTime()))) {
                    endWeekCal.setTime(satrtWeekCal.getTime());
                    endWeekCal.set(Calendar.DAY_OF_MONTH, endWeekCal.getActualMaximum(Calendar.DAY_OF_MONTH));
                }

                ProjectWeek projectWeek = new ProjectWeek();
                projectWeek.setStartWeekDate(satrtWeekCal.getTime());
                projectWeek.setStartWeekDateStr(formater2.format(satrtWeekCal.getTime()));
                //				if (monthFormater.format(endWeekCal.getTime()) != monthFormater.format(satrtWeekCal.getTime())) {
                //					endWeekCal = satrtWeekCal;
                //					while(monthFormater.format(endWeekCal.getTime()) == monthFormater.format(satrtWeekCal.getTime())) {
                //						endWeekCal.add(Calendar.DATE, 1);
                //					}
                //					projectWeek.setEndWeekDate(endWeekCal.getTime());
                //					projectWeek.setEndWeekDateStr(formater2.format(endWeekCal.getTime()));	
                //				} else {
                //					
                //				}
                projectWeek.setEndWeekDate(endWeekCal.getTime());
                projectWeek.setEndWeekDateStr(formater2.format(endWeekCal.getTime()));
                plan.put(colNumber + count, projectWeek);
                //				if (count < 3) {
                //					plan.put(colNumber + count, projectWeek);
                //				} else {
                //					plan.put(colNumber + 3, projectWeek);
                //				}
                count++;
                beginCalendar.add(Calendar.DATE, 7);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return plan;
    }

    public HashMap<String, String> getPlanMonthDate1(String type, String action, String startDate, String endDate) {
        HashMap<String, String> plan = new HashMap<>();
        DateFormat formater = new SimpleDateFormat("MMM-yyyy");
        DateFormat formater2 = new SimpleDateFormat("MM-dd-yyyy");
        if (startDate != null && endDate != null) {
            Calendar beginCalendar = Calendar.getInstance();
            Calendar finishCalendar = Calendar.getInstance();
            try {
                beginCalendar.setTime(formater.parse(startDate));
                finishCalendar.setTime(formater.parse(endDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            while (beginCalendar.before(finishCalendar)) {
                ProjectWeek projectWeek = new ProjectWeek();
                String date = formater2.format(beginCalendar.getTime()).toUpperCase();
                beginCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                projectWeek.setStartWeekDateStr(date);
                //plan.put(type + "_" + date, action);
                beginCalendar.add(Calendar.DATE, 7);
            }
        }

        return plan;
    }

    public void updateSavingData(Project project) {
        //if added by g
        if (Objects.isNull(project.getCompletionDate())) {
            return;
        }
        DateFormat formater = new SimpleDateFormat("MM");
        DateFormat formater2 = new SimpleDateFormat("yyyy");
        Calendar beginCalendar = Calendar.getInstance();
        beginCalendar.setTime(project.getCompletionDate());
        beginCalendar.add(Calendar.DATE, 1);

        Calendar finishCalendar = Calendar.getInstance();
        finishCalendar.setTime(beginCalendar.getTime());
        finishCalendar.add(Calendar.YEAR, 1);

        if (finishCalendar.after(new Date())) {
            finishCalendar.setTime(new Date());
            finishCalendar.add(Calendar.DATE, -1);
        }
        Integer totalDaysWorking = getdaysBetweenTwoDates(beginCalendar.getTime(), finishCalendar.getTime());
        List<ProjectFMatrixSavings> projectFMatrixSavingsList = projectFMatrixSavingsRepository.findByProject(project);
        Integer savingPerDay = (int) (project.getYearlyPotential() / 365);
        if (totalDaysWorking > 0) {
            while (beginCalendar.before(finishCalendar)) {
                String month = formater.format(beginCalendar.getTime());
                String year = formater2.format(beginCalendar.getTime());
                ProjectFMatrixSavings projectFMatrixSavings = new ProjectFMatrixSavings();
                for (ProjectFMatrixSavings fMatrixSavings : projectFMatrixSavingsList) {
                    if (fMatrixSavings.getMonth() == Integer.parseInt(month) && fMatrixSavings.getYear() == Integer.parseInt(month)) {
                        projectFMatrixSavings = fMatrixSavings;
                        projectFMatrixSavingsList.remove(fMatrixSavings);
                        break;
                    }
                }
                projectFMatrixSavings.setMonth(Integer.parseInt(month));
                projectFMatrixSavings.setYear(Integer.parseInt(year));

                Factorization factorization = factorizationRepository.findOneByFactorYearAndFactorMonth(projectFMatrixSavings.getYear(), projectFMatrixSavings.getMonth());
                Calendar endOfMonth = Calendar.getInstance();
                endOfMonth.setTime(beginCalendar.getTime());
                endOfMonth.set(Calendar.DAY_OF_MONTH, endOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
                if (endOfMonth.getTimeInMillis() > (new Date()).getTime()) {
                    if (month.equals(formater.format(new Date())) && year.equals(formater2.format(new Date()))) {
                        endOfMonth.setTime(new Date());
                    } else {
                        endOfMonth.setTime(beginCalendar.getTime());
                    }

                }

                Integer monthDaysWorking = getdaysBetweenTwoDates(beginCalendar.getTime(), endOfMonth.getTime());

                projectFMatrixSavings.setProjectedSavingValue((int) (savingPerDay * monthDaysWorking));
                if (factorization != null && factorization.getFactorValue() != 0) {
                    projectFMatrixSavings.setActualSavingValue((int) (savingPerDay * monthDaysWorking * factorization.getFactorValue()));
                } else {
                    projectFMatrixSavings.setActualSavingValue((int) (savingPerDay * monthDaysWorking));
                }
                projectFMatrixSavings.setProject(project);
                projectFMatrixSavingsRepository.save(projectFMatrixSavings);
                beginCalendar.add(Calendar.MONTH, 1);
                beginCalendar.set(Calendar.DAY_OF_MONTH, beginCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            }
        }
        for (ProjectFMatrixSavings fMatrixSavings : projectFMatrixSavingsList) {
            projectFMatrixSavingsRepository.delete(fMatrixSavings);
        }

    }

    public int getdaysBetweenTwoDates(Date date1, Date date2) {
        //		Calendar day1 = Calendar.getInstance();
        //		Calendar day2 = Calendar.getInstance(); 
        //		day1.setTime(date1);
        //		day2.setTime(date2);
        //		int daysBetween = day1.get(Calendar.DAY_OF_YEAR) - day2.get(Calendar.DAY_OF_YEAR);  
        //		return daysBetween;

        int daysdiff = 0;
        long diff = date2.getTime() - date1.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        daysdiff = (int) diffDays;
        return daysdiff;
    }

}

class ProjectWeek {

    private Date startWeekDate;
    private Date endWeekDate;
    private String startWeekDateStr;
    private String endWeekDateStr;

    public Date getStartWeekDate() {
        return startWeekDate;
    }

    public void setStartWeekDate(Date startWeekDate) {
        this.startWeekDate = startWeekDate;
    }

    public Date getEndWeekDate() {
        return endWeekDate;
    }

    public void setEndWeekDate(Date endWeekDate) {
        this.endWeekDate = endWeekDate;
    }

    public String getStartWeekDateStr() {
        return startWeekDateStr;
    }

    public void setStartWeekDateStr(String startWeekDateStr) {
        this.startWeekDateStr = startWeekDateStr;
    }

    public String getEndWeekDateStr() {
        return endWeekDateStr;
    }

    public void setEndWeekDateStr(String endWeekDateStr) {
        this.endWeekDateStr = endWeekDateStr;
    }

}
