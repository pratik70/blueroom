package com.blueroom.importdata;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.Pillar;
import com.blueroom.entities.SkillInventryEntity;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.repository.MethodsAndToolsRepository;
import com.blueroom.repository.PillarRepository;
import com.blueroom.repository.SkillInventryEntityRepository;
import com.blueroom.vm.MessageVM;

@Service
public class AdvancedPillarSpecificService {

	@Autowired
	PillarRepository pillarRepository;
	
	@Autowired
	AuthUserRepository authUserRepository;
	
	@Autowired
	MethodsAndToolsRepository methodsAndToolsRepository;
	
	@Autowired
	SkillInventryEntityRepository skillInventryEntityRepository;
	
	static Connection connection = null;
	static PreparedStatement preparedStatement = null;
	String filePath = "/home/mnt/govind/Skill_Inventory_Updated_Data.xlsx";
	
	public MessageVM loadAdvancedpillarData(){
		MessageVM messageVM = new MessageVM();
		long startTime= System.nanoTime();
		messageVM = updateExcelPath(filePath);
		long endTime= System.nanoTime();
		long totalTime=endTime-startTime;
		System.out.println("\n\nsuccessfully completed in " + totalTime / 1000000000);
		return messageVM;
	}

	private MessageVM updateExcelPath(String excelfilePath) {
	
		try {
			FileInputStream file = new FileInputStream(new File(excelfilePath));
			Workbook workbook=WorkbookFactory.create(new FileInputStream(excelfilePath));
			if(workbook instanceof HSSFWorkbook){
				workbook= new HSSFWorkbook(file);
			}else if (workbook instanceof XSSFWorkbook) {
				workbook= new XSSFWorkbook(file);
			}else{
	              throw new IllegalArgumentException("The specified file is not a Excel file");
	        }
			
			XSSFSheet sheet=(XSSFSheet) workbook.getSheetAt(2);
			Row headerRow = sheet.getRow(0);
			long count=32397;
			long actual_skill = 0;
			for(int i=1;i<=sheet.getLastRowNum();i++){
			//	System.out.println(i);
				Row row;
				String pillarCode=null;
				row=sheet.getRow(i);
				String methodAndToolsType = row.getCell(5).toString();
				if(methodAndToolsType.equals("Actual")){
					Pillar pillar=null;
					SkillInventryEntity skillInventryEntity=null;
					pillarCode = row.getCell(3).toString();
					if(pillarCode==null || pillarCode.equals("")){
						 continue;
					 }
					 if(pillarCode.equals("EEM")  || pillarCode.equals("EPM") ){
						 pillarCode="EPM / EEM";	
					 }
					 else if(pillarCode.equals("Energy & PM")){
						 pillarCode="PM";	
					 }else if(pillarCode.equals("Environment")){
						 pillarCode="ENV";	
					 }else if(pillarCode.equals("Logistics")){
						 pillarCode="LOG";	
					 }
					 
					 pillar = pillarRepository.findOneByCode(pillarCode);
					 
					 if(Objects.isNull(pillar)){
						 pillar = pillarRepository.findOneByPillarName(pillarCode);
					}
					 //System.out.println(i+"  levelFirstTool=="+pillar.getId());
				/////////////////////////////////////////////////////////////
					long employeeId = (long)row.getCell(6).getNumericCellValue();
					String employeeName = row.getCell(7).toString();

						AuthUser authuser=null;
						MethodsAndTools methodsAndTools=null;
						 authuser = authUserRepository.findOneByemployeeId(employeeId);
							if(Objects.isNull(authuser) )
							{
								authuser = !authUserRepository.findOneByName(employeeName).isEmpty() && authUserRepository.findOneByName(employeeName).size() == 1 ? authUserRepository.findOneByName(employeeName).get(0) : null ;
							}
						if(Objects.nonNull(authuser) && Objects.nonNull(pillar))
						for(int j=8;j<=121;j++){
							
							String levelFirstTool;
							if(headerRow.getCell(j)==null)
							{
								levelFirstTool = null;
							}
							else{
								levelFirstTool=headerRow.getCell(j).toString();
								 if(methodsAndToolsRepository.findOneByLevelFirstTool(levelFirstTool).size() == 1){
									methodsAndTools =	methodsAndToolsRepository.findOneByLevelFirstTool(levelFirstTool).get(0);
								}
							}
							
							if(Objects.isNull(methodsAndTools)){
								//	System.out.println("M & T not found == "+ i + " * " + j);
								}else{
									//System.out.println("M & T found == ");
								}
						
							
							if(row.getCell(j)==null){
								actual_skill = 0;
							}
							else{
								actual_skill=(long) row.getCell(j).getNumericCellValue(); 
							}
							//System.out.println("actual_skill=="+actual_skill);
							if(Objects.nonNull(authuser) && Objects.nonNull(pillar) && Objects.nonNull(methodsAndTools)){
								/*							 skillInventryEntity= new SkillInventryEntity();
								skillInventryEntity.setAuthUser(authuser);
								skillInventryEntity.setMethodsTools(methodsAndTools);
								skillInventryEntity.setPillar(pillar);
								skillInventryEntity.setActual_skill_id(actual_skill);
								System.out.println("saved  "+count);	
								skillInventryEntityRepository.save(skillInventryEntity);
	*/
								skillInventryEntity= new SkillInventryEntity();
								// skillInventryEntity.setId(count);
								 skillInventryEntity.setActual_skill_id(actual_skill);
								 //skillInventryEntity.setEmployeeId(methodsAndTools.getId());
								 skillInventryEntity.setMethodsTools(methodsAndTools);
								 
								 //skillInventryEntity.setUserPillarId(pillar.getId());
							
								 skillInventryEntity.setPillar(pillar);
						//	System.out.println(skillInventryEntity.getPillar().getId());	 
								 //skillInventryEntity.setUserId(authuser.getId());
								 skillInventryEntity.setAuthUser(authuser);
								 skillInventryEntityRepository.save(skillInventryEntity);
								
								 count++;
									
							}
						}
						
						
	
				
				}
			}//for i
			
			
			
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
}
