package com.blueroom.entities.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
/*
@Entity
@Table(name="tbl_user_master")
public class AuthUser {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="profile_img")
	private String profileImage;
	
	
	@Column(name="etu_level")
	private Long etuLevel;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getEtuLevel() {
		return etuLevel;
	}
	public void setEtuLevel(Long etuLevel) {
		this.etuLevel = etuLevel;
	}
	
	public String getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
}*/
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import com.blueroom.entities.ETU;
import com.blueroom.entities.Pillar;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tbl_user_master")
public class AuthUser implements UserDetails {

    private static final long serialVersionUID = 2097496846486828450L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private String email;

    private String gender;
    
	@Column(name="first_joining_date")
    private Date first_joining_date;
    
	@Column(name="fiapl_joining_date")
    private Date fiapl_joining_date;
    
	@Column(name="educational_background")
    private String educational_background;
    
	@Column(name="internal_trainer")
    private String internal_trainer;
    
	@Column(name="preffered_learning_style")
    private String preffered_learning_style;
    
    @Column(name = "employee_id", unique = true)
    private Long employeeId;
    
    @Column(name = "type")
    private String type = "blueroom";

    private String nationality;

    private String city;

    private int age;

    @Column(name = "name")
    private String name;

    @Column(name = "profile_img")
    private String profileImg;

    @Column(name = "etu_level")
    private Long etuLevel;

    private List<ETU> etuList = new ArrayList<>();
    
    @JsonIgnore //NEED TO DISCUS 
    private List<Pillar> pillarList = new ArrayList<>();

    private Date createdDate;

    private boolean enabled;

    private String status;
    private String approval;

    private List<Role> roles = new ArrayList<Role>();

    @Transient
    private List<PermissionMatrix> permissions = new ArrayList<PermissionMatrix>();

    @Transient
    public List<PermissionMatrix> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionMatrix> permissions) {
        this.permissions = permissions;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Transient
    @Transactional
    public Collection<GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
        roles.addAll(getRoles());
        return roles;
    }

    @Transient
    public boolean isAccountNonExpired() {
        return true;
    }

    @Transient
    public boolean isAccountNonLocked() {
        return true;
    }

    @Transient
    public boolean isCredentialsNonExpired() {
        return true;
    }


    /* non UserDetails methods */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "email_id", unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "userrole",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    // ============================= Commented below as it is WIP for ACL =========================//
    /*
    public List<Group> groups = new ArrayList<Group>();
    
    public List<PermissionMatrix> permissionMatrix = new ArrayList<PermissionMatrix>();
	
    @Transient
    public Map<String, Integer> privResourceMap;
    
    
    @ManyToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="user_id")
    public List<Group> getGroups() { return groups; }
    
    public void setGroups(List<Group> groups) {
    	this.groups = groups;
    }
    
    @OneToMany
    @JoinTable(
	       joinColumns=@JoinColumn(name="user_id"),
	       inverseJoinColumns=@JoinColumn(name="permisionmatrix_id"))
    public List<PermissionMatrix> getPermissionMatrix() { return permissionMatrix; }
    
    public void setPermissionMatrix(List<PermissionMatrix> permissionMatrix) {
    	this.permissionMatrix = permissionMatrix;
    }
   
     */
    //@Transient
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public AuthUser() {
    }

    public AuthUser(String username, String password, List<Role> role, boolean enabled) {
        this.username = username;
        this.password = password;
        this.roles = role;
        this.enabled = enabled;

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public Long getEtuLevel() {
        return etuLevel;
    }

    public void setEtuLevel(Long etuLevel) {
        this.etuLevel = etuLevel;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "tbl_user_etu_map", joinColumns = {
        @JoinColumn(name = "user_id")}, inverseJoinColumns = {
        @JoinColumn(name = "etu_id")})
    public List<ETU> getEtuList() {
        return etuList;
    }

    public void setEtuList(List<ETU> etuList) {
        this.etuList = etuList;
    }

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "tbl_user_pillar_map", joinColumns = {
        @JoinColumn(name = "user_id")}, inverseJoinColumns = {
        @JoinColumn(name = "pillar_id")})
    
    public List<Pillar> getPillarList() {
        return pillarList;
    }

    public void setPillarList(List<Pillar> pillarList) {
        this.pillarList = pillarList;
    }

	public Date getFirst_joining_date() {
		return first_joining_date;
	}

	public void setFirst_joining_date(Date first_joining_date) {
		this.first_joining_date = first_joining_date;
	}

	public Date getFiapl_joining_date() {
		return fiapl_joining_date;
	}

	public void setFiapl_joining_date(Date fiapl_joining_date) {
		this.fiapl_joining_date = fiapl_joining_date;
	}

	public String getEducational_background() {
		return educational_background;
	}

	public void setEducational_background(String educational_background) {
		this.educational_background = educational_background;
	}

	

	public String getInternal_trainer() {
		return internal_trainer;
	}

	public void setInternal_trainer(String internal_trainer) {
		this.internal_trainer = internal_trainer;
	}

	public String getPreffered_learning_style() {
		return preffered_learning_style;
	}

	public void setPreffered_learning_style(String preffered_learning_style) {
		this.preffered_learning_style = preffered_learning_style;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
