package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name = "tbl_development_plan")
public class DevelopmentPlan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private AuthUser user;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "competency_id")
	private Competencies competency;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tool_id")
	private DevelopmentTool tool;
	
	@Column(name = "type")
	private String type;

	@Column(name = "year")
	private Integer year;

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}

	public Competencies getCompetency() {
		return competency;
	}

	public void setCompetency(Competencies competency) {
		this.competency = competency;
	}

	public DevelopmentTool getTool() {
		return tool;
	}

	public void setTool(DevelopmentTool tool) {
		this.tool = tool;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
