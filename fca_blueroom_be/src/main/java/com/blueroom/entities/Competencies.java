package com.blueroom.entities;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_ma_competencies")
public class Competencies {
	
	   @Id
	   @GeneratedValue(strategy = GenerationType.AUTO)
	   @Column(name = "id", nullable = false)
	   private Long id;

	   @Column(name = "name")
	   private String name;

	   @Column(name = "area")
	   private String area;
	   
	   @Column(name = "type")
	   private String type;
	   
	   @Column(name = "created_on")
	   private Date createdOn = new Date();
	   
	   @Column(name = "status")
	   private Boolean status=true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
}
