package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name = "tbl_ma_pillar")
public class Pillar {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "name")
	private String pillerName;

	@Column(name = "code")
	private String code;

	@Column(name = "status")
	private boolean status = true;

	@ManyToOne
	@JoinColumn(name = "manager")
	private AuthUser manager;

	/*
	 * @ManyToMany
	 * 
	 * @JoinTable(name = "pillar_team", joinColumns = { @JoinColumn(name =
	 * "pillar_id") }, inverseJoinColumns = { @JoinColumn(name = "user_id") })
	 * private Set<AuthUser> teams = new HashSet<AuthUser>();
	 * 
	 * public Set<AuthUser> getTeams() { return teams; }
	 * 
	 * public void setTeams(Set<AuthUser> teams) { this.teams = teams; }
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPillerName() {
		return pillerName;
	}

	public void setPillerName(String pillerName) {
		this.pillerName = pillerName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AuthUser getManager() {
		return manager;
	}

	public void setManager(AuthUser manager) {
		this.manager = manager;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean getStatus() {
		return status;
	}

}
