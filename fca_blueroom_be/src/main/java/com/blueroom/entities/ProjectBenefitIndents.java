package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_project_benefit_indents")
public class ProjectBenefitIndents {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	private Project project;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "benifit_id")
	private Benifit benifit;
	
	
	@Column(name="benifit_value")
	private Integer benifitValue;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Project getProject() {
		return project;
	}


	public void setProject(Project project) {
		this.project = project;
	}


	public Benifit getBenifit() {
		return benifit;
	}


	public void setBenifit(Benifit benifit) {
		this.benifit = benifit;
	}


	public Integer getBenifitValue() {
		return benifitValue;
	}


	public void setBenifitValue(Integer benifitValue) {
		this.benifitValue = benifitValue;
	}
	
	
	

}
