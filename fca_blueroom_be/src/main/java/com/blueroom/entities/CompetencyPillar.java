package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "competency_pillar_mapping")
public class CompetencyPillar {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "competency_id")
	private Competencies competency;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pillar_id")
	private Pillar pillar;
	
	@Column(name="current_level")
	private Long current_level;
	
	@Column(name="required_level")
	private Long required_level;
	
	@Column(name="last_year_level")
	private Long last_year_level;
	
	@Column(name = "status")
	private boolean status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Competencies getCompetency() {
		return competency;
	}

	public void setCompetency(Competencies competency) {
		this.competency = competency;
	}

	public Pillar getPillar() {
		return pillar;
	}

	public void setPillar(Pillar pillar) {
		this.pillar = pillar;
	}

	public Long getCurrent_level() {
		return current_level;
	}

	public void setCurrent_level(Long current_level) {
		this.current_level = current_level;
	}

	public Long getRequired_level() {
		return required_level;
	}

	public void setRequired_level(Long required_level) {
		this.required_level = required_level;
	}

	public Long getLast_year_level() {
		return last_year_level;
	}

	public void setLast_year_level(Long last_year_level) {
		this.last_year_level = last_year_level;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	
}
