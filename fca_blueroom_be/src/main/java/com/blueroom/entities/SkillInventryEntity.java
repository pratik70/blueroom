package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;


@Entity
@Table(name = "tbl_tool_skillset_user")
public class SkillInventryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;
	
	/*@Column(name = "user_pillar_id")
	private Long userPillarId;
	*/

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pillar_id")
	private Pillar pillar;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private AuthUser authUser;
	
/*	@Column(name = "user_id")
	private Long userId;*/
	
	@Column(name="actual_skill")
	private Long actual_skill_id;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "method_tool_id")
	private MethodsAndTools methodsTools;

	
/*	@Column(name="method_tool_id")
	private Long employeeId;
*/
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

/*	public Long getUserPillarId() {
		return userPillarId;
	}

	public void setUserPillarId(Long userPillarId) {
		this.userPillarId = userPillarId;
	}*/

/*	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}*/

	public Long getActual_skill_id() {
		return actual_skill_id;
	}

	public void setActual_skill_id(Long actual_skill_id) {
		this.actual_skill_id = actual_skill_id;
	}

	public AuthUser getAuthUser() {
		return authUser;
	}

	public void setAuthUser(AuthUser authUser) {
		this.authUser = authUser;
	}

	public MethodsAndTools getMethodsTools() {
		return methodsTools;
	}

	public void setMethodsTools(MethodsAndTools methodsTools) {
		this.methodsTools = methodsTools;
	}

	public Pillar getPillar() {
		return pillar;
	}

	public void setPillar(Pillar pillar) {
		this.pillar = pillar;
	}

	
/*	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	*/
	
	
	
	
	
	
	
}
