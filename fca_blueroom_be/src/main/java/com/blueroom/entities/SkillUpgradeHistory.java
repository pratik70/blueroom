package com.blueroom.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name = "tbl_ma_skill_upgrade_history")
public class SkillUpgradeHistory {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "ACTUAL_SKILL")
    private int actualSkill;

    @Column(name = "UPGRADE_SKILL")
    private int upgradeSkill;

    @Column(name = "COMMENT")
    private String comment;

    @Column(name = "UPDATED_ON")
    private Date updatedOn;

    @Column(name = "CREATED_ON")
    private Date createdOn;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private AuthUser user;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "updated_by")
    private AuthUser updated_by;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getActualSkill() {
        return actualSkill;
    }

    public void setActualSkill(int actualSkill) {
        this.actualSkill = actualSkill;
    }

    public int getUpgradeSkill() {
        return upgradeSkill;
    }

    public void setUpgradeSkill(int upgradeSkill) {
        this.upgradeSkill = upgradeSkill;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public AuthUser getUser() {
        return user;
    }

    public void setUser(AuthUser user) {
        this.user = user;
    }

    public AuthUser getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(AuthUser updated_by) {
        this.updated_by = updated_by;
    }

}
