package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_ma_technicaltool")
public class TechnicalTool {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;


	@Column(name="profile_name")
	private String profileName;

	@Column(name="technical_tool_description")
	private String technicalToolDescription;

	@Column(name="required_skill")
	private int required_skill;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getTechnicalToolDescription() {
		return technicalToolDescription;
	}

	public void setTechnicalToolDescription(String technicalToolDescription) {
		this.technicalToolDescription = technicalToolDescription;
	}

	public int getRequired_skill() {
		return required_skill;
	}

	public void setRequired_skill(int required_skill) {
		this.required_skill = required_skill;
	}

	
}
