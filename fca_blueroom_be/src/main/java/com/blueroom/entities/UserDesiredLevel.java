package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name = "user_desired_level")
public class UserDesiredLevel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;   

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "competencies_id")
	private Competencies competencies;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
	private AuthUser user;
    
    @Column(name = "year")
    private Integer year;
    
    @Column(name = "planStatus")
    private String planStatus;
    
    public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Column(name = "actual_skill")
    private Long actualSkill;
    
    @Column(name = "required_skill")
    private Long requiredSkill;

    @Column(name = "status")
    private boolean status;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Competencies getCompetencies() {
		return competencies;
	}

	public void setCompetencies(Competencies competencies) {
		this.competencies = competencies;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Long getActualSkill() {
		return actualSkill;
	}

	public void setActualSkill(Long actualSkill) {
		this.actualSkill = actualSkill;
	}

	public Long getRequiredSkill() {
		return requiredSkill;
	}

	public void setRequiredSkill(Long requiredSkill) {
		this.requiredSkill = requiredSkill;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}
    
}
