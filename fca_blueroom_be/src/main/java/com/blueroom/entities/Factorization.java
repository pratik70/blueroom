package com.blueroom.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_factorization")
public class Factorization {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "plan_value")
    private Double planValue;

    @Column(name = "actual_value")
    private Double actualValue;

    @Column(name = "factor_value")
    private Double factorValue;

    @Column(name = "factor_year")
    private Integer factorYear;

    @Column(name = "factor_month")
    private Integer factorMonth;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "etu_id")
    private ETU etu;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPlanValue() {
        return planValue;
    }

    public void setPlanValue(Double planValue) {
        this.planValue = planValue;
    }

    public Double getActualValue() {
        return actualValue;
    }

    public void setActualValue(Double actualValue) {
        this.actualValue = actualValue;
    }

    public Double getFactorValue() {
        return factorValue;
    }

    public void setFactorValue(Double factorValue) {
        this.factorValue = factorValue;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getFactorYear() {
        return factorYear;
    }

    public void setFactorYear(Integer factorYear) {
        this.factorYear = factorYear;
    }

    public Integer getFactorMonth() {
        return factorMonth;
    }

    public void setFactorMonth(Integer factorMonth) {
        this.factorMonth = factorMonth;
    }

    public ETU getEtu() {
        return etu;
    }

    public void setEtu(ETU etu) {
        this.etu = etu;
    }

}
