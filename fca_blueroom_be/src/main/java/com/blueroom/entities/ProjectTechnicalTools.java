package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="tbl_project_technical_tools")
public class ProjectTechnicalTools {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	private Project project;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "technical_tool_id")
	private TechnicalTool technicalTool;

	@Column(name="required_skill")
	private Integer requiredSkill;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public TechnicalTool getTechnicalTool() {
		return technicalTool;
	}

	public void setTechnicalTool(TechnicalTool technicalTool) {
		this.technicalTool = technicalTool;
	}

	public Integer getRequiredSkill() {
		return requiredSkill;
	}

	public void setRequiredSkill(Integer requiredSkill) {
		this.requiredSkill = requiredSkill;
	}

	
}
