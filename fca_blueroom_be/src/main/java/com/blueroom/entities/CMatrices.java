package com.blueroom.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_cmatrices")
public class CMatrices {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;

	@Column(name="description")
	private String description;

	@Column(name="total_loss_value")
	private Integer totalLossValue;
	
	
	@Column(name="direct_labour")
	private Integer directLabour;
	
	
	@Column(name="indirect_labour")
	private Integer indirectLabour;
	
	@Column(name="salaried_payroll")
	private Integer salariedPayroll;
	
	@Column(name="indirect_material")
	private Integer indirectMaterial;
	
	@Column(name="manintenance_material")
	private Integer manintenanceMaterial;
	
	@Column(name="scrap")
	private Integer scrap;
	
	@Column(name="energy")
	private Integer energy;
	
	@Column(name="other_services")
	private Integer otherServices;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "machine_id")
	private Machine machine;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "process_id")
	private Process process;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "etu_id")
	private ETU etu;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "loss_type_id")
	private LossType lossType;

	@Column(name = "data_date")
	private Date dataDate;

	@Column(name = "created_at") 
	private Date createdAt;

	@Column(name = "updated_at")
	private Date updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getTotalLossValue() {
		return totalLossValue;
	}

	public void setTotalLossValue(Integer totalLossValue) {
		this.totalLossValue = totalLossValue;
	}

	public Machine getMachine() {
		return machine;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	public ETU getEtu() {
		return etu;
	}

	public void setEtu(ETU etu) {
		this.etu = etu;
	}

	public LossType getLossType() {
		return lossType;
	}

	public void setLossType(LossType lossType) {
		this.lossType = lossType;
	}

	public Date getDataDate() {
		return dataDate;
	}

	public void setDataDate(Date dataDate) {
		this.dataDate = dataDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getDirectLabour() {
		return directLabour;
	}

	public void setDirectLabour(Integer directLabour) {
		this.directLabour = directLabour;
	}

	public Integer getIndirectLabour() {
		return indirectLabour;
	}

	public void setIndirectLabour(Integer indirectLabour) {
		this.indirectLabour = indirectLabour;
	}

	public Integer getSalariedPayroll() {
		return salariedPayroll;
	}

	public void setSalariedPayroll(Integer salariedPayroll) {
		this.salariedPayroll = salariedPayroll;
	}

	public Integer getIndirectMaterial() {
		return indirectMaterial;
	}

	public void setIndirectMaterial(Integer indirectMaterial) {
		this.indirectMaterial = indirectMaterial;
	}

	public Integer getManintenanceMaterial() {
		return manintenanceMaterial;
	}

	public void setManintenanceMaterial(Integer manintenanceMaterial) {
		this.manintenanceMaterial = manintenanceMaterial;
	}

	public Integer getScrap() {
		return scrap;
	}

	public void setScrap(Integer scrap) {
		this.scrap = scrap;
	}

	public Integer getEnergy() {
		return energy;
	}

	public void setEnergy(Integer energy) {
		this.energy = energy;
	}

	public Integer getOtherServices() {
		return otherServices;
	}

	public void setOtherServices(Integer otherServices) {
		this.otherServices = otherServices;
	}

}
