/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.entities.masterdata;

import com.blueroom.entities.ETU;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author govind
 */
@Entity
@Table(name = "tbl_daily_basis_loss")
public class DailyBasisLoss implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lossDate;

    @Column(nullable = false)
    private String shift;

    @Column(nullable = false)
    private String opNo;

    @Column(nullable = false)
    private String lossDescription;

    @Column(nullable = false)
    private Long lossInMinutes = 0L;

    private Long scrapQuantitySameOP = 0L;

    private Long indirectMaterialCost = 0L;

    private Long maintenanceMaterialCost = 0L;

    private Long others = 0L;

    private Long scrapQuantityOtherOP = 0L;

    @ManyToOne
    @JoinColumn(name = "daily_basis_loss_type", nullable = false)
    private DailyBasisLossType dailyBasisLossType;

    @ManyToOne
    @JoinColumn(name = "etu_id", nullable = false)
    private ETU etu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getLossDate() {
        return lossDate;
    }

    public void setLossDate(Date lossDate) {
        this.lossDate = lossDate;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getOpNo() {
        return opNo;
    }

    public void setOpNo(String opNo) {
        this.opNo = opNo;
    }

    public String getLossDescription() {
        return lossDescription;
    }

    public void setLossDescription(String lossDescription) {
        this.lossDescription = lossDescription;
    }

    public Long getLossInMinutes() {
        return lossInMinutes;
    }

    public void setLossInMinutes(Long lossInMinutes) {
        this.lossInMinutes = lossInMinutes;
    }

    public Long getScrapQuantitySameOP() {
        return scrapQuantitySameOP;
    }

    public void setScrapQuantitySameOP(Long scrapQuantitySameOP) {
        this.scrapQuantitySameOP = scrapQuantitySameOP;
    }

    public Long getIndirectMaterialCost() {
        return indirectMaterialCost;
    }

    public void setIndirectMaterialCost(Long indirectMaterialCost) {
        this.indirectMaterialCost = indirectMaterialCost;
    }

    public Long getMaintenanceMaterialCost() {
        return maintenanceMaterialCost;
    }

    public void setMaintenanceMaterialCost(Long maintenanceMaterialCost) {
        this.maintenanceMaterialCost = maintenanceMaterialCost;
    }

    public Long getOthers() {
        return others;
    }

    public void setOthers(Long others) {
        this.others = others;
    }

    public Long getScrapQuantityOtherOP() {
        return scrapQuantityOtherOP;
    }

    public void setScrapQuantityOtherOP(Long scrapQuantityOtherOP) {
        this.scrapQuantityOtherOP = scrapQuantityOtherOP;
    }

    public DailyBasisLossType getDailyBasisLossType() {
        return dailyBasisLossType;
    }

    public void setDailyBasisLossType(DailyBasisLossType dailyBasisLossType) {
        this.dailyBasisLossType = dailyBasisLossType;
    }

    public ETU getEtu() {
        return etu;
    }

    public void setEtu(ETU etu) {
        this.etu = etu;
    }

    @PrePersist
    public void prePersist() {
        if (this.getCreatedAt() == null) {
            this.setCreatedAt(new Date());
        }
        if (this.getUpdatedAt() == null) {
            this.setUpdatedAt(new Date());
        }
    }

    @PreUpdate
    public void preUpdate() {
        this.setUpdatedAt(new Date());
    }
}
