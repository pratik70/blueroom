/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.entities.masterdata;

import com.blueroom.entities.ETU;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author govind
 */
@Entity
@Table(name = "tbl_master_data")
public class MasterData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "etu_id")
    private ETU etu;

    @Column(nullable = false)
    Double directLabourCost;

    @Column(nullable = false)
    Double indirectLabourCost;

    @Column(nullable = false)
    Double salariedCost;

    @Column(nullable = false)
    Double energyCost;

    @Column(nullable = false)
    Double energyFactor;

    @Column(nullable = false)
    Long noOfLabour;

    @Column(nullable = false)
    Double totalConnecedLoad;

    @Column(nullable = false)
    Double cycleTime;

    @Column(nullable = false)
    Double machineScrap;

    @Column(nullable = false)
    Double materialScrap;

    @Column(nullable = false)
    Long noOfWhiteCollar;

    @Column(nullable = false)
    Double toolCostPerPart;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ETU getEtu() {
        return etu;
    }

    public void setEtu(ETU etu) {
        this.etu = etu;
    }

    public Double getDirectLabourCost() {
        return directLabourCost;
    }

    public void setDirectLabourCost(Double directLabourCost) {
        this.directLabourCost = directLabourCost;
    }

    public Double getIndirectLabourCost() {
        return indirectLabourCost;
    }

    public void setIndirectLabourCost(Double indirectLabourCost) {
        this.indirectLabourCost = indirectLabourCost;
    }

    public Double getSalariedCost() {
        return salariedCost;
    }

    public void setSalariedCost(Double salariedCost) {
        this.salariedCost = salariedCost;
    }

    public Double getEnergyCost() {
        return energyCost;
    }

    public void setEnergyCost(Double energyCost) {
        this.energyCost = energyCost;
    }

    public Double getEnergyFactor() {
        return energyFactor;
    }

    public void setEnergyFactor(Double energyFactor) {
        this.energyFactor = energyFactor;
    }

    public Long getNoOfLabour() {
        return noOfLabour;
    }

    public void setNoOfLabour(Long noOfLabour) {
        this.noOfLabour = noOfLabour;
    }

    public Double getTotalConnecedLoad() {
        return totalConnecedLoad;
    }

    public void setTotalConnecedLoad(Double totalConnecedLoad) {
        this.totalConnecedLoad = totalConnecedLoad;
    }

    public Double getCycleTime() {
        return cycleTime;
    }

    public void setCycleTime(Double cycleTime) {
        this.cycleTime = cycleTime;
    }

    public Double getMachineScrap() {
        return machineScrap;
    }

    public void setMachineScrap(Double machineScrap) {
        this.machineScrap = machineScrap;
    }

    public Double getMaterialScrap() {
        return materialScrap;
    }

    public void setMaterialScrap(Double materialScrap) {
        this.materialScrap = materialScrap;
    }

    public Long getNoOfWhiteCollar() {
        return noOfWhiteCollar;
    }

    public void setNoOfWhiteCollar(Long noOfWhiteCollar) {
        this.noOfWhiteCollar = noOfWhiteCollar;
    }

    public Double getToolCostPerPart() {
        return toolCostPerPart;
    }

    public void setToolCostPerPart(Double toolCostPerPart) {
        this.toolCostPerPart = toolCostPerPart;
    }

    @PrePersist
    public void prePersist() {
        if (this.getCreatedAt() == null) {
            this.setCreatedAt(new Date());
        }
        if (this.getUpdatedAt() == null) {
            this.setUpdatedAt(new Date());
        }
    }

    @PreUpdate
    public void preUpdate() {
        this.setUpdatedAt(new Date());
    }
}
