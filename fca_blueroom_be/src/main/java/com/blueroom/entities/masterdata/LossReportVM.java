/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.entities.masterdata;

import java.util.Map;

/**
 *
 * @author govind
 */
public class LossReportVM {

    private DailyBasisLossType dailyBasisLossType;
    private Double total;
    private Map<String, Object> causulLoss;
    private Map<String, Object> resultantLoss;

    public DailyBasisLossType getDailyBasisLossType() {
        return dailyBasisLossType;
    }

    public void setDailyBasisLossType(DailyBasisLossType dailyBasisLossType) {
        this.dailyBasisLossType = dailyBasisLossType;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Map<String, Object> getCausulLoss() {
        return causulLoss;
    }

    public void setCausulLoss(Map<String, Object> causulLoss) {
        this.causulLoss = causulLoss;
    }

    public Map<String, Object> getResultantLoss() {
        return resultantLoss;
    }

    public void setResultantLoss(Map<String, Object> resultantLoss) {
        this.resultantLoss = resultantLoss;
    }

}
