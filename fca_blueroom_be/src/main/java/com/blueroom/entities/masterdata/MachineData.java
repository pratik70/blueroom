/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.entities.masterdata;

import com.blueroom.entities.ETU;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author govind
 */
@Entity
@Table(name = "tbl_machine_data")
public class MachineData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "etu_id")
    private ETU etu;

    @Column(nullable = false)
    String opNo;

    @Column(nullable = false)
    Long noOfMachine = 0L;

    @Column(nullable = false)
    Double factor = 0.0;

    @Column(nullable = false)
    Long noOfOperator = 0L;

    @Column(nullable = false)
    Long noOfKWh = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ETU getEtu() {
        return etu;
    }

    public void setEtu(ETU etu) {
        this.etu = etu;
    }

    public String getOpNo() {
        return opNo;
    }

    public void setOpNo(String opNo) {
        this.opNo = opNo;
    }

    public Long getNoOfMachine() {
        return noOfMachine;
    }

    public void setNoOfMachine(Long noOfMachine) {
        this.noOfMachine = noOfMachine;
    }

    public Double getFactor() {
        return factor;
    }

    public void setFactor(Double factor) {
        this.factor = factor;
    }

    public Long getNoOfOperator() {
        return noOfOperator;
    }

    public void setNoOfOperator(Long noOfOperator) {
        this.noOfOperator = noOfOperator;
    }

    public Long getNoOfKWh() {
        return noOfKWh;
    }

    public void setNoOfKWh(Long noOfKWh) {
        this.noOfKWh = noOfKWh;
    }

    @PrePersist
    public void prePersist() {
        if (this.getCreatedAt() == null) {
            this.setCreatedAt(new Date());
        }
        setDefaultData();
    }

    @PreUpdate
    public void preUpdate() {
        setDefaultData();
    }

    public void setDefaultData() {
        this.setUpdatedAt(new Date());

        if (this.getNoOfMachine() == null) {
            this.setNoOfMachine(0L);
        }
        if (this.getFactor() == null) {
            this.setFactor(0.0);
        }
        if (this.getNoOfOperator() == null) {
            this.setNoOfOperator(0L);
        }
        if (this.getNoOfKWh() == null) {
            this.setNoOfKWh(0L);
        }
    }

}
