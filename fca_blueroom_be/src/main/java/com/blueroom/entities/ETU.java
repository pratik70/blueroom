package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_ma_etu")
public class ETU {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "etu_no")
    private String etuNo;

    @Column(name = "etu_name")
    private String etuName;

    @Column(name = "project_abbrevation")
    private String projectAbbrevation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtuName() {
        return etuName;
    }

    public void setEtuName(String etuName) {
        this.etuName = etuName;
    }

    public String getEtuNo() {
        return etuNo;
    }

    public void setEtuNo(String etuNo) {
        this.etuNo = etuNo;
    }

    public String getProjectAbbrevation() {
        return projectAbbrevation;
    }

    public void setProjectAbbrevation(String projectAbbrevation) {
        this.projectAbbrevation = projectAbbrevation;
    }

}
