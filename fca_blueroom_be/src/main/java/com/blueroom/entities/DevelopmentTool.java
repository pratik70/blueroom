package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_development_tool")
public class DevelopmentTool {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;   
  
    @Column(name = "name")
	   private String name;
    
    @Column(name = "status")
    private Boolean status = true;
    
    
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="method_id")
    private DevelopmentMethod developmentMethod;
	
	public DevelopmentMethod getDevelopmentMethod() {
		return developmentMethod;
	}

	public void setDevelopmentMethod(DevelopmentMethod developmentMethod) {
		this.developmentMethod = developmentMethod;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	
}
