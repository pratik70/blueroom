package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_project_methods_tools")
public class ProjectMethodsAndTools {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;


	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	private Project project;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "methods_tools_id")
	private MethodsAndTools methodsTools;
	
	@Column(name="required_skill")
	private Integer requiredSkill;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public MethodsAndTools getMethodsAndTools() {
		return methodsTools;
	}

	public void setMethodsAndTools(MethodsAndTools methodsAndTools) {
		this.methodsTools = methodsAndTools;
	}

	public Integer getRequiredSkill() {
		return requiredSkill;
	}

	public void setRequiredSkill(Integer requiredSkill) {
		this.requiredSkill = requiredSkill;
	}
	
	
	
}
