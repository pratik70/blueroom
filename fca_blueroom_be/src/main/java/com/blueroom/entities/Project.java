package com.blueroom.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author User
 *
 */
@Entity
@Table(name = "tbl_projects")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "code")
    private String projectCode;

    @Column(name = "yearly_potential")
    private Double yearlyPotential;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "loss_id")
    private Loss lossType;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "op_num")
    private Machine optMachine;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "process")
    private Process process;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "etu_id")
    private ETU etu;

    @Column(name = "preliminary_analysis")
    private String preliminaryAnalysis;

    @Column(name = "description")
    private String description;

    @Column(name = "complete_stage")
    private Integer completeStage;

    @Column(name = "completion_date")
    private Date completionDate;

    @Column(name = "horizontal_expansion")
    private Boolean horizontalExpansion;

    @Column(name = "remark")
    private String remark;

    @Column(name = "direct_loss")
    private Integer directLoss;

    @Column(name = "indirect_loss")
    private Integer indirectLoss;

    @Column(name = "staff_loss")
    private Integer staffLoss;

    @Column(name = "indirect_materials_loss")
    private Integer indirectMaterialsLoss;

    @Column(name = "maintenance_loss")
    private Integer maintenanceLoss;

    @Column(name = "scrap_loss")
    private Integer scrapLoss;

    @Column(name = "energy_loss")
    private Integer energyLoss;

    @Column(name = "expensess_loss")
    private Integer expensessLoss;

    @Column(name = "execution_started")
    private Boolean executionStarted;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "standardisation")
    private Boolean standardisation;

    @Column(name = "standardisation_remark")
    private String standardisationRemark;

    @Column(name = "epmInfo")
    private Boolean epmInfo;

    @Column(name = "epm_info_remark")
    private String epmInfoRemark;

    @Column(name = "document")
    private String document;

    @Column(name = "ew_document")
    private String ewDocument;

    @OneToOne
    @JoinColumn(name = "project_cd")
    private CDYear cDYear;

    @Column(name = "is_project_hide")
	private boolean isProjectHide=false;
    
    @Column(name = "etu_status")
	private boolean etuStatus=false;
    
    @Column(name = "finance_status")
	private boolean financeStatus=false;
    
    @Column(name = "etuMailSend")
    public boolean etuMailSend;
    
    @Column(name = "financeMailSend")
    public boolean financeMailSend;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Double getYearlyPotential() {
        return yearlyPotential;
    }

    public void setYearlyPotential(Double yearlyPotential) {
        this.yearlyPotential = yearlyPotential;
    }

    public Loss getLossType() {
        return lossType;
    }

    public void setLossType(Loss lossType) {
        this.lossType = lossType;
    }

    public Machine getOptMachine() {
        return optMachine;
    }

    public void setOptMachine(Machine optMachine) {
        this.optMachine = optMachine;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public ETU getEtu() {
        return etu;
    }

    public void setEtu(ETU etu) {
        this.etu = etu;
    }

    public String getPreliminaryAnalysis() {
        return preliminaryAnalysis;
    }

    public void setPreliminaryAnalysis(String preliminaryAnalysis) {
        this.preliminaryAnalysis = preliminaryAnalysis;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Integer getCompleteStage() {
        return completeStage;
    }

    public void setCompleteStage(Integer completeStage) {
        this.completeStage = completeStage;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public Boolean getHorizontalExpansion() {
        return horizontalExpansion;
    }

    public void setHorizontalExpansion(Boolean horizontalExpansion) {
        this.horizontalExpansion = horizontalExpansion;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDirectLoss() {
        return directLoss;
    }

    public void setDirectLoss(Integer directLoss) {
        this.directLoss = directLoss;
    }

    public Integer getIndirectLoss() {
        return indirectLoss;
    }

    public void setIndirectLoss(Integer indirectLoss) {
        this.indirectLoss = indirectLoss;
    }

    public Integer getStaffLoss() {
        return staffLoss;
    }

    public void setStaffLoss(Integer staffLoss) {
        this.staffLoss = staffLoss;
    }

    public Integer getIndirectMaterialsLoss() {
        return indirectMaterialsLoss;
    }

    public void setIndirectMaterialsLoss(Integer indirectMaterialsLoss) {
        this.indirectMaterialsLoss = indirectMaterialsLoss;
    }

    public Integer getMaintenanceLoss() {
        return maintenanceLoss;
    }

    public void setMaintenanceLoss(Integer maintenanceLoss) {
        this.maintenanceLoss = maintenanceLoss;
    }

    public Integer getScrapLoss() {
        return scrapLoss;
    }

    public void setScrapLoss(Integer scrapLoss) {
        this.scrapLoss = scrapLoss;
    }

    public Integer getEnergyLoss() {
        return energyLoss;
    }

    public void setEnergyLoss(Integer energyLoss) {
        this.energyLoss = energyLoss;
    }

    public Integer getExpensessLoss() {
        return expensessLoss;
    }

    public void setExpensessLoss(Integer expensessLoss) {
        this.expensessLoss = expensessLoss;
    }

    public Boolean getExecutionStarted() {
        return executionStarted;
    }

    public void setExecutionStarted(Boolean executionStarted) {
        this.executionStarted = executionStarted;
    }

    public Boolean getStandardisation() {
        return standardisation;
    }

    public void setStandardisation(Boolean standardisation) {
        this.standardisation = standardisation;
    }

    public String getStandardisationRemark() {
        return standardisationRemark;
    }

    public void setStandardisationRemark(String standardisationRemark) {
        this.standardisationRemark = standardisationRemark;
    }

    public Boolean getEpmInfo() {
        return epmInfo;
    }

    public void setEpmInfo(Boolean epmInfo) {
        this.epmInfo = epmInfo;
    }

    public String getEpmInfoRemark() {
        return epmInfoRemark;
    }

    public void setEpmInfoRemark(String epmInfoRemark) {
        this.epmInfoRemark = epmInfoRemark;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public CDYear getcDYear() {
        return cDYear;
    }

    public void setcDYear(CDYear cDYear) {
        this.cDYear = cDYear;
    }

    public String getEwDocument() {
        return ewDocument;
    }

    public void setEwDocument(String ewDocument) {
        this.ewDocument = ewDocument;
    }
    
	public boolean isProjectHide() {
		return isProjectHide;
	}

	public void setProjectHide(boolean isProjectHide) {
		this.isProjectHide = isProjectHide;
	}
	
	

    public boolean isEtuStatus() {
		return etuStatus;
	}

	public void setEtuStatus(boolean etuStatus) {
		this.etuStatus = etuStatus;
	}

	public boolean isFinanceStatus() {
		return financeStatus;
	}

	public void setFinanceStatus(boolean financeStatus) {
		this.financeStatus = financeStatus;
	}
	
	

    public boolean isEtuMailSend() {
		return etuMailSend;
	}

	public void setEtuMailSend(boolean etuMailSend) {
		this.etuMailSend = etuMailSend;
	}

	public boolean isFinanceMailSend() {
		return financeMailSend;
	}

	public void setFinanceMailSend(boolean financeMailSend) {
		this.financeMailSend = financeMailSend;
	}

	@Override
    public String toString() {
        return "Project [id=" + id + ", projectName=" + projectName + ", projectCode=" + projectCode
                + ", yearlyPotential=" + yearlyPotential + ", lossType=" + lossType + ", optMachine=" + optMachine
                + ", process=" + process + ", etu=" + etu + ", preliminaryAnalysis=" + preliminaryAnalysis
                + ", description=" + description + ", completeStage=" + completeStage + ", completionDate="
                + completionDate + ", horizontalExpansion=" + horizontalExpansion + ", remark=" + remark
                + ", directLoss=" + directLoss + ", indirectLoss=" + indirectLoss + ", staffLoss=" + staffLoss
                + ", indirectMaterialsLoss=" + indirectMaterialsLoss + ", maintenanceLoss=" + maintenanceLoss
                + ", scrapLoss=" + scrapLoss + ", energyLoss=" + energyLoss + ", expensessLoss=" + expensessLoss
                + ", executionStarted=" + executionStarted + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt
                + ", standardisation=" + standardisation + ", standardisationRemark=" + standardisationRemark
                + ", epmInfo=" + epmInfo + ", epmInfoRemark=" + epmInfoRemark + ", document=" + document + ", cDYear="
                + cDYear + ", ewDocument=" + ewDocument + " ]";
    }

}
