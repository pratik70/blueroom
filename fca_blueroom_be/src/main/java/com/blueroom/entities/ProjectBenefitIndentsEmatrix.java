package com.blueroom.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_project_benefit_indents_ematrix")
public class ProjectBenefitIndentsEmatrix {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	private Project project;
	
	@Column(name="bc")
	private Double bc;
	
	@Column(name="savings_type")
	private String savingsType;
	
	@Column(name="confidence_level")
	private Integer confidenceLevel; 
	
	@Column(name="total_project_cost")
    private Integer totalProjectCost;
	
	@Column(name="total_potential_benefit")
	private Integer	totalPotentialBenefit;
	
	@Column(name="start_date")
	private String startDate;
	
	@Column(name="target_date")
	private String targetDate;
	
	@Column(name="p_planned_start_date")
	private String pPlannedStartDate;
	
	@Column(name="p_planned_end_date")
	private String pPlannedEndDate;
	
	@Column(name="d_planned_start_date")
	private String dPlannedStartDate;
	
	@Column(name="d_planned_end_date")
	private String dPlannedEndDate;
	
	@Column(name="c_planned_start_date")
	private String cPlannedStartDate;
	
	@Column(name="c_planned_end_date")
	private String cPlannedEndDate;
	
	@Column(name="a_planned_start_date")
	private String aPlannedStartDate
	;
	@Column(name="a_planned_end_date")
	private String aPlannedEndDate;
	
	@Column(name="p_actual_start_date")
	private String pActualStartDate;
	
	@Column(name="p_actual_end_date")
	private String pActualEndDate;
	
	@Column(name="d_actual_start_date")
	private String dActualStartDate;
	
	@Column(name="d_actual_end_date")
	private String dActualEndDate;
	
	@Column(name="c_actual_start_date")
	private String cActualStartDate;
	
	@Column(name="c_actual_end_date")
	private String cActualEndDate;
	
	@Column(name="a_actual_start_date")
	private String aActualStartDate;
	
	@Column(name="a_actual_end_date")
	private String aActualEndDate;
	
	@Column(name="created_date")
	private Date createdAt;
	
	@Column(name="updated_date")
	private Date updatedAt;

	@Column(name="completion_date")
	private String completionDate;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Double getBc() {
		return bc;
	}

	public void setBc(Double bc) {
		this.bc = bc;
	}

	public Integer getConfidenceLevel() {
		return confidenceLevel;
	}

	public void setConfidenceLevel(Integer confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	public Integer getTotalProjectCost() {
		return totalProjectCost;
	}

	public void setTotalProjectCost(Integer totalProjectCost) {
		this.totalProjectCost = totalProjectCost;
	}

	public Integer getTotalPotentialBenefit() {
		return totalPotentialBenefit;
	}

	public void setTotalPotentialBenefit(Integer totalPotentialBenefit) {
		this.totalPotentialBenefit = totalPotentialBenefit;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	public String getpPlannedStartDate() {
		return pPlannedStartDate;
	}

	public void setpPlannedStartDate(String pPlannedStartDate) {
		this.pPlannedStartDate = pPlannedStartDate;
	}

	public String getpPlannedEndDate() {
		return pPlannedEndDate;
	}

	public void setpPlannedEndDate(String pPlannedEndDate) {
		this.pPlannedEndDate = pPlannedEndDate;
	}

	public String getdPlannedStartDate() {
		return dPlannedStartDate;
	}

	public void setdPlannedStartDate(String dPlannedStartDate) {
		this.dPlannedStartDate = dPlannedStartDate;
	}

	public String getdPlannedEndDate() {
		return dPlannedEndDate;
	}

	public void setdPlannedEndDate(String dPlannedEndDate) {
		this.dPlannedEndDate = dPlannedEndDate;
	}

	public String getcPlannedStartDate() {
		return cPlannedStartDate;
	}

	public void setcPlannedStartDate(String cPlannedStartDate) {
		this.cPlannedStartDate = cPlannedStartDate;
	}

	public String getcPlannedEndDate() {
		return cPlannedEndDate;
	}

	public void setcPlannedEndDate(String cPlannedEndDate) {
		this.cPlannedEndDate = cPlannedEndDate;
	}

	public String getaPlannedStartDate() {
		return aPlannedStartDate;
	}

	public void setaPlannedStartDate(String aPlannedStartDate) {
		this.aPlannedStartDate = aPlannedStartDate;
	}

	public String getaPlannedEndDate() {
		return aPlannedEndDate;
	}

	public void setaPlannedEndDate(String aPlannedEndDate) {
		this.aPlannedEndDate = aPlannedEndDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getpActualStartDate() {
		return pActualStartDate;
	}

	public void setpActualStartDate(String pActualStartDate) {
		this.pActualStartDate = pActualStartDate;
	}

	public String getpActualEndDate() {
		return pActualEndDate;
	}

	public void setpActualEndDate(String pactualEndDate) {
		this.pActualEndDate = pactualEndDate;
	}

	public String getdActualStartDate() {
		return dActualStartDate;
	}

	public void setdActualStartDate(String dActualStartDate) {
		this.dActualStartDate = dActualStartDate;
	}

	public String getdActualEndDate() {
		return dActualEndDate;
	}

	public void setdActualEndDate(String dActualEndDate) {
		this.dActualEndDate = dActualEndDate;
	}

	public String getcActualStartDate() {
		return cActualStartDate;
	}

	public void setcActualStartDate(String cActualStartDate) {
		this.cActualStartDate = cActualStartDate;
	}

	public String getcActualEndDate() {
		return cActualEndDate;
	}

	public void setcActualEndDate(String cActualEndDate) {
		this.cActualEndDate = cActualEndDate;
	}

	public String getaActualStartDate() {
		return aActualStartDate;
	}

	public void setaActualStartDate(String aActualStartDate) {
		this.aActualStartDate = aActualStartDate;
	}

	public String getaActualEndDate() {
		return aActualEndDate;
	}

	public void setaActualEndDate(String aActualEndDate) {
		this.aActualEndDate = aActualEndDate;
	}

	public String getSavingsType() {
		return savingsType;
	}

	public void setSavingsType(String savingsType) {
		this.savingsType = savingsType;
	}
	
	

	public String getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	@Override
	public String toString() {
		return "ProjectBenefitIndentsEmatrix [id=" + id + ", project=" + project + ", bc=" + bc + ", savingsType="
				+ savingsType + ", confidenceLevel=" + confidenceLevel + ", totalProjectCost=" + totalProjectCost
				+ ", totalPotentialBenefit=" + totalPotentialBenefit + ", startDate=" + startDate + ", targetDate="
				+ targetDate + ", pPlannedStartDate=" + pPlannedStartDate + ", pPlannedEndDate=" + pPlannedEndDate
				+ ", dPlannedStartDate=" + dPlannedStartDate + ", dPlannedEndDate=" + dPlannedEndDate
				+ ", cPlannedStartDate=" + cPlannedStartDate + ", cPlannedEndDate=" + cPlannedEndDate
				+ ", aPlannedStartDate=" + aPlannedStartDate + ", aPlannedEndDate=" + aPlannedEndDate
				+ ", pActualStartDate=" + pActualStartDate + ", pActualEndDate=" + pActualEndDate
				+ ", dActualStartDate=" + dActualStartDate + ", dActualEndDate=" + dActualEndDate
				+ ", cActualStartDate=" + cActualStartDate + ", cActualEndDate=" + cActualEndDate
				+ ", aActualStartDate=" + aActualStartDate + ", aActualEndDate=" + aActualEndDate + ", createdAt="
				+ createdAt + ", updatedAt=" + updatedAt + "]";
	}

	
}
