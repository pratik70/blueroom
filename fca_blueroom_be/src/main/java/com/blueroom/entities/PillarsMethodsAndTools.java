package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_pillar_methods_tools")
public class PillarsMethodsAndTools {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "pillar_approach")
    private String pillarApproach;

    @Column(name = "value")
    private String value;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pillar_id")
    private Pillar pillar;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "method_tool_id")
    private MethodsAndTools methodsAndTools;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPillarApproach() {
        return pillarApproach;
    }

    public void setPillarApproach(String pillarApproach) {
        this.pillarApproach = pillarApproach;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Pillar getPillar() {
        return pillar;
    }

    public void setPillar(Pillar pillar) {
        this.pillar = pillar;
    }

    public MethodsAndTools getMethodsAndTools() {
        return methodsAndTools;
    }

    public void setMethodsAndTools(MethodsAndTools methodsAndTools) {
        this.methodsAndTools = methodsAndTools;
    }

}
