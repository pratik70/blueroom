package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name="tbl_tool_skillset_user_set")
public class UserSkillSet {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_pillar_id")
	private UserPillar userPillar;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "method_tool_id")
	private MethodsAndTools methodsTools;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private AuthUser authUser;

	
	@Column(name="actualSkill")
	private Integer actualSkill;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public MethodsAndTools getMethodsTools() {
		return methodsTools;
	}

	public void setMethodsTools(MethodsAndTools methodsTools) {
		this.methodsTools = methodsTools;
	}

	public Integer getActualSkill() {
		return actualSkill;
	}

	public void setActualSkill(Integer actualSkill) {
		this.actualSkill = actualSkill;
	}

	public UserPillar getUserPillar() {
		return userPillar;
	}

	public void setUserPillar(UserPillar userPillar) {
		this.userPillar = userPillar;
	}

	public AuthUser getAuthUser() {
		return authUser;
	}

	public void setAuthUser(AuthUser authUser) {
		this.authUser = authUser;
	}
	
	
	
}
