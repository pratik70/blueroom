package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name="tbl_technicaltool_skillset_user")
public class UserTechnicalToolSkillSet {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;

	@Column(name="actual_skill")
	private Long actualSkill;
	
	@Column(name="required_skill")
	private Long requiredSkill;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "technicaltool_id")
	private TechnicalTool technicalTool;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private AuthUser user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActualSkill() {
		return actualSkill;
	}

	public void setActualSkill(Long actualSkill) {
		this.actualSkill = actualSkill;
	}

	public TechnicalTool getTechnicalTool() {
		return technicalTool;
	}

	public void setTechnicalTool(TechnicalTool technicalTool) {
		this.technicalTool = technicalTool;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}

	public Long getRequiredSkill() {
		return requiredSkill;
	}

	public void setRequiredSkill(Long requiredSkill) {
		this.requiredSkill = requiredSkill;
	}

}
