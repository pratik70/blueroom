package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_machine_loss")
public class MachineLoss {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "source_loss")
    private String sourceLoss;

    @Column(name = "source_op")
    private String sourceOp;

    @Column(name = "impacted_loss")
    private String impactedLoss;

    @Column(name = "impacted_op")
    private String impactedOp;

    @Column(name = "value")
    private boolean value;

    @Column(name = "value_red")
    private boolean valueRed;

    @Column(name = "value_green")
    private boolean valueGreen;

    @Column(name = "value_yellow")
    private boolean valueYellow;
    
    @Column(name = "value_white")
    private boolean valueWhite;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSourceLoss() {
        return sourceLoss;
    }

    public void setSourceLoss(String sourceLoss) {
        this.sourceLoss = sourceLoss;
    }

    public String getSourceOp() {
        return sourceOp;
    }

    public void setSourceOp(String sourceOp) {
        this.sourceOp = sourceOp;
    }

    public String getImpactedLoss() {
        return impactedLoss;
    }

    public void setImpactedLoss(String impactedLoss) {
        this.impactedLoss = impactedLoss;
    }

    public String getImpactedOp() {
        return impactedOp;
    }

    public void setImpactedOp(String impactedOp) {
        this.impactedOp = impactedOp;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public boolean isValueRed() {
        return valueRed;
    }

    public void setValueRed(boolean valueRed) {
        this.valueRed = valueRed;
    }

    public boolean isValueGreen() {
        return valueGreen;
    }

    public void setValueGreen(boolean valueGreen) {
        this.valueGreen = valueGreen;
    }

    public boolean isValueYellow() {
        return valueYellow;
    }

    public void setValueYellow(boolean valueYellow) {
        this.valueYellow = valueYellow;
    }

	public boolean isValueWhite() {
		return valueWhite;
	}

	public void setValueWhite(boolean valueWhite) {
		this.valueWhite = valueWhite;
	}
    
    

}
