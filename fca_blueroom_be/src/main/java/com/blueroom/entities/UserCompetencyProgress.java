package com.blueroom.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name="tbl_user_copmetency_progress")
public class UserCompetencyProgress {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;

	@Column(name="previous_level")
	private Long previousLevel;
	
	@Column(name="updated_level")
	private Long updatedLevel;
	

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private AuthUser user;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "modefied_by_user_id")
	private AuthUser modefiedBy;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "competency_id")
	private Competencies competencyId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPreviousLevel() {
		return previousLevel;
	}

	public void setPreviousLevel(Long previousLevel) {
		this.previousLevel = previousLevel;
	}

	public Long getUpdatedLevel() {
		return updatedLevel;
	}

	public void setUpdatedLevel(Long updatedLevel) {
		this.updatedLevel = updatedLevel;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}

	public AuthUser getModefiedBy() {
		return modefiedBy;
	}

	public void setModefiedBy(AuthUser modefiedBy) {
		this.modefiedBy = modefiedBy;
	}

	public Competencies getCompetencyId() {
		return competencyId;
	}

	public void setCompetencyId(Competencies competencyId) {
		this.competencyId = competencyId;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	@Column(name="modified_at")
	private Date modifiedAt;


}
