package com.blueroom.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_ma_machine_operation")
public class MachineOperation {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "machine_name")
	private String machineName;
	
    @Column(name = "machine_code")
    private String machineCode;
    
    
	
	
	 @OneToOne(fetch = FetchType.EAGER)
	    @JoinColumn(name = "etu_id")
	    private ETU etu;
    /*@Column(name = "etu_id")
    private Long etuid;*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

/*	public ETU getEtu() {
		return etu;
	}

	public void setEtu(ETU etu) {
		this.etu = etu;
	}
*/
	
	
	public String getMachineCode() {
		return machineCode;
	}

/*	public Long getEtuid() {
		return etuid;
	}

	public void setEtuid(Long etuid) {
		this.etuid = etuid;
	}*/

	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}


	public ETU getEtu() {
		return etu;
	}

	public void setEtu(ETU etu) {
		this.etu = etu;
	}
	
	
}
