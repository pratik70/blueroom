package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_ma_methods_tools")
public class MethodsAndTools {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "level1_tool")
    private String levelFirstTool;

    @Column(name = "level2_tool")
    private String levelSecondTool;

    @Column(name = "type")
    private String type;

    @Column(name = "reprort_priority")
    private Integer reprortPriority;
    
    @Column(name = "pillar_approach")
    private String pillar_approach;

    public Integer getReprortPriority() {
		return reprortPriority;
	}

	public void setReprortPriority(Integer reprortPriority) {
		this.reprortPriority = reprortPriority;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLevelFirstTool() {
        return levelFirstTool;
    }

    public void setLevelFirstTool(String levelFirstTool) {
        this.levelFirstTool = levelFirstTool;
    }

    public String getLevelSecondTool() {
        return levelSecondTool;
    }

    public void setLevelSecondTool(String levelSecondTool) {
        this.levelSecondTool = levelSecondTool;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

	public String getPillar_approach() {
		return pillar_approach;
	}

	public void setPillar_approach(String pillar_approach) {
		this.pillar_approach = pillar_approach;
	}

    
}
