package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name="tbl_factorization_saving_project")
public class FactorizationSavingProject {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;
	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private AuthUser user;
	
	@Column(name = "project_factor")
	private Integer projectFactor;
	
	@Column(name = "saving_factor")
	private Double savingFactor;
	
	@Column(name = "factor_year")
	private Integer factorYear;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}

	public Integer getProjectFactor() {
		return projectFactor;
	}

	public void setProjectFactor(Integer projectFactor) {
		this.projectFactor = projectFactor;
	}

	public Double getSavingFactor() {
		return savingFactor;
	}

	public void setSavingFactor(Double savingFactor) {
		this.savingFactor = savingFactor;
	}

	public Integer getFactorYear() {
		return factorYear;
	}

	public void setFactorYear(Integer factorYear) {
		this.factorYear = factorYear;
	}
	
	

}
