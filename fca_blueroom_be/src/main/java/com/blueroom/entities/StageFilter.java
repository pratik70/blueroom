package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_ma_project_stage")
public class StageFilter {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="stage_id", nullable=false)
	private Long stageId;
	
	@Column(name="project_Id")
	private Long projectId;
	
	@Column(name="user_Id")
	private Long userId;
	
	@Column(name="role_Id")
	private Long roleId;
	
	@Column(name="pWorkingStage")
	private Long pWorkingStage;
	
	@Column(name="dWorkingStage")
	private Long dWorkingStage;
	
	@Column(name="cWorkingStage")
	private Long cWorkingStage;
	
	@Column(name="aWorkingStage")
	private Long aWorkingStage;

	public Long getStageId() {
		return stageId;
	}

	public void setStageId(Long stageId) {
		this.stageId = stageId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getpWorkingStage() {
		return pWorkingStage;
	}

	public void setpWorkingStage(Long pWorkingStage) {
		this.pWorkingStage = pWorkingStage;
	}

	public Long getdWorkingStage() {
		return dWorkingStage;
	}

	public void setdWorkingStage(Long dWorkingStage) {
		this.dWorkingStage = dWorkingStage;
	}

	public Long getcWorkingStage() {
		return cWorkingStage;
	}

	public void setcWorkingStage(Long cWorkingStage) {
		this.cWorkingStage = cWorkingStage;
	}

	public Long getaWorkingStage() {
		return aWorkingStage;
	}

	public void setaWorkingStage(Long aWorkingStage) {
		this.aWorkingStage = aWorkingStage;
	}
	
	
	
}
