package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;


@Entity
@Table(name = "tbl_user_pillar")
public class UserPillar {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "role")
	private String role;

	@Column(name = "function")
	private String functionName;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private AuthUser user;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pillar_id")
	private Pillar pillar;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}

	public Pillar getPillar() {
		return pillar;
	}

	public void setPillar(Pillar pillar) {
		this.pillar = pillar;
	}
	
	
	
}
