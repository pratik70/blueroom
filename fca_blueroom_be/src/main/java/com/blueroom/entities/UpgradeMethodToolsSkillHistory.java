package com.blueroom.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name="tbl_ma_upgrade_method_tools_skill_history")
public class UpgradeMethodToolsSkillHistory {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;

	@Column(name="old_skill")
	private Long oldSkill;
	
	@Column(name="new_skill")
	private Long newSkill;
	
	
	@Column(name="comment")
	private String comment;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
	private AuthUser user;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "updated_by_user_id")
	private AuthUser updatedBy;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "methods_tools_id")
	private MethodsAndTools methodsTools;
	
	@Column(name="created_at")
	private Date createdAt;
	
	@Column(name="updated_at")
	private Date updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}

	public AuthUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AuthUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public MethodsAndTools getMethodsTools() {
		return methodsTools;
	}

	public void setMethodsTools(MethodsAndTools methodsTools) {
		this.methodsTools = methodsTools;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getOldSkill() {
		return oldSkill;
	}

	public void setOldSkill(Long long1) {
		this.oldSkill = long1;
	}

	public Long getNewSkill() {
		return newSkill;
	}

	public void setNewSkill(Long long1) {
		this.newSkill = long1;
	}
	
	

}
