package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_project_fmatrix_saving")
public class ProjectFMatrixSavings {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;


	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	private Project project;
	
	@Column(name="projected_saving_value")
	private Integer projectedSavingValue;
	
	
	@Column(name="actual_saving_value")
	private Integer actualSavingValue;
	
	@Column(name="month")
	private Integer month;
	
	@Column(name="year")
	private Integer year;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getProjectedSavingValue() {
		return projectedSavingValue;
	}

	public void setProjectedSavingValue(Integer projectedSavingValue) {
		this.projectedSavingValue = projectedSavingValue;
	}

	public Integer getActualSavingValue() {
		return actualSavingValue;
	}

	public void setActualSavingValue(Integer actualSavingValue) {
		this.actualSavingValue = actualSavingValue;
	}
	
	
	

}
