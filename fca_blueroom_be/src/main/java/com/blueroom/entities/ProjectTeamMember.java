package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.blueroom.entities.auth.AuthUser;

@Entity
@Table(name="tbl_project_team_members")
public class ProjectTeamMember {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	private Project project;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
	private AuthUser user;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
	private TeamRole role;
	
	@Column(name="is_filtered_user")
	private boolean filteredUser = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}

	public TeamRole getRole() {
		return role;
	}

	public void setRole(TeamRole role) {
		this.role = role;
	}

	public boolean isFilteredUser() {
		return filteredUser;
	}

	public void setFilteredUser(boolean filteredUser) {
		this.filteredUser = filteredUser;
	}
	
}
