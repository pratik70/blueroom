package com.blueroom.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_layout_image")
public class LayoutImage   {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;

	@Column(name="name")
	private String name;

	@Column(name="lauout_type")
	private String layoutType;

	@Column(name="file_path")
	private String filePath;

	@OneToOne
	@JoinColumn(name="cd_year_id")
	private CDYear cDYear;

	@OneToOne
	@JoinColumn(name="etu_id")
	private ETU etu;

	@Column(name="created_at")
	private Date createdAt;

	@Column(name="updated_at")
	private Date updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLayoutType() {
		return layoutType;
	}

	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public CDYear getcDYear() {
		return cDYear;
	}

	public void setcDYear(CDYear cDYear) {
		this.cDYear = cDYear;
	}

	public ETU getEtu() {
		return etu;
	}

	public void setEtu(ETU etu) {
		this.etu = etu;
	}




}
