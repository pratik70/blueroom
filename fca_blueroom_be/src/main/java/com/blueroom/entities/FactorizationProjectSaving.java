package com.blueroom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_factorization_project_monthly_saving")
public class FactorizationProjectSaving {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;

	@Column(name="month")
	private Integer month;
	
	@Column(name="month_factor")
	private Double monthFactor;
	
	@Column(name="factor_year")
	private Integer factorYear;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Double getMonthFactor() {
		return monthFactor;
	}

	public void setMonthFactor(Double monthFactor) {
		this.monthFactor = monthFactor;
	}

	public Integer getFactorYear() {
		return factorYear;
	}

	public void setFactorYear(Integer factorYear) {
		this.factorYear = factorYear;
	}
	
	
}
