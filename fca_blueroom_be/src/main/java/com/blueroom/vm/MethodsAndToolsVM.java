package com.blueroom.vm;

public class MethodsAndToolsVM {

	private Long id;
	private String levelFirstTool;
	private String levelSecondTool;
	private Integer requiredSkill;
	private String type;
	private String pillarName;
	private String pillarApproach;
	private String code;
	private Long updatedSkill;
	private String comment;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLevelFirstTool() {
		return levelFirstTool;
	}

	public void setLevelFirstTool(String levelFirstTool) {
		this.levelFirstTool = levelFirstTool;
	}

	public String getLevelSecondTool() {
		return levelSecondTool;
	}

	public void setLevelSecondTool(String levelSecondTool) {
		this.levelSecondTool = levelSecondTool;
	}

	public Integer getRequiredSkill() {
		return requiredSkill;
	}

	public void setRequiredSkill(Integer requiredSkill) {
		this.requiredSkill = requiredSkill;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPillarName() {
		return pillarName;
	}

	public void setPillarName(String pillarName) {
		this.pillarName = pillarName;
	}

	public String getPillarApproach() {
		return pillarApproach;
	}

	public void setPillarApproach(String pillarApproach) {
		this.pillarApproach = pillarApproach;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public MethodsAndToolsVM() {
	}

	public MethodsAndToolsVM(Long id, String levelFirstTool, String levelSecondTool, String type, String pillarApproach, String code, String pillarName) {
		this.id = id;
		this.levelFirstTool = levelFirstTool;
		this.levelSecondTool = levelSecondTool;
		this.type = type;
		this.pillarApproach = pillarApproach;
		this.code = code;
		this.pillarName = pillarName;
	}

	public MethodsAndToolsVM(long id, String levelFirstTool, String levelSecondTool,
			String type, String pillarApproach) {
		this.id = id;
		this.levelFirstTool = levelFirstTool;
		this.levelSecondTool = levelSecondTool;
		this.type = type;
		this.pillarApproach = pillarApproach;
		
	}

	public Long getUpdatedSkill() {
		return updatedSkill;
	}

	public void setUpdatedSkill(Long updatedSkill) {
		this.updatedSkill = updatedSkill;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
