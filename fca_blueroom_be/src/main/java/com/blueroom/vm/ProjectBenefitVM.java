package com.blueroom.vm;

import java.util.Date;
import java.util.List;

public class ProjectBenefitVM {
    private Long id;
	private Double bc;
	private Integer confidenceLevel; 
    private Integer totalProjectCost;
    private Integer	totalPotentialBenefit;
    
    private String savingsType;
    
    private Date createdAt;
    private Date updatedAt;
    private List<BenifitVM> benifitList;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getBc() {
		return bc;
	}
	public void setBc(Double bc) {
		this.bc = bc;
	}
	public Integer getConfidenceLevel() {
		return confidenceLevel;
	}
	public void setConfidenceLevel(Integer confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}
	public Integer getTotalProjectCost() {
		return totalProjectCost;
	}
	public void setTotalProjectCost(Integer totalProjectCost) {
		this.totalProjectCost = totalProjectCost;
	}
	public Integer getTotalPotentialBenefit() {
		return totalPotentialBenefit;
	}
	public void setTotalPotentialBenefit(Integer totalPotentialBenefit) {
		this.totalPotentialBenefit = totalPotentialBenefit;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public List<BenifitVM> getBenifitList() {
		return benifitList;
	}
	public void setBenifitList(List<BenifitVM> benifitList) {
		this.benifitList = benifitList;
	}
	@Override
	public String toString() {
		return "ProjectBenefitVM [id=" + id + ", bc=" + bc + ", confidenceLevel=" + confidenceLevel
				+ ", totalProjectCost=" + totalProjectCost + ", totalPotentialBenefit=" + totalPotentialBenefit
				+ ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", benifitList=" + benifitList + "]";
	}
	public String getSavingsType() {
		return savingsType;
	}
	public void setSavingsType(String savingsType) {
		this.savingsType = savingsType;
	}
    
    
	
}
