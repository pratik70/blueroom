package com.blueroom.vm;

public class UserProjectSavingVM {

	private Long userId;
	
	private Long projectId;
	private Integer projectSavings;
	private Integer teamMemberSize;
	private Integer projectSavingInPercentage;
	private String persentage;
	private UserVM user;
	private Integer completeNoProject;
	
	@Override
	public String toString() {
		return "UserProjectSavingVM [userId=" + userId + ", projectId=" + projectId + ", projectSavings="
				+ projectSavings + ", teamMemberSize=" + teamMemberSize + ", projectSavingInPercentage="
				+ projectSavingInPercentage + ", persentage=" + persentage + ", user=" + user + ", completeNoProject="
				+ completeNoProject + "]";
	}
	public Integer getCompleteNoProject() {
		return completeNoProject;
	}
	public void setCompleteNoProject(Integer completeNoProject) {
		this.completeNoProject = completeNoProject;
	}
	public UserVM getUser() {
		return user;
	}
	public void setUser(UserVM user) {
		this.user = user;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Integer getProjectSavings() {
		return projectSavings;
	}
	public void setProjectSavings(Integer projectSavings) {
		this.projectSavings = projectSavings;
	}
	public Integer getTeamMemberSize() {
		return teamMemberSize;
	}
	public void setTeamMemberSize(Integer teamMemberSize) {
		this.teamMemberSize = teamMemberSize;
	}
	public Integer getProjectSavingInPercentage() {
		return projectSavingInPercentage;
	}
	public void setProjectSavingInPercentage(Integer projectSavingInPercentage) {
		this.projectSavingInPercentage = projectSavingInPercentage;
	}
	public String getPersentage() {
		return persentage;
	}
	public void setPersentage(String persentage) {
		this.persentage = persentage;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
