package com.blueroom.vm;

public class ProjectFMatrixSavingsVM {

	private Long id;
	private Long project_id;
	private Integer actualProjectedValue;
	private Integer actualSavingValue;
	private Integer month;
	private Integer year;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProject_id() {
		return project_id;
	}
	public void setProject_id(Long project_id) {
		this.project_id = project_id;
	}
	
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getActualProjectedValue() {
		return actualProjectedValue;
	}
	public void setActualProjectedValue(Integer actualProjectedValue) {
		this.actualProjectedValue = actualProjectedValue;
	}
	public Integer getActualSavingValue() {
		return actualSavingValue;
	}
	public void setActualSavingValue(Integer actualSavingValue) {
		this.actualSavingValue = actualSavingValue;
	}
	
	
	
	
}
