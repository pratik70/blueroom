package com.blueroom.vm;

import java.util.List;

public class UpgradeSkillVM {
	 private Long userId;
     private List<MethodsAndToolsVM> methodAndTools;
     private List<TechnicalToolVM> technicalTools;
     
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public List<MethodsAndToolsVM> getMethodAndTools() {
		return methodAndTools;
	}
	public void setMethodAndTools(List<MethodsAndToolsVM> methodAndTools) {
		this.methodAndTools = methodAndTools;
	}
	public List<TechnicalToolVM> getTechnicalTools() {
		return technicalTools;
	}
	public void setTechnicalTools(List<TechnicalToolVM> technicalTools) {
		this.technicalTools = technicalTools;
	}
     
     
}
