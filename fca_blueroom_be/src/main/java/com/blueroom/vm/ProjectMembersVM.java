package com.blueroom.vm;

import java.util.List;

public class ProjectMembersVM {

	private Long teamLeader;
	private boolean teamLeaderFiltered;
	
	private Long teamCoach;
	private boolean teamCoachFiltered;
	
	private List<MemberVM> teamMembers;

	private Long firstTeamMember;
	private Long secondTeamMember;
	private Long thirdTeamMember;
	
	
	public Long getTeamLeader() {
		return teamLeader;
	}

	public void setTeamLeader(Long teamLeader) {
		this.teamLeader = teamLeader;
	}

	public Long getTeamCoach() {
		return teamCoach;
	}

	public void setTeamCoach(Long teamCoach) {
		this.teamCoach = teamCoach;
	}

	


	
	public List<MemberVM> getTeamMembers() {
		return teamMembers;
	}

	public void setTeamMembers(List<MemberVM> teamMembers) {
		this.teamMembers = teamMembers;
	}
	
	
	
	
	
	

	public Long getFirstTeamMember() {
		return firstTeamMember;
	}

	public void setFirstTeamMember(Long firstTeamMember) {
		this.firstTeamMember = firstTeamMember;
	}

	public Long getSecondTeamMember() {
		return secondTeamMember;
	}

	public void setSecondTeamMember(Long secondTeamMember) {
		this.secondTeamMember = secondTeamMember;
	}

	public Long getThirdTeamMember() {
		return thirdTeamMember;
	}

	public void setThirdTeamMember(Long thirdTeamMember) {
		this.thirdTeamMember = thirdTeamMember;
	}

	@Override
	public String toString() {
		return "ProjectMembersVM [teamLeader=" + teamLeader + ", teamLeaderFiltered=" + teamLeaderFiltered
				+ ", teamCoach=" + teamCoach + ", teamCoachFiltered=" + teamCoachFiltered + ", memberList=" + teamMembers
				+ "]";
	}

	public boolean isTeamLeaderFiltered() {
		return teamLeaderFiltered;
	}

	public void setTeamLeaderFiltered(boolean teamLeaderFiltered) {
		this.teamLeaderFiltered = teamLeaderFiltered;
	}

	public boolean isTeamCoachFiltered() {
		return teamCoachFiltered;
	}

	public void setTeamCoachFiltered(boolean teamCoachFiltered) {
		this.teamCoachFiltered = teamCoachFiltered;
	}
	
	
}
