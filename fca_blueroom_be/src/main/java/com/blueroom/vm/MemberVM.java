package com.blueroom.vm;

public class MemberVM {
	private Long userId;
	private String roleName;
	private boolean filteredUser;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public boolean isFilteredUser() {
		return filteredUser;
	}
	public void setFilteredUser(boolean filteredUser) {
		this.filteredUser = filteredUser;
	}
	

}
