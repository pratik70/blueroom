package com.blueroom.vm;

import java.util.List;

/**
 * @author User
 *
 */
public class ProjectSupportingPillarVM {

	private Long projectId;
	private Long mainPillar;
	private List<Long> supportingPillars;
	private List<Long> kpis;
	
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Long getMainPillar() {
		return mainPillar;
	}
	public void setMainPillar(Long mainPillar) {
		this.mainPillar = mainPillar;
	}
	
	public List<Long> getSupportingPillars() {
		return supportingPillars;
	}
	public void setSupportingPillars(List<Long> supportingPillars) {
		this.supportingPillars = supportingPillars;
	}
	public List<Long> getKpis() {
		return kpis;
	}
	public void setKpis(List<Long> kpis) {
		this.kpis = kpis;
	}
	
}
