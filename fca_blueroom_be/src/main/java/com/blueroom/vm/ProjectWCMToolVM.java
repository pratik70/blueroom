package com.blueroom.vm;

import java.util.List;

public class ProjectWCMToolVM {
	private List<MethodsAndToolsVM> methodsAndTools;
	private List<TechnicalToolVM> technicalTools;
	public List<MethodsAndToolsVM> getMethodsAndTools() {
		return methodsAndTools;
	}
	public void setMethodsAndTools(List<MethodsAndToolsVM> methodsAndTools) {
		this.methodsAndTools = methodsAndTools;
	}
	public List<TechnicalToolVM> getTechnicalTools() {
		return technicalTools;
	}
	public void setTechnicalTools(List<TechnicalToolVM> technicalTools) {
		this.technicalTools = technicalTools;
	}
	
}
