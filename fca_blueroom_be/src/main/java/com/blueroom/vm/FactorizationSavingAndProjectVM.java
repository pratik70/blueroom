package com.blueroom.vm;

public class FactorizationSavingAndProjectVM {
	private Long userId;
	private Integer projectFactor;
	private Double savingFactor;
	private Integer factorYear;
	private Long employeeId;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getProjectFactor() {
		return projectFactor;
	}
	public void setProjectFactor(Integer projectFactor) {
		this.projectFactor = projectFactor;
	}
	
	public Double getSavingFactor() {
		return savingFactor;
	}
	public void setSavingFactor(Double savingFactor) {
		this.savingFactor = savingFactor;
	}
	public Integer getFactorYear() {
		return factorYear;
	}
	public void setFactorYear(Integer factorYear) {
		this.factorYear = factorYear;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	
	
}
