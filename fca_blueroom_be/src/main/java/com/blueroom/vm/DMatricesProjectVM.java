package com.blueroom.vm;

import java.util.HashMap;

public class DMatricesProjectVM {
	private Long projectId;
	private String projectName;
	private String projectUIN;
	private String loss ;
	private String etu ;
	private String process ;
	private String operationNo ;
	private Double yearlyPotential;
	private String personResponsible;
	HashMap<String, String> pillarsMap;
	
	
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectUIN() {
		return projectUIN;
	}
	public void setProjectUIN(String projectUIN) {
		this.projectUIN = projectUIN;
	}
	public String getLoss() {
		return loss;
	}
	public void setLoss(String loss) {
		this.loss = loss;
	}
	public String getEtu() {
		return etu;
	}
	public void setEtu(String etu) {
		this.etu = etu;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getOperationNo() {
		return operationNo;
	}
	public void setOperationNo(String operationNo) {
		this.operationNo = operationNo;
	}
	public Double getYearlyPotential() {
		return yearlyPotential;
	}
	public void setYearlyPotential(Double yearlyPotential) {
		this.yearlyPotential = yearlyPotential;
	}
	public String getPersonResponsible() {
		return personResponsible;
	}
	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}
	public HashMap<String, String> getPillarsMap() {
		return pillarsMap;
	}
	public void setPillarsMap(HashMap<String, String> pillarsMap) {
		this.pillarsMap = pillarsMap;
	}
	
	
}
