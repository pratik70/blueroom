package com.blueroom.vm;

import java.util.List;

public class SkillDataVM {
	private Long userId;
	private List<UserSkillSetVM> methodAndToolsList;
	private List<UserSkillTechnicalToolVM> technicalToolsList;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public List<UserSkillSetVM> getMethodAndToolsList() {
		return methodAndToolsList;
	}
	public void setMethodAndToolsList(List<UserSkillSetVM> methodAndToolsList) {
		this.methodAndToolsList = methodAndToolsList;
	}
	public List<UserSkillTechnicalToolVM> getTechnicalToolsList() {
		return technicalToolsList;
	}
	public void setTechnicalToolsList(List<UserSkillTechnicalToolVM> technicalToolsList) {
		this.technicalToolsList = technicalToolsList;
	}
	
	
}
