package com.blueroom.vm;

import java.util.HashMap;
import java.util.List;

public class DMatricesVM {
	
	private Integer pageNumber;
	private Integer size;
	private Integer totalCount;
	private List<DMatricesProjectVM> dMatricesProjectVMs;
	private List<HashMap<String, String>> projectList;
	private HashMap<String, String> methodColumns;
	private HashMap<String, String> toolColumns;
	private HashMap<String, String> pillarColumns;
	private HashMap<String, String> kipColumns;
	private Integer totalTeamMemberCount;
	public List<DMatricesProjectVM> getdMatricesProjectVMs() {
		return dMatricesProjectVMs;
	}

	public void setdMatricesProjectVMs(List<DMatricesProjectVM> dMatricesProjectVMs) {
		this.dMatricesProjectVMs = dMatricesProjectVMs;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}


	public List<HashMap<String, String>> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<HashMap<String, String>> projectList) {
		this.projectList = projectList;
	}

	public HashMap<String, String> getMethodColumns() {
		return methodColumns;
	}

	public void setMethodColumns(HashMap<String, String> methodColumns) {
		this.methodColumns = methodColumns;
	}

	public HashMap<String, String> getToolColumns() {
		return toolColumns;
	}

	public void setToolColumns(HashMap<String, String> toolColumns) {
		this.toolColumns = toolColumns;
	}

	public HashMap<String, String> getPillarColumns() {
		return pillarColumns;
	}

	public void setPillarColumns(HashMap<String, String> pillarColumns) {
		this.pillarColumns = pillarColumns;
	}

	public HashMap<String, String> getKipColumns() {
		return kipColumns;
	}

	public void setKipColumns(HashMap<String, String> kipColumns) {
		this.kipColumns = kipColumns;
	}

	public Integer getTotalTeamMemberCount() {
		return totalTeamMemberCount;
	}

	public void setTotalTeamMemberCount(Integer totalTeamMemberCount) {
		this.totalTeamMemberCount = totalTeamMemberCount;
	}


	
}
