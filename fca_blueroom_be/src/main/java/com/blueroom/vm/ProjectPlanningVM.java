package com.blueroom.vm;

import javax.persistence.Column;

public class ProjectPlanningVM {
    
	private Long projectId;
	private String startDate;
	private String targetDate;
	
	//Plan Dates
	private String pPlannedStartDate;
	private String pPlannedEndDate;
	private String dPlannedStartDate;
	private String dPlannedEndDate;
	private String cPlannedStartDate;
	private String cPlannedEndDate;
	private String aPlannedStartDate;
	private String aPlannedEndDate;
	
	//actual Dates
	private String pActualStartDate;
	private String pActualEndDate;
	private String dActualStartDate;
	private String dActualEndDate;
	private String cActualStartDate;
	private String cActualEndDate;
	private String aActualStartDate;
	private String aActualEndDate;
	private String completionDate;
	private Integer	totalPotentialBenefit;
	
	private boolean saveCompletionDate;
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getTargetDate() {
		return targetDate;
	}
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
	public String getpPlannedStartDate() {
		return pPlannedStartDate;
	}
	public void setpPlannedStartDate(String pPlannedStartDate) {
		this.pPlannedStartDate = pPlannedStartDate;
	}
	public String getpPlannedEndDate() {
		return pPlannedEndDate;
	}
	public void setpPlannedEndDate(String pPlannedEndDate) {
		this.pPlannedEndDate = pPlannedEndDate;
	}
	public String getdPlannedStartDate() {
		return dPlannedStartDate;
	}
	public void setdPlannedStartDate(String dPlannedStartDate) {
		this.dPlannedStartDate = dPlannedStartDate;
	}
	public String getdPlannedEndDate() {
		return dPlannedEndDate;
	}
	public void setdPlannedEndDate(String dPlannedEndDate) {
		this.dPlannedEndDate = dPlannedEndDate;
	}
	public String getcPlannedStartDate() {
		return cPlannedStartDate;
	}
	public void setcPlannedStartDate(String cPlannedStartDate) {
		this.cPlannedStartDate = cPlannedStartDate;
	}
	public String getcPlannedEndDate() {
		return cPlannedEndDate;
	}
	public void setcPlannedEndDate(String cPlannedEndDate) {
		this.cPlannedEndDate = cPlannedEndDate;
	}
	public String getaPlannedStartDate() {
		return aPlannedStartDate;
	}
	public void setaPlannedStartDate(String aPlannedStartDate) {
		this.aPlannedStartDate = aPlannedStartDate;
	}
	public String getaPlannedEndDate() {
		return aPlannedEndDate;
	}
	public void setaPlannedEndDate(String aPlannedEndDate) {
		this.aPlannedEndDate = aPlannedEndDate;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public String getpActualStartDate() {
		return pActualStartDate;
	}
	public void setpActualStartDate(String pActualStartDate) {
		this.pActualStartDate = pActualStartDate;
	}
	public String getpActualEndDate() {
		return pActualEndDate;
	}
	public void setpActualEndDate(String pActualEndDate) {
		this.pActualEndDate = pActualEndDate;
	}
	public String getdActualStartDate() {
		return dActualStartDate;
	}
	public void setdActualStartDate(String dActualStartDate) {
		this.dActualStartDate = dActualStartDate;
	}
	public String getdActualEndDate() {
		return dActualEndDate;
	}
	public void setdActualEndDate(String dActualEndDate) {
		this.dActualEndDate = dActualEndDate;
	}
	public String getcActualStartDate() {
		return cActualStartDate;
	}
	public void setcActualStartDate(String cActualStartDate) {
		this.cActualStartDate = cActualStartDate;
	}
	public String getcActualEndDate() {
		return cActualEndDate;
	}
	public void setcActualEndDate(String cActualEndDate) {
		this.cActualEndDate = cActualEndDate;
	}
	public String getaActualStartDate() {
		return aActualStartDate;
	}
	public void setaActualStartDate(String aActualStartDate) {
		this.aActualStartDate = aActualStartDate;
	}
	public String getaActualEndDate() {
		return aActualEndDate;
	}
	public void setaActualEndDate(String aActualEndDate) {
		this.aActualEndDate = aActualEndDate;
	}
	public Integer getTotalPotentialBenefit() {
		return totalPotentialBenefit;
	}
	public void setTotalPotentialBenefit(Integer totalPotentialBenefit) {
		this.totalPotentialBenefit = totalPotentialBenefit;
	}
	public boolean isSaveCompletionDate() {
		return saveCompletionDate;
	}
	public void setSaveCompletionDate(boolean saveCompletionDate) {
		this.saveCompletionDate = saveCompletionDate;
	}
	public String getCompletionDate() {
		return completionDate;
	}
	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}
	
	
	
}
