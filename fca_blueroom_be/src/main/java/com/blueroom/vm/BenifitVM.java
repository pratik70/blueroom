package com.blueroom.vm;

public class BenifitVM {
	
	private Long id;
	private String benifitName;
	private Integer benifitValue = 0;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBenifitName() {
		return benifitName;
	}
	public void setBenifitName(String benifitName) {
		this.benifitName = benifitName;
	}
	public Integer getBenifitValue() {
		return benifitValue;
	}
	public void setBenifitValue(Integer benifitValue) {
		this.benifitValue = benifitValue;
	}
	
	@Override
	public String toString() {
		return "BenifitVM [id=" + id + ", benifitName=" + benifitName + ", benifitValue=" + benifitValue + "]";
	}
	
	 
	

}
