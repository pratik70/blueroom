package com.blueroom.vm;

import java.util.List;

public class TrainingMailVM {

	private Long projectId;
	private String mailSubject;
	private String mailDescription;
	private List<String> upgradeSkillUserList;
	
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailDescription() {
		return mailDescription;
	}
	public void setMailDescription(String mailDescription) {
		this.mailDescription = mailDescription;
	}
	public List<String> getUpgradeSkillUserList() {
		return upgradeSkillUserList;
	}
	public void setUpgradeSkillUserList(List<String> upgradeSkillUserList) {
		this.upgradeSkillUserList = upgradeSkillUserList;
	}
	
	
}
