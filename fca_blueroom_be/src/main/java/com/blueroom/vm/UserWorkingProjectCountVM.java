package com.blueroom.vm;

public class UserWorkingProjectCountVM {

	private Long userId;
	private int totalWorkingProject;
	private int totalProjectLeader;
	private int totalProjectCoach;
	private int totalProjectMember;
	private int totalProjectSaving;
	
	public int getTotalWorkingProject() {
		return totalWorkingProject;
	}
	public void setTotalWorkingProject(int totalWorkingProject) {
		this.totalWorkingProject = totalWorkingProject;
	}
	public int getTotalProjectLeader() {
		return totalProjectLeader;
	}
	public void setTotalProjectLeader(int totalProjectLeader) {
		this.totalProjectLeader = totalProjectLeader;
	}
	public int getTotalProjectCoach() {
		return totalProjectCoach;
	}
	public void setTotalProjectCoach(int totalProjectCoach) {
		this.totalProjectCoach = totalProjectCoach;
	}
	public int getTotalProjectMember() {
		return totalProjectMember;
	}
	public void setTotalProjectMember(int totalProjectMember) {
		this.totalProjectMember = totalProjectMember;
	}
	public int getTotalProjectSaving() {
		return totalProjectSaving;
	}
	public void setTotalProjectSaving(int totalProjectSaving) {
		this.totalProjectSaving = totalProjectSaving;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
