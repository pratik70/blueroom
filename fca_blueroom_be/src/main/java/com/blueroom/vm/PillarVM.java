package com.blueroom.vm;

import java.util.List;

import com.blueroom.entities.auth.AuthUser;

public class PillarVM {
	private Long id;
	
	private String name;
	
	private String code;
	
	private AuthUser managerID;
	
	private List<IDVM> memberID;
	
	private List<IDVM> competencyID;
	
	private List<IDVM> removingCompetencyID;
	
	private List<IDVM> removingMemberID;
	
	private Boolean status=true;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public AuthUser getManagerID() {
		return managerID;
	}

	public void setManagerID(AuthUser managerID) {
		this.managerID = managerID;
	}

    public List<IDVM> getMemberID() {
		return memberID;
	}

	public void setMemberID(List<IDVM> memberID) {
		this.memberID = memberID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<IDVM> getRemovingMemberID() {
		return removingMemberID;
	}

	public void setRemovingMemberID(List<IDVM> removingMemberID) {
		this.removingMemberID = removingMemberID;
	}

	public List<IDVM> getCompetencyID() {
		return competencyID;
	}

	public void setCompetencyID(List<IDVM> comppitancyID) {
		this.competencyID = comppitancyID;
	}

	public List<IDVM> getRemovingCompetencyID() {
		return removingCompetencyID;
	}

	public void setRemovingCompetencyID(List<IDVM> removingCompetencyID) {
		this.removingCompetencyID = removingCompetencyID;
	}

}
