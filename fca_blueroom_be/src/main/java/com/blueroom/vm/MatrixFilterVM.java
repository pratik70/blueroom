package com.blueroom.vm;

import com.blueroom.entities.ETU;
import java.util.List;

public class MatrixFilterVM {

    private String currentWorkingState;
    private Integer startOfYear;
    private Integer startOfMonth;
    private Integer endOfYear;
    private Integer endOfMonth;
    private String isEmployee;
    private List<ETU> etuList;

    public Integer getStartOfYear() {
        return startOfYear;
    }

    public void setStartOfYear(Integer startOfYear) {
        this.startOfYear = startOfYear;
    }

    public Integer getStartOfMonth() {
        return startOfMonth;
    }

    public void setStartOfMonth(Integer startOfMonth) {
        this.startOfMonth = startOfMonth;
    }

    public Integer getEndOfYear() {
        return endOfYear;
    }

    public void setEndOfYear(Integer endOfYear) {
        this.endOfYear = endOfYear;
    }

    public Integer getEndOfMonth() {
        return endOfMonth;
    }

    public void setEndOfMonth(Integer endOfMonth) {
        this.endOfMonth = endOfMonth;
    }

    public String getCurrentWorkingState() {
        return currentWorkingState;
    }

    public void setCurrentWorkingState(String currentWorkingState) {
        this.currentWorkingState = currentWorkingState;
    }

    public List<ETU> getEtuList() {
        return etuList;
    }

    public void setEtuList(List<ETU> etuList) {
        this.etuList = etuList;
    }

	public String getIsEmployee() {
		return isEmployee;
	}

	public void setIsEmployee(String isEmployee) {
		this.isEmployee = isEmployee;
	}



}
