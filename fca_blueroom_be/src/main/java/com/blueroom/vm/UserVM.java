package com.blueroom.vm;

public class UserVM {

	private Long id;
	private String name;
	private Integer workingProject; 
	private Long etuLevel;
	private UserWorkingProjectCountVM projectWorkingCount;
	private String profileImage;
	private Long employeeId;
	
	public String getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
	public Integer getWorkingProject() {
		return workingProject;
	}
	public void setWorkingProject(Integer workingProject) {
		this.workingProject = workingProject;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getEtuLevel() {
		return etuLevel;
	}
	public void setEtuLevel(Long etuLevel) {
		this.etuLevel = etuLevel;
	}
	public UserWorkingProjectCountVM getProjectWorkingCount() {
		return projectWorkingCount;
	}
	public void setProjectWorkingCount(UserWorkingProjectCountVM projectWorkingCount) {
		this.projectWorkingCount = projectWorkingCount;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	

}
