package com.blueroom.vm;

public class FactorizationProjectSavingVM {
	
	private Integer month;
	private Integer monthlyProjectSaving;
	private Double monthFactor;
	private Integer factorYear;
	
	public FactorizationProjectSavingVM() {
		super();
	}
	public FactorizationProjectSavingVM(Integer month,Integer factorYear) {
		this.month = month;
		this.factorYear = factorYear;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getMonthlyProjectSaving() {
		return monthlyProjectSaving;
	}
	public void setMonthlyProjectSaving(Integer monthlyProjectSaving) {
		this.monthlyProjectSaving = monthlyProjectSaving;
	}
	public Double getMonthFactor() {
		return monthFactor;
	}
	public void setMonthFactor(Double monthFactor) {
		this.monthFactor = monthFactor;
	}
	public Integer getFactorYear() {
		return factorYear;
	}
	public void setFactorYear(Integer factorYear) {
		this.factorYear = factorYear;
	}
	
	

}
