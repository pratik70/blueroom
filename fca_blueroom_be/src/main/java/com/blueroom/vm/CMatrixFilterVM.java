package com.blueroom.vm;

import com.blueroom.entities.ETU;
import java.util.List;

public class CMatrixFilterVM {

    private Long etuType;
    private int year;
    private int month;
    private List<ETU> etuList;

    public Long getEtuType() {
        return etuType;
    }

    public void setEtuType(Long etuType) {
        this.etuType = etuType;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public List<ETU> getEtuList() {
        return etuList;
    }

    public void setEtuList(List<ETU> etuList) {
        this.etuList = etuList;
    }

}
