package com.blueroom.vm;

import java.util.List;

public class DevelopmentMethodVM {

	private Long id;
	
	private String name;
	
	private List<IDVM > tools;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<IDVM > getTools() {
		return tools;
	}

	public void setTools(List<IDVM > tools) {
		this.tools = tools;
	}
}
