package com.blueroom.vm;

import java.util.List;

import com.blueroom.entities.ETU;

public class StageFilterVM {

private Long stageId;
private Long projectId;
private Long userId;
private Long roleId;
private Long pWorkingStage;
private Long dWorkingStage;
private Long cWorkingStage;
private Long aWorkingStage;


public Long getStageId() {
	return stageId;
}
public void setStageId(Long stageId) {
	this.stageId = stageId;
}
public Long getProjectId() {
	return projectId;
}
public void setProjectId(Long projectId) {
	this.projectId = projectId;
}
public Long getUserId() {
	return userId;
}
public void setUserId(Long userId) {
	this.userId = userId;
}
public Long getRoleId() {
	return roleId;
}
public void setRoleId(Long roleId) {
	this.roleId = roleId;
}
public Long getpWorkingStage() {
	return pWorkingStage;
}
public void setpWorkingStage(Long pWorkingStage) {
	this.pWorkingStage = pWorkingStage;
}
public Long getdWorkingStage() {
	return dWorkingStage;
}
public void setdWorkingStage(Long dWorkingStage) {
	this.dWorkingStage = dWorkingStage;
}
public Long getcWorkingStage() {
	return cWorkingStage;
}
public void setcWorkingStage(Long cWorkingStage) {
	this.cWorkingStage = cWorkingStage;
}
public Long getaWorkingStage() {
	return aWorkingStage;
}
public void setaWorkingStage(Long aWorkingStage) {
	this.aWorkingStage = aWorkingStage;
}


	
}
