package com.blueroom.vm;

import java.util.ArrayList;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.Pillar;

public class CompetencyPillarVM {
	
	public Competencies competency;
	
	public ArrayList<Pillar> pillars;

	public Competencies getCompetency() {
		return competency;
	}

	public void setCompetency(Competencies competency) {
		this.competency = competency;
	}

	public ArrayList<Pillar> getPillars() {
		return pillars;
	}

	public void setPillars(ArrayList<Pillar> pillars) {
		this.pillars = pillars;
	}

}
