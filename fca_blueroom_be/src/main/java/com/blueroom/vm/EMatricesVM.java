package com.blueroom.vm;

import java.util.HashMap;
import java.util.List;

public class EMatricesVM {
	private Integer pageNumber;
	private Integer size;
	private Integer totalCount;
	private Integer totalTeamMemberCount;
	private List<HashMap<String, Object>> projectList;
	private HashMap<String, String> membersColumns;
	
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public List<HashMap<String, Object>> getProjectList() {
		return projectList;
	}
	public void setProjectList(List<HashMap<String, Object>> projectList) {
		this.projectList = projectList;
	}
	
	public HashMap<String, String> getMembersColumns() {
		return membersColumns;
	}
	public void setMembersColumns(HashMap<String, String> membersColumns) {
		this.membersColumns = membersColumns;
	}
	public Integer getTotalTeamMemberCount() {
		return totalTeamMemberCount;
	}
	public void setTotalTeamMemberCount(Integer totalTeamMemberCount) {
		this.totalTeamMemberCount = totalTeamMemberCount;
	}
	
	
}
