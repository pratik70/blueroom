package com.blueroom.vm;

import java.util.HashMap;
import java.util.List;

public class MatricesVM {

	private Integer pageNumber;
	private Integer size;
	private Integer totalCount;
	private Integer totalTeamMemberCount;
	private List<HashMap<String, Object>> projectList;
	private HashMap<String, String> methodColumns;
	private HashMap<String, String> toolColumns;
	private HashMap<String, String> pillarColumns;
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Integer getTotalTeamMemberCount() {
		return totalTeamMemberCount;
	}
	public void setTotalTeamMemberCount(Integer totalTeamMemberCount) {
		this.totalTeamMemberCount = totalTeamMemberCount;
	}
	public List<HashMap<String, Object>> getProjectList() {
		return projectList;
	}
	public void setProjectList(List<HashMap<String, Object>> projectList) {
		this.projectList = projectList;
	}
	public HashMap<String, String> getMethodColumns() {
		return methodColumns;
	}
	public void setMethodColumns(HashMap<String, String> methodColumns) {
		this.methodColumns = methodColumns;
	}
	public HashMap<String, String> getToolColumns() {
		return toolColumns;
	}
	public void setToolColumns(HashMap<String, String> toolColumns) {
		this.toolColumns = toolColumns;
	}
	public HashMap<String, String> getPillarColumns() {
		return pillarColumns;
	}
	public void setPillarColumns(HashMap<String, String> pillarColumns) {
		this.pillarColumns = pillarColumns;
	}

	
}
