package com.blueroom.vm;

import java.math.BigInteger;

public class EtuSavingsVM {

	Long etuId;
	BigInteger actualSavings;
	 private String etu_name;
	 
	 public EtuSavingsVM(Long etuId, BigInteger actualSavings, String etu_name) {
		// TODO Auto-generated constructor stub
		 this.etuId=etuId;
		 this.actualSavings=actualSavings;
		 this.etu_name=etu_name;
	}



	public EtuSavingsVM() {
		// TODO Auto-generated constructor stub
	}



	public Long getEtuId() {
		return etuId;
	}



	public void setEtuId(Long etuId) {
		this.etuId = etuId;
	}



	public BigInteger getActualSavings() {
		return actualSavings;
	}

	public void setActualSavings(BigInteger actualSavings) {
		this.actualSavings = actualSavings;
	}

	public String getEtu_name() {
		return etu_name;
	}

	public void setEtu_name(String etu_name) {
		this.etu_name = etu_name;
	}
	 
}
