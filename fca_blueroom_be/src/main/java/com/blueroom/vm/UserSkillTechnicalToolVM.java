package com.blueroom.vm;

import com.blueroom.entities.TechnicalTool;
import com.blueroom.entities.auth.AuthUser;

public class UserSkillTechnicalToolVM {
	private Long id;
	private AuthUser user;
	private TechnicalTool technicalTool;
	private Long actualSkill;
	private String commentText;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public AuthUser getUser() {
		return user;
	}
	public void setUser(AuthUser user) {
		this.user = user;
	}
	public TechnicalTool getTechnicalTool() {
		return technicalTool;
	}
	public void setTechnicalTool(TechnicalTool technicalTool) {
		this.technicalTool = technicalTool;
	}
	public Long getActualSkill() {
		return actualSkill;
	}
	public void setActualSkill(Long long1) {
		this.actualSkill = long1;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	
	
}
