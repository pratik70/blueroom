package com.blueroom.vm;

public class TechnicalToolVM {

	private Long technicalToolId;
	private String profileName;
	private String technicalToolDescription;
	private Integer actualSkill;
	private Integer requiredSkill;
	private Long updatedSkill;
	private String comment;
	private Long projectId;
	private Long userId;
	public Long getTechnicalToolId() {
		return technicalToolId;
	}
	public void setTechnicalToolId(Long technicalToolId) {
		this.technicalToolId = technicalToolId;
	}
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public String getTechnicalToolDescription() {
		return technicalToolDescription;
	}
	public void setTechnicalToolDescription(String technicalToolDescription) {
		this.technicalToolDescription = technicalToolDescription;
	}
	public Integer getActualSkill() {
		return actualSkill;
	}
	public void setActualSkill(Integer actualSkill) {
		this.actualSkill = actualSkill;
	}
	public Integer getRequiredSkill() {
		return requiredSkill;
	}
	public void setRequiredSkill(Integer requiredSkill) {
		this.requiredSkill = requiredSkill;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "TechnicalToolVM [technicalToolId=" + technicalToolId + ", profileName=" + profileName
				+ ", technicalToolDescription=" + technicalToolDescription + ", actualSkill=" + actualSkill
				+ ", requiredSkill=" + requiredSkill + ", projectId=" + projectId + ", userId=" + userId + "]";
	}
	public Long getUpdatedSkill() {
		return updatedSkill;
	}
	public void setUpdatedSkill(Long updatedSkill) {
		this.updatedSkill = updatedSkill;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
