package com.blueroom.vm;

import com.blueroom.entities.ETU;

public class MachineOperationVM {
	private Long etu_Id;
	
	private String machineName;
	
	private String machineCode;
	
	private String etuName;

	public Long getEtu_Id() {
		return etu_Id;
	}

	public void setEtu_Id(Long etu_Id) {
		this.etu_Id = etu_Id;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getMachineCode() {
		return machineCode;
	}

	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}

	public String getEtuName() {
		return etuName;
	}

	public void setEtuName(String etuName) {
		this.etuName = etuName;
	}

	
	
	
	
}
