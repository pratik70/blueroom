package com.blueroom.vm;

import java.util.List;

public class DevelopmentPlanVM {
	
	private Long userId;
	
	private Long competencyId;
	
	private Integer year;
	
	private List<IDVM> toolId;
	
	private Long methodId;
	
	private String type;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCompetencyId() {
		return competencyId;
	}

	public void setCompetencyId(Long competencyId) {
		this.competencyId = competencyId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public List<IDVM> getToolId() {
		return toolId;
	}

	public void setToolId(List<IDVM> toolId) {
		this.toolId = toolId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getMethodId() {
		return methodId;
	}

	public void setMethodId(Long methodId) {
		this.methodId = methodId;
	}

}
