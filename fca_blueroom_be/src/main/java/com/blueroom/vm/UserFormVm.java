package com.blueroom.vm;


public class UserFormVm {
	
	
	private long userId;
	
	private long employeeId;
	
	private int userAge;
	
	private String userFname;
	
	private String userGender;
	
	private String userLname;

	public long getuserId() {
		return userId;
	}

	public void setuserId(long userId) {
		this.userId = userId;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public int getuserAge() {
		return userAge;
	}

	public void setuserAge(int userAge) {
		this.userAge = userAge;
	}

	public String getuserFname() {
		return userFname;
	}

	public void setuserFname(String userFname) {
		this.userFname = userFname;
	}

	public String getuserGender() {
		return userGender;
	}

	public void setuserGender(String userGender) {
		this.userGender = userGender;
	}

	public String getuserLname() {
		return userLname;
	}

	public void setuserLname(String userLname) {
		this.userLname = userLname;
	}
	
	
	
	
}
