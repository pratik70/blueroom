package com.blueroom.vm;

import java.util.Date;

public class FactorizationVM {

	private Long    id;
	private Double planValue;
	private Double actualValue;
	private Double factorValue;
	private Integer  factorMonth;
	private Integer  factorYear;
	private Long etuId;
	private Date   createdAt;
	private Date   updatedAt;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getPlanValue() {
		return planValue;
	}
	public void setPlanValue(Double planValue) {
		this.planValue = planValue;
	}
	public Double getActualValue() {
		return actualValue;
	}
	public void setActualValue(Double actualValue) {
		this.actualValue = actualValue;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public Integer getFactorMonth() {
		return factorMonth;
	}
	public void setFactorMonth(Integer factorMonth) {
		this.factorMonth = factorMonth;
	}
	public Double getFactorValue() {
		return factorValue;
	}
	public void setFactorValue(Double factorValue) {
		this.factorValue = factorValue;
	}
	public Integer getFactorYear() {
		return factorYear;
	}
	public void setFactorYear(Integer factorYear) {
		this.factorYear = factorYear;
	}
	public Long getEtuId() {
		return etuId;
	}
	public void setEtuId(Long etuId) {
		this.etuId = etuId;
	}
	
	
}
