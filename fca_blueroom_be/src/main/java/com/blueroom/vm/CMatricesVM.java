package com.blueroom.vm;

public class CMatricesVM {

	private Long id;
	private String description;
	private Integer totalLossValue;
	private String machine;
	private String process;
	private String etu;
	private String lossType;
	private String dataDate;

	private Integer directLabour;
	private Integer indirectLabour;
	private Integer salariedPayroll;
	private Integer indirectMaterial;
	private Integer manintenanceMaterial;
	private Integer scrap;
	private Integer energy;
	private Integer otherServices;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getTotalLossValue() {
		return totalLossValue;
	}
	public void setTotalLossValue(Integer totalLossValue) {
		this.totalLossValue = totalLossValue;
	}
	public String getMachine() {
		return machine;
	}
	public void setMachine(String machine) {
		this.machine = machine;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getEtu() {
		return etu;
	}
	public void setEtu(String etu) {
		this.etu = etu;
	}
	public String getLossType() {
		return lossType;
	}
	public void setLossType(String lossType) {
		this.lossType = lossType;
	}
	public String getDataDate() {
		return dataDate;
	}
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	public Integer getDirectLabour() {
		return directLabour;
	}
	public void setDirectLabour(Integer directLabour) {
		this.directLabour = directLabour;
	}
	public Integer getIndirectLabour() {
		return indirectLabour;
	}
	public void setIndirectLabour(Integer indirectLabour) {
		this.indirectLabour = indirectLabour;
	}
	public Integer getSalariedPayroll() {
		return salariedPayroll;
	}
	public void setSalariedPayroll(Integer salariedPayroll) {
		this.salariedPayroll = salariedPayroll;
	}
	public Integer getIndirectMaterial() {
		return indirectMaterial;
	}
	public void setIndirectMaterial(Integer indirectMaterial) {
		this.indirectMaterial = indirectMaterial;
	}
	public Integer getManintenanceMaterial() {
		return manintenanceMaterial;
	}
	public void setManintenanceMaterial(Integer manintenanceMaterial) {
		this.manintenanceMaterial = manintenanceMaterial;
	}
	public Integer getScrap() {
		return scrap;
	}
	public void setScrap(Integer scrap) {
		this.scrap = scrap;
	}
	public Integer getEnergy() {
		return energy;
	}
	public void setEnergy(Integer energy) {
		this.energy = energy;
	}
	public Integer getOtherServices() {
		return otherServices;
	}
	public void setOtherServices(Integer otherServices) {
		this.otherServices = otherServices;
	}

}
