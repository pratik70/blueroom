package com.blueroom.vm;

public class MethodToolsUserOperator {

	private MethodsAndToolsVM methodsAndToolsVM;
	private ProjectVM projectVM;
	private Integer skill;
	
	public MethodsAndToolsVM getMethodsAndToolsVM() {
		return methodsAndToolsVM;
	}
	public void setMethodsAndToolsVM(MethodsAndToolsVM methodsAndToolsVM) {
		this.methodsAndToolsVM = methodsAndToolsVM;
	}
	public ProjectVM getProjectVM() {
		return projectVM;
	}
	public void setProjectVM(ProjectVM projectVM) {
		this.projectVM = projectVM;
	}
	public Integer getSkill() {
		return skill;
	}
	public void setSkill(Integer skill) {
		this.skill = skill;
	} 
}
