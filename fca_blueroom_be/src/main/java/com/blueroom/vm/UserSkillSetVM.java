package com.blueroom.vm;

import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.auth.AuthUser;

public class UserSkillSetVM {

	private Long id;
	private AuthUser user;
	private MethodsAndTools methodsTools;
	private Long actualSkill;
	private String commentText;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public AuthUser getUser() {
		return user;
	}
	public void setUser(AuthUser user) {
		this.user = user;
	}
	public MethodsAndTools getMethodsTools() {
		return methodsTools;
	}
	public void setMethodsTools(MethodsAndTools methodsTools) {
		this.methodsTools = methodsTools;
	}
	public Long getActualSkill() {
		return actualSkill;
	}
	public void setActualSkill(Long long1) {
		this.actualSkill = long1;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	
}
