package com.blueroom.vm;

import java.util.Date;

public class ProjectVM {

    private Long projectId;
    private String projectName;
    private String projectCode;
    private Double yearlyPotential;
    private Long lossType;
    private Long optMachine;
    private Long process;
    private Long etu;
    private String preliminaryAnalysis;
    private String description;
    private boolean horizontalExpansion;
    private Date completionDate;
    private String remark;
    private Integer directLoss;
    private Integer indirectLoss;
    private Integer staffLoss;
    private Integer indirectMaterialsLoss;
    private Integer maintenanceLoss;
    private Integer scrapLoss;
    private Integer energyLoss;
    private Integer expensessLoss;
    private Date createdAt;
    private Date updatedAt;
    private Boolean standardisation;
    private String standardisationRemark;
    private Boolean epmInfo;
    private String epmInfoRemark;
    private String document;
    private Long projectCD;
    private String ewDocument;
    private boolean finance_mail_send;
    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Double getYearlyPotential() {
        return yearlyPotential;
    }

    public void setYearlyPotential(Double yearlyPotential) {
        this.yearlyPotential = yearlyPotential;
    }

    public Long getLossType() {
        return lossType;
    }

    public void setLossType(Long lossType) {
        this.lossType = lossType;
    }

    public Long getOptMachine() {
        return optMachine;
    }

    public void setOptMachine(Long optMachine) {
        this.optMachine = optMachine;
    }

    public Long getProcess() {
        return process;
    }

    public void setProcess(Long process) {
        this.process = process;
    }

    public Long getEtu() {
        return etu;
    }

    public void setEtu(Long etu) {
        this.etu = etu;
    }

    public String getPreliminaryAnalysis() {
        return preliminaryAnalysis;
    }

    public void setPreliminaryAnalysis(String preliminaryAnalysis) {
        this.preliminaryAnalysis = preliminaryAnalysis;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "ProjectVM [projectId=" + projectId + ", projectName=" + projectName + ", yearlyPotential="
                + yearlyPotential + ", lossType=" + lossType + ", optMachine=" + optMachine + ", process=" + process
                + ", etu=" + etu + ", preliminaryAnalysis=" + preliminaryAnalysis + ", description=" + description
                + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public boolean isHorizontalExpansion() {
        return horizontalExpansion;
    }

    public void setHorizontalExpansion(boolean horizontalExpansion) {
        this.horizontalExpansion = horizontalExpansion;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDirectLoss() {
        return directLoss;
    }

    public void setDirectLoss(Integer directLoss) {
        this.directLoss = directLoss;
    }

    public Integer getIndirectLoss() {
        return indirectLoss;
    }

    public void setIndirectLoss(Integer indirectLoss) {
        this.indirectLoss = indirectLoss;
    }

    public Integer getStaffLoss() {
        return staffLoss;
    }

    public void setStaffLoss(Integer staffLoss) {
        this.staffLoss = staffLoss;
    }

    public Integer getIndirectMaterialsLoss() {
        return indirectMaterialsLoss;
    }

    public void setIndirectMaterialsLoss(Integer indirectMaterialsLoss) {
        this.indirectMaterialsLoss = indirectMaterialsLoss;
    }

    public Integer getMaintenanceLoss() {
        return maintenanceLoss;
    }

    public void setMaintenanceLoss(Integer maintenanceLoss) {
        this.maintenanceLoss = maintenanceLoss;
    }

    public Integer getScrapLoss() {
        return scrapLoss;
    }

    public void setScrapLoss(Integer scrapLoss) {
        this.scrapLoss = scrapLoss;
    }

    public Integer getEnergyLoss() {
        return energyLoss;
    }

    public void setEnergyLoss(Integer energyLoss) {
        this.energyLoss = energyLoss;
    }

    public Integer getExpensessLoss() {
        return expensessLoss;
    }

    public void setExpensessLoss(Integer expensessLoss) {
        this.expensessLoss = expensessLoss;
    }

    public Boolean getStandardisation() {
        return standardisation;
    }

    public void setStandardisation(Boolean standardisation) {
        this.standardisation = standardisation;
    }

    public String getStandardisationRemark() {
        return standardisationRemark;
    }

    public void setStandardisationRemark(String standardisationRemark) {
        this.standardisationRemark = standardisationRemark;
    }

    public Boolean getEpmInfo() {
        return epmInfo;
    }

    public void setEpmInfo(Boolean epmInfo) {
        this.epmInfo = epmInfo;
    }

    public String getEpmInfoRemark() {
        return epmInfoRemark;
    }

    public void setEpmInfoRemark(String epmInfoRemark) {
        this.epmInfoRemark = epmInfoRemark;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Long getProjectCD() {
        return projectCD;
    }

    public void setProjectCD(Long projectCD) {
        this.projectCD = projectCD;
    }

    public String getEwDocument() {
        return ewDocument;
    }

    public void setEwDocument(String ewDocument) {
        this.ewDocument = ewDocument;
    }

	public boolean isFinance_mail_send() {
		return finance_mail_send;
	}

	public void setFinance_mail_send(boolean finance_mail_send) {
		this.finance_mail_send = finance_mail_send;
	}

    
    
    

}
