package com.blueroom.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.CompetencyPillar;
import com.blueroom.entities.Pillar;
import com.blueroom.entities.UserDesiredLevel;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.CompetenciesRepository;
import com.blueroom.repository.CompetencyPillarRepository;
import com.blueroom.repository.PillarRepository;

@Component
public class PillarDesiredLevelExcelUtil {
	@Autowired
	PillarRepository pillarRepository;
	
	@Autowired
	CompetencyPillarRepository competencyPillarRepository;
	
	@Autowired
	CompetenciesRepository competenciesRepository;
	
	private DataFormatter formatter = new DataFormatter();

	public  Map<String, Object> parse(MultipartFile readableFile)throws IOException {
		
		  Workbook workbook = new XSSFWorkbook(readableFile.getInputStream());
		  List<String> success = new ArrayList<String>();
	        List<String> failed = new ArrayList<String>();
	        
	        for(int cnt = 0; cnt < workbook.getNumberOfSheets(); cnt++) {
	        	Sheet sheet = workbook.getSheetAt(cnt);
	      
	        	Pillar pillar =  pillarRepository.findOneByPillarName(sheet.getSheetName());
	        	
	        	   for (int index = 2; index < sheet.getPhysicalNumberOfRows(); index++) {
	        		   
	        			String competencyType="";
	                	
	                	if(index >=2 && index <=11) {
	                		competencyType="Reactive";
	                	}else if(index >=12 && index <=21) {    
	                		competencyType="Preventive";
	                	}else if(index >=22){
	                		competencyType="Proactive";
	                	}
	                	try {
	                		List<CompetencyPillar> userDesiredLevels = map(pillar, sheet.getRow(index), competencyType);
	                	 	
	                	}catch(IllegalArgumentException e) {
	                    	failed.add(index + ") " + e.getMessage());
	                	} catch(Exception e){
	                    	failed.add(index + ") Failed to read row");
	                	}
	        	   
	        	   }
	        }
	        workbook.close();
	        Map<String, Object> result = new HashMap<String, Object>();
	        result.put("success", success);
	        result.put("failed", failed);
	    	return result;
	}
	private List<CompetencyPillar> map(Pillar pillarId,  Row row, String competencyType) {
		List<CompetencyPillar> competencyPillars = new ArrayList<CompetencyPillar>();
		Competencies competencies = competenciesRepository.findByNameAndCompetencyType(readString(row.getCell(1)), competencyType);
		CompetencyPillar competencyPillar=	competencyPillarRepository.findOneByCompetencyIdAndPillarId(competencies.getId(),  pillarId.getId());
		if(competencyPillar!=null)
	{
			competencyPillar.setId(competencyPillar.getId());
			competencyPillar.setStatus(true);
			competencyPillar.setCompetency(competencyPillar.getCompetency());
			competencyPillar.setPillar(competencyPillar.getPillar());
			competencyPillar.setRequired_level(readLong(row.getCell(3)));
			competencyPillar.setCurrent_level(readLong(row.getCell(4)));
			competencyPillar.setLast_year_level(readLong(row.getCell(5)));
			//competencyPillars.add(competencyPillar);
			competencyPillarRepository.save(competencyPillar);
//		userDesiredLevel.setActualSkill(readLong(row.getCell(4)));
//		userDesiredLevel.setYear(2021);
//		userDesiredLevel.setPlanStatus("Created");
//		userDesiredLevel.setStatus(true);
//		userDesiredLevels.add(userDesiredLevel);
//		
//		UserDesiredLevel userDesiredLevel1 = new UserDesiredLevel();
//		userDesiredLevel1.setUser(user_id);
//		userDesiredLevel1.setCompetencies(competencies);
//		userDesiredLevel1.setRequiredSkill(readLong(row.getCell(5)));
//		userDesiredLevel1.setActualSkill(readLong(row.getCell(5)));
//		userDesiredLevel1.setYear(2020);
//		userDesiredLevel1.setPlanStatus("Created");
//		userDesiredLevel1.setStatus(true);
//		userDesiredLevels.add(userDesiredLevel1);
//		}
	}
	    return competencyPillars;
    }
	
	 private String readString(Cell cell) {
			String val = null;
			if(cell != null) {
				try {
					val = cell.getStringCellValue();
				} catch (Exception e) {
					val = formatter.formatCellValue(cell);
				}
			}
	    	return val;
		}
	 
	 protected Long readLong(Cell cell) {
			Long val = 0l;
			if(cell != null) {
				try {
					try {
						val = (long) cell.getNumericCellValue();
					} catch (Exception e) {
						val = Long.parseLong(formatter.formatCellValue(cell));
					}
				} catch(Exception e) {}
			}
			return val;
		}

}
