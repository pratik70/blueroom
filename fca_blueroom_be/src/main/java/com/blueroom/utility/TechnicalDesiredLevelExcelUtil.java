package com.blueroom.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.UserDesiredLevel;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.CompetenciesRepository;
import com.blueroom.repository.UserDao;
import com.blueroom.services.UserDesiredLevelService;

@Component
public class TechnicalDesiredLevelExcelUtil {
	

	@Autowired
	UserDesiredLevelService userDesiredLevelService;
	
	@Autowired
	UserDao userDao;
	
	 private DataFormatter formatter = new DataFormatter();
	 
		@Autowired
		CompetenciesRepository competenciesRepository;
	
	public  Map<String, Object> parse(MultipartFile readableFile)throws IOException {
		
		  Workbook workbook = new XSSFWorkbook(readableFile.getInputStream());
		  List<String> success = new ArrayList<String>();
	        List<String> failed = new ArrayList<String>();
	        
	        
	        for(int cnt = 0; cnt < workbook.getNumberOfSheets(); cnt++) {
	        	Sheet sheet = workbook.getSheetAt(cnt);
	        	
	        	List<AuthUser> authUsers = userDao.findAllByName(sheet.getSheetName());
	        	
	        	   for (int index = 1; index < sheet.getPhysicalNumberOfRows(); index++) {
	        		   
	        		   try {
	               		List<UserDesiredLevel> userDesiredLevels = map(authUsers.get(0), sheet.getRow(index));
	                   	int res = process(userDesiredLevels.get(0));
	                       if(res == 1) {
	                       	success.add(index + ") " + userDesiredLevels.get(0).getCompetencies() + " Added successfully");
	                       } else if(res == -1) {
	                       	success.add(index + ") " + userDesiredLevels.get(0).getCompetencies() + " Updated successfully");
	                       } else {
	                       	failed.add(index + ") " + userDesiredLevels.get(0).getCompetencies() + " Unable to add");
	                       }
	   					res = process(userDesiredLevels.get(1));
	                       if(res == 1) {
	                       	success.add(index + ") " + userDesiredLevels.get(0).getCompetencies() + " Added successfully");
	                       } else if(res == -1) {
	                       	success.add(index + ") " + userDesiredLevels.get(0).getCompetencies() + " Updated successfully");
	                       } else {
	                       	failed.add(index + ") " + userDesiredLevels.get(0).getCompetencies() + " Unable to add");
	                       }
	               	} catch(IllegalArgumentException e) {
	                   	failed.add(index + ") " + e.getMessage());
	               	} catch(Exception e){
	                   	failed.add(index + ") Failed to read row");
	               	}
	               }

	        }
	           workbook.close();
	           Map<String, Object> result = new HashMap<String, Object>();
	           result.put("success", success);
	           result.put("failed", failed);
	       	return result;
	        	   }
	    
	  private int process(UserDesiredLevel userDesiredLevel) throws IllegalArgumentException {
	    	int res = 0;
	UserDesiredLevel existingUserDesiredLevel = null;
	    	
	    	if(userDesiredLevel.getId() != null) {
	    		existingUserDesiredLevel = userDesiredLevelService.getUserDesiredLevelById(userDesiredLevel.getId());
	    	}
	    	if(existingUserDesiredLevel == null) {
	    		userDesiredLevelService.addUserDesiredLevel(userDesiredLevel);
	    		res = 1;
	    	} else {
	    		existingUserDesiredLevel.setCompetencies(userDesiredLevel.getCompetencies());
	    		existingUserDesiredLevel.setUser(userDesiredLevel.getUser());
	    		existingUserDesiredLevel.setActualSkill(userDesiredLevel.getActualSkill());
	    		existingUserDesiredLevel.setRequiredSkill(userDesiredLevel.getRequiredSkill());
	    		existingUserDesiredLevel.setPlanStatus(userDesiredLevel.getPlanStatus());
	    		existingUserDesiredLevel.setYear(userDesiredLevel.getYear());
	    		existingUserDesiredLevel.setStatus(userDesiredLevel.isStatus());
	       		res = -1;
	    	}
			return res;
		}

	private List<UserDesiredLevel> map(AuthUser user_id,  Row row) {
		List<UserDesiredLevel> userDesiredLevels = new ArrayList<UserDesiredLevel>();
		UserDesiredLevel userDesiredLevel = new UserDesiredLevel();
		userDesiredLevel.setUser(user_id);
		Competencies competencies = competenciesRepository.findByNameAndCompetencyType(readString(row.getCell(0)),"Technical Competencies");
		if(Objects.nonNull(competencies))
		{
			
		userDesiredLevel.setCompetencies(competencies);
		userDesiredLevel.setRequiredSkill(readLong(row.getCell(2)));
		userDesiredLevel.setActualSkill(readLong(row.getCell(3)));
		userDesiredLevel.setYear(2021);
		userDesiredLevel.setPlanStatus("Created");
		userDesiredLevel.setStatus(true);
		userDesiredLevels.add(userDesiredLevel);
		
		UserDesiredLevel userDesiredLevel1 = new UserDesiredLevel();
		userDesiredLevel1.setUser(user_id);
		userDesiredLevel1.setCompetencies(competencies);
		userDesiredLevel1.setRequiredSkill(readLong(row.getCell(4)));
		userDesiredLevel1.setActualSkill(readLong(row.getCell(4)));
		userDesiredLevel1.setYear(2020);
		userDesiredLevel1.setPlanStatus("Created");
		userDesiredLevel1.setStatus(true);
		userDesiredLevels.add(userDesiredLevel1);
		}
	    return userDesiredLevels;
    }

	
	protected Long readLong(Cell cell) {
		Long val = 0l;
		if(cell != null) {
			try {
				try {
					val = (long) cell.getNumericCellValue();
				} catch (Exception e) {
					val = Long.parseLong(formatter.formatCellValue(cell));
				}
			} catch(Exception e) {}
		}
		return val;
	}

    protected Integer readInteger(Cell cell) {
    	Integer val = 0;
		if(cell != null) {
			try {
				try {
					val = Integer.parseInt(cell.getNumericCellValue() + "");
				} catch (Exception e) {
					val = Integer.parseInt(formatter.formatCellValue(cell));
				}
			} catch(Exception e) { }
		}
		return val;
	}

    private Date readDate(Cell cell) {
		Date val = null;
		if(cell != null) {
			try { val = cell.getDateCellValue(); } catch (Exception e) {}
		}
		return val;
	}

    private Date loadDate(Cell cell) {
		Date val = readDate(cell);
		if(val == null) {
    		throw new IllegalArgumentException("Unable to read date " + readString(cell));
		}
		return val;
	}

    private String readString(Cell cell) {
		String val = null;
		if(cell != null) {
			try {
				val = cell.getStringCellValue();
			} catch (Exception e) {
				val = formatter.formatCellValue(cell);
			}
		}
    	return val;
	}
}


