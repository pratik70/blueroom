package com.blueroom.utility;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.auth.AuthUser;
import com.blueroom.repository.UserDao;
import com.blueroom.services.UserDesiredLevelService;
import com.blueroom.services.UserGetservice;

@Component
public class UserEntityExcelUtil {

	@Autowired
	UserDesiredLevelService userDesiredLevelService;

	@Autowired
	UserGetservice userGetservice;

	@Autowired
	UserDao userDao;

	private DataFormatter formatter = new DataFormatter();

	public Map<String, Object> parse(MultipartFile readableFile) throws IOException {
		Workbook workbook = new XSSFWorkbook(readableFile.getInputStream());
		List<String> success = new ArrayList<String>();
		List<String> failed = new ArrayList<String>();
		for (int cnt = 0; cnt < workbook.getNumberOfSheets(); cnt++) {
			Sheet sheet = workbook.getSheetAt(cnt);

			for (int index = 1; index < sheet.getPhysicalNumberOfRows(); index++) {

				try {
					AuthUser user = map(sheet.getRow(index));

					int res = process(user);
					if (res == 1) {
						success.add(index + ") " + user.getName() + " Added successfully");
					} else if (res == -1) {
						success.add(index + ") " + user.getName() + " Updated successfully");
					} else {
						failed.add(index + ") " + user.getName() + " Unable to add");
					}

				} catch (IllegalArgumentException e) {
					failed.add(index + ") " + e.getMessage());
				} catch (Exception e) {
					failed.add(index + ") Failed to read row");
				}
			}

		}
		workbook.close();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", success);
		result.put("failed", failed);
		return result;
	}

	private int process(AuthUser user) throws IllegalArgumentException {
		int res = 0;
		AuthUser existingUser = null;

		if (user.getId() != null) {
			existingUser = userDao.findOneByemployeeId(user.getId());
		}
		if (existingUser == null) {
			user.setPassword("12345");
			userGetservice.saveUser(user);
			res = 1;
		} else {
			existingUser.setEmployeeId(user.getEmployeeId());
			existingUser.setName(user.getName());
			existingUser.setEmail(user.getEmail());
			existingUser.setPassword(user.getEmail());
			existingUser.setGender(user.getGender());
			existingUser.setPreffered_learning_style(user.getPreffered_learning_style());
			existingUser.setCity(user.getCity());
			existingUser.setFirst_joining_date(user.getFirst_joining_date());
			existingUser.setFiapl_joining_date(user.getFiapl_joining_date());
			existingUser.setEducational_background(user.getEducational_background());
			existingUser.setRoles(user.getRoles());
			existingUser.setStatus(user.getStatus());
			existingUser.setType("PD");
			res = -1;
		}
		return res;
	}

	private AuthUser map(Row row) {
		AuthUser user = new AuthUser();

		user.setEmployeeId(readLong(row.getCell(1)));
		user.setName(readString(row.getCell(2)));
		user.setEmail(readString(row.getCell(3)));
		user.setGender(readString(row.getCell(5)));
		user.setPreffered_learning_style(readString(row.getCell(6)));
		user.setCity(readString(row.getCell(7)));
		user.setFirst_joining_date(readDate(row.getCell(9)));
		user.setFiapl_joining_date(readDate(row.getCell(10)));
		user.setEducational_background(readString(row.getCell(11)));
		user.setStatus(readString(row.getCell(13)));
		user.setType("PD");
		return user;
	}

	protected Long readLong(Cell cell) {
		Long val = 0l;
		if (cell != null) {
			try {
				try {
					val = (long) cell.getNumericCellValue();
				} catch (Exception e) {
					val = Long.parseLong(formatter.formatCellValue(cell));
				}
			} catch (Exception e) {
			}
		}
		return val;
	}

	protected Integer readInteger(Cell cell) {
		Integer val = 0;
		if (cell != null) {
			try {
				try {
					val = Integer.parseInt(cell.getNumericCellValue() + "");
				} catch (Exception e) {
					val = Integer.parseInt(formatter.formatCellValue(cell));
				}
			} catch (Exception e) {
			}
		}
		return val;
	}

	private Date readDate(Cell cell) {
		Date val = null;
		if (cell != null) {
			try {
				String sDate1=readString(cell);  
			    val=new SimpleDateFormat("dd-MMM-yyyy").parse(sDate1); 
			} catch (Exception e) {
			}
		}
		return val;
	}

	private Date loadDate(Cell cell) {
		Date val = readDate(cell);
		if (val == null) {
			throw new IllegalArgumentException("Unable to read date " + readString(cell));
		}
		return val;
	}

	private String readString(Cell cell) {
		String val = null;
		if (cell != null) {
			try {
				val = cell.getStringCellValue();
			} catch (Exception e) {
				val = formatter.formatCellValue(cell);
			}
		}
		return val;
	}

}
