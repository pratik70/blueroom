package com.blueroom.auth;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.blueroom.entities.auth.AuthUser;
import com.blueroom.entities.auth.PermissionMatrix;
import com.blueroom.entities.auth.Role;
import com.blueroom.repository.AuthUserRepository;
import com.blueroom.repository.PermissionMatrixRepository;
import java.util.Objects;

@Service
public class UserService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    AuthUserRepository authUserRepository;

    @Autowired
    PermissionMatrixRepository permissionMatrixRepository;

    @Override
    public final UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AuthUser user = null;
        try {
            try {

                user = authUserRepository.findOneByUsername(username); // session.createQuery("from AuthUser where username = :username").setString("username", username).uniqueResult();
                if (Objects.nonNull(user)) {
                    List<PermissionMatrix> permissions = new ArrayList<>();
                    for (Role role : user.getRoles()) {
                        permissions = permissionMatrixRepository.findAllByRole(role);
                        permissions.addAll(permissions);
                    }
                    user.setPermissions(permissions);
                }

            } catch (org.hibernate.HibernateException ex) {
                try {
                    user = authUserRepository.findOneByUsername(username); //session.createQuery("from AuthUser where username = :username").setString("username", username).uniqueResult();
                } finally {

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }

        return user;
    }

    public AuthUser getUserByUserName(String username) {
        AuthUser user = authUserRepository.findOneByUsername(username); //(AuthUser) session.createQuery("from AuthUser where username = :username").setString("username", username).uniqueResult();
        if (user != null) {
            return user;
        } else {
            return null;
        }
    }

    /* public AuthUser getUserByUserNameAndProvider(String username,String provider){
    	Session session = sessionFactory.getCurrentSession();
    	  AuthUser user = (AuthUser) session.createQuery("from AuthUser where username = :username AND provider=:provider").setString("username", username).setString("provider", provider).uniqueResult();
    	   if(user!=null){
    		    return user;    
             }else{
               return null; 
             }
    }
     */
    public void createUser(AuthUser authUser) {
        authUserRepository.save(authUser);
    }

}