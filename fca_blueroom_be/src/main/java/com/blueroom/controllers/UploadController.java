package com.blueroom.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.services.UploadService;
import com.blueroom.vm.MessageVM;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/blueroom/api/")
public class UploadController {

    @Autowired
    UploadService uploadService;

    @RequestMapping(value = "/upload/layout", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getDEFMatricesData(@RequestParam("file") MultipartFile file, @RequestParam("layoutType") String type, @RequestParam("etu") Long etu, @RequestParam("cdYear") Long cdYear) throws Exception {
        return uploadService.uploadLayout(file, type, cdYear, etu);
    }

    @RequestMapping(value = "/layout/{layoutType}", method = RequestMethod.GET)
    @ResponseBody
    public void getImagePreview(HttpServletRequest request, HttpServletResponse response, @PathVariable("layoutType") String layoutType) {
        Long etuId = Long.parseLong(Objects.nonNull(request.getParameter("etuId")) ? request.getParameter("etuId") : "0");
        Long yearId = Long.parseLong(Objects.nonNull(request.getParameter("yearId")) ? request.getParameter("yearId") : "0");
        response.setContentType("image/jpeg");
        uploadService.getImageLayoutPreview(response, layoutType, etuId, yearId);
    }

    @RequestMapping(value = "/layout/{etuId}/{cdId}/{layoutType}", method = RequestMethod.GET)
    @ResponseBody
    public void getImagePreviewByETUAndCdYear(HttpServletResponse response, @PathVariable("layoutType") String layoutType, @PathVariable("etuId") Long etuId, @PathVariable("cdId") Long cdId) {
        response.setContentType("image/jpeg");
        uploadService.getImagePreviewByETUAndCdYear(response, layoutType, etuId, cdId);
    }

    @RequestMapping(value = "/upload/document", method = RequestMethod.POST)
    public MessageVM uploadDocumnet(@RequestParam("file") MultipartFile file) throws Exception {
        return uploadService.uploadDocumnet(file);
    }

    @RequestMapping(value = "/upload/ew-document", method = RequestMethod.POST)
    public MessageVM uploadEwDocumnet(@RequestParam("file") MultipartFile file) throws Exception {
        return uploadService.uploadDocumnet(file);
    }

    @RequestMapping(value = "/userImage/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public void getUserImage(HttpServletResponse response, @PathVariable("userId") Long userId) {
        response.setContentType("image/jpeg");
        uploadService.getUserImage(response, userId);
    }

    @RequestMapping(value = "/projectDoc/Kaizen/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public void getProjectKaizenDoc(HttpServletResponse response, @PathVariable("projectId") Long projectId) {
        uploadService.getProjectKaizenDoc(response, projectId);
    }

    @RequestMapping(value = "/projectDoc/entirework/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public void getProjectEntireWorkDoc(HttpServletResponse response, @PathVariable("projectId") Long projectId) {
        uploadService.getProjectEntireWorkDoc(response, projectId);
    }

    @RequestMapping(value = "/projectDoc/Kaizen1/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public void downloadFile(@PathVariable("projectId") Long projectId, HttpServletResponse response) {
        try {
            File file = new File("D:\\dev\\workspace\\Blue Room\\Assets\\raw_data\\userImages\\Amlendu Shaw.jpg");
            //file= new File("D:\\dev\\workspace\\Blue Room\\Assets\\raw_data\\developer.pdf");
            InputStream is = new FileInputStream(file.getPath());
            // set file as attached data and copy file data to response output stream
            //response.setContentType("application/pdf");
            //response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() );
            FileCopyUtils.copy(is, response.getOutputStream());
            // close stream and return to view
            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
