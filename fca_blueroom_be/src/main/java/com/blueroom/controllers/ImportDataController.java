package com.blueroom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.importdata.ProjectImportService;
import com.blueroom.vm.MessageVM;

@RestController
@RequestMapping("/blueroom/api/import")
public class ImportDataController {

	@Autowired
	ProjectImportService projectImportService;
	 
	@RequestMapping(value = "/project", method = RequestMethod.GET)
	@ResponseBody
	public MessageVM getAllLossGroup() {
		return projectImportService.loadProjectMatrixData();
	}
}
