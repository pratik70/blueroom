package com.blueroom.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.entities.ETU;
import com.blueroom.entities.Loss;
import com.blueroom.entities.MachineLoss;
import com.blueroom.entities.MachineOperation;
import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.TechnicalTool;
import com.blueroom.importdata.AdvancedPillarSpecificService;
import com.blueroom.importdata.IntermediatePillarSpecificService;
import com.blueroom.importdata.SkillInventoryService;
import com.blueroom.services.CommonService;
import com.blueroom.vm.MachineOperationVM;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;

@RestController
@RequestMapping("/blueroom/api/")
public class CommonController {

    @Autowired
    CommonService commonService;

    @Autowired
    AdvancedPillarSpecificService advancedPillarSpecificService;

    @Autowired
    SkillInventoryService skillInventoryService;

    @Autowired
    IntermediatePillarSpecificService intermediatePillarSpecificService;

    @RequestMapping(value = "/allLossGroup", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllLossGroup() {
        return commonService.getAllLossGroup();
    }

    @RequestMapping(value = "/allMachine/{id}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllMachine(@PathVariable("id") long etuId) {
        return commonService.getAllMachine(etuId);
    }

    @RequestMapping(value = "/allMachine", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM allMachine() {
        return commonService.getAllMachine();
    }

    @RequestMapping(value = "/allETUType", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllETUs() {
        return commonService.getAllETUs();
    }

    @RequestMapping(value = "/allProcess", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllProcess() {
        return commonService.getAllProcess();
    }

    @RequestMapping(value = "/allPillars", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllPillars() {
        return commonService.getAllPillars();
    }

    @RequestMapping(value = "/getUserList", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllUserList() {
        return commonService.getAllUserList();
    }

    @RequestMapping(value = "/getEtuUserList", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getEtuUserList(@RequestBody MatrixFilterVM filterVM) {
        return commonService.getEtuUserList(filterVM);
    }

    @RequestMapping(value = "/getAllBenifitTypeList", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllBenifitTypeList() {
        return commonService.getAllBenifitTypeList();
    }

    @RequestMapping(value = "/getAllMachineLossList", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllMachineLossList() {
        return commonService.getAllMachineLossList();
    }

    @RequestMapping(value = "/changeBMatrixDataColor", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM changeBMatrixColor(@RequestBody MachineLoss machineLoss) {
        return commonService.changeBMatrixColor(machineLoss);
    }

    @RequestMapping(value = "/getAllTechnicalToolList", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllTechnicalToolList() {
        return commonService.getAllTechnicalToolList();
    }

    @RequestMapping(value = "/getCDYearList", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getCDYearList() {
        return commonService.getCDYearList();
    }

    @RequestMapping(value = "/getKPIsList", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getKPIsList() {
        return commonService.getKPIsList();
    }

    @RequestMapping(value = "/updateLoss", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM updateLoss(HttpServletRequest request, HttpServletResponse response, @RequestBody Loss loss) {
        return commonService.updateLoss(loss);
    }

    @RequestMapping(value = "/getAllETU", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllETU() {
        return commonService.getAllETUs();
    }

    @RequestMapping(value = "/updateETU", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM updateETU(@RequestBody ETU etu) {
        return commonService.updateETU(etu);
    }

    @RequestMapping(value = "/getLoss", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<Loss> getLoss() {

        return commonService.getLoss();
    }

    @RequestMapping(value = "/getTheTechnicalTools", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<TechnicalTool> getTheTechnicalTools() {

        return commonService.getTheTechnicalTools();
    }

    @RequestMapping(value = "/getwcmMethods", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<MethodsAndTools> getWcmMethods() {

        return commonService.getWcmMethods();
    }

    @RequestMapping(value = "/getwcmTools", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<MethodsAndTools> getWcmTools() {

        return commonService.getWcmTools();
    }

    @RequestMapping(value = "/getOperations/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<MachineOperation> getOperations(@PathVariable("id") Long etu_id) {

        return commonService.getOperations(etu_id);
    }

    @RequestMapping(value = "/addWcmTool", method = RequestMethod.POST)
    @ResponseBody
    public MethodsAndTools addWcmTool(@RequestBody MethodsAndTools methodsAndTools) {

        return commonService.addWcmTool(methodsAndTools);
    }

    @RequestMapping(value = "/updateWcmTool/{id}", method = RequestMethod.POST)
    @ResponseBody
    public MethodsAndTools updateWcmTool(@PathVariable("id") Long id, @RequestBody MethodsAndTools methodsAndTools) {

        return commonService.updateWcmTool(id, methodsAndTools);
    }

    @RequestMapping(value = "/addWcmMethod", method = RequestMethod.POST)
    @ResponseBody
    public MethodsAndTools addWcmMethod(@RequestBody MethodsAndTools methodsAndTools) {

        return commonService.addWcmMethod(methodsAndTools);
    }

    @RequestMapping(value = "/updateWcmMethod/{id}", method = RequestMethod.POST)
    @ResponseBody
    public MethodsAndTools updateWcmMethod(@PathVariable("id") Long id, @RequestBody MethodsAndTools methodsAndTools) {

        return commonService.updateWcmMethod(id, methodsAndTools);
    }

    @RequestMapping(value = "/addlossType", method = RequestMethod.POST)
    @ResponseBody
    public Loss addLossType(@RequestBody Loss loss) {

        return commonService.addLossType(loss);
    }

    @RequestMapping(value = "/addtechnicalTool", method = RequestMethod.POST)
    @ResponseBody
    public TechnicalTool addTechnicalTool(@RequestBody TechnicalTool technicalTool) {

        return commonService.addTechnicalTool(technicalTool);
    }

    @RequestMapping(value = "/addOperations", method = RequestMethod.POST)
    @ResponseBody
    public MachineOperation addOperations(@RequestBody MachineOperationVM machineOperationVM) {

        return commonService.addOperations(machineOperationVM);
    }

    @RequestMapping(value = "/deleteOperation/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public MachineOperation deleteOperations(@PathVariable("id") Long id) {

        return commonService.deleteOperations(id);
    }

    ////////
    @RequestMapping(value = "/getBasicPillars", method = RequestMethod.GET)
    public MessageVM getBasicPillars() {

        return skillInventoryService.loadSkillInventoryData();
    }

    @RequestMapping(value = "/getIntermediatePillars", method = RequestMethod.GET)
    public MessageVM getIntermediatePillars() {

        return intermediatePillarSpecificService.loadIntermediateData();
    }

    @RequestMapping(value = "/getAdvancedPillars", method = RequestMethod.GET)
    public MessageVM getAdvancedPillars() {

        return advancedPillarSpecificService.loadAdvancedpillarData();
    }

}
