package com.blueroom.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.services.FactorizationService;
import com.blueroom.vm.FactorizationProjectSavingVM;
import com.blueroom.vm.FactorizationSavingAndProjectVM;
import com.blueroom.vm.FactorizationVM;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;
import java.util.ArrayList;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/blueroom/api/factorization/")
public class FactorizationController {

    @Autowired
    FactorizationService factorizationService;

    @RequestMapping(value = "/{factorYear}", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM addFactorizationData(@PathVariable Integer factorYear, @RequestBody List<FactorizationVM> factorizationList) {
        return factorizationService.addFactorizationData(factorYear, factorizationList);
    }

    @RequestMapping(value = "/{factorYear}/{etuId}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getFactorizationData(@PathVariable Integer factorYear, @PathVariable("etuId") Long etuId) {
        return factorizationService.getFactorizationData(factorYear, etuId);
    }

    //Only for Team Leader(TL)
    @RequestMapping(value = "/usersavingsonproject/{factorYear}", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getUserSavingsWithProject(@PathVariable Integer factorYear, @RequestBody MatrixFilterVM filterVM) {
        return factorizationService.getUserSavingsWithProject(factorYear, filterVM);
    }

    @RequestMapping(value = "/addUserSavingsAndProjectFactors/{factorYear}", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM addUserSavingsAndProjectFactors(@PathVariable Integer factorYear, @RequestBody List<FactorizationSavingAndProjectVM> factorizationSavingAndProjectList) {
        return factorizationService.addUserSavingsAndProjectFactors(factorYear, factorizationSavingAndProjectList);
    }

    @RequestMapping(value = "/getUserSavingsAndProjectFactors/{factorYear}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getUserSavingsAndProjectFactors(@PathVariable Integer factorYear) {
        return factorizationService.getUserSavingsAndProjectFactors(factorYear);
    }

    @RequestMapping(value = "/getProjectMonthlySavingFactor/{factorYear}/{etuID}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectMonthlySavingFactor(@PathVariable("factorYear") Integer factorYear, @PathVariable("etuID") Long etuID) {
        return factorizationService.getProjectMonthlySavingFactor(factorYear,etuID);
    }

    @RequestMapping(value = "/saveProjectSavingsFactor/{factorYear}", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveProjectSavingsFactor(@PathVariable Integer factorYear, @RequestBody List<FactorizationProjectSavingVM> factorizationProjectSavingVMs) {
        return factorizationService.saveProjectSavingsFactor(factorYear, factorizationProjectSavingVMs);
    }

    @RequestMapping(value = "/getPeoplePerformanceEvaluationReport/{factorYear}/{employees}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getUserSavingsAndProjectFactorsForReport(@PathVariable Integer factorYear, @PathVariable("employees") String employees) {
        MessageVM returnData = new MessageVM();
        String[] array = employees.split(",");
        List<Long> employess = new ArrayList<>();
        List<FactorizationSavingAndProjectVM> factorizationSavingAndProjectList = (List<FactorizationSavingAndProjectVM>) factorizationService.getUserSavingsAndProjectFactors(factorYear).getData();
        for (String empId : array) {
            employess.add(Long.parseLong(empId));
        }
        List<FactorizationSavingAndProjectVM> resultList = factorizationSavingAndProjectList.stream().filter(data -> employess.contains(data.getUserId())).collect(Collectors.toList());
        returnData.setData(resultList);
        return returnData;
    }
}
