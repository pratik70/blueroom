package com.blueroom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.services.SkillService;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/blueroom/api/skill")
public class SkillController {

    @Autowired
    SkillService skillService;

    @RequestMapping(value = "/getEtuUsersSkill", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getEtuUsersSkill(@RequestBody MatrixFilterVM filterVM) {
        return skillService.getEtuUsersSkill(filterVM);
    }

    @RequestMapping(value = "/getUserSkillReport/{year}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getUserSkillReport(@PathVariable("year") Integer year) {
        return skillService.getUserSkillReport(year);
    }
    
    @RequestMapping(value = "/getUserDesiredSkill/{id}/{year}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getUserDesiredSkillByUserId(@PathVariable("id") Integer userId, @PathVariable("year") Integer year) {
        return skillService.getUserDesiredSkillByUserId(userId, year);
    }

    @RequestMapping(value = "/getUserDesiredSkill", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getUserDesiredSkillByUserId(@RequestParam("userId") Integer userId, @RequestParam("year") Integer year, @RequestParam("area") String area) {
        return skillService.getUserDesiredSkillByUserIdAndYear(userId,year,area);
    }
    
    @RequestMapping(value = "/getTeamMaxSkillByPillar", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getTeamMaxSkillByPillarIdAndCompetencyArea(@RequestParam("pillarId") Long pillarId, @RequestParam("competency_area") String competency_area) {
        return skillService.getTeamMaxSkillByPillarIdAndCompetencyArea(pillarId, competency_area);
    }
    
    @RequestMapping(value = "/getTeamAverageSkillByPillar", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getTeamAverageSkillByPillarIdAndCompetencyArea(@RequestParam("pillarId") Long pillarId, @RequestParam("competency_area") String competency_area) {
        return skillService.getTeamAverageSkillByPillarIdAndCompetencyArea(pillarId, competency_area);
    }

    @RequestMapping(value = "/getPlantHeadTeamMaxSkill", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getPlantHeadTeamMaxSkill(@RequestParam("competency_area") String competency_area) {
        return skillService.getPlantHeadTeamMaxSkill(competency_area);
    }
    
    @RequestMapping(value = "/getPlantHeadTeamAverageSkill", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getPlantHeadTeam(@RequestParam("competency_area") String competency_area) {
        return skillService.getPlantHeadTeamAverageSkill(competency_area);
    }

}
