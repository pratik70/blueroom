package com.blueroom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.entities.DevelopmentMethod;
import com.blueroom.services.DevelopmentMethodsService;
import com.blueroom.vm.DevelopmentMethodVM;
import com.blueroom.vm.MessageVM;

@RestController
@RequestMapping("/blueroom/api/")
public class DevelopmentMethodsController {
	
	@Autowired
	DevelopmentMethodsService developmentMethodsService;
	
	@RequestMapping(value = "/developmentMethod/{id}", method = RequestMethod.GET)
	public MessageVM getDevelopmentByIdAndStatus(@PathVariable Long id) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentMethodsService.getDevelopmentByIdAndStatus(id));
		return vm;
	}
	
	@RequestMapping(value = "/developmentMethod", method = RequestMethod.GET)
	public MessageVM getAllDevelopment() {
		MessageVM vm = new MessageVM();
		vm.setData(developmentMethodsService.getAllDevelopmentMethods());
		return vm;
	}

	@RequestMapping(value = "/add-developmentMethod", method = RequestMethod.POST)
	public MessageVM addDevelopment(@RequestBody DevelopmentMethodVM developmentMethodVM) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentMethodsService.addDevelopment(developmentMethodVM));
		return vm;
	}
	
	@RequestMapping(value = "/developmentMethod/{id}", method = RequestMethod.DELETE)
	public MessageVM deleteDevelopment(@PathVariable Long id) {
		developmentMethodsService.deleteDevelopment(id);
		MessageVM vm = new MessageVM();
		vm.setData("Deleted");
		return vm;
	}
	
	@RequestMapping(value = "/developmentMethod/update", method = RequestMethod.PUT)
	public MessageVM updateDevelopment(@RequestBody DevelopmentMethodVM developmentMethodVM) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentMethodsService.updateDevelopment(developmentMethodVM));
		return vm;
	}
}
