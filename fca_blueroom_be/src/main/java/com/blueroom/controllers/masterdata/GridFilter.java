/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.controllers.masterdata;

import com.blueroom.entities.ETU;
import com.blueroom.entities.masterdata.DailyBasisLossType;
import java.util.Date;
import java.util.List;

/**
 *
 * @author govind
 */
public class GridFilter {

    private Date startDate;
    private Date endDate;
    private ETU etu;
    private List<DailyBasisLossType> dailyBasisLossTypes;

    public ETU getEtu() {
        return etu;
    }

    public void setEtu(ETU etu) {
        this.etu = etu;
    }

    public List<DailyBasisLossType> getDailyBasisLossTypes() {
        return dailyBasisLossTypes;
    }

    public void setDailyBasisLossTypes(List<DailyBasisLossType> dailyBasisLossTypes) {
        this.dailyBasisLossTypes = dailyBasisLossTypes;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
