/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.controllers.masterdata;

import com.blueroom.entities.masterdata.MachineData;
import com.blueroom.services.masterdata.MachineDataService;
import com.blueroom.vm.MessageVM;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author govind
 */
@RestController
@RequestMapping("/blueroom/api/")
public class MachineDataController {

    @Autowired
    MachineDataService machineDataService;

    @RequestMapping(value = "{etuId}/getMachineDataByEtu", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getMachineDataByEtu(@PathVariable("etuId") Long etuId) {
        return machineDataService.getMachineDataByEtu(etuId);
    }

    @RequestMapping(value = "{machineDataId}/getMasterDataById", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getMasterDataById(@PathVariable("machineDataId") Long machineDataId) {
        return machineDataService.findById(machineDataId);
    }

    @RequestMapping(value = "/saveMachineData", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveMasterData(@RequestBody List<MachineData> machineData) {
        return machineDataService.saveMachineData(machineData);
    }

}
