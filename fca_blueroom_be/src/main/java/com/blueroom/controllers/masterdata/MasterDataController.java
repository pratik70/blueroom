/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.controllers.masterdata;

import com.blueroom.entities.masterdata.MasterData;
import com.blueroom.services.masterdata.MasterDataService;
import com.blueroom.vm.MessageVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author govind
 */
@RestController
@RequestMapping("/blueroom/api/")
public class MasterDataController {

    @Autowired
    MasterDataService masterDataService;

    @RequestMapping(value = "{etuId}/getMasterDataByEtu", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getMasterDataByEtu(@PathVariable("etuId") Long etuId) {
        return masterDataService.getMasterDataByETU(etuId);
    }

    @RequestMapping(value = "{masterDataId}/getMasterDataById", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getMasterDataById(@PathVariable("masterDataId") Long masterDataId) {
        return masterDataService.findById(masterDataId);
    }

    @RequestMapping(value = "/saveMasterData", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveMasterData(@RequestBody MasterData masterData) {
        return masterDataService.saveMasterData(masterData);
    }

}
