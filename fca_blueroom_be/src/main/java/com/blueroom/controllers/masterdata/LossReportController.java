/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.controllers.masterdata;

import com.blueroom.services.masterdata.LossReportService;
import com.blueroom.vm.MessageVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author govind
 */
@RestController
@RequestMapping("/blueroom/api/")
public class LossReportController {

    @Autowired
    LossReportService lossReportService;

    @RequestMapping(value = "{etuId}/getLossReport", method = RequestMethod.GET)
    public MessageVM getLossReport(@PathVariable("etuId") Long etuId) {
        return lossReportService.getLossReport(etuId);
    }
}
