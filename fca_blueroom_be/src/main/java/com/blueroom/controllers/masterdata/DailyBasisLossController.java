/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.controllers.masterdata;

import com.blueroom.entities.masterdata.DailyBasisLoss;
import com.blueroom.repository.masterdata.DailyBasisLossTypeRepository;
import com.blueroom.services.masterdata.DailyBasisLossService;
import com.blueroom.vm.MessageVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author govind
 */
@RestController
@RequestMapping("/blueroom/api/")
public class DailyBasisLossController {

    @Autowired
    DailyBasisLossTypeRepository dailyBasisLossTypeRepository;

    @Autowired
    DailyBasisLossService dailyBasisLossService;

    @RequestMapping(value = "/getDailyBasisLossTypes", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getDailyBasisLossTypesData() {
        MessageVM result = new MessageVM();
        result.setData(dailyBasisLossTypeRepository.findAll(new Sort(Sort.Direction.ASC, "id")));
        return result;
    }

    @RequestMapping(value = "{etuId}/getDailyBasisLossByEtu", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getDailyBasisLossData(@PathVariable("etuId") Long etuId) {
        MessageVM result = new MessageVM();
        result.setData(dailyBasisLossService.findByEtuId(etuId));
        return result;
    }

    @RequestMapping(value = "{dailyBasisLossId}/getDailyBasisLoss", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getDailyBasisLossById(@PathVariable("dailyBasisLossId") Long dailyBasisLossId) {
        MessageVM result = new MessageVM();
        result.setData(dailyBasisLossService.findById(dailyBasisLossId));
        return result;
    }

    @RequestMapping(value = "/saveDailyBasisLoss", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveDailyBasisLoss(@RequestBody DailyBasisLoss dailyBasisLoss) {
        MessageVM result = new MessageVM();
        result.setData(dailyBasisLossService.save(dailyBasisLoss));
        return result;
    }

    @RequestMapping(value = "/getDailyBasisLossFilteredList", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveDailyBasisLoss(@RequestBody GridFilter gridFilter) {
        MessageVM result = new MessageVM();
        result.setData(dailyBasisLossService.getAllListByFilter(gridFilter));
        return result;
    }

}
