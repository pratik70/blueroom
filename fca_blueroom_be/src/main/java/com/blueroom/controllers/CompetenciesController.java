package com.blueroom.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.Competencies;
import com.blueroom.services.CompetenciesService;
import com.blueroom.utility.BehavioralUserDesiredLevelExcelUtil;
//import com.blueroom.utility.ExportExcelToDatabase;
import com.blueroom.vm.MessageVM;
//import com.blueroom.utility.ExportExcelToDatabase;

@RestController
@RequestMapping(value = "/blueroom/api/")
public class CompetenciesController {

	@Autowired
	CompetenciesService competenciesService;

	@Autowired
	BehavioralUserDesiredLevelExcelUtil behavioralUserDesiredLevelExcelUtil;
	
	@RequestMapping(value = "/competencies", method = RequestMethod.POST)
	public MessageVM addCompetencies(@RequestBody Competencies competencies) {
		MessageVM vm = new MessageVM();
		vm.setData(competenciesService.addCompetencies(competencies));
		return vm;
	}

	@RequestMapping(value = "/competencies", method = RequestMethod.GET)
	public MessageVM getAllcompetencies() {
		System.out.println("get method");
		MessageVM vm = new MessageVM();
		vm.setData(competenciesService.getCompetencies());
		return vm;
	}

	@RequestMapping(value = "/competencies/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageVM deletePillar(@PathVariable("id") long id) {
		MessageVM vm = new MessageVM();
		vm.setData(competenciesService.deleteCompetencieUser(id));
		return vm;
	}

	@RequestMapping(value = "/competencies/{id}", method = RequestMethod.GET)
	@ResponseBody
	public MessageVM getCompetencie(@PathVariable("id") long id) {
		MessageVM vm = new MessageVM();
		vm.setData(competenciesService.getCompetencie(id));
		return vm;
	}
	@RequestMapping(value = "/competencies/area/{area}", method = RequestMethod.GET)
	@ResponseBody
	public MessageVM getCompetencie(@PathVariable("area") String area) {
		MessageVM vm = new MessageVM();
		System.out.println(area);
		vm.setData(competenciesService.getCompetencieArea(area));
		return vm;
	}
	
	@RequestMapping(value = "/excel/db", method = RequestMethod.POST)
	@ResponseBody
	public void exportToDB(@RequestParam("file") MultipartFile file) {
		try {
			behavioralUserDesiredLevelExcelUtil.parse(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
