package com.blueroom.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.entities.Pillar;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.services.PillarService;
import com.blueroom.vm.MessageVM;
import com.blueroom.vm.PillarVM;

@RestController
@RequestMapping(value = "/blueroom/api/")
public class PillarController{
	
	@Autowired
	PillarService pillarservice;
	
	@RequestMapping(value = "/pillar/{id}", method = RequestMethod.GET)
	public MessageVM getPillarById(@PathVariable Long id) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.getPillarById(id));
		return vm;
	}
	
	@RequestMapping(value = "/pillar", method = RequestMethod.GET)
	public MessageVM getAllPillar() {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.getAllPillars());
		return vm;
	}
	
	@RequestMapping(value = "/pillar", method = RequestMethod.POST)
	public MessageVM addPillar(@RequestBody PillarVM pillarVM ) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.addPillar(pillarVM));
		return vm;
	}
	@RequestMapping(value = "/pillar/update", method = RequestMethod.PUT)
	public MessageVM updatePillar(@RequestBody PillarVM pillarVM) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.updatePillar(pillarVM));
		return vm;
	}
	
	@RequestMapping(value = "/pillar/{id}", method = RequestMethod.DELETE)
	public MessageVM deletePillar(@PathVariable Long id) {
		pillarservice.deletePillar(id);
		MessageVM vm = new MessageVM();
		vm.setData("Deleted");
		return vm;
	}
	
	
	  @RequestMapping(value = "/pillar/{id}/team", method = RequestMethod.GET)
	  public MessageVM pillarTeam(@PathVariable Long id) { 
		  MessageVM vm = new MessageVM(); 
		  vm.setData(pillarservice.pillarTeam(id));
		  return vm;
	  }
	 
	
	@RequestMapping(value = "/pillar/{id}/manager", method = RequestMethod.GET)
	public MessageVM pillarManager(@PathVariable Long id) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.pillarManager(id));
		return vm;
	}
	
	@RequestMapping(value = "/pillarName ", method = RequestMethod.GET)
	public MessageVM getPillarByName(@RequestParam String name) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.getPillarByName(name));
		return vm;
	}
	

	
	@RequestMapping(value = "/pillar/{id}/addMember", method = RequestMethod.POST)
	public MessageVM pillarTeamAddMember(@PathVariable("id") Long id, @RequestBody Pillar pillarId) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.setPillarUser(id, pillarId));
		return vm;
	}
	
	/*@RequestMapping(value = "/pillar/{id}/addMembers", method = RequestMethod.POST)
	public MessageVM pillarTeamAddMembers(@PathVariable("id") Long id, @RequestBody List<AuthUser> members) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.setPillarUsers(id, members));
		return vm;
	} */
	
	@RequestMapping(value = "/pillar/{id}/addMembers", method = RequestMethod.POST)
	public MessageVM pillarTeamAddMembers(@PathVariable("id") Long id, @RequestBody List<Pillar> pillars) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.setPillarUsers(id, pillars));
		return vm;
	}
	
	@RequestMapping(value = "/pillar/{pillarId}/user/{userId}", method = RequestMethod.DELETE)
	public MessageVM pillarTeamDeleteUser(@PathVariable("pillarId") Long pillarId, @PathVariable("userId") Long userId) {
		pillarservice.pillarTeamDeleteUser(pillarId,userId);
		MessageVM vm = new MessageVM();
		vm.setData("deleted");
		return vm;
	}

	@RequestMapping(value = "/pillarByUserId/{userId}", method = RequestMethod.GET)
	public MessageVM getPillarByUserId(@PathVariable("userId") Long userId) {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.getPillarByUserId(userId));
		return vm;
	}
	
	@RequestMapping(value = "/pillar/{id}/user", method = RequestMethod.GET)
	 public MessageVM getPillarByUser(@PathVariable Long id) { 
		MessageVM vm = new MessageVM(); 
		vm.setData(pillarservice.getPillarByUser(id));
		return vm;
	}
	
	@RequestMapping(value = "/pillarLeaders", method = RequestMethod.GET)
	public MessageVM getAllPillarLeaders() {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.getAllPillarLeaders());
		return vm;
	}
	
	@RequestMapping(value = "/teamMembers", method = RequestMethod.GET)
	public MessageVM getAllTeamMembers() {
		MessageVM vm = new MessageVM();
		vm.setData(pillarservice.getAllTeamMembers());
		return vm;
	}
	 
}
