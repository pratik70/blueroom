package com.blueroom.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.blueroom.entities.UserTechnicalToolSkillSet;
import com.blueroom.entities.auth.AuthUser;
import com.blueroom.entities.auth.Role;
import com.blueroom.services.UserGetservice;
import com.blueroom.utility.UserEntityExcelUtil;
import com.blueroom.vm.MessageVM;
import com.blueroom.vm.UploadFileResponse;
import com.blueroom.vm.UserFormVm;
import com.blueroom.vm.UserSkillTechnicalToolVM;

import java.util.Objects;

@Controller

@RequestMapping(value = "/blueroom/api/user")
public class Usercontroller {

    @Value("${userFolderPath}")
    String userFolderPath;

    @Autowired
    private JavaMailSender sender;

    @Autowired
    UserGetservice userService;

    @Autowired
    UserEntityExcelUtil userEntityExcelUtil;
    @ResponseBody
    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public List<AuthUser> getAllUsers() {
        return userService.getall();
    }
    
    @ResponseBody
    @RequestMapping(value = "/getallUser", method = RequestMethod.GET)
    public List<AuthUser> getAllPDUsers() {
        return userService.getAllPDUsers();
    }

    @ResponseBody
    @RequestMapping(value = "/deleteuser", method = RequestMethod.DELETE)
    public void deleteuser(@RequestBody UserFormVm userform) {
        userService.deleteusers(userform);
    }

    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity<MessageVM> updateusers(@RequestBody AuthUser userform) throws MessagingException {
        MessageVM vm = new MessageVM();
        AuthUser previousUser = userService.findById(userform.getId());
        List<AuthUser> usersByEmpId = userService.findAllByEmployeeId(userform.getEmployeeId());
        List<AuthUser> usersByEmail = userService.findAllByEmail(userform.getEmail());

        if (usersByEmpId.size() > 1 || usersByEmail.size() > 1) {
            vm.setMessage(usersByEmpId.size() > 1 ? "Employee Id is already Present." : "Email Id is already Present.");
            vm.setCode("201");
            return new ResponseEntity<MessageVM>(vm, HttpStatus.OK);
        }

        if (usersByEmpId.size() > 1) {

            boolean flag = Objects.nonNull(previousUser.getEmployeeId()) && previousUser.getEmployeeId().equals(usersByEmpId.get(0).getEmployeeId());
            if (!flag) {
                vm.setCode("201");
                vm.setMessage("Employee Id is already Present.");
                return new ResponseEntity<MessageVM>(vm, HttpStatus.OK);
            }
        }

        if (usersByEmail.size() > 1) {
            boolean flag = Objects.nonNull(previousUser.getEmail()) && previousUser.getEmail().equals(usersByEmail.get(0).getEmail());
            if (!flag) {
                vm.setMessage("Email Id is already Present.");
                vm.setCode("201");
                return new ResponseEntity<MessageVM>(vm, HttpStatus.OK);
            }
        }

        userService.saveUser(userform);
        vm.setData(userform);
        if (!(previousUser.getPassword().equals(userform.getPassword()))) {
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setTo(userform.getEmail());
            helper.setText("Password = " + userform.getPassword());
            helper.setSubject("Bluroom: User Password");
            new Thread(() -> {
                sender.send(message);
            }).start();
        }
        vm.setMessage("User updated successfully.");
        vm.setCode("200");
        return new ResponseEntity<MessageVM>(vm, HttpStatus.OK);
    }
    
    @ResponseBody
    @RequestMapping(value = "/profile/update", method = RequestMethod.POST)
    public ResponseEntity<MessageVM> updateusersprofile(@RequestBody AuthUser userform) throws MessagingException {
    	  MessageVM vm = new MessageVM();
    	  AuthUser previousUser = userService.findById(userform.getId());
    	  userService.saveUser(userform);
          vm.setData(userform);
          if (!(previousUser.getPassword().equals(userform.getPassword()))) {
              MimeMessage message = sender.createMimeMessage();
              MimeMessageHelper helper = new MimeMessageHelper(message);
              helper.setTo(userform.getEmail());
              helper.setText("Password = " + userform.getPassword());
              helper.setSubject("Bluroom: User Password");
              new Thread(() -> {
                  sender.send(message);
              }).start();
          }
          vm.setMessage("User updated successfully.");
          vm.setCode("200");
          return new ResponseEntity<MessageVM>(vm, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<MessageVM> saveUser(@RequestBody AuthUser userform) throws MessagingException {
        MessageVM vm = new MessageVM();
        List<AuthUser> usersByEmpId = userService.findAllByEmployeeId(userform.getEmployeeId());
        List<AuthUser> usersByEmail = userService.findAllByEmail(userform.getEmail());

        if (usersByEmpId.isEmpty() && usersByEmail.isEmpty()) {
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setTo(userform.getEmail());
            helper.setText("Password = " + userform.getPassword());
            helper.setSubject("Bluroom: User Password");

            new Thread(() -> {
                sender.send(message);
            }).start();

            userform = userService.saveUser(userform);
            vm.setData(userform);
            vm.setCode("200");
            vm.setMessage("New User Saved Successfully.");
        } else {
            vm.setCode("201");
            vm.setMessage(usersByEmpId.size() > 0 ? "Employee Id is already Present." : "Email Id is already Present.");
        }
        return new ResponseEntity<MessageVM>(vm, HttpStatus.OK);
    }

    @RequestMapping(value = "/download-image/{filePath:.+}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletRequest request, HttpServletResponse response, @PathVariable("filePath") String fileName) {
        String path = userFolderPath + File.separator + fileName;
        File file = new File(path);
        return new FileSystemResource(file);
    }

    @ResponseBody
    @RequestMapping(value = "/getuser/{id}", method = RequestMethod.GET, headers = "accept=application/json")
    public AuthUser getuserById(@PathVariable("id") long uid) {
        return userService.findById(uid);
    }

    @ResponseBody
    @RequestMapping(value = "/getUserByEmployeeId/{id}", method = RequestMethod.GET, headers = "accept=application/json")
    public MessageVM getUserByEmployeeId(@PathVariable("id") long employeeId) throws MessagingException {
        MessageVM vm = new MessageVM();
        AuthUser user = userService.findByEmployeeId(employeeId);
        if (Objects.nonNull(user)) {
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setTo(user.getEmail());
            helper.setText("Password = " + user.getPassword());
            helper.setSubject("Bluroom: User Password");
            new Thread(() -> {
                sender.send(message);
            }).start();
            vm.setData("Email has been sent to " + user.getEmail());
            return vm;
        } else {
            vm.setData("You are not a valid user ");
            return vm;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/fileUploader", method = RequestMethod.POST)
    public UploadFileResponse upload(@RequestParam("file") MultipartFile file) {

        if (!file.isEmpty()) {
            String fileName = userService.storeFile(file, userFolderPath);

            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path(userFolderPath)
                    .path(fileName)
                    .toUriString();

            return new UploadFileResponse(fileName, fileDownloadUri,
                    file.getContentType(), file.getSize());

        } else {
            return null;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/getAllRoles", method = RequestMethod.GET)
    public List<Role> getAllRoles() {
        return userService.getAllRoles();
    }
    
    @ResponseBody
    @RequestMapping(value = "/getlearningStyle", method = RequestMethod.GET)
    public List<String> getLearningStyle() {
        return userService.getLearningStyle();
    }

    @ResponseBody
    @RequestMapping(value = "/getAllPDRoles", method = RequestMethod.GET)
    public List<Role> getAllPDRoles() {
        return userService.getAllPDRoles();
    }
    
    @ResponseBody
    @RequestMapping(value = "/addUserSkillTechnicalTools", method = RequestMethod.PUT)
    public Object addUserSkillTechnicalTools(@RequestBody UserSkillTechnicalToolVM userSkillTechnicalToolVM) {
    	return userService.addUserSkillTechnicalTools(userSkillTechnicalToolVM);
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/getUserSkillTechnicalTools/{id}", method = RequestMethod.GET, headers = "accept=application/json")
    public List<UserTechnicalToolSkillSet> getUserSkillTechnicalTools(@PathVariable("id") Long userId) {
    	return userService.getUserSkillTechnicalTools(userId);
    }
    
    @ResponseBody
    @RequestMapping(value = "/plantHeadTeam", method = RequestMethod.GET, headers = "accept=application/json")
    public List<AuthUser> getPlantHeadTeam() {
    	return userService.getPlantHeadTeam();
    }
    
    @ResponseBody
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Map<String, Object> uploadUserData(@RequestParam("file") MultipartFile file) throws IOException {
     
            return userEntityExcelUtil.parse(file);
    }
}
