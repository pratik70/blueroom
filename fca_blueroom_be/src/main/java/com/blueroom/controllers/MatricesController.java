package com.blueroom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.Project;
import com.blueroom.services.MatricesService;
import com.blueroom.vm.CMatrixFilterVM;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;

import java.text.ParseException;
import java.util.Objects;

@RestController
@RequestMapping("/blueroom/api/")
public class MatricesController {

    @Autowired
    MatricesService matricesService;

    @RequestMapping(value = "/getMatricesData", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllMatricesData() {
        return matricesService.getAllMatricesData();
    }

    @RequestMapping(value = "/getCMatricesParetoData", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getCMatricesParetoData(@RequestBody CMatrixFilterVM cMatrixFilterVM) {
        return matricesService.getCMatricesParetoData(cMatrixFilterVM);
    }

    @RequestMapping(value = "/getCMatricesData", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getCMatricesData(@RequestBody CMatrixFilterVM cMatrixFilterVM) {
        return matricesService.getCMatricesData(cMatrixFilterVM);
    }

    @RequestMapping(value = "/getDMatricesData", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getDMatricesData(@RequestBody MatrixFilterVM matrixFilterVM) {
        return matricesService.getDMatricesData(matrixFilterVM);
    }

    @RequestMapping(value = "/getEMatricesData", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getEMatricesData(@RequestBody MatrixFilterVM matrixFilterVM) {
        return matricesService.getEMatricesData(matrixFilterVM);
    }

    @RequestMapping(value = "/setProjectStatus", method = RequestMethod.POST)
    @ResponseBody
    public Project setProjectStatus(@RequestBody Project project) {
        return matricesService.getProjectStatus(project);
    }

    @RequestMapping(value = "/getFMatricesData", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getFMatricesData(@RequestBody MatrixFilterVM matrixFilterVM) {
        return matricesService.getFMatricesData(matrixFilterVM);
    }

    @RequestMapping(value = "/getDEFMatricesData", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getDEFMatricesData(@RequestBody MatrixFilterVM matrixFilterVM) {
        return matricesService.getDEFMatricesData(matrixFilterVM);
    }

    @RequestMapping(value = "/getPillarSavings/{factorYear}", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getPillarSavings(@PathVariable Integer factorYear) {
        return matricesService.getPillarSavings(factorYear);

    }

    @RequestMapping(value = "/getETUSavings/{factorYear}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getETUSavings(@PathVariable Integer factorYear) {
        return matricesService.getETUSavings(factorYear);
    }

    @ResponseBody
    @RequestMapping(value = "/getAllPendingPDCAProjectListByETU/{id}", method = RequestMethod.GET)
    public MessageVM getAllProjectListByETU(@PathVariable("id") String etu) {
        Long etuId = Objects.isNull(etu) || etu.equalsIgnoreCase("null") ? null : Long.parseLong(etu);
        return matricesService.getAllPendingPDCAProjectListByETU(etuId);
    }

    @RequestMapping(value = "/uploadMatrixFile", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getDEFMatricesData(@RequestParam("file") MultipartFile file) throws ParseException {
        return matricesService.uploadMatrices(file);
    }

    @RequestMapping(value = "/getProjectDetails/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectDetails(@PathVariable Long projectId) {
        return matricesService.getProjectDetails(projectId);
    }

}
