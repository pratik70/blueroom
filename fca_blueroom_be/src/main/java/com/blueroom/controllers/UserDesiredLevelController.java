package com.blueroom.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.blueroom.entities.UserDesiredLevel;
import com.blueroom.services.UserDesiredLevelService;
import com.blueroom.utility.PillarDesiredLevelExcelUtil;
import com.blueroom.utility.TechnicalDesiredLevelExcelUtil;
import com.blueroom.utility.UserDesiredLevelExcelUtil;
import com.blueroom.vm.MessageVM;

@RestController
@RequestMapping("/blueroom/api/")
public class UserDesiredLevelController {
	
	@Autowired
	UserDesiredLevelService userdesiredlevelservice;
	
	@Autowired
	UserDesiredLevelExcelUtil userDesiredLevelExcelUti;
	
	@Autowired
	PillarDesiredLevelExcelUtil pillarDesiredLevelExcelUti;
	
	@Autowired
	TechnicalDesiredLevelExcelUtil technicalDesiredLevelExcelUtil;
	
	@RequestMapping(value = "/userdesiredlevel/{id}", method = RequestMethod.GET)
	public MessageVM getUserDesiredLevelById(@PathVariable Long id) {
		MessageVM vm = new MessageVM();
		vm.setData(userdesiredlevelservice.getUserDesiredLevelById(id));
		return vm;
	}
	
	@RequestMapping(value = "/userdesiredlevel", method = RequestMethod.GET)
	public MessageVM getUserDesiredLevel() {
		MessageVM vm = new MessageVM();
		vm.setData(userdesiredlevelservice.getAllUserDesiredLevel());
		return vm;
	}
	
	@RequestMapping(value = "/userdesiredlevel-v2", method = RequestMethod.GET)
	public MessageVM getUserDesiredLevelV2() {
		MessageVM vm = new MessageVM();
		vm.setData(userdesiredlevelservice.getAllUserDesiredLevelV2());
		return vm;
	}
	
	@RequestMapping(value = "/userdesiredlevel", method = RequestMethod.POST)
	public MessageVM addUserDesiredLevel(@RequestBody UserDesiredLevel userdesiredlevel ) {
		MessageVM vm = new MessageVM();
		vm.setData(userdesiredlevelservice.addUserDesiredLevel(userdesiredlevel));
		return vm;
	}
	
	@RequestMapping(value = "/updateUserDesiredLevel", method = RequestMethod.POST)
	   
	   public MessageVM updateUserDesiredLevel(@RequestBody UserDesiredLevel userdesiredlevel ) {
		MessageVM vm = new MessageVM();
		vm.setData(userdesiredlevelservice.updateUserDesiredLevel(userdesiredlevel));
		return vm;      
	}
	
	@RequestMapping(value = "/updateUserCurrentLevel", method = RequestMethod.POST)
	   
	   public MessageVM updateUserCurrentLevel(@RequestBody UserDesiredLevel userdesiredlevel ) {
		MessageVM vm = new MessageVM();
		vm.setData(userdesiredlevelservice.updateUserCurrentLevel(userdesiredlevel));
		return vm;      
	}
	
	@RequestMapping(value = "/userdesiredlevel/upload", method = RequestMethod.POST)
	public MessageVM parse(@RequestParam("file") MultipartFile file) throws IOException
	{
		MessageVM vm = new MessageVM();
		vm.setData( userDesiredLevelExcelUti.parse(file));
		return vm;
	}
	
	@RequestMapping(value = "/pillardesiredlevel/upload", method = RequestMethod.POST)
	public MessageVM pillarParse(@RequestParam("file") MultipartFile file) throws IOException
	{
		MessageVM vm = new MessageVM();
		vm.setData( pillarDesiredLevelExcelUti.parse(file));
		return vm;
	}
	
	@RequestMapping(value = "/technicaldesiredlevel/upload", method = RequestMethod.POST)
	public MessageVM technicalParse(@RequestParam("file") MultipartFile file) throws IOException
	{
		MessageVM vm = new MessageVM();
		vm.setData( technicalDesiredLevelExcelUtil.parse(file));
		return vm;
	}
}
