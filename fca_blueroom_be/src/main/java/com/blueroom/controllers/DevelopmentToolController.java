package com.blueroom.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.entities.DevelopmentMethod;
import com.blueroom.entities.DevelopmentTool;
import com.blueroom.services.DevelopmentToolService;
import com.blueroom.vm.MessageVM;

@RestController
@RequestMapping("/blueroom/api/")
public class DevelopmentToolController {
	
	@Autowired
	DevelopmentToolService developmentToolService;
	
	
	@RequestMapping(value = "/developmentTool/{id}", method = RequestMethod.GET)
	public MessageVM getDevelopmentToolById(@PathVariable Long id) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentToolService.getDevelopmentToolById(id));
		return vm;
	}
	

	@RequestMapping(value = "/developmentTool", method = RequestMethod.GET)
	public MessageVM getAllDevelopmentTool() {
		MessageVM vm = new MessageVM();
		vm.setData(developmentToolService.getAllDevelopmentTool());
		return vm;
	}
	
	@RequestMapping(value = "/add-developmentTool", method = RequestMethod.POST)
	public MessageVM addDevelopmentTool(@RequestBody DevelopmentTool developmentTool) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentToolService.addDevelopmentTool(developmentTool));
		return vm;
	}
	

	@RequestMapping(value = "/developmentTool/{id}", method = RequestMethod.DELETE)
	public MessageVM deleteDevelopmentTool(@PathVariable Long id) {
		developmentToolService.deleteDevelopmentTool(id);
		MessageVM vm = new MessageVM();
		vm.setData("Deleted");
		return vm;
	}
	
	@RequestMapping(value = "/developmentTool/update", method = RequestMethod.PUT)
	public MessageVM updateDevelopmentTool(@RequestBody DevelopmentTool developmentTool) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentToolService.updateDevelopmentTool(developmentTool));
		return vm;
	}
	
	@RequestMapping(value = "/developmentTool/plan", method = RequestMethod.POST)
	public MessageVM getDevelopmentPlan(@RequestBody HashMap<String, Long> data) {
		System.out.println(data);
		MessageVM vm = new MessageVM();
		vm.setData(developmentToolService.getDevelopmentPlan(data));
		return vm;
	}
	
}
