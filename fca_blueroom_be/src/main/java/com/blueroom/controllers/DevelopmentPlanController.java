package com.blueroom.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.services.DevelopmentPlanService;
import com.blueroom.vm.DevelopmentPlanVM;
import com.blueroom.vm.MessageVM;

@RestController
@RequestMapping("/blueroom/api/")
public class DevelopmentPlanController {
	
	@Autowired
	DevelopmentPlanService developmentPlanService;
	
	
	@RequestMapping(value = "/development/plan/tool", method = RequestMethod.PUT)
	public MessageVM updateDevelopmentPlan(@RequestBody DevelopmentPlanVM developmentPlanVM) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentPlanService.updateDevelopmentPlan(developmentPlanVM));
		return vm;
	}
	
	@RequestMapping(value = "/developmentplan/{userId}/{year}/{pillarId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
	public ResponseEntity exportDevelopmentPlan(HttpServletRequest request, HttpServletResponse response,@PathVariable Long userId, @PathVariable Long year, @PathVariable Long pillarId) throws IOException {
		
		ByteArrayInputStream in = developmentPlanService.exportDevelopmentPlan(userId, year, pillarId);
		// return IOUtils.toByteArray(in);
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=customers.xlsx");
		
		 return ResponseEntity
	                .ok()
	                .headers(headers)
	                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
	                .body(new InputStreamResource(in));
	}

	@RequestMapping(value = "/teamMemberDevelopmentPlan/{userId}/{year}/{pillarId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
	public ResponseEntity exportTeamMemberDevelopmentPlan(HttpServletRequest request, HttpServletResponse response,@PathVariable Long userId, @PathVariable Long year, @PathVariable Long pillarId) throws IOException {
		
		ByteArrayInputStream in = developmentPlanService.exportTeamMemberDevelopmentPlan(userId, year, pillarId);
		// return IOUtils.toByteArray(in);
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment;");
		
		 return ResponseEntity
	                .ok()
	                .headers(headers)
	                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
	                .body(new InputStreamResource(in));
	}

	@RequestMapping(value = "/createDevelopmentPlan", method = RequestMethod.PUT)
	public void createDevelopmentPlan(@RequestBody Map<String, Object> data) throws IOException {
		developmentPlanService.createDevelopmentPlan(data);
	}
	
	@RequestMapping(value = "/submitDevelopmentPlan", method = RequestMethod.PUT)
	public void submitDevelopmentPlan(@RequestBody Map<String, Object> data) throws IOException {
		developmentPlanService.submitDevelopmentPlan(data);
	}
	
	@RequestMapping(value = "/approveDevelopmentPlan", method = RequestMethod.PUT)
	public void appoveDevelopmentPlan(@RequestBody Map<String, Object> data) throws IOException {
		developmentPlanService.approveDevelopmentPlan(data);
	}
	
	@RequestMapping(value="developmentPlan/{userId}/{competencyArea}/{year}/{pillarId}", method = RequestMethod.GET)
	public MessageVM getDevelopmentPlanByCompetencyAreaAndYear(@PathVariable Long userId, @PathVariable String competencyArea, @PathVariable Long year, @PathVariable Long pillarId) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentPlanService.getDevelopmentPlanByUserIdAndCompetencyAreaAndYear(userId, competencyArea, year, pillarId));
		return vm;	
	}
	
	@RequestMapping(value="teamMemberDevelopmentPlan/{userId}/{competencyArea}/{year}/{pillarId}", method = RequestMethod.GET)
	public MessageVM getTeamMemberDevelopmentPlanByCompetencyAreaAndYear(@PathVariable Long userId, @PathVariable String competencyArea, @PathVariable Long year, @PathVariable Long pillarId) {
		MessageVM vm = new MessageVM();
		vm.setData(developmentPlanService.getTeamMemberDevelopmentPlanByUserIdAndCompetencyAreaAndYear(userId, competencyArea, year, pillarId));
		return vm;	
	}
	
	@RequestMapping(value = "/GapClosure/{id}/{year}", method = RequestMethod.GET)
	public MessageVM getTeamMemberGapClosure(@PathVariable("id") Long userId, @PathVariable("year") Long year) throws IOException {
		MessageVM vm = new MessageVM();
		vm.setData(developmentPlanService.getTeamMemberGapClosure(userId, year));
		return vm;
	}
	
	@RequestMapping(value = "/PillarTeamGapClosure/{id}/{year}", method = RequestMethod.GET)
	public MessageVM getPillarTeamGapClosure(@PathVariable("id") Long pillarId, @PathVariable("year") Long year) throws IOException {
		MessageVM vm = new MessageVM();
		vm.setData(developmentPlanService.getPillarTeamGapClosure(pillarId, year));
		return vm;
	}
	
	@RequestMapping(value = "/PlantHeadTeamGapClosure/{year}", method = RequestMethod.GET)
	public MessageVM getPlantHeadTeamGapClosure(@PathVariable("year") Long year) throws IOException {
		MessageVM vm = new MessageVM();
		vm.setData(developmentPlanService.getPlantHeadTeamGapClosure(year));
		return vm;
	}
}
