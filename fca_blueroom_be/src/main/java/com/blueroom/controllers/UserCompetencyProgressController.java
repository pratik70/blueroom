package com.blueroom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.services.UserCompetencyProgressService;
import com.blueroom.vm.MessageVM;

@RestController
@RequestMapping(value = "/blueroom/api/")
public class UserCompetencyProgressController {
	
	@Autowired
	UserCompetencyProgressService userCompetencyProgressService;
	
	@RequestMapping(value = "/UserCompetencyProgress/{userId}/{competencyId}", method = RequestMethod.GET)
	@ResponseBody
	public MessageVM getUserCompetencyProgress(@PathVariable("userId") long userId, @PathVariable("competencyId") long competencyId) {
		MessageVM vm = new MessageVM();
		vm.setData(userCompetencyProgressService.getUserCompetencyProgress(userId,competencyId));
		return vm;
	}

}
