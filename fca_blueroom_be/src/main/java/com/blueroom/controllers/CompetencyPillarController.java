package com.blueroom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.entities.CompetencyPillar;
import com.blueroom.services.CompetencyPillarService;
import com.blueroom.vm.CompetencyPillarVM;
import com.blueroom.vm.MessageVM;

@RestController
@RequestMapping(value = "/blueroom/api/")
public class CompetencyPillarController {
	
	@Autowired
	CompetencyPillarService competencypillarservice;
	
	
	@RequestMapping(value = "/competencypillar", method = RequestMethod.GET)
	public MessageVM getUserDesiredLevel() {
		MessageVM vm = new MessageVM();
		vm.setData(competencypillarservice.getAllCompetencyPillar());
		return vm;
	}
	@RequestMapping(value = "/competencypillar/{id}", method = RequestMethod.GET)
	public MessageVM getCompetencyPillar(@PathVariable Long id) {
		MessageVM vm = new MessageVM();
		vm.setData(competencypillarservice.getCompetencyPillar(id));
		return vm;
	}
	
	@RequestMapping(value = "/competencypillar", method = RequestMethod.POST)
	public MessageVM addCompetencyPillar(@RequestBody CompetencyPillar competencypillar ) {
		MessageVM vm = new MessageVM();
		vm.setData(competencypillarservice.addCompetencyPillar(competencypillar));
		return vm;
	}
	
	@RequestMapping(value = "/competencypillar/{id}", method = RequestMethod.DELETE)
	public MessageVM deleteCompetencyPillar(@PathVariable Long id) {
		competencypillarservice.deleteCompetencyPillar(id);
		MessageVM vm = new MessageVM();
		vm.setData("Deleted");
		return vm;
	}
	
	@RequestMapping(value = "/competencypillar/update", method = RequestMethod.PUT)
	public MessageVM updatePillar(@RequestBody CompetencyPillar competencypillar) {
		MessageVM vm = new MessageVM();
		vm.setData(competencypillarservice.updateCompetencyPillar(competencypillar));
		return vm;
	}
	
	@RequestMapping(value = "/competencypillarva", method = RequestMethod.POST)
	public MessageVM addCompetencyPillarva(@ModelAttribute CompetencyPillarVM competencypillarVM ) {
		MessageVM vm = new MessageVM();
		vm.setData(competencypillarservice.addCompetencyPillarv2(competencypillarVM));
		return vm;
	}

}
