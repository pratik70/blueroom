package com.blueroom.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blueroom.entities.Project;
import com.blueroom.services.ProjectService;
import com.blueroom.vm.MatrixFilterVM;
import com.blueroom.vm.MessageVM;
import com.blueroom.vm.ProjectBenefitVM;
import com.blueroom.vm.ProjectFMatrixSavingsVM;
import com.blueroom.vm.ProjectMembersVM;
import com.blueroom.vm.ProjectPlanRemarkVM;
import com.blueroom.vm.ProjectPlanningVM;
import com.blueroom.vm.ProjectSupportingPillarVM;
import com.blueroom.vm.ProjectVM;
import com.blueroom.vm.ProjectWCMToolVM;
import com.blueroom.vm.TrainingMailVM;
import com.blueroom.vm.UpgradeSkillVM;

@RestController
@RequestMapping("/blueroom/api/project")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @RequestMapping(value = "/code/{projectCode}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProject(@PathVariable String projectCode) {
        return projectService.getProject(projectCode);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProject(@PathVariable Long id) {
        return projectService.getProject(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public MessageVM updatedProject(@PathVariable Long id, @RequestBody ProjectVM projectVM) {
        return projectService.updatedProject(id, projectVM);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public MessageVM deleteProject(@PathVariable("id") Long id) {
        return projectService.deleteProject(id);
    }

    @RequestMapping(value = "/changeETUStatus/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Project changeETUApprovalStatusById(@PathVariable Long id) {
        return projectService.changeETUApproval(id);
    }

    @RequestMapping(value = "/changeFinanceStatus/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Project changeFinanceApprovalStatusById(@PathVariable Long id) {
        return projectService.changeFinanceApproval(id);

    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM createProject(@RequestBody ProjectVM projectVM) {
        return projectService.createProject(projectVM);
    }

    @RequestMapping(value = "/addPillars", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM addSupportingPillars(@RequestBody ProjectSupportingPillarVM projectSupportingPillarVM) {
        return projectService.addSupportingPillars(projectSupportingPillarVM);
    }

    @RequestMapping(value = "/{projectId}/getPillars", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectPillars(@PathVariable Long projectId) {
        return projectService.getProjectPillars(projectId);
    }

    @RequestMapping(value = "/getMethodsToolsList/{projectId}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getAllSelectedPillarMethodsAndTools(@PathVariable Long projectId) {
        return projectService.getAllSelectedPillarMethodsAndTools(projectId);
    }

    @RequestMapping(value = "/{projectId}/saveMethodsAndTools", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveMethodsAndTools(@PathVariable Long projectId, @RequestBody ProjectWCMToolVM projectWCMToolVM) {
        return projectService.saveMethodsAndTools(projectId, projectWCMToolVM);
    }

    @RequestMapping(value = "/{projectId}/getMethodsAndTools", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectMethodsAndTools(@PathVariable Long projectId) {
        return projectService.getProjectMethodsAndTools(projectId);
    }

    @RequestMapping(value = "/{projectId}/qualifiedusers/{usersmode}", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectQualifiedUsers(@PathVariable Long projectId, @PathVariable("usersmode") Boolean usersmode) {
        return projectService.getProjectSkillQualifiedUsers(projectId, usersmode);
    }

    @RequestMapping(value = "/{projectId}/addMembers", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM addProjectMembers(@PathVariable Long projectId, @RequestBody ProjectMembersVM projectMembersVM) {
        return projectService.addProjectMembers(projectId, projectMembersVM);
    }

    @RequestMapping(value = "/{projectId}/getProjectTeamMembers", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectTeamMembers(@PathVariable Long projectId) {
        return projectService.getProjectTeamMembers(projectId);
    }

    @RequestMapping(value = "/{projectId}/manageProjectCosting", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM manageProjectCosting(@PathVariable Long projectId, @RequestBody ProjectBenefitVM projectBenefitVM) {
        return projectService.manageProjectCosting(projectId, projectBenefitVM);
    }

    @RequestMapping(value = "/{projectId}/getProjectCostingData", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectCostingData(@PathVariable Long projectId) {
        return projectService.getProjectCostingData(projectId);
    }

    @RequestMapping(value = "/{projectId}/savePlanningDates", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM savePlanningDates(@PathVariable Long projectId, @RequestBody ProjectPlanningVM projectPlanningVM) {
        return projectService.savePlanningDates(projectId, projectPlanningVM);
    }

    @RequestMapping(value = "/{projectId}/saveExecutionDates", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveExecutionDates(@PathVariable Long projectId, @RequestBody ProjectPlanningVM projectPlanningVM) {
        return projectService.saveProjectExecutionDates(projectId, projectPlanningVM);
    }

    @RequestMapping(value = "/{projectId}/getProjectPlanningDates", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectPlanningDates(@PathVariable Long projectId) {
        return projectService.getProjectPlanningDates(projectId);
    }

    @RequestMapping(value = "/{projectId}/saveProjectActualSavings", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveProjectActualSavings(@PathVariable Long projectId, @RequestBody List<ProjectFMatrixSavingsVM> projectFMatrixSavingsVMList) {
        return projectService.saveProjectActualSavings(projectId, projectFMatrixSavingsVMList);
    }

    @RequestMapping(value = "/{projectId}/getProjectActualSavings", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectActualSavings(@PathVariable Long projectId) {
        return projectService.getProjectActualSavings(projectId);
    }

    @RequestMapping(value = "/{projectId}/saveProjectComplete", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM saveProjectComplete(@PathVariable Long projectId, @RequestBody ProjectVM projectVM) {
        return projectService.saveProjectComplete(projectId, projectVM);
    }

    @RequestMapping(value = "/{projectId}/startExecutionPlan", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM startExecutionPlan(@PathVariable Long projectId) {
        return projectService.startExecutionPlan(projectId);
    }

    @RequestMapping(value = "/{projectId}/addProjectRemark", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM addProjectRemark(@PathVariable Long projectId, @RequestBody ProjectPlanRemarkVM projectPlanRemarkVM) {
        return projectService.addProjectRemark(projectId, projectPlanRemarkVM);
    }

    @RequestMapping(value = "/{projectId}/getProjectRemark", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getProjectRemark(@PathVariable Long projectId, @RequestBody ProjectPlanRemarkVM projectPlanRemarkVM) {
        return projectService.getProjectRemark(projectId, projectPlanRemarkVM);
    }

    @RequestMapping(value = "/{projectId}/getProjectTechnicalTool", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getProjectTechnicalTool(@PathVariable Long projectId) {
        return projectService.getProjectTechnicalTool(projectId);
    }

    @RequestMapping(value = "/{projectId}/getSkillUpdationData", method = RequestMethod.GET)
    @ResponseBody
    public MessageVM getSkillUpdationData(@PathVariable Long projectId) {
        return projectService.getSkillUpdationData(projectId);
    }

    @RequestMapping(value = "/report/stageWorkingCount", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM getSatgeCount(@RequestBody MatrixFilterVM filterVM) {
        return projectService.getSatgeCount(filterVM);
    }

    @RequestMapping(value = "/{projectId}/sendTrainingMail", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM sendTrainingMail(@PathVariable Long projectId, @RequestBody TrainingMailVM trainingMailVM) {
        return projectService.sendTrainingMail(projectId, trainingMailVM);
    }

    @RequestMapping(value = "/upgradeUserSkill", method = RequestMethod.POST)
    @ResponseBody
    public MessageVM upgradeUserSkill(@RequestBody List<UpgradeSkillVM> upgradeSkillList) {
        return projectService.upgradeUserSkill(upgradeSkillList);
    }

}
