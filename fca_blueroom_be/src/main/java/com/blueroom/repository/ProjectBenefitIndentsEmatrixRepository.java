package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectBenefitIndentsEmatrix;

@Repository
public interface ProjectBenefitIndentsEmatrixRepository extends JpaRepository<ProjectBenefitIndentsEmatrix, Long> {

    ProjectBenefitIndentsEmatrix findOneByProject(Project project);

    List<ProjectBenefitIndentsEmatrix> findAllByProjectId(Long id);

}
