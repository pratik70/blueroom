package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.MethodsAndTools;
import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectMethodsAndTools;

@Repository
public interface ProjectMethodsAndToolsRepository extends JpaRepository<ProjectMethodsAndTools, Long> {	
	List<ProjectMethodsAndTools> findByProjectAndMethodsTools(Project project, MethodsAndTools methodsAndTools);

	List<ProjectMethodsAndTools> findByProject(Project project);

	@Query(value ="SELECT DISTINCT methods_tools_id FROM tbl_project_methods_tools", nativeQuery = true)
	List<String> getUniqueMethodsAndToolList();

	List<ProjectMethodsAndTools> findAllByProjectId(Long id);
}
