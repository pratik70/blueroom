package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.blueroom.entities.Pillar;
import com.blueroom.entities.auth.AuthUser;

public interface AuthUserRepository extends JpaRepository<AuthUser, Long> {

    @Query(value = "SELECT * FROM tbl_user_master tum WHERE tum.id IN (:ids) AND tum.`status`='Active' ", nativeQuery = true)
    List<AuthUser> findAll(@Param("ids") List<String> ids);
    
    @Query(value = "SELECT * FROM tbl_user_master WHERE id IN (:ids)AND `status`='Active'", nativeQuery = true)
    List<AuthUser> findAllByPillarId(@Param("ids") List<Long> ids);

    List<AuthUser> findOneByName(String leaderName);
    

    @Query(value = "SELECT * FROM tbl_user_master WHERE etu_level IN (:etuIds) AND `status`='Active'", nativeQuery = true)
    List<AuthUser> findAllByEtuLevel(@Param("etuIds") List<Long> etuIds);
    
    @Query(value = "SELECT * FROM tbl_user_etu_map WHERE etu_id IN (:etuIds) ", nativeQuery = true)
    AuthUser findOneByEtuId(@Param("etuIds") Long etuid);
    
/*    @Query(value="select * from  tbl_user_master tbm where tbm.approval='Finance Approval' OR tbm.approval='ETU Approval'",nativeQuery=true)
    List<AuthUser> findAllByApproval();
*/
    
    @Query(value="select * from tbl_user_master u inner join tbl_user_etu_map ue on ue.user_id = u.id inner join tbl_projects p on p.etu_id = ue.etu_id  where p.id IN (:projectId) and (u.approval='Finance Approval' OR u.approval='ETU Approval')AND u.`status`='Active'",nativeQuery=true)
    List<AuthUser> findAllByApproval(@Param("projectId")Long projectId);

    @Query(value="select * from tbl_user_master u inner join tbl_user_etu_map ue on ue.user_id = u.id inner join tbl_projects p on p.etu_id = ue.etu_id  where p.id IN (:projectId) and (u.approval='Finance Approval')AND (p.finance_mail_send=false) AND u.`status`='Active'",nativeQuery=true)
    List<AuthUser> findAllByFinanceApproval(@Param("projectId")Long projectId);

    
    AuthUser findOneByUsername(String username);

    @Query(value = "SELECT * FROM tbl_user_master u WHERE u.username =:username AND u.`type` =:type", nativeQuery = true)
    AuthUser findOneByUsernameAndType(@Param("username") String username, @Param("type") String type);

    @Query(value = "SELECT u.profile_img FROM tbl_user_master u WHERE u.id = :userId ", nativeQuery = true)
    public String getProfileImage(@Param("userId") Long userId);

	AuthUser getUserByName(String name);

	AuthUser findOneByemployeeId(long employeeId);

	List<AuthUser> findAllByIdIn(List<Long> useridstoretrive);

	List<AuthUser> findAllByStatus(String string);

    @Query(value = "select * from tbl_user_master where id in (select user_id from tbl_user_pillar_map where pillar_id = :pillarId) ", nativeQuery = true)
    public List<AuthUser> getTeamByPillar(@Param("pillarId") Long pillarId);
    
    @Modifying
    @Query(value = "delete FROM tbl_user_pillar_map WHERE pillar_id = :pillarId AND user_id = :userId", nativeQuery = true)
    void deleteUserPiller(@Param("pillarId") Long pillarId, @Param("userId") Long userId);
    @Modifying
    @Query(value = "delete FROM tbl_user_pillar_map WHERE pillar_id = :pillarId AND user_id = :memberid", nativeQuery = true)
	void deleteUserPiller(Long id, AuthUser memberid);

    @Query(value="select user_id from tbl_user_pillar_map where pillar_id = :pillarId",nativeQuery =true)
	public long[] findByteamMember(@Param("pillarId")Long pillarId);
    
    @Modifying
    @Transactional
    @Query(value = "delete  from tbl_user_pillar_map where pillar_id = :pillarId", nativeQuery = true)
	void deletebyTeam(@Param("pillarId")Long pillarId);
   
    @Query(value="SELECT * FROM tbl_ma_pillar WHERE id IN (select pillar_id from tbl_user_pillar_map where user_id = :userId)", nativeQuery=true)		 
    List<Pillar> getPillarByuser(@Param("userId")Long userId);

    @Query(value="SELECT * FROM tbl_user_master t WHERE t.email_id = :emailId", nativeQuery=true)
    AuthUser findOneByEmailId(@Param("emailId") Long emailId);
	
}