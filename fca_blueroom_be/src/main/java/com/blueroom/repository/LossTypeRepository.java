package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.LossType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface LossTypeRepository extends JpaRepository<LossType, Long> {

    @Query(value = "SELECT * FROM tbl_loss_type_master tml WHERE tml.loss_name =:loss_name LIMIT 1", nativeQuery = true)
    LossType findByLossName(@Param("loss_name") String lossName);
}
