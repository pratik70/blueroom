package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Process;

@Repository
public interface ProcessRepository extends JpaRepository<Process, Long> {

}
