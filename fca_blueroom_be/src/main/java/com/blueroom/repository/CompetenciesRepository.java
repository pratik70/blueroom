
package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.Pillar;

public interface CompetenciesRepository extends JpaRepository<Competencies, Long> {

	List<Competencies> findAllByStatus(boolean b);

	@Query(value = "SELECT * FROM tbl_ma_competencies c WHERE c.area=:competencyArea ORDER BY c.type", nativeQuery= true)
	List<Competencies> findByArea(@Param("competencyArea") String competencyArea);
	
	List<Competencies> findAllByAreaAndStatus(String s, Boolean b);
	
	@Query(value = "SELECT *  from tbl_ma_competencies c WHERE c.id IN (select competency_id FROM competency_pillar_mapping cp WHERE cp.pillar_id = :pillarId) ORDER BY c.type", nativeQuery = true)
	List<Competencies> getCompetenciesByPillarId(@Param("pillarId") Long pillarId);

	@Query(value = "SELECT * FROM tbl_ma_competencies c WHERE c.name=:competencyName", nativeQuery= true)
	Competencies findByName(@Param("competencyName") String competencyName);

	@Query(value = "SELECT * FROM tbl_ma_competencies c WHERE c.name=:competencyName AND c.`type` =:competencyType", nativeQuery= true)
	Competencies findByNameAndCompetencyType(@Param("competencyName") String competencyName, @Param("competencyType") String competencyType);

	@Query(value = "SELECT DISTINCT c.* FROM user_desired_level u, tbl_ma_competencies c WHERE u.competencies_id = c.id AND c.area =:competencyArea AND u.user_id =:userId", nativeQuery= true)
	List<Competencies> findByAreaAndUser(@Param("competencyArea")String competencyArea, @Param("userId") Long userId);
}
