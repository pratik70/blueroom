package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectTechnicalTools;
import com.blueroom.entities.TechnicalTool;

@Repository
public interface ProjectTechnicalToolsRepository extends JpaRepository<ProjectTechnicalTools, Long> {

	List<ProjectTechnicalTools> findByProjectAndTechnicalTool(Project project, TechnicalTool technicalTool);

	List<ProjectTechnicalTools> findByProject(Project project);

	List<ProjectTechnicalTools> findAllByProjectId(Long id);
}
