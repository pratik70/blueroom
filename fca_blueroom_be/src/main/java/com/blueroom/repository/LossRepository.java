package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Loss;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface LossRepository extends JpaRepository<Loss, Long> {

    Loss findAllByLoss(String lossValue);
    
    @Modifying
    @Query(value = "UPDATE tbl_ma_loss SET priority=:priority ,severity=:severity WHERE id=:id", nativeQuery = true)
    public void updateLoss(@Param("priority") String priority, @Param("severity") String severity, @Param("id") Long id);

	List<Loss> findAllByOrderByLossType();
}
