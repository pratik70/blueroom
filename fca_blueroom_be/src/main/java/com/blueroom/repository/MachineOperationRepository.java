package com.blueroom.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.blueroom.entities.MachineOperation;

public interface MachineOperationRepository extends JpaRepository<MachineOperation, Long> {

	@Query(value="select * from tbl_ma_machine_operation operation where operation.etu_id = :etu_id",nativeQuery=true)
	List <MachineOperation> findOperationByETU(@Param("etu_id") Long etu_id);
}
