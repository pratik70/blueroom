package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectSupportingPillars;

@Repository
public interface ProjectSupportingPillarsRepository extends JpaRepository<ProjectSupportingPillars, Long> {

    List<ProjectSupportingPillars> findByProject(Project project);

    @Query(value = "SELECT DISTINCT pillar_id FROM tbl_project_supporting_pillars WHERE project_id = :projectId", nativeQuery = true)
    List<Long> getAllSelectedPillarByProjectId(@Param("projectId") Long projectId);

    @Query(value = "SELECT DISTINCT pillar_id FROM tbl_project_supporting_pillars", nativeQuery = true)
    List<Long> getUniquePillarList();

    ProjectSupportingPillars findOneByProjectAndPrimary(Project project, boolean b);

    List<ProjectSupportingPillars> findAllByProjectId(Long id);

    @Query(value = "SELECT DISTINCT user_id FROM tbl_user_pillar_map WHERE pillar_id IN(:pillarId)", nativeQuery = true)
    List<Long> getAllSelectedUserIdsByPillarId(@Param("pillarId") List<Long> pillarids);

    @Query(value = "select DISTINCT p.project_id FROM tbl_project_supporting_pillars p where p.pillar_id In(:id);", nativeQuery = true)
    List<Long> getProjectIdsFromPillarId(@Param("id") Long id);

}
