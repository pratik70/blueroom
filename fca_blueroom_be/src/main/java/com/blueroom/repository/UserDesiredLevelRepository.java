package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blueroom.entities.UserDesiredLevel;

@Repository
public interface UserDesiredLevelRepository extends JpaRepository<UserDesiredLevel, Long> {

	@Query(value="SELECT * FROM user_desired_level u WHERE u.user_id =:userId ", nativeQuery = true)
	List<UserDesiredLevel> getUserDesiredSkillByUserId(@Param("userId") Integer userId);

	@Query(value="SELECT * FROM user_desired_level u WHERE u.user_id = :userId and u.year = :year and status=true and u.competencies_id IN (SELECT c.id FROM  tbl_ma_competencies c WHERE c.area = :area)", nativeQuery = true)
	List<UserDesiredLevel> getUserDesiredSkillByUserIdAndYear(@Param("userId") Integer userId, @Param("year") Integer year, @Param("area") String area);

	@Query(value= "SELECT c.name, competencies_id, MAX(actual_skill) AS maxActualSkill FROM user_desired_level u ,tbl_user_pillar_map pm ,tbl_ma_competencies c WHERE u.user_id = pm.user_id AND pm.pillar_id =:pillarId AND u.competencies_id = c.id AND c.area =:competency_area GROUP BY u.competencies_id", nativeQuery = true)
	List<Object[]> getTeamMaxSkillByPillarIdAndCompetencyArea(@Param("pillarId") Long pillarId, @Param("competency_area") String competency_area);

	@Query(value= "SELECT c.name, competencies_id, sum(actual_skill) AS totalActualSkill, sum(required_skill) AS totalRequiredSkill, COUNT(actual_skill) AS noOfSkills FROM user_desired_level u ,tbl_user_pillar_map pm ,tbl_ma_competencies c WHERE u.user_id = pm.user_id AND pm.pillar_id =:pillarId AND u.competencies_id = c.id AND c.area =:competency_area GROUP BY u.competencies_id", nativeQuery = true)
	List<Object[]> getTeamAverageSkillByPillarIdAndCompetencyArea(@Param("pillarId") Long pillarId, @Param("competency_area") String competency_area);
	
	@Query(value="SELECT * FROM user_desired_level u WHERE u.user_id =:userId AND u.competencies_id =:competencyId", nativeQuery = true)
	List<UserDesiredLevel> getUserDesiredSkillByUserIdAndCompetencyId(@Param("userId") Long userId, @Param("competencyId") Long competencyId);
	
	@Query(value="SELECT * FROM user_desired_level u WHERE u.user_id =:userId AND u.competencies_id =:competencyId AND u.year =:year", nativeQuery = true)
	List<UserDesiredLevel> getUserDesiredSkillByUserIdAndCompetencyIdAndYear(@Param("userId") Long userId, @Param("competencyId") Long competencyId, @Param("year") Long year);

	
	@Query(value="SELECT * FROM user_desired_level u WHERE u.user_id =:userId AND u.competencies_id =:competencyId AND u.year =:year", nativeQuery = true)
	UserDesiredLevel findOneByUserIdCompetencyIDYear(@Param("userId") Long userId, @Param("competencyId") Long competencyId, @Param("year") Integer year);

	@Transactional
	@Modifying
	@Query(value = "UPDATE user_desired_level ud SET ud.plan_status = \"created\" WHERE ud.user_id =:userId AND ud.year=:year AND ud.competencies_id IN :competencyIds", nativeQuery = true)
	void createDevelopmentPlan(@Param("userId") Integer userId, @Param("competencyIds") List<Long> competencyIds, @Param("year") Integer year);

	@Transactional
	@Modifying
	@Query(value = "UPDATE user_desired_level ud SET ud.plan_status = \"submitted\" WHERE ud.user_id =:userId AND ud.year=:year AND ud.competencies_id IN :competencyIds", nativeQuery = true)
	void submitDevelopmentPlan(@Param("userId") Integer userId, @Param("competencyIds") List<Long> competencyIds, @Param("year") Integer year);

	@Transactional
	@Modifying
	@Query(value = "UPDATE user_desired_level ud SET ud.plan_status = \"approved\" WHERE ud.user_id =:userId AND ud.year=:year AND ud.competencies_id IN :competencyIds", nativeQuery = true)
	void approveDevelopmentPlan(@Param("userId") Integer userId, @Param("competencyIds") List<Long> competencyIds, @Param("year") Integer year);
	
	@Query(value="SELECT c.name, competencies_id, MAX(actual_skill) AS totalActualSkill FROM user_desired_level u, userrole ur, user_roles r, tbl_ma_competencies c WHERE u.user_id = ur.user_id AND ur.role_id = r.role_id AND r.name =\"PILLAR_LEADER\" AND u.competencies_id = c.id AND c.area =:competency_area GROUP BY u.competencies_id", nativeQuery = true )
	List<Object[]> getPlantHeadTeamMaxSkill(@Param("competency_area") String competency_area);

	@Query(value= "SELECT c.name, competencies_id, sum(actual_skill) AS totalActualSkill, sum(required_skill) AS totalRequiredSkill, COUNT(actual_skill) AS noOfSkills FROM user_desired_level u, userrole ur, user_roles r,tbl_ma_competencies c WHERE u.user_id = ur.user_id AND ur.role_id = r.role_id AND r.name =\"PILLAR_LEADER\" AND u.competencies_id = c.id AND c.area =:competency_area GROUP BY u.competencies_id", nativeQuery = true)
	List<Object[]> getPlantHeadTeamAverage(@Param("competency_area") String competency_area);

	@Query(value= "SELECT * FROM user_desired_level u WHERE u.user_id =:userId", nativeQuery = true)
	List<UserDesiredLevel> findAllByUserId(@Param("userId")Long userId);

	@Query(value= "SELECT SUM(u.actual_skill) , SUM(u.required_skill), c.`type` FROM user_desired_level u, tbl_ma_competencies c WHERE u.user_id =:userId AND u.year=:year AND u.competencies_id = c.id GROUP BY c.`type`", nativeQuery = true)
	List<Object[]> getGapClosureByUserId(@Param("userId") Long userId, @Param("year") Long year);
	
	@Query(value="SELECT SUM(u.actual_skill) , SUM(u.required_skill), c.`type` FROM user_desired_level u, tbl_ma_competencies c, tbl_user_master um, tbl_user_pillar_map p \r\n" + 
			"WHERE u.user_id = um.id AND um.id = p.user_id AND p.pillar_id =:pillarId AND u.competencies_id = c.id AND u.year=:year GROUP BY c.`type`", nativeQuery = true)
	List<Object[]> getGapClosureByPillarId(@Param("pillarId")Long pillarId, @Param("year") Long year);

	@Query(value="SELECT SUM(u.actual_skill) , SUM(u.required_skill), c.`type` FROM user_desired_level u, tbl_ma_competencies c, tbl_user_master um, userrole r, user_roles ur \r\n" + 
			"WHERE u.user_id = um.id AND um.id = r.user_id AND r.role_id = ur.role_id AND ur.name = \"PILLAR_LEADER\" AND u.competencies_id = c.id AND u.year=:year GROUP BY c.`type`", nativeQuery = true)
	List<Object[]> getPlantHeadTeamGapClosure(@Param("year") Long year);

	@Query(value="SELECT u.* FROM user_desired_level u, tbl_ma_competencies c WHERE u.user_id =:userId AND u.year =:year AND u.status=true AND u.competencies_id = c.id ORDER BY c.type", nativeQuery = true)
	List<UserDesiredLevel> getUserDesiredSkillByUserIdAndYear(@Param("userId") Integer userId, @Param("year") Integer year);

	@Query(value="SELECT u.actual_skill FROM user_desired_level u WHERE u.user_id =:userId AND u.year =:year AND  u.competencies_id=:competencyId AND u.status=true", nativeQuery = true)
	Object getUserDesiredSkillByUserIdAndYearAndComptencyId(@Param("userId") Integer userId, @Param("year") Integer year, @Param("competencyId") Long id);

}
