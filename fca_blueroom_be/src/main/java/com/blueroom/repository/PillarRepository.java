package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blueroom.entities.Pillar;
import com.blueroom.entities.auth.AuthUser;

@Repository
public interface PillarRepository extends JpaRepository<Pillar, Long> {

    @Query(value = "SELECT * FROM tbl_ma_pillar WHERE id IN (:pillarIds)", nativeQuery = true)
    List<Pillar> findAll(@Param("pillarIds") List<Long> pillarIds);

    Pillar findOneByCode(String code); //1

    @Query(value = "select * FROM tbl_ma_pillar t WHERE t.name=:pillarName", nativeQuery = true)
    Pillar findOneByPillarName(@Param("pillarName") String pillarName);

    @Query(value = "UPDATE tbl_user_pillar_map p SET p.pillar_id =:pillar_id WHERE p.user_id =:user_id", nativeQuery = true)
    void updatePillarIdByUserId(@Param("user_id") Long user_id, @Param("pillar_id") Long pillar_id);
    
    @Query(value = "select * FROM tbl_ma_pillar t WHERE t.status=:status", nativeQuery = true)
    List<Pillar> getAllByStatus(@Param("status") Boolean status);
    
    @Query(value = "select * FROM tbl_ma_pillar t WHERE t.name=:name and t.status=TRUE", nativeQuery = true)
    List<Pillar> getAllByName(@Param("name") String name);
    
    @Modifying
    @Query(value = "delete FROM pillar_team WHERE pillar_id=:pillarId and user_id=:userId", nativeQuery = true)
    void deletePillarTeamUser(@Param("pillarId") Long pillarId, @Param("userId") Long userId);

	 List<Pillar>  findAllByStatus(boolean b);
	 
	 /*
	 * @Modifying
	 * 
	 * @Query(value =
	 * "delete FROM pillar_team WHERE pillar_id=:pillarId and user_id=:userId",
	 * nativeQuery = true) void deletePillarTeamUser(@Param("pillarId") Long
	 * pillarId, @Param("userId") Long userId);
	 */
    
    @Query(value = "insert into tbl_user_pillar_map values(user_id=:user_id and pillar_id=:pillar_id)", nativeQuery = true)
    void adduser(@Param("user_id") Long user_id, @Param("pillar_id") Long pillar_id);

    @Query(value = "SELECT p.* FROM tbl_user_pillar_map u, tbl_ma_pillar p WHERE u.pillar_id= p.id  AND u.user_id =:user_id", nativeQuery = true)
    Pillar getPillarByUserId(@Param("user_id") Long userId);
    
    @Modifying
    @Transactional
    @Query(value = "delete FROM tbl_user_pillar_map WHERE pillar_id=:pillarId", nativeQuery = true) 
	void deletePillarTeam(@Param("pillarId")Long pillarId);

    @Query(value="SELECT * FROM tbl_ma_pillar p WHERE p.manager=:user_id", nativeQuery = true)
	Pillar getPillarByPillarLeader(@Param("user_id") Long userId);
    
}
