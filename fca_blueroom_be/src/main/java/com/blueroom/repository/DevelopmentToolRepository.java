package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.DevelopmentTool;

@Repository
public interface DevelopmentToolRepository extends JpaRepository<DevelopmentTool, Long> {
	
	@Query(value="SELECT * FROM tbl_development_tool d WHERE d.name =:name", nativeQuery = true)
	DevelopmentTool findByName(@Param("name") String name);
	
	@Query(value="SELECT * FROM tbl_development_tool dtool WHERE dtool.method_id = :methodId", nativeQuery = true)
	List<DevelopmentTool> findByDevelopmentMethodId(@Param("methodId") Long methodId);

	@Query(value="SELECT t.* FROM tbl_development_plan p, tbl_development_tool t WHERE p.user_id = :userId AND p.competency_id=:competencyId AND p.year=:year AND t.id = p.tool_id AND t.method_id = :methodId", nativeQuery = true)
	List<DevelopmentTool> getPlan(@Param("userId") Long userId, @Param("competencyId") Long competencyId, @Param("year") Long year, @Param("methodId") Long methodId);

	@Query(value="SELECT t.method_id FROM tbl_development_tool t WHERE t.id =:tool_id", nativeQuery = true)
	Object findByToolId(@Param("tool_id") Long toolId);
  
	@Query(value="SELECT * FROM tbl_development_tool d WHERE d.status =:status", nativeQuery = true)
	List<DevelopmentTool> findByStatus(@Param("status") boolean status);
	
	  
 
}
