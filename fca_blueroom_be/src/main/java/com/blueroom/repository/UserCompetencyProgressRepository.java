package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.UserCompetencyProgress;

@Repository
public interface UserCompetencyProgressRepository extends JpaRepository<UserCompetencyProgress, Long>{
	
	@Query(value="SELECT * FROM tbl_user_copmetency_progress u WHERE u.user_id = :userId and u.competency_id = :competencyId", nativeQuery = true)
	List<UserCompetencyProgress> getUserCompetencyProgressByUserIdAndComepetencyID(@Param("userId") Long userId, @Param("competencyId") Long competencyId);


}
