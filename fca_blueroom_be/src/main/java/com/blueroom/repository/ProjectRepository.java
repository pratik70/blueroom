package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.ETU;
import com.blueroom.entities.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query(value = "SELECT project.* FROM tbl_projects  project LEFT JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByOrderByCreatedAtDescForD();

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByOrderByCreatedAtDesc();

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE   project.completion_date IS NOT NULL ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllCompletedProjectByOrderByCreatedAtDesc();

//    @Query(value = "SELECT team.user_id as userid , COUNT(*) as totalWorkingProject, COUNT(CASE WHEN role_id= 1 THEN 1 END )  AS countTotalLeader, COUNT(CASE WHEN role_id= 2 THEN 1 END )  AS countTotalCoach, COUNT(CASE WHEN role_id= 3 THEN 1 END )  AS countTotalMember FROM tbl_projects p JOIN tbl_project_team_members team ON p.id  = team.project_id WHERE p.completion_date  IS NULL GROUP BY team.user_id ", nativeQuery = true)
//    List<Object[]> getAllUserWorkingProjectCount();
    
    @Query(value = "SELECT team.user_id as userid , COUNT(*) as totalWorkingProject, COUNT(CASE WHEN role_id= 1 THEN 1 END )  AS countTotalLeader, COUNT(CASE WHEN role_id= 2 THEN 1 END )  AS countTotalCoach, COUNT(CASE WHEN role_id= 3 THEN 1 END )  AS countTotalMember FROM tbl_projects p JOIN tbl_project_team_members team ON p.id  = team.project_id join tbl_project_benefit_indents_ematrix ematrix on p.id=ematrix.project_id WHERE p.completion_date  IS  NULL AND ematrix.a_actual_start_date is not null AND ematrix.completion_date is null GROUP BY team.user_id ", nativeQuery = true)
    List<Object[]> getAllUserWorkingProjectCount();

    //@Query(value = " SELECT * FROM tbl_projects WHERE id IN ( SELECT e_tbl.project_id  FROM tbl_project_benefit_indents_ematrix AS e_tbl WHERE STR_TO_DATE(e_tbl.start_date ,'%m-%d-%Y')  BETWEEN  STR_TO_DATE( :startDate, '%m-%d-%Y') AND  STR_TO_DATE(:endDate,'%m-%d-%Y') ORDER BY STR_TO_DATE(e_tbl.start_date ,'%m-%d-%Y'))", nativeQuery = true)
    @Query(value = " SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')  BETWEEN  STR_TO_DATE( :startDate, '%m-%d-%Y') AND  STR_TO_DATE( :endDate ,'%m-%d-%Y') ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findProjectUsingStartDate(@Param("startDate") String startDate, @Param("endDate") String endDate);

    List<Project> findByProjectCode(String projectCode);

    //@Query(value = "SELECT * FROM tbl_projects WHERE id IN ( SELECT e_tbl.project_id  FROM tbl_project_benefit_indents_ematrix AS e_tbl WHERE STR_TO_DATE(e_tbl.target_date ,'%m-%d-%Y') BETWEEN  STR_TO_DATE( :targetFromDate, '%m-%d-%Y')  AND  STR_TO_DATE(:targetUptoDateDate,'%m-%d-%Y') ORDER BY STR_TO_DATE(e_tbl.start_date ,'%m-%d-%Y'))", nativeQuery = true)
    @Query(value = "SELECT * FROM tbl_projects project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id JOIN tbl_ma_loss loss on project.loss_id = loss.id WHERE project.completion_date IS NOT NULL  AND STR_TO_DATE(benifit.target_date ,'%m-%d-%Y') BETWEEN  STR_TO_DATE( :targetFromDate, '%m-%d-%Y') AND STR_TO_DATE(:targetUptoDateDate,'%m-%d-%Y') AND loss.loss != 'S MATRIX' ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findProjectByProjectEndDated(@Param("targetFromDate") String targetDate, @Param("targetUptoDateDate") String string);

    @Query(value = "SELECT MAX(CONVERT(SUBSTRING_INDEX(tp.code,  :etuType  ,-1),UNSIGNED INTEGER)) AS code FROM tbl_projects tp", nativeQuery = true)
    Long getPreviousProjectCodeByETU(@Param("etuType") String etuType);

    //    @Query(value = "SELECT  COUNT(CASE WHEN ( ematrix.p_actual_start_date IS NULL OR ematrix.p_actual_start_date IS NOT NULL ) AND  ematrix.d_actual_start_date IS  NULL AND ematrix.c_actual_start_date IS NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END )  as pWorkingSatges,\r\n" + 
    //    		"COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT NULL AND ematrix.c_actual_start_date IS NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END ) as dWorkingSatges ,\r\n" + 
    //    		"COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT NULL AND ematrix.c_actual_start_date IS NOT NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END ) cWorkingSatges ,\r\n" + 
    //    		"COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT  NULL AND ematrix.c_actual_start_date IS NOT NULL AND ematrix.a_actual_start_date IS NOT NULL AND project.completion_date IS NULL  THEN 1 END ) aWorkingSatges FROM tbl_projects project  INNER JOIN tbl_project_benefit_indents_ematrix ematrix ON project.id = ematrix.project_id \r\n" + 
    //    		"", nativeQuery = true)
    @Query(value = "SELECT  COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL  AND  ematrix.d_actual_start_date IS  NULL AND ematrix.c_actual_start_date IS NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END )  as pWorkingSatges,\r\n"
            + "COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT NULL AND ematrix.c_actual_start_date IS NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END ) as dWorkingSatges ,\r\n"
            + "COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT NULL AND ematrix.c_actual_start_date IS NOT NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END ) cWorkingSatges ,\r\n"
            + "COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT  NULL AND ematrix.c_actual_start_date IS NOT NULL AND ematrix.a_actual_start_date IS NOT NULL AND project.completion_date IS NULL  THEN 1 END ) aWorkingSatges FROM tbl_projects project  INNER JOIN tbl_project_benefit_indents_ematrix ematrix ON project.id = ematrix.project_id \r\n"
            + " ", nativeQuery = true)

    List<Object[]> getSatgeCount();

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE benifit.p_actual_start_date IS NOT NULL AND  benifit.d_actual_start_date IS  NULL AND benifit.c_actual_start_date IS NULL AND benifit.a_actual_start_date IS NULL  ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByStatePOrderByCreatedAtDesc();

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE benifit.p_actual_start_date IS NOT NULL AND  benifit.d_actual_start_date IS NOT NULL AND benifit.c_actual_start_date IS NULL AND benifit.a_actual_start_date IS NULL  ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByStateDOrderByCreatedAtDesc();

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE benifit.p_actual_start_date IS NOT NULL AND  benifit.d_actual_start_date IS NOT NULL AND benifit.c_actual_start_date IS NOT NULL AND benifit.a_actual_start_date IS NULL  ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByStateCOrderByCreatedAtDesc();

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE benifit.p_actual_start_date IS NOT NULL AND  benifit.d_actual_start_date IS NOT NULL AND benifit.c_actual_start_date IS NOT NULL AND benifit.a_actual_start_date IS NOT NULL AND project.completion_date IS NULL ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByStateAOrderByCreatedAtDesc();

    @Query(value = "SELECT  * FROM tbl_projects project WHERE DATEDIFF(CURDATE()  , project.completion_date )  <= 367", nativeQuery = true)
    List<Project> findAllInSavingMonitor();

    @Query(value = "SELECT  * FROM tbl_projects project WHERE project.etu_id = :etuId && YEAR(project.completion_date) IN (:yearList)", nativeQuery = true)
    List<Project> findAllInEtuAndFactorYear(@Param("etuId") Long etuId, @Param("yearList") List<Integer> yearList);

    @Query(value = "SELECT project.* FROM tbl_projects  project LEFT JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE project.etu_id IN (:etuIds) ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByOrderByCreatedAtDescForDWhereETUIn(@Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE benifit.p_actual_start_date IS NOT NULL AND benifit.d_actual_start_date IS  NULL AND benifit.c_actual_start_date IS NULL AND benifit.a_actual_start_date IS NULL AND project.etu_id IN (:etuIds) ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByStatePOrderByCreatedAtDesc(@Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE benifit.p_actual_start_date IS NOT NULL AND  benifit.d_actual_start_date IS NOT NULL AND benifit.c_actual_start_date IS NULL AND benifit.a_actual_start_date IS NULL AND project.etu_id IN (:etuIds)  ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByStateDOrderByCreatedAtDesc(@Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE benifit.p_actual_start_date IS NOT NULL AND  benifit.d_actual_start_date IS NOT NULL AND benifit.c_actual_start_date IS NOT NULL AND benifit.a_actual_start_date IS NULL AND project.etu_id IN (:etuIds)  ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByStateCOrderByCreatedAtDesc(@Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE benifit.p_actual_start_date IS NOT NULL AND  benifit.d_actual_start_date IS NOT NULL AND benifit.c_actual_start_date IS NOT NULL AND benifit.a_actual_start_date IS NOT NULL AND project.completion_date IS NULL AND project.etu_id IN (:etuIds) ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByStateAOrderByCreatedAtDesc(@Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE project.etu_id IN (:etuIds)  ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findAllByOrderByCreatedAtDesc(@Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT distinct project.project_name,project.code FROM tbl_projects  project WHERE project.etu_id = :etuId", nativeQuery = true)
    List<String> findAllProjectNamesByEtuIds(@Param("etuId") Long etuId);

    @Query(value = "SELECT project.* FROM tbl_projects  project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id WHERE STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')  BETWEEN  STR_TO_DATE( :startDate, '%m-%d-%Y') AND  STR_TO_DATE( :endDate ,'%m-%d-%Y') AND project.etu_id IN (:etuIds) ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findProjectUsingStartDateAndEtu(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT * FROM tbl_projects project JOIN tbl_project_benefit_indents_ematrix  benifit ON project.id = benifit.project_id JOIN tbl_ma_loss loss on project.loss_id = loss.id WHERE project.completion_date IS NOT NULL  AND STR_TO_DATE(benifit.target_date ,'%m-%d-%Y') BETWEEN  STR_TO_DATE( :targetFromDate, '%m-%d-%Y') AND STR_TO_DATE(:targetUptoDateDate,'%m-%d-%Y') AND loss.loss != 'S MATRIX' AND project.etu_id IN (:etuIds) ORDER BY STR_TO_DATE(benifit.start_date ,'%m-%d-%Y')", nativeQuery = true)
    List<Project> findProjectByProjectEndDated(@Param("targetFromDate") String targetDate, @Param("targetUptoDateDate") String string, @Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT  COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL  AND  ematrix.d_actual_start_date IS  NULL AND ematrix.c_actual_start_date IS NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END )  as pWorkingSatges,\r\n"
            + "COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT NULL AND ematrix.c_actual_start_date IS NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END ) as dWorkingSatges ,\r\n"
            + "COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT NULL AND ematrix.c_actual_start_date IS NOT NULL AND ematrix.a_actual_start_date IS NULL THEN 1 END ) cWorkingSatges ,\r\n"
            + "COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL AND  ematrix.d_actual_start_date IS NOT  NULL AND ematrix.c_actual_start_date IS NOT NULL AND ematrix.a_actual_start_date IS NOT NULL AND project.completion_date IS NULL  THEN 1 END ) aWorkingSatges FROM tbl_projects project  INNER JOIN tbl_project_benefit_indents_ematrix ematrix ON project.id = ematrix.project_id \r\n"
            + "WHERE project.etu_id IN (:etuIds) ", nativeQuery = true)
    List<Object[]> getSatgeCount(@Param("etuIds") List<Long> etuIds);

    @Query(value = "SELECT distinct project.project_name,project.code FROM tbl_projects  project", nativeQuery = true)
    List<String> findAllProjectNames();



	List<BigInteger> findAllProjectIdsByEtu(ETU etu);

	List<BigInteger> findAllProjectIdsByEtuId(List<ETU> etulist);

	
	@Query(value = "SELECT DISTINCT ptm.project_id ,ptm.user_id , ptm.role_id, COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL "  
+"AND  ematrix.d_actual_start_date IS  NULL " 
+"AND ematrix.c_actual_start_date IS NULL " 
+"AND ematrix.a_actual_start_date IS NULL THEN 1 END )  as pWorkingSatges,\r\n "+
"COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL "+ 
"AND  ematrix.d_actual_start_date IS NOT NULL "+ 
"AND ematrix.c_actual_start_date IS NULL "+ 
"AND ematrix.a_actual_start_date IS NULL THEN 1 END ) as dWorkingSatges ,\r\n "+
"COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL "+ 
"AND  ematrix.d_actual_start_date IS NOT NULL "+ 
"AND ematrix.c_actual_start_date IS NOT NULL "+ 
"AND ematrix.a_actual_start_date IS NULL THEN 1 END ) cWorkingSatges ,"+ 
"COUNT(CASE WHEN ematrix.p_actual_start_date IS NOT NULL "+ 
"AND  ematrix.d_actual_start_date IS NOT  NULL "+ 
"AND ematrix.c_actual_start_date IS NOT NULL "+ 
"AND ematrix.a_actual_start_date IS NOT NULL "+ 
"AND project.completion_date IS NULL  THEN 1 END ) aWorkingSatges "+ 
"FROM tbl_projects project "+  
"INNER JOIN tbl_project_team_members ptm "+
"ON project.id = ptm.project_id "+
"INNER JOIN tbl_project_benefit_indents_ematrix ematrix "+ 
"ON project.id = ematrix.project_id "+ 
"GROUP BY ptm.project_id ,ptm.user_id , ptm.role_id " , nativeQuery=true)
	List<Object[]> getstagescounts();
	
	

	@Query(value="SELECT team.user_id as userid , COUNT(*) as totalWorkingProject, "+
"COUNT(CASE WHEN role_id= 1 THEN 1 END )  AS countTotalLeader,"+ 
"COUNT(CASE WHEN role_id= 2 THEN 1 END )  AS countTotalCoach,"+ 
"COUNT(CASE WHEN role_id= 3 THEN 1 END )  AS countTotalMember "+ 
"FROM  tbl_project_team_members team INNER JOIN tbl_projects  project ON project.id = team.project_id "+ 
"WHERE  team.project_id IN(:onGoingProjects) GROUP BY team.user_id",nativeQuery=true)
	List<Object[]> getAllPreferedUserWorkingProjectCount(@Param("onGoingProjects") List<Long> onGoingProjects);
}

