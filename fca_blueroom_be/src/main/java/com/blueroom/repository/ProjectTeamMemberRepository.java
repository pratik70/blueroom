package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectTeamMember;
import com.blueroom.entities.TeamRole;
import com.blueroom.entities.auth.AuthUser;

@Repository
public interface ProjectTeamMemberRepository extends JpaRepository<ProjectTeamMember, Long> {

	List<ProjectTeamMember> findByProjectAndUserAndRole(Project project, AuthUser teamLeader, TeamRole role);
	List<ProjectTeamMember> findByProject(Project project);
	ProjectTeamMember findOneByProjectAndRole(Project project, TeamRole role);
	List<ProjectTeamMember> findAllByProjectId(Long id);
	List<ProjectTeamMember> findOneByProjectId(Long projectId);

}
