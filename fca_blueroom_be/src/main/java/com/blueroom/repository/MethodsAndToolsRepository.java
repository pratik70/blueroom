package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.LossType;
import com.blueroom.entities.MethodsAndTools;
import com.blueroom.vm.MethodsAndToolsVM;

@Repository
public interface MethodsAndToolsRepository extends JpaRepository<MethodsAndTools, Long> {

/*    @Query(value = "SELECT t.id, t.level1_tool, t.level2_tool, t.type, pt.pillar_approach, p.code, p.name"
            + " FROM tbl_ma_methods_tools t"
            + " INNER JOIN tbl_pillar_methods_tools pt on(pt.method_tool_id = t.id)"
            + " INNER JOIN tbl_ma_pillar p on(pt.pillar_id = p.id) WHERE t.id IN (:ids) and p.id IN (:pillerIds)", nativeQuery = true)
    List<Object[]> findAllMethodsToolsAndPillarInfo(@Param("ids") List<String> ids,@Param("pillerIds") List<Long> pillerIds);
*/
	
	
    @Query(value = "SELECT DISTINCT t.id, t.level1_tool, t.level2_tool, t.type,pt.pillar_approach"
    + " FROM tbl_ma_methods_tools t"
    + " left outer JOIN tbl_pillar_methods_tools pt"
    + " on(t.id = pt.method_tool_id)"
    + " WHERE pt.pillar_id IN (:pillerIds) or pt.method_tool_id is null", nativeQuery = true)
    List<Object[]> findAllMethodsToolsAndPillarInfo(@Param("pillerIds") List<Long> pillerIds);

    @Query(value = "SELECT * FROM tbl_ma_methods_tools WHERE id IN (:ids)", nativeQuery = true)
    List<MethodsAndTools> findAll(@Param("ids") List<String> ids);

    
    @Query(value = "SELECT * FROM tbl_ma_methods_tools tml WHERE tml.type ='COMMON METHODS' OR tml.type ='SPECIFIC METHODS'", nativeQuery = true)
    List<MethodsAndTools> findAllcommonandspecificMethods();
    
    @Query(value = "SELECT * FROM tbl_ma_methods_tools tml WHERE tml.type ='COMMON TOOLS' OR tml.type ='SPECIFIC TOOLS'", nativeQuery = true)
    List<MethodsAndTools> findAllcommonandspecificTools();

	List<MethodsAndTools> findOneByLevelFirstTool(String levelFirstTool);

	MethodsAndTools findOneByLevelFirstToolAndLevelSecondTool(String levelFirstTool, String levelSecondTool);

	MethodsAndTools findOneByLevelSecondTool(String levelFirstTool);

	@Query(value="SELECT DISTINCT id FROM tbl_ma_methods_tools WHERE id NOT IN (:methodsToolsListIds)",nativeQuery=true)
	List<BigInteger> getRemainingMethodAndToolIds(@Param("methodsToolsListIds")List<BigInteger> methodsToolsListIds);


	@Query(value="SELECT t.id, t.level1_tool, t.level2_tool, t.type,t.pillar_approach,t.value,t.code FROM tbl_ma_methods_tools t WHERE t.id NOT IN (:methodsToolsIds)",nativeQuery=true)
	List<Object[]> findAllRemaingMethodAndTools(@Param("methodsToolsIds")List<BigInteger> methodsToolsIds);
    
}
