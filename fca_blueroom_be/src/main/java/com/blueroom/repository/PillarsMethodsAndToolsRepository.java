package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.PillarsMethodsAndTools;

@Repository
public interface PillarsMethodsAndToolsRepository extends JpaRepository<PillarsMethodsAndTools, Long> {

	
	/*@Query(value ="SELECT P.AMOUNT, P.AMOUNT - SUM(P1.AMOUNT) AS 'AMOUNTSUM', P.LEDGER_ID, P.AGENT_ID FROM TBL_PAY_LEDGERS P"
	           + " LEFT OUTER JOIN TBL_PAY_LEDGERS P1"
	           + " ON P.LEDGER_ID = P1.APPLY_TO"
	           + " WHERE P.TRANSACTION_CATEGORY_ID =:transactionCategoryId"
	           + " AND P.LEDGER_ID IN (:chargeLedgers)"
	           + " GROUP BY P.LEDGER_ID"
	           + " ORDER BY P.AMOUNT DESC", nativeQuery = true)
	            List<String> getAllMethodToolListUsingPillar(@Param("transactionCategoryId")Long transactionCategoryId, @Param("chargeLedgers")List<Long> chargeLedgers);*/
	  @Query(value ="SELECT DISTINCT method_tool_id FROM tbl_pillar_methods_tools WHERE pillar_id IN (:pillarIds) AND value <> 0", nativeQuery = true)
	  List<BigInteger> getAllMethodToolListUsingPillar(@Param("pillarIds")List<Long> pillarIds);
}
