package com.blueroom.repository;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.blueroom.entities.TechnicalTool;

@Resource
public interface TechnicalToolRepository extends JpaRepository<TechnicalTool, Long> {

	 @Query(value = "SELECT * FROM tbl_ma_technicaltool WHERE id IN (:ids)", nativeQuery = true)
	List<TechnicalTool> findAll(@Param("ids") List<String> ids);
	
}
