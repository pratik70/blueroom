package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.TeamRole;
import com.blueroom.entities.auth.Role;

@Repository
public interface RoleRepository extends JpaRepository<TeamRole, Integer> {

	Role findByName(String string);

}
