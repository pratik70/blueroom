package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Benifit;
import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectBenefitIndents;

@Repository
public interface ProjectBenefitIndentsRepository extends JpaRepository<ProjectBenefitIndents, Long> {

	ProjectBenefitIndents findOneByProjectAndBenifit(Project project, Benifit benifit);

	List<ProjectBenefitIndents> findByProject(Project project);

	List<ProjectBenefitIndents> findAllByProjectId(Long id);

}
