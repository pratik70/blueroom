package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectPlanRemarks;

@Repository
public interface ProjectPlanRemarksRepository extends JpaRepository<ProjectPlanRemarks, Long> {

	List<ProjectPlanRemarks> findByProjectAndWeekStartDate(Project project, String weekStartDate);

	List<ProjectPlanRemarks> findAllByProject(Project project);

	List<com.blueroom.entities.ProjectPlanRemarks> findAllByProjectId(Long id);

}
