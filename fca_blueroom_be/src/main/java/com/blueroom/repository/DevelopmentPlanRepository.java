package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.blueroom.entities.DevelopmentPlan;

public interface DevelopmentPlanRepository  extends JpaRepository<DevelopmentPlan, Long> {
	
	@Query(value="SELECT p.* FROM tbl_development_plan p, tbl_development_tool t WHERE p.user_id =:userId AND p.type=:type AND p.YEAR =:year AND p.competency_id =:competencyId AND t.id = p.tool_id AND t.method_id =:methodId", nativeQuery=true)
	List<DevelopmentPlan> getDevelopmentPlan(@Param("userId") Long userId, @Param("competencyId") Long competencyId, @Param("year") Integer year, @Param("type") String type, @Param("methodId") Long methodId);

	@Query(value="SELECT tool_id FROM tbl_development_plan p WHERE p.user_id =:userId AND p.YEAR =:year AND p.competency_id =:competencyId AND p.type=:type", nativeQuery=true)
	List<Object> getToolIdsByUserIdAndCompetencyIdAndYearAndType(@Param("userId") Long userId, @Param("competencyId") Long competencyId, @Param("year") Long year, @Param("type") String type);

}
