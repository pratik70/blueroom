package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectKIP;

@Repository
public interface ProjectKIPRepository extends JpaRepository<ProjectKIP, Long> {

	List<ProjectKIP> findAllByProject(Project project);

	List<ProjectKIP> findAllByProjectId(Long id);

}
