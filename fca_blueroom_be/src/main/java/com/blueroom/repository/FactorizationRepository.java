package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.ETU;
import com.blueroom.entities.Factorization;
import org.springframework.data.repository.query.Param;

@Repository
public interface FactorizationRepository extends JpaRepository<Factorization, Long> {

	List<Factorization> findAllByFactorYear(Integer factorYear);

	Factorization findOneByFactorYearAndFactorMonth(Integer factorYear, Integer factorMonth );

	List<Factorization> findAllByFactorYearAndEtu(Integer factorYear, Long etu);

	List<Factorization> findAllByFactorYearAndEtu(Integer factorYear, ETU etu);

//	@Query(value="SELECT  ptm.user_id, ptm.project_id ,sum(savings.actual_saving_value ) as save,\r\n" + 
//			"(select count(*) from tbl_project_team_members p where p.project_id = savings.project_id),\r\n" + 
//			"case when (select count(*) from tbl_project_team_members p where p.project_id = savings.project_id) > 2 then sum(savings.actual_saving_value )*40/100 \r\n" + 
//			"else sum(savings.actual_saving_value )*50/100 end,\r\n" + 
//			"case when (select count(*) from tbl_project_team_members p where p.project_id = savings.project_id) > 2 then \"40%\"\r\n" + 
//			"else \"50%\" end\r\n" + 
//			"FROM tbl_project_fmatrix_saving savings \r\n" + 
//			"INNER JOIN tbl_project_team_members ptm \r\n" + 
//			"on ptm.project_id = savings.project_id\r\n" + 
//			"where ptm.role_id = 1\r\n" + 
//			"GROUP BY ptm.project_id;", nativeQuery = true)
	
//	@Query(value="SELECT  ptm.user_id, ptm.project_id ,sum(savings.actual_saving_value ) as save,\r\n" + 
//			"(select count(*) from tbl_project_team_members p where p.project_id = savings.project_id),\r\n" + 
//			"case when (select count(*) from tbl_project_team_members p where p.project_id = savings.project_id) > 2 then sum(savings.actual_saving_value )*40/100 \r\n" + 
//			"else sum(savings.actual_saving_value )*50/100 end,\r\n" + 
//			"case when (select count(*) from tbl_project_team_members p where p.project_id = savings.project_id) > 2 then \"40%\"\r\n" + 
//			"else \"50%\" end\r\n" + 
//			"FROM tbl_project_fmatrix_saving savings \r\n" + 
//			"INNER JOIN tbl_project_team_members ptm \r\n" + 
//			"on ptm.project_id = savings.project_id\r\n" + 
//			"where ptm.role_id = 1 and savings.year = :currenYear\r\n" + 
//			"GROUP BY ptm.project_id;", nativeQuery = true)
	@Query(value="SELECT  ptm.user_id, ptm.role_id, ptm.project_id ,sum(savings.actual_saving_value ) as save,\r\n" + 
			"(select count(*) from tbl_project_team_members p where p.project_id = savings.project_id),\r\n" + 
			"case when (select count(*) from tbl_project_team_members p where p.project_id = savings.project_id) > 2 && ptm.role_id = 1 \r\n" + 
			"then sum(savings.actual_saving_value )*40/100\r\n" + 
			" when (select count(*) from tbl_project_team_members p where p.project_id = savings.project_id) > 2 && ptm.role_id <> 1 \r\n" + 
			"then (sum(savings.actual_saving_value) - sum(savings.actual_saving_value )*40/100) / ((select count(*) from tbl_project_team_members p where p.project_id = savings.project_id) -1) \r\n" + 
			"else sum(savings.actual_saving_value )*50/100 end,\r\n" + 
			"case when (select count(*) from tbl_project_team_members p where p.project_id = savings.project_id) > 2 then \"40%\"\r\n" + 
			"else \"50%\" end\r\n" + 
			"FROM tbl_project_fmatrix_saving savings \r\n" + 
			"INNER JOIN tbl_project_team_members ptm \r\n" + 
			"on ptm.project_id = savings.project_id INNER JOIN tbl_projects tp ON tp.id = ptm.project_id \r\n" + 
			"where savings.year = :currenYear and year(tp.completion_date) = :currenYear \r\n" + 
			"\r\n" + 
			"GROUP BY ptm.project_id, ptm.user_id,ptm.role_id ;\r\n" + 
			"\r\n" + 
			"", nativeQuery = true)
	List<Object[]> findAllUserWithProjectIdAndSaving(@Param("currenYear") Integer currenYear);
	
	@Query(value="SELECT member.user_id, COUNT(member.project_id) FROM tbl_project_team_members `member` WHERE member.role_id = 1 GROUP BY member.user_id ", nativeQuery = true)
	List<Object[]> findProjectCountWorkingAsTL (); 
	
}
