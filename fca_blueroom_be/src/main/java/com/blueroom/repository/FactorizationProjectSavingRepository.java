package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.FactorizationProjectSaving;

@Repository
public interface FactorizationProjectSavingRepository extends JpaRepository<FactorizationProjectSaving, Long> {

	List<FactorizationProjectSaving> findAllByFactorYear(Integer factorYear);

	List<FactorizationProjectSaving> findAllByFactorYearAndMonth(Integer factorYear, Integer month);

}
