package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.DevelopmentMethod;

@Repository
public interface DevelopmentMethodsRepository extends JpaRepository<DevelopmentMethod, Long> {

	@Query(value="SELECT * FROM tbl_ma_development d WHERE d.name =:name", nativeQuery = true)
	DevelopmentMethod findByName(@Param("name") String name);
	
	DevelopmentMethod findOneById(Long id);

	List<DevelopmentMethod> findAllByStatus(boolean b);

	@Query(value="SELECT * FROM tbl_ma_development d WHERE d.id =:id and d.status=true", nativeQuery = true)
	DevelopmentMethod findOneByIdAndStatus(@Param("id") Long id);

}
