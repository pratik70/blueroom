package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.blueroom.entities.auth.AuthUser;
import java.util.List;

public interface UserDao extends JpaRepository<AuthUser, Long> {

    List<AuthUser> findAllByEmployeeId(Long employeeId);

    List<AuthUser> findAllByEmail(String email);

    @Query(value="INSERT INTO  tbl_tool_skillset_user2  VALUES(:count, :employeeId,:actual_skill_id, :userPillarId,:userId);",nativeQuery=true)
	void insertIntoTblToolSkillsetUser2(@Param("count")Long count,@Param("employeeId")Long employeeId,@Param("actual_skill_id") Long actual_skill_id,@Param("userPillarId")Long userPillarId,@Param("userId") Long userId);

	List<AuthUser> findAllByStatus(String string);

	List<AuthUser> findAllByEmployeeIdAndStatus(Long employeeId, String string);

	List<AuthUser> findAllByEmailAndStatus(String email, String string);

	AuthUser findOneByemployeeId(long uid);

    @Query(value="SELECT * FROM tbl_user_master t WHERE t.type = \"PD\" ORDER BY t.name ASC;",nativeQuery=true)
	List<AuthUser> findAllPDUser();

    @Query(value="SELECT * FROM tbl_user_master t WHERE t.type = \"blueroom\" ORDER BY t.name ASC;",nativeQuery=true)
   	List<AuthUser> findAllBlueroomUser();

    @Query(value="SELECT u.* FROM tbl_user_master u, userrole ur, user_roles r  WHERE u.id = ur.user_id AND ur.role_id = r.role_id AND r.name = \"PILLAR_LEADER\";",nativeQuery=true)
    List<AuthUser> findPlantHeadTeam();
    
    @Query(value="SELECT DISTINCT u.* FROM tbl_user_master u, userrole ur, user_roles r  WHERE u.id = ur.user_id AND ur.role_id = r.role_id AND  r.name = \"TEAM_MEMBER\" AND u.`type` = \"PD\" AND u.id NOT IN (SELECT user_id FROM tbl_user_pillar_map p );",nativeQuery=true)
    List<AuthUser> findAllTeamMembers();

    @Query(value="SELECT * FROM tbl_user_master t WHERE t.type = \"WO\";",nativeQuery=true)
	List<AuthUser> findAllWOUser();
    
    @Query(value="SELECT * FROM tbl_user_master t WHERE t.name =:name AND t.type = \"PD\";",nativeQuery=true)
	List<AuthUser> findAllByName(@Param("name")String name);
}
