package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blueroom.entities.Competencies;
import com.blueroom.entities.CompetencyPillar;

@Repository
public interface CompetencyPillarRepository extends JpaRepository<CompetencyPillar, Long> {

	CompetencyPillar findOneByCompetencyIdAndPillarId(Long id, Long id2);

	@Query(value = "select c.* FROM competency_pillar_mapping c WHERE c.pillar_id=:pillar_id AND c.`status`=true", nativeQuery = true)
	List<CompetencyPillar> getAllById(@Param("pillar_id") Long pillar_id);

	@Modifying
	@Transactional
	@Query(value = "update competency_pillar_mapping c set c.status=false where pillar_id = :pillarId", nativeQuery = true)
	void deletebycompetency(@Param("pillarId") Long pillarId);
}
