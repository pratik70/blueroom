package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blueroom.entities.SkillInventryEntity;
import com.blueroom.entities.UserSkillSet;

@Repository
public interface UserSkillSetRepository extends JpaRepository<SkillInventryEntity, Long> {

    //skill.method_tool_id  1 and skill.actual_skill >= 3 OR  skill.method_tool_id = 2 and skill.actual_skill >=5
    //@Query(value ="SELECT DISTINCT user_id FROM tbl_tool_skillset_user  WHERE method_tool_id = :toolId and  actual_skill >= :requiredSkill", nativeQuery = true)
    //@Query(value="SELECT DISTINCT user_id FROM tbl_tool_skillset_user  WHERE method_tool_id = :methodToolId AND  actual_skill >= :actualSkill AND user_id NOT IN (:presentUser)", nativeQuery = true)
    @Query(value = "SELECT DISTINCT user_pillar.user_id FROM tbl_tool_skillset_user tbl_skill_user  JOIN tbl_user_pillar_map user_pillar ON tbl_skill_user.pillar_id = user_pillar.pillar_id join tbl_user_master tbl_user_mst on tbl_skill_user.user_id = tbl_user_mst.id  WHERE tbl_skill_user.method_tool_id = :methodToolId AND tbl_skill_user.actual_skill >= :actualSkill AND user_pillar.user_id NOT IN (:presentUser) AND tbl_user_mst.`status`='Active'  ", nativeQuery = true)
    List<BigInteger> getUserListBySkill(@Param("methodToolId") Long methodToolId, @Param("actualSkill") Integer actualSkill, @Param("presentUser") List<BigInteger> presentUser);

//    @Query(value = "SELECT DISTINCT user_pillar.user_id FROM tbl_tool_skillset_user  tbl_skill_user JOIN tbl_user_pillar_map user_pillar ON tbl_skill_user.pillar_id = user_pillar.pillar_id  WHERE tbl_skill_user.method_tool_id = :methodToolId AND  tbl_skill_user.actual_skill >= :actualSkill AND user_pillar.pillar_id in(:pillarids)", nativeQuery = true)
//    List<BigInteger> getUserListBySkillWithoutUser(@Param("methodToolId") Long methodToolId, @Param("actualSkill") Integer actualSkill,@Param("pillarids")List<Long> pillarids);

//    @Query(value = "select DISTINCT upm.user_id from tbl_tool_skillset_user tsu inner join tbl_user_pillar_map upm on upm.user_id = tsu.user_id inner join tbl_user_master tum on tsu.user_id=tum.id  where upm.pillar_id = tsu.pillar_id and tsu.actual_skill >= :actualSkill and tsu.method_tool_id = :methodToolId and  tsu.pillar_id in(:pillarids) and tum.`status`='Active'", nativeQuery = true)
//    List<BigInteger> getUserListBySkillWithoutUser(@Param("methodToolId") Long methodToolId, @Param("actualSkill") Integer actualSkill,@Param("pillarids")List<Long> pillarids);

    @Query(value = "select DISTINCT tsu.user_id from tbl_tool_skillset_user tsu inner join tbl_user_master tum on tsu.user_id=tum.id  where tsu.actual_skill >= :actualSkill and tsu.method_tool_id = :methodToolId  and tum.`status`='Active'", nativeQuery = true)
    List<BigInteger> getUserListBySkillWithoutUser(@Param("methodToolId") Long methodToolId, @Param("actualSkill") Integer actualSkill);
    
    @Query(value = "SELECT tbl_skill_user.* FROM tbl_tool_skillset_user tbl_skill_user JOIN tbl_ma_pillar user_pillar ON tbl_skill_user.pillar_id = user_pillar.id JOIN tbl_user_master tbl_u_mst ON tbl_skill_user.user_id=tbl_u_mst.id WHERE tbl_skill_user.method_tool_id = :methodToolid AND tbl_skill_user.user_id = :userId AND tbl_u_mst.`status`='Active'", nativeQuery = true)
    List<SkillInventryEntity> findAllByMethodsToolsAndUser(@Param("methodToolid") Long methodToolid, @Param("userId") Long userId);

  /*  @Query(value = "SELECT tbl_skill_user.* FROM tbl_tool_skillset_user tbl_skill_user JOIN tbl_ma_pillar user_pillar ON tbl_skill_user.pillar_id = user_pillar.id"" WHERE user_pillar.id = :userId", nativeQuery = true)
    List<SkillInventryEntity> findAllByUser(@Param("userId") Long userId);
*/
    @Query(value = "SELECT tbl_skill_user.* FROM tbl_tool_skillset_user tbl_skill_user JOIN tbl_user_master tbl_u_mst ON tbl_skill_user.user_id=tbl_u_mst.id WHERE tbl_skill_user.user_id = :userId AND tbl_u_mst.`status`='Active'", nativeQuery = true)
    List<SkillInventryEntity> findAllByUser(@Param("userId") Long userId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE tbl_tool_skillset_user tbl_skill_user"
            + " INNER JOIN tbl_ma_pillar user_pillar ON tbl_skill_user.pillar_id = user_pillar.id "
            + " SET tbl_skill_user.actual_skill = :updatedSkill "
            + " WHERE tbl_skill_user.method_tool_id = :methodToolId AND user_pillar.id = :userId", nativeQuery = true)
    void saveData(@Param("userId") Long userId, @Param("methodToolId") Long methodToolId, @Param("updatedSkill") Long long1);
    
    @Query(value = "SELECT tbl_skill_user.* FROM tbl_tool_skillset_user tbl_skill_user JOIN tbl_ma_pillar user_pillar ON tbl_skill_user.pillar_id = user_pillar.id WHERE tbl_skill_user.method_tool_id = :methodToolid", nativeQuery = true)
    public List<SkillInventryEntity> findAllByMethodsTools(@Param("methodToolid") Long methodToolid);
}
