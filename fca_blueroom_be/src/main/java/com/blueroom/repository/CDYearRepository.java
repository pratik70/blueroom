package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.CDYear;

@Repository
public interface CDYearRepository extends JpaRepository<CDYear, Long> {

}
