package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.FactorizationSavingProject;
import com.blueroom.entities.auth.AuthUser;

@Repository
public interface FactorizationSavingProjectRepository extends JpaRepository<FactorizationSavingProject, Long> {

	List<FactorizationSavingProject> findAllByUserAndFactorYear(AuthUser user, Integer factorYear);

	List<FactorizationSavingProject> findAllByFactorYear(Integer factorYear);

}
