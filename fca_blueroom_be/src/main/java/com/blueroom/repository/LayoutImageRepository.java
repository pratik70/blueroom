package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.CDYear;
import com.blueroom.entities.ETU;
import com.blueroom.entities.LayoutImage;
import java.util.List;

@Repository
public interface LayoutImageRepository extends JpaRepository<LayoutImage, Long> {

    LayoutImage findOneByLayoutType(String layoutType);

    LayoutImage findOneByLayoutTypeAndCDYearAndEtu(String layoutType, CDYear year, ETU etu2);

    LayoutImage findOneByLayoutTypeAndCDYear(String layoutType, CDYear year);

    List<LayoutImage> findAllByLayoutTypeAndEtuId(String layoutType, Long etuId);

    List<LayoutImage> findAllByLayoutTypeAndCDYearIdAndEtuId(String layoutType, Long yearId, Long etuId);
}
