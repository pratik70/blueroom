package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.UserPillar;

@Repository
public interface UserPillarRepository extends JpaRepository<UserPillar, Long> {

	
	@Query(value="select tool.pillar_id from tbl_pillar_methods_tools tool JOIN tbl_user_pillar userpillar " + 
			" ON tool.pillar_id = userpillar.pillar_id " + 
			" where userpillar.user_id = :userId  AND tool.method_tool_id = :toolId ", nativeQuery = true)
	List<String> getAllUserPillar (@Param("userId")Long userId, @Param("toolId")Long toolId );
}
