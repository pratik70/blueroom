package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.ETU;

@Repository
public interface ETURepository extends JpaRepository<ETU, Long> {

    @Query(value = "SELECT * From tbl_ma_etu e WHERE REPLACE(e.etu_no, ' ', '') =:etu_no", nativeQuery = true)
    ETU findByETUNumber(@Param("etu_no") String etuNo);

	ETU findOneByEtuNo(String etuNo);

	@Query(value="select DISTINCT tuem.user_id FROM tbl_user_etu_map tuem JOIN tbl_user_master tbl_u_mst ON tuem.user_id=tbl_u_mst.id  where tuem.etu_id =:id AND tbl_u_mst.`status`='Active'",nativeQuery = true)
	List< BigInteger> findUserIdsByETUId(@Param("id")Long id);

	@Query(value="SELECT * FROM tbl_ma_etu e WHERE e.etu_name =:etu_name",nativeQuery=true)
	ETU findOneByETUName(@Param("etu_name")String etuName);

	@Query(value="UPDATE tbl_user_etu_map e SET e.etu_id =:etu_id WHERE e.user_id =:user_id",nativeQuery=true)
	void updateUerIdByETUId(@Param("user_id") Long user_id,@Param("etu_id")Long etu_id);
}
