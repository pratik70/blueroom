package com.blueroom.repository;

import com.blueroom.entities.auth.PermissionMatrix;
import com.blueroom.entities.auth.Role;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionMatrixRepository extends JpaRepository<PermissionMatrix, Integer> {

    public List<PermissionMatrix> findAllByRole(Role role);

}
