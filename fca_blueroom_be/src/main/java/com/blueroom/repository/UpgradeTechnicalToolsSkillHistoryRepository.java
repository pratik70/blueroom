package com.blueroom.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.UpgradeTechnicalToolsSkillHistory;

@Repository
public interface UpgradeTechnicalToolsSkillHistoryRepository extends JpaRepository<UpgradeTechnicalToolsSkillHistory, Long> {

	
	
	@Query(value="SELECT DISTINCT skill.technical_tool_id FROM tbl_ma_upgrade_technical_tools_skill_history skill WHERE YEAR(skill.created_at) = :yearVal ",nativeQuery = true)
	List<String> getUpdatedToolIdsUsingYear(@Param("yearVal")Integer yearVal);

	@Query(value="SELECT DISTINCT skill.user_id FROM tbl_ma_upgrade_technical_tools_skill_history skill WHERE YEAR(skill.created_at) = :yearVal ",nativeQuery=true)
	Collection<? extends String> findAllUpdatedUsersByYear(@Param("yearVal")Integer yearVal);

	@Query(value="SELECT * FROM tbl_ma_upgrade_technical_tools_skill_history skill WHERE YEAR(skill.created_at) = :yearVal AND skill.user_id = :userId ORDER BY skill.created_at DESC;",nativeQuery=true)
	List<UpgradeTechnicalToolsSkillHistory> findAllByUserAndYear(@Param("userId")Long userId, @Param("yearVal")Integer yearVal);


}
