package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.blueroom.entities.StageFilter;
import com.blueroom.vm.StageFilterVM;

public interface StageFilterRepository extends JpaRepository<StageFilter, Long> {

	@Query(value="SELECT DISTINCT project_id FROM tbl_ma_project_stage",nativeQuery=true)
	List<Long> getAllonGoingProjects();

	

}
