package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blueroom.entities.SkillInventryEntity;

public interface SkillInventryEntityRepository extends JpaRepository<SkillInventryEntity, Long> {

}
