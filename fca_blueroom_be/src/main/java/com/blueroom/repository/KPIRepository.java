package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.KPI;

@Repository
public interface KPIRepository extends JpaRepository<KPI, Long> {

}
