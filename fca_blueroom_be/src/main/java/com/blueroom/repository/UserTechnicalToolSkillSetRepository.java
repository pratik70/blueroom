package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.TechnicalTool;
import com.blueroom.entities.UserTechnicalToolSkillSet;
import com.blueroom.entities.auth.AuthUser;

@Repository
public interface UserTechnicalToolSkillSetRepository extends JpaRepository<UserTechnicalToolSkillSet, Long>{

	@Query(value="SELECT DISTINCT tsu.user_id FROM tbl_technicaltool_skillset_user tsu JOIN tbl_user_pillar_map user_pillar ON user_pillar.user_id = tsu.user_id WHERE tsu.technicaltool_id = :technicaltoolId AND  tsu.actual_skill >= :actualSkill ", nativeQuery = true)
	List<BigInteger> getUserListBySkillWithoutUser(@Param("technicaltoolId")Long technicaltoolId, @Param("actualSkill") Integer actualSkill/*,@Param("pillarids")List<Long> pillarids*/);
	
	@Query(value="SELECT DISTINCT tsu.user_id FROM tbl_technicaltool_skillset_user tsu JOIN tbl_user_pillar_map user_pillar ON user_pillar.user_id = tsu.user_id WHERE technicaltool_id = :technicaltoolId AND  actual_skill >= :actualSkill AND tsu.user_id NOT IN (:presentUser) ", nativeQuery = true)
	List<BigInteger> getUserListBySkill(@Param("technicaltoolId")Long technicaltoolId, @Param("actualSkill") Integer actualSkill,@Param("presentUser") List<BigInteger> presentUser/*,@Param("pillarids")List<Long> pillarids*/);

	List<UserTechnicalToolSkillSet> findAllByTechnicalToolAndUser(TechnicalTool technicalTool, AuthUser user);

	List<UserTechnicalToolSkillSet> findAllByUser(AuthUser user);

	@Query(value = "SELECT * FROM tbl_technicaltool_skillset_user WHERE id IN (:ids)", nativeQuery = true)
		List<UserTechnicalToolSkillSet> findAll(@Param("ids") List<String> ids);

	@Query(value = "SELECT * FROM tbl_technicaltool_skillset_user WHERE user_id = :userId", nativeQuery = true)
	List<UserTechnicalToolSkillSet> findAllByUserId(@Param("userId") Long userId);


}
