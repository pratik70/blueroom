package com.blueroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Machine;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface MachineRepository extends JpaRepository<Machine, Long> {

    @Query(value = "SELECT * FROM tbl_ma_machine tm WHERE tm.machine_code = :machineCode LIMIT 1", nativeQuery = true)
    Machine findByMachinCode(@Param("machineCode") String machineCode);
}
