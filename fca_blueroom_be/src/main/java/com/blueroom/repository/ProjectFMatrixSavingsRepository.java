package com.blueroom.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.Project;
import com.blueroom.entities.ProjectFMatrixSavings;

@Repository
public interface ProjectFMatrixSavingsRepository extends JpaRepository<ProjectFMatrixSavings, Long> {

	ProjectFMatrixSavings findOneByProjectAndMonthAndYear(Project project, Integer month, Integer year);

	List<ProjectFMatrixSavings> findByProject(Project project);

	@Query(value="SELECT s.month , SUM(s.actual_saving_value) FROM tbl_project_fmatrix_saving s WHERE s.year = :selectedYear AND s.project_id in(:projectlist) GROUP BY s.month", nativeQuery = true)
	List<Object[]> findAllProjectMonthlySaving(@Param("selectedYear")Integer selectedYear,@Param("projectlist") List<BigInteger> projectlist);

	List<ProjectFMatrixSavings> findAllByProjectId(Long id);

	@Query(value="SELECT  tpsp.pillar_id, SUM(s.actual_saving_value) FROM tbl_project_fmatrix_saving s join tbl_project_supporting_pillars tpsp on  s.project_id=tpsp.project_id where  s.year = :factorYear and tpsp.is_primary = true GROUP BY tpsp.pillar_id;",nativeQuery=true)
	List<Object[]> getPillarSavings(@Param("factorYear")Integer factorYear);

	@Query(value ="select sum(f.actual_saving_value) as total from tbl_project_fmatrix_saving f where f.project_id in (:projectIdsbyPillar) limit 1",nativeQuery=true)
	BigInteger getSumOfActualSavingsByProjectIds(@Param("projectIdsbyPillar")List<Long> projectIdsbyPillar);

	@Query(value="SELECT  tme.id, SUM(s.actual_saving_value) FROM tbl_project_fmatrix_saving s join tbl_projects  tp on  s.project_id=tp.id join tbl_ma_etu tme on tp.etu_id=tme.id where  s.year = :factorYear  GROUP BY tme.id;",nativeQuery=true)
	List<Object[]> getETUSavings(@Param("factorYear")Integer factorYear);
}
