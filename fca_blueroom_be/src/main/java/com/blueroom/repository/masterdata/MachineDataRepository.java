/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.repository.masterdata;

import com.blueroom.entities.masterdata.MachineData;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author govind
 */
@Repository
public interface MachineDataRepository extends JpaRepository<MachineData, Long> {

    public List<MachineData> findByEtuId(Long etuId);
}
