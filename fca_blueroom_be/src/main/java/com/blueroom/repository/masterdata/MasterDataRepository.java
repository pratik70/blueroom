/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.repository.masterdata;

import com.blueroom.entities.masterdata.MasterData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author govind
 */
@Repository
public interface MasterDataRepository extends JpaRepository<MasterData, Long> {

    public MasterData findByEtuId(Long etuID);

}
