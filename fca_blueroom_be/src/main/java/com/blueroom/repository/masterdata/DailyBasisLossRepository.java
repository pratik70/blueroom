/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blueroom.repository.masterdata;

import com.blueroom.entities.ETU;
import com.blueroom.entities.masterdata.DailyBasisLoss;
import com.blueroom.entities.masterdata.DailyBasisLossType;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author govind
 */
@Repository
public interface DailyBasisLossRepository extends JpaRepository<DailyBasisLoss, Long> {

    public List<DailyBasisLoss> findByEtuId(Long etuId);

    public List<DailyBasisLoss> findByEtuAndLossDateGreaterThanEqual(ETU etu, Date startDate);

    public List<DailyBasisLoss> findByEtuAndLossDateLessThanEqual(ETU etu, Date endDate);

    public List<DailyBasisLoss> findByEtuAndLossDateGreaterThanEqualAndLossDateLessThanEqual(ETU etu, Date startDate, Date endDate);

    public List<DailyBasisLoss> findByEtuAndDailyBasisLossTypeIn(ETU etu, List<DailyBasisLossType> lossTypes);

    public List<DailyBasisLoss> findByEtuAndDailyBasisLossTypeInAndLossDateGreaterThanEqual(ETU etu, List<DailyBasisLossType> lossTypes, Date startDate);

    public List<DailyBasisLoss> findByEtuAndDailyBasisLossTypeInAndLossDateLessThanEqual(ETU etu, List<DailyBasisLossType> lossTypes, Date endDate);

    public List<DailyBasisLoss> findByEtuAndDailyBasisLossTypeInAndLossDateGreaterThanEqualAndLossDateLessThanEqual(ETU etu, List<DailyBasisLossType> lossTypes, Date startDate, Date endDate);

    public List<DailyBasisLoss> findByEtuIdAndDailyBasisLossType(Long etuId, DailyBasisLossType lossTypeId);

}
