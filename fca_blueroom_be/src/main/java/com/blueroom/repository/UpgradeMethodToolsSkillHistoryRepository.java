package com.blueroom.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.UpgradeMethodToolsSkillHistory;

@Repository
public interface UpgradeMethodToolsSkillHistoryRepository extends JpaRepository<UpgradeMethodToolsSkillHistory, Long> {

	@Query(value="SELECT * FROM tbl_ma_upgrade_method_tools_skill_history skill WHERE YEAR(skill.created_at) =:yearVal AND skill.user_id =:userId ORDER BY skill.created_at DESC",nativeQuery = true)
	List<UpgradeMethodToolsSkillHistory> findAllByUserAndYear(@Param("userId")Long userId, @Param("yearVal")Integer yearVal);

	@Query(value="SELECT DISTINCT skill.methods_tools_id FROM tbl_ma_upgrade_method_tools_skill_history skill WHERE YEAR(skill.created_at) = :yearVal ",nativeQuery = true)
	List<String> getUpdatedMethodsIdsUsingYear(@Param("yearVal")Integer yearVal);

	@Query(value="SELECT DISTINCT skill.user_id FROM tbl_ma_upgrade_method_tools_skill_history skill WHERE YEAR(skill.created_at) =:yearVal ",nativeQuery=true)
	List<String> findAllUpdatedUsersByYear(@Param("yearVal")Integer yearVal);

}
