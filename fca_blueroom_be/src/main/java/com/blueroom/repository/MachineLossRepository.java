package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.MachineLoss;

@Repository
public interface MachineLossRepository extends JpaRepository<MachineLoss, Long> {

    @Query(value = "SELECT DISTINCT(source_loss) FROM tbl_machine_loss", nativeQuery = true)
    List<String> getUniqueSoureceLosses();

    @Query(value = "SELECT DISTINCT(impacted_loss) FROM tbl_machine_loss", nativeQuery = true)
    List<String> getUniqueImpactedLosses();

    @Query(value = "SELECT CONCAT(t.source_loss ,' ', t.source_op ,' ', t.impacted_loss,' ', t.impacted_op, case when t.value_red = 1 then 'color red' when t.value_green = 1 then 'color green'  when t.value_yellow = 1 then 'color yellow' else 'color white' end) FROM tbl_machine_loss t WHERE t.value_red = 1 or t.value_green = 1 or t.value_yellow = 1", nativeQuery = true)
    List<String> getActiveSourceInpactedLoss();

    @Query(value = "SELECT tm.id FROM tbl_machine_loss tm WHERE  tm.source_loss =:sourceLoss AND  (tm.source_op =:sourceOp OR tm.source_op =:sourceOp1)  AND  tm.impacted_loss =:impactedLoss  AND  (tm.impacted_op =:impactedOp OR tm.impacted_op =:impactedOp1) limit 1", nativeQuery = true)
    String findIdByCategories(@Param("sourceOp") String sourceOp, @Param("sourceOp1") String sourceOp1, @Param("sourceLoss") String sourceLoss, @Param("impactedOp") String impactedOp, @Param("impactedOp1") String impactedOp1, @Param("impactedLoss") String impactedLoss);

}
