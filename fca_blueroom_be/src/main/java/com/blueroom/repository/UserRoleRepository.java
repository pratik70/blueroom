package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.auth.Role;

@Repository
public interface UserRoleRepository extends JpaRepository<Role, Integer> {

    @Query(value="SELECT * FROM user_roles u WHERE u.`type` = \"PD\";",nativeQuery=true)
	List<Role> findAllPDRole();
    
    @Query(value="SELECT * FROM user_roles u WHERE u.`type` = \"blueroom\";",nativeQuery=true)
	List<Role> findAllBlueroomRole();

    @Query(value="SELECT DISTINCT preffered_learning_style FROM tbl_user_master;",nativeQuery=true)
   List<String> findAllLearingStyle();
}
