package com.blueroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blueroom.entities.CMatrices;
import com.blueroom.entities.ETU;
import com.blueroom.entities.Machine;
import java.util.Date;

@Repository
public interface CMatricesRepository extends JpaRepository<CMatrices, Long> {

    //SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl where tbl.etu_id = 1 AND MONTH(tbl.data_date) = 5 AND YEAR(tbl.data_date) = 2018 GROUP BY  tbl.loss_type_id;
    @Query(value = "SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl  GROUP BY  tbl.loss_type_id", nativeQuery = true)
    List<Object[]> getTotalLossValueForETU();

    @Query(value = "SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl WHERE tbl.etu_id = :etuType AND MONTH(tbl.data_date) = :month AND YEAR(tbl.data_date) = :year GROUP BY  tbl.loss_type_id", nativeQuery = true)
    List<Object[]> getTotalLossValueForETU(@Param("etuType") Long etuType, @Param("year") int year, @Param("month") int month);

    @Query(value = "SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl WHERE tbl.etu_id = :etuType AND YEAR(tbl.data_date) = :year GROUP BY  tbl.loss_type_id", nativeQuery = true)
    List<Object[]> getTotalLossValueForETUUsingTypeAndYear(@Param("etuType") Long etuType, @Param("year") int year);

    @Query(value = "SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl WHERE tbl.etu_id = :etuType AND MONTH(tbl.data_date) = :monht GROUP BY  tbl.loss_type_id", nativeQuery = true)
    List<Object[]> getTotalLossValueForETUUsingTypeAndMonth(@Param("etuType") Long etuType, @Param("monht") int monht);

    @Query(value = "SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl WHERE MONTH(tbl.data_date) = :month AND YEAR(tbl.data_date) = :year GROUP BY  tbl.loss_type_id", nativeQuery = true)
    List<Object[]> getTotalLossValueForETU(@Param("year") int year, @Param("month") int month);

    @Query(value = "SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl WHERE tbl.etu_id = :etuType GROUP BY  tbl.loss_type_id", nativeQuery = true)
    List<Object[]> getTotalLossValueForETU(@Param("etuType") Long etuType);

    @Query(value = "SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl WHERE YEAR(tbl.data_date) = :year GROUP BY  tbl.loss_type_id", nativeQuery = true)
    List<Object[]> getTotalLossValueForETUUsingYear(@Param("year") int year);

    @Query(value = "SELECT tbl.loss_type_id , SUM(tbl.total_loss_value) FROM tbl_cmatrices tbl WHERE  MONTH(tbl.data_date) = :month  GROUP BY  tbl.loss_type_id", nativeQuery = true)
    List<Object[]> getTotalLossValueForETUUsingMonth(@Param("month") int month);

    @Query(value = "SELECT * FROM tbl_cmatrices tbl WHERE tbl.etu_id = :etuType AND MONTH(tbl.data_date) = :month AND YEAR(tbl.data_date) = :year ORDER BY tbl.data_date ", nativeQuery = true)
    List<CMatrices> getCMatricesDataForETU(@Param("etuType") Long etuType, @Param("year") int year, @Param("month") int month);

    public CMatrices findByDescriptionAndMachineAndEtuAndDataDate(String description, Machine machine, ETU etu, Date dataDate);

}
