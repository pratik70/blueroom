'use strict';

app.factory('userService',userService);

function userService($http,$q,$rootScope,$cookies){
    return{
        createUser: function (data) {
            $http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
	    	var defer = $q.defer();
			$http.post($rootScope.baseURL+'/user/save',data).then(function(res){
                defer.resolve(res.data);
			}, function(err){
                defer.reject(err);
                
			});
			return defer.promise;
        },
        
        updateUser: function (data) {
            $http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
	    	var defer = $q.defer();
			$http.post($rootScope.baseURL+'/user/update',data).then(function(res){
                defer.resolve(res.data);
                
			}, function(err){
                defer.reject(err);
                
			});
			return defer.promise;
        },

        uploadfile: function (data) {
            $http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
            var defer = $q.defer();
            var formdata = new FormData();
            formdata.append("file", data);
            $http.post($rootScope.baseURL+'/user/fileUploader',formdata,{transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(res){
                defer.resolve(res.data);
			}, function(err){
                defer.reject(err);
			});
			return defer.promise;
        },

        getAllUsers:function(){
            $http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/user/getall').then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
        },
        
      getAllETUType: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/allETUType').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

        getUserDetails:function(userid){
            userid = userid ? userid : JSON.parse(localStorage.getItem("userDetails")).id;
            $http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token'); 
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/user/getuser/'+userid).then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;

        },

         getProfile: function (imgName) {
            $http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token'); 
            var defer = $q.defer();
            $http.get($rootScope.baseURL+'/user/download-image/'+imgName).then(function(res){
                defer.resolve(res.data);
            }, function(err){
                defer.reject(err);
            });
            return defer.promise;
        },

        getAllRoles:function(){
            $http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/user/getAllRoles').then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
        }

    };
}