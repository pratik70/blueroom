'use strict';

app.factory('dashboardService', dashboardService);

 /** @ngInject */
function dashboardService($http,$q,$rootScope,$cookies) {
	return {
        getAllLossGroup: function (data) {	
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/allLossGroup').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllMachineLossList: function (data) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getAllMachineLossList').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},		
		updateLoss : function (data) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/updateLoss', data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		changeBMatrixDataColor: function(data) {
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/changeBMatrixDataColor',data).then(function(res){
				defer.resolve(res.data);
			},function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
    };
}    
