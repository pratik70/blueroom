'use strict';

app.factory('projectService', projectService);

 /** @ngInject */
function projectService($http,$q,$rootScope,$cookies) {
	return {
		findProjectByProjectCode: function(projectCode){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/code/'+projectCode).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
	    getProjectInformation: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+projectId).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
	    createProject: function (data) {
	    	var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
	    },
	    updateProject: function (data) {
	    	var defer = $q.defer();
			$http.put($rootScope.baseURL+'/project/'+data.id,data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
	    },
		getAllLossGroup: function (data) {
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');		
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/allLossGroup').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getKPIsList: function (data) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getKPIsList').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllMachine: function(id){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			var api = '/allMachine';
			if(id){
				api = '/allMachine/'+id;
			}
			$http.get($rootScope.baseURL+ api).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		getAllETUType: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/allETUType').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllProcess: function(){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/allProcess').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllPillars: function(){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/allPillars').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		addProjectPillars: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/addPillars',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectPillars: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+ projectId +'/getPillars').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllSelectedPillarMethodsAndTools: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/getMethodsToolsList/'+projectId).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		saveProjectMethodsAndTool: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/saveMethodsAndTools',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectMethodsAndTool: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+ projectId +'/getMethodsAndTools').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectTeamMembers: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+ projectId +'/getProjectTeamMembers').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		saveProjectMembers: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/addMembers',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getUserList: function(){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getUserList').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		getUserListBySkill: function(projectId,usersmode){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+projectId+'/qualifiedusers/'+usersmode).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllBenifitTypeList: function(){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getAllBenifitTypeList').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		manageProjectCosting: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/manageProjectCosting',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		sendEmailsToETUAndFinanceApprovals: function(projectID){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+projectID+'/sendEmailsToETUAndFinanceApprovals').then(function(res){
				defer.resolve(res.data);
			},function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		savePlanningDates: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/savePlanningDates',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectCostingData: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+projectId+'/getProjectCostingData').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		saveProjectExecutionDates: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/saveExecutionDates',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectPlanningDates: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+projectId+'/getProjectPlanningDates').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		saveProjectActualSavings: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/saveProjectActualSavings',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectActualSavings: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+projectId+'/getProjectActualSavings').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		saveProjectCompletionData: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/saveProjectComplete',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		startExecutionPlan: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+projectId+'/startExecutionPlan').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		addProjectRemark: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/addProjectRemark',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectRemark: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/getProjectRemark',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
  	   saveProjectDocument: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
			var defer = $q.defer();
			var formData = new FormData();
                formData.append("file", data);
                $http.post($rootScope.baseURL+'/upload/document', formData, {transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllTechnicalToolList: function(){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getAllTechnicalToolList').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectTechnicalTool : function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+projectId+'/getProjectTechnicalTool').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getSkillUpdationData: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/project/'+ projectId +'/getSkillUpdationData').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		saveProjectEwDocument: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
			var defer = $q.defer();
			var formData = new FormData();
                formData.append("file", data);
                $http.post($rootScope.baseURL+'/upload/ew-document', formData, {transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		sendTrainingMail: function(projectId,data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/'+projectId+'/sendTrainingMail',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		upgradeUserSkill: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/upgradeUserSkill' ,data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		changeETUApprovalStatusById: function (id) {
			
	    	var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/changeETUStatus/'+id).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		changeFinanceApprovalStatusById: function (id) {
			
	    	var defer = $q.defer();
			$http.post($rootScope.baseURL+'/project/changeFinanceStatus/'+id).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		
	};
}