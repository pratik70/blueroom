'use strict';

app.factory('commonService', commonService);

 /** @ngInject */
function commonService($http,$q,$rootScope,$cookies) {
	return {
        getAllLossGroup: function (data) {	
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/allLossGroup').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllMachineLossList: function (data) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getAllMachineLossList').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllLosses:function(){
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/getLoss').then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
		},
		getTechnicalTools:function(){
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/getTheTechnicalTools').then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
		},
		getWCMMethods:function(){
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/getwcmMethods').then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
		},
		getWCMTools:function(){
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/getwcmTools').then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
		},
		getOperations:function(id){
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/getOperations/'+id).then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
		},
		addWCMTool: function (data) {	
			var defer = $q.defer();
			$http.post($rootScope.baseURL + '/addWcmTool', data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		updateWCMTool: function (id,data) {	
			var defer = $q.defer();
			$http.post($rootScope.baseURL + '/updateWcmTool/'+id, data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		updateWCMMethod: function (id,data) {	
			var defer = $q.defer();
			$http.post($rootScope.baseURL + '/updateWcmMethod/'+id, data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		deleteOperation: function (id) {	
			var defer = $q.defer();
			$http.delete($rootScope.baseURL + '/deleteOperation/'+id).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		addWCMMethod: function (data) {	
			var defer = $q.defer();
			$http.post($rootScope.baseURL + '/addWcmMethod', data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		addLossType: function (data) {	
			var defer = $q.defer();
			$http.post($rootScope.baseURL + '/addlossType', data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		addTechnicalTool: function (data) {	
			var defer = $q.defer();
			$http.post($rootScope.baseURL + '/addtechnicalTool', data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		addOperations: function (data) {	
			var defer = $q.defer();
			$http.post($rootScope.baseURL + '/addOperations', data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		
		getCDYearList : function() {
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
            var defer = $q.defer();
            $http.get($rootScope.baseURL+'/getCDYearList').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },
        getImage : function(imageUrl) {
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
            var defer = $q.defer();
            $http({
            	method: 'GET',
            	headers: { 'Accept': 'application/jpge' },
            	url : $rootScope.baseURL+''+imageUrl,
            }).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },
      	getAllEmployeeList: function (data) {	
      		$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getEtuUserList').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		userLogin: function (data) {	
			var defer = $q.defer();
			$http.post($rootScope.authURL + '/login', data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllETUType: function(data){
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getAllETU').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		updateETUType: function(data){
			var defer = $q.defer();
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token'); 
			$http.post($rootScope.baseURL+'/updateETU',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		
		getUserDetails:function(userid){
			userid = userid ? userid : localStorage.getItem("user_id"); 
            $http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token'); 
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/user/getuser/'+userid).then(function(response){
                defer.resolve(response.data);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
	},
		getUserDetailsByEmployeeId:function(employeeId){
            var defer= $q.defer();
            $http.get($rootScope.baseURL+'/user/getUserByEmployeeId/'+employeeId).then(function(response){
				console.log(response);
                defer.resolve(response);
            },function(error){
                defer.reject(error);
            });
            return defer.promise;
	},

		
    };
}    
