'use strict';

app.factory('matricesService', matricesService);

 /** @ngInject */
function matricesService($http,$q,$rootScope,$cookies) {
	return {
        getCMatricesInformation: function(data){
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			if($rootScope.isSelectAllETU){
				data = addEtuFilter(null, data);
			} else{
				data = addEtuFilter($rootScope.selectedETU, data);
			}
			

			$http.post($rootScope.baseURL+'/getCMatricesData',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },
        getCMatricesParetoData: function(data){
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();

			$http.post($rootScope.baseURL+'/getCMatricesParetoData',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },
        getDMatricesInformation: function(filterData){
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();

			//var filterData = {};
			
			if($rootScope.isSelectAllETU){
				filterData = addEtuFilter(null, filterData);
			} else{
				filterData = addEtuFilter($rootScope.selectedETU, filterData);
			}
			$http.post($rootScope.baseURL+'/getDMatricesData',filterData).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getAllPendingPDCAProjectListByETU: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getAllPendingPDCAProjectListByETU/'+data).then(function(res){
				defer.resolve(res);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },
        getEMatricesInformation: function(data){
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();

			if($rootScope.isSelectAllETU){
				data = addEtuFilter(null, data);
			} else{
				data = addEtuFilter($rootScope.selectedETU, data);
			}

			$http.post($rootScope.baseURL+'/getEMatricesData',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },
        setProjectStatus: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/setProjectStatus',data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },
        getFMatricesData: function(data){
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			if($rootScope.isSelectAllETU){
				data = addEtuFilter(null, data);
			} else{
				data = addEtuFilter($rootScope.selectedETU, data);
			}
			$http.post( $rootScope.baseURL+'/getFMatricesData', data ).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getPillarSavings: function(year){
			
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/getPillarSavings/'+year).then(function(res){
				defer.resolve(res.data);
			},function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		getETUSavings: function(year){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getETUSavings/'+year).then(function(res){
				defer.resolve(res.data);
			},function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
        getDEFMatricesData: function(filterData){
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();

						
			if($rootScope.isSelectAllETU){
				filterData = addEtuFilter(null, filterData);
			} else{
				filterData = addEtuFilter($rootScope.selectedETU, filterData);
			}

			$http.post($rootScope.baseURL+'/getDEFMatricesData',filterData).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },
        getAllETUType: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/allETUType').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectDetails: function(projectId){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/getProjectDetails/'+projectId).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		 deleteProject: function(data){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');
			var defer= $q.defer();
			$http.delete($rootScope.baseURL+'/project/'+data.projectId).then(function(res){
				defer.resolve(res.data);
			},function(err){
					defer.reject(err);
			});
			return defer.promise;
		},
		
		stageWorkingCount: function(){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			var filterData = {};
						
			if($rootScope.isSelectAllETU){
				filterData = addEtuFilter(null, filterData);
			} else{
				filterData = addEtuFilter($rootScope.selectedETU, filterData);
			}
			$http.post($rootScope.baseURL+'/project/report/stageWorkingCount',filterData).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getEtuUsersSkill: function(){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			var filterData = {};
						
			if($rootScope.isSelectAllETU){
				filterData = addEtuFilter(null, filterData);
			} else{
				filterData = addEtuFilter($rootScope.selectedETU, filterData);
			}
			$http.post($rootScope.baseURL+'/skill/getEtuUsersSkill', filterData).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
    }
}    

function addEtuFilter(etu,filterData){
	var etuList  = [];
	if(etu){
		etuList.push(etu);
	}
	filterData.etuList = etuList;
	return filterData;
}
