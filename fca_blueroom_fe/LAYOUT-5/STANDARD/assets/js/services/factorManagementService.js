'use strict';

app.factory('factorManagementService', factorManagementService);

 /** @ngInject */
function factorManagementService($http,$q,$rootScope,$cookies) {
	return {
        getFactorizationData: function (year, etuID) {	
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/factorization/'+year+'/'+ etuID).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		saveFactorizationData: function (year, data) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/factorization/'+year,data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		userSavingsOnProject: function (year) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			var filterData = {};
			if($rootScope.isSelectAllETU){
				filterData = addEtuFilter(null, filterData);
			} else{
				filterData = addEtuFilter($rootScope.selectedETU, filterData);
			}
			
			$http.post($rootScope.baseURL+'/factorization/usersavingsonproject/'+year, filterData).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		addUserSavingsOnProjectFactors: function (year, data) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/factorization/addUserSavingsAndProjectFactors/'+year, data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getUserSavingsAndProjectFactors: function (year) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/factorization/getUserSavingsAndProjectFactors/'+year).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getProjectMonthlySavingFactor: function (year,etuID) {
			if(etuID == null){
				etuID = 0;
			}	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/factorization/getProjectMonthlySavingFactor/'+year+"/"+etuID).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		saveProjectSavingsFactor: function (year,data) {	
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/factorization/saveProjectSavingsFactor/'+year,data).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
    };
}    

function addEtuFilter(etu,filterData){
	var etuList  = [];
	if(etu){
		etuList.push(etu);
	}
	filterData.etuList = etuList;
	return filterData;
}