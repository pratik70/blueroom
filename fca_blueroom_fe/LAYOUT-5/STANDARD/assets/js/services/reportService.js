'use strict';

app.factory('reportService', reportService);

 /** @ngInject */
function reportService($http,$q,$rootScope,$cookies) {
	return {
        getAllEmployeeList: function (data) {	
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			var filterData = {};
			if($rootScope.isSelectAllETU){
				filterData = addEtuFilter(null, filterData);
			} else{
				filterData = addEtuFilter($rootScope.selectedETU, filterData);
			}
			
			$http.post($rootScope.baseURL+'/getEtuUserList',filterData).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getPeoplePerformanceEvaluationReport: function (year,employees) {
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/factorization/getPeoplePerformanceEvaluationReport/'+ year + '/' + employees).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
		getUserSkillReport: function (year) {
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL+'/skill/getUserSkillReport/'+ year ).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
    };
}    

function addEtuFilter(etu,filterData){
	var etuList  = [];
	if(etu){
		etuList.push(etu);
	}
	filterData.etuList = etuList;
	return filterData;
}