'use strict';

app.factory('masterService', masterService);

 /** @ngInject */
function masterService($http,$q,$rootScope,$cookies) {
	return {

		//master data

        getMasterDataByEtu: function(etuId){
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL + '/' + etuId + '/getMasterDataByEtu').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },

		saveMasterData: function(masterData){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/saveMasterData', masterData).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},
        

		//machine data

        getMachineDataByEtu: function(etuId){
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL + '/' + etuId + '/getMachineDataByEtu').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
        },

		saveMachineData: function(machineData){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/saveMachineData', machineData).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

        //Daily Loss

		getDailyBasisLossTypes: function() {
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL + '/getDailyBasisLossTypes').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		getDailyBasisLoss: function(dailyBasisLossId) {
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL + '/' + dailyBasisLossId +  '/getDailyBasisLoss').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		getDailyBasisLossFilteredList: function(filter) {
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL + '/getDailyBasisLossFilteredList',filter).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		getDailyBasisLossList: function(etuId) {
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL + '/' + etuId + '/getDailyBasisLossByEtu').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

		saveDailyBasisLoss: function(dailyBasisLoss){
			$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.post($rootScope.baseURL+'/saveDailyBasisLoss', dailyBasisLoss).then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},		

		//Loss Report
		
		getLossReport: function(etuId) {
        	$http.defaults.headers.common['X-AUTH-TOKEN'] = $cookies.get('auth_token');	
			var defer = $q.defer();
			$http.get($rootScope.baseURL + '/' + etuId + '/getLossReport').then(function(res){
				defer.resolve(res.data);
			}, function(err){
				defer.reject(err);
			});
			return defer.promise;
		},

    }
}    

function addEtuFilter(etu,filterData){
	var etuList  = [];
	etuList.push(etu);
	filterData.etuList = etuList;
	return filterData;
}
