'use strict';

/**
 * Config constant
 */
app.constant('APP_MEDIAQUERY', {
    'desktopXL': 1200,
    'desktop': 992,
    'tablet': 768,
    'mobile': 480
});
app.constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {
        //*** Javascript Plugins
        'd3': '../../bower_components/d3/d3.min.js',
        'ag-grid':'./assets/js/lib/ag-grid.js' , 
        'radarChart': './assets/js/lib/RadarChart.js',
        'plotlyJs': './assets/js/lib/plotly-latest.min.js',
        //*** jQuery Plugins
        'chartjs': '../../bower_components/Chart.js/dist/Chart.min.js',
        'ckeditor-plugin': '../../bower_components/ckeditor/ckeditor.js',
        'jquery-nestable-plugin': ['../../bower_components/jquery-nestable/jquery.nestable.js'],
        'touchspin-plugin': ['../../bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js', '../../bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
        'jquery-appear-plugin': ['../../bower_components/jquery-appear/build/jquery.appear.min.js'],
        'spectrum-plugin': ['../../bower_components/spectrum/spectrum.js', '../../bower_components/spectrum/spectrum.css'],
        'jcrop-plugin': ['../../bower_components/Jcrop/js/Jcrop.min.js', '../../bower_components/Jcrop/css/Jcrop.min.css'],
        'lodash' : 'assets/js/lib/lodash.min.js',
        'd3-3d': ['assets/js/lib/bar3d.js','assets/js/lib/utils.js'],
       
        //*** Controllers
        'dashboardCtrl': 'assets/js/controllers/dashboardCtrl.js',
        'iconsCtrl': 'assets/js/controllers/iconsCtrl.js',
        'vAccordionCtrl': 'assets/js/controllers/vAccordionCtrl.js',
        'ckeditorCtrl': 'assets/js/controllers/ckeditorCtrl.js',
        'laddaCtrl': 'assets/js/controllers/laddaCtrl.js',
        'ngTableCtrl': 'assets/js/controllers/ngTableCtrl.js',
        'cropCtrl': 'assets/js/controllers/cropCtrl.js',
        'asideCtrl': 'assets/js/controllers/asideCtrl.js',
        'toasterCtrl': 'assets/js/controllers/toasterCtrl.js',
        'sweetAlertCtrl': 'assets/js/controllers/sweetAlertCtrl.js',
        'mapsCtrl': 'assets/js/controllers/mapsCtrl.js',
        'chartsCtrl': 'assets/js/controllers/chartsCtrl.js',
        'calendarCtrl': 'assets/js/controllers/calendarCtrl.js',
        'nestableCtrl': 'assets/js/controllers/nestableCtrl.js',
        'validationCtrl': ['assets/js/controllers/validationCtrl.js'],
        'userCtrl': ['assets/js/controllers/userCtrl.js'],
        'selectCtrl': 'assets/js/controllers/selectCtrl.js',
        'wizardCtrl': 'assets/js/controllers/wizardCtrl.js',
        'uploadCtrl': 'assets/js/controllers/uploadCtrl.js',
        'treeCtrl': 'assets/js/controllers/treeCtrl.js', 'inboxCtrl': 'assets/js/controllers/inboxCtrl.js',
        'xeditableCtrl': 'assets/js/controllers/xeditableCtrl.js',
        'chatCtrl': 'assets/js/controllers/chatCtrl.js',
        'dynamicTableCtrl': 'assets/js/controllers/dynamicTableCtrl.js',
        'notificationIconsCtrl': 'assets/js/controllers/notificationIconsCtrl.js',
        'dateRangeCtrl': 'assets/js/controllers/daterangeCtrl.js',
        'notifyCtrl': 'assets/js/controllers/notifyCtrl.js',
        'sliderCtrl': 'assets/js/controllers/sliderCtrl.js',
        'knobCtrl': 'assets/js/controllers/knobCtrl.js',
        'crop2Ctrl': 'assets/js/controllers/crop2Ctrl.js',

        'NewProjectCtrl':'assets/js/controllers/projectCtrl.js',
        'DashboardMainCtrl':'assets/js/controllers/dashboardMainCtrl.js',
        'DashAMatrixCtrl':'assets/js/controllers/dashAMatrixCtrl.js',
        'DashBMatrixCtrl':'assets/js/controllers/dashBMatrixCtrl.js',
        'MatricesCtrl':'assets/js/controllers/matricesCtrl.js',
        'CMatrixCtrl':'assets/js/controllers/cMatrixCtrl.js',
        'DMatrixCtrl':'assets/js/controllers/dMatrixCtrl.js',
        'EMatrixCtrl':'assets/js/controllers/eMatrixCtrl.js',
        'FMatrixCtrl':'assets/js/controllers/fMatrixCtrl.js',
        'DEFMatrixCtrl':'assets/js/controllers/defMatrixCtrl.js',

        'SkillInventoryCtrl' : 'assets/js/controllers/skillInventoryCtrl.js',
        'FactorManagementCtrl':'assets/js/controllers/factorManagementCtrl.js',

        'MatricesManagementCtrl':'assets/js/controllers/matricesManagementCtrl.js',

        'LayoutUploadCtrl':'assets/js/controllers/layoutUploadCtrl.js',
        
        'GetUsersCtrl':'assets/js/controllers/GetUsersCtrl.js',
        'getModifiedDataCtrl':'assets/js/controllers/getModifiedDataCtrl.js',
        'technicalToolDataCtrl':'assets/js/controllers/technicalToolDataCtrl.js',
        'wcmMethodDataCtrl':'assets/js/controllers/wcmMethodDataCtrl.js',
        'wcmToolDataCtrl':'assets/js/controllers/wcmToolDataCtrl.js',
        'lossTypeDataCtrl':'assets/js/controllers/lossTypeDataCtrl.js',
        'operationsDataCtrl':'assets/js/controllers/operationsDataCtrl.js',
        'pdcaProjectCtrl':'assets/js/controllers/pdcaProjectCtrl.js',
        'wcmMethodUpdateCtrl':'assets/js/controllers/wcmMethodUpdateCtrl.js',
        'updateWCToolCtrl':'assets/js/controllers/updateWCToolCtrl.js',
        'wcmToolUpdateCtrl':'assets/js/controllers/wcmToolUpdateCtrl.js',
        'deleteWCMProjectDataCtrl':'assets/js/controllers/deleteWCMProjectDataCtrl.js',
        'deleteProjectOfWCMCtrl':'assets/js/controllers/deleteProjectOfWCMCtrl.js',
        'etuApprovalCtrl':'assets/js/controllers/etuApprovalCtrl.js',
        'financeApprovalCtrl':'assets/js/controllers/financeApprovalCtrl.js',
        'bMatrixColorChangeCtrl':'assets/js/controllers/bMatrixColorChangeCtrl.js',
        'updateETUCtrl':'assets/js/controllers/updateETUCtrl.js',
        'deleteTotalProjectCtrl':'assets/js/controllers/deleteTotalProjectCtrl.js',
        
        'ReportCtrl':'assets/js/controllers/reportCtrl.js',
        'LoginCtrl':'assets/js/controllers/loginCtrl.js',
        'UserProjectSavingReportCtrl':'assets/js/controllers/userProjectSavingReportCtrl.js',
        'ProjectSavingReportCtrl':'assets/js/controllers/projectSavingReportCtrl.js',
        'SkillInventoryReportCtrl':'assets/js/controllers/skillInventoryReportCtrl.js',
        'UserPlantPillarSavingReportCtrl':'assets/js/controllers/UserPlantPillarSavingReportCtrl.js',
        'UserPlantETUSavingReportCtrl':'assets/js/controllers/UserPlantETUSavingReportCtrl.js',
        
        
        'PeoplePerformanceEvaluationReportCtrl':'assets/js/controllers/peoplePerformanceEvaluationReportCtrl.js',
        'HomeCtrl':'assets/js/controllers/homeCtrl.js',
        'HomepageCtrl':'assets/js/controllers/homepageCtrl.js',
        

        //master data
        'MasterDataCtrl':'assets/js/controllers/masterData/masterDataCtrl.js',
        'MachineDataCtrl':'assets/js/controllers/masterData/machineDataCtrl.js',
        'DailyBasisLossCtrl':'assets/js/controllers/masterData/dailyBasisLossCtrl.js',
        'LossReportCtrl':'assets/js/controllers/masterData/lossReportCtrl.js',



        //servicess
        'projectService': 'assets/js/services/projectService.js',
        'userService': 'assets/js/services/userService.js',
        'dashboardService':'assets/js/services/dashboardService.js',
        'matricesService':'assets/js/services/matricesService.js',
        'factorManagementService':'assets/js/services/factorManagementService.js',
        'commonService':'assets/js/services/commonService.js',
        'reportService':'assets/js/services/reportService.js',
        'masterService':'assets/js/services/masterService.js',
        
    },
    //*** angularJS Modules
    modules: [{
        name: 'toaster',
        files: ['../../bower_components/AngularJS-Toaster/toaster.js', '../../bower_components/AngularJS-Toaster/toaster.css']
    }, {
        name: 'angularBootstrapNavTree',
        files: ['../../bower_components/angular-bootstrap-nav-tree/dist/abn_tree_directive.js', '../../bower_components/angular-bootstrap-nav-tree/dist/abn_tree.css']
    }, {
        name: 'ngTable',
        files: ['../../bower_components/ng-table/dist/ng-table.min.js', '../../bower_components/ng-table/dist/ng-table.min.css']
    }, {
        name: 'ui.mask',
        files: ['../../bower_components/angular-ui-mask/dist/mask.min.js']
    }, {
        name: 'ngImgCrop',
        files: ['../../bower_components/ng-img-crop/compile/minified/ng-img-crop.js', '../../bower_components/ng-img-crop/compile/minified/ng-img-crop.css']
    }, {
        name: 'angularFileUpload',
        files: ['../../bower_components/angular-file-upload/dist/angular-file-upload.min.js']
    }, {
        name: 'monospaced.elastic',
        files: ['../../bower_components/angular-elastic/elastic.js']
    }, {
        name: 'ngMap',
        files: ['../../bower_components/ngmap/build/scripts/ng-map.min.js']
    }, {
        name: 'chart.js',
        files: ['../../bower_components/angular-chart.js/dist/angular-chart.min.js']
    }, {
        name: 'flow',
        files: ['../../bower_components/ng-flow/dist/ng-flow-standalone.min.js']
    }, {
        name: 'ckeditor',
        files: ['../../bower_components/angular-ckeditor/angular-ckeditor.min.js']
    }, {
        name: 'mwl.calendar',
        files: ['../../bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js', '../../bower_components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css', 'assets/js/config/config-calendar.js']
    }, {
        name: 'ng-nestable',
        files: ['../../bower_components/angular-nestable/src/angular-nestable.js']
    }, {
        name: 'ngNotify',
        files: ['../../bower_components/ng-notify/dist/ng-notify.min.js', '../../bower_components/ng-notify/dist/ng-notify.min.css']
    }, {
        name: 'xeditable',
        files: ['../../bower_components/angular-xeditable/dist/js/xeditable.min.js', '../../bower_components/angular-xeditable/dist/css/xeditable.css', 'assets/js/config/config-xeditable.js']
    }, {
        name: 'checklist-model',
        files: ['../../bower_components/checklist-model/checklist-model.js']
    }, {
        name: 'ui.knob',
        files: ['../../bower_components/ng-knob/dist/ng-knob.min.js']
    }, {
        name: 'ngAppear',
        files: ['../../bower_components/angular-appear/build/angular-appear.min.js']
    }, {
        name: 'countTo',
        files: ['../../bower_components/angular-filter-count-to/dist/angular-filter-count-to.min.js']
    }, {
        name: 'angularSpectrumColorpicker',
        files: ['../../bower_components/angular-spectrum-colorpicker/dist/angular-spectrum-colorpicker.min.js']
    }]
});