app.controller('UserPlantPillarSavingReportCtrl',['$scope','$rootScope','matricesService','userService','moment',
function($scope,$rootScope,matricesService,userService,moment){
    var userDetails = JSON.parse(localStorage.getItem("UserDetails"));   
    $scope.projectList = [];
    $scope.projectIds = [];
    $scope.pilarSavingList = [];
    $rootScope.lastId;
    $scope.calendarYearList = [];
    $scope.calendarSelectedYear = 2018;
    $scope.pilarSavingListId = [];
    $scope.pilarSavingListActual = [];
    var startYear = 2018;
    while( startYear < 2030) {
        $scope.calendarYearList.push(startYear);
        startYear++;
    }


    var userApproval; 
    var userRole;
    $scope.filterFMatrix = {
        startOfYear : moment().format("YYYY"),
        startOfMonth : "1",
        endOfYear : moment().format("YYYY"),
        endOfMonth : parseInt(moment().format("MM"))+"",
        isEmployee: false
      };

      userApproval = userDetails.approval;
      userRole = userDetails.roles[0].authority;


      var margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 80,
        front: 0,
        back: 0
      };



      var width = 960 - margin.left - margin.right;
      var height = 500 - margin.top - margin.bottom;
      var depth = 100 - margin.front - margin.back;
    
      var xScale = d3.scale.ordinal().rangeRoundBands([0, width], 0.2);
    
      var yScale = d3.scale.linear().range([height, 0]);
    
      var zScale = d3.scale.ordinal()
      .domain([0, 1, 2])
      .rangeRoundBands([0, depth], 0.4);
    
      var xAxis = d3.svg.axis()
        .scale(xScale)
        .orient('bottom');
        
      var yAxis = d3.svg.axis()
        .scale(yScale)
        .orient('left')
        .ticks(10, "Million");
    





    $scope.getFMatricesData= function(){
        $scope.filterFMatrix.isEmployee = userRole;
    matricesService.getFMatricesData($scope.filterFMatrix).then(function(response){
    //console.log(response.data.projectList);
    $scope.projectList = response.data.projectList; 
    for(var i=0;i<$scope.projectList.length;i++){
        $scope.projectIds[i] = $scope.projectList[i].projectId;
    }
    $scope.getPillarSavings($scope.calendarSelectedYear);
    },function(error){
    console.error(error);
    });
    }

    $scope.getPillarSavings = function(year){
        $scope.id;
       
        $scope.savings;
         var x=0;
        matricesService.getPillarSavings(year).then(function(response){
           
            $scope.pilarSavingList = response.data;
            //$scope.pilarSavingList  = $scope.pilarSavingList.map(function(d){ return  d.pillarId ,   d.projectSavings  ? d.projectSavings  / 1000000 : 0 ; }) ;
            console.log($scope.pilarSavingList);
         //   $scope.userPerformenceReportData  = $scope.pilarSavingList.map(function(d){ return   d.pillerName ,    d.projectSavings    ; }) ;
           // loadProjectSavingBarChart($scope.userPerformenceReportData,'Savings In Millions (INR)' , 2 );

          loadProjectSavingBarChart ($scope.pilarSavingList); 
        //    console.log($scope.pilarSavingList.length); 
            for(var i=0;i<$scope.pilarSavingList.length;i++){
                console.log(typeof($scope.pilarSavingList[i].pillarId),"...",typeof($scope.pilarSavingList[i].actualSavings));
                    $scope.pilarSavingListId[i] = $scope.pilarSavingList[i].pillarId;
                    $scope.pilarSavingListActual[i] = $scope.pilarSavingList[i].actualSavings / 1000000;
                    $rootScope.lastId = $scope.pilarSavingListId[i];
                  //  console.log("$scope.pilarSavingListId[i]",$scope.pilarSavingListId[i],"$scope.pilarSavingListActual[i]",$scope.pilarSavingListActual[i]);
                    }
            $scope.setData($scope.pilarSavingListId, $scope.pilarSavingListActual);
            
        },function(error){
            console.log(error);
        });
       
            
    }



    $scope.setData = function(data,values){
        //set data
        for(var i=0;i<data.length;i++){
           console.log(data[i],values[i]); 
        }         
         
    }
    function loadProjectSavingBarChart (data) {
       // console.log("loadProjectSavingBarChart");
       data.sort(GetSortOrder("actualSavings"));
       // console.log(d3);
        d3.select('#userPlantPillarSavingChart').selectAll("*").remove();
        var chart = d3.select('#userPlantPillarSavingChart')
        .append('svg')
        .attr('width',width + margin.left + margin.right)
        .attr('height',height+ margin.top + margin.bottom)
        .append('g')
        .attr('transform',svgHelp.translate(margin.left, margin.right));
        xScale.domain(_.uniq(_.map(data, 'pillerName')));
        yScale.domain([0, _.max(data, 'actualSavings').actualSavings/ 1000000 ]);
        
        function x(d) { return xScale(d.pillerName); }
        function y(d) { return yScale(d.actualSavings/ 1000000 ); }
    
        var camera = [width / 2, height / 2, - 1000];
        var barGen = bar3d()
          .camera(camera)
          .x(x)
          .y(y)
          .z(zScale(0))
          .width(xScale.rangeBand())
          .height(function(d) { return height - y(d); })
          .depth(xScale.rangeBand());
    
        chart.append('g')
          .attr('class', 'x axis')
          .style('font-size', '12px')
          .attr('transform', svgHelp.translate(0, height))
          .call(xAxis);
    
       
        chart.selectAll('g.tick text')
        .style('font-size', '12px')
        .attr('transform', 'rotate(-12)')
        .style('font-weight', '600');
    
            
        var extent = xScale.rangeExtent();
        var middle = (extent[1] - extent[0]) / 2;
        chart.selectAll('g.bar').data(data)
        .enter().append('g')
        .attr('class', 'bar')
        .sort(function(a, b) {
          return Math.abs(x(b) - middle) - Math.abs(x(a) - middle);
        })
        .call(barGen);
    
         chart.append('g')
          .attr('class', 'y axis')
          .style('font-size', '12px')
          .style('font-weight', '600')
          .call(yAxis);
      
          
          chart.append('text')
          .attr('x', -(height / 2) - margin.top)
          .attr('y', - margin.left / 1.4)
          .attr('transform', 'rotate(-90)')
          .attr('text-anchor', 'middle')
          .text('Savings In Millions (INR)');
       
        chart.selectAll("g.bar")
        .data(data)
        .append("text")
        .attr("class", "bar")
        .attr("text-anchor", "middle")
        .attr("x", function(d) { return x(d) + 28; })
        .attr("y", function(d) { return y(d)-10; })
        .text(function(d) { return ( d.actualSavings / 1000000 ).toFixed(2) ; });
      }
      function type(d) {
        d.frequency = +d.frequency;
        return d;
      }
      function GetSortOrder(prop) {  
        return function(a, b) {  
            if (a[prop] < b[prop]) {  
                return 1;  
            } else if (a[prop] > b[prop]) {  
                return -1;  
            }  
            return 0;  
        }  
    }



    



// function loadProjectSavingBarChart(){
//     var element = d3.select('#userPlantPillarSavingChart').node();
//     var width = element.getBoundingClientRect().width - margin.left - margin.right;
//     var height = 500 - margin.top - margin.bottom;
//     var depth = 100 -margin.front - margin.back;
// }

}]);