
app.controller('wcmMethodDataCtrl', ["$rootScope","$scope","commonService",
function($rootScope,$scope,commonService,$projectWCMCtrlInstance) {

     $scope.methodDropdown = ["COMMON METHODS", "SPECIFIC METHODS"];
     $scope.pillarApproachDropdown = ["REACTIVE", "PROACTIVE","PREVENTIVE"];
  
    $scope.addWCMMethod = function(){
      $rootScope.showWCMMethods=false;
      commonService.addWCMMethod($scope.WCMMethodForm).then(function(response){
        $rootScope.mod.dismiss('cancel');
        $rootScope.showWCMMethods=true;   
        $rootScope.getWCMMethods();  
      },
      function(error){
        console.log(error);
      });
    }

    $scope.cancel = function() {
      $rootScope.mod.dismiss('cancel');
    }
    
}]);
