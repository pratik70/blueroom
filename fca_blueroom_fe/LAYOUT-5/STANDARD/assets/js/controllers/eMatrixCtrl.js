app.controller('deleteTotalProjectCtrl', ["$rootScope","$scope", 'matricesService',"commonService","userService","$uibModal",
function($rootScope,$scope,matricesService,commonService,userService, $uibModal) {
  

      
      
      $scope.deleteProject = function(){
        matricesService.deleteProject($rootScope.deleteEMatricesData).then(function(response){

          $rootScope.loadEMatrixGrid();
          $rootScope.mod.dismiss('cancel');
        },
        function(error){
          console.log(error);
        });
      }

  $scope.cancel=function(){
    $rootScope.mod.dismiss('cancel');
  }
  
  
}]);










app.controller('EMatrixCtrl', ['$rootScope',"$uibModal", '$scope', 'matricesService','userService','projectService', 'moment',"$timeout", "$state",
function ($rootScope,$uibModal, $scope ,matricesService,userService,projectService, moment, $timeout, $state) {
  
  
  var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
  var userApproval; 
  var userRole;
  var isProjectHide=true;
  var columnDefs = [];
  $scope.showFilterPanel = true;
  $scope.yearList = [];
  for (var i = 2017;i <= 2050 ;i++)
    $scope.yearList.push(i);
  
  $scope.monthList = 
  [
    { value : 01 , label :  'January' },
    { value : 02 , label :  'February' }, 
    { value : 03 , label :  'March' },
    { value : 04 , label :  'April' }, 
    { value : 05 , label :  'May' }, 
    { value : 06 , label :  'June' },
    { value : 07 , label :  'July' },
    { value : 08 , label :  'August' },
    { value : 09 , label :  'September' },
    { value : 10, label :  'October' },
    { value : 11, label :  'November' },
    { value : 12, label :  'December' }
  ];

  $scope.filterEMatrix = {
    startOfYear : moment().format("YYYY"),
    startOfMonth : "1",
    endOfYear :  moment().format("YYYY"),
    endOfMonth : parseInt(moment().startOf('month').format("MM"))+"",
    isEmployee: false
  };
  $scope.AdminEMatrix = {
    id : '',
    status : ""
    
  };
  $scope.eid;
  $scope.estatus;
  var rowData=[];
  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    animateRows: true,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    defaultColDef : {
      width: 150,
    },
    headerHeight:90,
    rowHeight:50,
    onGridReady: function(params) {
      //params.api.sizeColumnsToFit();
      params.columnApi.autoSizeColumns();
    },
    autoGroupColumnDef: {
      /*<!-- headerName: "Organisation Hierarchy", -->
      <!-- cellRendererParams: { -->
          <!-- suppressCount: true -->
      <!-- } -->*/
    }
  };

  function onFilterTextBoxChanged() {
    gridOptions.api.setQuickFilter(document.getElementById('filter-text-box').value);
  }

  $scope.$on('loadEMatrixGrid', function(e) {  
    //$scope.loadEMatrixGrid();        
  });
  
  $scope.$on('filterEMatrixGrid', function(e) { 
    $scope.showFilterPanel = true;  
    $scope.$parent.showFilterPanel =  true;
  });


  userApproval = userDetails.approval;
  userRole = userDetails.roles[0].authority;


  $rootScope.loadEMatrixGrid = function() {
    $rootScope.loaderAction = true;
      
      //$scope.filterEMatrix.isEmployee = userRole == "EMPLOYEE";
      $scope.filterEMatrix.isEmployee = userRole;
    matricesService.getEMatricesInformation($scope.filterEMatrix).then(function (response) {
      
      clearGrid();
      $scope.$parent.showFilterPanel =  false;
      $scope.showFilterPanel = false;
      columnDefs = getDefaultColumns();
      gridOptions.rowData = response.data.projectList;
      var columns = response.data.totalTeamMemberCount;
      for (var i = 0; i < columns-1 ; i++) {
        columnDefs.push({headerName: "Team Member ("+(i+1) +")" , field:  "team_member_"+(i+1) ,width:120,cellClass:["grid-cell-text-center","wrap-grid-text"]});
      }
      
      columnDefs.push({headerName: "Direct labour" , field:  "direct" ,width:120,cellClass:["grid-cell-text-end"] ,cellRenderer: currencyFormatter});
      columnDefs.push({headerName: "Indirect Labour" , field:  "indirect" ,width:120,cellClass:["grid-cell-text-end"] ,cellRenderer: currencyFormatter});
      columnDefs.push({headerName: "Staff Payroll" , field:  "staff" ,width:120,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter});
      
      columnDefs.push({headerName: "Indirect Materials" , field: "indirectMaterials" ,width:120,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter});
      columnDefs.push({headerName: "Maintenance  Material" , field:  "maintenance" ,width:120,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter});
      columnDefs.push({headerName: "SCRAP" , field:  "scrap" ,width:120,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter});
      
      columnDefs.push({headerName: "Energy" , field: "energy",width:120,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter});
      columnDefs.push({headerName: "GENERAL EXPENSES" , field:  "generalExpenses" ,width:120,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter});
      //columnDefs.push({headerName: "Yearly Potential(365 Days)",field: "yearlyPotential",width:109,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter});
    
      columnDefs.push({headerName: "Project Cost" , field: "totalProjectCost",width:120,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter});
      columnDefs.push({headerName: "CONFIDENCE LEVEL IN %" , field:  "confidenceLevel" ,width:120,cellClass:["grid-cell-text-end"]});
      columnDefs.push({headerName: "B/C",field: "bc",width:109,cellClass:["grid-cell-text-end"]});
      columnDefs.push({ headerName: "PROJECT START DATE",width:100, field: "startDateFormated",cellClass:["grid-cell-text-end"] });
      columnDefs.push({ headerName: "PROJECT TARGET DATE",width:100, field: "targetDateFormated",cellClass:["grid-cell-text-end"]});
      columnDefs.push({headerName: "Plan Vs\n Actual",field: "plan",width:109, cellRenderer: customCellRendererModel });
      
      var month = moment();
      month.month(parseInt($scope.filterEMatrix.startOfMonth)-1);
      month.year($scope.filterEMatrix.startOfYear);
      var headerPlan = getPlanningHeader(month);
      for (var i in headerPlan  ) {
        columnDefs.push({ headerName: headerPlan[i].label, field: headerPlan[i].date, width:170, cellRenderer: customCellRendererPlanModel });
      }
      // columnDefs.push({headerName: "Jun-18",field: "plan_column",width:150, cellRenderer: customCellRendererPlanModel });
     
      /*columnDefs.push({ headerName: "Horizontal  Deployment", field: "horizontalExpansion", width:109 });
      columnDefs.push({ headerName: "Standardisation (SOP etc.)", field: "standardisation", width:109 });
      columnDefs.push({ headerName: "EEM / EPM / MP INFO", field: "epmInfo", width:109 });
      
      */
      columnDefs.push({ headerName: "PDCA STATUS",width:100, field: "gold",cellClass:["grid-cell-text-center"] , cellRenderer : PDCAStatusComponent });
      columnDefs.push({ headerName: "PROJECT CLOSURE DATE",width:100, field: "projectCloserDate", cellClass:["grid-cell-text-center"] });
      if(userRole =="SUPER_ADMIN" || userRole =="SUPER_EMPLOYEE" || userRole =="ADMIN"){
        userApproval="None"
      }

      if(userApproval=="ETU Approval")
      {
      
          columnDefs.push({
             headerName: "ETU Approval",width:100, field: "isETUApproval", cellClass:["grid-cell-text-center","text-hover-uid"],onCellClicked:changeETUApprovalStatusById,
             cellRenderer: function (params) {
              if(params.data.benefitIndentsEmatrix.project.etuStatus){
                return '<button style="border-radius: 10px; " disabled="true">Approval </button>';
              }
              return '<button style="border-radius: 10px;color:white; background-color: #4CAF50;">Approval </button>';
              
          }
        
        });
                                  
      }
       
      else if(userApproval=="Finance Approval" )
      {
            columnDefs.push({ headerName: "Finance Approval",width:100, field: "isFinanceApproval", cellClass:["grid-cell-text-center","text-hover-uid"], onCellClicked:changeFinanceApprovalStatusById,       
            cellRenderer: function (params) {
              if(params.data.benefitIndentsEmatrix.project.financeStatus){
                return '<button style="border-radius: 10px; " disabled="true">Approval </button>';
              }
              return '<button style="border-radius: 10px;color:white; background-color: #4CAF50;">Approval </button>';
         }});            
                      
      }
       
        if(userRole =="SUPER_ADMIN" || userRole =="SUPER_EMPLOYEE" || userRole =="ADMIN" ){
          columnDefs.push({
            headerName: "ETU Approval",width:100, field: "isETUApproval", cellClass:["grid-cell-text-center","text-hover-uid"],onCellClicked:changeETUApprovalStatusById, 
            cellRenderer: function (params) {
             if(params.data.benefitIndentsEmatrix.project.etuStatus){
               return '<button style="border-radius: 10px; " disabled="true"> Approval </button>';
             }
             return '<button style="border-radius: 10px;color:white; background-color: #4CAF50;"> Approval </button>';
              }
              });

              columnDefs.push({ 
                headerName: "Finance Approval",width:100, field: "isFinanceApproval", cellClass:["grid-cell-text-center","text-hover-uid"], onCellClicked:changeFinanceApprovalStatusById,      
                cellRenderer: function (params) {
              if(params.data.benefitIndentsEmatrix.project.financeStatus){
                return '<button style="border-radius: 10px; " disabled="true">Approval </button>';
              }
              return '<button style="border-radius: 10px;color:white; background-color: #4CAF50;"> Approval </button>';
              }});
              if(userRole !="SUPER_EMPLOYEE"){
                columnDefs.push({ headerName: "Hide",width:100, field: "isProjectHide", cellClass:["grid-cell-text-center","text-hover-uid"], cellRenderer: checkboxCellRendererFunc, onCellClicked: changeProjectStatus });
                if(userRole !="ADMIN"){
                  columnDefs.push({ headerName: "Delete",width:100, cellClass:["grid-cell-text-center","text-hover-uid"], cellRenderer: deleteProject,onCellClicked:deleteProjectById });
                  }
              }
        }
      /*
      columnDefs.push({ headerName: "Remark", field: "remark", width:109 });*/
    
     /* columnDefs.push( {
        headerName: 'May-18',
        children: [
            {headerName: '2', columnGroupShow: 'closed', field: 'total', width: 30 },
            {headerName: '3', columnGroupShow: 'closed', field: 'gold', width: 30  },
            {headerName: '4', columnGroupShow: 'closed', field: 'silver', width: 40 },
            {headerName: 'Bronze', columnGroupShow: 'closed', field: 'bronze', width: 40}
        ]
        });*/
      /*columns = response.data.methodColumns;
      for (var key in columns) {
        columnDefs.push({headerName: columns[key] , field:  key ,headerClass:'my-class back-color2',width:60});
      }
      columns = response.data.toolColumns;
      for (var key in columns) {
        columnDefs.push({headerName: columns[key] , field:  key ,headerClass:'my-class back-color2',width:60});
      }*/
      gridOptions.columnDefs = columnDefs;
      $timeout(function() {
        var cMatrixDiv = document.querySelector('#e_matrix');
        new agGrid.Grid(cMatrixDiv, gridOptions);
        $rootScope.loaderAction = false;
      }, 100);

    }, function (error) {
    $rootScope.loaderAction = false;
      
        console.error(error);
    });
  };

  function changeETUApprovalStatusById(gridData){
    if(userRole =="SUPER_ADMIN"){

    $rootScope.etuStatusId = gridData.data.projectId;

    

    $rootScope.loaderAction = true;
    var tools = {};
    $rootScope.mod = $uibModal.open({
       templateUrl : './assets/views/dashboard/etuApproval.html',
       controller : 'etuApprovalCtrl',
       size : 'lg',
       windowClass : 'projectTechnicalToolWindow',
       resolve : {
        data : function (){
          return tools;
        }
      }
     });
     $rootScope.loaderAction = false;
    }
  }

  function changeFinanceApprovalStatusById(gridData){
    if(userRole =="SUPER_ADMIN"){

    $rootScope.financeStatusId = gridData.data.projectId;

    var tools = {};
    $rootScope.mod = $uibModal.open({
       templateUrl : './assets/views/dashboard/financeApproval.html',
       controller : 'financeApprovalCtrl',
       size : 'lg',
       windowClass : 'projectTechnicalToolWindow',
       resolve : {
        data : function (){
          return tools;
        }
      }
     });
     $rootScope.loaderAction = false;
    }
  }

  function deleteProjectById(gridData) {
    if(userRole =="SUPER_ADMIN"){
    $rootScope.deleteEMatricesData=gridData.data;
    
    $rootScope.loaderAction = true;
  
    $rootScope.mod = $uibModal.open({
      templateUrl : './assets/views/dashboard/projectDeletionDialog.html',
      controller : 'deleteTotalProjectCtrl',
      size : 'sm',
      windowClass : 'projectDetailsWindow',
      resolve : {
        
      }
    });

    $rootScope.loaderAction = false;
  }
}
function changeProjectStatus (gridData){
  if(userRole =="SUPER_ADMIN" || userRole =="ADMIN"){
  
 $scope.AdminEMatrix.id=gridData.data.projectId;
 
  matricesService.setProjectStatus($scope.AdminEMatrix).then(function(response){

  },
   function (error) {
      
        console.error(error);
    });
  }
}
  function checkboxCellRendererFunc(data){
   
      if(data.data.benefitIndentsEmatrix.project.projectHide == true)
        return ' <input  type="checkbox" checked />';
      else
        return ' <input  type="checkbox" />';
    
  }

  /*
  function getETUApproval(){
      return '<button style="border-radius: 10px;"> Approval </button>';
    
        
  }

  function getFinanceApproval(){
    
      return '<button style="border-radius: 10px;"> Approval </button>';
     
    
  }*/

  function deleteProject() {
    if(userRole !="ADMIN"){
    return '<button style="border-radius: 10px;" >Delete Project</button>';
    }
}

  function currencyFormatter (params) {//Only needed if you need to do some custom parsing before show value in the UI, i.e: '$' + params.value
    return params.value ? "<span> &#8377;" + parseInt(params.value||0).toLocaleString('en-US', {maximumFractionDigits:0, minimumFractionDigits: 0}) + '</span>' : '';
  }

  function getDefaultColumns() {
      return [
      {headerName:"S.No.",field: "projectId",width:75,backgroundColor:'red',cellClass:["grid-cell-text-center"]},
      {headerName: "PROJECT UIN",field: "projectUIN",width:74,cellClass:["grid-cell-text-center text-hover-uid"], onCellClicked : onCellClicked},
      {headerName: "Loss Type",field: "loss",width:200,cellClass:["grid-cell-text-center","wrap-grid-text"],
        cellRenderer: function (params) {
          if(params.value == "S MATRIX"){
            return "<span style='color:green; font-weight: bold;'>"+ params.value +"  </span>";
          }else if (params.value == "MACHINE SCRAP" || params.value == "MATERIAL SCRAP"){
            return "<span style='color:red;   font-weight: bold;'>"+ params.value +"  </span>";
          }
          return params.value;
      }},// cellClass: 'wrap-grid-text', autoHeight: true},
      {headerName: "ETU",field: "etu",width:70,cellClass:["grid-cell-text-center"]},
    /*  {headerName: "Base Pillar",field: "basePillar",width:100,cellClass:["grid-cell-text-center"]},*/
      {headerName: "Process",field: "process",width:120,cellClass:["grid-cell-text-center"]},
      {headerName:"Op No.",field: "operationNo",width:70,cellClass:["grid-cell-text-center"]},
      {headerName: "Project Description",field: "projectName",width:225,cellClass:["grid-cell-text-center","wrap-grid-text"]},// cellClass: 'wrap-grid-text', autoHeight: true},
      {headerName: "Yearly Potential(365 Days)",field: "yearlyPotential",width:109,cellClass:["grid-cell-text-end"],cellRenderer: currencyFormatter},
      {headerName: "Team Leader",field: "personResponsible",width:100,cellClass:["grid-cell-text-center","wrap-grid-text"]},    
       {headerName: "Team Coach",field: "personCoach",width:100,cellClass:["grid-cell-text-center","wrap-grid-text"]},    
    ];
  }

  function onCellClicked (gridData) {
    if (gridData.data.projectId) {
      
      // $location.path("app/project/"+$scope.projectId+"/8");
      $state.go('app.project',{ projectId : gridData.data.projectId , stage : 1 });
    }
  }

  function customCellRendererModel(params) {}
  
  customCellRendererModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: center;border-bottom: 1px solid #bdc3c7;"> Plan </div>'+
                                '<div class="col-md-12" style="text-align: center;"> Actual </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };

  customCellRendererModel.prototype.getGui = function getGui() {
    return this.eGui;
  };

  function customCellRendererPlanModel(params) {}
  
  customCellRendererPlanModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    //var oddCol = params.colDef.headerName.con"background-color: #fde9d9;"
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    var innerHTMLPlanContent ="",innerHTMLActualContent =""; 
    var wednesdayList = getMonthWednesdays(params.colDef.field);
    var colWidth = "width : "+(100 / wednesdayList.length)+ "%;";
    var remarkList = params.data.remarkList || [];
    var remark ;
    for (var i in wednesdayList ) {
      remark = null;
      var plan = params.data[wednesdayList[i].planColumn] ? params.data[wednesdayList[i].planColumn] : "&nbsp;";
      var actual = params.data[wednesdayList[i].actualColumn] && moment(wednesdayList[i].weekStartDate).isBefore(moment()) ? params.data[wednesdayList[i].actualColumn] : "&nbsp;"; 
      var colColor = plan != '&nbsp;' && actual != '&nbsp;'  && plan != actual ?   "color: red" : "";
      
      if (plan && plan !== '&nbsp;') {
        innerHTMLPlanContent +=  '<td class="text-center blue-plan-status" style="border: 1px solid #bdc3c7;'+ colWidth + '"> '+ plan +'</td>';
      } else {
        innerHTMLPlanContent +=  '<td class="text-center" style="border: 1px solid #bdc3c7;'+ colWidth + '"> '+ plan +'</td>';

      }
      var bkColor = "";
      if (plan != "&nbsp;" && actual != "&nbsp;") {
        if ( plan == actual) {
          bkColor = "green-plan-status";
        } else {
          bkColor = "red-plan-status";
        }
      } else if (actual != "&nbsp;") {
        bkColor = "red-plan-status";
      }
      
      remarkList.some(function(v){
        if (v.weekStartDate == wednesdayList[i].weekStartDate) {
          remark = v.remarkText;
          return true;   
        }
        return false;   
      });
      if (remark &&  actual != "&nbsp;" ) {
        innerHTMLActualContent +=  '<td onmouseleave="hideTooltip()" class="text-center ' + bkColor+ '" style="border: 1px solid #bdc3c7;overflow: hidden;position: relative;'+ colWidth +'"> <span class="remark-present">*</span><a class="btn btn-transparent btn-xs tooltips" style="color: white;" onmouseenter="showTooltip(\''+ remark+'\')">'+ actual +'</a> </td>';
      } else {
        innerHTMLActualContent +=  '<td class="text-center ' + bkColor+ '" style="border: 1px solid #bdc3c7;'+ colWidth  + '"> '+ actual +'</td>';
      }
    } 
  //var div = '<span style="margin:7px;" onmouseleave="hideTooltip();"><span><img  id="tooltip-costtable"  onmouseenter="val" style="width: 20px; height:20px;vertical-align: -webkit-baseline-middle;" src="' + icon + '"></span> '+ value + '</span>' ;
    resultElement.innerHTML = '<table style="width:100%;">'+ 
                                '<tbody style="width:100%;">'+  
                                '<tr class="row" style="border: 1px solid #bdc3c7;">'+  
                                  innerHTMLPlanContent +
                              //    '<td "><a href="#" class="btn btn-transparent btn-xs tooltips" onmouseenter="showTooltip()"><i class="fa fa-share"></i></a></td>'+
                                '</tr>'+
                                '<tr class="row" style="border: 1px solid #bdc3c7;">'+  
                                  innerHTMLActualContent+
                                '</tr>'+
                                '</tbody>'+
                              '</table>';                          
    this.eGui = resultElement;
  };
  customCellRendererPlanModel.prototype.getGui = function getGui() {
    return this.eGui;
  };
  $scope.tooltipData = null;
  $scope.showTooltipData = function(params) {
    $scope.tooltipData = params;
    var tooltipSpan = document.getElementById('costGrid-tooltip');
    window.onmousemove = function (e) {
      var x = e.clientX,
       y = e.clientY;
         tooltipSpan.style.top = (y-70) + 'px';
         tooltipSpan.style.left = (x-73) + 'px';
      };
      $timeout(function() {
        var a = document.getElementById('costGrid-tooltip');
        a.style.display = 'block';
        a.children[0].style.visibility = "visible";
        a.children[0].innerHTML = params;
      },10);
  };

  $scope.hideTooltipData = function() {
     $timeout(function(){
          //$('#costGrid-tooltip').hide();
          var a = document.getElementById('costGrid-tooltip');
          a.style.display = 'none';
          a.children[0].style.visibility = "hidden";
     },4);
  };

  function getPlanningHeader(startDate) {
      var plans = []; 
      var month = moment(startDate);
      var endDate = moment(month);
      endDate.add(1,'year');
      // if ( !endDate.isSame(moment()) && endDate.isAfter(moment())) {
      //   endDate = moment().startOf('month').day("Monday");
      //   endDate.add(1,'month');
      // }
      while( month < endDate ) {
          plans.push({
            label : month.format('MMM-YY'),
            date : month.format('MM-DD-YYYY')
          });
          month.add(1, "month");
      }
      return plans;
  };
// PLAN_16_JUL_18
// ACTUAL_20_AUG_18
  function getMonthWednesdays (date) {
    
    var wednesday = moment(date).startOf('month').day("Wednesday");
    if ( wednesday.date() > 7 ) wednesday.add(7,'d');
    var month = wednesday.month();
    var wednesdayList = [];
    var count = 1;
    while(month === wednesday.month()) {
      var data = {
        planColumn  :  ("plan_" + moment(wednesday).format('DD_MMM_YY')).toUpperCase(),
        actualColumn:  ("actual_" +moment(wednesday).format('DD_MMM_YY')).toUpperCase(),
        weekStartDate :moment(wednesday).format('MM-DD-YYYY')
      };
      count++;
      wednesday.add(7,'d');
      wednesdayList.push(data);
    }
    return wednesdayList;
  }

  $scope.filterData = function () {
    $scope.loadEMatrixGrid();
  }; 
  $scope.openFilterPanel = function () {
    $scope.showFilterPanel = !$scope.showFilterPanel;
  }; 
  
  function clearGrid () {
    var myNode = document.getElementById("e_matrix");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
  }


   function PDCAStatusComponent() {
  }

 PDCAStatusComponent.prototype.init = function (params) {
    var executionStarted = params.data.executionStarted;
    var planningData  = params.data.benefitIndentsEmatrix;
    var pStage ='',cStage ='',dStage ='',aStage ='';
    if (  executionStarted ) {
      // if ( planningData.aActualEndDate && isDateSameOrBefore(moment(planningData.aActualEndDate), moment(planningData.pPlannedEndDate))  ){
      //   pStage ='active';dStage ='active';cStage ='active';aStage ='active';
      // } else {
      //   if ( planningData.cActualEndDate && isDateSameOrBefore(moment(planningData.cActualEndDate) ,moment(planningData.cPlannedEndDate) ) ){
      //     pStage ='active';dStage ='active';cStage ='active-org';
      //   } else {
      //     if ( planningData.dActualEndDate && isDateSameOrBefore( moment(planningData.dActualEndDate) , moment(planningData.dPlannedEndDate) ) ){
      //       pStage ='active';dStage ='active-org';
      //     } else {
      //       if ( planningData.pActualEndDate && isDateSameOrBefore(moment(planningData.pActualEndDate) , moment(planningData.pPlannedEndDate) ) ){
      //         pStage ='active-org';
      //       }
      //     }
      //   }
      // }
      if (planningData.aActualEndDate){
        var aPlannedEndDate = Date.parse(planningData.aPlannedEndDate);
        var aActualEndDate = Date.parse(planningData.aActualEndDate);
        if (aPlannedEndDate <= aActualEndDate) {
          pStage ='active';dStage ='active';cStage ='active';aStage ='active';
        } else {
          pStage ='active';dStage ='active';cStage ='active';aStage ='active-org';
        }
        
      } else {
        if (planningData.cActualEndDate) {
          pStage ='active';dStage ='active';cStage ='active-org';
        } else {
          if (planningData.dActualEndDate) {
            pStage ='active';dStage ='active-org';
          } else {
            if (planningData.pActualEndDate) {
              pStage ='active-org';
            }
          }
        }
      }
    } else {
      pStage ='';dStage ='';cStage ='';aStage ='';
    } 
    
   
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: 4px;display: flex; justify-content: center;"); 
    resultElement.innerHTML = '<div class="circ">'+
      '<div class="sect first ' + dStage + '"><span class="text"> D </span></div>'+
      '<div class="sect ' + cStage + '"><span class="text"> C </span></div>'+
      '<div class="sect ' + pStage + '"><span class="text"> P </span></div>'+
      '<div class="sect ' + aStage + '"><span class="text"> A </span></div>'+
      '</div>'; 
      // resultElement.innerHTML = '<table>'+ 
      // '<tr> <td> P</td><td> D</td></tr>'+
      // '<tr> <td> A</td><td> C</td></tr>'+
      // '</table>';  
    this.eGui = resultElement;
  };

  PDCAStatusComponent.prototype.getGui = function getGui() {
    return this.eGui;
  };

 /* if ($scope.currentStep ==3) {
    $scope.loadEMatrixGrid();
  }*/
  
  
}]);