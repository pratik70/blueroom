app.controller('wcmToolUpdateCtrl', ["$rootScope","$scope","commonService","userService","$uibModal",
function($rootScope,$scope,commonService,userService, $uibModal) {
  $scope.toolDropdown = ["COMMON TOOLS", "SPECIFIC TOOLS"];
  $scope.pillarApproachDropdown = ["REACTIVE", "PROACTIVE","PREVENTIVE"];   
    $scope.WCMToolUpdateForm=$rootScope.gridItems.data;
    $scope.WCMToolUpdateFormId = $rootScope.gridItems.data.id;
    $scope.WCMToolUpdateFormLevelFirstTool = $rootScope.gridItems.data.levelFirstTool;
    $scope.WCMToolUpdateFormLevelSecondTool = $rootScope.gridItems.data.levelSecondTool;
    $scope.WCMToolUpdateFormReprortPriority = $rootScope.gridItems.data.reprortPriority;
    $scope.WCMToolUpdateFormType = $rootScope.gridItems.data.type; 
    $scope.WCMMethodUpdateFormTypePillar_approach = $rootScope.gridItems.data.pillar_approach;
  
    $scope.updateWCMTool = function(){
      var rawData = {
          id : $scope.WCMToolUpdateFormId ? $scope.WCMToolUpdateFormId: null,
          levelFirstTool : $scope.WCMToolUpdateFormLevelFirstTool ? $scope.WCMToolUpdateFormLevelFirstTool :null,
          levelSecondTool : $scope.WCMToolUpdateFormLevelSecondTool ? $scope.WCMToolUpdateFormLevelSecondTool : null,
          reprortPriority : $scope.WCMToolUpdateFormReprortPriority ? $scope.WCMToolUpdateFormReprortPriority : null,
          type : $scope.WCMToolUpdateFormType ? $scope.WCMToolUpdateFormType : null,
          pillar_approach : $scope.WCMMethodUpdateFormTypePillar_approach ? $scope.WCMMethodUpdateFormTypePillar_approach :null
      }
      $rootScope.showWCMTools=false;
      commonService.updateWCMTool($scope.WCMToolUpdateFormId,rawData).then(function(response){
        $rootScope.mod.dismiss('cancel');
        $rootScope.showLossTypes=false;
        $rootScope.showEmployee=false;
        $rootScope.showWCMTools=false;
        $rootScope.getWCMtools();
      },
      function(error){
        console.log(error);
      });
    }

    $scope.cancel = function() {
      $rootScope.mod.dismiss('cancel');
    }

}]);