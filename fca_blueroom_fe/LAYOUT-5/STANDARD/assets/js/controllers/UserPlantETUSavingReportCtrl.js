app.controller('UserPlantETUSavingReportCtrl',['$scope','$rootScope','matricesService','userService','moment',
function($scope,$rootScope,matricesService,userService,moment){
    var userDetails = JSON.parse(localStorage.getItem("UserDetails"));  
    $scope.calendarYearList = [];
    $scope.calendarSelectedYear = 2018;
    $scope.etuSavingList = [];
    $scope.projectList = [];
    $scope.projectIds = [];

    var startYear = 2018;
    while( startYear < 2030) {
        $scope.calendarYearList.push(startYear);
        startYear++;
    }
    var userApproval; 
    var userRole;
    
    $scope.filterFMatrix = {
        startOfYear : moment().format("YYYY"),
        startOfMonth : "1",
        endOfYear : moment().format("YYYY"),
        endOfMonth : parseInt(moment().format("MM"))+"",
        isEmployee: false
      };

      var margin = {
          top: 20,
          bottom:30,
          left:80,
          right:20,
          front:0,
          back:0
      };
      var width = 960 - margin.left - margin.right;
      var height = 500 - margin.top - margin.bottom;
      var depth = 100 - margin.front - margin.back;
      
      var xScale = d3.scale.ordinal().rangeRoundBands([0,width],0.2);

      var yScale = d3.scale.linear().range([height,0]);

      var zScale = d3.scale.ordinal()
      .domain([0,1,2])
      .rangeRoundBands([0,depth],0.4);

      var xAxis = d3.svg.axis()
      .scale(xScale)
      .orient('bottom');

      var yAxis = d3.svg.axis()
      .scale(yScale)
      .orient('left')
      .ticks(10,"Million");


      userApproval = userDetails.approval;
      userRole = userDetails.roles[0].authority;


      $scope.getFMatricesData= function(){
        $scope.getETUSavings($scope.calendarSelectedYear);
        // $scope.filterFMatrix.isEmployee = userRole;
        // matricesService.getFMatricesData($scope.filterFMatrix).then(function(response){
        
        // $scope.projectList = response.data.projectList; 
        // for(var i=0;i<$scope.projectList.length;i++){
        //     $scope.projectIds[i] = $scope.projectList[i].projectId;

        // }

        // },function(error){
        // console.error(error);
        // });
        }

        $scope.getETUSavings = function(year){
            
            matricesService.getETUSavings(year).then(function(response){
                $scope.etuSavingList = response.data;
                loadProjectSavingBarChart($scope.etuSavingList);
            },function(error){
                console.log(error);
            });
        };

        function loadProjectSavingBarChart(data){
            data.sort(GetSortOrder("actualSavings"));
            d3.select('#userPlantETUSavingChart').selectAll("*").remove();
            var chart = d3.select('#userPlantETUSavingChart')
            .append('svg')
            .attr('width',width + margin.left + margin.right)
            .attr('height',height+ margin.top + margin.bottom)
            .append('g')
            .attr('transform',svgHelp.translate(margin.left , margin.right));
            xScale.domain(_.uniq(_.map(data,'etu_name')));
            yScale.domain([0,_.max(data,'actualSavings').actualSavings/ 1000000]);

            function x(d) { return xScale(d.etu_name); }
            function y(d) { return yScale(d.actualSavings / 1000000); }
        

        var camera = [width / 2, height / 2, - 1000];
        var barGen = bar3d()
          .camera(camera)
          .x(x)
          .y(y)
          .z(zScale(0))
          .width(xScale.rangeBand())
          .height(function(d) { return height - y(d); })
          .depth(xScale.rangeBand());
    
        chart.append('g')
          .attr('class', 'x axis')
          .style('font-size', '12px')
          .attr('transform', svgHelp.translate(0, height))
          .call(xAxis);
    
       
        chart.selectAll('g.tick text')
        .style('font-size', '12px')
        .attr('transform', 'rotate(-12)')
        .style('font-weight', '600');
    
            
        var extent = xScale.rangeExtent();
        var middle = (extent[1] - extent[0]) / 2;
        chart.selectAll('g.bar').data(data)
        .enter().append('g')
        .attr('class', 'bar')
        .sort(function(a, b) {
          return Math.abs(x(b) - middle) - Math.abs(x(a) - middle);
        })
        .call(barGen);
    
         chart.append('g')
          .attr('class', 'y axis')
          .style('font-size', '12px')
          .style('font-weight', '600')
          .call(yAxis);
      
          
          chart.append('text')
          .attr('x', -(height / 2) - margin.top)
          .attr('y', - margin.left / 1.4)
          .attr('transform', 'rotate(-90)')
          .attr('text-anchor', 'middle')
          .text('Savings In Millions (INR)');
       
        chart.selectAll("g.bar")
        .data(data)
        .append("text")
        .attr("class", "bar")
        .attr("text-anchor", "middle")
        .attr("x", function(d) { return x(d) + 28; })
        .attr("y", function(d) { return y(d)-10; })
        .text(function(d) { return ( d.actualSavings / 1000000 ).toFixed(2) ; });

        
        
        } 



        function GetSortOrder(prop){
            return function(a,b){
                if(a[prop] < b[prop]){
                    return 1;
                }else if(a[prop] > b[prop]){
                    return -1;
                }else{
                    return 0;
                }
            }
        }

    
}]);