app.controller('CMatrixCtrl', ['$rootScope', '$scope','matricesService', 'moment',
function ($rootScope, $scope, matricesService,moment) {

  
  var rowData=[];

  var columnDefs =[
    {headerName: "Date",field: "dataDate",width:100,cellClass:["grid-cell-text-center"]},
    {headerName: "Loss Type",field: "lossType",width:200,cellClass:["grid-cell-text-center"]},// cellClass: 'cell-wrap-text', autoHeight: true},
    {headerName: "ETU",field: "etu",width:70,cellClass:["grid-cell-text-center"]},
    {headerName: "Process",field: "process",width:90,cellClass:["grid-cell-text-center"]},
    {headerName:"Op No.",field: "machine",width:100,cellClass:["grid-cell-text-center"]},
    {headerName: "Description",field: "description", cellClass:["grid-cell-text-center-hrz"]},// cellClass: 'cell-wrap-text', autoHeight: true},
    {headerName: "Total Loss Value",field: "totalLossValue",width:140,cellClass:["grid-cell-text-center"]},
   
    {headerName: "Direct Labour",field: "directLabour",width:140,cellClass:["grid-cell-text-center"]},
    {headerName: "Indirect Labour",field: "indirectLabour",width:140,cellClass:["grid-cell-text-center"]},
    {headerName: "Salaried Payroll",field: "salariedPayroll",width:140,cellClass:["grid-cell-text-center"]},
    {headerName: "Indirect Material",field: "indirectMaterial",width:140,cellClass:["grid-cell-text-center"]},
    {headerName: "Manintenance Material",field: "manintenanceMaterial",width:140,cellClass:["grid-cell-text-center"]},
    {headerName: "scrap",field: "scrap",width:140,cellClass:["grid-cell-text-center"]},
    {headerName: "energy",field: "energy",width:140,cellClass:["grid-cell-text-center"]},
    {headerName: "Other Services",field: "otherServices",width:140,cellClass:["grid-cell-text-center"]}
  ];


  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    groupHeaderHeight:40,
    headerHeight:70,
    rowHeight:40,
    onGridReady: function(params) {
      //params.api.sizeColumnsToFit();
      params.columnApi.autoSizeColumns();
    }
  };
  
  if (localStorage.getItem("ETU_value") ) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem("ETU_value"));
  } else {
    $scope.selectedETU = $rootScope.selectedETU || {};
  }
  $scope.filterForm = {
    etuType : $scope.selectedETU.id,
    year : moment().format("YYYY"),
    month :  parseInt(moment().format("MM"))+""
  };
  
  $scope.cMatrixPanel = "C Matrix Loss Distribution";
  $scope.allYearList = [];
  for (let i = 1900 ; i < 2999;i++ ){
    $scope.allYearList.push(i);
  }
  $scope.allMonthList = [
  { value : 01 , label :  'January' },
  { value : 02 , label :  'February' }, 
  { value : 03 , label :  'March' },
  { value : 04 , label :  'April' }, 
  { value : 05 , label :  'May' }, 
  { value : 06 , label :  'June' },
  { value : 07 , label :  'July' },
  { value : 08 , label :  'August' },
  { value : 09 , label :  'September' },
  { value : 10, label :  'October' },
  { value : 11, label :  'November' },
  { value : 12, label :  'December' }];

  $scope.$on('loadCMatrixGrid', function(e) {  
      $scope.loadCMatrixGrid();        
  });

  var colorArray = ['#53b35e', '#7ade77', '#84cee1', '#e1bb84', '#e1dc84', '#829adf', '#e7e7a4', '#e06668', '#a57451', '#da77de', '#e6a2a2', '#e1bb84', '#da77de', '#e6a2a2', '#e1bb84', '#57D3D2'];
  var count = -1;
  
  // function getAllETUType() {
  //       matricesService.getAllETUType().then(function (response) {
  //           $scope.etuTypeList  = response.data;
  //           console.log( $scope.etuTypeList);
  //       }, function (error) {
  //           console.error(error);
  //       });
  //   }

  if ( $rootScope.selectedETU ) {
    $scope.selectedETU = $rootScope.selectedETU ;
  }   else if ( localStorage.getItem( "ETU_value")) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem( "ETU_value"));
  } else {
    $scope.selectedETU = {};
  }

  if ($rootScope.selectedCD) {
    $scope.selectedCD = $rootScope.selectedCD ;
  } else if ( localStorage.getItem( "CD_value")) {
    $scope.selectedCD =  JSON.parse(localStorage.getItem( "CD_value"));
  } else {
    $scope.selectedCD = {};
  }

  $scope.getImage =function(imageFor){
      $scope.displayingImage = imageFor;
      $scope.panelTitle = imageFor;
      $scope.imageUrl = $rootScope.baseURL + "/layout/" + $scope.selectedETU.id + "/" + $scope.selectedCD.id + "/" + imageFor +"?t="+(new Date()).getTime();
  };
  
  $scope.changeTab = function(index) {
      $('input:radio[name=graphImage]').attr('checked',false);
      if(index == 0){ 
        setTimeout(function(){
          $("#lossDistribution").prop("checked" ,true);
          $("#pareto").prop("checked" ,false);
          $scope.displayingImage = "Plant C Matrix Loss Distribution";
        },100);
        $scope.getImage("Plant C Matrix Loss Distribution");
      }
      if(index == 1){
        setTimeout(function(){
          $("#lossDistribution1").prop("checked" ,true);
          $("#pareto1").prop("checked" ,false);
          $scope.displayingImage = "ETU C Matrix Loss Distribution";
        },100);
        $scope.getImage("ETU C Matrix Loss Distribution");
      }
      if(index == 2){
         $scope.$broadcast('loadCMatrixGrid'); 
      }
  };

  $scope.changeTab(0);

  $scope.radioButtonClicked = function(data){
    if(data != $scope.displayingImage){
      $scope.displayingImage = data;
      $scope.getImage(data);
    }
  };

  $scope.loadCMatrixGrid = function() {
    $rootScope.loaderAction = true;
    var myNode = document.getElementById("c-matrix-dist");
    if ( myNode )
      while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
      }
    $scope.filterForm.etuType = $scope.selectedETU.id;
    matricesService.getCMatricesInformation($scope.filterForm).then(function (response) {
      var data = response.data || [];
      gridOptions.rowData = data;
      var cMatrixDiv = document.querySelector('#c-matrix-dist');
      new agGrid.Grid(cMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, function (error) {
        console.error(error);
    });
  };

  $scope.getCMatricesParetoData = function() {
    $rootScope.loaderAction = true;
    $scope.filterForm.etuType = $scope.selectedETU.id;
    matricesService.getCMatricesParetoData($scope.filterForm).then(function (response) {
      var data = response.data || [];
      data = data.sort(function (a, b) { return  b.totalLossValue - a.totalLossValue; });
      data = data.map(function (a) { 
        if (a.totalLossValue > 0 ) {
          a.totalLossValue = parseInt(a.totalLossValue/1000);
          return a;
        } else {
          return a;
        } 
      });
      var data2 = _.filter( data , function(value, key) {
            return value.totalLossValue > 0;
      });
      renderChart(data2);
      $rootScope.loaderAction = false;
    }, function (error) {
        console.error(error);
    });
  };

  function renderChart(data) {
    var element = d3.select('svg').node();
    var margin = {top: 20, right: 20, bottom: 100, left: 70},
    width = element.getBoundingClientRect().width - margin.left - margin.right,
    height = element.getBoundingClientRect().height - margin.top - margin.bottom;
    
    var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);
    var y = d3.scale.linear().range([height, 0]);
    var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

    var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(10);
    d3.select("svg").selectAll("*").remove();
    var svg = d3.select("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    x.domain(data.map(function(d) { return d.lossType.length > 16 ? d.lossType.substring(0, 15) + '...' : d.lossType; }));
    //x.domain(data.map(function(d) { return d.lossType; }));
    y.domain([0, d3.max(data, function(d) { return d.totalLossValue; })]);
   /* svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", "-.55em")
      .attr("transform", "rotate(-65)");
     */
    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll("text")
       .style("font-size", "11px")
      .attr("y", 0)
      .attr("x", -6)

      .attr("dy", ".35em")
      //.attr("transform", "rotate(90)")
      .attr("transform", "rotate(-90)")
      .style("font-size", "11px")
      .style("text-anchor", "end")
      .style( "text-transform", "uppercase");
      var tooltip = d3.select("body").append("div").attr("class", "toolTip");

      svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", -45)
        .attr("x", -( (height - margin.bottom ) /2)-10)
       /* .attr("dy", ".71em")*/
        .style("text-anchor", "end")
        .style("font-size", "14px")
        .style("font-weight", 550)
        .text("Values In K- INR");
      svg.selectAll("bar")
        .data(data)
        .enter().append("rect")
        .attr("x", function(d) {return x(d.lossType.length > 16 ? d.lossType.substring(0, 15) + '...' : d.lossType)})
        .attr("y", function(d) { return y(d.totalLossValue); })
        .attr("width", x.rangeBand())
        //.attr("width", 40)
        .attr("rx", 4)
        .attr("ry", 4)
        .transition()
        .duration(750)
        .style("fill", function(){
          count++;
          if (colorArray.length <= count)
            count = 0; 
          return colorArray[count];
        })
        .attr("height", function(d) { return height  - y(d.totalLossValue) ;  })
        
      svg.selectAll(".text")     
        .data(data)
        .enter()
        .append("text")
        .attr("class","label1")
        .style("font-size", "11px")
        .attr("x", (function(d) { return x(d.lossType.length > 16 ? d.lossType.substring(0, 15) + '...' : d.lossType);   }  ))
        .attr("y", function(d) {  return y(d.totalLossValue) ;  })
        .attr("dy", "-0.75em")
        .text(function(d) { return d.totalLossValue; });      
        /*.on("mousemove", function(d){
            tooltip
              .style("left", d3.event.pageX - 50 + "px")
              .style("top", d3.event.pageY - 70 + "px")
              .style("display", "inline-block")
              .html((d.area) + "<br>" + "£" + (d.value));
        })
        .on("mouseout", function(d){ tooltip.style("display", "none");})*/;
        /*svg.selectAll("g.rect")
        .data(data)
        .append("text")
        .attr("class", "bar")
        .attr("text-anchor", "middle")
         .style('font-size', '12px')
          .style('font-weight', '600')
        .attr("x", function(d) { return x(d) + 28; })
        .attr("y", function(d) { return ( y(d) - 20 ) < 0 ?  0 : y(d) - 20 ; })
        .text(function(d) { return ( d.totalLossValue ) ; });*/
      /*  svg.selectAll("rect")
        .data(data)
        .enter().append("text")
        .text(function(d) {return d})
        .attr("x", function(d, i) {return (i * 60) + 25})
        .attr("y", function(d, i) {return 400 - (d * 10)}); */
  }

  $scope.filterData = function(data) {
    if ($scope.cMatrixPanel == "C Matrix Loss Distribution" ) {
      $scope.loadCMatrixGrid();
    } else {
      $scope.getCMatricesParetoData();
    }
  };


  $scope.loadCMatrixFilterCheck = function ( option ) {
    $scope.cMatrixPanel = option;
    if (option == "C Matrix Loss Distribution" ) {
      $scope.loadCMatrixGrid();
    } else {
      $scope.getCMatricesParetoData();
    }
  };
  // $scope.loadCMatrixPereto = function() {
  //   console.log(" cMatrixPanel :: ", $scope.cMatrixPanel);
  //   $scope.getCMatricesParetoData();
  // };

}]);