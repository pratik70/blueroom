
app.controller('employeeFormCtrl', ["$rootScope","$scope","$uibModal","$uibModalInstance","projectService", "data","userService", '$timeout','projectService', 'moment',
function($rootScope,$scope, $uibModal,$uibModalInstance,projectService, data,userService, $timeout, moment) {

  var filename;
  var filepath;
  $scope.employeeData = data;
  $scope.employeeData.enabled = $scope.employeeData.enabled ? "true" : "false" ;

  // if($scope.employeeData.profileImg){
  //   userService.getProfile($scope.employeeData.profileImg).then(function(response){
  //     $scope.userProfilePicture = "http://localhost:7077/blueroom/api/user/download-image/1becefcd-aff1-4a52-822f-39392ad92a06%20(1).jpg";
  //   });
  // }

  $scope.roles = [];
  $scope.rolesObj = [];
  $scope.allPillars = [];
  $scope.allPillarsObj = [];
  $scope.allETUs = [];
  $scope.allETUsObj = [];
  $scope.errorMessage = "";

  $scope.update=function(){
    $rootScope.showEmployee=false;
   
  var f = document.getElementById('emp_profile_img').files[0],
        r = new FileReader();

    r.onloadend = function(e) {
      var data = e.target.result;
    
    }

    if(f) {
      $rootScope.loaderAction = true;
      userService.uploadfile(f).then(function(response){
        if(response.fileName!=null){
          $scope.employeeData['profileImg'] = response.fileName;
          updateUserData();
        }
      },function(error){
        console.log(error);
      });
    } else {
      updateUserData();
    }
  }

  function updateUserData() {
    for(var i=0;i<$scope.rolesObj.length;i++){
      if($scope.employeeData.roles != null){
          if($scope.rolesObj[i].name == $scope.employeeData.roles.name){
            $scope.employeeData.roles = [$scope.rolesObj[i]];
          }
      }
    }
    var j=0;
     for(var i=0;i<$scope.allPillarsObj.length;i++){
      if($scope.allPillarsObj[i].pillerName == $scope.employeeData.pillarList[j]){
        $scope.employeeData.pillarList[j] = $scope.allPillarsObj[i];
        j++;
      }
    }
     var j=0;
     for(var i=0;i<$scope.allETUsObj.length;i++){
      if($scope.allETUsObj[i].etuName == $scope.employeeData.etuList[j]){
        $scope.employeeData.etuList[j] = $scope.allETUsObj[i];
        j++;
      }
    }
    $rootScope.loaderAction = true;
    if( $scope.employeeData.etuList){
      
      for(var i=0;i < $scope.employeeData.etuList.length; i++){
          if (typeof $scope.employeeData.etuList[i] =="string"){
            $scope.employeeData.etuList[i] = JSON.parse($scope.employeeData.etuList[i]);
          };
      }
    }
    if( $scope.employeeData.pillarList){
      for(var i=0; i < $scope.employeeData.pillarList.length; i++){
          if (typeof $scope.employeeData.pillarList[i] =="string"){
            $scope.employeeData.pillarList[i] = JSON.parse($scope.employeeData.pillarList[i]);
          };
      }
    } 


    if($scope.employeeData.roles && $scope.employeeData.roles.length > 0){   
        if (typeof $scope.employeeData.roles =="string"){
          var data = JSON.parse($scope.employeeData.roles);
          $scope.employeeData.roles = [];
          $scope.employeeData.roles[0] = data;
        }
    }else{
      for(var i=0; i < $scope.roles.length; i++){
        if($scope.roles[i].name == 'EMPLOYEE'){
          $scope.employeeData.roles[0] = $scope.roles[i];
          break;
        }
      }
    }
    
    delete $scope.employeeData["authorities"];
    $scope.employeeData.enabled = $scope.employeeData.enabled == "true" ? true : false ;
    userService.updateUser($scope.employeeData).then(function(response){
        $rootScope.loaderAction = false;
        if(response.code == "200"){
          $uibModalInstance.dismiss('cancel');
          $rootScope.getEmployees();
        }else{
          $scope.errorMessage = response.message;
        }
    },
    function(error){
      console.log(error);
      $rootScope.loaderAction = false;
    });
  }

  $scope.getAllETUType = function () {
      userService.getAllETUType().then(function (response) {
           if(response!=null){
          $scope.allETUsObj = response.data;
          for(var i=0;i<response.data.length;i++){
              $scope.allETUs[i]  = response.data[i].etuName;
          }
        }
      }, function (error) {
          console.error(error);
      });
  }

  $scope.getAllRoles = function () {
      userService.getAllRoles().then(function (response) {
         if(response!=null){
          $scope.rolesObj = response;
          for(var i=0;i<response.length;i++){
              $scope.roles[i]  = response[i].name;
          }
        }
          else {
             $scope.roles =[];
          }
      }, function (error) {
          console.error(error);
      });
  }

  $scope.getAllPillars = function() {
    projectService.getAllPillars().then(function (response) {
        if(response!=null){
          $scope.allPillarsObj = response.data;
          for(var i=0;i<response.data.length;i++){
              $scope.allPillars[i]  = response.data[i].pillerName;
          }
        }
    }, function (error) {
        console.error(error);
    });
  }

$scope.saveUser=function(){
  $rootScope.showEmployee = false;
  
  var f = document.getElementById('emp_profile_img').files[0],
        r = new FileReader();
    r.onloadend = function(e) {
      var data = e.target.result;
    }
    if(f) {
      userService.uploadfile(f).then(function(response){
        if(response.fileName != null){
          $scope.newEmployee['profileImg'] = response.fileName;
          saveUserData();
        }
      },function(error){
        $rootScope.loaderAction = false;
        console.log(error);
      });
    }else {
      saveUserData();
    }
    
  }

  function saveUserData() {

    for(var i=0;i<$scope.rolesObj.length;i++){
      if($scope.newEmployee.roles != null){
          if($scope.rolesObj[i].name == $scope.newEmployee.roles){
            $scope.newEmployee.roles = [$scope.rolesObj[i]];
          }
      }
    }
    var j=0;
     for(var i=0;i<$scope.allPillarsObj.length;i++){
      if($scope.allPillarsObj[i].pillerName == $scope.newEmployee.pillarList[j]){
        $scope.newEmployee.pillarList[j] = $scope.allPillarsObj[i];
        j++;
      }
    }
     var j=0;
     for(var i=0;i<$scope.allETUsObj.length;i++){
      if($scope.allETUsObj[i].etuName == $scope.newEmployee.etuList[j]){
        $scope.newEmployee.etuList[j] = $scope.allETUsObj[i];
        j++;
      }
    }
    
    
    $rootScope.loaderAction = true;
     
    if($scope.newEmployee.etuList){
      for(var i=0;i < $scope.newEmployee.etuList.length; i++){
        if (typeof $scope.newEmployee.etuList[i] =="string"){
          $scope.newEmployee.etuList[i] = JSON.parse($scope.newEmployee.etuList[i]);
        };
      }
    }

    if($scope.newEmployee.pillarList) {
      for(var i=0;i < $scope.newEmployee.pillarList.length; i++){
        if (typeof $scope.newEmployee.pillarList[i] =="string"){
          $scope.newEmployee.pillarList[i] = JSON.parse($scope.newEmployee.pillarList[i]);
        };
      } 
    }   
      
    if($scope.newEmployee.roles && $scope.newEmployee.roles.length > 0){   
        if (typeof $scope.newEmployee.roles =="string"){
          var data = JSON.parse($scope.newEmployee.roles);
          $scope.newEmployee.roles = [];
          $scope.newEmployee.roles[0] = data;
        }
    } else {
      for(var i=0; i < $scope.roles.length; i++){
        if($scope.roles[i].name == 'EMPLOYEE'){
          $scope.newEmployee.roles = [];
          $scope.newEmployee.roles[0] = $scope.roles[i];
          break;
        }
      }
    }
    
    $scope.errorMessage = "";  
    userService.createUser($scope.newEmployee).then(function(response){
        $rootScope.loaderAction = false;
        if(response.code == "200"){
          $uibModalInstance.dismiss('cancel');
          $rootScope.getEmployees();
        }else{
          $scope.errorMessage = response.message;
        }
    },
    function(error){
      $rootScope.loaderAction = false;
      console.log(error);
    });
  }

  $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
  }

}]);


app.controller('GetUsersCtrl', ['$rootScope', '$scope',"$uibModal","projectService",'matricesService','userService',"$timeout",
function ($rootScope, $scope,$uibModal,projectService, matricesService, userService,  $timeout ) {
  
  var columnDefs = [];
  
  $scope.yearList = [];
  for (var i = 2017;i <= 2050 ;i++)
    $scope.yearList.push(i);


  var rowData=[];
  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    animateRows: true,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    defaultColDef : {
      width: 150,
    },
    groupHeaderHeight:40,
    headerHeight:90,
    rowHeight:40,
    onGridReady: function(params) {
      params.api.sizeColumnsToFit();
      params.columnApi.autoSizeColumns();
    },
    autoGroupColumnDef: {
    }
  };
  function clearGrid () {
    var myNode = document.getElementById("skill_inventory");
  }

  $scope.addNewUser = function(){
    
    $rootScope.loaderAction = true;
        var newEmployee = {};
        var projectCostingCtrlInstance = $uibModal.open({
           templateUrl : './assets/views/get-users/newuser.html',
           controller : 'employeeFormCtrl',
           size : 'lg',
           windowClass : 'projectDetailsWindow',
           resolve : {
            data : function (){
              return newEmployee;
            }
          }
         });
   
         $rootScope.loaderAction = false;      
  }


  function checkData (gridData) {
     if (!gridData.data) {
       return;
     }
     $rootScope.loaderAction = true;
     userService.getUserDetails(gridData.data.id).then(function(response){
         var projectCostingCtrlInstance = $uibModal.open({
            templateUrl : './assets/views/get-users/singleuser.html',
            controller : 'employeeFormCtrl',
            size : 'lg',
            windowClass : 'projectDetailsWindow',
            resolve : {
              data : function (){
                response.roles = response.roles[0];
                for(var i=0; i< response.pillarList.length ; i++ ){
                 response.pillarList[i] = response.pillarList[i].pillerName;
                }
                for(var i=0; i< response.etuList.length ; i++ ){
                 response.etuList[i] = response.etuList[i].etuName;
                }
                
                return response;
              }
            }
          });
          $rootScope.loaderAction = false;
    }); 
    
 }
$scope.update=function(){

  
  var f = document.getElementById('emp_profile_img').files[0],
        r = new FileReader();

    r.onloadend = function(e) {
      var data = e.target.result;
   
      
    }
   
    userService.uploadfile(f).then(function(response){
      if(!response.fileName==null){
        filename = response.fileName;
        filepath = response.response.fileDownloadUri;
      }
    },function(error){
      console.log(error);
    });

    delete $scope.UserForm["authorities"];
    $scope.employeeData.enabled = $scope.employeeData.enabled == "true" ? true : false ;
    userService.updateUser($scope.UserForm).then(function(response){
        $uibModalInstance.dismiss('cancel');
    },
    function(error){
        console.log(error);
    });
}

$rootScope.loaduserInventoryGrid=function(){
    
    $rootScope.loaderAction = true;
    userService.getAllUsers().then(function(response){
        clearGrid();
        columnDefs = [
          {headerName: '',cellClass:["grid-cell-float-left"],
          children:[
            { headerName: "Sr. No.", field:  "id"  ,width:80,cellClass:["grid-cell-text-center "],pinned: 'left'},
            { headerName: "Employee ID", field:  "employeeId"  ,width:80,cellClass:["grid-cell-text-center text-hover-uid"],pinned: 'left',onCellClicked : checkData},
            { headerName: "Employee Name", field:  "name"  ,width:120,cellClass:["grid-cell-text-center"],pinned: 'left'},
            { headerName: "ETU Name", field:  "etuList"  ,width:250,cellClass:["grid-cell-text-center","wrap-grid-text"],pinned: 'left',
              cellRenderer: function (params) {
                
                var etuListString = '';
                for(var i=0;i< params.data.etuList.length;i++){
                                   etuListString += params.data.etuList[i].etuName + ', ';
                }
            return etuListString;
            }
            },
            { headerName: "Piller Name", field:  "pillarList"  ,width:200,cellClass:["grid-cell-text-center","wrap-grid-text"],pinned: 'left',
          cellRenderer: function(params){
           var pillarListString = '';
            for(var i=0;i<params.data.pillarList.length;i++){
              pillarListString += params.data.pillarList[i].pillerName + ', ';
            }
            return pillarListString;
          }
          },
            //{ headerName: "Password", field:  "password"  ,width:100,cellClass:["grid-cell-text-center"],pinned: 'left'},
            //{ headerName: "Email", field:  "email"  ,width:170,cellClass:["grid-cell-text-center"],pinned: 'left'},        
            { headerName: "Status", field:  "status"  ,width:85,cellClass:["grid-cell-text-center"],pinned: 'left'},
            { headerName: " Approval", field:  "approval"  ,width:100,cellClass:["grid-cell-text-center"],pinned: 'left'},
          ]
        }
        ];
        gridOptions.rowData = response || [];

        /***** */ 
        
        var colMethod =    { headerName: "Sr. No." , children : []}  ;
        var columns = response.id || {};
        for (var key in columns) {
            colMethod.children.push({ headerName: columns[key] , field:  id  ,width:120,cellClass:["grid-cell-text-center"]});
          }
          columnDefs.push(colMethod);
        /***** */  
          var firstName =    { headerName: "Employee Name " , children : []};  
          columns = response.firstName || {};
          for (var key in columns) {
            firstName.children.push({ headerName: columns[key] , field:  name  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
          }
          columnDefs.push(firstName);
          
          /*
          var password =    { headerName: "Password " , children : []};  
          columns = response.password || {};
          for (var key in columns) {
            password.children.push({ headerName: columns[key] , field:  password  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
          }
          columnDefs.push(password);
          **** */
          /*****   
          
          
          var email =    { headerName: "Email " , children : []};  
          columns = response.email || {};
          for (var key in columns) {
            email.children.push({ headerName: columns[key] , field:  email  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
          }
          columnDefs.push(email); 
         */
         
         
         
          var employeeId =    { headerName: "Employee ID " , children : []};  
          columns = response.employeeId || {};
          for (var key in columns) {
            employeeId.children.push({ headerName: columns[key] , field:  employeeId  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
          }
          columnDefs.push(employeeId);

          /*
          var etuLevel =    { headerName: "ETU Name " , children : []};  
          columns = response.etu || {};
          for (var key in columns) {
            etuLevel.children.push({ headerName: columns[key] , field:  etu.etuName  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
          }
          columnDefs.push(etuLevel);
          */

          var pillarList = {headerName: "Piller Name ",children: []};
          columns = response.pillarList || {};
          for(var key in columns){
            pillarList.children.push({ headerName:columns[key] , field: pillarList.pillerName ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn', default: 0});
          }
          columnDefs.push(pillarList);
          /***
           * 
           * pillarList
           * etuList
           */
          
          var etuList = {headerName: "ETU Name ",children: []};
          columns = response.etuList || {};
          for(var key in columns){
            etuList.children.push({ headerName:columns[key] , field: etuList ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn', default: 0});   
          }
          columnDefs.push(etuList);

          var status =    { headerName: "Status " , children : []};  
          columns = response.status || {};
          for (var key in columns) {
            status.children.push({ headerName: columns[key] , field:  status  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
          }
          columnDefs.push(status);

          /********** */
          var approval =    { headerName: "Approval " , children : []};  
          columns = response.approval || {};
          for (var key in columns) {
            approval.children.push({ headerName: columns[key] , field:  approval  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
          }
          columnDefs.push(approval);
          /***** */
          /***** */  
          
          
          gridOptions.columnDefs = columnDefs;

          $timeout(function() {
            var cMatrixDiv = document.querySelector('#skill_inventory');
            new agGrid.Grid(cMatrixDiv, gridOptions);
            $rootScope.loaderAction = false;
          }, 100);
    
    },
    function(error){
        console.log(error);
    });
    
};







}]);