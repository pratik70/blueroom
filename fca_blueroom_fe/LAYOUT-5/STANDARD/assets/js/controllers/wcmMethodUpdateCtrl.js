app.controller('wcmMethodUpdateCtrl', ["$rootScope","$scope","commonService","userService","$uibModal",
function($rootScope,$scope,commonService,userService, $uibModal) {
    $scope.methodDropdown = ["COMMON METHODS", "SPECIFIC METHODS"];
     
    $scope.WCMMethodUpdateForm=$rootScope.gridItems.data;
    $scope.WCMMethodUpdateFormId = $rootScope.gridItems.data.id;
    $scope.WCMMethodUpdateFormLevelFirstTool = $rootScope.gridItems.data.levelFirstTool;
    $scope.WCMMethodUpdateFormLevelSecondTool = $rootScope.gridItems.data.levelSecondTool;
    $scope.WCMMethodUpdateFormReprortPriority = $rootScope.gridItems.data.reprortPriority;
    $scope.WCMMethodUpdateFormType = $rootScope.gridItems.data.type; 
    $scope.WCMMethodUpdateFormTypePillar_approach = $rootScope.gridItems.data.pillar_approach;
 
    $scope.updateWCMMethod = function(){
        var rawData = {
            id : $scope.WCMMethodUpdateFormId ? $scope.WCMMethodUpdateFormId: null,
            levelFirstTool : $scope.WCMMethodUpdateFormLevelFirstTool ? $scope.WCMMethodUpdateFormLevelFirstTool :null,
            levelSecondTool : $scope.WCMMethodUpdateFormLevelSecondTool ? $scope.WCMMethodUpdateFormLevelSecondTool : null,
            reprortPriority : $scope.WCMMethodUpdateFormReprortPriority ? $scope.WCMMethodUpdateFormReprortPriority : null,
            type : $scope.WCMMethodUpdateFormType ? $scope.WCMMethodUpdateFormType : null,
            pillar_approach :$scope.WCMMethodUpdateFormTypePillar_approach ? $scope.WCMMethodUpdateFormTypePillar_approach: null
        }
       $rootScope.showWCMMethods=false;
        commonService.updateWCMMethod($scope.WCMMethodUpdateFormId,rawData).then(function(response){
          $rootScope.mod.dismiss('cancel');
          $rootScope.showLossTypes=false;
          $rootScope.showEmployee=false;
          $rootScope.showWCMTools=false;
              $rootScope.getWCMMethods();
        },
        function(error){
          console.log(error);
        });
      }

      $scope.cancel = function() {
    
        $rootScope.mod.dismiss('cancel');
     }
    
}]);