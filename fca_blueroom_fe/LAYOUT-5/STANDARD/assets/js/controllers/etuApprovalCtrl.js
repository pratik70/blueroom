app.controller('etuApprovalCtrl',["$scope","$rootScope","projectService",
function($scope,$rootScope,projectService){
    $scope.changeETUApproval=function(){
      projectService.changeETUApprovalStatusById($rootScope.etuStatusId).then(function(response){
          $rootScope.mod.dismiss('cancel');
          $rootScope.loadEMatrixGrid();
        },
        function(error){
          console.log(error);
        });
    }

    $scope.cancel = function() {
      $rootScope.mod.dismiss('cancel');
    }
    
}]);