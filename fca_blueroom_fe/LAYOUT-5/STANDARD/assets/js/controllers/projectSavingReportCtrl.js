app.controller('ProjectSavingReportCtrl', ['$rootScope', '$scope', 'moment', 'factorManagementService',
function ($rootScope, $scope,moment, factorManagementService) {
  $scope.calendarYearList = [];
  $scope.calendarSelectedYear = 2018;
  $scope.totalRMCSavings = 0;
  $scope.rmcFactor = 0;
  var startYear = 2018;
  while( startYear < 2030) {
    $scope.calendarYearList.push(startYear);
    startYear++;
  }

  var margin = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 80,
    front: 0,
    back: 0
  };

  var width = 960 - margin.left - margin.right;
  var height = 500 - margin.top - margin.bottom;
  var depth = 100 - margin.front - margin.back;

  var xScale = d3.scale.ordinal().rangeRoundBands([0, width], 0.2);

  var yScale = d3.scale.linear().range([height, 0]);

  var zScale = d3.scale.ordinal()
  .domain([0, 1, 2])
  .rangeRoundBands([0, depth], 0.4);

  var xAxis = d3.svg.axis()
    .scale(xScale)
    .orient('bottom');

  var yAxis = d3.svg.axis()
    .scale(yScale)
    .orient('left')
    .ticks(10, "Million");


  $scope.projectMonthlySavingDataLoad = function (year) {
    var actulData = [];
    $scope.rmcFactor = 1;
    $scope.monthlySavingData = [];
    $scope.monthlySavingLabels = [];
    $scope.monthlySavingSeries = [];
    factorManagementService.getProjectMonthlySavingFactor(year,$rootScope.selectedETU.id).then(function (response) {
     // console.log(response);
      $scope.projectPlanYearList = getProjectSavingFactorHeader(year);
      if(response && response.data.length > 0 ) {
        var list = response.data;
        list.forEach(function(factor1){
          $scope.projectPlanYearList.some(function(factor2,index){
            if(factor1.month == factor2.month ) {
              $scope.projectPlanYearList[index].monthlyProjectSaving = factor1.monthlyProjectSaving;
              $scope.projectPlanYearList[index].monthFactor = factor1.monthFactor <= 0 ? 1 : factor1.monthFactor ;     
              return true;
            }
            if (factor1.month == 13 ) {
              $scope.rmcFactor = factor1.monthFactor; 
            }
            return false;
          });
        });
       $scope.monthlySavingData = [$scope.projectPlanYearList.map(function(d){ return d.monthlyProjectSaving  ? d.monthlyProjectSaving * d.monthFactor: 0; })];
        $scope.monthlySavingLabels = $scope.projectPlanYearList.map(function(d){ return d.label ;});
        var totalFrequency = 0;
        actulData = $scope.projectPlanYearList.map(function(d){ totalFrequency += d.monthlyProjectSaving  ? (d.monthlyProjectSaving * d.monthFactor ) / 1000000 : 0 ; return  { letter : d.label, frequency :  d.monthlyProjectSaving  ? (d.monthlyProjectSaving * d.monthFactor ) / 1000000 : 0 }; }) ;
        $scope.monthlySavingSeries = [$scope.calendarSelectedYear];
        $scope.loadRMCSavings();
        actulData.push({
          frequency: totalFrequency,
          letter:"Cumulative Savings"
          });
        loadProjectSavingBarChart(actulData);
      } else {
        $scope.monthlySavingData = [[]];
        var totalFrequency = 0;
        $scope.monthlySavingLabels = $scope.projectPlanYearList.map(function(d){ return d.label ;});
        actulData = $scope.projectPlanYearList.map(function(d){ totalFrequency += d.monthlyProjectSaving  ? (d.monthlyProjectSaving * d.monthFactor ) / 1000000 : 0 ;return  { letter : d.label, frequency :  d.monthlyProjectSaving  ? (d.monthlyProjectSaving * d.monthFactor ) / 1000000 : 0 }; }) ;
        $scope.loadRMCSavings();
        actulData.push({
          frequency: totalFrequency,
          letter:"Cumulative Savings"});
        loadProjectSavingBarChart(actulData);

      }
    }, function (error) {
        console.error(error);
    });
  };

  function getProjectSavingFactorHeader(year) {
    var plans = []; 
    var month = moment();
    month.month(0);
    month.year(year);
    var endDate = moment(month);
    endDate.add(1,'year');
    while( month < endDate ) {
        plans.push({
          cssClass : month.format('MMM'),
          label : month.format('MMMM'),
          month : parseInt(month.format('MM')),
          monthFactor : 0,
          monthlyProjectSaving : 0,
          totalMonthlyProjectSaving : 0,
          etuId : $scope.etuId
        });
        month.add(1, "month");
    }
    return plans;
  }; 

 
  $scope.loadRMCSavings = function () {
    var totalSavings = 0;
    $scope.projectPlanYearList.forEach( function(d) {
      if (d.month !== 13 ) {
        totalSavings += d.monthlyProjectSaving * d.monthFactor;
      }
    }); 
    $scope.totalRMCSavings = totalSavings ;
  };

 

  function loadProjectSavingBarChart (data) {
    d3.select('#chart').selectAll("*").remove();
    var chart = d3.select('#chart')
    .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
        .attr('transform', svgHelp.translate(margin.left, margin.right));
    xScale.domain(_.uniq(_.map(data, 'letter')));
    yScale.domain([0, _.max(data, 'frequency').frequency]);

    function x(d) { return xScale(d.letter); }
    function y(d) { return yScale(d.frequency); }

    var camera = [width / 2, height / 2, - 1000];
    var barGen = bar3d()
      .camera(camera)
      .x(x)
      .y(y)
      .z(zScale(0))
      .width(xScale.rangeBand())
      .height(function(d) { return height - y(d); })
      .depth(xScale.rangeBand());

    chart.append('g')
      .attr('class', 'x axis')
      .style('font-size', '12px')
      .attr('transform', svgHelp.translate(0, height))
      .call(xAxis);

   
    chart.selectAll('g.tick text')
    .style('font-size', '12px')
    .attr('transform', 'rotate(-12)')
    .style('font-weight', '600');

        
    var extent = xScale.rangeExtent();
    var middle = (extent[1] - extent[0]) / 2;
    chart.selectAll('g.bar').data(data)
    .enter().append('g')
    .attr('class', function(i,j){
      return i.letter == "Cumulative Savings" ? 'cumu bar' : 'bar';
    })
    .sort(function(a, b) {
      return Math.abs(x(b) - middle) - Math.abs(x(a) - middle);
    })
    .call(barGen);

     chart.append('g')
      .attr('class', 'y axis')
      .style('font-size', '12px')
      .style('font-weight', '600')
      .call(yAxis);
  
      chart.append('text')
      .attr('x', -(height / 2) - margin.top)
      .attr('y', - margin.left / 1.4)
      .attr('transform', 'rotate(-90)')
      .attr('text-anchor', 'middle')
      .text('Savings In Millions (INR)');
   
    chart.selectAll("g.bar")
    .data(data)
    .append("text")
    .attr("class", "bar")
    .attr("text-anchor", "middle")
    .attr("x", function(d) { return x(d) + 28; })
    .attr("y", function(d) { return y(d)-10; })
    .text(function(d) { return ( d.frequency ).toFixed(2) ; });
  }
  function type(d) {
    d.frequency = +d.frequency;
    return d;
  }

  $scope.projectMonthlySavingDataLoad($scope.calendarSelectedYear);
}]);
