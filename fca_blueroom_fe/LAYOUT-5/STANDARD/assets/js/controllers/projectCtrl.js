'use strict';



app.controller('ProjectPlanRemarkCtrl', ["$scope", "$uibModalInstance","planDetails",'projectService',
function($scope, $uibModalInstance, planDetails,projectService) {
    $scope.projectPlan = {};
    $scope.remarkText = "";
    function loadProjectremark () {
        var data = {
            projectId : planDetails.projectId,
            weekStartDate : planDetails.weekStartDate,
            remarkText :  $scope.remarkText 
        };
    projectService.getProjectRemark(planDetails.projectId,data).then(function (response) {
            if ( response &&  response.data && response.data.remarkText) {
                $scope.remarkText = response.data.remarkText;
            }
        }, function (error) {
            console.error(error);
        });
    }
    $scope.save = function(form) {
        if (form.$valid) {
            form.$setPristine();
            var data = {
                projectId : planDetails.projectId,
                weekStartDate : planDetails.weekStartDate,
                remarkText :  $scope.remarkText 
            };
            projectService.addProjectRemark(planDetails.projectId, data).then(function (response) {
                $uibModalInstance.close(true);
            }, function (error) {
                console.error(error);
            });
        } else {
            var field = null, firstError = null;
            for (field in form) {
                if (field[0] != '$') {
                    if (firstError === null && !form[field].$valid) {
                        firstError = form[field].$name;
                    }

                    if (form[field].$pristine) {
                        form[field].$dirty = true;
                    }
                }
            }
            angular.element('.ng-invalid[name=' + firstError + ']').focus();
        }
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    loadProjectremark();
}]);


app.controller('NewProjectCtrl', ["$rootScope","$scope", 'ngNotify','projectService', "$filter",'NgTableParams','$stateParams', '$state','$location','$q', 'moment', '$uibModal','$timeout', 'commonService' , function ( $rootScope, $scope, ngNotify, projectService, $filter, NgTableParams, $stateParams, $state, $location, $q, moment, $uibModal, $timeout,commonService ) {
    
    $timeout(function() {
        $rootScope.pageHeaderTitle = 'PROJECT MANAGEMENT';
    }, 10);
    
    if ( !isNaN(parseInt($stateParams.projectId)) ) {
        $scope.projectId = $stateParams.projectId;
    } else {
        $scope.projectId = null;
    }

    if ( !isNaN(parseInt($stateParams.stage)) ) {
        $scope.currentStep = parseInt($stateParams.stage);
    } else {
        $scope.currentStep = 1;
    }

    if ( $scope.currentStep > 1 && $scope.projectId == null) {
        $location.path("app/project");
    }

    $scope.completeStage  = 0;
    $scope.minProjectDate = new Date();
    
    $scope.methodModel = true;
    $scope.toolModel = true;
    $rootScope.financeStatus;
    $rootScope.etuStatus ;
    $scope.etuMailSend ;
    $scope.financeMailSend ;
    $scope.mainLossesList = [];
    $scope.allLossGroups =[];
    $scope.allLosses =[];
    $scope.allMachines= []; 
    $scope.allETUs= []; 
    $scope.allProcess= []; 
    $scope.allPillars=[];
    $scope.kpisList=[];
    $scope.cdYearList =[];
    $scope.example1model = []; 

    $scope.calendarYear = moment().year();
    if ( $rootScope.selectedETU ) {
        $scope.selectedETU = $rootScope.selectedETU ;
    }   else if ( localStorage.getItem( "ETU_value")) {
        $scope.selectedETU =  JSON.parse(localStorage.getItem( "ETU_value"));
    } else {
        $scope.selectedETU = {};
    }
    $scope.etuId =  $scope.selectedETU.id;
     if ( $rootScope.selectedCD ) {
        $scope.selectedCD = $rootScope.selectedCD ;
        $scope.projectCD  = $scope.selectedCD.id; 
    }   else if ( localStorage.getItem( "CD_value")) {
        $scope.selectedCD =  JSON.parse(localStorage.getItem( "CD_value"));
        $scope.projectCD  = $scope.selectedCD.id;  
    }
    
    $scope.basicInfoForm = { etu : $scope.etuId ? $scope.etuId +"" : null,projectCD : $scope.projectCD + ""}; 
    $scope.pillarInfoForm = {
        supportingPillars : [],
        kpisList : []
    };
    $scope.selectedMethodsList = [];
    $scope.selectedToolsList = [];
    $scope.allToolsList = [];  
    $scope.allMethodsList = [];  
    $scope.totalMethodToolsList=[];
    $scope.allBenifitList = [];
    $scope.userList = [];
    $scope.qualifiedUsers = [];
    $scope.userListBySkill = [];
    $scope.removedUsersList = [];
    $scope.userRemovedListBySkill=[];
    var x = 0;
    $scope.teamCoachBySkill = [];
    $scope.teamMemberBySkill = [];
    
    $scope.technicalToolList = [];
    var teamsCoach;
    var teamsMember;
    $scope.selectedTechnicalToolList = [];
    if($rootScope.isHideExicutionButton == true){
        $rootScope.isHideExicutionButton = true;
    } else {
        $rootScope.isHideExicutionButton = false;
    }
    $scope.projectMembers = {
        teamLeader : "",
        teamLeaderFiltered: false,
        teamCoach : "",
        teamCoachFiltered: false,
        firstTeamMember:'',
        secondTeamMember:'',
        thirdTeamMember:'',
        teamMembers : [ {
            userId : "",
            filteredUser  : false
        }] 
    };

    $scope.benifitData = {};
    $scope.projectCostingError = false;
    $scope.projectCostingBcError = 'NO';
    $scope.projectCostingBcErrorPanel = false;
    $scope.dateDisabledOptions = {
     
    };

    $scope.projectPlan = {
        startDate : moment().toDate(),
        endDate : moment().add(2,"months").toDate(),
        planTable : []
    };
    $scope.planTable = [];
    $scope.isPlanningCompleted = false;
   

    $scope.totalYearlyPotential  = 0;
    $scope.fMatrix = { list : [] } ;
    $scope.completionForm = {};
    // Initial Value
    $scope.form = {
        saveProject(form) {
            $scope.toTheTop();
            if (form.$valid) {
                form.$setPristine();

                $scope.addProject =function(){  
                    if (!$scope.projectId) {
                        projectService.createProject($scope.basicInfoForm).then(function (response) {
                            $location.path("app/project/"+ response.data.projectId+"/2");
                        }, function (error) {
                            console.error(error);
                        });
                    } else {
                        projectService.updateProject($scope.basicInfoForm).then(function (response) {
                           $location.path("app/project/"+response.data.projectId+"/2");
                        }, function (error) {
                            console.error(error);
                        });
                    }
                }

                if($scope.ewDocument && $scope.ewDocument != "" && $scope.ewDocument !== null){
                    projectService.saveProjectEwDocument($scope.ewDocument).then(function (response) {
                        if ( response && response.code) {
                            $scope.basicInfoForm.ewDocument = response.data;
                            $scope.addProject();
                        }    
                    }, function (error) {
                        $scope.addProject();    
                        console.error(error);
                    });   
                }else{
                    $scope.addProject();                    
                }

            } else {
               showFromFieldError(form);
            }
        }, 
        saveProjectPillar(form) {
            $scope.toTheTop();
            if (form.$valid) {
                form.$setPristine();
                var data = JSON.parse(JSON.stringify($scope.pillarInfoForm));
                data.projectId = $scope.projectId ;
                data.supportingPillars = data.supportingPillars.filter(function(v,i){return v !== null && v !== '' ;});
                data.kpis = data.kpisList.filter(function(v,i){return v !== null && v !== '' ;});
                projectService.addProjectPillars(data).then(function (response) {
                    $location.path("app/project/"+ $scope.projectId+"/3");
                }, function (error) {
                    console.error(error);
                });
                
            } else {
                showFromFieldError(form);
            }
        },
        saveProjectMethodsAndTool:function(form) {
            $scope.toTheTop();
            var methodsAndTools = JSON.parse(JSON.stringify($scope.selectedMethodsList));
            methodsAndTools = methodsAndTools.concat($scope.selectedToolsList);
            var data = {
                methodsAndTools : methodsAndTools,
                technicalTools: $scope.selectedTechnicalToolList
            };
            if ( $scope.selectedToolsList.length > 0) {
                projectService.saveProjectMethodsAndTool( $scope.projectId, data).then(function (response) {
                    nextStep();
                }, function (error) {
                    console.error(error);
                });
                
            } else {
                errorMessage();
            }
            
        },
        saveProjectMembers: function(form) {
            if (form.$valid) {
                form.$setPristine();
                //$scope.projectMembers.teamMembers = $scope.example1model;
                ///////////////
                $scope.example1model.filter(function(s){
                    var obj = {userId:s.id,filteredUser:false}
                    $scope.projectMembers.teamMembers.push(obj);
                });
                ////////////////
                var data = JSON.parse(JSON.stringify($scope.projectMembers));
                data.teamMembers = data.teamMembers.filter(function(item) { return item.userId !== null && item.userId !== ''; });
                projectService.saveProjectMembers( $scope.projectId, data).then(function (response) {
                    $scope.toTheTop();
                    nextStep();
                }, function (error) {
                    console.error(error);
                });
                projectService.getProjectInformation($scope.projectId).then(function(response){
                    
                    $rootScope.etuStatus = response.data.etuStatus;
                    $rootScope.financeStatus = response.data.financeStatus;
               
                   
                },function(error){
                    console.log(error);
                });

                
            } else {
                showFromFieldError(form);
            }
        },
        saveProjectCosting : function(form) {
            if (form.$valid) {
                form.$setPristine();
                
                var totalCount = 0;
                $scope.allBenifitList.forEach(item=>{
                    if ( item.benifitValue ) {
                       totalCount = totalCount + item.benifitValue;
                    }
                });
                $scope.projectCostingError = false;
                $scope.isProjectSsavingTypeSelected = false;
                if ( $scope.benifitData.savingsType != null  ) {
                    var data = JSON.parse(JSON.stringify($scope.benifitData));
                    data.benifitList = $scope.allBenifitList;
                    var previouslyMailSended = $scope.etuMailSend;
                    projectService.manageProjectCosting( $scope.projectId, data).then(function (resp) {

                         $scope.etuMailSend = resp.data.project.etuMailSend;
                         $scope.financeMailSend = resp.data.project.financeMailSend;
                     if(!previouslyMailSended && $scope.etuMailSend) {
                    //    // projectService.sendEmailsToETUAndFinanceApprovals( $scope.projectId).then(function(response){
                         
                            //alert(response.data);
                            var confirmationDialogCtrlInstance = $uibModal.open({
                                templateUrl : './assets/views/project/ConfirmationMessageForEmail.html',
                                controller : 'ConfirmationDialogCtrl1',
                                size : 'sm',
                                resolve : {
                                   data : function () {
                                        return { headerTitle : "Execution Complete" , textMessage :  "Are you want to complete execution ?" , data : data };
                                    }
                                }
                            });                     
            
                        // },function(error){
                        //     console.log(error);
                        // });      
                     //   backToHome();
                        



                         }
                         else{
                                 nextStep();
                             } 


                        }, function (error) {
                            console.error(error);
                    });

                }   else {
                    $scope.isProjectSsavingTypeSelected = true;
                }
                
                  
   
                        
            } else {
                showFromFieldError(form);
            }
        }, 
        saveProjectActual: function(form,flag) {
            if (form.$valid) {
                form.$setPristine();
                if ($scope.projectPlan.startDate && $scope.projectPlan.endDate) {
                    var data = {
                        pActualStartDate : null,
                        pActualEndDate : null,
                        dActualStartDate : null,
                        dActualEndDate : null,
                        cActualStartDate : null,
                        cActualEndDate : null,
                        aActualStartDate : null,
                        aActualEndDate : null,
                        completionDate : null
                    };
                    var planTable = JSON.parse(JSON.stringify($scope.projectPlan.executionPlanTable));
                    planTable.forEach(function(weekDates){
                        weekDates.weekOfMonth.forEach(function(item){
                            if (item.actualStatus && item.actualStatus!== null) {
                                if(item.actualStatus == "P") {
                                    data.pActualStartDate = data.pActualStartDate ? data.pActualStartDate : item.weekStartDate;
                                    data.pActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "D") {
                                    data.dActualStartDate = data.dActualStartDate ? data.dActualStartDate : item.weekStartDate;
                                    data.dActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "C") {
                                    data.cActualStartDate = data.cActualStartDate ? data.cActualStartDate : item.weekStartDate;
                                    data.cActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "A") {
                                    data.aActualStartDate = data.aActualStartDate ? data.aActualStartDate : item.weekStartDate ;
                                    data.aActualEndDate =  item.weekEndDate;
                                    data.completionDate = item.weekEndDate;
                                }
                                
                            }
                        });
                    });
                    data.saveCompletionDate = flag ;
                    projectService.saveProjectExecutionDates($scope.projectId, data).then(function (response) {
                        if (flag )
                            $location.path("app/project/"+$scope.projectId+"/7");
                        $rootScope.loaderAction = false;
                    }, function (error) {
                        console.error(error);
                    });

                }
            } else {
                showFromFieldError(form);
            }

        },

        saveProjectActualWithoutNext: function(form,flag) {
            if (form.$valid) {
                form.$setPristine();
                if ($scope.projectPlan.startDate && $scope.projectPlan.endDate) {
                    var data = {
                        pActualStartDate : null,
                        pActualEndDate : null,
                        dActualStartDate : null,
                        dActualEndDate : null,
                        cActualStartDate : null,
                        cActualEndDate : null,
                        aActualStartDate : null,
                        aActualEndDate : null
                    };
                    var planTable = JSON.parse(JSON.stringify($scope.projectPlan.executionPlanTable));
                    planTable.forEach(function(weekDates){
                        weekDates.weekOfMonth.forEach(function(item){
                            if (item.actualStatus && item.actualStatus!== null) {
                                if(item.actualStatus == "P") {
                                    data.pActualStartDate = data.pActualStartDate ? data.pActualStartDate : item.weekStartDate;
                                    data.pActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "D") {
                                    data.dActualStartDate = data.dActualStartDate ? data.dActualStartDate : item.weekStartDate;
                                    data.dActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "C") {
                                    data.cActualStartDate = data.cActualStartDate ? data.cActualStartDate : item.weekStartDate;
                                    data.cActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "A") {
                                    data.aActualStartDate = data.aActualStartDate ? data.aActualStartDate : item.weekStartDate ;
                                    data.aActualEndDate =  item.weekEndDate;
                                }
                                
                            }
                        });
                    });
                    data.saveCompletionDate = flag ;
                    projectService.saveProjectExecutionDates($scope.projectId, data).then(function (response) {
                        if (flag )
                            $location.path("app/project/"+$scope.projectId+"/6");
                        $rootScope.loaderAction = false;
                    }, function (error) {
                        console.log(error);
                    });

                }
            } else {
                showFromFieldError(form);
            }

        },

        saveProjectplaning: function(form,flag) {
            if (form.$valid) {
                form.$setPristine();
                updateProjectPlanning().then(function(response){
                    if(response) {
                        $location.path("app/project/"+$scope.projectId+"/8");
                    } 
                }).catch(function(response){

                });
            } else {
                showFromFieldError(form);
            }

        },
        saveProjectplaningWithoutNext: function(form,flag) {
          
            $scope.form.saveProjectActualWithoutNext(form, true);
            if (form.$valid) {
                form.$setPristine();
                updateProjectPlanningWithoutCompletion().then(function(response){
                    if(response) {
                        $location.path("app/project/"+$scope.projectId+"/6");
                    } 
                }).catch(function(response){

                });
              
                getProjectInformation();
            } else {
                showFromFieldError(form);
            }

        },
        completeExecutionPlanWithoutNext: function(form,flag){
         
            
            if (form.$valid) {
                form.$setPristine();
                if ($scope.projectPlan.startDate && $scope.projectPlan.endDate) {
                    var data = {
                        pActualStartDate : null,
                        pActualEndDate : null,
                        dActualStartDate : null,
                        dActualEndDate : null,
                        cActualStartDate : null,
                        cActualEndDate : null,
                        aActualStartDate : null,
                        aActualEndDate : null
                    };
                    var planTable = JSON.parse(JSON.stringify($scope.projectPlan.executionPlanTable));
                    planTable.forEach(function(weekDates){
                        weekDates.weekOfMonth.forEach(function(item){
                            if (item.actualStatus && item.actualStatus!== null) {
                                if(item.actualStatus == "P") {
                                    data.pActualStartDate = data.pActualStartDate ? data.pActualStartDate : item.weekStartDate;
                                    data.pActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "D") {
                                    data.dActualStartDate = data.dActualStartDate ? data.dActualStartDate : item.weekStartDate;
                                    data.dActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "C") {
                                    data.cActualStartDate = data.cActualStartDate ? data.cActualStartDate : item.weekStartDate;
                                    data.cActualEndDate = item.weekEndDate;
                                } else if(item.actualStatus == "A") {
                                    data.aActualStartDate = data.aActualStartDate ? data.aActualStartDate : item.weekStartDate ;
                                    data.aActualEndDate =  item.weekEndDate;
                                }
                                
                            }
                        });
                    });
    
                   /* var confirmationDialogCtrlInstance = $uibModal.open({
                        templateUrl : './assets/views/project/ConfirmationDialog.html',
                        controller : 'ConfirmationDialogCtrl1',
                        size : 'sm',
                        resolve : {
                           data : function () {
                                return { headerTitle : "Execution Complete" , textMessage :  "Are you want to complete execution ?" , data : data };
                            }
                        }
                    });*/
                    
                    //$scope.saveProjectActualWithoutNext(form, true);
                    $scope.form.saveProjectActualWithoutNext(form, true);
                    confirmationDialogCtrlInstance.result.then(function(value) {
                        if (value) {
                            $rootScope.loaderAction = true;
                            $scope.form.saveProjectActualWithoutNext(form, true);
                        } else {
                            $scope.form.saveProjectActualWithoutNext(form, false);
                        }
                    }, function() {
                        //$log.info('Modal dismissed at: ' + new Date());
                    });
                    
                }
                 if(form.$name == "Form6"){
                       $rootScope.isHideExicutionButton = true;
                  }
            } else {
                showFromFieldError(form);
            }
    
        },
        saveProjectComplete(form) {
            $scope.toTheTop();
            if (form.$valid) {
                form.$setPristine();
                var previouslyMailSended = $scope.finance_mail_send;
                if($scope.documentFile && $scope.documentFile != "" && $scope.documentFile !== null){
                    projectService.saveProjectDocument($scope.documentFile).then(function (response) {
                        if ( response && response.code) {
                            $scope.completionForm.document = response.data;
                            projectService.saveProjectCompletionData($scope.projectId,$scope.completionForm).then(function (response) {
                                if ( response && response.code ) {
                                    $location.path("app/project/"+$scope.projectId+"/8");
                                }    
                            }, function (error) {
                                console.error(error);
                            }); 
                        }    
                    }, function (error) {
                        console.error(error);
                    });   
                } else {
                    projectService.saveProjectCompletionData($scope.projectId,$scope.completionForm).then(function (response) {
                        $scope.finance_mail_send = response.data.finance_mail_send;
                       var message = response.message;
                        if($scope.finance_mail_send ==false ){
                         if ( response && response.code ) {
                            
                            //if($scope.sMatrixLossTypeSelected){
                                
                                var confirmationDialogCtrlInstance = $uibModal.open({
                                    templateUrl : './assets/views/project/ConfirmationMessageForFinanceEmail.html',
                                    controller : 'ConfirmationDialogCtrl1',
                                    size : 'sm',
                                    resolve : {
                                       data : function () {
                                            return '';
                                        }
                                    }
                                });
                        
                            //    $state.go('app.dashboard_main');
                            // }else{
                            //     $location.path("app/project/"+$scope.projectId+"/8");
                            // }
                        }
                    }else{
                        nextStep();
                    }    
                
                    }, function (error) {
                        console.error(error);
                    });  
                }
            } else {
               showFromFieldError(form);
            }
        }, 
        next: function (form) {

            $scope.toTheTop();

            if (form.$valid) {
                form.$setPristine();
                nextStep();
            } else {
                showFromFieldError(form);
            }
        },
        prev: function (form) {
            $scope.toTheTop();
            prevStep();
        },
        goTo: function (form, i) {
            if ( $scope.projectId && i <= $scope.completeStage+1  ) {
                $scope.toTheTop();
                goToStep(i);
               
            } else {
                // goToStep(i);
                
                /*if (!form.$valid) {
                    $scope.toTheTop();
                    goToStep(i);

                } else*/
                errorMessage();
            }
        },
        submit: function () {

        },
        reset: function () {

        }
    };

    $scope.completeExecutionPlan = function(form) {
        if (form.$valid) {
            form.$setPristine();
            if ($scope.projectPlan.startDate && $scope.projectPlan.endDate) {
                var data = {
                    pActualStartDate : null,
                    pActualEndDate : null,
                    dActualStartDate : null,
                    dActualEndDate : null,
                    cActualStartDate : null,
                    cActualEndDate : null,
                    aActualStartDate : null,
                    aActualEndDate : null
                };
                var planTable = JSON.parse(JSON.stringify($scope.projectPlan.executionPlanTable));
                planTable.forEach(function(weekDates){
                    weekDates.weekOfMonth.forEach(function(item){
                        if (item.actualStatus && item.actualStatus!== null) {
                            if(item.actualStatus == "P") {
                                data.pActualStartDate = data.pActualStartDate ? data.pActualStartDate : item.weekStartDate;
                                data.pActualEndDate = item.weekEndDate;
                            } else if(item.actualStatus == "D") {
                                data.dActualStartDate = data.dActualStartDate ? data.dActualStartDate : item.weekStartDate;
                                data.dActualEndDate = item.weekEndDate;
                            } else if(item.actualStatus == "C") {
                                data.cActualStartDate = data.cActualStartDate ? data.cActualStartDate : item.weekStartDate;
                                data.cActualEndDate = item.weekEndDate;
                            } else if(item.actualStatus == "A") {
                                data.aActualStartDate = data.aActualStartDate ? data.aActualStartDate : item.weekStartDate ;
                                data.aActualEndDate =  item.weekEndDate;
                            }
                            
                        }
                    });
                });

                var confirmationDialogCtrlInstance = $uibModal.open({
                    templateUrl : './assets/views/project/ConfirmationDialog.html',
                    controller : 'ConfirmationDialogCtrl1',
                    size : 'sm',
                    resolve : {
                       data : function () {
                            return { headerTitle : "Execution Complete" , textMessage :  "Are you want to complete execution ?" , data : data };
                        }
                    }
                });

                confirmationDialogCtrlInstance.result.then(function(value) {
                    if (value) {
                        $rootScope.loaderAction = true;
                        $scope.form.saveProjectActual(form, true);
                    } else {
                        $scope.form.saveProjectActual(form, false);
                    }
                }, function() {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
                
            }
             if(form.$name == "Form6"){
                   $rootScope.isHideExicutionButton = true;
              }
        } else {
            showFromFieldError(form);
        }

        
    };

    $scope.errorMessage = "";
    function updateProjectPlanning() {
        return new Promise((resolve,reject) => {
            if ($scope.projectPlan.startDate && $scope.projectPlan.endDate) {
                var data = {
                    startDate :  moment($scope.projectPlan.startDate).format('MM-DD-YYYY'),
                    targetDate : moment($scope.projectPlan.endDate).format('MM-DD-YYYY'),
                    pPlannedStartDate : null,
                    pPlannedEndDate : null,
                    dPlannedStartDate : null,
                    dPlannedEndDate : null,
                    cPlannedStartDate : null,
                    cPlannedEndDate : null,
                    aPlannedStartDate : null,
                    aPlannedEndDate : null,
                    completionDate : null
                };
                var planTable = JSON.parse(JSON.stringify($scope.projectPlan.planTable));
                planTable.forEach(function(weekDates){
                    weekDates.weekOfMonth.forEach(function(item){
                        if (item.status && item.status!== null) {
                            if(item.status == "P") {
                                data.pPlannedStartDate = data.pPlannedStartDate ? data.pPlannedStartDate : item.weekStartDate;
                                data.pPlannedEndDate = item.weekEndDate;
                            } else if(item.status == "D") {
                                data.dPlannedStartDate = data.dPlannedStartDate ? data.dPlannedStartDate : item.weekStartDate;
                                data.dPlannedEndDate = item.weekEndDate;
                            } else if(item.status == "C") {
                                data.cPlannedStartDate = data.cPlannedStartDate ? data.cPlannedStartDate : item.weekStartDate;
                                data.cPlannedEndDate = item.weekEndDate;
                            } else if(item.status == "A") {
                                data.aPlannedStartDate = data.aPlannedStartDate ? data.aPlannedStartDate : item.weekStartDate ;
                                data.aPlannedEndDate =  item.weekEndDate;
                            }
                            
                        }
                    });
                });
                if ( data.pPlannedStartDate !== null &&  data.pPlannedEndDate !== null && data.dPlannedStartDate !== null &&  data.dPlannedEndDate !== null && data.cPlannedStartDate !== null &&  data.cPlannedEndDate !== null && data.cPlannedStartDate !== null &&  data.aPlannedEndDate !== null ) {
                    $scope.errorMessage = "";
                    projectService.savePlanningDates($scope.projectId, data).then(function (response) {
                       resolve(true);
                    }, function (error) {
                       reject(error); 
                        console.error(error);
                    });
                }   else {
                     resolve(false);
                    $scope.errorMessage = "Please plan all the stages (P/D/C/A) to save data.";
                }
            }
        }); 
    }
   function updateProjectPlanningWithoutCompletion(){
   
    return new Promise((resolve,reject) => {
        if ($scope.projectPlan.startDate && $scope.projectPlan.endDate) {
            var data = {
                startDate :  moment($scope.projectPlan.startDate).format('MM-DD-YYYY'),
                targetDate : moment($scope.projectPlan.endDate).format('MM-DD-YYYY'),
                pPlannedStartDate : null,
                pPlannedEndDate : null,
                dPlannedStartDate : null,
                dPlannedEndDate : null,
                cPlannedStartDate : null,
                cPlannedEndDate : null,
                aPlannedStartDate : null,
                aPlannedEndDate : null
            };
            var planTable = JSON.parse(JSON.stringify($scope.projectPlan.planTable));
            planTable.forEach(function(weekDates){
                weekDates.weekOfMonth.forEach(function(item){
                    if (item.status && item.status!== null) {
                        if(item.status == "P") {
                            data.pPlannedStartDate = data.pPlannedStartDate ? data.pPlannedStartDate : item.weekStartDate;
                            data.pPlannedEndDate = item.weekEndDate;
                        } else if(item.status == "D") {
                            data.dPlannedStartDate = data.dPlannedStartDate ? data.dPlannedStartDate : item.weekStartDate;
                            data.dPlannedEndDate = item.weekEndDate;
                        } else if(item.status == "C") {
                            data.cPlannedStartDate = data.cPlannedStartDate ? data.cPlannedStartDate : item.weekStartDate;
                            data.cPlannedEndDate = item.weekEndDate;
                        } else if(item.status == "A") {
                            data.aPlannedStartDate = data.aPlannedStartDate ? data.aPlannedStartDate : item.weekStartDate ;
                            data.aPlannedEndDate =  item.weekEndDate;
                        }
                        
                    }
                });
            });
            if ( data.pPlannedStartDate !== null &&  data.pPlannedEndDate !== null && data.dPlannedStartDate !== null &&  data.dPlannedEndDate !== null && data.cPlannedStartDate !== null &&  data.cPlannedEndDate !== null  ) {
                $scope.errorMessage = "";
                projectService.savePlanningDates($scope.projectId, data).then(function (response) {
                   resolve(true);
                }, function (error) {
                   reject(error); 
                    console.error(error);
                });
                getProjectPlanningDates();
            }   else {
                 resolve(false);
                $scope.errorMessage = "Please plan all the stages (P/D/C/A) to save data.";
            }
        }
    });
    
   }

    $scope.removeOptions = ['SCRAP','ENERGY COST'];

    $scope.filterByBenifitName= function(benifit) {
        return ($scope.removeOptions.indexOf(benifit.benifitName) == -1);
    };
    function showFromFieldError(form) {
        if (form) {
            var field = null, firstError = null;
            for (field in form) {
                if (field[0] != '$') {
                    if (firstError === null && !form[field].$valid) {
                        firstError = form[field].$name;
                    }

                    if (form[field].$pristine) {
                        form[field].$dirty = true;
                    }
                }
            }
            angular.element('.ng-invalid[name=' + firstError + ']').focus();
            errorMessage();
        }
    }
    
    $("#upload").change(function () {
        if ($(this)[0].files.length > 0) {
            $('.selected-fileName').text($(this)[0].files[0].name);
            $scope.documentFile = $(this)[0].files[0];
        }
    });

    $("#ewDocument").change(function () {
        if ($(this)[0].files.length > 0) {
            $('.ewDocument').text($(this)[0].files[0].name);
            $scope.ewDocument = $(this)[0].files[0];
        }
    });

    var backToHome = function(){
        $state.go('app.dashboard_main');        
    }

    var nextStep = function () {
        $scope.currentStep++;
        $location.path("app/project/"+$scope.projectId+"/"+$scope.currentStep);
    };
    var prevStep = function () {
        $scope.currentStep--;
        $location.path("app/project/"+$scope.projectId+"/"+$scope.currentStep);
    };
    var goToStep = function (i) {
        //$scope.currentStep = i;
        $location.path("app/project/"+$scope.projectId+"/"+i);
    };
    var errorMessage = function (i) {

        ngNotify.set('please complete the form in this step before proceeding', {
            theme: 'pure',
            position: 'top',
            type: 'error',
            button: 'true',
            sticky: 'false',
        });
    };

    function getProjectInformation() {
        projectService.getProjectInformation($scope.projectId).then(function (response) {
            if (response.code == "200" ) {
                $scope.basicInfoForm = JSON.parse(JSON.stringify(response.data));
                $rootScope.pageSubHeaderTitle = $scope.basicInfoForm && $scope.basicInfoForm.projectName ? $scope.basicInfoForm.projectName  : "";
                $scope.basicInfoForm.process = $scope.basicInfoForm.process ? $scope.basicInfoForm.process.id + "" : '';
                $scope.basicInfoForm.optMachine = $scope.basicInfoForm.optMachine ? $scope.basicInfoForm.optMachine.id + "" : "";
                $scope.basicInfoForm.etu = $scope.basicInfoForm.etu ? $scope.basicInfoForm.etu.id + "" : '';
                var lossData = $scope.basicInfoForm.lossType;
                $scope.basicInfoForm.lossType = $scope.basicInfoForm.lossType ? $scope.basicInfoForm.lossType.id + "" : "";
                $scope.changeLoss(lossData , true);
                $scope.basicInfoForm.completeStage = $scope.basicInfoForm.completeStage;
                $scope.basicInfoForm.ewDocument = $scope.basicInfoForm.ewDocument;
               
                $scope.benifitData.totalPotentialBenefit = $scope.basicInfoForm.yearlyPotential;
                $scope.totalYearlyPotential = $scope.basicInfoForm.yearlyPotential || 0;
                $scope.basicInfoForm.projectCD = $scope.basicInfoForm.cDYear ? $scope.basicInfoForm.cDYear.id  : '';
                $scope.completeStage = $scope.basicInfoForm.completeStage;
                //$scope.basicInfoForm.cDYear = null;
                if ($scope.currentStep > $scope.completeStage) {
                    $location.path("app/project/"+$scope.projectId+"/" + ($scope.completeStage+1));
                }
                if ($scope.basicInfoForm.completionDate !== null) {
                    $scope.completionForm.completionDate = new Date($scope.basicInfoForm.completionDate);
                    $rootScope.beforeSevenStepDate = $scope.completionForm.completionDate;
                }
                $scope.completionForm.horizontalExpansion = $scope.basicInfoForm.horizontalExpansion;
                $scope.completionForm.remark = $scope.basicInfoForm.remark;
                $scope.completionForm.standardisationRemark = $scope.basicInfoForm.standardisationRemark;
                $scope.completionForm.standardisation = $scope.basicInfoForm.standardisation;
                $scope.completionForm.epmInfo = $scope.basicInfoForm.epmInfo;
                $scope.completionForm.epmInfoRemark = $scope.basicInfoForm.epmInfoRemark;
                // $scope.completionForm.document = $scope.basicInfoForm.document === null || $scope.basicInfoForm.document === "" ? "No File selected." : $scope.basicInfoForm.document;
                $scope.completionForm.document = $scope.basicInfoForm.document ;
                
                if ( $scope.currentStep - 1 <= $scope.completeStage ) {
                  loadStageData();
                }
                $scope.isPlanningCompleted = $scope.basicInfoForm.executionStarted || false;
                $rootScope.isHideExicutionButton =  $scope.basicInfoForm.executionStarted || false;
                $scope.etuMailSend = $scope.basicInfoForm.etuMailSend;
                
            } else {
                $location.path("app/project");
            }
        }, function (error) {
            console.error(error);
        });
    }

    function getProjectPillars() {
        projectService.getProjectPillars($scope.projectId).then(function (response) {
            if ( response.data &&  response.data.mainPillar) {
                var data = response.data;
                $scope.pillarInfoForm.mainPillar = data.mainPillar + '';
                for (var i in data.supportingPillars) {
                    $scope.pillarInfoForm.supportingPillars[data.supportingPillars[i]] = data.supportingPillars[i]+"";
                }
                for (var i in data.kpis) {
                    $scope.pillarInfoForm.kpisList[data.kpis[i]] = data.kpis[i]+"";
                }
            }
        }, function (error) {
            console.error(error);
        });
    }

    function getAllLossGroup() {
        projectService.getAllLossGroup().then(function (response) {
            var list = response.data || [];
          //  var groups = list.map(function(obj) { return obj.group; });
            var sources = list.map(function(obj){return obj.source; });
            //groups = groups.filter(function(v,i) { return groups.indexOf(v) == i; });
            sources = sources.filter(function(v,i){ return sources.indexOf(v) == i;});
            $scope.mainLossesList = list;
            $scope.allLossGroups = sources;
            $scope.allSources = sources;
            $scope.allLosses = list;            
        }, function (error) {
            console.error(error);
        });
    }

    function getAllCDYear() {
        commonService.getCDYearList().then(function (response) {
            $scope.cdYearList = response.data || [];
        }, function (error) {
            console.error(error);
        });
    }

    function getKPIsList() {
        projectService.getKPIsList().then(function (response) {
            $scope.kpisList = response.data || [];
        }, function (error) {
            console.error(error);
        });
    }
    function getAllMachine() {
        if(!$rootScope.selectedETU || !$rootScope.selectedETU.id) {
            $rootScope.selectedETU = JSON.parse(localStorage.getItem("ETU_value"));
        }
        projectService.getAllMachine($rootScope.selectedETU.id).then(function (response) {
            $scope.allMachines  = response.data;
        }, function (error) {
            console.error(error);
        });
    }
    function getAllETUType() {
        projectService.getAllETUType().then(function (response) {
            $scope.allETUs  = response.data;
        }, function (error) {
            console.error(error);
        });
    }
    function getAllProcess() {
        projectService.getAllProcess().then(function (response) {
            $scope.allProcess = response.data;
        }, function (error) {
            console.error(error);
        });
    }
    function getAllPillars() {
        projectService.getAllPillars().then(function (response) {
            $scope.allPillars = response.data;
            $scope.allPillars.forEach(function(value,i){
                $scope.pillarInfoForm.supportingPillars[i] = '';
            });
        }, function (error) {
            console.error(error);
        });
    }

    function getUserList() {
        projectService.getUserList().then(function (response) {
            $scope.userList = response.data;
           
            
        }, function (error) {
            console.error(error);
        });
    }
  
    $scope.usersmode=false;
    $scope.getUserListBySkill = function() {
        $scope.usersmode = $scope.projectMembers.teamLeaderFiltered;
        projectService.getUserListBySkill($scope.projectId,$scope.usersmode ).then(function (response) {
            $scope.userListBySkill = response.data;
            teamsCoach = angular.copy($scope.userListBySkill);
           $scope.userRemovedListBySkill=angular.copy(teamsCoach);
            
            for(var r=0;r<$scope.userRemovedListBySkill.length;r++){
               
                for(var i=0;i<$scope.removedUsersList.length;i++){
                
                if($scope.removedUsersList[i].id == $scope.userRemovedListBySkill[r].id){
                    
                    $scope.userRemovedListBySkill.splice(r,1);
                }
                
            }
            }
            if(!$scope.usersmode){
                $scope.qualifiedUsers = response.data;
            }            
        }, function (error) {
            console.error(error);
        });
    }

    function getProjectTeamMembers() {
        projectService.getProjectTeamMembers($scope.projectId).then(function (response) {
            var data = response.data;
            $scope.projectMembers.teamLeader = data.teamLeader ?  data.teamLeader + "" : "";
            $scope.projectMembers.teamLeaderFiltered = data.teamLeaderFiltered;
            $scope.projectMembers.teamCoach  =  data.teamCoach ?  data.teamCoach + "" : "";
            $scope.projectMembers.teamCoachFiltered = data.teamCoachFiltered; 
            
            
            $scope.projectMembers.firstTeamMember  =  data.firstTeamMember ?  data.firstTeamMember + "" : "";
            $scope.projectMembers.secondTeamMember  =  data.secondTeamMember ?  data.secondTeamMember + "" : "";
            $scope.projectMembers.thirdTeamMember  =  data.thirdTeamMember ?  data.thirdTeamMember + "" : "";




            if ( data.teamMembers && data.teamMembers.length > 0) {
                $scope.projectMembers.teamMembers  = data.teamMembers.map(function(v) { v.userId = v.userId + ''; return v;});
            }   
        }, function (error) {
            console.error(error);
        });
    }
    
    $scope.removeMember = function( index ) {
        $scope.projectMembers.teamMembers.splice(index,1);
    };

    $scope.addMember = function( index ) {
        $scope.projectMembers.teamMembers.push({
            userId : "",
            filteredUser  : false  
        });
    };

    $scope.selectedFilterList = function(userList, userListBySkill, value ) {
        var resultList =  userListBySkill;
        if(value){
            if(resultList && resultList.length > 0){
              for(var i = 0; i < $scope.qualifiedUsers.length; i++){
                   var index = resultList.findIndex(x => x.id == $scope.qualifiedUsers[i].id);
                    if(index > -1){
                        resultList[index].etuLevel = 0; 
                    }
                    
                }
            }
        }

       return resultList;
    };
    $scope.selectedFilterListOfCoach = function(userRemovedListBySkill, teamCoachBySkill, value){
        var resultList =  value ? userRemovedListBySkill: teamCoachBySkill;
        if(value){
            if(resultList && resultList.length > 0){
              for(var i = 0; i < $scope.qualifiedUsers.length; i++){
                   var index = resultList.findIndex(x => x.id == $scope.qualifiedUsers[i].id);
                    if(index > -1){
                        resultList[index].etuLevel = 0; 
                    }
                    
                }
            }
        }

       return resultList;
    };

    $scope.selectedFilterListOfMember = function(userRemovedListBySkill, teamMemberBySkill, value){
        var resultList =  value ? userRemovedListBySkill: teamMemberBySkill;
        if(value){
            if(resultList && resultList.length > 0){
              for(var i = 0; i < $scope.qualifiedUsers.length; i++){
                   var index = resultList.findIndex(x => x.id == $scope.qualifiedUsers[i].id);
                    if(index > -1){
                        resultList[index].etuLevel = 0; 
                    }
                    
                }
            }
        }

       return resultList;
    };

    
    
    $scope.filterSourceList = function (sourceName) {
        var list = $scope.mainLossesList;
        list  = list.filter(function(v,i) { return list[i].source == sourceName ; });
        $scope.allLosses = list; 
        $scope.basicInfoForm.lossType = null;     
    };

    $scope.unselectPillar = function(mainPillarId) {
        if ($scope.pillarInfoForm && $scope.pillarInfoForm.supportingPillars.length > 0) {
            for (let i in $scope.pillarInfoForm.supportingPillars) {
                if($scope.pillarInfoForm.supportingPillars[i] === mainPillarId)
                    $scope.pillarInfoForm.supportingPillars[i] = '';
            }
        } 
    };

    $scope.removeTeamLeader = function(teamLeaderId){
     
        $rootScope.teamMemberId = teamLeaderId;
        $scope.teamCoachBySkill = angular.copy(teamsCoach);
        teamsMember = angular.copy($scope.teamCoachBySkill);
        $scope.teamMemberBySkill = angular.copy(teamsMember);
        if($scope.userListBySkill.length > 0){
          
            for(var i=0;i<$scope.userListBySkill.length;i++){
                if($scope.userListBySkill[i].id == teamLeaderId){
                    $scope.removedUsersList[x] = $scope.userListBySkill[i];
                    x++;
                    $scope.teamCoachBySkill.splice(i,1); 
                      }
            }
        }

    };

    $scope.removeTeamCoach = function(teamCoachId){
      
        
        if($scope.teamCoachBySkill.length > 0){
          
            for(var i=0;i<$scope.teamCoachBySkill.length;i++){
            
                if($scope.teamCoachBySkill[i].id == teamCoachId){
                    $scope.removedUsersList[x] = $scope.teamCoachBySkill[i];
                    x++;
                
                    $scope.teamMemberBySkill.splice(i,1); 
                    $scope.emplEtuList= $scope.teamMemberBySkill;
                    
                    
                    
                    $scope.example1data = [];
                    $scope.teamCoachBySkill.filter(function(s){
                        var obj = {id:s.id,label:s.name}
                        $scope.example1data.push(obj);
                    });
                }
            }
        }

    };

    $scope.removeTeamMember = function(teamMemberId){
        var teamMember2 = angular.copy($scope.teamMemberBySkill);
        $scope.teamMemberBySkill2 = angular.copy(teamMember2);
        if($scope.teamMemberBySkill.length > 0){
            for(var i=0;i<$scope.teamMemberBySkill.length;i++){
            
                if($scope.teamMemberBySkill[i].id == teamMemberId){
                    $scope.removedUsersList[x] = $scope.teamMemberBySkill[i];
                    x++;
                
                    $scope.teamMemberBySkill2.splice(i,1); 
                  
                }
            }
         }
    };

    $scope.shiftRightToLeftMethods = function() {
        var tempList = angular.copy($scope.allMethodsList);
        var moreSelectedItem = tempList.filter(function(v){ return v.selected;});
        moreSelectedItem.map(x => x.selected = false);
        moreSelectedItem.map(x => x.requiredSkill = 3 );
        $scope.selectedMethodsList =  $scope.selectedMethodsList.concat(moreSelectedItem);
        tempList = angular.copy($scope.allMethodsList);
        $scope.allMethodsList  = tempList.filter(function(v){ return !v.selected;});
        };

    $scope.shiftLeftToRightMethods = function() {
       var tempList = angular.copy($scope.selectedMethodsList);
       var moreSelectedItem = tempList.filter(function(v){ return v.selected;});
       moreSelectedItem.map(x => x.selected = false);
       moreSelectedItem.map(x => x.requiredSkill = 3 );
       $scope.allMethodsList =  $scope.allMethodsList.concat(moreSelectedItem);
       tempList = angular.copy($scope.selectedMethodsList);
       $scope.selectedMethodsList  = tempList.filter(function(v){ return !v.selected;});
        };

    $scope.shiftRightToLeftTools = function() {
       var tempList = angular.copy($scope.allToolsList);
       var moreSelectedItem = tempList.filter(function(v){ return v.selected;});
       moreSelectedItem.map(x => x.selected = false);
       moreSelectedItem.map(x => x.requiredSkill = 3 );
       $scope.selectedToolsList =  $scope.selectedToolsList.concat(moreSelectedItem);
       tempList = angular.copy($scope.allToolsList);
       $scope.allToolsList  = tempList.filter(function(v){ return !v.selected;});
        };

    $scope.shiftLeftToRightTools = function() {
       var tempList = angular.copy($scope.selectedToolsList);
       var moreSelectedItem = tempList.filter(function(v){ return v.selected;});
       moreSelectedItem.map(x => x.selected = false);
       $scope.allToolsList =  $scope.allToolsList.concat(moreSelectedItem);
       tempList = angular.copy($scope.selectedToolsList);
       $scope.selectedToolsList  = tempList.filter(function(v){ return !v.selected;});
        };

    $scope.shiftRightToLeftTechTools = function() {
       var tempList = angular.copy($scope.technicalToolList);
       var moreSelectedItem = tempList.filter(function(v){ return v.selected;});
       moreSelectedItem.map(x => x.selected = false);
       moreSelectedItem.map(x => x.requiredSkill = 3 );
       $scope.selectedTechnicalToolList =  $scope.selectedTechnicalToolList.concat(moreSelectedItem);
       tempList = angular.copy($scope.technicalToolList);
       $scope.technicalToolList  = tempList.filter(function(v){ return !v.selected;});
        };

    $scope.shiftLeftToRightTechTools = function() {
       var tempList = angular.copy($scope.selectedTechnicalToolList);
       var moreSelectedItem = tempList.filter(function(v){ return v.selected;});
       moreSelectedItem.map(x => x.selected = false);
       $scope.technicalToolList =  $scope.technicalToolList.concat(moreSelectedItem);
       tempList = angular.copy($scope.selectedTechnicalToolList);
       $scope.selectedTechnicalToolList  = tempList.filter(function(v){ return !v.selected;});
        };

    function getMethodsAndPillar(){
        projectService.getAllSelectedPillarMethodsAndTools($scope.projectId).then(function (response) {
            var list = groupBy( response.data||[], function(item) {
                return item.id;
            });
            $scope.totalMethodToolsList = list;
            $scope.allToolsList = list.filter(function(v){ return v.type === "COMMON TOOLS";}); 
            $scope.allMethodsList = tableInitialization(list.filter(function(v){ return v.type === "COMMON METHODS";}));
            $scope.toolTypeChange("COMMON TOOLS");
            $scope.methodTypeChange("COMMON METHODS");
        }, function (error) {
            console.error(error);
        });
        }

    function groupBy( array , f ){
        var groups = {};
        array.forEach( function( o ) {
        var group = JSON.stringify( f(o) );
            if (!groups[group]) {
                groups[group] = o;
                groups[group].pillarListName = [o.pillarName];
            } else {
                 groups[group].pillarListName.push(o.pillarName);
            }
        });
        return Object.keys(groups).map( function( group ){
          return groups[group]; 
        });
      }

  

     function getProjectMethodsAndTool ()  {
        projectService.getProjectMethodsAndTool($scope.projectId).then(function (response) {
            var list = response.data;
            $scope.selectedToolsList = list.filter(function(v){ return v.type === "COMMON TOOLS";});  
            $scope.selectedMethodsList = list.filter(function(v){ return v.type === "COMMON METHODS";});
            $scope.toolTypeChange("COMMON TOOLS");
            $scope.methodTypeChange("COMMON METHODS");
        }, function (error) {
            console.error(error);
        });
    }

    function getAllTechnicalToolList ()  {
        var deferred = $q.defer();
        deferred.notify();
        projectService.getAllTechnicalToolList($scope.projectId).then(function (response) {
            deferred.resolve(response.data);
        }, function (error) {
            console.error(error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getProjectTechnicalTool() {
        getAllTechnicalToolList().then(data => {
            $scope.technicalToolList = data || [];
            return projectService.getProjectTechnicalTool($scope.projectId);
        }).then(function (response) {
            $scope.selectedTechnicalToolList = [];
            if (response.code == "200" && response.data != null ) {
                var selectedData = response.data;
                selectedData.forEach(function(item){
                    $scope.technicalToolList.some(function(item2,index) {
                        if ( item.technicalToolId == item2.technicalToolId ) {
                            var tempData = item2;
                            tempData.requiredSkill = item.requiredSkill;
                            $scope.selectedTechnicalToolList.push(tempData);
                            $scope.technicalToolList.splice(index,1);
                            return true;
                        }
                        return false;
                    });
                });
            }
        }, function (error) {
            console.error(error);
        });
    }



    function getAllBenifitTypeList() {
        var deferred = $q.defer();
        deferred.notify();
        projectService.getAllBenifitTypeList().then(function (response) {
            deferred.resolve(response.data);
        }, function (error) {
            console.error(error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getProjectCostingData() {
        getAllBenifitTypeList().then(data=>{
            $scope.allBenifitList = data || [];
            return projectService.getProjectCostingData($scope.projectId);
        }).then(function (response) {
            if (response.code == "200" && response.data != null ) {
                $scope.benifitData = response.data;
                for(var i in $scope.allBenifitList ){
                    $scope.benifitData.benifitList.some(item=>{
                        if ( item.id == $scope.allBenifitList[i].id ) {
                            $scope.allBenifitList[i] = item;
                            return true;
                        }
                        return false;
                    });
                }
                if ( $scope.benifitData && $scope.benifitData.bc < 1 ) {
                    $scope.projectCostingBcError = 'NO';
                    $scope.projectCostingBcErrorPanel = true;
                } else {
                    $scope.projectCostingBcError = 'YES';
                    $scope.projectCostingBcErrorPanel = false;
                }
                delete  $scope.benifitData.benifitList;
            }
        }, function (error) {
            console.error(error);
        });

        projectService.getProjectInformation($scope.projectId).then(function(response){

            $rootScope.etuStatus = response.data.etuStatus;
            $rootScope.financeStatus = response.data.financeStatus;
       
           
        },function(error){
            console.log(error);
        });
    }

    function getProjectPlanningDates() {
        projectService.getProjectPlanningDates($scope.projectId).then(function (response) {
            if (response.code =="200" && response.data) {
                var data = response.data;
                if (data.targetDate && data.startDate) {
                    var startDate = moment(data.startDate);
                    var endDate = moment(data.targetDate);
                    $scope.projectPlan.startDate = new Date(data.startDate);
                    $scope.minProjectDate = $scope.projectPlan.startDate;
                    $scope.projectPlan.endDate = new Date(data.targetDate);
                    /*var plans = getPlanningTable(startDate, endDate);
                    plans.forEach(function(weekDates,mainIndex) {
                        weekDates.weekOfMonth.forEach(function(item,subIndex) {
                            if(isBetweenDate(data.pPlannedStartDate, data.pPlannedEndDate, item.weekEndDate) ) {
                               plans[mainIndex].weekOfMonth[subIndex].status = "P"; 
                            } else if(isBetweenDate(data.dPlannedStartDate, data.dPlannedEndDate, item.weekEndDate)) {
                               plans[mainIndex].weekOfMonth[subIndex].status = "D"; 
                            } else if( isBetweenDate(data.cPlannedStartDate, data.cPlannedEndDate, item.weekEndDate)) {
                                plans[mainIndex].weekOfMonth[subIndex].status = "C"; 
                            } else if(isBetweenDate(data.aPlannedStartDate, data.aPlannedEndDate, item.weekEndDate)) {
                                plans[mainIndex].weekOfMonth[subIndex].status = "A"; 
                            }

                            if(isBetweenDate(data.pActualStartDate, data.pActualEndDate, item.weekEndDate) ) {
                               plans[mainIndex].weekOfMonth[subIndex].actualStatus = "P"; 
                            } else if(isBetweenDate(data.dActualStartDate, data.dActualEndDate, item.weekEndDate)) {
                               plans[mainIndex].weekOfMonth[subIndex].actualStatus = "D"; 
                            } else if( isBetweenDate(data.cActualStartDate, data.cActualEndDate, item.weekEndDate)) {
                                plans[mainIndex].weekOfMonth[subIndex].actualStatus = "C"; 
                            } else if(isBetweenDate(data.aActualStartDate, data.aActualEndDate, item.weekEndDate)) {
                                plans[mainIndex].weekOfMonth[subIndex].actualStatus = "A"; 
                            }
                           
                        });
                    });*/

                   if (data && data.aPlannedEndDate) {
                      if(moment($scope.basicInfoForm.completionDate) < moment(data.aPlannedEndDate)){
                        endDate = moment(data.aPlannedEndDate);
                      }
                    }

                   if (data && data.pPlannedStartDate) {
                      if( startDate > moment(data.pPlannedStartDate) ){
                        startDate = moment(data.pPlannedStartDate);
                      }
                   }

                    $scope.projectPlan.planTable = getSettedPlanTable (data,startDate, endDate) ;
                    $scope.projectPlan.executionPlanTable =[]; 
                    if ( $scope.isPlanningCompleted ){
                        endDate = endDate.add(3,'month').endOf('month');
                        $scope.projectPlan.executionPlanTable = getSettedPlanTable (data,startDate, endDate) ;  
                    }
                } else {
                    var startDate = moment($scope.projectPlan.startDate).startOf('month');
                    var endDate = moment($scope.projectPlan.endDate).endOf('month');
                    $scope.projectPlan.planTable = getPlanningTable(startDate, endDate);
                }
            }  else {
                var startDate = moment($scope.projectPlan.startDate).startOf('month');
                var endDate = moment($scope.projectPlan.endDate).endOf('month');
                $scope.projectPlan.planTable = getPlanningTable(startDate, endDate); 
            }         
        }, function (error) {
            console.error(error);
        });
    }

    function getSettedPlanTable (data,startDate, endDate) {
        var plans = getPlanningTable(startDate, endDate);
        plans.forEach(function(weekDates,mainIndex) {
         
                        weekDates.weekOfMonth.forEach(function(item,subIndex) {
                if(isBetweenDate(data.pPlannedStartDate, data.pPlannedEndDate, item.weekEndDate) ) {
                   plans[mainIndex].weekOfMonth[subIndex].status = "P"; 
                } else if(isBetweenDate(data.dPlannedStartDate, data.dPlannedEndDate, item.weekEndDate)) {
                   plans[mainIndex].weekOfMonth[subIndex].status = "D"; 
                } else if( isBetweenDate(data.cPlannedStartDate, data.cPlannedEndDate, item.weekEndDate)) {
                    plans[mainIndex].weekOfMonth[subIndex].status = "C"; 
                } else if(isBetweenDate(data.aPlannedStartDate, data.aPlannedEndDate, item.weekEndDate)) {
                    plans[mainIndex].weekOfMonth[subIndex].status = "A"; 
                }

                if(isBetweenDate(data.pActualStartDate, data.pActualEndDate, item.weekEndDate) ) {
                   plans[mainIndex].weekOfMonth[subIndex].actualStatus = "P"; 
                } else if(isBetweenDate(data.dActualStartDate, data.dActualEndDate, item.weekEndDate)) {
                   plans[mainIndex].weekOfMonth[subIndex].actualStatus = "D"; 
                } else if( isBetweenDate(data.cActualStartDate, data.cActualEndDate, item.weekEndDate)) {
                    plans[mainIndex].weekOfMonth[subIndex].actualStatus = "C"; 
                } else if(isBetweenDate(data.aActualStartDate, data.aActualEndDate, item.weekEndDate)) {
                    plans[mainIndex].weekOfMonth[subIndex].actualStatus = "A"; 
                }
               
            });
        });
        return plans || [];
    }

    function isBetweenDate(sd, ed, cd){
        if ( sd && ed && cd) {
           var startDate = moment(sd);
           var endDate   = moment(ed);
           var date  = moment(cd);
           return date.isBetween(startDate, endDate) || date.isSame(startDate) || date.isSame(endDate);
        }

       return false;
    }

    $scope.methodTypeChange = function (type) {
        var list = $scope.totalMethodToolsList.filter(function(obj) {
            return !$scope.selectedMethodsList.some(function(obj2) {
                return obj.id == obj2.id;
            });
        });
       // var list = $scope.totalMethodToolsList;
        $scope.allMethodsList = list.filter(function(v){ return v.type === type;});
    };

    $scope.toolTypeChange = function (type) {
        //var list = $scope.totalMethodToolsList;
        var list = $scope.totalMethodToolsList.filter(function(obj) {
            return !$scope.selectedToolsList.some(function(obj2) {
                return obj.id == obj2.id;
            });
        });
        $scope.allToolsList = list.filter(function(v){ return v.type === type;});
    };

    function tableInitialization (data) {
        data = data || [];
        var tableParams = new NgTableParams({
                page: 1, // show first page
                total: 1,
                noPager: true, // hides pager
                filter: {
                    levelFirstTool: "" // initial filter
                },
                count:  data.length
                },{
                    counts:[],
                    total: data.length, // length of data
                    dataset: data            
                });
        return tableParams;
    }

    $scope.onProjectCostChange= function() {
        var totalCount = 0;
        $scope.allBenifitList.forEach(item=>{
            if ( item.benifitValue ) {
               totalCount = totalCount + item.benifitValue;
            }
        });
        $scope.benifitData.totalProjectCost = totalCount;
        var per = $scope.benifitData.confidenceLevel ? $scope.benifitData.confidenceLevel / 100 : 1; 
        if ( $scope.benifitData.totalPotentialBenefit &&  $scope.benifitData.totalProjectCost) {
            $scope.benifitData.bc = parseFloat(( ($scope.benifitData.totalPotentialBenefit / $scope.benifitData.totalProjectCost ) * per ).toFixed(2));
        } else {
            $scope.benifitData.bc = 0;
        }

        if ( $scope.benifitData && $scope.benifitData.bc < 1 ) {
            $scope.projectCostingBcErrorPanel = true;
        } else {
            $scope.projectCostingBcErrorPanel = false;
        }
    };

    $scope.isValidCostingForm = function (projectCostingBcErrorPanel, projectCostingBcError) {
        if (!projectCostingBcErrorPanel ) return false;
        if ( projectCostingBcError == 'NO' ) {
            return true;
        };
        return false;
    }

    $scope.enableSavingButton = function (value) {
        $scope.projectCostingBcError = value;
    };

    $scope.updatePlanningTable = function() {
        if ($scope.projectPlan.startDate && $scope.projectPlan.endDate) {
            var ds = moment($scope.projectPlan.startDate);
            var de = moment($scope.projectPlan.endDate);
            if (de.isAfter(ds) || ds.isSame(de)) {
                var startDate = moment($scope.projectPlan.startDate).format('MM-DD-YYYY');
                var stDate = moment($scope.projectPlan.startDate);
                var checkDate = new Date(moment($scope.projectPlan.startDate));
                var date = checkDate.getDate();
              var day = stDate.day();  
                if(day > 3 || day == 0   && date > 4){
                var startDate = moment($scope.projectPlan.startDate).subtract(4, "days").format('MM-DD-YYYY');
                }
              
                var endDate = moment($scope.projectPlan.endDate);
                $scope.projectPlan.planTable = getPlanningTable(startDate, endDate);   
            } 
        }  else {
            $scope.projectPlan.planTable = [];
        }
    };

    function getPlanningTable(startDate, endDate) {
        var plans = []; 
        if (startDate && endDate) {
            var month = moment(startDate).startOf('month'); 
            while( month <= endDate ) {
                plans.push(
                    {   
                        monthStartDate : month.startOf('month').format('MM-DD-YYYY'),
                        monthEndDate : month.endOf('month').format('MM-DD-YYYY'),
                        colorClass : month.format('MMM') ,
                        label : month.format('MMM-YY') , 
                        //weekOfMonth : getMonthMondays(startDate, endDate, month)
                        weekOfMonth : getMonthWednesdays(startDate, endDate, month)
                    } 
                );
                month = month.add(1, "month").startOf('month');
            }
        }
        return plans;
    };

    // function getMonthMondays (startDate, endDate, date) {
    //   //  var monday = moment(date).startOf('month').day("Wednesday");
    //     var monday = moment(date).startOf('month').day("monday");
    //     if ( monday.date() > 7 ) monday.add(7,'d');
    //     var month = monday.month();
    //     var mondayList = [];
    //     var count = 1;
    //     while( month === monday.month() ) {
    //         if (isBetweenDate(startDate, endDate, monday)) {
    //             var data = {
    //                 status : 'X',
    //                 actualStatus : 'X',
    //                 weekNo : count,
    //                 weekStartDate :  moment(monday).format('MM-DD-YYYY'),
    //                 weekEndDate: getWeekLastDate(month, monday)
                    
    //             };
    //             mondayList.push(data);
    //         }
    //         count++;
    //         monday.add(7,'d');
    //     }
    //     return mondayList;
    // }

    function getMonthWednesdays (startDate, endDate, date) {
          var Wednesday = moment(date).startOf('month').day("Wednesday");
        //  var monday = moment(date).startOf('month').day("monday");
          if ( Wednesday.date() > 7 ) Wednesday.add(7,'d');
          var month = Wednesday.month();
          var WednesdayList = [];
          var count = 1;
          while( month === Wednesday.month() ) {
              if (isBetweenDate(startDate, endDate, Wednesday)) {
                  var data = {
                      status : 'X',
                      actualStatus : 'X',
                      weekNo : count,
                      weekStartDate :  moment(Wednesday).format('MM-DD-YYYY'),
                      weekEndDate: getWeekLastDate(month, Wednesday)
                      
                  };
                  WednesdayList.push(data);
              }
              count++;
              Wednesday.add(7,'d');
          }
          return WednesdayList;
      }



    function getWeekLastDate(month, Wednesday) {
        if(moment(Wednesday).endOf('month') > moment(Wednesday).add(6,'d')){
            return moment(Wednesday).add(6,'d').format('MM-DD-YYYY');
        }else{
            return moment(Wednesday).endOf('month').format('MM-DD-YYYY');
        }
    }                
    function getFMatrxMonthsTable(startDate,totalYearlyPotential) {
        var fMatrix = []; 
        if (startDate) {
            var month = moment(startDate).add(1,'d'); 
            var endDate = moment(startDate).add(1,'d').add(1,'year');
            var totalWorkingDays = endDate.diff(month, 'days');
            var perDaySaving = ( totalYearlyPotential / totalWorkingDays ) || 0;
            while( month <= endDate ) {
                var startOfMonthDate = moment(month).startOf('month') ;
                if (startOfMonthDate.isBefore( moment(startDate))) {
                   startOfMonthDate = month;
                }
                var endOfMonthDate = moment(month).endOf('month');
                if (  endOfMonthDate.isAfter(moment())) {
                    if (endOfMonthDate.format('MM') == moment().format('MM') && endOfMonthDate.format('YYYY')== moment().format('YYYY') ) {
                        endOfMonthDate = moment();
                    } else {
                        endOfMonthDate = month;
                    }
                }
                var workingDaysInMonth = endOfMonthDate.diff(startOfMonthDate, 'days') ;
                workingDaysInMonth = workingDaysInMonth !== null && workingDaysInMonth > 0 ? workingDaysInMonth : 0; 
                fMatrix.push(
                    {   
                        monthStartDate : month.startOf('month').format('MM-DD-YYYY'),
                        monthEndDate : month.endOf('month').format('MM-DD-YYYY'),
                        colorClass : month.format('MMM') ,
                        label : month.format('MMM-YY') ,
                        savingDays :  workingDaysInMonth,
                        expectedSaving :   null ,
                        actualSavingValue : null
                    } 
                );
                month.add(1, "month");
                month = month.startOf('month');
            }
        }
        return fMatrix;
    }

    $scope.saveProjectActualSavings = function () {
        // if ( $scope.fMatrix && $scope.fMatrix.list.length > 0) {
        //     var savingList = [];
        //      $scope.fMatrix.list.forEach((monthSaving)=>{
        //         var data =  {
        //             actualSavingValue : monthSaving.actualSavingValue,
        //             month : moment(monthSaving.monthStartDate).format('MM'),
        //             year : moment(monthSaving.monthStartDate).format('YYYY')
        //         };
        //         savingList.push(data);

        //      });

        //     projectService.saveProjectActualSavings($scope.projectId, savingList).then(function(response){
        //         if (response.code="200") {
        //             // $location.path("app/project/"+$scope.projectId+"/8");
        //             $state.go('app.dashboard_main');
        //         }
        //     },function (error) {
        //         console.error(error);
        //     });
            
        // }
        $state.go('app.dashboard_main');
    }

    function getProjectActualSavings() {
        var planningData = {};
        projectService.getProjectPlanningDates($scope.projectId).then(function (response) {
            planningData = response.data;
            return projectService.getProjectActualSavings($scope.projectId);
        }).then(function(response){
            if ( response.code == "200" ) {
                if ( response.data.length > 0 ) {
                    var savingsList = response.data;
                   $scope.fMatrix.list = getFMatrxMonthsTable($scope.basicInfoForm.completionDate,$scope.basicInfoForm.yearlyPotential);    
                    savingsList.forEach((item) =>{
                        $scope.fMatrix.list.forEach((data,index) => {
                            var d = moment().date(1).month(item.month-1).year(item.year).format('MM-DD-YYYY'); 
                            if ( d == data.monthStartDate && ( moment(data.monthStartDate).isSame(moment()) || moment(data.monthStartDate).isBefore(moment()) )) {
                                $scope.fMatrix.list[index].actualSavingValue = item.actualSavingValue;
                                $scope.fMatrix.list[index].expectedSaving = item.actualProjectedValue;
                                return true;
                            }
                            return false;
                        });
                        calculateTotal ();
                    });
                } else {
                    $scope.fMatrix.list = getFMatrxMonthsTable($scope.basicInfoForm.completionDate,planningData.totalPotentialBenefit);    
                    calculateTotal ();
                }
            } else {

            }
        },function (error) {
            console.error(error);
        });
    } 
    $scope.totalAttSaving = 0;
    $scope.totalActualSaving = 0;
    function calculateTotal () {

        $scope.fMatrix.list.forEach((data,index) => {
            $scope.totalAttSaving += data.expectedSaving >=0 ? data.expectedSaving : 0;
            $scope.totalActualSaving += data.actualSavingValue >=0 ? data.actualSavingValue : 0;
        });    
    }


    $scope.statuses = [
        {
            value: '',
            text: ''
        },{
            value: 'P',
            text: 'P'
        }, {
            value: 'D',
            text: 'D'
        }, {
            value: 'C',
            text: 'C'
        }, {
            value: 'A',
            text: 'A'
        }
    ];

    $scope.showStatus = function (data) {
        return data.status ? data.status : "X";
    };

    $scope.weekStartDateBefore;
    $scope.weekEndDateBefore;
    var isValidDateProjectSave = true;
    $scope.showActualStatus = function (data) {
        if(data.actualStatus =='A'){
            if(isValidDateProjectSave){
                 $scope.weekStartDateBefore = new Date(data.weekStartDate);
                 $scope.weekEndDateBefore = new Date(data.weekEndDate);
                isValidDateProjectSave = false;
                
            }       
                
        }
        return data.actualStatus ? data.actualStatus : "X";
    };
    $scope.validDate = true;
    $scope.changProjectCompletionDate = function(changingDate){
      
        var beforeSeven = new Date(changingDate).getTime();
        var weekStart = new Date($scope.weekStartDateBefore).getTime();
        var wkStart = new Date($scope.weekStartDateBefore).getDay();
        var wkEnd = new Date($scope.weekEndDateBefore).getDay();
        var weekEnd = new Date($scope.weekEndDateBefore).getTime();
        var month_name = function(dt){
           var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
              return mlist[dt.getMonth()];
            };
        if(beforeSeven <= weekEnd && beforeSeven >= weekStart){
            $rootScope.beforeSevenStepDate = changingDate; 
          
            $scope.validDate = true; 
        }else{
            $scope.validDate = false;
            $rootScope.beforeSevenStepDate = $scope.completionForm.completionDate;
           alert("Please enter closure date between "+$scope.weekStartDateBefore.getDate()+"/"+month_name($scope.weekStartDateBefore)+"/"+$scope.weekStartDateBefore.getFullYear()+" and "+$scope.weekEndDateBefore.getDate()+"/"+month_name($scope.weekEndDateBefore)+"/"+$scope.weekEndDateBefore.getFullYear());
        }                                                                                                                                                   
    };

    $scope.getTotalActualSpending = function (data) {
        var total = 0;
        if (data && data.length > 0) {
           data.forEach(value=>{
            total += (value.actualSavingValue || 0); 
           });
           return total;
        } else 
        {
            return 0;
        }
    };
    
    $scope.getTotalProtectedSpending = function (data) {
        var total = 0;
        if (data && data.length > 0) {
           data.forEach(value=>{
            total += (value.expectedSaving || 0); 
           });
           return total;
          //data
        } else {
            return 0;
            
        }
    };

    $scope.openModal = function(size) {
        var projectCostingCtrlInstance = $uibModal.open({
            templateUrl : './assets/views/project/ProjectCostingContent.html',
            controller : 'ProjectLossCostingCtrl',
            size : size,
            resolve : {
                project : function (){
                    return $scope.basicInfoForm;
                }
            }
        });

        projectCostingCtrlInstance.result.then(function(project) {
            $scope.basicInfoForm = project;
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };
    
    $scope.startExecutionPlan = function() {

        var confirmationDialogCtrlInstance = $uibModal.open({
            templateUrl : './assets/views/project/ConfirmationDialog.html',
            controller : 'ConfirmationDialogCtrl',
            size : 'sm',
            resolve : {
                data : function () {
                    return { headerTitle : "Execution Start" , textMessage :  "Are you want to start execution?" };
                }
               
            }
        });

        confirmationDialogCtrlInstance.result.then(function(value) {
            //$rootScope.loaderAction = true;
            updateProjectPlanning().then(function(response){
                    if(response) {
                        projectService.startExecutionPlan($scope.projectId).then(function(response){
                            if ( response.code == "200" ) {
                                $scope.isPlanningCompleted = true;
                                getProjectPlanningDates();
                                $rootScope.loaderAction = false;
                            }
                        },function (error) {
                            console.error(error);
                            $rootScope.loaderAction = false;
                        });
                    } 
                }).catch(function(response){
                $rootScope.loaderAction = false;
                });
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
        
    };

     $scope.openRemarkModal = function(data,size) {
        data.projectId = $scope.projectId;
        var projectPlanRemarkCtrlInstance = $uibModal.open({
            templateUrl : './assets/views/project/ProjectPlanRemark.html',
            controller : 'ProjectPlanRemarkCtrl',
            size : size,
            resolve : {
                planDetails : function (){
                    return data;
                }
            }
        });

        projectPlanRemarkCtrlInstance.result.then(function(planRemarkDetails) {
           
        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };


     $scope.trainingMail = function(size) {
        var data = {};
        data.projectId = $scope.projectId;
        data.userList  = $scope.userList ;
        var trainingMailCtrlInstance = $uibModal.open({
            templateUrl : './assets/views/project/TrainingMail.html',
            controller : 'TrainingMailCtrl',
            size : size,
            resolve : {
                data : function (){
                    return data;
                }
            }
        });

        trainingMailCtrlInstance.result.then(function(resDetails) {

        }, function() {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };
    

    $scope.checkProject = function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            event.stopPropagation();
            //findProjectByProjectCode();
        }
    };

    $scope.addPlanRemark = function (planData) {

    };


    $scope.updateSkillLevel = function () {
        $rootScope.loaderAction = true;
        projectService.getSkillUpdationData($scope.projectId).then(function(response){
            if ( response.code == "200" ) {
                var userSkillLevelUpgradeCtrlInstance = $uibModal.open({
                    templateUrl : './assets/views/project/UserSkillLevelUpgrade.html',
                    controller : 'UserSkillLevelUpgrade',
                    size : 'lg',
                    windowClass : 'user-skill-level-window',
                    resolve : {
                        data : function (){
                            return response.data;
                        }
                    }
                });

                userSkillLevelUpgradeCtrlInstance.result.then(function(project) {
                }, function() {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            }
            $rootScope.loaderAction = false;
        },function (error) {
            console.error(error);
            $rootScope.loaderAction = false;
        });
       
    };



    function findProjectByProjectCode() {
        if ( $scope.basicInfoForm && $scope.basicInfoForm.projectCode ) {
            projectService.findProjectByProjectCode($scope.basicInfoForm.projectCode).then(function (response) {
                if (response.code == "200" ) {
                    $scope.projectId = response.data.id;
                    $scope.basicInfoForm = response.data;
                    $scope.basicInfoForm.process = $scope.basicInfoForm.process.id + "";
                    $scope.basicInfoForm.optMachine = $scope.basicInfoForm.optMachine.id + "";
                    $scope.basicInfoForm.etu = $scope.basicInfoForm.etu.id + "";
                    var lossData = $scope.basicInfoForm.lossType;
                    $scope.basicInfoForm.lossType = $scope.basicInfoForm.lossType.id + "";
                    $scope.changeLoss(lossData , true);
                    $scope.basicInfoForm.projectCD = $scope.basicInfoForm.projectCD;
                    $scope.basicInfoForm.completeStage = $scope.basicInfoForm.completeStage;
                    $scope.benifitData.totalPotentialBenefit = $scope.basicInfoForm.yearlyPotential;
                    $scope.totalYearlyPotential = $scope.basicInfoForm.yearlyPotential;
                    
                    $scope.completeStage = $scope.basicInfoForm.completeStage;
                   
                    // if ($scope.currentStep > $scope.completeStage) {
                    //     $location.path("app/project/"+$scope.projectId+"/" + ($scope.completeStage+1));
                    // }
                    // if ($scope.basicInfoForm.completionDate != null) {
                    //     $scope.completionForm.completionDate = new Date($scope.basicInfoForm.completionDate);
                    // }
                    // $scope.completionForm.horizontalExpansion = $scope.basicInfoForm.horizontalExpansion;
                    // $scope.completionForm.remark = $scope.basicInfoForm.remark;
                    
                   //  if ( $scope.currentStep - 1 <= $scope.completeStage ) {
                   //    loadStageData();
                   //  }
                   // $scope.isPlanningCompleted = $scope.basicInfoForm.executionStarted || false;
                } else {
                    $location.path("app/project");
                }
            }, function (error) {
                console.error(error);
            });
        }
    }
    
    function loadData () {
        if ($scope.currentStep === 1 || !$scope.currentStep ) {
            getAllLossGroup();
            getAllMachine();
            getAllETUType();
            getAllProcess();
            getAllCDYear();
        }
        
        if ( !isNaN(parseInt($scope.projectId)))  {
            getProjectInformation();
        }
    }


    function loadStageData () {
        if ($scope.currentStep === 2 ) {
            getAllPillars();
            getKPIsList();
            getProjectPillars();
        }
       
        if ($scope.currentStep === 3) {
            getMethodsAndPillar();
            getProjectMethodsAndTool();
            getProjectTechnicalTool();
        }

        if ($scope.currentStep === 4 ) {
            getUserList();
            $scope.getUserListBySkill();
            getProjectTeamMembers();
        }

        if ($scope.currentStep === 5 ) {
            //getAllBenifitTypeList();
            getProjectCostingData();
        }

        if ( $scope.currentStep === 6  ) {
            getProjectPlanningDates();
        }

        if ( $scope.currentStep === 8  ) {
            getProjectActualSavings();
        }
    }

    
    $scope.changeLoss = function(lossId , onInit){
        if(onInit){
            $scope.sMatrixLossTypeSelected = lossId.lossType == "S MATRIX";
        }else{
            $scope.selectedLossType = $scope.allLosses.filter(function(data) {
                return data.id == lossId;
            });
            $scope.sMatrixLossTypeSelected = $scope.selectedLossType[0] && $scope.selectedLossType[0].lossType == "S MATRIX" || false;
            $scope.basicInfoForm.yearlyPotential = $scope.sMatrixLossTypeSelected ? 0 : $scope.basicInfoForm.yearlyPotential;
        }
   }
    loadData();

}]);

app.controller('ProjectLossCostingCtrl', ["$scope", "$uibModalInstance", "project",
function($scope, $uibModalInstance, project) {
    $scope.project = project;
    $scope.ok = function() {
        $scope.project.yearlyPotential =  $scope.project.directLoss ? $scope.project.directLoss : 0 ;
        $scope.project.yearlyPotential +=  $scope.project.indirectLoss ? $scope.project.indirectLoss : 0 ;
        $scope.project.yearlyPotential +=  $scope.project.staffLoss ? $scope.project.staffLoss : 0 ;
        $scope.project.yearlyPotential +=  $scope.project.indirectMaterialsLoss ? $scope.project.indirectMaterialsLoss : 0 ;
        $scope.project.yearlyPotential +=  $scope.project.maintenanceLoss ? $scope.project.maintenanceLoss : 0 ;
        $scope.project.yearlyPotential +=  $scope.project.scrapLoss ? $scope.project.scrapLoss : 0 ;
        $scope.project.yearlyPotential +=  $scope.project.energyLoss ? $scope.project.energyLoss : 0 ;
        $scope.project.yearlyPotential +=  $scope.project.expensessLoss ? $scope.project.expensessLoss : 0 ;
        $scope.project.yearlyPotential  = $scope.project.yearlyPotential ? $scope.project.yearlyPotential : null;
        $uibModalInstance.close($scope.project);
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
}]);

app.controller('ConfirmationDialogCtrl', ["$scope", "$uibModalInstance",'data',
function($scope, $uibModalInstance,data) {
    $scope.headerTitle= data.headerTitle || "";
    $scope.textMessage= data.textMessage || "";
    $scope.ok = function() {
        $uibModalInstance.close(true);
        
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
}]);

app.controller('ConfirmationDialogCtrl1', ["$scope","$state", "$uibModalInstance",'data',
function($scope,$state, $uibModalInstance,data) {
    $scope.headerTitle= data.headerTitle || "";
    $scope.textMessage= data.textMessage || "";
    var planningData = data.data;
    $scope.ok = function() {
        if ( !planningData ) {
            $uibModalInstance.close(true);
        }   else if (planningData.aActualStartDate != null && planningData.aActualEndDate != null  ) {
            $uibModalInstance.close(true);
        }   else {
            $scope.errorMessageExc  = "Please set stage A to complete execution.";
        }
    };

    $scope.cancel = function() {
        $uibModalInstance.close(false);
      
    };
    $scope.sendEmail = function(){
        $uibModalInstance.close(false);
        backToHome();
    };
    var backToHome = function(){
        $state.go('app.dashboard_main');        
    }
}]);

app.controller('UserSkillLevelUpgrade', ["$rootScope","$scope", "$uibModalInstance",'data','projectService',
function($rootScope,$scope, $uibModalInstance,data,projectService) {
    $scope.userSkillList  = data;
    $scope.methodsAndToolsList = [];
    $scope.technicalToolsList = [];
    if ($scope.userSkillList.length > 0 ) {
        var tempData = $scope.userSkillList[0];
        $scope.methodsAndToolsList = tempData.methodAndTools.map(function(item){ return item.methodsTools.levelFirstTool + " " +item.methodsTools.levelSecondTool;});
        $scope.technicalToolsList = tempData.technicalTools.map(function(item){ return item.technicalTool.technicalToolDescription ;});
    }
    $scope.save = function() {
        var reqData = [];
        $rootScope.loaderAction = false;
        $scope.userSkillList.forEach(function(item,index){
            var upgradeUser = {
                userId : item.user.id,
                methodAndTools : [],
                technicalTools : [],

            };   
           (item.methodAndTools||[]).forEach(function(item1,index){
                if ( item1.updatedSkill >= 0  && item1.actualSkill !=  item1.updatedSkill ) {
                    item1.methodsTools.updatedSkill = item1.updatedSkill; 
                    item1.methodsTools.comment = item1.comment; 
                    upgradeUser.methodAndTools.push(item1.methodsTools);
                }
           }); 
           (item.technicalTools||[]).forEach(function(item1,index){
                if ( item1.updatedSkill >= 0  && item1.actualSkill !=  item1.updatedSkill ) {
                    item1.technicalTool.updatedSkill = item1.updatedSkill; 
                    item1.technicalTool.comment = item1.comment; 
                    item1.technicalTool.technicalToolId = item1.technicalTool.id; 
                    upgradeUser.technicalTools.push(item1.technicalTool);
                }
           }); 
           reqData.push(upgradeUser);
        });
        projectService.upgradeUserSkill(reqData).then(function(response){
            if ( response.code == "200" ) {

            }
            $uibModalInstance.close(true);
            $rootScope.loaderAction = false;
        },function (error) {
            console.error(error);
            $rootScope.loaderAction = false;
        });
    };

    $scope.cancel = function() {
        $uibModalInstance.close(false);
    };
}]);

app.controller('TrainingMailCtrl', ["$scope", "$uibModalInstance","data",'projectService',
function($scope, $uibModalInstance, data, projectService) {
    $scope.data  = data;
    $scope.allUserList = data.userList || [];
    $scope.mailInfo = {
        mailSubject : 'About to Increase Employees Skill',
        mailDescription : '',
        upgradeSkillUserList : [{ userId : ''}]

    };

    $scope.removeMember = function( index ) {
        $scope.mailInfo.upgradeSkillUserList.splice(index,1);
    };

    $scope.addMember = function( index ) {
        $scope.mailInfo.upgradeSkillUserList.push({
            userId : ""
        });
    };

    $scope.send = function(form) {
        if (form.$valid) {
            form.$setPristine();
            $scope.mailInfo.upgradeSkillUserList = $scope.mailInfo.upgradeSkillUserList.map(function(d){ return d.userId !== null ? d.userId : null; });
            //$scope.mailInfo.upgradeSkillUserList = $scope.mailInfo.upgradeSkillUserList.filter(function(d){ return d.userId ;});
            projectService.sendTrainingMail(data.projectId, $scope.mailInfo).then(function (response) { 
           
            }, function (error) {
                console.error(error);
            });
            $uibModalInstance.close(true);
        } else {
            var field = null, firstError = null;
            for (field in form) {
                if (field[0] != '$') {
                    if (firstError === null && !form[field].$valid) {
                        firstError = form[field].$name;
                    }

                    if (form[field].$pristine) {
                        form[field].$dirty = true;
                    }
                }
            }
            angular.element('.ng-invalid[name=' + firstError + ']').focus();
        }
          
    };

    $scope.cancel = function() {
        $uibModalInstance.close(false);
    };
}]);


