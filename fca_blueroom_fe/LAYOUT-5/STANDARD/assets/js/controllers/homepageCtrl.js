'use strict';

app.controller('HomepageCtrl', ["$scope", "$rootScope", "$uibModal", "$timeout", "commonService", "$state", "matricesService", function ($scope, $rootScope, $uibModal, $timeout, commonService, $state ,matricesService) {
    
    $scope.gotoPage = function(route){
        if(route == 'app.masterData') {
            localStorage.setItem("isMasterData",true);
            localStorage.setItem("permissions_masterData", "Master Data,Machine Data,Daily Loss,Loss Report");
        } else {
            localStorage.removeItem("isMasterData");
            localStorage.removeItem("permissions_masterData");

            $rootScope.loaderAction = true;
            var tools = {};
            $rootScope.pdcaStatusModal = $uibModal.open({
               templateUrl : './assets/views/dashboard/pdcaProjects.html',
               controller : 'HomepageCtrl',
               size : 'lg',
               windowClass : 'projectStatusWindow',
               
               resolve : {
                data : function (){
                  return tools;
                }
              }
            });
            $rootScope.loaderAction = false;
        }
        $state.go(route);
    }


    $scope.cancel=function(){
      $rootScope.pdcaStatusModal.dismiss('cancel');
    }

    $scope.getPDCA_Status = function(){
      matricesService.getAllPendingPDCAProjectListByETU($rootScope.isSuperAdmin ? null :$rootScope.selectedETU.id).then(function(res){  
        $scope.projectnames = res.data.data;
      },
      function(err){
          console.log(err);
      });
    }

    $scope.loadInitData = function() {
        $rootScope.userId = $rootScope.userId ? $rootScope.userId : JSON.parse(localStorage.getItem("UserDetails")) ? JSON.parse(localStorage.getItem("UserDetails")).id : null;
        if($rootScope.userId) {
            commonService.getUserDetails($rootScope.userId).then(function(response){
                localStorage.setItem("UserDetails",JSON.stringify(response));
                $rootScope.userEtuList = response.etuList;

                if($rootScope.userEtuList.length > 0) {
                  $rootScope.selectedETU = $rootScope.userEtuList[0];
                  localStorage.setItem("ETU_value", JSON.stringify($rootScope.selectedETU));
                  $scope.getPDCA_Status();
                } else {
                  commonService.getAllETUType().then(function(response){
                    $rootScope.selectedETU = response.data[0];
                    localStorage.setItem("ETU_value", JSON.stringify($rootScope.selectedETU));
                    $scope.getPDCA_Status();
                  },function(error){
                    console.log(error);
                  });
                }

              },function(error){
                console.log(error);
            });//end of load getUserDetails()
        } else {
            $state.go('signin');
        }
    }//end of load projetcs()

}]);