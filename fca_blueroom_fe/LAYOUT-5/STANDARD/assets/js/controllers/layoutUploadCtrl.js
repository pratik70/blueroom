app.controller('LayoutUploadCtrl', ['$rootScope', '$scope', 'FileUploader', '$timeout',
function ($rootScope, $scope, FileUploader, $timeout) {

    $timeout(function(){
        $rootScope.pageHeaderTitle = "Layout Management";
    },10);

    $scope.layoutType = "";
    $scope.selectedCDYear="";

    if ( $rootScope.selectedETU ) {
        $scope.selectedETU = $rootScope.selectedETU ;
    }   else if ( localStorage.getItem( "ETU_value")) {
        $scope.selectedETU =  JSON.parse(localStorage.getItem( "ETU_value"));
    } else {
        $scope.selectedETU = {};
    }

    if ( $rootScope.selectedCD ) {
        $scope.selectedCD = $rootScope.selectedCD ;
    }   else if ( localStorage.getItem( "CD_value")) {
        $scope.selectedCD =  JSON.parse(localStorage.getItem( "CD_value"));
    } else {
        $scope.selectedCD = {};
    }


    var uploaderImages = $scope.uploaderImages = new FileUploader({
        url: $rootScope.baseURL + '/upload/layout'
    });

    // FILTERS

    uploaderImages.filters.push({
        name: 'imageFilter',
        fn: function (item/*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    // CALLBACKS

    uploaderImages.onWhenAddingFileFailed = function (item/*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploaderImages.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploaderImages.onAfterAddingAll = function (addedFileItems) {
        for(var i = 0; i < $scope.uploaderImages.queue.length-1; i++) {
          $scope.uploaderImages.queue[i].remove(); 
        }
    };
    uploaderImages.onBeforeUploadItem = function (item) {
        if ($scope.LayoutForm.$valid) {
            item.formData = [ { layoutType : $scope.layoutType, cdYear : $scope.selectedCDYear , etu : $scope.selectedETU.id } ];
        } else {
            item.cancel();
            showFromFieldError($scope.LayoutForm);
        }
    };
    uploaderImages.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploaderImages.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploaderImages.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploaderImages.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploaderImages.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploaderImages.onCompleteItem = function (fileItem, response, status, headers) {
        if ( fileItem.isSuccess ) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        }
    };
    uploaderImages.onCompleteAll = function () {
        console.info('onCompleteAll');
    };

    function showFromFieldError(form) {
        if (form) {
            var field = null, firstError = null;
            for (field in form) {
                if (field[0] != '$') {
                    if (firstError === null && !form[field].$valid) {
                        firstError = form[field].$name;
                    }

                    if (form[field].$pristine) {
                        form[field].$dirty = true;
                    }
                }
            }
            angular.element('.ng-invalid[name=' + firstError + ']').focus();
        }
    }
}]);