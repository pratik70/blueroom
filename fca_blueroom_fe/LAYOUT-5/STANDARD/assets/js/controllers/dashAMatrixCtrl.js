app.controller('DashAMatrixCtrl', ['$rootScope', '$scope', 'dashboardService', 'moment',
	function ($rootScope, $scope, dashboardService, moment) {
    $scope.aMatrixDataList = [];
    function getAllLossGroup() {
      $rootScope.loaderAction = true;
      dashboardService.getAllLossGroup().then(function (response) {
        var list = response.data || [];
	      $scope.aMatrixDataList = list;
        var groups = list.map(function(obj) { return obj.group; });
        /*groups = groups.filter(function(v,i) { return groups.indexOf(v) == i; });
        $scope.mainLossesList = list;
        $scope.allLossGroups = groups;
        $scope.allLosses = list;*/  
        $rootScope.loaderAction = false;
      }, function (error) {
        $rootScope.loaderAction = false;
        console.error(error);
      });
    }
    $scope.getTypeCount = function (lossType) {
    	var groupType = $scope.aMatrixDataList.filter(function(v,i) { return v.lossType == lossType; });
      return ( groupType || [] ) ? groupType.length : 1  ;
    };

    $scope.getGroupCount = function (group, lossType) {
    	var groupType = $scope.aMatrixDataList.filter(function(v,i) { return v.group == group && v.lossType == lossType; });
      return ( groupType || [] ) ? groupType.length : 1  ;
    };
    
    getAllLossGroup();

    $scope.$on('loadAMatrixGrid', function(e) {  
      getAllLossGroup();        
    });

    $scope.changeSeverity = function(data){
      if(data.severity)
      data.priority = data.severity == "STRONG IMPACT" ? "HIGH PRIORITY" : data.severity == "MEDIUM IMPACT" ? "MEDIUM PRIORITY" : data.severity == "LOW IMPACT" ? "LOW PRIORITY" : "NO PRIORITY";
      dashboardService.updateLoss(data).then(function (response) {
        var list = response.data || [];
        $scope.aMatrixDataList = list;
      }, function (error) {
        console.error(error);
      });
    }
	}
]);