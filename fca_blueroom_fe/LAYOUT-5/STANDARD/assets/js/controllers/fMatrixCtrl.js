app.controller('FMatrixCtrl', ['$rootScope', '$scope', 'matricesService','userService', 'moment',
function ($rootScope, $scope, matricesService,userService, moment) {
  var columnDefs = [];
  var userRole;
  $scope.yearList = [];
  var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
  for (var i = 2017;i <= 2050 ;i++)
    $scope.yearList.push(i);
  
  $scope.monthList = [
    { value : 01 , label :  'January' },
    { value : 02 , label :  'February' }, 
    { value : 03 , label :  'March' },
    { value : 04 , label :  'April' }, 
    { value : 05 , label :  'May' }, 
    { value : 06 , label :  'June' },
    { value : 07 , label :  'July' },
    { value : 08 , label :  'August' },
    { value : 09 , label :  'September' },
    { value : 10, label :  'October' },
    { value : 11, label :  'November' },
    { value : 12, label :  'December' }];
  
  $scope.filterFMatrix = {
    startOfYear : moment().format("YYYY"),
    startOfMonth : "1",
    endOfYear : moment().format("YYYY"),
    endOfMonth : parseInt(moment().format("MM"))+"",
    isEmployee: ''
  };

  var rowData=[];
  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    defaultColDef : {
      width: 150,
    },
    headerHeight:90,
    rowHeight:50,
    onGridReady: function(params) {
      params.columnApi.autoSizeColumns();
    }
  };

  $scope.showFFilterPanel = true;

  /*$scope.$on('loadFMatrixGrid', function(e) {  
    $scope.loadFMatrixGrid();        
  });*/
  $scope.$on('filterFMatrixGrid', function(e) {  
    $scope.showFFilterPanel = true;         
    $scope.$parent.showFilterPanel =  true;
  });
  
  userApproval = userDetails.approval;
  userRole = userDetails.roles[0].authority;

  

  $scope.loadFMatrixGrid = function() {
    $rootScope.loaderAction = true;
    $scope.filterFMatrix.isEmployee = userRole;
    matricesService.getFMatricesData($scope.filterFMatrix).then(function (response) {
      $scope.showFFilterPanel = false;
      $scope.$parent.showFilterPanel =  false;
      clearGrid();
      columnDefs =getDefaultColumns();
      gridOptions.rowData = response.data.projectList;
      
      var month = moment(),
      satrtFromDate = moment();
      if ($scope.filterFMatrix && $scope.filterFMatrix.startOfMonth ) 
        satrtFromDate.month($scope.filterFMatrix.startOfMonth-1);
      if ($scope.filterFMatrix && $scope.filterFMatrix.startOfYear ) 
        satrtFromDate.year($scope.filterFMatrix.startOfYear);
      
      if ($scope.filterFMatrix && $scope.filterFMatrix.endOfMonth ) 
        month.month($scope.filterFMatrix.endOfMonth);
      if ($scope.filterFMatrix && $scope.filterFMatrix.endOfYear ) 
        month.year($scope.filterFMatrix.endOfYear);

      var headerPlan = getPlanningHeader(satrtFromDate);
      for (var i in headerPlan  ) {
        columnDefs.push({headerName: headerPlan[i].label ,field: headerPlan[i].date ,width:90, cellRenderer: customCellRendererSavingModel });
      }

      gridOptions.columnDefs = columnDefs;
      var fMatrixDiv = document.querySelector('#f_matrix');
      new agGrid.Grid(fMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, function (error) {
      $rootScope.loaderAction = false;
      console.error(error);
    });
  };

  function getDefaultColumns() {
      return [
      {headerName: "UIN",field: "projectUIN",width:74,cellClass:["grid-cell-text-center"]},
      {headerName: "LOSS TYPE",field: "loss",width:200,cellClass:["grid-cell-text-center"],
        cellRenderer: function(params){
         if (params.value == "MACHINE SCRAP" || params.value == "MATERIAL SCRAP"){
            return "<span style='color:red;font-weight: bold;'>"+ params.value +"  </span>";
          }
          return params.value;
        }
      },// cellClass: 'cell-wrap-text', autoHeight: true},
      {headerName: "ETU",field: "etu",width:70,cellClass:["grid-cell-text-center"]},
      {headerName: "Process",field: "process",width:170,cellClass:["grid-cell-text-center"]},
      {headerName: "OP NO.", field: "operationNo", cellClass : [ "grid-cell-text-center","wrap-grid-text"],width:60},
     /* {headerName: "Base Pillar",field: "basePillar", width:100,cellClass:["grid-cell-text-center"] },*/
      {headerName: "PROJECT DESCRIPTION",field: "projectName",width:225,cellClass:["grid-cell-text-center","wrap-grid-text"]},// cellClass: 'cell-wrap-text', autoHeight: true},
      {headerName: "POTENTIAL SAVING",field: "yearlyPotential",width:109,cellClass:["grid-cell-text-end"],
        cellRenderer: function (params) {//Only needed if you need to do some custom parsing before show value in the UI, i.e: '$' + params.value
          return params.value ? "<span> &#x20b9;" + parseInt(params.value||0).toLocaleString('en-US', { maximumFractionDigits : 0 , minimumFractionDigits: 0 }) + '</span>' : '';
        }
      },
      {headerName: "SAV ATT \n ACT",field: "__empty",width:109, cellRenderer: customCellRendererModel },
      {headerName: "TOTAL",field: "__empty",width:109, cellRenderer: customCellRendererTotalModel},
     /* {headerName: " ",field: "targetDate",width:109 ,cellClass:["grid-cell-text-center"]}*/
    ];
  }

  function customCellRendererModel(params) {}
  
  customCellRendererModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: center;border-bottom: 1px solid #bdc3c7;"> SAV ATT </div>'+
                                '<div class="col-md-12" style="text-align: center;"> ACT </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };

  customCellRendererModel.prototype.getGui = function getGui() {
    return this.eGui;
  };

 function customCellRendererTotalModel(params) {}
  
  customCellRendererTotalModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: right;border-bottom: 1px solid #bdc3c7;">'+ getValidData(parseInt(params.data.totalATTSaving))+'</div>'+
                                '<div class="col-md-12" style="text-align: right;"> '+ getValidData(parseInt(params.data.totalACTSaving))+' </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };

  customCellRendererTotalModel.prototype.getGui = function getGui() {
    return this.eGui;
  };

 

 function customCellRendererSavingModel(params) {}
  
  customCellRendererSavingModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    var projectedSaving = moment(params.colDef.field).isSame(moment()) || moment(params.colDef.field).isBefore(moment()) ? getValidData(parseInt(params.data["SAVING_ATT_"+ ( moment(params.colDef.field).format('MMM_YY')).toUpperCase() ])) : "&nbsp;" ;
    var actualSaving =  moment(params.colDef.field).isBefore(moment()) ? getValidData(parseInt(params.data["SAVING_ACT_"+ ( moment(params.colDef.field ).format('MMM_YY')).toUpperCase()] )) : "&nbsp;" ;
    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: right;border-bottom: 1px solid #bdc3c7;background: #50eef6;">'+ projectedSaving +'</div>'+
                                '<div class="col-md-12" style="text-align: right;background: #7de07dfc;">  '+ actualSaving+' </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };
  customCellRendererSavingModel.prototype.getGui = function getGui() {
    return this.eGui;
  };

  function getValidData (value) {
    return value ? "&#8377;" + value.toLocaleString('en-US', {minimumFractionDigits: 0}) : '&nbsp;';
  }
  function getPlanningHeader(startDate) {
      var plans = []; 
      var month = moment(startDate);
      var endDate = moment(month);
      endDate.add(1, 'year');
      while( month < endDate ) {
          plans.push({
            label : month.format('MMM-YY'),
            date : month.format('MM-DD-YYYY')
          });
          month.add(1, "month");
      }
      return plans;
  };

  function clearGrid () {
    var myNode = document.getElementById("f_matrix");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
  }
  $scope.filterData = function () {
    $scope.loadFMatrixGrid();
  }; 
  $scope.openFilterPanel = function () {
    $scope.showFFilterPanel = !$scope.showFFilterPanel;
  }; 

  /*if ( $scope.currentStep == 4 ) {
    $scope.loadFMatrixGrid();
  }*/
  
  
}]);