
app.controller('ReportCtrl', ['$rootScope', '$scope', 'moment', 'factorManagementService','userService',
function ($rootScope, $scope,moment, factorManagementService, userService) {

var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
$scope.panelTitle = "Reports";
$scope.isHideDashboardPanel = false;
$scope.showReportPanel = true;

$scope.monthlySavingData = [];
$scope.monthlySavingLabels = [];
$scope.monthlySavingSeries = [];
$scope.monthlySavingOptions = {
    maintainAspectRatio: false,
    showScale: true,
    barDatasetSpacing: 0,
    tooltipFontSize: 11,
    tooltipFontFamily: "'Helvetica', 'Arial', sans-serif",
    responsive: true,
    scaleBeginAtZero: true,
    scaleShowGridLines: true,
    scaleLineColor: 'transparent',
    barShowStroke: false,
    barValueSpacing: 5
  };
$scope.calendarYearList = [];
$scope.calendarSelectedYear = 2018;
var startYear = 2018;
while( startYear < 2030) {
  $scope.calendarYearList.push(startYear);
  startYear++;
}

var userApproval; 
var userRole;


userApproval = userDetails.approval;
userRole = userDetails.roles[0].authority;
if(userRole =="SUPER_ADMIN" || userRole =="SUPER_EMPLOYEE"){
  $scope.showEtuPillar = true;
}


$scope.showEtuPillar = false;


 $scope.changeReport = function(value){
 	
  	$scope.isHideDashboardPanel = true;

  	if ( value === 'report1' ) {
      $scope.panelTitle = "On going Project Status";
      $scope.isShowReportFirst = true;
    }    
    if ( value === 'report2' ) {
      $scope.panelTitle = "User Savings";
      $scope.isShowReportSecond = true;
    }    
    if ( value === 'report3' ) {
      $scope.panelTitle = "Project Savings";
      $scope.isShowReportThird = true;
    }
    if (value === 'report4' ) {
      $scope.panelTitle = "People Performance";
      $scope.isShowReportForth = true;
    }
    if (value ==='report5') {
      $scope.panelTitle = "Skill Inventory";
      $scope.isShowReportFifth = true;
    }
    if(value ==='report6') {
      $scope.panelTitle = "Plant Pillar";
      $scope.isShowReportSixth = true;
    }
    if(value ==='report7') {
      $scope.panelTitle = "Plant ETU";
      $scope.isShowReportSeventh = true;
    }
  };

  $scope.backToReportPanel = function () {
    if ( !$scope.showReportPanel ) {
      $scope.panelTitle = "On going Project Status";
      $scope.$broadcast('filterFMatrixGridReport'); 
      $scope.showReportPanelChnage(true);
    } else {
      $scope.panelTitle = "Reports";
      $scope.isHideDashboardPanel = false;
      $scope.isShowReportFirst = false;
      $scope.isShowReportSecond = false;
      $scope.isShowReportThird = false;
      $scope.isShowReportForth = false;
      $scope.isShowReportFifth = false;
      $scope.isShowReportSixth = false;
      $scope.isShowReportSeventh = false;
    }
  };


  $scope.showReportPanelChnage = function(value) {
    $scope.showReportPanel = value;
  };

  $scope.projectMonthlySavingDataLoad = function (year) {
    $scope.monthlySavingData = [];
    $scope.monthlySavingLabels = [];
    $scope.monthlySavingSeries = [];

    factorManagementService.getProjectMonthlySavingFactor(year).then(function (response) {
      $scope.projectPlanYearList = getProjectSavingFactorHeader(year);
      if(response && response.data.length > 0 ) {
        var list = response.data;
        list.forEach(function(factor1){
          $scope.projectPlanYearList.some(function(factor2,index){
            if(factor1.month == factor2.month) {
              $scope.projectPlanYearList[index].monthlyProjectSaving = factor1.monthlyProjectSaving;
              $scope.projectPlanYearList[index].monthFactor = factor1.monthFactor <= 0 ? 1 : factor1.monthFactor ;     
              return true;
            }
            return false;
          });
        });
        $scope.monthlySavingData = [$scope.projectPlanYearList.map(function(d){ return d.monthlyProjectSaving  ? d.monthlyProjectSaving * d.monthFactor: 0; })];
        $scope.monthlySavingLabels = $scope.projectPlanYearList.map(function(d){ return d.label ;});
        $scope.monthlySavingSeries = [$scope.calendarSelectedYear];
      } else {
        $scope.monthlySavingData = [[]];
        $scope.monthlySavingLabels = $scope.projectPlanYearList.map(function(d){ return d.label ;});
      }
    }, function (error) {
        console.error(error);
    });

  };

  function getProjectSavingFactorHeader(year) {
    var plans = []; 
    var month = moment();
    month.month(0);
    month.year(year);
    var endDate = moment(month);
    endDate.add(1,'year');
    while( month < endDate ) {
        plans.push({
          cssClass : month.format('MMM'),
          label : month.format('MMMM'),
          month : parseInt(month.format('MM')),

          monthFactor : 0,
          monthlyProjectSaving : 0,
          totalMonthlyProjectSaving : 0,
          etuId : $scope.etuId
        });
        month.add(1, "month");
    }
    return plans;
  }; 

  

}]);