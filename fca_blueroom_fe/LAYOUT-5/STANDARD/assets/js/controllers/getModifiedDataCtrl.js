app.controller('lossTypeCtrl', ["$rootScope","$scope","$uibModal","$$uibModalInstance",'dashboardService',"projectService", "data","userService", '$timeout','projectService', 'moment',
function($rootScope,$scope, $uibModal,$$uibModalInstance,dashboardService,projectService, data,userService, $timeout, moment) {

  $scope.lossData = data;

  $scope.update= function(){
    
    dashboardService.updateLoss($scope.lossData).then(function (response) {
      var list = response.data || [];
    
    }, function (error) {
      console.error(error);
    });
  }

  $scope.cancel = function() {
    $uibModal.dismiss('cancel');
}
}]);

app.controller('getModifiedDataCtrl', ["$rootScope","$scope","$uibModal",'dashboardService','commonService',"$timeout",
function($rootScope,$scope,$uibModal,dashboardService,commonService,$timeout) {
  $scope.showProject=true;
  $rootScope.showTechTools=true;
  $scope.showTechnicalToolsAddButton=false;
  $rootScope.showLossTypes=true;
  $scope.showLossTypesAddButton=false;
  $rootScope.showEmployee=true;
  $scope.showEmployeeAddButton=false;
  $rootScope.showWCMMethods=true;
  $scope.showWCMMethodsAddButton=false;
  $rootScope.showWCMTools=true;
  $scope.showWCMToolsAddButton=false;
  $rootScope.showOperation=true;
  $scope.showOperationAddButton=false;
  $rootScope.showAllETUs=true;

  $scope.headerNameForTable = "Add Modify Fields";

  $scope.lossData={
    id:'',
    loss:'',
    group:'',
    lossType:'',
    priority:'',
    severity:''
  };

  $scope.wcmMethodData={
    id:'',
    levelFirstTool:'',
    levelSecondTool:'',
    reprortPriority:'',
    type:''
    };
    $rootScope.WCMMethodUpdateForm = {};
  function openWCMMethod(gridData){
    $rootScope.gridItems=gridData;
        
    $scope.WCMMethodUpdateForm.Id = gridData.data.id;
    $scope.WCMMethodUpdateForm.levelFirstTool = gridData.data.levelFirstTool;
    $scope.WCMMethodUpdateForm.levelSecondTool = gridData.data.levelSecondTool;
    $scope.WCMMethodUpdateForm.reprortPriority = gridData.data.reprortPriority;
    $scope.WCMMethodUpdateForm.type = gridData.data.type;
    $scope.WCMMethodUpdateForm.pillar_approach = gridData.data.pillar_approach;      
    
    
    $rootScope.loaderAction = true;
  var tools = {};
  $rootScope.mod = $uibModal.open({
    templateUrl : './assets/views/get-Projectuibs/update-wcmMethods.html',
    controller : 'wcmMethodUpdateCtrl',
    size : 'lg',
    windowClass : 'projectOperationsWindow',
    resolve : {
      data : function (){
        return $scope.WCMMethodUpdateForm;
      }
    }
  });
     
  $rootScope.loaderAction = false;

  /*
    $scope.wcmMethodData.levelFirstTool=gridData.data.levelFirstTool;
    $scope.wcmMethodData.levelSecondTool=gridData.data.levelSecondTool;
    $scope.wcmMethodData.reprortPriority=gridData.data.reprortPriority;
    $scope.wcmMethodData.type=gridData.data.type;

    commonService.updateWCMMethod(gridData.data.id,$scope.wcmMethodData).then(function(response){
      console.log(response);
    },
    function(error){
      console.log(error);
    });*/
  }
  $scope.wcmMethodData={
    id:'',
    levelFirstTool:'',
    levelSecondTool:'',
    reprortPriority:'',
    type:''
    };
    

    $scope.wcmToolData={
      levelFirstTool:'',
      levelSecondTool:'',
      reprortPriority:'',
      type:''
      };


      function updateETU(params){
        if(!params.data){
          return;
        }
        $rootScope.etuUpdateData = params.data;
        $rootScope.loaderAction = true;
        var newEmployee = {};
        $rootScope.mod = $uibModal.open({
           templateUrl : './assets/views/get-Projectuibs/updateETU.html',
           controller : 'updateETUCtrl',
           size : 'lg',
           windowClass : 'projectWCMWindow'
          //  resolve : {
          //   data : function (){
          //     return newEmployee;
          //   }
          // }
         });
         $rootScope.loaderAction = false;


        
      }


  $scope.backToAddModifyFields= function(){
    $scope.showProject=true;
  $rootScope.showTechTools=false;
  $scope.showTechnicalToolsAddButton=false;
  $rootScope.showLossTypes=false;
  $scope.showLossTypesAddButton=false;
  $rootScope.showEmployee=false;
  $scope.showEmployeeAddButton=false;
  $rootScope.showWCMMethods=false;
  $scope.showWCMMethodsAddButton=false;
  $rootScope.showWCMTools=false;
  $scope.showWCMToolsAddButton=false;
  $rootScope.showOperation=false;
  $scope.showOperationAddButton=false;
  $rootScope.showAllETUs=false;
  $scope.headerNameForTable = "Add Modify Fields";
  
  }
  $scope.methodDropdown = ["COMMON METHODS", "SPECIFIC METHODS"];
 
  // $scope.$on('filterEMatrixGrid', function(e) { 
  //   $scope.showFilterPanel = true;  
  //   $scope.$parent.showFilterPanel =  true;
  // });
  $scope.addNewWCMTool= function(){
    
    $rootScope.loaderAction = true;
        var newEmployee = {};
        $rootScope.mod = $uibModal.open({
           templateUrl : './assets/views/get-Projectuibs/get-wcmTools.html',
           controller : 'technicalToolDataCtrl',
           size : 'lg',
           windowClass : 'projectWCMWindow'
          //  resolve : {
          //   data : function (){
          //     return newEmployee;
          //   }
          // }
         });
         $rootScope.loaderAction = false;
  }
  $scope.addNewWCMMethod= function(){
    
    $rootScope.loaderAction = true;
        var newEmployee = {};
        $rootScope.mod = $uibModal.open({
           templateUrl : './assets/views/get-Projectuibs/get-wcmMethods.html',
           controller : 'wcmMethodDataCtrl',
           size : 'lg',
           windowClass : 'projectWCMWindow',
           resolve : {
            data : function (){
              return newEmployee;
            }
          }
         });
         $rootScope.loaderAction = false;
  }
  $scope.addNewLossType= function(){
    
    $rootScope.loaderAction = true;
        var newEmployee = {};
        $rootScope.mod= $uibModal.open({
           templateUrl : './assets/views/get-Projectuibs/get-lossType.html',
           controller : 'lossTypeDataCtrl',
           size : 'lg',
           windowClass : 'projectLossTypeWindow',
           resolve : {
            data : function (){
              return newEmployee;
            }
          }
         });
         $rootScope.loaderAction = false;

  }
  $scope.addNewTechnicalTool = function(){
    
    $rootScope.loaderAction = true;
    var tools = {};
    $rootScope.mod = $uibModal.open({
       templateUrl : './assets/views/get-Projectuibs/get-technicalTool.html',
       controller : 'technicalToolDataCtrl',
       size : 'lg',
       windowClass : 'projectTechnicalToolWindow',
       resolve : {
        data : function (){
          return tools;
        }
      }
     });
     $rootScope.loaderAction = false;


  }
  $scope.addNewOperations = function(){
    
    $rootScope.loaderAction = true;
    var tools = {};
    $rootScope.mod = $uibModal.open({
       templateUrl : './assets/views/get-Projectuibs/get-operations.html',
       controller : 'technicalToolDataCtrl',
       size : 'lg',
       windowClass : 'projectOperationsWindow',
       resolve : {
        data : function (){
          return tools;
        }
      }
     });
     $rootScope.loaderAction = false;
  }
   

  function editLossTypes(gridData){
   
    if (!gridData.data) {
      return;
    }
    $rootScope.loaderAction = true;
    var newEmployee = {};
    $scope.lossData.id=gridData.data.id;
    $scope.lossData.group=gridData.data.group;
    $scope.lossData.loss=gridData.data.loss;
    $scope.lossData.lossType=gridData.data.lossType;
    $scope.lossData.priority=gridData.data.priority;
    $scope.lossData.severity=gridData.data.severity;
        var projectCostingCtrlInstance = $uibModal.open({
           templateUrl : './assets/views/get-lossTypes/singlelossType.html',
           controller : 'lossTypeCtrl',
           size : 'lg',
           windowClass : 'projectDetailsWindow',
           resolve : {
             data : function (){
               return $scope.lossData;
             }
           }
         });
   
         $rootScope.loaderAction = false;
  }
  
 
  $rootScope.getEmployees= function(){

    $scope.headerNameForTable = "All Employees"
    $rootScope.showEmployee=true;
    $scope.showEmployeeAddButton=true;

    $rootScope.loaduserInventoryGrid();
    $scope.showProject=false;
    $rootScope.showLossTypes=false;
    $rootScope.showWCMTools=false;
    $rootScope.showTechTools=false;
    $rootScope.showWCMMethods=false;
    $rootScope.showAllETUs=false;
  }



  /***************************************************************************** */

  $rootScope.getWCMtools= function(){

    $rootScope.showWCMTools=true;
    $scope.showWCMToolsAddButton=true;
    $scope.headerNameForTable = "All WCM Tools";
    function clearGrid() {
      var myNode = document.getElementById("WCM_Tools");
    }


    var columnDefs = [];
    var columnDefsextra = [];
    var rowData=[];
    var gridOptions = {
      columnDefs: columnDefs,
      
      rowData: rowData,
      animateRows: true,
      enableFilter: true,
      enableSorting: true, 
      enableColResize: true, 
      defaultColDef : {
        width: 282,
      },
      groupHeaderHeight:40,
      headerHeight:90,
      rowHeight:40,
      onGridReady: function(params) {
     //   params.api.sizeColumnsToFit();
      //  params.columnApi.autoSizeColumns();
      },
      autoGroupColumnDef: {
      }
    };
    
    commonService.getWCMTools().then(function(response){
          clearGrid();
      columnDefsextra=[
        {headerName: 'Group B',cellRenderer: deleteProject},
      ];
      columnDefs = [
        
        {headerName: '',
        children:[
          
        
        { headerName: "Sr. No.", field:  "id"  , width : 80,cellClass:["grid-cell-text-center text-hover-uid"],pinned: 'left',onCellClicked: openWCMTool},
        { headerName: "levelFirst", field:  "levelFirstTool" , width : 300 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        { headerName: "levelSecond", field:  "levelSecondTool" , width : 300 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        
        { headerName: "type", field:  "type" , width : 120 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        
        ]
        }
    ];
    gridOptions.rowData = response || [];

    var colMethod =    { headerName: "Sr. No." , children : []}  ;
    var columns = response.id || {};
    for (var key in columns) {
        colMethod.children.push({ headerName: columns[key] , field:  id  ,width:120,cellClass:["grid-cell-text-center"]});
      }
      columnDefs.push(colMethod);
    /***** */ 
    
    var levelFirstTool =    { headerName: "levelFirstTool " , children : []};  
    columns = response.levelFirstTool || {};
    for (var key in columns) {
      levelFirstTool.children.push({ headerName: columns[key] , field:  levelFirstTool  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(levelFirstTool);
    /***** */
    /***** */ 
    
    var levelSecondTool =    { headerName: "levelSecondTool " , children : []};  
    columns = response.levelSecondTool || {};
    for (var key in columns) {
      levelSecondTool.children.push({ headerName: columns[key] , field:  levelSecondTool  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(levelSecondTool);
    /***** */
    /***** */ 
    /*
    var reprortPriority =    { headerName: "reprortPriority " , children : []};  
    columns = response.reprortPriority || {};
    for (var key in columns) {
      reprortPriority.children.push({ headerName: columns[key] , field:  reprortPriority  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(reprortPriority);
    **** */
    /***** */ 
    
    var type =    { headerName: "type " , children : []};  
    columns = response.type || {};
    for (var key in columns) {
      type.children.push({ headerName: columns[key] , field:  type  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(type);
    /***** */
    gridOptions.columnDefs = columnDefsextra;
    gridOptions.columnDefs = columnDefs;

    $timeout(function() {
      var cMatrixDiv = document.querySelector('#WCM_Tools');
      new agGrid.Grid(cMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, 100);
    },
    function(error){
      console.log(error);
    });
    $scope.showProject=false;
    $rootScope.showLossTypes=false;
    $rootScope.showEmployee=false;

    function openWCMTool(gridData){
      
    $rootScope.gridItems=gridData;
        
    $scope.WCMMethodUpdateForm.Id = gridData.data.id;
    $scope.WCMMethodUpdateForm.levelFirstTool = gridData.data.levelFirstTool;
    $scope.WCMMethodUpdateForm.levelSecondTool = gridData.data.levelSecondTool;
    $scope.WCMMethodUpdateForm.reprortPriority = gridData.data.reprortPriority;
    $scope.WCMMethodUpdateForm.type = gridData.data.type; 
    $scope.WCMMethodUpdateForm.pillar_approach = gridData.data.pillar_approach;      
    
    $rootScope.loaderAction = true;
  var tools = {};
  $rootScope.mod = $uibModal.open({
    templateUrl : './assets/views/get-Projectuibs/update-wcmTools.html',
    controller : 'wcmMethodUpdateCtrl',
    size : 'lg',
    windowClass : 'projectOperationsWindow',
    resolve : {
      data : function (){
        return $scope.WCMMethodUpdateForm;
      }
    }
  });
     
  $rootScope.loaderAction = false;


      
    }
    function deleteProject() {
     
      //return '<button ng-click="ageClicked(data.age)" ng-bind="data.age"></button>';
      return '<button >Delete Project</button>';
  }
  }
 
  /***************************************************************************** */
  /***************************************************************************** */
  $rootScope.getWCMMethods= function(){

    $rootScope.showWCMMethods=true;
    $scope.showWCMMethodsAddButton=true;
    $scope.headerNameForTable = "All WCM Methods";
    function clearGrid() {
      var myNode = document.getElementById("WCM_Methods");
   /*   while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
      }*/
    }


    var columnDefs = [];
    var rowData=[];
    var gridOptions = {
      columnDefs: columnDefs,
      rowData: rowData,
      animateRows: true,
      enableFilter: true,
      enableSorting: true, 
      enableColResize: true, 
      defaultColDef : {
        width: 100,
      },
      groupHeaderHeight:40,
      headerHeight:90,
      rowHeight:40,
      onGridReady: function(params) {
       // params.api.sizeColumnsToFit();
      //  params.columnApi.autoSizeColumns();
      },
      autoGroupColumnDef: {
        /*<!-- headerName: "Organisation Hierarchy", -->
        <!-- cellRendererParams: { -->
            <!-- suppressCount: true -->
        <!-- } -->*/
      }
    };
  
    
    commonService.getWCMMethods().then(function(response){

     
      clearGrid();
      columnDefs = [ {headerName: '',
      children:[
        { headerName: "Sr. No.", field:  "id"  , width : 80 ,cellClass:["grid-cell-text-center text-hover-uid"],pinned: 'left',onCellClicked: openWCMMethod},
        { headerName: "levelFirst", field:  "levelFirstTool" , width : 300  ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        { headerName: "levelSecond", field:  "levelSecondTool"  , width : 300 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        
        { headerName: "type", field:  "type" ,width : 120 ,cellClass:["grid-cell-text-center"],pinned: 'left'},

      ]
    }
    ];
    gridOptions.rowData = response || [];

    var colMethod =    { headerName: "Sr. No." , children : []}  ;
    var columns = response.id || {};
    for (var key in columns) {
        colMethod.children.push({ headerName: columns[key] , field:  id  ,width:80,cellClass:["grid-cell-text-center"]});
      }
      columnDefs.push(colMethod);
    /***** */ 
    
    var levelFirstTool =    { headerName: "levelFirstTool " , children : []};  
    columns = response.levelFirstTool || {};
    for (var key in columns) {
      levelFirstTool.children.push({ headerName: columns[key] , field:  levelFirstTool  ,width:100,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(levelFirstTool);
    /***** */
    /***** */ 
    
    var levelSecondTool =    { headerName: "levelSecondTool " , children : []};  
    columns = response.levelSecondTool || {};
    for (var key in columns) {
      levelSecondTool.children.push({ headerName: columns[key] , field:  levelSecondTool  , width : 100,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(levelSecondTool);
    /***** */
    /***** */ 
    /*
    var reprortPriority =    { headerName: "reprortPriority " , children : []};  
    columns = response.reprortPriority || {};
    for (var key in columns) {
      reprortPriority.children.push({ headerName: columns[key] , field:  reprortPriority  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(reprortPriority);
    **** */
    /***** */ 
    
    var type =    { headerName: "type " , children : []};  
    columns = response.type || {};
    for (var key in columns) {
      type.children.push({ headerName: columns[key] , field:  type  ,width : 100,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(type);
    /***** */
    gridOptions.columnDefs = columnDefs;

    $timeout(function() {
      var cMatrixDiv = document.querySelector('#WCM_Methods');
      new agGrid.Grid(cMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, 100);
    },
    function(error){
      console.log(error);
    });
    $scope.showProject=false;
    $rootScope.showLossTypes=false;
    $rootScope.showEmployee=false;
    $rootScope.showWCMTools=false;
    $rootScope.showAllETUs=false;
    
  }
  /***************************************************************************** */



  /***************************************************************************** */
  $rootScope.getTechnicalTools= function(){
    $rootScope.showTechTools=true;
    $scope.showTechnicalToolsAddButton=true;
    $scope.headerNameForTable = "All Technical Tools";

    function clearGrid() {
      var myNode = document.getElementById("Technical_Tools");
     /* while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
      }*/
    }


    var columnDefs = [];
    var rowData=[];
    var gridOptions = {
      columnDefs: columnDefs,
      rowData: rowData,
      animateRows: true,
      enableFilter: true,
      enableSorting: true, 
      enableColResize: true, 
      defaultColDef : {
        width: 450,
      },
      groupHeaderHeight:40,
      headerHeight:90,
      rowHeight:40,
      onGridReady: function(params) {
      //  params.api.sizeColumnsToFit();
      //  params.columnApi.autoSizeColumns();
      },
      autoGroupColumnDef: {
        /*<!-- headerName: "Organisation Hierarchy", -->
        <!-- cellRendererParams: { -->
            <!-- suppressCount: true -->
        <!-- } -->*/
      }
    };
  
 
 
 
 
    
    commonService.getTechnicalTools().then(function(response){
     
      clearGrid();
      columnDefs = [
        {headerName: '',
        children:[
        { headerName: "Sr. No.", field:  "id" , width : 200 ,cellClass:["grid-cell-text-center text-hover-uid"],pinned: 'left'},
        { headerName: "ProfileName", field:  "profileName" , width : 200 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        { headerName: "TechnicalToolDescription", field:  "technicalToolDescription" , width : 200 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
      /*  { headerName: "user", field:  "name"  ,width:170,cellClass:["grid-cell-text-center"],pinned: 'left'},*/
        ]
      }
    ];
    gridOptions.rowData = response || [];

    var colMethod =    { headerName: "Sr. No." , children : []}  ;
    var columns = response.id || {};
    for (var key in columns) {
        colMethod.children.push({ headerName: columns[key] , field:  id  ,width:120,cellClass:["grid-cell-text-center"]});
      }
      columnDefs.push(colMethod);
    /***** */ 
    
    var profileName =    { headerName: "ProfileName " , children : []};  
    columns = response.profileName || {};
    for (var key in columns) {
      profileName.children.push({ headerName: columns[key] , field:  profileName  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(profileName);
    /***** */

    var technicalToolDescription =    { headerName: "TechnicalToolDescription " , children : []};  
    columns = response.technicalToolDescription || {};
    for (var key in columns) {
      technicalToolDescription.children.push({ headerName: columns[key] , field:  technicalToolDescription  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(technicalToolDescription);
    /***** */

/*
    var user =    { headerName: "user " , children : []};  
    columns = response.user.name || {};
    for (var key in columns) {
      user.children.push({ headerName: columns[key] , field:  name  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(user);*/
    /***** */ 
    gridOptions.columnDefs = columnDefs;

    $timeout(function() {
      var cMatrixDiv = document.querySelector('#Technical_Tools');
      new agGrid.Grid(cMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, 100);
    },
    function(error){
      console.log(error);
    });
    $scope.showProject=false;
    $rootScope.showLossTypes=false;
    $rootScope.showEmployee=false;
    $rootScope.showWCMTools=false;
    $rootScope.showWCMMethods=false;
    $rootScope.showAllETUs=false;
  }  
  /***************************************************************************** */




  /***************************************************************************** */
  $rootScope.getLossTypes = function(){
    $rootScope.showLossTypes=true;
    $scope.showLossTypesAddButton=true;
    $scope.headerNameForTable = "All Losses";
    function clearGrid () {
      var myNode = document.getElementById("Loss_Types");
   /*   while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
      }*/
    }

    var columnDefs = [];
    var rowData=[];
    var gridOptions = {
      columnDefs: columnDefs,
      rowData: rowData,
      animateRows: true,
      enableFilter: true,
      enableSorting: true, 
      enableColResize: true, 
      defaultColDef : {
        width: 237,
      },
      groupHeaderHeight:40,
      headerHeight:90,
      rowHeight:40,
      onGridReady: function(params) {
        params.api.sizeColumnsToFit();
        params.columnApi.autoSizeColumns();
      },
      autoGroupColumnDef: {
        /*<!-- headerName: "Organisation Hierarchy", -->
        <!-- cellRendererParams: { -->
            <!-- suppressCount: true -->
        <!-- } -->*/
      }
    };
  
   // $rootScope.loaderAction = true;
    commonService.getAllLosses().then(function(response){

    
      clearGrid();
      
      columnDefs = [
        {headerName: '',
        children:[
        { headerName: "Sr. No.", field:  "id"  ,cellClass:["grid-cell-text-center text-hover-uid"],pinned: 'left',onCellClicked : editLossTypes},
        { headerName: "Loss Type", field:  "lossType"  ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        { headerName: "Group", field:  "group"  ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        { headerName: "Loss", field:  "loss"  ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        { headerName: "Source", field:  "source"  ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        
        ]
      }
    ];
    gridOptions.rowData = response || [];

   
    var colMethod =    { headerName: "Sr. No." , children : []}  ;
    var columns = response.id || {};
    for (var key in columns) {
        colMethod.children.push({ headerName: columns[key] , field:  id  ,width:120,cellClass:["grid-cell-text-center"]});
      }
      columnDefs.push(colMethod);
    /***** */ 
    
    var lossType =    { headerName: "Loss Type " , children : []};  
    columns = response.lossType || {};
    for (var key in columns) {
      lossType.children.push({ headerName: columns[key] , field:  lossType  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(lossType);
    /***** */
    
    /***** */ 
    var group =    { headerName: "Group " , children : []};  
    columns = response.group || {};
    for (var key in columns) {
      group.children.push({ headerName: columns[key] , field:  group  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(group);
    
    
    /***** */
    var loss =    { headerName: "Loss " , children : []};  
    columns = response.loss || {};
    for (var key in columns) {
      loss.children.push({ headerName: columns[key] , field:  loss  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(loss);
    /***** */
    
    
    /***** */
    var source =    { headerName: "Source " , children : []};  
    columns = response.source || {};
    for (var key in columns) {
      source.children.push({ headerName: columns[key] , field:  source  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(source);
    /***** */
    
    
    /***** */
   
  
    /***** */
    gridOptions.columnDefs = columnDefs;

    $timeout(function() {
      var cMatrixDiv = document.querySelector('#Loss_Types');
      new agGrid.Grid(cMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, 100);

    },
    function(error){
      console.log(error);
  });


  $scope.showProject=false;
  $rootScope.showTechTools=false;
  $rootScope.showWCMTools=false;
  $rootScope.showWCMMethods=false;
  $rootScope.showAllETUs=false;
  }
  /***************************************************************************** */

$rootScope.getAllETU = function(){
  $rootScope.showAllETUs=true;
  $scope.headerNameForTable = "All ETU's";

  function clearGrid () {
    var myNode = document.getElementById("AllETUs");
 /*   while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }*/
  }

  var columnDefs = [];
  var rowData=[];
  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    animateRows: true,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    defaultColDef : {
      width: 237,
    },
    groupHeaderHeight:40,
    headerHeight:90,
    rowHeight:40,
    onGridReady: function(params) {
    //  params.api.sizeColumnsToFit();
    //  params.columnApi.autoSizeColumns();
    },
    autoGroupColumnDef: {
      /*<!-- headerName: "Organisation Hierarchy", -->
      <!-- cellRendererParams: { -->
          <!-- suppressCount: true -->
      <!-- } -->*/
    }
  };
  commonService.getAllETUType().then(function(response){

    clearGrid();
      
      columnDefs = [
        {headerName: '',
        children:[
        { headerName: "Sr. No.", field:  "id" , width : 200 ,cellClass:["grid-cell-text-center "],pinned: 'left',onCellClicked : editLossTypes},
        { headerName: "ETU No.", field:  "etuNo" , width : 200 ,cellClass:["grid-cell-text-center text-hover-uid"],pinned: 'left',onCellClicked:updateETU},
        { headerName: "ETU Name", field:  "etuName" , width : 200 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        
        ]
      }
    ];
    gridOptions.rowData = response.data || [];

   
    var colMethod =    { headerName: "Sr. No." , children : []}  ;
    var columns = response.data.id || {};
    for (var key in columns) {
        colMethod.children.push({ headerName: columns[key] , field:  id  ,width:120,cellClass:["grid-cell-text-center"]});
      }
      columnDefs.push(colMethod);
    /***** */ 

    var etuNo = { headerName: "ETU No.", children : []};
    var columns = response.data.etuNo || {};
    for(var key in columns){
      etuNo.children.push({headerName: columns[key] , field: etuNo ,width:120,cellClass:["grid-cell-text-center"]});
    }
    columnDefs.push(etuNo);
    /***** */

    var etuName = { headerName: "ETU Name.", children : []};
    var columns = response.data.etuName || {};
    for(var key in columns){
      etuName.children.push({headerName: columns[key] , field: etuName ,width:120,cellClass:["grid-cell-text-center"]});
    }
    columnDefs.push(etuName);



    /******* */
    gridOptions.columnDefs = columnDefs;

    $timeout(function() {
      var cMatrixDiv = document.querySelector('#AllETUs');
      new agGrid.Grid(cMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, 100);
  }, function(error){
    console.log(error);
  });
  $scope.showProject=false;
  $rootScope.showLossTypes=false;
  $rootScope.showEmployee=false;
  $rootScope.showWCMTools=false;
  $rootScope.showWCMMethods=false;
  $rootScope.showTechTools=false;
  
  
}





  /***************************************************************************** */
  $rootScope.getOperations=function(){
    $rootScope.showOperation=true;
    $scope.showOperationAddButton=true;
    $scope.headerNameForTable = "All Operations";
    function clearGrid () {
      var myNode = document.getElementById("Operation");
   /*   while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
      }*/
    }

    var columnDefs = [];
    var rowData=[];
    var gridOptions = {
      columnDefs: columnDefs,
      rowData: rowData,
      animateRows: true,
      enableFilter: true,
      enableSorting: true, 
      enableColResize: true, 
      defaultColDef : {
        width: 460,
      },
      groupHeaderHeight:40,
      headerHeight:90,
      rowHeight:40,
      onGridReady: function(params) {
      //  params.api.sizeColumnsToFit();
      //  params.columnApi.autoSizeColumns();
      },
      autoGroupColumnDef: {
        /*<!-- headerName: "Organisation Hierarchy", -->
        <!-- cellRendererParams: { -->
            <!-- suppressCount: true -->
        <!-- } -->*/
      }
    };
    if($rootScope.selectedETU.id==null){
      $rootScope.selectedETU.id=0
    }
    console.log($rootScope.selectedETU.id);
    commonService.getOperations($rootScope.selectedETU.id).then(function(response){
     
      clearGrid();
      
      columnDefs = [
        {headerName: '',
        children:[
        { headerName: "Sr. No.", field:  "id" , width : 200 ,cellClass:["grid-cell-text-center text-hover-uid"],pinned: 'left',onCellClicked: openOperation},
        { headerName: "Operation Name", field:  "machineName" , width : 200 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        
        { headerName: "ETU", field:  "etu.etuName" , width : 200 ,cellClass:["grid-cell-text-center"],pinned: 'left'},
        ]
      }
    ];
    gridOptions.rowData = response || [];

  
    var colMethod =    { headerName: "Sr. No." , children : []}  ;
    var columns = response.id || {};
    for (var key in columns) {
        colMethod.children.push({ headerName: columns[key] , field:  id  ,width:120,cellClass:["grid-cell-text-center"]});
      }
      columnDefs.push(colMethod);
    /***** */ 
    
    var machineName =    { headerName: "Operation Name " , children : []};  
    columns = response.machineName || {};
    for (var key in columns) {
      machineName.children.push({ headerName: columns[key] , field:  machineName  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(machineName);
    /***** */

    var etu =    { headerName: "ETU" , children : []};  
    columns = response.etu || {};
    for (var key in columns) {
      etu.children.push({ headerName: columns[key] , field:  etu.etuName  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(etu);
    /***** */
    /*
    var machineCode =    { headerName: "Machine Code " , children : []};  
    columns = response.machineCode || {};
    for (var key in columns) {
      machineCode.children.push({ headerName: columns[key] , field:  machineCode  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
    }
    columnDefs.push(machineCode);*/
    
    gridOptions.columnDefs = columnDefs;

    $timeout(function() {
      var cMatrixDiv = document.querySelector('#Operation');
      new agGrid.Grid(cMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, 100);





    },
    function(error){
      console.log(error);
    });


    $scope.showProject=false;
    $rootScope.showLossTypes=false;
    $rootScope.showEmployee=false;
    $rootScope.showWCMTools=false;
    $rootScope.showWCMMethods=false;
    $rootScope.showTechTools=false;
    $rootScope.showAllETUs=false;
    function openOperation(gridData){
      $rootScope.gridItems=gridData;
        
      $scope.WCMMethodUpdateForm.Id = gridData.data.id;
      $scope.WCMMethodUpdateForm.levelFirstTool = gridData.data.levelFirstTool;
      $scope.WCMMethodUpdateForm.levelSecondTool = gridData.data.levelSecondTool;
      $scope.WCMMethodUpdateForm.reprortPriority = gridData.data.reprortPriority;
      $scope.WCMMethodUpdateForm.type = gridData.data.type;      
      
      $rootScope.loaderAction = true;
    var tools = {};
    $rootScope.mod = $uibModal.open({
      templateUrl : './assets/views/dashboard/deleteWCMProject.html',
      controller : 'deleteProjectOfWCMCtrl',
      size : 'lg',
      windowClass : 'projectOperationsWindow',
      resolve : {
        data : function (){
          return $scope.WCMMethodUpdateForm;
        }
      }
    });
       
    $rootScope.loaderAction = false;
  


    }
  }
}]);