app.controller('DashBMatrixCtrl', ['$rootScope', '$scope', 'dashboardService','$uibModal', 'moment','$timeout',
	function ($rootScope, $scope, dashboardService,$uibModal, moment, $timeout) {
    var defaultOperations = [
      { id : 1 , label : "OP-10" , key : "OP 10" },
      { id : 2 , label : "OP-20" , key : "OP 20"  },
      { id : 3 , label : "OP-30" , key : "OP 30"  },
      { id : 4 , label : "OP-40"  , key : "OP 40" },
      { id : 5 , label : "OP-50"  , key : "OP 50" },
      { id : 6 , label : "OP-60"  , key : "OP 60" },
      { id : 7 , label : "OP-70"  , key : "OP 70" },
      { id : 8 , label : "OP-80"  , key : "OP 80"  },
      { id : 9 , label : "OP-90"  , key : "OP 90" },
      { id : 10 , label : "OP-100" , key : "OP 100"  },
      { id : 11 , label : "OP-110" , key : "OP 110"  },
      { id : 12 , label : "OP-120" , key : "OP 120"  },
      { id : 13 , label : "OP-130" , key : "OP 130"  },
      { id : 14 , label : "OP-140" , key : "OP 140"  }
    ]; 

    $scope.bMatrixData = {
      sourceLoss: [],
      impactedLoss:[],
      operationSourceNo : [],
      operationImpactedNo : []
    };

    $scope.activeSourceInpactedLossWithColor = [];
    $scope.activeSourceInpactedLoss = [];
   
    $scope.getSourceLossOptHeader = function (sourceLoss) {
      var headerList = [];
      var count = 0 ; 
      for (var i in  sourceLoss) {
        for (var j in defaultOperations) {
          var data = JSON.parse(JSON.stringify(defaultOperations[j]));
          data.id = count;
          data.title = sourceLoss[i];
          headerList.push(data);
          count++;
        }
      }
      return headerList;
    };

    $scope.getImpactedLossOptHeader = function (impactedLoss) {
      var headerList = [];
      var count = 0 ; 
      for (var i in  impactedLoss) {
        for (var j in defaultOperations) {
          var data = JSON.parse(JSON.stringify(defaultOperations[j]));
          data.id = count;
          data.title = impactedLoss[i];
          headerList.push(data);
          count++;
        }
      }
      return headerList;
    };
    function getAllMachineLossList() {
      $rootScope.loaderAction = true;
      dashboardService.getAllMachineLossList().then(function (response) {
        var data = response.data;
        var bMatrixData = {
          sourceLoss : data.sourceLossList,
          impactedLoss : data.impactedLossList,
          operationSourceNo  : $scope.getSourceLossOptHeader(data.sourceLossList),
          operationImpactedNo  : $scope.getImpactedLossOptHeader(data.impactedLossList)
        };
        $scope.activeSourceInpactedLossWithColor = data.activeSourceInpactedLoss.map(function(v){ return v.replace(/[^\w ]/g, ' ');});
        for (var i =0; i < $scope.activeSourceInpactedLossWithColor.length; i++) {
          var source = $scope.activeSourceInpactedLossWithColor[i].split("color ")[0];
          $scope.activeSourceInpactedLoss.push(source);
        }
        $scope.bMatrixData = bMatrixData;
        
         angular.element(document).ready(function () {
          $timeout(function() {
            $rootScope.loaderAction = false;
          }, 1000);
        });
      }, function (error) {
          console.error(error);
      });
    }

    $scope.openModal=function(row,column){
      var data = null;
      var color = null;
      var index = null;
      if($scope.activeSourceInpactedLoss) {
        var checkFor = (row.title + ' ' + row.key + ' '+ column.title + ' ' + column.key ).replace(/[^\w ]/g, ' ');
        index = $scope.activeSourceInpactedLoss.indexOf(checkFor);
        if(index > -1)  {
            color = $scope.activeSourceInpactedLossWithColor[index].split("color ")[1];
            data = color;
        } else {
          data = "white";
        }
      } else {
        data = "white";
      }
      $rootScope.bMatrixDataColorForChange = {
        color:  data,
        sourceLoss : row.title,
        sourceOp : row.key,
        impactedLoss : column.title,
        impactedOp : column.key,
        valueRed : true,
        valueGreen : true,
        valueYellow : true
      };
      
        $rootScope.loaderAction = true;
        var tools = {};
        $rootScope.mod = $uibModal.open({
           templateUrl : './assets/views/dashboard/bMatrixColorChange.html',
           controller : 'bMatrixColorChangeCtrl',
           size : 'lg',
           windowClass : 'projectTechnicalToolWindow',
           resolve : {
            data : function (){
              return tools;
            }
          }
         });
         if(data != "white") {
            $scope.activeSourceInpactedLossWithColor[index].split("color ")[1] = $rootScope.bMatrixDataColorForChange.color;
         }
         
         $rootScope.loaderAction = false;
        }
        
      // }      

    $scope.getTypeCount = function (lossType) {
    	var groupType = $scope.aMatrixDataList.filter(function(v,i) { return v.lossType == lossType; });
      return ( groupType || [] ) ? groupType.length : 1  ;
    };

    $scope.getGroupCount = function (group, lossType) {
    	var groupType = $scope.aMatrixDataList.filter(function(v,i) { return v.group == group && v.lossType == lossType; });
      return ( groupType || [] ) ? groupType.length : 1  ;
    };


    $scope.isActiveCell = function(row,column) {
      if($scope.activeSourceInpactedLoss) {
        var checkFor = (row.title + ' ' + row.key + ' '+ column.title + ' ' + column.key ).replace(/[^\w ]/g, ' ');
        var color = "white";
        var index = $scope.activeSourceInpactedLoss.indexOf(checkFor);
        if(index != -1)  {
          color = $scope.activeSourceInpactedLossWithColor[index].split("color ")[1];
        } else {
          return 'active-white-cell';
        }
        return color == 'red' ? 'active-red-loss' : color == 'green' ?  'active-green-loss' : 'active-yellow-loss';
      }
      return 'active-white-cell';
    };

    $rootScope.refreshGridAgain = function(data){
      $scope.activeSourceInpactedLoss= [];
      $scope.activeSourceInpactedLossWithColor = data.map(function(v){ return v.replace(/[^\w ]/g, ' ');});
      for (var i =0; i < $scope.activeSourceInpactedLossWithColor.length; i++) {
        var source = $scope.activeSourceInpactedLossWithColor[i].split("color ")[0];
        $scope.activeSourceInpactedLoss.push(source);
      }
    }
    
    getAllMachineLossList();

    function getRandomColor() {
      var letters = '0123456789ABCDEF';
      var color = '';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    }

    $scope.$on('loadBMatrixGrid', function(e) {  
      $scope.activeSourceInpactedLossWithColor = [];
      getAllMachineLossList();        
    });
    
	}

]);