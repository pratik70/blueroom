app.controller('lossTypeDataCtrl', ["$rootScope","$scope","commonService","projectService",
function($rootScope,$scope,commonService,projectService,) {
  
  
  $scope.addLossType = function(){
    $rootScope.showLossTypes=false;

    $scope.lossTypeForm.priority = "asas";
    $scope.lossTypeForm.severity = "asas";
    var losstypes ={
      group : $scope.lossTypeForm.group,
      loss : $scope.lossTypeForm.loss,
      lossType : $scope.lossTypeForm.lossType,
      priority : $scope.lossTypeForm.priority,
      severity : $scope.lossTypeForm.severity,
      source : $scope.lossTypeForm.source
    }
    
    commonService.addLossType(losstypes).then(function(response){

    $rootScope.mod.dismiss('cancel');
    $rootScope.getLossTypes();
    },
    function(error){
      console.log(error);
    });
  }

  $scope.getSources= function(){
    projectService.getAllLossGroup().then(function(response){
            var list = response.data || [];

            $scope.sources = list.map(function(obj){return obj.source; });
            $scope.sources =  $scope.sources.filter(function(v,i){return $scope.sources.indexOf(v)==i});

            $scope.groups = list.map(function(obj){return obj.group; });
            $scope.groups =  $scope.groups.filter(function(v,i){return $scope.groups.indexOf(v)==i});
            
            $scope.lossTypes = list.map(function(obj){return obj.lossType; });
            $scope.lossTypes =  $scope.lossTypes.filter(function(v,i){return $scope.lossTypes.indexOf(v)==i});
            
            
    },function(error){
      console.log(error);
    });
  }

  

  
  $scope.cancel = function() {
    $rootScope.mod.dismiss('cancel');
}
}]);
