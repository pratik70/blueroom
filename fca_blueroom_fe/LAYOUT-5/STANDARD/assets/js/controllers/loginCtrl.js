

app.controller('LoginCtrl', ['$rootScope', '$scope', '$state' , 'commonService', '$http' , '$cookies','$uibModal',"matricesService", 
function ($rootScope, $scope, $state, commonService ,$http, $cookies, $uibModal, matricesService) {
  $rootScope.pageHeaderTitle = 'Login'; 
    $scope.emailSent=false;
  	$scope.message = null;
    $scope.loadingSpin = false;

    localStorage.removeItem("ETU_value");
    localStorage.removeItem("UserDetails");
      
    $scope.backToHome = function(){
        $state.go('signin');        
    }

    $scope.goToForgetPassword = function(){
        $state.go('login.forgot');
    }

    
    $scope.sendPasswordOnMail= function(){
      commonService.getUserDetailsByEmployeeId($scope.searchUser).then(function(response){
        $scope.emailSent=true;
        $scope.getMessage=response.data.data;
      },function(error){
        console.log(error);
      });
    }

    function replaceAll(str, find, replace) { return str.replace(new RegExp(find, 'g'), replace); }

  	$scope.login = function(){
  		var data ={
          "username": $scope.username,
          "password": $scope.password
      };

      $rootScope.isAdmin = false;
      $rootScope.isSuperAdmin = false;
      $rootScope.isEmployee = false;
      $rootScope.isSuperEmployee = false;
      $scope.loadingSpin = true;

  		commonService.userLogin(data).then(function (response) {
        if(response.token){
          $cookies.put('auth_token', response.token);
          var data = parseJwt(response.token);
          $rootScope.userId = data.userId;
        	var result = data.roles.slice(1, -1); 
          var permissionsString = data.permissions.slice(1,-1);
          
          var roles = result.split(',');
          var replacedString = replaceAll(permissionsString , ':0', '');
          var permissions = replacedString.split(',');

          if(permissions.length > 0){
             for(var i = 0 ; i < permissions.length ; i++) {
               permissions[i] = permissions[i].trim();
             }
          }
              
          localStorage.setItem("permissions_blueroom", permissions);
        	if(roles.length > 0){
        		for(var i = 0 ; i < roles.length ; i++) {
        			$rootScope.isSuperAdmin = $rootScope.isSuperAdmin ? true : roles[i].trim() == "SUPER_ADMIN";
              $rootScope.isAdmin = $rootScope.isAdmin ? true : roles[i].trim() == "ADMIN";
              $rootScope.isEmployee = $rootScope.isEmployee ? true : roles[i].trim() == "EMPLOYEE";
              $rootScope.isSuperEmployee = $rootScope.isSuperEmployee ? true : roles[i].trim() =="SUPER_EMPLOYEE"
            }
        	}

          if($rootScope.isAdmin  || $rootScope.isSuperAdmin || $rootScope.isEmployee || $rootScope.isSuperEmployee) {
            $state.go('homepage');
            // $rootScope.loaderAction = true;
            // var tools = {};
            // $rootScope.pdcaStatusModal = $uibModal.open({
            //    templateUrl : './assets/views/dashboard/pdcaProjects.html',
            //    controller : 'LoginCtrl',
            //    size : 'lg',
            //    windowClass : 'projectStatusWindow',
            //    resolve : {
            //     data : function (){
            //       return tools;
            //     }
            //   }
            // });
            // $rootScope.loaderAction = false;
        	} else {
        		$scope.message = "Access Denied";
        	}
        } else {
        	$scope.message = response.data.message;
        }

        $scope.loadingSpin = false;

      }, function (error) {
        $scope.loadingSpin = false;
        if(error.data && error.data.message){
           $scope.message = error.data.message;
        } else {
            window.location.reload();
        }
      });
  	}

    $scope.cancel=function(){
      $rootScope.pdcaStatusModal.dismiss('cancel');
    }

    function parseJwt(token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    };

}]);