app.controller('deleteDMatricesProjectCtrl', ["$rootScope","$scope", 'matricesService',"commonService","userService","$uibModal",
function($rootScope,$scope,matricesService,commonService,userService, $uibModal) {
  
      $scope.deleteProject = function(){
        $rootScope.isShowDMatrixData = false;
        matricesService.deleteProject($rootScope.deleteDMatricesData).then(function(response){

        //  $scope.$emit('loadDMatrixGrid', "some data");
          $rootScope.mod.dismiss('cancel');
          $rootScope.loadDMatrixGridAgain();  
        },
        function(error){
          console.log(error);
        });
      }

  $scope.cancel=function(){
    $rootScope.mod.dismiss('cancel');
  }
  
  
}]);

app.controller('DMatrixCtrl', ['$rootScope','$uibModal', '$scope','userService', 'matricesService',"$state",
function ($rootScope,$uibModal, $scope,userService, matricesService,$state) {
  var userRole;
  var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
  $scope.filterDMatrix = {
    startOfYear : moment().format("YYYY"),
    startOfMonth : "1",
    endOfYear :  moment().format("YYYY"),
    endOfMonth : parseInt(moment().startOf('month').format("MM"))+"",
    isEmployee: false
  };
  
  
  
  function numberParser(params) {
    var newValue = params.newValue;
    var valueAsNumber;
    if (newValue === null || newValue === undefined || newValue === '') {
        valueAsNumber = null;
    } else {
        valueAsNumber = parseFloat(params.newValue);
    }
    return valueAsNumber;
  }


  userApproval=userDetails.approval;
  userRole = userDetails.roles[0].authority;

  
  function deleteProject() {
    return '<button style="border-radius: 10px;">Delete Project</button>';
  }
  function deleteProjectById(gridData) {
    $rootScope.deleteDMatricesData=gridData.data;
    
    $rootScope.loaderAction = true;
  
    $rootScope.mod = $uibModal.open({
      templateUrl : './assets/views/dashboard/dMatricesProjectDeletionDialog.html',
      controller : 'deleteDMatricesProjectCtrl',
      size : 'sm',
      windowClass : 'projectDetailsWindow',
      resolve : {
        
      }
    });

    $rootScope.loaderAction = false;
   
   /* console.log(gridData);
    $scope.deletedata=gridData.data;
    
    matricesService.deleteProject($scope.deletedata).then(function(response){
      console.log(response);
      
      $rootScope.loadDMatrixGridAgain = function(){
        $scope.loadDMatrixGrid();
      };
    },
    function(error){
      console.log(error);
    });*/
  }


    // specify the columns
    
    
  var columnDefs = [ ];

    function customCellRendererFunc(params){
      let cellContent = '';
      try {
          for (let attItr = 0; attItr < params.value.length; attItr++) {
              let fileName = params.value[attItr].fileIcon;

              fileType = fileIcon;
              if (fileType === 'jpg') {
                  imagePath = 'E:\New folder/img1.jpg';
              } else if (fileType === 'doc' || fileType === 'docx') {
                  imagePath = 'assets/doc-icon.png';
              } else if (fileType === 'xls' || fileType === 'xlsx') {
                  imagePath = 'assets/xls-icon.png';
              } else {
                  imagePath = '';
              }


              cellContent += '<image src="' +
                  imagePath + '" title="' + filetype, '"></a> &nbsp;';
          }
      } catch (exception) {

          console.error(exception);
      }

      return cellContent
  }
  var rowData=[];
  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    /*<-- suppressMenuHide: true, -->*/
    defaultColDef : {
        width: 150,
    },
    groupHeaderHeight:40,
    headerHeight:90,
    rowHeight:50,
    onGridReady: function(params) {
      //params.api.sizeColumnsToFit();
      params.columnApi.autoSizeColumns();
    }
  };



  function onFilterTextBoxChanged() {
    gridOptions.api.setQuickFilter(document.getElementById('filter-text-box').value);
  }
    $scope.$on('loadDMatrixGrid', function(e) {  
    $scope.loadDMatrixGrid();        
  });

  $rootScope.loadDMatrixGridAgain = function(){
    $scope.loadDMatrixGrid();
  };

  $scope.loadDMatrixGrid = function() {
    //document.getElementById("d_matrix").remove();
    var myNode = document.getElementById("d_matrix");
      while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
      }

    $rootScope.isShowDMatrixData = false;
    $rootScope.loaderAction = true;
    $scope.filterDMatrix.isEmployee = userRole;
    matricesService.getDMatricesInformation($scope.filterDMatrix).then(function (response) {
      columnDefs = getDefaultColumns();
      gridOptions.rowData = response.data.projectList;

      var teamMembers = { headerName:"Members", marryChildren : true,  headerClass: 'back-color6',children :[
      { headerName: "Team Leader",field: "personResponsible", width:92, headerClass: 'back-color6', cellClass:["grid-cell-text-center","wrap-grid-text"]}, 
      { headerName: "Team Coach",field: "personCoach", width:92, headerClass: 'back-color6', cellClass:["grid-cell-text-center","wrap-grid-text"] }]};
      var columns = response.data.totalTeamMemberCount;
      for (var i = 0; i < columns-1 ; i++) {
        teamMembers.children.push({headerName: "Team Member ("+(i+1) +")" , headerClass: 'back-color6', field:  "team_member_"+(i+1) ,width:120,cellClass:["grid-cell-text-center","wrap-grid-text"]});
      }
      columnDefs.push(teamMembers);
      columnDefs.push({headerName: "Base Pillar", marryChildren: true,headerClass:'back-color3', children: [{headerName: "Base Pillar",headerClass:"back-color3", field: "basePillar", width:100, cellClass:["grid-cell-text-center"]}] });

      columns = response.data.pillarColumns;
      var supportingPillar = {headerName: "Supporting Pillar", headerClass:"back-color3", marryChildren: true, children: [] };
      for (var key in columns) {
        supportingPillar.children.push({headerName: columns[key] , field:  key ,headerClass:' back-color3',width:60,cellClass:["grid-cell-text-center"]});
      }
      columnDefs.push(supportingPillar);
      var methodAndTool = { headerName: "WCM Methods And Tools",headerClass:' back-color5', marryChildren: true, children: [] };
      columns = response.data.methodColumns;
      var count = 0 ;
      var statusCol = 'open';
      for (var key in columns) {
        if (count > 10 )
          statusCol = 'closed';
        methodAndTool.children.push({headerName: columns[key] , field:  key ,headerClass:'my-class back-color5',width:60,cellClass:["grid-cell-text-center"],columnGroupShow : statusCol});
      }
      columns = response.data.toolColumns;
      for (var key in columns) {
        methodAndTool.children.push({headerName: columns[key] , field:  key ,headerClass:'my-class back-color5',width:60,cellClass:["grid-cell-text-center"],columnGroupShow : statusCol});
      }
      columnDefs.push( methodAndTool);
      var projectKpis = { headerName: "KPI", headerClass:"back-color2", marryChildren: true, children: [] };
      columns = response.data.kipColumns;
      for (var key in columns) {
         projectKpis.children.push({headerName: columns[key] , field:  key ,headerClass:'my-class back-color2',width:60,cellClass:["grid-cell-text-center"]});
      }
      columnDefs.push( projectKpis);
      if(userRole =="SUPER_ADMIN" || userRole =="ADMIN" ){
            columnDefs.push({ headerName: "Delete",width:100, cellClass:["grid-cell-text-center","text-hover-uid"], cellRenderer: deleteProject,onCellClicked:deleteProjectById });
          }

      gridOptions.columnDefs = columnDefs;
      var cMatrixDiv = document.querySelector('#d_matrix');
      new agGrid.Grid(cMatrixDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, function (error) {
      $rootScope.loaderAction = false;
      console.error(error);
    });
    $rootScope.isShowDMatrixData = true;
  };

  function getDefaultColumns() {
    return [
      { headerName:"Basic Info",marryChildren : true, headerClass: 'basic-info-header',
        children :[
          {headerName:"S.No.",field: "projectId",width:75 ,cellClass:["grid-cell-text-center"]},
          /*{headerName: "PROJECT UIN",field: "projectUIN",width:100,cellClass:["grid-cell-text-center"]},*/
          {headerName: "LOSS TYPE",field: "loss",width:200,cellClass:["grid-cell-text-center","wrap-grid-text"],
          cellRenderer: function (params) {
            if(params.value == "S MATRIX"){
              return "<span style='color:green; font-weight: bold;'>"+ params.value +"  </span>";
            }else if (params.value == "MACHINE SCRAP" || params.value == "MATERIAL SCRAP"){
              return "<span style='color:red;   font-weight: bold;'>"+ params.value +"  </span>";
            }
            return params.value;
          }},// cellClass: 'cell-wrap-text', autoHeight: true},
          {headerName: "ETU",field: "etu",width:70,cellClass:["grid-cell-text-center"]},
          {headerName: "Process",field: "process",width:90,cellClass:["grid-cell-text-center","wrap-grid-text"]},
          {headerName:"Op No.",field: "operationNo",width:100,cellClass:["grid-cell-text-center"]},
          {headerName: "PROJECT DESCRIPTION",field: "projectName",width:225,cellClass:["grid-cell-text-center-hrz","wrap-grid-text","text-hover-uid"],onCellClicked : onCellClicked},// cellClass: 'cell-wrap-text', autoHeight: true},
          {headerName: "YEARLY POTENTIAL(365 DAYS)",field: "yearlyPotential",width:140,cellClass:["grid-cell-text-center"], cellRenderer : currencyFormatter},
        ]
      }
    ];
  }

  function onCellClicked (gridData) {
    if (gridData.data.projectId) {
      console.log(gridData);
      // $location.path("app/project/"+$scope.projectId+"/8");
      $state.go('app.project',{ projectId : gridData.data.projectId , stage : 1 });
    }
  }

  function currencyFormatter (params) {//Only needed if you need to do some custom parsing before show value in the UI, i.e: '$' + params.value
    return params.value ? "<span> &#8377;" + parseInt(params.value||0).toLocaleString('en-US', {maximumFractionDigits:0, minimumFractionDigits: 0}) + '</span>' : '';
  }

  function customCellRendererModel(params) {}
  
  customCellRendererModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: center;border-bottom: 1px solid #bdc3c7;">'+ getValidData(params.data.totalATTSaving)+'</div>'+
                                '<div class="col-md-12" style="text-align: center;"> '+ getValidData(params.data.totalACTSaving)+' </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };

  customCellRendererModel.prototype.getGui = function getGui() {
    return this.eGui;
  };

  if ($scope.currentStep == 2 ) {
    $scope.loadCMatrixGrid();
  }
  
  
}]);