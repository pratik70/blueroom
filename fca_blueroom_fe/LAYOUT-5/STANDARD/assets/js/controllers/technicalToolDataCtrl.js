

app.controller('technicalToolDataCtrl', ["$rootScope","$scope","commonService","userService","$uibModal",
function($rootScope,$scope,commonService,userService, $uibModal) {


  
   $scope.toolDropdown = ["COMMON TOOLS", "SPECIFIC TOOLS"];
   $scope.pillarApproachDropdown = ["REACTIVE", "PROACTIVE","PREVENTIVE"];
   $scope.technicalToolDropdown = ["Maintenance", "Prod / Process / Quality"];
    $scope.operations = [];
    
  $scope.addTechnicalTool= function(){
    
    $rootScope.showTechTools=false;
    commonService.addTechnicalTool($scope.technicalToolForm).then(function(response){
      $rootScope.mod.dismiss('cancel');
      $rootScope.getTechnicalTools();
    },
    function(error){
      console.log(error);
    });
  }

  $scope.updateWCMMethod = function(){
    $rootScope.showWCMMethods=false;
    commonService.updateWCMMethod($scope.WCMMethodUpdateForm.id,$scope.WCMMethodUpdateForm).then(function(response){
      $rootScope.mod.dismiss('cancel');
      $rootScope.showLossTypes=false;
      $rootScope.showEmployee=false;
      $rootScope.showWCMTools=false;
        
          $rootScope.getWCMMethods();

    },
    function(error){
      console.log(error);
    });
    
  }








  $scope.addOperations= function(){
    
    $rootScope.showOperation=false;
   /* $scope.operationsForm.etu=$rootScope.selectedETU;
    console.log($scope.operationsForm.etu);*/
    $scope.operationsForm.etu_Id=$rootScope.selectedETU.id;
    commonService.addOperations($scope.operationsForm).then(function(response){
      $rootScope.mod.dismiss('cancel');
      $rootScope.getOperations();
    },
    function(error){
      console.log(error);
    });
  }

  $scope.addWCMTool = function(){
    
    $rootScope.showWCMTools=false;
    commonService.addWCMTool($scope.WCMToolForm).then(function(response){
      $rootScope.mod.dismiss('cancel');
      $rootScope.getWCMtools();
    },
    function(error){
      console.log(error);
    });
  }

  $scope.getAllETUType1 = function () {
    userService.getAllETUType().then(function (response) {
        $scope.allETUs  = response.data;
    }, function (error) {
        console.error(error);
    });
}

  
  $scope.cancel = function() {
    
     $rootScope.mod.dismiss('cancel');
  }
  
}]);
