app.controller('financeApprovalCtrl',["$scope","$rootScope","projectService",
function($scope,$rootScope,projectService){
    
  $scope.changeFinanceApproval = function(){
    projectService.changeFinanceApprovalStatusById($rootScope.financeStatusId).then(function(response){
      $rootScope.mod.dismiss('cancel');
      $rootScope.loadEMatrixGrid();
    },
    function(error){
      console.log(error);
    });
  }

  $scope.cancel = function() {
    $rootScope.mod.dismiss('cancel');
  }
  
}]);