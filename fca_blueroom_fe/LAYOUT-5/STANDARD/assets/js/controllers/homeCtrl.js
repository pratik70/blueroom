'use strict';
/** 
  * controller for angular-ladda
  * An angular directive wrapper for Ladda buttons.
*/
app.controller('HomeCtrl', ["$scope", "$rootScope", "$timeout", "commonService", "$state", function ($scope, $rootScope, $timeout, commonService, $state) {
    $scope.ldloading = {};

    $scope.etuList = [];
    $scope.etuList1 = [];

    $scope.etuListTopPosition = [];
    $scope.etuListLeftPosition = [];
    $scope.etuListRightPosition = [];
    $scope.etuListBottomPosition = [];

    commonService.getAllETUType().then(function (response) {
        $scope.etuList1 = response.data || [];
        $scope.etuList = $scope.createGroupedArray($scope.etuList1);

        $scope.etuListTopPosition = $scope.etuList.length >= 1 ? $scope.etuList[0] : [];
        $scope.etuListLeftPosition = $scope.etuList.length >= 2 ? $scope.etuList[1] : [];
        $scope.etuListRightPosition = $scope.etuList.length >= 3 ? $scope.etuList[2] : [];
        $scope.etuListBottomPosition = $scope.etuList.length >= 4 ? $scope.etuList[3] : [];
    }, function (error) {
        console.error(error);
    });

    /*$scope.selectEtu = function(etu){
        $rootScope.selectedETU = etu;
        localStorage.setItem( "ETU_value", JSON.stringify($rootScope.selectedETU));
        $state.go('signin');
    }*/

    $scope.createGroupedArray = function(arr) {
        var chunkSize = Math.round(arr.length / 4 );
        var groups = [], i;
        for (i = 0; i < arr.length; i += chunkSize) {
            groups.push(arr.slice(i, i + chunkSize));
        }
        return groups;
    }

    $scope.clearLocalStorage = function(){
        localStorage.removeItem("ETU_value");
    }

    $scope.clearLocalStorage();

}]);