app.controller('MatricesCtrl', ['$rootScope', '$scope', 
function ($rootScope, $scope) {
  $rootScope.pageHeaderTitle = 'MATRICES'; 
  $scope.currentStep = 4;

  $scope.next = function() {
    $scope.currentStep++;
  };
  $scope.prev = function() {
    $scope.currentStep--;
  };
  $scope.goTo = function(index) {
    var myNode = document.getElementById("d_matrix");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
   
    myNode = document.getElementById("e_matrix");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
    myNode = document.getElementById("f_matrix");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
    myNode = document.getElementById("def_matrix");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
    
    $scope.currentStep = index;
    if ($scope.currentStep == 1) {     
      $scope.$broadcast ('loadCMatrixGrid'); 
    } else if ($scope.currentStep == 2) {
      $scope.$broadcast ('loadDMatrixGrid'); 
    }/* else if ($scope.currentStep == 3) {
      $scope.$broadcast ('loadEMatrixGrid'); 
    } else if ($scope.currentStep == 4) {
      $scope.$broadcast ('loadFMatrixGrid'); 
    } */else if ($scope.currentStep == 5) {
      $scope.$broadcast ('loadDEFMatrixGrid'); 
    } else if ($scope.currentStep == 6) {
      //$scope.$broadcast ('loadAMatrixGrid'); 
    } else if ($scope.currentStep == 7) {
      //$scope.$broadcast ('loadBMatrixGrid'); 
    }
  };

}]);