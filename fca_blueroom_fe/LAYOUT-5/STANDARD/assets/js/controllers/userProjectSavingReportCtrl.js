app.controller('UserProjectSavingReportCtrl', ['$rootScope', '$scope', 'moment', 'factorManagementService',
function ($rootScope, $scope, moment, factorManagementService) {
  $scope.calendarYearList = [];
  $scope.calendarSelectedYear = 2018;
  $scope.displayingReportType = 'savings';
  $scope.displayingReportCount = '';
  $scope.displayingNumberCount = '';
  $scope.isSavingSelected = false;
  $scope.isNumberSelected = false;
  $scope.totalRMCSavings = 0;
  $scope.rmcFactor = 0;
  var startYear = 2018;
  while( startYear < 2030) {
    $scope.calendarYearList.push(startYear);
    startYear++;
  }

  var margin = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 80,
    front: 0,
    back: 0
  };
  $scope.userPerformenceReportData = []; 
  $scope.userSavingsOnProject = function(year) {
    factorManagementService.userSavingsOnProject(year).then(function (response) {
      factorManagementService.getUserSavingsAndProjectFactors(year).then(function (response2) {
        var factorList = response2.data || [];
        var list =   Object.keys(response.data || []).map( function( key ){
          return response.data[key]; 
        });
        var result = list ;
        $scope.usersData = result;
        if ( factorList.length > 0 ) {
          factorList.forEach( function(item) {
            $scope.usersData.some( function(item2,index){
              if (item.userId == item2.userId ) {
                $scope.usersData[index].projectFactor = item.projectFactor;
                $scope.usersData[index].savingFactor = item.savingFactor;
                $scope.usersData[index].noOfProjects = ( item.projectFactor || 0 ) + ( item2.completeNoProject || 0 );
              }
            });
          });
        } else {
          $scope.usersData.some( function(item2,index){
            $scope.usersData[index].projectFactor = 0;
            $scope.usersData[index].savingFactor = 1;
            $scope.usersData[index].noOfProjects = item2.completeNoProject || 0 ;
          });
        }
        $scope.changeGraphOnClicked($scope.displayingReportType); 
       
      }, function (error) {
          console.error(error);
    });
    }, function (error) {
        console.error(error);
    });
  };
  function getOptimisedName(val){ 
    return val &&  val.length > 12 ? val.substring(0, 12) + '...' : val ; 
  }
  function getProjectSavingFactorHeader(year) {
    var plans = []; 
    var month = moment();
    month.month(0);
    month.year(year);
    var endDate = moment(month);
    endDate.add(1,'year');
    while( month < endDate ) {
        plans.push({
          cssClass : month.format('MMM'),
          label : month.format('MMMM'),
          month : parseInt(month.format('MM')),

          monthFactor : 0,
          monthlyProjectSaving : 0,
          totalMonthlyProjectSaving : 0,
          etuId : $scope.etuId
        });
        month.add(1, "month");
    }
    return plans;
  }; 
 
  $scope.changeGraphOnClicked = function (type) {
    if ( type== 'savings') {
      $scope.isSavingSelected = true;
      $scope.isNumberSelected = false;
      $scope.userPerformenceReportData  = $scope.usersData.map(function(d){ return  { letter : getOptimisedName(d.user.employeeId) , frequency :  d.projectSavings  ? (d.projectSavings * d.savingFactor || 1 ) / 1000000 : 0 }; }) ;
      loadProjectSavingBarChart($scope.userPerformenceReportData,'Savings In Millions (INR)' , 2 );
    } else if ( type== 'noOfProject') {
      $scope.isSavingSelected = false;
      $scope.isNumberSelected = true;
      $scope.userPerformenceReportData  = $scope.usersData.map(function(d){ return  { letter : getOptimisedName(d.user.employeeId) , frequency :  d.noOfProjects }; }) ;
      loadProjectSavingBarChart($scope.userPerformenceReportData ,'No of Project\'s',0);
  
    }
  };

  $scope.changeGraphOnCheckClicked = function (top,counts ={}) {
      
      $scope.usersData2  = $scope.usersData.filter(({ user }) => (counts[user.projectSavings] = (counts[user.projectSavings] || 0) + 1) <= top);
                     // users.filter(({ user }) => (counts[user] = (counts[user] || 0) + 1) <= top);
                     // $scope.usersData2  = $scope.usersData.filter(function(d) {return (counts[d.projectSavings] = (counts[d.projectSavings] || 0) + 1) < top});
      $scope.userPerformenceReportData  = $scope.usersData2.map(function(d){ return  { letter : getOptimisedName(d.user.employeeId) , frequency :  d.projectSavings  ? (d.projectSavings * d.savingFactor || 1 ) / 1000000 : 0 }; }) ;
      console.log("$scope.usersData",counts);
      loadProjectSavingBarChart($scope.userPerformenceReportData,'Savings In Millions (INR)' , 2 );

  };




  $scope.changeGraphOnNumberChanged =function(top,counts ={}){
  
    $scope.usersData2  = $scope.usersData.filter(({ user }) => (counts[user.projectSavings] = (counts[user.projectSavings] || 0) + 1) <= top);
  
  
    $scope.userPerformenceReportData  = $scope.usersData2.map(function(d){ return  { letter : getOptimisedName(d.user.employeeId) , frequency :  d.noOfProjects }; }) ;
    loadProjectSavingBarChart($scope.userPerformenceReportData ,'No of Project\'s',0);
  }




  function loadProjectSavingBarChart (data,leftBarHeader, digitFix) {
    var element = d3.select('#userProjectSavingChart').node();
    var width = /*960*/ element.getBoundingClientRect().width - margin.left - margin.right;
    var height = 500 /*element.getBoundingClientRect().height*/  - margin.top - margin.bottom;
    var depth = 100 - margin.front - margin.back;
    data.sort(GetSortOrder("frequency"));
    var xScale = d3.scale.ordinal().rangeRoundBands([0, width], 0.2);

    var yScale = d3.scale.linear().range([height, 0]);
 
    var zScale = d3.scale.ordinal()
    .domain([0, 1, 2])
    .rangeRoundBands([0, depth], 0.4);

    var xAxis = d3.svg.axis()
      .scale(xScale)
      .orient('bottom');

    var yAxis = d3.svg.axis()
      .scale(yScale)
      .orient('left')
      .ticks(10, "Million");


    d3.select('#userProjectSavingChart').selectAll("*").remove();
    var chart = d3.select('#userProjectSavingChart')
    .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
        .attr('transform', svgHelp.translate(margin.left, margin.right));
    xScale.domain(_.uniq(_.map(data, 'letter')));
    yScale.domain([0, _.max(data, 'frequency').frequency]);

    function x(d) { return xScale(d.letter); }
    function y(d) { return yScale(d.frequency); }

    var camera = [width / 2, height / 2, -1000];
    var barGen = bar3d()
      .camera(camera)
      .x(x)
      .y(y)
      .z(zScale(0))
      .width(xScale.rangeBand())
      .height(function(d) { return height - y(d); })
      .depth(xScale.rangeBand());

    chart.append('g')
      .attr('class', 'x axis')
      .style('font-size', '12px')
      .attr('transform', svgHelp.translate(0, height))
      .call(xAxis);

   
    chart.selectAll('g.tick text')
    .style('font-size', '12px')
    .style('font-weight', '600');

        
    var extent = xScale.rangeExtent();
    var middle = (extent[1] - extent[0]) / 2;
    chart.selectAll('g.bar').data(data)
    .enter().append('g')
    .attr('class', 'bar')
    .sort(function(a, b) {
      return Math.abs(x(b) - middle) - Math.abs(x(a) - middle);
    })
    .call(barGen);

     chart.append('g')
      .attr('class', 'y axis')
      .style('font-size', '12px')
      .style('font-weight', '600')
      .call(yAxis);
  
      chart.append('text')
      .attr('x', -(height / 2) - margin.top)
      .attr('y', - margin.left / 1.4)
      .attr('transform', 'rotate(-90)')
      .attr('text-anchor', 'middle')
      .text(leftBarHeader);
   
    chart.selectAll("g.bar")
    .data(data)
    .append("text")
    .attr("class", "bar")
    .attr("text-anchor", "middle")
     .style('font-size', '12px')
      .style('font-weight', '600')
    .attr("x", function(d) { return x(d) + 28; })
    .attr("y", function(d) { return ( y(d) - 20 ) < 0 ?  0 : y(d) - 20 ; })
    .text(function(d) { return ( d.frequency ).toFixed(2); });
  }
  function type(d) {
    d.frequency = +d.frequency;
    return d;
  }

function GetSortOrder(prop) {  
    return function(a, b) {  
        if (a[prop] < b[prop]) {  
            return 1;  
        } else if (a[prop] > b[prop]) {  
            return -1;  
        }  
        return 0;  
    }  
}  

  $scope.userSavingsOnProject($scope.calendarSelectedYear);
}]);
