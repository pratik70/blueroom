app.controller('DashboardMainCtrl', ['$rootScope', '$scope','$timeout','commonService','$state', 'factorManagementService', 'reportService',
function ($rootScope, $scope, $timeout, commonService, $state, factorManagementService, reportService) {
  
  $timeout(function() {
    $rootScope.pageHeaderTitle = 'Dashboard'; 
  }, 10);
  
  $scope.isHideDashboardPanel = false;
  $scope.isShowImageLayoutData = false;
  $scope.isShowAMatrixData = false;
  $scope.isShowBMatrixData = false;
  $scope.isShowCMatrixData = false;
  $rootScope.isShowDMatrixData = false;
  $scope.isShowEMatrixData = false;
  $scope.isShowFMatrixData = false;
  $scope.isShowDEFMatrixData = false;
  $scope.isShowRainbowData = false;
  $scope.isShowSMatrixData = false;
  $scope.isShowSkillInventoryData =false;
  $scope.showFilterPanel = true;
  $scope.imageUrl = "";
  $scope.panelTitle = "Dashboard";
  $scope.activeDashBoard = "";
  $scope.employeeList = [];
  $scope.imagesData = [];
  $scope.maxNoOfProjects = 22;
  $scope.maxSavingInMillions = 8;
  $scope.etuList = [];

  if ( $rootScope.selectedETU ) {
    $scope.selectedETU = $rootScope.selectedETU ;
  } else if ( localStorage.getItem( "ETU_value")) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem( "ETU_value"));
  } else {
    $scope.selectedETU = {};
  }
  $scope.etuId = $scope.selectedETU && $scope.selectedETU.id ? $scope.selectedETU.id : 0;
  $rootScope.isSelectAllETU = !($scope.selectedETU && $scope.selectedETU.id);
  commonService.getAllETUType().then(function(response){
    $scope.etuList = response.data;
  });

  if ( $rootScope.selectedCD ) {
    $scope.selectedCD = $rootScope.selectedCD ;
  } else if ( localStorage.getItem( "CD_value")) {
    $scope.selectedCD =  JSON.parse(localStorage.getItem( "CD_value"));
  } else {
    $scope.selectedCD = {};
  }
  $scope.selectedCD =  $scope.selectedCD.id;
 
  $scope.goToPage = function (page) {
    $state.go(page);
  }; 
  $scope.changeDashPanel = function (panel) {

    if (localStorage.getItem( "ETU_value")) {
      $scope.selectedETU =  JSON.parse(localStorage.getItem( "ETU_value"));
      $scope.etuId = $scope.selectedETU && $scope.selectedETU.id ? $scope.selectedETU.id : 0;
      $scope.etuId = $scope.etuId == 0 ? $scope.etuList && $scope.etuList.length > 0 ? $scope.etuList[0].id : 0 : $scope.etuId ;
    }
    
    if ( localStorage.getItem( "CD_value")) {
      $scope.selectedCD =  JSON.parse(localStorage.getItem( "CD_value")).id;
    }

    $scope.isHideDashboardPanel = true;
    $scope.showFilterPanel = true;
    $scope.activeDashBoard = panel;
    if ( panel === 'plant' ) {
      $scope.panelTitle = "Plant Layout";
      $scope.imageUrl = $rootScope.baseURL + "/layout/Plant Layout?etuId="+  $scope.etuId +"&yearId="+ $scope.selectedCD +"&t="+(new Date()).getTime();
      $scope.isShowImageLayoutData = true;
    }

    
    if ( panel === 'line' ) {
      $scope.panelTitle = "Line Layout";
      $scope.imageUrl = $rootScope.baseURL + "/layout/Line Layout?etuId="+  $scope.etuId +"&yearId="+ $scope.selectedCD +"&t="+(new Date()).getTime();
      $scope.isShowImageLayoutData = true;
    }
    if ( panel === 'plantRMC' ) {
      $scope.panelTitle = "Plant RMC";
      $scope.imageUrl = $rootScope.baseURL + "/layout/Plant RMC?etuId="+  $scope.etuId +"&yearId="+ $scope.selectedCD +"&t="+(new Date()).getTime();
      $scope.isShowImageLayoutData = true;
    }
    
    if ( panel === 'etuRMC' ) {
      $scope.panelTitle = "ETU RMC";
      $scope.imageUrl = $rootScope.baseURL + "/layout/ETU RMC?etuId="+  $scope.etuId +"&yearId="+ $scope.selectedCD +"&t="+(new Date()).getTime();
      $scope.isShowImageLayoutData = true;
    }

    if ( panel === 'amatrix' ) {
      $scope.panelTitle = "A Matrix";
      $scope.isShowAMatrixData = true;
    }
    if ( panel === 'bmatrix' ) {
      $scope.panelTitle = "SDE Cylinder Block B Matrix";
      $scope.isShowBMatrixData = true;
    }
    if ( panel === 'cmatrix' ) {
      $scope.panelTitle = "C Matrix";
      $scope.isShowCMatrixData = true;
      //$scope.$broadcast('loadCMatrixGrid'); 
    }
    if ( panel === 'dmatrix' ) {
      $scope.panelTitle = "D Matrix";
      $rootScope.isShowDMatrixData = true;
      $scope.$broadcast('loadDMatrixGrid'); 
    }
    if ( panel === 'ematrix' ) {
      $scope.panelTitle = "E Matrix";
      $scope.isShowEMatrixData = true;
      $scope.$broadcast('filterEMatrixGrid');  
    }
    if ( panel === 'fmatrix' ) {
      $scope.panelTitle = "F Matrix";
      $scope.isShowFMatrixData = true;
      $scope.$broadcast('filterFMatrixGrid'); 
    }
    if ( panel === 'defmatrix' ) {
      $scope.panelTitle = "7 Steps Of Project Tracking";
      $scope.isShowDEFMatrixData = true;
      $scope.$broadcast('loadDEFMatrixGrid'); 
    }

    if ( panel === 'rainbowChart' ) {
      $scope.panelTitle = "Rainbow Chart";
      $scope.isShowRainbowData = true;
      $scope.getRinbowChartData();
    }

    if ( panel === 'smatrix' ) {
      $scope.panelTitle = "S Matrix";
      $scope.isShowSMatrixData = true;
      $scope.imageUrl = $rootScope.baseURL + "/layout/S Matrix?etuId="+  $scope.etuId +"&yearId="+ $scope.selectedCD +"&t="+(new Date()).getTime();
    }

    if ( panel === 'skillInventory' ) {
      $scope.panelTitle = "Skill Inventory";
      $scope.isShowSkillInventoryData =true;
    }


  };

  $scope.backToDashPanel = function () {
    $scope.panelTitle = "Dashboard";
    $scope.imageUrl = null;
    $scope.isHideDashboardPanel = false;
    $scope.isShowImageLayoutData = false;
    $scope.isShowAMatrixData = false;
    $scope.isShowBMatrixData = false;
    $scope.isShowCMatrixData = false;
    $rootScope.isShowDMatrixData = false;
    $scope.isShowEMatrixData = false;
    $scope.isShowFMatrixData = false;
    $scope.isShowDEFMatrixData = false;
    $scope.isShowRainbowData = false;
    $scope.isShowSMatrixData = false;
    $scope.isShowSkillInventoryData =false;
    $scope.showFilterPanel = true;
  };

  $scope.filterPanelData = function () {
    $scope.showFilterPanel = false;
    if ($scope.activeDashBoard == 'ematrix') {
      $scope.$broadcast('filterEMatrixGrid'); 
    } else if ($scope.activeDashBoard == 'fmatrix') {
      $scope.$broadcast('filterFMatrixGrid'); 
    }
  };


  $scope.calendarYearList = [];
  $scope.calendarSelectedYear = new Date().getFullYear();
  var startYear = 2018;
  while( startYear < 2100) {
    $scope.calendarYearList.push(startYear);
    startYear++;
  }


  function loadRainbowChart() {
    // rainbowChart
    var totalNoOfProjects = $scope.maxNoOfProjects;
    var totalSaving = $scope.maxSavingInMillions;
    totalSaving = totalSaving < 750 ? 750 : totalSaving;
    var xAxis_labels = [];
    var yAxis_labels = [];

    for (var i =1; i <= totalNoOfProjects ; i++) {
      xAxis_labels.push(i);
    }    

    for (var i = 0; i <= totalSaving ; i+=50) {
      // if( i == 1 ){
      //   yAxis_labels.push(0);
      // }
      //yAxis_labels.push(i - 0.5);
      yAxis_labels.push(i);
    }

    var trace1 = {
      x: [0, 12],
      y: [225, 225],
      fill: 'tozeroy',
      type: 'scatter',
      mode: 'none',
      name: 'Basic',
      fillcolor: '#FF0000'
    };

    var trace2 = {
      x: [0,14,14,12],
      y: [375,375,0,0],
      fill: 'tonexty',
      type: 'scatter',
      mode: 'none',
      name: 'Intermediate',
      fillcolor: '#FBD4B4'
    };

    var trace3 = { 
      x: [0,18,18,14],
      y: [600,600,0,0],
      fill: 'tonexty',
      type: 'scatter',
      mode: 'none',
      name: 'Highly Qualified',
      fillcolor: '#FFFF00'
    };

    var trace4= { 
      x: [0,totalNoOfProjects,totalNoOfProjects,18],
      y: [totalSaving,totalSaving,0,0],
      fill: 'tonexty',
      type: 'scatter',
      mode: 'none',
      name: 'Exceptional',
      fillcolor: '#00B050'
    };

    var layout = {
      title: "Rainbow Chart",
      images: $scope.imagesData,
      xaxis: {
        showgrid: false,
        zeroline: false,
        title: "No. of Project",
        fixedrange: true,
        tickvals: xAxis_labels,
        ticktext: xAxis_labels
      },

      yaxis: {
        showline: true,
        /*title: "Savings in Million(INR)",*/
        title: "Savings in Thousands(INR)",
        showticklabels: true,
        fixedrange: true,
        scroll:0,
        type: "Savings(Thousands)",
        tickvals: yAxis_labels,
        ticktext: yAxis_labels,
      }

    };

    var data = [trace1, trace2, trace3, trace4];
    Plotly.newPlot('rainbowChart', data, layout);
    //  {
    //   title: "Rainbow Chart",
    //   images: $scope.imagesData,
    //   xaxis: {
    //     title: "No. of Project",
    //     fixedrange: true,
    //     tickvals: xAxis_labels,
    //     ticktext: xAxis_labels
    //   },
    //   yaxis: {
    //     gridwidth: 1,
    //     title: "Savings in Million(INR)",
    //     showticklabels: true,
    //     type: "Savings(Million)",
    //     tickvals: yAxis_labels,
    //     ticktext: yAxis_labels,
    //   }
    // });
  }

  $scope.getRinbowChartData = function(){
      $scope.imagesData = [];
      factorManagementService.userSavingsOnProject($scope.calendarSelectedYear).then(function (response) {
      factorManagementService.getUserSavingsAndProjectFactors($scope.calendarSelectedYear).then(function (response2) {
        var factorList = response2.data || [];
        var list =   Object.keys(response.data || []).map( function( key ){
          return response.data[key]; 
        });

        var result = list;
        $scope.usersData = result;
        if ( factorList.length > 0 ) {
          factorList.forEach( function(item) {
            $scope.usersData.some( function(item2,index){
              if (item.userId == item2.userId ) {

                $scope.usersData[index].projectFactor = item.projectFactor;
                $scope.usersData[index].savingFactor = item.savingFactor;
                $scope.usersData[index].noOfProjects = (item2.completeNoProject + item.projectFactor) || 0 ;
                $scope.usersData[index].projectSavings = item2.projectSavings ? (item2.projectSavings / 1000) * (item.savingFactor) : 0;
                $scope.maxNoOfProjects = $scope.usersData[index].noOfProjects > $scope.maxNoOfProjects ? $scope.usersData[index].noOfProjects : $scope.maxNoOfProjects;
                $scope.maxSavingInMillions = $scope.usersData[index].projectSavings > $scope.maxSavingInMillions ? $scope.usersData[index].projectSavings : $scope.maxSavingInMillions;
                
                var myObj =  {
                  "source": $rootScope.baseURL + '/userImage/'+ $scope.usersData[index].userId,
                  "xref": "x",
                  "yref": "y",
                  "x": $scope.usersData[index].noOfProjects,
                  "y": $scope.usersData[index].projectSavings,
                  "sizex": 35,
                  "sizey": 35,
                  "xanchor": "center",
                  "yanchor": "middle"
                };    
                $scope.imagesData.push(myObj);

              }
            });
          });
        }

        loadRainbowChart();
      }, function (error) {
          console.error(error);
    });
    }, function (error) {
        console.error(error);
    });
  }

  $scope.getSMatrixImage = function(data){
    if(data == "S Matrix"){
      $scope.imageUrl = $rootScope.baseURL + "/layout/S Matrix?etuId="+  $scope.etuId +"&yearId="+ $scope.selectedCD +"&t="+(new Date()).getTime();
    } else if(data == "QA Matrix"){
      $scope.imageUrl = $rootScope.baseURL + "/layout/QA Matrix?etuId="+  $scope.etuId +"&yearId="+ $scope.selectedCD +"&t="+(new Date()).getTime();
    }
  };

}]);