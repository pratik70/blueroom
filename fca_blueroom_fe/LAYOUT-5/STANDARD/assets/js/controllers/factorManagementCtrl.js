app.controller('FactorManagementCtrl', ['$rootScope', '$scope', 'moment', 'factorManagementService', '$timeout', 'commonService',
function ($rootScope, $scope, moment, factorManagementService, $timeout, commonService) {
  $scope.etuList = [];
  commonService.getAllETUType().then(function(response){
    $scope.etuList = response.data;
  });

  $timeout(function() {
    $rootScope.pageHeaderTitle = 'Factor Management'; 
  }, 10);
  $scope.calendarYearList = [];
  $scope.planYearList = [];
  $scope.usersJsonList = [];
  $scope.projectPlanYearList = [];

  var startYear = 2017;
  while( startYear < 2030) {
    $scope.calendarYearList.push(startYear);
    startYear++;
  }
  $scope.calendarYear = moment().year();
  if ( $rootScope.selectedETU ) {
    $scope.selectedETU = $rootScope.selectedETU ;
  }   else if ( localStorage.getItem( "ETU_value")) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem( "ETU_value"));
  } else {
    $scope.selectedETU = {};
  }
  
  $scope.etuId =  $scope.selectedETU.id ? $scope.selectedETU.id : $scope.etuList && $scope.etuList.length > 0 ? $scope.etuList[0].id : 0;
  //$scope.changeDashPanel('amatrix');
  $scope.loadCalendarYear = function(year) {
  	$scope.planYearList = getFactorHeader(year);
  	factorManagementService.getFactorizationData(year, $scope.etuId).then(function (response) {
   	  if(response && response.data.length > 0 ) {
   			var list = response.data;
				//$scope.planYearList = getFactorHeader(year);
				list.forEach(function(factor1){
					$scope.planYearList.some(function(factor2,index){
						if(factor1.factorMonth == factor2.factorMonth) {
							$scope.planYearList[index].planValue = factor1.planValue;
							$scope.planYearList[index].actualValue = factor1.actualValue;
							$scope.planYearList[index].factorValue = factor1.factorValue <= 0 ? 1 : factor1.factorValue ;			
							return true;
						}
						return false;
					});
				});
   		}
    }, function (error) {
        console.error(error);
    });
	};


	$scope.saveFactorizationData = function() {
		factorManagementService.saveFactorizationData($scope.calendarYear,$scope.planYearList).then(function (response) {
    }, function (error) {
        console.error(error);
    });
	};

	$scope.calculateFactorization = function() {
		for(var index in $scope.planYearList) {
			$scope.planYearList[index].factorValue = ( $scope.planYearList[index].actualValue / $scope.planYearList[index].planValue);
			$scope.planYearList[index].factorValue = $scope.planYearList[index].factorValue && $scope.planYearList[index].factorValue != Infinity ? parseFloat($scope.planYearList[index].factorValue.toFixed(2)) || 1 : 1;
		}
	};

  function getFactorHeader(year) {
    var plans = []; 
    var month = moment();
    month.month(0);
    month.year(year);
    var endDate = moment(month);
    endDate.add(1,'year');
    while( month < endDate ) {
        plans.push({
          cssClass : month.format('MMM'),
          label : month.format('MMM-YYYY'),
          factorMonth : parseInt(month.format('MM')),
          planValue : 0 ,
          actualValue : 0,
          factorValue : 0,
          etuId : $scope.etuId
        });
        month.add(1, "month");
    }
    return plans;
  }; 


  $scope.calculateTotalProject = function(data){
    var index = $scope.usersData.findIndex(function(item, i){
      return item.name === data.name;
    });
    if(index > -1){
      var temp = $scope.usersData[index].noOfProjects ? $scope.usersData[index].noOfProjects : 0;
      $scope.usersData[index].totalProjects = temp + (data.projectFactor = data.projectFactor ? data.projectFactor : 0);
    }
  }


  $scope.calculateTotalSaving = function(data){
    var index = $scope.usersData.findIndex(function(item, i){
    return item.name === data.name;
    });
    if(index > -1){
      var temp = $scope.usersData[index].actualSaving ? $scope.usersData[index].actualSaving : 0;
      $scope.usersData[index].totalSaving = temp * (data.savingFactor = data.savingFactor ? data.savingFactor : 0);
    }
  }

  
 $scope.usersData = [];
  $scope.usersDataq = [
        {
            "name": 'Kishor K',
            "actualSaving": 12000,
            "noOfProjects":4,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
            "totalSaving" : 120000
        },
        {
            "name": 'Gyaneshwar L',
            "actualSaving": 0,
            "noOfProjects":4,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
             "totalSaving" : 120000
        },
        {
            "name": 'C Hapan',
            "actualSaving": 12000,
            "noOfProjects":5,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
             "totalSaving" : 120000
        },
        {
            "name": 'N Varpe',
            "actualSaving": 12000,
            "noOfProjects":2,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
            "totalSaving" : 120000
        },
        {
            "name": 'Kiran B',
            "actualSaving": 12000,
            "noOfProjects":2,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
             "totalSaving" : 120000
        },
        {
            "name": 'Piyush H',
            "actualSaving": 12000,
            "noOfProjects":5,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 , 
            "totalSaving" : 120000
        },
        {
            "name": 'Karthikeyan P',
            "actualSaving": 12000,
            "noOfProjects":4,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
             "totalSaving" : 120000
        },
        {
            "name": 'Amlendu S',
            "actualSaving": 12000,
            "noOfProjects":4,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
             "totalSaving" : 120000
        },
        {
            "name": 'Vinod S',
            "actualSaving": 12000,
            "noOfProjects":5,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
            "totalSaving" : 120000
        },
        {
            "name": 'Shashikant  G',
            "actualSaving": 12000,
            "noOfProjects":2,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
             "totalSaving" : 120000
        },
        {
            "name": 'C Tapise',
            "actualSaving": 12000,
            "noOfProjects":2,
            "projectFactor": 1,
            "savingFactor" : 1.2 ,
            "totalProjects" : 4 ,
            "totalSaving" : 120000
        }
  ];

  
  $scope.userSavingsOnProject = function() {
    factorManagementService.userSavingsOnProject($scope.calendarYear).then(function (response) {
      factorManagementService.getUserSavingsAndProjectFactors($scope.calendarYear).then(function (response2) {
        var factorList = response2.data || [];
        var list =   Object.keys(response.data || []).map( function( key ){
          return response.data[key]; 
        });
       /* var result = groupBy(list, function(item) {
          return item.userId;
        });*/
        var result = list;
        $scope.usersData = result;
        if ( factorList.length > 0 ) {
          factorList.forEach( function(item) {
            $scope.usersData.some( function(item2,index){
              if (item.userId == item2.userId ) {
                $scope.usersData[index].projectFactor = item.projectFactor;
                $scope.usersData[index].savingFactor = item.savingFactor;
                $scope.usersData[index].noOfProjects = item2.completeNoProject || 0 ;
              }
            });
          });
        } else {
          $scope.usersData.some( function(item2,index){
            $scope.usersData[index].projectFactor = 0;
            $scope.usersData[index].savingFactor = 1;
            $scope.usersData[index].noOfProjects = item2.completeNoProject || 0 ;
          });
        }
      }, function (error) {
          console.error(error);
    });
    }, function (error) {
        console.error(error);
    });
  };

  $scope.saveReportData = function() {
    factorManagementService.addUserSavingsOnProjectFactors($scope.calendarYear, $scope.usersData).then(function (response) {
    }, function (error) {
        console.error(error);
    });
  };

  function groupBy( array , f ){
    var groups = {};
    array.forEach( function( o ) {
      var group = JSON.stringify( f(o) );
      if (!groups[group]) {
        groups[group] = {
            "userId" : o.userId,
            "name": o.user.name,
            "actualSaving": o.projectSavingInPercentage,
            "noOfProjects":  1 ,
            "projectFactor": 0,
            "savingFactor" : 0 ,
            "totalProjects" :  1 ,
            "totalSaving" : o.projectSavingInPercentage
        };
      } else {
        groups[group].actualSaving += o.projectSavingInPercentage || 0;
        groups[group].totalSaving += o.projectSavingInPercentage || 0;
        groups[group].noOfProjects += 1;
        groups[group].totalProjects += 1;
        
      }
    });
    return Object.keys(groups).map( function( group ){
      return groups[group]; 
    });
  }


  function getProjectSavingFactorHeader(year) {
    var plans = []; 
    var month = moment();
    month.month(0);
    month.year(year);
    var endDate = moment(month);
    endDate.add(1,'year');
    while( month < endDate ) {
        plans.push({
          cssClass : month.format('MMM'),
          label : month.format('MMM-YYYY'),
          month : parseInt(month.format('MM')),
          monthFactor : 0,
          monthlyProjectSaving : 0,
          totalMonthlyProjectSaving : 0,
          etuId : $scope.etuId
        });
        month.add(1, "month");
    }
    plans.push ({
          cssClass : 'RMC',
          label : 'RMC',
          month : 13,
          monthFactor : 0,
          monthlyProjectSaving : 0,
          totalMonthlyProjectSaving : 0,
          etuId : $scope.etuId
        });
    return plans;
  }; 

  $scope.getProjectMonthlySavingFactor = function() {
    factorManagementService.getProjectMonthlySavingFactor($scope.calendarYear).then(function (response) {
      $scope.projectPlanYearList = getProjectSavingFactorHeader($scope.calendarYear);
      if(response && response.data.length > 0 ) {
        var list = response.data;
        list.forEach(function(factor1){
          $scope.projectPlanYearList.some(function(factor2,index){
            if(factor1.month == factor2.month ) {
              $scope.projectPlanYearList[index].monthlyProjectSaving = factor1.monthlyProjectSaving;
              $scope.projectPlanYearList[index].monthFactor = factor1.monthFactor <= 0 ? 1 : factor1.monthFactor ;     
              return true;
            } 
            return false;
          });
        });
        $scope.loadRMCSavings();
      }
    }, function (error) {
        console.error(error);
    });
  };
  $scope.totalRMCSavings = 0;
  $scope.loadRMCSavings = function () {
    var totalSavings = 0;
    var rmcFactor = 1;
    $scope.projectPlanYearList.forEach( function(d) {
      if (d.month !== 13 ) {
        totalSavings += d.monthlyProjectSaving * d.monthFactor;
      }
      if (d.month == 13) {
        rmcFactor = d.monthFactor; 
      }
    }); 
    $scope.totalRMCSavings = totalSavings ;
  };
  $scope.saveProjectSavingsFactor = function () {
    factorManagementService.saveProjectSavingsFactor($scope.calendarYear,$scope.projectPlanYearList).then(function (response) {
     
    }, function (error) {
        console.error(error);
    });
  };


  $scope.loadCalendarYear($scope.calendarYear);
  $scope.userSavingsOnProject();
  $scope.getProjectMonthlySavingFactor();
}]);