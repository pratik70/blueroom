app.controller('bMatrixColorChangeCtrl',["$scope","$rootScope","dashboardService",
function($scope,$rootScope,dashboardService){
    $scope.colour = '';
    
    
    $scope.getCheckedColor = function(color){
        
        $scope.colour = color;
        $rootScope.bMatrixDataColorForChange.color=$scope.colour;
        if($scope.colour=='red'){
            $rootScope.bMatrixDataColorForChange.valueRed=true;
            $rootScope.bMatrixDataColorForChange.valueGreen=false;
            $rootScope.bMatrixDataColorForChange.valueYellow=false;
        }else if($scope.colour=='green'){
            $rootScope.bMatrixDataColorForChange.valueRed=false;
            $rootScope.bMatrixDataColorForChange.valueGreen=true;
            $rootScope.bMatrixDataColorForChange.valueYellow=false;
        }else if($scope.colour=='yellow'){
            $rootScope.bMatrixDataColorForChange.valueRed=false;
            $rootScope.bMatrixDataColorForChange.valueGreen=false;
            $rootScope.bMatrixDataColorForChange.valueYellow=true;
        }
        
    }
    
    $scope.cancel=function(){
        $rootScope.mod.dismiss('cancel');
    }

    $scope.showButtonToRun = false;
    $("#square1").click(function(){
        $scope.showButtonToRun = true;
        $("#square2").css("opacity","0.8");
        $("#square3").css("opacity","0.8");
        $("#square1").css("opacity","0.4");
    });  
    

    $("#square2").click(function(){
        $scope.showButtonToRun = true;
        $("#square1").css("opacity","0.8");
        $("#square3").css("opacity","0.8");
        $("#square2").css("opacity","0.4");
    });
    
    $("#square3").click(function(){
        $scope.showButtonToRun = true;
        $("#square1").css("opacity","0.8");
        $("#square2").css("opacity","0.8");
        $("#square3").css("opacity","0.4");
    });

    $scope.changeBMatrixDataColor= function(){
        
         
        
        dashboardService.changeBMatrixDataColor($rootScope.bMatrixDataColorForChange).then(function(response){
            
            // $rootScope.changedCellColour = $scope.colour;
            $rootScope.refreshGridAgain(response.data);    
            $rootScope.mod.dismiss('cancel');
        },
        function(error){
            console.log(error);
        });
        
       
        
        //$rootScope.getLoader();
        
    }


}]);