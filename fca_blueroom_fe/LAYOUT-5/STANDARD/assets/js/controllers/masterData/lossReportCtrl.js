app.controller('LossReportCtrl', ["$rootScope", "$scope", "commonService", "masterService", "$uibModal",
function($rootScope, $scope, commonService, masterService, $uibModal) {

//9960411606
//8806122032
//9975537216

//9881775756

  $socpe = this;
  $scope.isLoading = false;
  $scope.selectedETU = null;
  $scope.dailyBasisLossTypes = [];
  $rootScope.selectedDailyBasisLoss = null;
  var rowData = [];


  if (localStorage.getItem("ETU_value")) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem("ETU_value"));
  } else {
    $scope.selectedETU = $rootScope.selectedETU || {};
  }

  var totdayDate = new Date();

  var gridOptions = {
    columnDefs: getDefaultColumns(),
    rowData: rowData,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    groupHeaderHeight:40,
    headerHeight:70,
    rowHeight:40,
    onGridReady: function(params) {
      // params.api.sizeColumnsToFit();
      params.columnApi.autoSizeColumns();
    }
  };


  function getDefaultColumns() {
    return [
      	{headerName: "Loss Type",  field:"dailyBasisLossType.lossName", width:170,  cellClass:["grid-cell-text-center"]},
      	{headerName:"Causul Loss", marryChildren : true, headerClass: 'casual-header',
	        children :[
	        	{headerName: "Direct Labour Cost", field:"causulLoss.directLabourCost",headerClass: 'casual-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Indirect Labour Cost", field:"causulLoss.indirectLabourCost",headerClass: 'casual-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Salaried Cost", field:"causulLoss.salariedCost",headerClass: 'casual-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Indirect Material Cost", field:"causulLoss.indirectMaterialCost",headerClass: 'casual-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Maintenance material Cost", field:"causulLoss.maintenanceMaterialCost",headerClass: 'casual-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Scrap Cost", field:"causulLoss.scrapCost",headerClass: 'casual-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Energy Cost", field:"causulLoss.energyCost",headerClass: 'casual-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Other", field:"causulLoss.other",headerClass: 'casual-header', width:110, cellClass:["grid-cell-text-center"]},
	        ]
	    },
        {headerName:"Resultant Loss",marryChildren : true, headerClass: 'resultant-header',
	        children :[
	        	{headerName: "Direct Labour Cost", field:"resultantLoss.directLabourCost",headerClass: 'resultant-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Indirect Labour Cost", field:"resultantLoss.indirectLabourCost",headerClass: 'resultant-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Salaried Cost", field:"resultantLoss.salariedCost",headerClass: 'resultant-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Indirect Material Cost", field:"resultantLoss.indirectMaterialCost",headerClass: 'resultant-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Maintenance material Cost", field:"resultantLoss.maintenanceMaterialCost",headerClass: 'resultant-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Scrap Cost", field:"resultantLoss.scrapCost",headerClass: 'resultant-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Energy Cost", field:"resultantLoss.energyCost",headerClass: 'resultant-header', width:110, cellClass:["grid-cell-text-center"]},
	        	{headerName: "Other", field:"resultantLoss.other",headerClass: 'resultant-header', width:110, cellClass:["grid-cell-text-center"]},
        	]
        },
        {headerName: "Total", field:"total", width:100,  cellClass:["grid-cell-text-center"]}
    ];
  }

  $scope.loadLossReportGrid = function() {
    // $rootScope.loaderAction = true;
    var myNode = document.getElementById("lossReportId");
    if (myNode) {
      while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
      }
    }

    masterService.getLossReport($scope.selectedETU.id).then(function (response) {
      var data = response.data || [];
      gridOptions.rowData = data;
      var gridDiv = document.querySelector('#lossReportId');
      new agGrid.Grid(gridDiv, gridOptions);
      // $rootScope.loaderAction = false;
    }, function (error) {
        console.error(error);
    });
  };

  $scope.loadLossReportGrid();

}]);