app.controller('MachineDataCtrl', ["$rootScope", "$scope", "commonService", "masterService",
function($rootScope, $scope, commonService, masterService) {

  $scope.etuList = [];
  $scope.selectedETU = null;
  $scope.machinDataList = [];

  if (localStorage.getItem("ETU_value") ) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem("ETU_value"));
  } else {
    $scope.selectedETU = $rootScope.selectedETU || {};
  }
  $scope.etuList.push($scope.selectedETU);

  $scope.defaultOperations = [
    { id : 1 , label : "OP-10" , key : "OP 10" },
    { id : 2 , label : "OP-20" , key : "OP 20"  },
    { id : 3 , label : "OP-30" , key : "OP 30"  },
    { id : 4 , label : "OP-40"  , key : "OP 40" },
    { id : 5 , label : "OP-50"  , key : "OP 50" },
    { id : 6 , label : "OP-60"  , key : "OP 60" },
    { id : 7 , label : "OP-70"  , key : "OP 70" }
    // { id : 8 , label : "OP-80"  , key : "OP 80"  },
    // { id : 9 , label : "OP-90"  , key : "OP 90" },
    // { id : 10 , label : "OP-100" , key : "OP 100"  },
    // { id : 11 , label : "OP-110" , key : "OP 110"  },
    // { id : 12 , label : "OP-120" , key : "OP 120"  },
    // { id : 13 , label : "OP-130" , key : "OP 130"  },
    // { id : 14 , label : "OP-140" , key : "OP 140"  }
  ];

  $scope.initialMachineData = function () {
    $scope.machinDataList = [];
    for (var i = 0; i < $scope.defaultOperations.length; i++) {
      var machinData = {
        etu : $scope.selectedETU,
        opNo : $scope.defaultOperations[i].key,
        noOfMachine : 0,
        factor : 0,
        noOfOperator : 0,
        noOfKWh : 0
      };

      $scope.machinDataList.push(machinData);
    }
  }


  $scope.getMachineDataByEtu = function() {
    // $rootScope.loaderAction = true;
    if(!$scope.selectedETU || !$scope.selectedETU.id) {
      $scope.initialMachineData();
      return;
    }
    masterService.getMachineDataByEtu($scope.selectedETU.id).then(function(response){
      if(response.data && response.data.length > 0) {
        $scope.machinDataList = response.data;
      } else {
        $scope.initialMachineData();
      }
      // $rootScope.loaderAction = false;
    },
    function(error){
      console.log(error);
    });
  }

  $scope.saveMachineData = function() {
    $scope.isLoading = true;
    masterService.saveMachineData($scope.machinDataList).then(function(response){
      if(response.data) {
        $scope.machinDataList = response.data;
      } else {
        $scope.initialMachineData();
      }
      $scope.isLoading = false;
      // $rootScope.loaderAction = false;
    },
    function(error){
      console.log(error);
    });
  }

  $rootScope.refreshMachineData = function() {
    $scope.etuList = [];
    if (localStorage.getItem("ETU_value")) {
      $scope.selectedETU = JSON.parse(localStorage.getItem("ETU_value"));
    } else {
      $scope.selectedETU = $rootScope.selectedETU || {};
    }
    $scope.etuList.push($scope.selectedETU);
    $scope.getMachineDataByEtu();
  }

  $scope.getMachineDataByEtu();

  $scope.cancel = function() {
    $rootScope.mod.dismiss('cancel');
  }


}]);