app.controller('DailyBasisLossCtrl', ["$rootScope", "$scope", "commonService", "masterService", "$uibModal",
function($rootScope, $scope, commonService, masterService, $uibModal) {
  $socpe = this;
  $scope.isLoading = false;
  $scope.selectedETU = null;
  $scope.dailyBasisLossTypes = [];
  $rootScope.selectedDailyBasisLoss = null;
  var rowData = [];

  $scope.dropdownLossTypes = [];
  $scope.lossFilterItems = [];

  if (localStorage.getItem("ETU_value")) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem("ETU_value"));
  } else {
    $scope.selectedETU = $rootScope.selectedETU || {};
  }
  var totdayDate = new Date();

  $scope.filter = {
    dailyBasisLossTypes : null,
    startDate : new Date(totdayDate.setMonth(totdayDate.getMonth() - 1)),
    endDate : new Date(),
    etu : $scope.selectedETU
  };

  var columnDefs =[
    {headerName: "Loss Type", field: "dailyBasisLossType.lossName", width:170, cellClass:["grid-cell-text-center"]},//  cellClass: 'cell-wrap-text', autoHeight: true},
    {headerName: "Date", field: "lossDate", width:65, cellClass:["grid-cell-text-center"],
      cellRenderer: (data) => {
        return moment(data.value).format('MM.DD.YYYY');
      }
    },
    {headerName: "Shift", field: "shift", width:60, cellClass:["grid-cell-text-center"]},//  cellClass: 'cell-wrap-text', autoHeight: true},
    {headerName: "OP no", field: "opNo", width:55, cellClass:["grid-cell-text-center"]},
    {headerName: "Loss Description", field: "lossDescription", width:170, cellClass:["grid-cell-text-center"]},
    {headerName: "Loss in Min", field: "lossInMinutes", width:75, cellClass:["grid-cell-text-center"]},
    {headerName: "Scrap/Rework qty (Same OP)", field: "scrapQuantitySameOP", width:120, cellClass:["grid-cell-text-center"]},//  cellClass: 'cell-wrap-text', autoHeight: true},
    {headerName: "Indirect Material Cost", field: "indirectMaterialCost", width:110, cellClass:["grid-cell-text-center"]},
    {headerName: "Maintenance Material Cost", field: "maintenanceMaterialCost", width:130, cellClass:["grid-cell-text-center"]},
    {headerName: "Others", field: "others",  width:85, cellClass:["grid-cell-text-center"]},
    {headerName: "Scrap qty (Other OP)", field: "scrapQuantityOtherOP", width:100, cellClass:["grid-cell-text-center"]},
    {headerName: "Edit", field: "id", width:55, cellClass:["grid-cell-text-center"],onCellClicked : editDailyLoss,
      cellRenderer: (data) => {
        return '<button class="btn btn-primary btn-o" title="Edit"> <i class="fa fa-pencil"></i> </button>';
      }
    }
  ];

  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    groupHeaderHeight:40,
    headerHeight:70,
    rowHeight:40,
    onGridReady: function(params) {
      params.api.sizeColumnsToFit();
      params.columnApi.autoSizeColumns();
    }
  };

  $scope.loadDailyBasisLossGrid = function() {
    $rootScope.loaderAction = true;
    var myNode = document.getElementById("dailyBasisLossId");
    if (myNode) {
      while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
      }
    }

    $scope.filter.dailyBasisLossTypes = $scope.lossFilterItems || [];

    masterService.getDailyBasisLossFilteredList($scope.filter).then(function (response) {
      var data = response.data || [];
      gridOptions.rowData = data;
      var gridDiv = document.querySelector('#dailyBasisLossId');
      new agGrid.Grid(gridDiv, gridOptions);
      $rootScope.loaderAction = false;
    }, function (error) {
        console.error(error);
    });
  };

  $scope.loadDailyBasisLossTypes = function() {
    masterService.getDailyBasisLossTypes().then(function (response) {
      $scope.dailyBasisLossTypes = response.data || [];
      $scope.dropdownLossTypes = $scope.dailyBasisLossTypes.map(function(map) {
        return {
          "id": map.id,
          "label": map.lossName
        }
      });
    }, function (error) {
        console.error(error);
    });
  }

  $scope.openDailyBasisLossPopup = function() {
    $uibModal.open({
        templateUrl : './assets/views/masterData/newDailyBasisLoss.html',
        controller : 'NewDailyBasisLossCtrl',
        windowClass : 'new-daily-basis-loss',
        backdrop: 'static',
        resolve : {
          data : function (){
              return $scope;
          }
        }
    });
  }

  $scope.setFilter = function() {
    $scope.filter = {
      dailyBasisLossTypes : null,
      startDate : new Date(totdayDate.setMonth(totdayDate.getMonth() - 1)),
      endDate : new Date(),
      etu : $scope.selectedETU
    };
  }

  function editDailyLoss(grid) {
    $rootScope.selectedDailyBasisLoss = grid.data;
    $scope.openDailyBasisLossPopup();
  }

  $scope.addDailyBasisLoss = function() {
    $rootScope.selectedDailyBasisLoss = null;
    $scope.openDailyBasisLossPopup();
  }

  $rootScope.refreshGrid = function() {  
    $scope.filter.dailyBasisLossTypes = [];
    $scope.loadDailyBasisLossGrid();
  };

  $rootScope.resetGridFilter = function() {  
    $scope.setFilter();
    $scope.lossFilterItems = [];
    $scope.loadDailyBasisLossGrid();
  }

  $scope.loadDailyBasisLossGrid();
  $scope.loadDailyBasisLossTypes();
  $scope.setFilter();
}]);


app.controller('NewDailyBasisLossCtrl', ["$rootScope", "$scope", "$uibModalInstance","commonService",'masterService',
function($rootScope, $scope, $uibModalInstance, commonService,masterService, data) {
  $scope.modalTitle = "Add Daily Loss";
  $scope.isLoading = false;
  $scope.selectedETU = null;
  $scope.dailyBasisLossTypes = [];
  $scope.shiftList = ["General", "A-Shift", "B-Shift", "C-Shift"];
  $scope.opNumberList = ["10.1", "10.2", "10.3", "10.4", "10.5", "10.6", "10.7", "10.8", "10.9"];

  if (localStorage.getItem("ETU_value") ) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem("ETU_value"));
  } else {
    $scope.selectedETU = $rootScope.selectedETU || {};
  }

  $rootScope.setDailyBasisForm = function() {
    $scope.dailyBasisForm = {
      dailyBasisLossType : {},
      etu : $scope.selectedETU,
      lossDate : null,
      shift : null,
      opNo : null,
      lossDescription : null,
      lossInMinutes : 0,
      scrapQuantitySameOP : 0,
      indirectMaterialCost : 0,
      maintenanceMaterialCost : 0,
      others : 0,
      scrapQuantityOtherOP :0
    }
    $scope.showAllFeilds(true);
  }

  $rootScope.saveDailyBasisLoss = function(form) {
    if(form.$valid) {
      masterService.saveDailyBasisLoss($scope.dailyBasisForm).then(function (response) {
        $rootScope.refreshGrid();
        $scope.cancel();
      }, function (error) {
          console.error(error);
      });
    } else {
      var field = null, firstError = null;
      for (field in form) {
          if (field[0] != '$') {
              if (firstError === null && !form[field].$valid) {
                  firstError = form[field].$name;
              }
              if (form[field].$pristine) {
                  form[field].$dirty = true;
              }
          }
      }
      angular.element('.ng-invalid[name=' + firstError + ']').focus();      
    }
  }

  $scope.loadDailyBasisLossTypes = function() {
    masterService.getDailyBasisLossTypes().then(function (response) {
      $scope.dailyBasisLossTypes = response.data || [];
    }, function (error) {
        console.error(error);
    });
  }

  $scope.lossChange = function() {
    let isPresentLoss = $scope.dailyBasisForm.dailyBasisLossType && $scope.dailyBasisForm.dailyBasisLossType.id;
    if(isPresentLoss && $scope.dailyBasisLossTypes.length > 0) {
      let lossName = $scope.dailyBasisLossTypes.filter(e => e.id == $scope.dailyBasisForm.dailyBasisLossType.id)[0].lossName;
      if(lossName != 'stand still' && lossName != 'startup-shutdown') {
        $scope.isShow_scrapQuantitySameOP = !(lossName == 'Hydulic/Chemical Over sumption' || lossName == 'Quality mDI');
        $scope.isShow_indirectMaterialCost = true;
        $scope.isShow_maintenanceMaterialCost = !(lossName == 'Tool Over consumption' || lossName == 'Quality mDI' || lossName == 'Type Change')
        $scope.isShow_others = !(lossName == 'Type Change');
        $scope.isShow_scrapQuantityOtherOP = !(lossName == 'Hydulic/Chemical Over sumption');
      } else {
        $scope.showAllFeilds(false);
      }
    } else {
      $scope.showAllFeilds(true);
    }
  }

  $scope.showAllFeilds = function(isShow) {
    $scope.isShow_scrapQuantitySameOP = isShow;
    $scope.isShow_indirectMaterialCost =  isShow;
    $scope.isShow_maintenanceMaterialCost = isShow;
    $scope.isShow_others = isShow;
    $scope.isShow_scrapQuantityOtherOP = isShow;
    if(!isShow) {
      $scope.resetHideFieldsToZero();
    }
  }

  $scope.resetHideFieldsToZero = function() {
    $scope.dailyBasisForm.scrapQuantitySameOP = 0;
    $scope.dailyBasisForm.indirectMaterialCost =  0;
    $scope.dailyBasisForm.maintenanceMaterialCost = 0;
    $scope.dailyBasisForm.others = 0;
    $scope.dailyBasisForm.scrapQuantityOtherOP = 0;
  }

  $scope.lossChangeaAtete = function(lossName) {
    if(lossName != 'stand still' && lossName != 'startup-shutdown') {
      $scope.isShow_scrapQuantitySameOP = !(lossName == 'Hydulic/Chemical Over sumption' || lossName == 'Quality mDI');
      $scope.isShow_indirectMaterialCost = true;
      $scope.isShow_maintenanceMaterialCost = !(lossName == 'Tool Over consumption' || lossName == 'Quality mDI' || lossName == 'Type Change')
      $scope.isShow_others = !(lossName == 'Type Change');
      $scope.isShow_scrapQuantityOtherOP = !(lossName == 'Hydulic/Chemical Over sumption');
    } else {
      $scope.showAllFeilds(false);
    }
  } 

  $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
  };

  $scope.setDailyBasisForm();
  $scope.loadDailyBasisLossTypes();
  $scope.showAllFeilds(true);

  if($rootScope.selectedDailyBasisLoss) {
    $scope.modalTitle = "Update Daily Loss";
    $scope.dailyBasisForm = $rootScope.selectedDailyBasisLoss;
    $scope.dailyBasisForm.lossDate = new Date($scope.dailyBasisForm.lossDate);
    $scope.dailyBasisForm.opNo = parseFloat($scope.dailyBasisForm.opNo);
    setIntervalForHidePage();
  }

  function setIntervalForHidePage() {
    var intervalId = setInterval(function() {
      if($scope.dailyBasisLossTypes.length > 0) {
        $scope.lossChangeaAtete($scope.dailyBasisForm.dailyBasisLossType.lossName);
        clearInterval(intervalId);
      }
    },200);
  }

}]);
