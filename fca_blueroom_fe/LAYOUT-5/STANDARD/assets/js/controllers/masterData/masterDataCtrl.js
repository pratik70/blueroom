app.controller('MasterDataCtrl', ["$rootScope", "$scope", "commonService", "masterService",
function($rootScope, $scope, commonService, masterService) {

  $scope.etuList = [];
  $scope.selectedETU = null;
  $scope.isLoading = false;

  
  if (localStorage.getItem("ETU_value") ) {
    $scope.selectedETU =  JSON.parse(localStorage.getItem("ETU_value"));
  } else {
    $scope.selectedETU = $rootScope.selectedETU || {};
  }

  $scope.etuList.push($scope.selectedETU);
  $scope.setMasterForm = function() {
    $scope.masterDataForm = {
      etu :$scope.selectedETU,
      directLabourCost : 0,
      indirectLabourCost : 0,
      salariedCost : 0,
      energyCost : 0,
      energyFactor : 0,
      noOfLabour : 0,
      cycleTime : 0,
      totalConnecedLoad : 0,
      machineScrap : 0,
      materialScrap : 0,
      noOfWhiteCollar : 0,
      toolCostPerPart : 0
    }
  }

  $scope.getMasterDataByEtu = function() {
    // $rootScope.loaderAction = true;
    if(!$scope.selectedETU || !$scope.selectedETU.id) {
      $scope.setMasterForm();
      return;
    }
    masterService.getMasterDataByEtu($scope.selectedETU.id).then(function(response){
      if(response.data) {
        $scope.masterDataForm = response.data;
      } else {
        $scope.setMasterForm();
      }
      // $rootScope.loaderAction = false;
    },
    function(error){
      console.log(error);
    });
  }

  $scope.saveMasterData = function(form){
    if(form.$valid) {
      // $rootScope.loaderAction = true;
      $scope.isLoading = true;
      masterService.saveMasterData($scope.masterDataForm).then(function(response){
        if(response.data) {
          $scope.masterDataForm = response.data;
        } else {
          $scope.setMasterForm();
        }
        
        $scope.isLoading = false;
        // $rootScope.loaderAction = false;
      },
      function(error){
        console.log(error);
      });
    } else {
      var field = null, firstError = null;
      for (field in form) {
          if (field[0] != '$') {
              if (firstError === null && !form[field].$valid) {
                  firstError = form[field].$name;
              }

              if (form[field].$pristine) {
                  form[field].$dirty = true;
              }
          }
      }
      angular.element('.ng-invalid[name=' + firstError + ']').focus();      
    }
  }

  $rootScope.refreshMasterData = function() {
    $scope.etuList = [];
    if (localStorage.getItem("ETU_value")) {
      $scope.selectedETU =  JSON.parse(localStorage.getItem("ETU_value"));
    } else {
      $scope.selectedETU = $rootScope.selectedETU || {};
    }
    $scope.etuList.push($scope.selectedETU);
    // $scope.etuList = $rootScope.userEtuList || JSON.parse(localStorage.getItem("UserDetails")).etuList;
    $scope.getMasterDataByEtu();
  }


  $scope.getMasterDataByEtu();

}]);