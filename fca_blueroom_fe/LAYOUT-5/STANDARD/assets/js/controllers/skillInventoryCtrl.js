app.controller('SkillInventoryCtrl', ['$rootScope', '$scope', 'matricesService', 'moment',"$timeout", "$state",
function ($rootScope, $scope, matricesService, moment, $timeout, $state) {

  var columnDefs = [];
  $scope.showFilterPanel = true;
  $scope.yearList = [];
  for (var i = 2017;i <= 2050 ;i++)
    $scope.yearList.push(i);


  var rowData=[];
  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    animateRows: true,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    defaultColDef : {
      width: 150,
    },
    groupHeaderHeight:40,
    headerHeight:90,
    rowHeight:40,
    onGridReady: function(params) {
      //params.api.sizeColumnsToFit();
      params.columnApi.autoSizeColumns();
    },
    autoGroupColumnDef: {
      /*<!-- headerName: "Organisation Hierarchy", -->
      <!-- cellRendererParams: { -->
          <!-- suppressCount: true -->
      <!-- } -->*/
    }
  };

  // $scope.$on('filterEMatrixGrid', function(e) { 
  //   $scope.showFilterPanel = true;  
  //   $scope.$parent.showFilterPanel =  true;
  // });

  function clearGrid () {
    var myNode = document.getElementById("skill_inventory");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
  }

  $scope.loadSkillInventoryGrid = function() {
    $rootScope.loaderAction = true;
    matricesService.getEtuUsersSkill().then(function (response) {
      clearGrid();
      $scope.$parent.showFilterPanel =  false;
      $scope.showFilterPanel = false;
      var count=1;
      columnDefs = [{ headerName: "("+count+") - "+"User Name", field:  "userName"  ,width:170,cellClass:["grid-cell-text-center"],pinned: 'left'}] ;
      gridOptions.rowData = response.data.userList || [];
      count++;
      var colMethod =    { headerName: "WCM Methods And Tools " , children : []}  ;
      var columns = response.data.methodsAndToolHeaderList || {};
      for (var key in columns) {
        colMethod.children.push({ headerName: "("+count+") - "+columns[key] , field:  key  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
      count++;
      }
      columnDefs.push(colMethod);

      var space = {headerName : "XXX---XXX", children : []};
      columns = "xxx";
      colMethod.children.push({headerName: 'XXXXXXXXXXX---XXXXXXXXXXXXXX',width:820,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
      columnDefs.push(space);


      var colTechTool =    { headerName: "Technical Tools " , children : []};  
      columns = response.data.technicalToolList || {};
      for (var key in columns) {
        colTechTool.children.push({ headerName: "("+count+") - "+columns[key] , field:  key  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
      count++;
      }
      columnDefs.push(colTechTool);
      gridOptions.columnDefs = columnDefs;
      $timeout(function() {
        var cMatrixDiv = document.querySelector('#skill_inventory');
        new agGrid.Grid(cMatrixDiv, gridOptions);
        $rootScope.loaderAction = false;
      }, 100);

    }, function (error) {
      $rootScope.loaderAction = false;
      console.error(error);
    });
  };

}]);