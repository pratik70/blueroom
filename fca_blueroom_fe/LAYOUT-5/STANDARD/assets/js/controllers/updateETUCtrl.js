

app.controller('updateETUCtrl', ["$rootScope","$scope","commonService","userService","$uibModal",
function($rootScope,$scope,commonService,userService, $uibModal) {
 $scope.toolDropdown = ["COMMON TOOLS", "SPECIFIC TOOLS"];

   $scope.ETUForm={
     id:$rootScope.etuUpdateData.id,
     etuNo:$rootScope.etuUpdateData.etuNo,
     etuName:$rootScope.etuUpdateData.etuName,
     projectAbbrevation:$rootScope.etuUpdateData.projectAbbrevation,
   }
  $scope.updateSelectedETU = function(){
   
    $rootScope.showAllETUs=false;
    commonService.updateETUType($scope.ETUForm).then(function(response){
      $rootScope.mod.dismiss('cancel');
      $rootScope.getAllETU();
    },
    function(error){
      console.log(error);
    });
  }
  $scope.cancel = function() {
    
     $rootScope.mod.dismiss('cancel');
  }
  
}]);
