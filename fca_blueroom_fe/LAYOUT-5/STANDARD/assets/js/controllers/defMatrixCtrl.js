
app.controller('ProjectDetailsCtrl', ["$rootScope","$scope", "$uibModalInstance", "data", '$timeout', 'moment',
function($rootScope,$scope, $uibModalInstance, data, $timeout, moment) {
  $rootScope.financeStatus;
    $rootScope.etuStatus ;
  $scope.projectInfo = data.projectInfo;
  $scope.projectMoreInfo = data.benefitIndentsEMatrix;
  $scope.personTeamLeader = data.personTeamLeader;
  /*if ($scope.projectMoreInfo && $scope.projectMoreInfo.startDate != null ) {
    $scope.projectMoreInfo.startDate = moment($scope.projectMoreInfo.startDate).format('DD.MM.YYYY'); 
  }
  if ($scope.projectMoreInfo && $scope.projectMoreInfo.targetDate != null ) {
    $scope.projectMoreInfo.targetDate = moment($scope.projectMoreInfo.targetDate).format('DD.MM.YYYY'); 
  }*/

  $scope.kaizenDocUrl = $rootScope.baseURL + '/projectDoc/Kaizen/' + $scope.projectInfo.id;
  $scope.EntireWorkUrl = $rootScope.baseURL + '/projectDoc/entirework/' + $scope.projectInfo.id;

  $scope.getKaizenDocUrl = function(projectId) {
    document.getElementById('fred').src =  $rootScope.baseURL + '/projectDoc/Kaizen/' + projectId + "#view=FitH";
   
    // return $rootScope.baseURL + '/projectDoc/Kaizen/' + projectId ;
  };
  $scope.getEntireWorkUrl = function(projectId) {
    document.getElementById('entirework').src =  $rootScope.baseURL + '/projectDoc/entirework/' + projectId + "#view=FitH";
    //return $rootScope.baseURL + '/projectDoc/entirework/' + projectId ;
  };

  $scope.getTransformDate = function ( dateObj) {
    return dateObj && dateObj.length > 0 ? moment(dateObj).format('DD.MM.YYYY') : '';
  }
  $scope.rdLabelPersonTeamLeader = [];
  $scope.rdDataPersonTeamLeader = [];

  $scope.personCoach = data.personCoach;

  $scope.rdLabelPersonCoach = [];
  $scope.rdDataPersonCoach = [];
  $scope.teamMember = data.teamMember;
  $scope.basePillar = data.basePillar;
  $scope.supportingPillars = data.supportingPillars.toString();
  $scope.projectPlan = {};
  $scope.labels = [];
  $scope.series = [];
  $scope.data = [];
  $scope.colors = [];
   // Chart.js Options
  $scope.options  = {

    // Sets the chart to be responsive
    responsive: true,

    //Boolean - Whether to show lines for each scale point
    scaleShowLine: true,
    //Boolean - Whether we show the angle lines out of the radar
    angleShowLineOut: true,

    //Boolean - Whether to show labels on the scale
    scaleShowLabels: true,
    scaleIntegersOnly: true,

    // Boolean - Whether the scale should begin at zero
   /* scaleBeginAtZero: true,*/
    scaleFontSize: 16,
    scaleFontStyle: '700',
    //String - Colour of the angle line
    angleLineColor: 'rgba(0,0,0,.1)',

    //Number - Pixel width of the angle line
    angleLineWidth: 1,

    //String - Point label font declaration
    pointLabelFontFamily: '"Arial"',
    
    pointLabelFontSize: 28,
    
    //String - Point label font weight
    pointLabelFontStyle: 'normal',

    //Number - Point label font size in pixels
    pointLabelFontSize: 10,

    //String - Point label font colour
    pointLabelFontColor: '#666',

    //Boolean - Whether to show a dot for each point
    pointDot: true,

    //Number - Radius of each point dot in pixels
    pointDotRadius: 1,

    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,

    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,

    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,

    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 1,

    //Boolean - Whether to fill the dataset with a colour
    datasetFill: true,

    scaleStartValue : 1,
    scaleSteps : 1,
    showLevelsLabels: true,
 
    scale: {
      reverse: true,
      //yAxes: [{id: 'y-axis-1', type: 'linear', position: 'left', ticks: {min: 6, max:100, fontSize: 40}}]
      /*ticks: {
        max: 5,
        min: 0
      }*/
      ticks: {
        min: 1,
        max: 5,
        stepSize: 1,
        beginAtZero: false
      }
      // pointLabels: {
      //   fontSize: 13,
      //   callback : function(label) {
      //     console.log(" label ", label);
      //     return label.substr(0,7)+'...';
      //   }
      // },
      // gridLines:{
      //   display: true,
      // }
    }

  }; 

  $scope.options12 = {
        legend: {
          display: true,
          position: 'bottom',
          onClick: function(e, legendItem){
                   
                    
           }    
        },
        showLevelsLabels: true,
 
         scale: {
                 reverse: false,
                 ticks: {
                    max:5,
                    min: 1
                  }
             }
      };

   $scope.options4  = {

    // Sets the chart to be responsive
    showLevelsLabels: true,
    responsive: true,
    scaleStartValue : 1,
    scaleSteps : 4,

    scale: {
      reverse: false,
      //yAxes: [{id: 'y-axis-1', type: 'linear', position: 'left', ticks: {min: 6, max:100, fontSize: 40}}]
      ticks: {
        max: 100,
        min: 1
      }

    }
  }; 

  $scope.getRemarkSfromStartWeekDate = function(weekStartDate) {
    var remarkText = "";
    if (data.projectPlanRemarkList.length > 0 ) {
      data.projectPlanRemarkList.some(function(item){
        if(item && item.weekStartDate === weekStartDate) {
          remarkText = item.remarkText;
          return true;
        }
        return false;
      });
    }
    return remarkText;
  };

  $scope.getProfileImageUrl = function (id) {
    return   $rootScope.baseURL + '/userImage/'+ id;
  }
  $scope.getTotalActualSpending = function (data) {
        var total = 0;
        if (data && data.length > 0) {
           data.forEach(value=>{
            total += (value.actualSavingValue || 0); 
           });
           return total;
          //data
        } else {
            return 0;
            
        }
    };
    
    $scope.getTotalProtectedSpending = function (data) {
        var total = 0;
        if (data && data.length > 0) {
           data.forEach(value=>{
            total += (value.expectedSaving || 0); 
           });
           return total;
          //data
        } else {
            return 0;
            
        }
    };

  function loadSavingData () {
    $scope.savingList = getFMatrxMonthsTable($scope.projectInfo.completionDate, $scope.projectInfo.yearlyPotential);
    data.projectSavings.forEach((item) =>{
      $scope.savingList.forEach((data,index) => {
        var d = moment().date(1).month(item.month-1).year(item.year).format('MM-DD-YYYY'); 
        if ( d == data.monthStartDate && ( moment(data.monthStartDate).isSame(moment()) || moment(data.monthStartDate).isBefore(moment()) )) {
            $scope.savingList[index].actualSavingValue = item.actualSavingValue;
            $scope.savingList[index].expectedSaving = item.actualProjectedValue;
            return true;
        }
        return false;
      });
    });
  } 
  function loadExecutionPlan() {
    var startDate = moment($scope.projectMoreInfo.startDate);
    var endDate = moment($scope.projectMoreInfo.targetDate) ;
    if ($scope.projectInfo.completionDate) {
      endDate = moment($scope.projectInfo.completionDate) ;
    }
   
   if ($scope.projectMoreInfo && $scope.projectMoreInfo.aPlannedEndDate) {
      if( endDate < moment($scope.projectMoreInfo.aPlannedEndDate) ){
        endDate = moment($scope.projectMoreInfo.aPlannedEndDate);
      }
   }   

   if ($scope.projectMoreInfo && $scope.projectMoreInfo.pPlannedStartDate) {
      if( startDate > moment($scope.projectMoreInfo.pPlannedStartDate) ){
        startDate = moment($scope.projectMoreInfo.pPlannedStartDate);
      }
   }

    
    $scope.projectPlan.startDate = new Date($scope.projectMoreInfo.startDate);
    $scope.projectPlan.planTable = getSettedPlanTable ($scope.projectMoreInfo,startDate, endDate) ;
    if ( $scope.isPlanningCompleted ){
        endDate = endDate.add(3,'month').endOf('month');
        $scope.projectPlan.executionPlanTable = getSettedPlanTable ($scope.projectMoreInfo, startDate, endDate) ;  
    }
  }

  $scope.ok = function() {
     $uibModalInstance.close($scope.project);
  };

  $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
  };


  $scope.radarChartLeaderWithCanvas = function () {
    $scope.rdDataPersonTeamLeader = [];
    $scope.rdLabelPersonTeamLeader = [];
    if ( $scope.personTeamLeader && $scope.personTeamLeader.radarAxisData) {
      var data1 =  $scope.personTeamLeader.radarAxisData.map(function(val){return val.value < 0 ? 1 :  val.value ; });
      var label1 = $scope.personTeamLeader.radarAxisData.map(function(val){ return val.axis /*&&  val.axis.length > 15 ? val.axis.substring(0, 15) + '...' : val.axis */; });
      if ( data1.length > 30 ) {
          $scope.rdDataPersonTeamLeader = [ data1.slice(0,30)];
          $scope.rdLabelPersonTeamLeader = label1.slice(0,30);
          $scope.rdLabelPersonTeamLeader = $scope.rdLabelPersonTeamLeader.map(function(val){ return val &&  val.length > 15 ? val.substring(0, 15) + '...' : val ; });
      } else {
        $scope.rdDataPersonTeamLeader = [ data1];
        $scope.rdLabelPersonTeamLeader = label1;
        $scope.rdLabelPersonTeamLeader = $scope.rdLabelPersonTeamLeader.map(function(val){ return val &&  val.length > 20 ? val.substring(0, 20) + '...' : val ; });
      }
    }
    //  $scope.rdDataPersonTeamLeader = [];
    // $scope.rdLabelPersonTeamLeader = [];
    // if ( $scope.personCoach.radarAxisData) {
    //   $scope.rdDataPersonTeamLeader =  [$scope.personCoach.radarAxisData.map(function(val){return val.value; })];
    //   $scope.rdLabelPersonTeamLeader = $scope.personCoach.radarAxisData.map(function(val){ return val.axis; });
    // }
  } ;

  $scope.radarChartCoachWithCanvas = function () {
    $scope.rdDataPersonCoach = [];
    $scope.rdLabelPersonCoach = [];
    if ($scope.personCoach &&  $scope.personCoach.radarAxisData) {
      var data1  =  $scope.personCoach.radarAxisData.map(function(val){return val.value < 0 ? 1 :  val.value ; }) ;
      var label1 = $scope.personCoach.radarAxisData.map(function(val){ return val.axis; });

      if ( data1.length > 30 ) {
          $scope.rdDataPersonCoach = [ data1.slice(0,30)];
          $scope.rdLabelPersonCoach = label1.slice(0,30);
          $scope.rdLabelPersonCoach = $scope.rdLabelPersonCoach.map(function(val){ return val &&  val.length > 15 ? val.substring(0, 15) + '...' : val ; });
      } else {
        $scope.rdDataPersonCoach = [ data1];
        $scope.rdLabelPersonCoach = label1;
        $scope.rdLabelPersonCoach = $scope.rdLabelPersonCoach.map(function(val){ return val &&  val.length > 20 ? val.substring(0, 20) + '...' : val ; });
      }
    }
  };

  $scope.radarChartMemberWithCanvas = function () {
    for (var i in $scope.teamMember) {
      $scope.teamMember[i].data = [];
      var label1 = $scope.teamMember[i].radarAxisData.map(function(val){ return val.axis; });
      var data1 =  $scope.teamMember[i].radarAxisData.map(function(val){return val.value < 0 ? 1 :  val.value ; });
      if ( data1.length > 30 ) {
        $scope.teamMember[i].data = [ data1.slice(0,30)];
        $scope.teamMember[i].labels = label1.slice(0,30);
        $scope.teamMember[i].labels = $scope.teamMember[i].labels.map(function(val){ return val &&  val.length > 15 ? val.substring(0, 15) + '...' : val ; });
      } else {
        $scope.teamMember[i].data = [ data1];
        $scope.teamMember[i].labels = label1;
        $scope.teamMember[i].labels = $scope.teamMember[i].labels.map(function(val){ return val &&  val.length > 20 ? val.substring(0, 20) + '...' : val ; });
      } 
    }   
  };

  $scope.loadCanvasData = function (data) {
    var dataList = [];
    if ( data.radarAxisData) {
      dataList = data.radarAxisData.map(function(val){return val.value; });
    }
    return [dataList];
  };
  
  $scope.loadCanvasSeries = function(data) {
    var series = [];
    if ( data.radarAxisData) {
      series = data.radarAxisData.map(function(val){return val.axis; });
    }
    return series;
  };

  $scope.radarChartWithCanvas = function () {
    var colorscale = d3.scale.category10();
    var LegendOptions  = [];
    $scope.series =[];
    $scope.data = [];
    var data1 =[];
    if ( $scope.personTeamLeader && $scope.personTeamLeader.radarAxisData && $scope.personTeamLeader.radarAxisData.length >= 0) {
      var d = [ ];
      $scope.labels =  $scope.personTeamLeader.allTool || [];
      for(var i = 0 ; i <  $scope.labels.length ; i ++ )
        data1.push(1);
      $scope.labels.forEach( function(LABEL,index) {
        var isPresentAxis = false;
        $scope.personTeamLeader.radarAxisData.some(function(item){
          if( LABEL == item.axis) {
            if (item.value && data1[index] < item.value ) {
              data1[index] = item.value;
            } 
            return true;
          }
          return false;
        }); 
        
      });
      //data1 = d;
      LegendOptions.push($scope.personTeamLeader.user.name);
    } 
    if ( $scope.personCoach  && $scope.personCoach.radarAxisData && $scope.personCoach.radarAxisData.length >= 0) {
      $scope.labels.forEach( function(LABEL,index) {
        var isPresentAxis = false;
        $scope.personCoach.radarAxisData.some(function(item){
          if( LABEL == item.axis) {
            if ( item.value && data1[index] < item.value ) {
              data1[index] = item.value;
            } 
            return true;
          }
          return false;
        }); 
         
      });
      LegendOptions.push($scope.personCoach.user.name);
    }    
 
    $scope.teamMember.forEach( function(team) {
      if ( team && team.radarAxisData && team.radarAxisData.length >= 0) {
        $scope.labels.forEach( function(LABEL, index) {
          var isPresentAxis = false;
          team.radarAxisData.some(function(item){
            if( LABEL == item.axis) {
              isPresentAxis = true;
              if ( item.value && data1[index] < item.value ) {
                data1[index] = item.value;
              } 
              return true;
            }
            return false;
          }); 
         
        });
        LegendOptions.push(team.user.name);
      } 
    });
    $scope.data.push(data1);
  } ;

  

  $scope.loadRadarTeamChart = function () {
    var element = d3.select('#projectTeamChart').node();
    var margin = {top: 50, right: 20, bottom: 20, left: 20},
    width =element.getBoundingClientRect().width - margin.left - margin.right,
    height= element.getBoundingClientRect().width - margin.left - margin.right, 
    w = (width )/ 1.5,
    h = ( height ) / 1.5 ;

    var colorscale = d3.scale.category10();

    //Legend titles
    var LegendOptions = [];

    //Data
    var d = [ ];
    if ( $scope.personTeamLeader != null && $scope.personTeamLeader.radarAxisData && $scope.personTeamLeader.radarAxisData.length > 0) {
      d.push( $scope.personTeamLeader.radarAxisData );
      LegendOptions.push($scope.personTeamLeader.user.name);
    } 
    if ( $scope.personCoach != null && $scope.personCoach.radarAxisData && $scope.personCoach.radarAxisData.length > 0) {
      d.push( $scope.personCoach.radarAxisData );
      LegendOptions.push($scope.personCoach.user.name);

    }    
 
    $scope.teamMember.forEach( function(team){
      if ( team && team.radarAxisData && team.radarAxisData.length > 0) {
        d.push( team.radarAxisData );
        LegendOptions.push(team.user.name);
      } 
    });
  //Options for the Radar chart, other than default
    var mycfg = {
      w: w,
      h: h,
      maxValue: 5,
      levels: 5,
      ExtraWidthX: 200,
      showLevelsLabels: false
    };
    $timeout(function() {
      RadarChart.draw("#projectTeamChart", d, mycfg);
      var svg = d3.select('#projectTeamChart')
      .selectAll('svg')
      .append('svg')
      .attr("width", w)
      .attr("height", h);

    //Create the title for the legend
    var text = svg.append("text")
    .attr("class", "title")
    .attr('transform', 'translate(-90,0)') 
    .attr("x", width - 60)
    .attr("y", 10)
    .attr("font-size", "12px")
    .attr("fill", "#404040")
    .text("What % of member use a specific skill");
    var legend = svg.append("g")
    .attr("class", "legend")
    .attr("height", 100)
    .attr("width", 200)
    .attr('transform', 'translate(90,20)') 
    ;
    //Create colour squares
    legend.selectAll('rect')
      .data(LegendOptions)
      .enter()
      .append("rect")
      .attr("x", w - 100)
      .attr("y", function(d, i){ return i * 20;})
      .attr("width", 10)
      .attr("height", 10)
      .style("fill", function(d, i){ return colorscale(i);})
      ;
    //Create text next to squares
    legend.selectAll('text')
      .data(LegendOptions)
      .enter()
      .append("text")
      .attr("x", w - 110)
      .attr("y", function(d, i){ return i * 20 + 9;})
      .attr("font-size", "11px")
      .attr("fill", "#737373")
      .text(function(d) { return d; });
    },100);
    ////////////////////////////////////////////
    /////////// Initiate legend ////////////////
    ////////////////////////////////////////////
  };
  $scope.loadRadarChart = function(id, data, index) {
    if(!data.radarAxisData || data.radarAxisData.length <= 0 )
      return;
    var w = 300,
    h = 300;

    var colorscale = d3.scale.category10();

    //Legend titles
    var LegendOptions = [];

    //Data
    var d = [
        data.radarAxisData
      ];

  //Options for the Radar chart, other than default
    var mycfg = {
      w: w,
      h: h,
      maxValue: 5,
      levels: 5,
      ExtraWidthX: 180,
      legendBoxSize: 5,
      showLevelsLabels: false,
    };

    var docId = index >= 0 ? "#"+id +'_'+index : "#"+id ; 
    //Call function to draw the Radar chart
    //Will expect that data is in %'s
    $timeout(function() {
      RadarChart.draw(docId, d, mycfg);
      var svg = d3.select('projectTeamChart')
    .selectAll('svg')
    .append('svg')
    .attr("width", w)
    .attr("height", h);

    //Create the title for the legend
    var text = svg.append("text")
    .attr("class", "title")
    .attr('transform', 'translate(90,0)') 
    .attr("x", w - 70)
    .attr("y", 10)
    .attr("font-size", "12px")
    .attr("fill", "#404040")
    .text("What % of owners use a specific service in a week");
    
    var legend = svg.append("g")
      .attr("class", "legend")
      .attr("height", 100)
      .attr("width", 200)
      .attr('transform', 'translate(90,20)') 
      ;
      //Create colour squares
      legend.selectAll('rect')
        .data(LegendOptions)
        .enter()
        .append("rect")
        .attr("x", w - 65)
        .attr("y", function(d, i){ return i * 20;})
        .attr("width", 10)
        .attr("height", 10)
        .style("fill", function(d, i){ return colorscale(i);})
        ;
      //Create text next to squares
      legend.selectAll('text')
        .data(LegendOptions)
        .enter()
        .append("text")
        .attr("x", w - 52)
        .attr("y", function(d, i){ return i * 20 + 9;})
        .attr("font-size", "11px")
        .attr("fill", "#737373")
        .text(function(d) { return d; });
    },100);
    ////////////////////////////////////////////
    /////////// Initiate legend ////////////////
    ////////////////////////////////////////////

    var svg = d3.select('#projectDetailsCon')
    .selectAll('svg')
    .append('svg')
    .attr("width", w)
    .attr("height", h);

    //Create the title for the legend
    var text = svg.append("text")
    .attr("class", "title")
    .attr('transform', 'translate(90,0)') 
    .attr("x", w - 70)
    .attr("y", 10)
    .attr("font-size", "12px")
    .attr("fill", "#404040")
    .text("What % of owners use a specific service in a week");
        
    //Initiate Legend 
   /* var legend = svg.append("g")
      .attr("class", "legend")
      .attr("height", 100)
      .attr("width", 200)
      .attr('transform', 'translate(90,20)') 
      ;
      //Create colour squares
      legend.selectAll('rect')
        .data(LegendOptions)
        .enter()
        .append("rect")
        .attr("x", w - 65)
        .attr("y", function(d, i){ return i * 20;})
        .attr("width", 10)
        .attr("height", 10)
        .style("fill", function(d, i){ return colorscale(i);})
        ;
      //Create text next to squares
      legend.selectAll('text')
        .data(LegendOptions)
        .enter()
        .append("text")
        .attr("x", w - 52)
        .attr("y", function(d, i){ return i * 20 + 9;})
        .attr("font-size", "11px")
        .attr("fill", "#737373")
        .text(function(d) { return d; })
        ; */
    };
    function getFMatrxMonthsTable(startDate,totalYearlyPotential) {
      var fMatrix = []; 
      if (startDate) {
          var month = moment(startDate).add(1,'d'); 
          var endDate = moment(startDate).add(1,'d').add(1,'year');
          var totalWorkingDays = endDate.diff(month, 'days');
          var perDaySaving = ( totalYearlyPotential / totalWorkingDays ) || 0;
          while( month <= endDate ) {
              var startOfMonthDate = moment(month).startOf('month') ;
              if (startOfMonthDate.isBefore( moment(startDate))) {
                 startOfMonthDate = month;
              }
              var endOfMonthDate = moment(month).endOf('month');
              if (  endOfMonthDate.isAfter(moment())) {
                  if (endOfMonthDate.format('MM') == moment().format('MM') && endOfMonthDate.format('YYYY')== moment().format('YYYY') ) {
                      endOfMonthDate = moment();
                  } else {
                      endOfMonthDate = month;
                  }
              }
              var workingDaysInMonth = endOfMonthDate.diff(startOfMonthDate, 'days') ;
              workingDaysInMonth = workingDaysInMonth !== null && workingDaysInMonth > 0 ? workingDaysInMonth : 0; 
              fMatrix.push(
                  {   
                      monthStartDate : month.startOf('month').format('MM-DD-YYYY'),
                      monthEndDate : month.endOf('month').format('MM-DD-YYYY'),
                      colorClass : month.format('MMM') ,
                      label : month.format('MMM-YY') ,
                      savingDays :  workingDaysInMonth,
                      expectedSaving :   null ,
                      actualSavingValue : null
                  } 
              );
              month.add(1, "month");
              month = month.startOf('month');
          }
      }
      return fMatrix;
    }


    function getSettedPlanTable (data, startDate, endDate) {
      var plans = getPlanningTable(startDate, endDate);
      plans.forEach(function(weekDates,mainIndex) {
          weekDates.weekOfMonth.forEach(function(item,subIndex) {
            if(isBetweenDate(data.pPlannedStartDate, data.pPlannedEndDate, item.weekEndDate) ) {
               plans[mainIndex].weekOfMonth[subIndex].status = "P"; 
            } else if(isBetweenDate(data.dPlannedStartDate, data.dPlannedEndDate, item.weekEndDate)) {
               plans[mainIndex].weekOfMonth[subIndex].status = "D"; 
            } else if( isBetweenDate(data.cPlannedStartDate, data.cPlannedEndDate, item.weekEndDate)) {
                plans[mainIndex].weekOfMonth[subIndex].status = "C"; 
            } else if(isBetweenDate(data.aPlannedStartDate, data.aPlannedEndDate, item.weekEndDate)) {
                plans[mainIndex].weekOfMonth[subIndex].status = "A"; 
            }

            if(isBetweenDate(data.pActualStartDate, data.pActualEndDate, item.weekEndDate) ) {
               plans[mainIndex].weekOfMonth[subIndex].actualStatus = "P"; 
            } else if(isBetweenDate(data.dActualStartDate, data.dActualEndDate, item.weekEndDate)) {
               plans[mainIndex].weekOfMonth[subIndex].actualStatus = "D"; 
            } else if( isBetweenDate(data.cActualStartDate, data.cActualEndDate, item.weekEndDate)) {
                plans[mainIndex].weekOfMonth[subIndex].actualStatus = "C"; 
            } else if(isBetweenDate(data.aActualStartDate, data.aActualEndDate, item.weekEndDate)) {
                plans[mainIndex].weekOfMonth[subIndex].actualStatus = "A"; 
            }
             
          });
        });
        return plans || [];
    }
    function getPlanningTable(startDate, endDate) {
        var plans = []; 
        if (startDate && endDate) {
            var month = moment(startDate).startOf('month'); 
            while( month <= endDate ) {
              plans.push(
                  {   
                      monthStartDate : month.startOf('month').format('MM-DD-YYYY'),
                      monthEndDate : month.endOf('month').format('MM-DD-YYYY'),
                      colorClass : month.format('MMM') ,
                      label : month.format('MMM-YY') , 
                      //weekOfMonth : getMonthMondays(startDate, endDate, month)
                      weekOfMonth : getMonthWednesdays(startDate, endDate, month)
                  } 
              );
              month = month.add(1, "month").startOf('month');
            }
        }
        return plans;
    };
    // function getMonthMondays (startDate, endDate, date) {
    //     var monday = moment(date).startOf('month').day("Monday");
    //     if ( monday.date() > 7 ) monday.add(7,'d');
    //     var month = monday.month();
    //     var mondayList = [];
    //     var count = 1;
    //     while( month === monday.month() ) {
    //         if (isBetweenDate(startDate, endDate, monday)) {
    //             var data = {
    //                 status : 'X',
    //                 actualStatus : 'X',
    //                 weekNo : count,
    //                 weekStartDate :  moment(monday).format('MM-DD-YYYY'),
    //                 weekEndDate: getWeekLastDate(month, monday)
                    
    //             };
    //             mondayList.push(data);
    //         }
    //         count++;
    //         monday.add(7,'d');
    //     }
    //     return mondayList;
    // }
    function getMonthWednesdays (startDate, endDate, date) {
      var wednesday = moment(date).startOf('month').day("Monday");
      if ( wednesday.date() > 7 ) wednesday.add(7,'d');
      var month = wednesday.month();
      var wednesdayList = [];
      var count = 1;
      while( month === wednesday.month() ) {
          if (isBetweenDate(startDate, endDate, wednesday)) {
              var data = {
                  status : 'X',
                  actualStatus : 'X',
                  weekNo : count,
                  weekStartDate :  moment(wednesday).format('MM-DD-YYYY'),
                  weekEndDate: getWeekLastDate(month, wednesday)
                  
              };
              wednesdayList.push(data);
          }
          count++;
          wednesday.add(7,'d');
      }
      return wednesdayList;
  }
     function getWeekLastDate(month, wednesday) {
        if(moment(wednesday).endOf('month') > moment(wednesday).add(6,'d')){
            return moment(wednesday).add(6,'d').format('MM-DD-YYYY');
        }else{
            return moment(wednesday).endOf('month').format('MM-DD-YYYY');
        }
    }  
  function isBetweenDate(sd, ed, cd){
    if ( sd && ed && cd) {
       var startDate = moment(sd);
       var endDate   = moment(ed);
       var date  = moment(cd);
       return date.isBetween(startDate, endDate) || date.isSame(startDate) || date.isSame(endDate);
    }
    return false;
  } 
  $scope.radarChartMemberWithCanvas();
  loadSavingData();
  loadExecutionPlan();
}]);
app.controller('DEFMatrixCtrl', ['$rootScope', '$scope', 'matricesService','userService', 'moment', '$uibModal','$timeout',
function ($rootScope, $scope, matricesService,userService, moment, $uibModal, $timeout) {
  var columnDefs = [];
  var userRole;
  var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
  $scope.projectStatusData = [[0,10,20,10]];
  $scope.projectStatusLabels = ["PLAN","DO","CHECK","ACT"];
  $scope.projectStatusSeries = [];
  $scope.projectStatusOptions = {
    maintainAspectRatio: false,
    showScale: true,
    barDatasetSpacing: 0,
    tooltipFontSize: 11,
    tooltipFontFamily: "'Helvetica', 'Arial', sans-serif",
    responsive: true,
    scaleBeginAtZero: true,
    scaleShowGridLines: true,
    scaleLineColor: 'transparent',
    barShowStroke: true,
    barValueSpacing: 5,
    scaleStartValue: 0,
    scaleSteps: 10,
  };

  userApproval = userDetails.approval;
  userRole = userDetails.roles[0].authority;

  $scope.AdminEMatrix = {
    id : '',
    status : ""
    
  };

  var margin = {
    top: 50,
    right: 20,
    bottom: 30,
    left: 80,
    front: 0,
    back: 0
  };

  var width = 960 - margin.left - margin.right;
  var height = 500 - margin.top - margin.bottom;
  var depth = 100 - margin.front - margin.back;

  var xScale = d3.scale.ordinal().rangeRoundBands([0, width], 0.2);

  var yScale = d3.scale.linear().range([height, 0]);

  var zScale = d3.scale.ordinal().domain([0, 1, 2])
  .rangeRoundBands([0, depth], 0.4);

  var xAxis = d3.svg.axis()
    .scale(xScale)
    .orient('bottom');

  var yAxis = d3.svg.axis()
    .scale(yScale)
    .orient('left')
    .ticks(10, "Million");



  $scope.stageWorkingData = {};
  var rowData=[];
  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    suppressMovable:true,
    defaultColDef : {
      width: 150,
    },
    groupHeaderHeight:40,
    headerHeight:90,
    rowHeight:50,
    onGridReady: function(params) {
      params.columnApi.autoSizeColumns();
    }
  };

  $scope.$on('loadDEFMatrixGrid', function(e) {  
      $scope.loadDEFMatrixGrid();        
  });

  $scope.filterData = {
    startOfYear : moment().format("YYYY"),
    startOfMonth : "1",
    endOfYear :  moment().format("YYYY"),
    endOfMonth : parseInt(moment().startOf('month').format("MM"))+"",
    isEmployee: false
  };
  $scope.filterByState = function(state) {
    $scope.filterData.currentWorkingState = state;
    $scope.loadDEFMatrixGrid();
  };

  $scope.$on('filterFMatrixGridReport', function(e) {  
    var myNode = document.getElementById("def_matrix");
    while (myNode && myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
    $scope.showReportPanel = true;        
  });

  $scope.goToReport = function (points, evt) {
  };
  
  $scope.loadDEFMatrixGrid = function(filterData) {

    var gridOptions = {
      columnDefs: columnDefs,
      rowData: rowData,
      enableFilter: true,
      enableSorting: true, 
      enableColResize: true, 
      suppressMovable:true,
      defaultColDef : {
        width: 150,
      },
      groupHeaderHeight:40,
      headerHeight:90,
      rowHeight:50,
      onGridReady: function(params) {
        params.columnApi.autoSizeColumns();
      }
    }; 
    $rootScope.loaderAction = true;
    var myNode = document.getElementById("def_matrix");
    while (myNode && myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
    $scope.filterData.isEmployee = userRole;
    matricesService.getDEFMatricesData($scope.filterData).then(function (response) {
      if ( $scope.$parent.showReportPanelChnage ) {
        $scope.$parent.showReportPanelChnage(false);
      }
      $scope.showReportPanel = false;
      gridOptions.rowData = response.data.projectList || [];
      columnDefs = getDefaultColumns();
      var step3Col = { headerName: "B.R. STEP 3",field: "projectUIN",width:74,cellClass:["grid-cell-text-center"],marryChildren: true,
        children: [
            {headerName: "PROJECT DESCRIPTION", field: "projectName" ,headerClass:'method-tool-bg-color',width:150 ,cellClass : [ "wrap-grid-text"]},
            {headerName: "PRELIMINARY ANALYSIS", field: "preliminaryAnalysis",headerClass:'method-tool-bg-color',cellClass : [ "grid-cell-text-center"],},
            {headerName: "WCM PILLAR", field: "basePillar",headerClass:'method-tool-bg-color',width:75}

        ]};

      var supportingPillarsCol = { 
        headerName: "SUPPORTING PILLARS", 
        marryChildren: true,
        children: []
      };

      var columns = response.data.pillarColumns;
      for (var key in columns) {
        supportingPillarsCol.children.push({headerName: columns[key] , field:  key ,headerClass:'my-class pillar-bg-color',width:50,cellClass:["grid-cell-text-center"]});
      }
      step3Col.children.push(supportingPillarsCol);
      var methodsAndToolCol = { 
          headerName: "WCM METHODS AND TOOLS", 
          groupHeaderClass:'method-tool-bg-color',
          marryChildren: true,
          children: []
        };
      columns = response.data.methodColumns;
      var count = 0 ;
      var statusCol = 'open';
      for (var key in columns) {
        if (count > 10 )
           statusCol = 'closed';
        methodsAndToolCol.children.push({headerName: columns[key] , field:  key ,headerClass:'my-class method-tool-bg-color',width:90,cellClass:["grid-cell-text-center"],columnGroupShow : statusCol });
        count++;
      }
      statusCol = 'open';
      count = 0 ;
      columns = response.data.toolColumns;
      for (var key in columns) {
        if (count > 10 )
           statusCol = 'closed';
        methodsAndToolCol.children.push({headerName: columns[key] , field:  key ,headerClass:'my-class method-tool-bg-color',width:90,cellClass:["grid-cell-text-center"] ,columnGroupShow : statusCol});
        count++;
      }
      step3Col.children.push(methodsAndToolCol);
      columnDefs.push(step3Col);
      
      var step4Col = { headerName: "B.R. STEP 4", width:74,cellClass:["grid-cell-text-center"], children: [],marryChildren: true};
      var team = { 
        headerName: "PROJECT TEAM",  disableMoveColumn: true,marryChildren: true,
        children: [
          {headerName: "TEAM LEADER",headerClass:'team-bg-color',field: "personResponsible",cellClass :["wrap-grid-text"]}
        ]
      };
      columns = response.data.totalTeamMemberCount;
      for (var i = 0; i < columns-1 ; i++) {
        team.children.push({headerName: "TEAM MEMBER - "+(i+1) ,headerClass:' team-bg-color', field:  "team_member_"+(i+1) ,width:120,cellClass:["grid-cell-text-center","wrap-grid-text"]});
      }
      team.children.push( {headerName: "TEAM COACH",headerClass:'team-bg-color', field: "coach" ,cellClass :["wrap-grid-text"]});
      step4Col.children.push(team);
      columnDefs.push(step4Col);

      var step5Col = { headerName: "B.R. STEP - 5",width:74,marryChildren: true,
          children: [
            { headerName: "PROJECT START DATE",width:100, field: "startDateFormated",cellClass:["grid-cell-text-center"] },
            { headerName: "PROJECT TARGET DATE",width:100, field: "targetDateFormated",cellClass:["grid-cell-text-center"] },
            /* { headerName: "PROJECT PROGRESS",width:100, field: "", cellRenderer :  customCellSavingTracking},
            { headerName: "CALENDER PRIOR YEAR",width:100, field: "gold" ,cellClass:["grid-cell-text-center"]},*/
            {headerName: "Plan \n Actual",field: "plan",width:109, cellRenderer: customCellRendererModel }
          ]
        };
        //var month = moment().startOf('month');
        var month = moment();
        month.month(0);
        //month.year($scope.filterEMatrix.startOfYear);
        var headerPlan = getPlanningHeader(month);
        for ( var i in headerPlan  ) {
          step5Col.children.push({ headerName: headerPlan[i].label, field: headerPlan[i].date, width:170, cellRenderer: customCellRendererPlanModel });
        }
      step5Col.children.push({ headerName: "PDCA STATUS",width:100, field: "gold",cellClass:["grid-cell-text-center"] , cellRenderer : PDCAStatusComponent });
      if ($scope.filterData.currentWorkingState == null) {
        step5Col.children.push({ headerName: "PROJECT CLOSURE DATE",width:100, field: "projectCloserDate", cellClass:["grid-cell-text-center"] });
      }
      columnDefs.push(step5Col);

      if ($scope.filterData.currentWorkingState == null) {
        var step6Col = {
          headerName: "B.R. STEP - 6",cellClass:["grid-cell-text-center"],marryChildren: true,
          children: [
            { headerName: "PROJECT COST (Rs)", field: "totalProjectCost",width:100,headerClass:'my-class saving-bg-color',cellClass:["grid-cell-text-center"] },
            { headerName: "B/C RATIO", field: "bc",width:100,headerClass:'my-class saving-bg-color',cellClass:["grid-cell-text-center"] },
            /*  { headerName: "SAVINGS TRACKING",width:100, field: "",headerClass:'my-class saving-bg-color' ,cellRenderer :  customCellSavingTracking},
            { headerName: "CALENDER PRIOR YEAR",width:100, field: "",headerClass:'my-class saving-bg-color' },*/
          ]
        };
        headerPlan = getSavingsHeader();
        for (var i in headerPlan  ) {
          step6Col.children.push({headerName: headerPlan[i].label ,field: headerPlan[i].date ,width:170, headerClass:'saving-bg-color', cellRenderer: customCellRendererSavingModel });
        }
        step6Col.children.push( { headerName: "HARD SAVINGS (Rs)",width:100, field: "hardSavings",headerClass:'my-class saving-bg-color',cellClass:["grid-cell-text-center"] });
        step6Col.children.push( { headerName: "VIRTUAL SAVINGS (Rs)",width:100, field: "virtualSavings",headerClass:'my-class saving-bg-color',cellClass:["grid-cell-text-center"] });
        step6Col.children.push( { headerName: "COST AVOIDANCE (Rs)",width:100, field: "costAvoidance",headerClass:'my-class saving-bg-color',cellClass:["grid-cell-text-center"] });
        step6Col.children.push( { headerName: "SOFT SAVINGS (Rs)",width:100, field: "softSavings",headerClass:'my-class saving-bg-color',cellClass:["grid-cell-text-center"] });
           
      /*  step6Col.children.push( { headerName: "CALENDER FOLLOW YEAR",width:100, field: "",headerClass:'my-class saving-bg-color' });*/
        columnDefs.push(step6Col);
        columnDefs.push({ headerName: "B.R. STEP - 7",field: "projectUIN",width:74,cellClass:["grid-cell-text-center"],
          children: [
            { headerName: "STANDARDISATION", field: "gold" ,marryChildren: true,
              children: [
                { headerName: "STANDARDISATION (SOP etc.)", field: "standardisation" ,cellClass:["grid-cell-text-center"]},
                { headerName: "HORIZONTAL EXPANSION", field: "horizontalExpansion",cellClass:["grid-cell-text-center"] },
                { headerName: "EEM / EPM /MP INFO", field: "epmInfo" ,cellClass:["grid-cell-text-center"]},
                //{ headerName: "KAIZEN SHEET", field: "kDocument" ,cellClass:["grid-cell-text-center"], onCellClicked : onCKaizenDoc}, 
                { headerName: "KAIZEN SHEET", field: "kDocument" ,cellTemplate:'<div><i class="fas fa-file-alt"></i></div>'  ,cellClass:["grid-cell-text-center"], onCellClicked : onCKaizenDoc,
                cellRenderer: function (params) {
                  if(params.value != "Document Not Available"){
                    return '<i class="glyphicon glyphicon-file"></i>';
                  }else {
                   // return  params.value ;
                   return  " " ;  
                  }
            }},   




                { headerName: "EWO DOCUMENT", field: "ewoDocument", cellTemplate:'<div><img ng-src="{{row.getProperty(\'ewoDocument.url\')}}" </img></div>'  ,cellClass:["grid-cell-text-center"], onCellClicked : onEWODocument,
                cellRenderer: function (params) {
                  if(params.value != "Document Not Available"){
                    return '<i class="glyphicon glyphicon-file"></i>';
                  }else {
                    //return  params.value ;
                    return  " " ;  
          
                  }
            }},             
             /*   { headerName: "EWO DOCUMENT", field: "ewoDocument", 
                cellTemplate:'<div>row.getProperty(<i class="fas fa-file-alt"></i>)</div>'  ,
                cellClass:["grid-cell-text-center"], onCellClicked : onEWODocument},*/             
              ]
            },
          ]});
        
      }
      gridOptions.columnDefs = columnDefs;
      $timeout(function() {
        var defMatrixDiv = document.querySelector('#def_matrix');
        new agGrid.Grid(defMatrixDiv, gridOptions);
        $rootScope.loaderAction = false;
      }, 100);

    }, function (error) {
      $rootScope.loaderAction = false;
      console.error(error);
    });
  };

  function deleteProject() {
     
    //return '<button ng-click="ageClicked(data.age)" ng-bind="data.age"></button>';
    return '<button >Delete Project</button>';
}
function checkboxClicked (gridData){
  if (!gridData.data) {
    return;
  }
  $rootScope.loaderAction = true;
  matricesService.getProjectDetails(gridData.data.projectId).then(function(response) {
    var projectCostingCtrlInstance = $uibModal.open({
      templateUrl : './assets/views/dashboard/projectDeletionDialog.html',
      controller : 'ProjectDetailsCtrl',
      size : 'sm',
      windowClass : 'projectDetailsWindow',
      resolve : {
        data : function (){
          return response.data;
        }
      }
    });

    $rootScope.loaderAction = false;

    projectCostingCtrlInstance.result.then(function(project) {
    }, function() {
        //$log.info('Modal dismissed at: ' + new Date());
    });
  }, function() {
      //$log.info('Modal dismissed at: ' + new Date());
  });

 }


  function onCellClicked (gridData) {
    if (!gridData.data) {
      return;
    }
    $rootScope.loaderAction = true;
    matricesService.getProjectDetails(gridData.data.projectId).then(function(response) {
      var projectCostingCtrlInstance = $uibModal.open({
        templateUrl : './assets/views/dashboard/ProjectDetailsDialog.html',
        controller : 'ProjectDetailsCtrl',
        size : 'lg',
        windowClass : 'projectDetailsWindow',
        resolve : {
          data : function (){
            return response.data;
          }
        }
      });

      $rootScope.loaderAction = false;

      projectCostingCtrlInstance.result.then(function(project) {
      }, function() {
          //$log.info('Modal dismissed at: ' + new Date());
      });
    }, function() {
        //$log.info('Modal dismissed at: ' + new Date());
    });

  }

  function onCKaizenDoc (gridData) {
    if (!gridData.data) {
      return;
    }
    $rootScope.loaderAction = true;
    matricesService.getProjectDetails(gridData.data.projectId).then(function(response) {
      var projectCostingCtrlInstance = $uibModal.open({
        templateUrl : './assets/views/dashboard/kaizenSheetDialog.html',
        controller : 'ProjectDetailsCtrl',
        size : 'lg',
        windowClass : 'projectDetailsWindow',
        resolve : {
          data : function (){
            return response.data;
          }
        }
      });

      $rootScope.loaderAction = false;

      projectCostingCtrlInstance.result.then(function(project) {
      }, function() {
          //$log.info('Modal dismissed at: ' + new Date());
      });
    }, function() {
        //$log.info('Modal dismissed at: ' + new Date());
    });

  }
  function onEWODocument (gridData) {
    if (!gridData.data) {
      return;
    }
    $rootScope.loaderAction = true;
    matricesService.getProjectDetails(gridData.data.projectId).then(function(response) {
      var projectCostingCtrlInstance = $uibModal.open({
        templateUrl : './assets/views/dashboard/ewoDocument.html',
        controller : 'ProjectDetailsCtrl',
        size : 'lg',
        windowClass : 'projectDetailsWindow',
        resolve : {
          data : function (){
            return response.data;
          }
        }
      });

      $rootScope.loaderAction = false;

      projectCostingCtrlInstance.result.then(function(project) {
      }, function() {
          //$log.info('Modal dismissed at: ' + new Date());
      });
    }, function() {
        //$log.info('Modal dismissed at: ' + new Date());
    });

  }


  $scope.stageWorkingCount = function () {
    $rootScope.loaderAction = true;
    matricesService.stageWorkingCount().then(function(response) {
      if (response) {
        $scope.stageWorkingData = response.data;
        loadProjectStatusBarChart($scope.stageWorkingData);
      }
      $rootScope.loaderAction = false;
    }, function() {
        //$log.info('Modal dismissed at: ' + new Date());
         $rootScope.loaderAction = false;
    });
  };

  function getDefaultColumns() {
      return [
      { headerName:"B.R. STEP 1 & 2", width:74, cellClass : [ "grid-cell-text-center"],
        children: [
            {headerName: "PROJECT NO", field: "projectUIN" ,cellClass : [ "grid-cell-text-center text-hover-uid"], onCellClicked : onCellClicked },
            {headerName: "LOSS TYPE", field: "loss", cellClass : [ "grid-cell-text-center","wrap-grid-text"],
              cellRenderer: function (params) {
                  if(params.value == "S MATRIX"){
                    return "<span style='color:green; font-weight: bold;'>"+ params.value +"  </span>";
                  }else if (params.value == "MACHINE SCRAP" || params.value == "MATERIAL SCRAP"){
                    return "<span style='color:red;   font-weight: bold;'>"+ params.value +"  </span>";
                  }
                  return params.value;
            }},
            {headerName: "ETU",field: "etu",width:70,cellClass:["grid-cell-text-center"]},
            {headerName: "Process",field: "process",width:120,cellClass:["grid-cell-text-center"]},
            {headerName: "OP NO.", field: "operationNo", cellClass : [ "grid-cell-text-center"],width:60},
            {headerName: "Potential Savings", field: "yearlyPotential" ,cellClass : [ "grid-cell-text-center"], 
              cellRenderer: function (params) {
                return params.value ? "<span> &#x20b9;" + parseInt(params.value||0).toLocaleString('en-US', { maximumFractionDigits : 0 , minimumFractionDigits: 0 }) + '</span>' : '';
              }
            }

        ]
      },
      
      /* 
        { headerName: "B.R. STEP - 5",field: "projectUIN",width:74,cellClass:["grid-cell-text-center"],
          children: [
            { headerName: "PROJECT START DATE", field: "startDate" },
            { headerName: "PROJECT PROGRESS", field: "projectProgress" },
            { headerName: "CALENDER PRIOR YEAR", field: "gold" },
            { headerName: "CALENDER FOLLOW YEAR", field: "gold" },
            { headerName: "PDCA STATUS", field: "gold" },
            { headerName: "PROJECT CLOSER DATE", field: "targetDate" },
          ]
        },
        { headerName: "B.R. STEP - 6",cellClass:["grid-cell-text-center"],
        children: [
           
        ]},
         { headerName: "B.R. STEP - 7",field: "projectUIN",width:74,cellClass:["grid-cell-text-center"],
        children: [
          { headerName: "STANDARDISATION", field: "gold" ,
            children: [
              { headerName: "STANDARDISATION (SOP etc.)", field: "gold" },
              { headerName: "HORIZONTAL EXPANSION", field: "horizontalExpansion" },
              { headerName: "EEM / EPM /MP INFO", field: "gold" },

               
            ]
          },
        ]}*/
    ];
  }


  function MyHeaderComponent(params) {
  }

 MyHeaderComponent.prototype.init = function (params) {
    // var resultElement = document.createElement("div");
    // resultElement.setAttribute("class", "row");
    // resultElement.setAttribute("style", " margin-top: -3px;"); 
    // resultElement.innerHTML = '<div class="column">'+ 
    //                             '<div class="col-md-12" style="text-align: center;border-bottom: 1px solid #bdc3c7;"> SAV ATT </div>'+
    //                             '<div class="col-md-12" style="text-align: center;"> ACT </div> '+
    //                             '</div>'; 
    // this.eGui = resultElement;
  };

  // MyHeaderComponent.prototype.getGui = function getGui() {
  //   return this.eGui;
  // };

  function isDateSameOrAfter (d1, d2) {
    if (d1.isSame(d2) || d1.isAfter(d2)) {
      return true;
    }
    return false;
  }

  function isDateSameOrBefore (d1, d2) {
    if (d1.isSame(d2) || d1.isBefore(d2)) {
      return true;
    }
    return false;
  }

  function PDCAStatusComponent() {
  }

 PDCAStatusComponent.prototype.init = function (params) {
    var executionStarted = params.data.executionStarted;
    var planningData  = params.data.benefitIndentsEmatrix;
    var pStage ='',cStage ='',dStage ='',aStage ='';
    if (  executionStarted ) {
      // if ( planningData.aActualEndDate && isDateSameOrBefore(moment(planningData.aActualEndDate), moment(planningData.pPlannedEndDate))  ){
      //   pStage ='active';dStage ='active';cStage ='active';aStage ='active';
      // } else {
      //   if ( planningData.cActualEndDate && isDateSameOrBefore(moment(planningData.cActualEndDate) ,moment(planningData.cPlannedEndDate) ) ){
      //     pStage ='active';dStage ='active';cStage ='active-org';
      //   } else {
      //     if ( planningData.dActualEndDate && isDateSameOrBefore( moment(planningData.dActualEndDate) , moment(planningData.dPlannedEndDate) ) ){
      //       pStage ='active';dStage ='active-org';
      //     } else {
      //       if ( planningData.pActualEndDate && isDateSameOrBefore(moment(planningData.pActualEndDate) , moment(planningData.pPlannedEndDate) ) ){
      //         pStage ='active-org';
      //       }
      //     }
      //   }
      // }
      if (planningData.aActualEndDate){
        var aPlannedEndDate = Date.parse(planningData.aPlannedEndDate);
        var aActualEndDate = Date.parse(planningData.aActualEndDate);
        if (aPlannedEndDate <= aActualEndDate) {
          pStage ='active';dStage ='active';cStage ='active';aStage ='active';
        } else {
          pStage ='active';dStage ='active';cStage ='active';aStage ='active-org';
        }
        
      } else {
        if (planningData.cActualEndDate) {
          pStage ='active';dStage ='active';cStage ='active-org';
        } else {
          if (planningData.dActualEndDate) {
            pStage ='active';dStage ='active-org';
          } else {
            if (planningData.pActualEndDate) {
              pStage ='active-org';
            }
          }
        }
      }
    } else {
      pStage ='';dStage ='';cStage ='';aStage ='';
    } 
   
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: 4px;display: flex; justify-content: center;"); 
    resultElement.innerHTML = '<div class="circ">'+
      '<div class="sect first ' + dStage + '"><span class="text"> D </span></div>'+
      '<div class="sect ' + cStage + '"><span class="text"> C </span></div>'+
      '<div class="sect ' + pStage + '"><span class="text"> P </span></div>'+
      '<div class="sect ' + aStage + '"><span class="text"> A </span></div>'+
      '</div>'; 
      // resultElement.innerHTML = '<table>'+ 
      // '<tr> <td> P</td><td> D</td></tr>'+
      // '<tr> <td> A</td><td> C</td></tr>'+
      // '</table>';  
    this.eGui = resultElement;
  };

  PDCAStatusComponent.prototype.getGui = function getGui() {
    return this.eGui;
  };

function customCellSavingTracking(params) {}
  
  customCellSavingTracking.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: center;border-bottom: 1px solid #bdc3c7;"> Plan </div>'+
                                '<div class="col-md-12" style="text-align: center;"> Actual </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };

  customCellSavingTracking.prototype.getGui = function getGui() {
    return this.eGui;
  };

  function customCellRendererModel(params) {}
  
  customCellRendererModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: center;border-bottom: 1px solid #bdc3c7;"> Plan </div>'+
                                '<div class="col-md-12" style="text-align: center;"> Actual </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };

  customCellRendererModel.prototype.getGui = function getGui() {
    return this.eGui;
  };

 function customCellRendererTotalModel(params) {}
  
  customCellRendererTotalModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: center;border-bottom: 1px solid #bdc3c7;">'+ getValidData(params.data.totalATTSaving)+'</div>'+
                                '<div class="col-md-12" style="text-align: center;"> '+ getValidData(params.data.totalACTSaving)+' </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };

  customCellRendererTotalModel.prototype.getGui = function getGui() {
    return this.eGui;
  };

 

 function customCellRendererSavingModel(params) {}
  
  customCellRendererSavingModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    var projectedSaving = moment(params.colDef.field).isSame(moment()) || moment(params.colDef.field).isBefore(moment()) ? getValidData(parseInt(params.data["SAVING_ATT_"+ ( moment(params.colDef.field).format('MMM_YY')).toUpperCase() ])) : "&nbsp;" ;
    var actualSaving =  moment(params.colDef.field).isBefore(moment()) ? getValidData(parseInt(params.data["SAVING_ACT_"+ ( moment(params.colDef.field ).format('MMM_YY')).toUpperCase()] )) : "&nbsp;" ;

    resultElement.innerHTML = '<div class="column">'+ 
                                '<div class="col-md-12" style="text-align: right;border-bottom: 1px solid #bdc3c7;background: #50eef6;">'+ projectedSaving +'</div>'+
                                '<div class="col-md-12" style="text-align: right;background: #7de07dfc;">  '+ actualSaving +' </div> '+
                                '</div>'; 
    this.eGui = resultElement;
  };
  customCellRendererSavingModel.prototype.getGui = function getGui() {
    return this.eGui;
  };

  function customCellRendererPlanModel(params) {}
  
  customCellRendererPlanModel.prototype.init = function (params) {
    var resultElement = document.createElement("div");
    resultElement.setAttribute("class", "row");
    //var oddCol = params.colDef.headerName.con"background-color: #fde9d9;"
    resultElement.setAttribute("style", " margin-top: -3px;"); 
    var innerHTMLPlanContent ="",innerHTMLActualContent =""; 
    var wednesdayList = getMonthWednesdays(params.colDef.field);

    var colWidth = "width : "+(100 / wednesdayList.length)+ "%;";
    var remarkList = params.data.remarkList || [];
    var remark ;
    for (var i in wednesdayList ) {
      remark = null;
      var plan = params.data[wednesdayList[i].planColumn] ? params.data[wednesdayList[i].planColumn] : "&nbsp;";
      var actual = params.data[wednesdayList[i].actualColumn] && moment(wednesdayList[i].weekStartDate).isBefore(moment()) ? params.data[wednesdayList[i].actualColumn] : "&nbsp;"; 
      var colColor = plan != '&nbsp;' && actual != '&nbsp;'  && plan != actual ?   "color: red" : "";
      
      if (plan && plan !== '&nbsp;') {
        innerHTMLPlanContent +=  '<td class="text-center blue-plan-status" style="border: 1px solid #bdc3c7;'+ colWidth + '"> '+ plan +'</td>';
      } else {
        innerHTMLPlanContent +=  '<td class="text-center" style="border: 1px solid #bdc3c7;'+ colWidth + '"> '+ plan +'</td>';

      }
      var bkColor = "";
      if (plan != "&nbsp;" && actual != "&nbsp;") {
        if ( plan == actual) {
          bkColor = "green-plan-status";
        } else {
          bkColor = "red-plan-status";
        }
      } else if (actual != "&nbsp;") {
        bkColor = "red-plan-status";
      }
      
      remarkList.some(function(v){
        if (v.weekStartDate == wednesdayList[i].weekStartDate) {
          remark = v.remarkText;
          return true;   
        }
        return false;   
      });
      if (remark &&  actual != "&nbsp;" ) {
        innerHTMLActualContent +=  '<td onmouseleave="hideTooltip()" class="text-center ' + bkColor+ '" style="border: 1px solid #bdc3c7;overflow: hidden;position: relative;'+ colWidth +'"> <span class="remark-present">*</span><a class="btn btn-transparent btn-xs tooltips" style="color: white;" onmouseenter="showTooltip(\''+ remark+'\')">'+ actual +'</a> </td>';
      } else {
        innerHTMLActualContent +=  '<td class="text-center ' + bkColor+ '" style="border: 1px solid #bdc3c7;'+ colWidth  + '"> '+ actual +'</td>';
      }
    } 
  //var div = '<span style="margin:7px;" onmouseleave="hideTooltip();"><span><img  id="tooltip-costtable"  onmouseenter="val" style="width: 20px; height:20px;vertical-align: -webkit-baseline-middle;" src="' + icon + '"></span> '+ value + '</span>' ;
    resultElement.innerHTML = '<table style="width:100%;">'+ 
                                '<tbody style="width:100%;">'+  
                                '<tr class="row" style="border: 1px solid #bdc3c7;">'+  
                                  innerHTMLPlanContent +
                              //    '<td "><a href="#" class="btn btn-transparent btn-xs tooltips" onmouseenter="showTooltip()"><i class="fa fa-share"></i></a></td>'+
                                '</tr>'+
                                '<tr class="row" style="border: 1px solid #bdc3c7;">'+  
                                  innerHTMLActualContent+
                                '</tr>'+
                                '</tbody>'+
                              '</table>';                          
    this.eGui = resultElement;
  };
  customCellRendererPlanModel.prototype.getGui = function getGui() {
    return this.eGui;
  };


  function getValidData (value) {
    return value ? value : '&nbsp;';
  }
  function getSavingsHeader() {
    var plans = []; 
    var month = moment();
    month.month(0);
    var endDate = moment(month);
    endDate.add(1, 'year');
    while( month < endDate ) {
        plans.push({
          label : month.format('MMM-YY'),
          date : month.format('MM-DD-YYYY')
        });
        month.add(1, "month");
    }
    return plans;
  };


function getPlanningHeader(startDate) {
  var plans = []; 
  var month = moment(startDate);
  var endDate = moment(month);
  endDate.add(1,'year');
   while( month < endDate ) {
      plans.push({
        label : month.format('MMM-YY'),
        date : month.format('MM-DD-YYYY')
      });
      month.add(1, "month");
  }
  return plans;
};

function getMonthWednesdays (date) {
    var wednesday = moment(date).startOf('month').day("Wednesday");
    if ( wednesday.date() > 7 ) wednesday.add(7,'d');
    var month = wednesday.month();
    var wednesdayList = [];
    var count = 1;
    while(month === wednesday.month()) {
      var data = {
        planColumn  :  ("plan_" + moment(wednesday).format('DD_MMM_YY')).toUpperCase(),
        actualColumn:  ("actual_" +moment(wednesday).format('DD_MMM_YY')).toUpperCase(),
        weekStartDate :moment(wednesday).format('MM-DD-YYYY')
      };
      count++;
      wednesday.add(7,'d');
      wednesdayList.push(data);
    }
    return wednesdayList;
  }
  


  function drill (d1,d2) {
    if (d2 == 0) {
      $scope.filterByState("P");
    } else if (d2 == 2) {
      $scope.filterByState("D");
    } else if (d2 == 3) {
      $scope.filterByState("C");
    } else if (d2 == 1) {
      $scope.filterByState("A");
    }
    
  }
  function loadProjectStatusBarChart (statusData) {
    var data = [ {
      letter : 'PLAN',frequency : statusData.pWorkingStages || 0 ,state : "P"
    },
    {
      letter : 'DO', frequency : statusData.dWorkingStages || 0,state : "D"
    },{
      letter : 'CHECK', frequency : statusData.cWorkingStages || 0,state : "C"
    },{
      letter : 'ACT', frequency : statusData.aWorkingStages || 0,state : "A"
    }];
        
    d3.select('#chartPdcaStatus').selectAll("*").remove();
    var chart = d3.select('#chartPdcaStatus')
    .append('svg')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', svgHelp.translate(margin.left, margin.right));
    xScale.domain(_.uniq(_.map(data, 'letter')));
    yScale.domain([0, _.max(data, 'frequency').frequency]);

    function x(d) { return xScale(d.letter); }
    function y(d) { return yScale(d.frequency); }

    var camera = [width / 2, height / 2, -1000];
    var barGen = bar3d()
      .camera(camera)
      .x(x)
      .y(y)
      .z(zScale(0))
      .width(xScale.rangeBand())
      .height(function(d) { return height - y(d); })
      .depth(xScale.rangeBand());

    chart.append('g')
      .attr('class', 'x axis')
      .style('font-size', '12px')
      .attr('transform', svgHelp.translate(0, height))
      .call(xAxis);

   
    chart.selectAll('g.tick text')
    .style('font-size', '15px')
    .style('font-weight', '600');

        
    var extent = xScale.rangeExtent();
    var middle = (extent[1] - extent[0]) / 2;
    chart.selectAll('g.bar').data(data)
    .enter().append('g')
    .attr('class', 'bar')
    .sort(function(a, b) {
      return Math.abs(x(b) - middle) - Math.abs(x(a) - middle);
    })
    .call(barGen);

     chart.append('g')
      .attr('class', 'y axis')
      .style('font-size', '12px')
      .style('font-weight', '600')
      .call(yAxis);
      // .append('text')
      // .attr('transform', svgHelp.rotate(-90))
      // //.attr('x', height /2 )
      // .attr('y', 6)
      // .attr('dy', '.71em')
      // .style('text-anchor', 'end')
      // .text('Savings In Millions');

      chart.append('text')
      .attr('x', -(height / 2) - margin.top)
      .attr('y', - margin.left / 1.4)
      .attr('transform', 'rotate(-90)')
      .attr('text-anchor', 'middle')
      .text('No of Project\'s');
    
    chart.selectAll("g.bar .front").on('click',drill);
    
    chart.selectAll("g.bar")
    .data(data)
    .append("text")
    .attr("class", "bar")
    .attr("text-anchor", "middle")
    .style('font-size', '16px')
    .style('font-weight', '600')
    .attr("x", function(d) { return x(d) + 84; })
    .attr("y", function(d) { return ( y(d)-30 ) < 0 ?  0 : y(d)-30 ; })
    .text(function(d) { return d.frequency  ; });
  }
  
  function type(d) {
    d.frequency = +d.frequency;
    return d;
  }



  if ( $scope.currentStep == 5 ) {
    $scope.loadDEFMatrixGrid();
  }
  
  
}]);