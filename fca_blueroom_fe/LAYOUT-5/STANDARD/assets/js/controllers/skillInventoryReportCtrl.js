app.controller('SkillInventoryReportCtrl', ['$rootScope', '$scope', 'moment','reportService','$timeout',
function ($rootScope, $scope, moment,reportService, $timeout) {
  $scope.calendarYearList = [];
  $scope.calendarSelectedYear = parseInt(moment().format("YYYY"));
  var startYear = 2018;
  while( startYear < 2030) {
    $scope.calendarYearList.push(startYear);
    startYear++;
  }

  var rowData=[];
  var columnDefs =[];
  var gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData,
    animateRows: true,
    enableFilter: true,
    enableSorting: true, 
    enableColResize: true, 
    defaultColDef : {
      width: 150,
    },
    groupHeaderHeight:40,
    headerHeight:90,
    rowHeight:40,
    onGridReady: function(params) {
      params.columnApi.autoSizeColumns();
    },
    autoGroupColumnDef: {
    }
  };

  function clearGrid () {
    var myNode = document.getElementById("skill_inventory_report");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
  }

  $scope.userSkillReportDataLoad = function (year) {
    $rootScope.loaderAction = true;
    reportService.getUserSkillReport(year).then(function (response) {
      clearGrid();
      columnDefs = [{  headerName: "User",  children : [{ headerName: "Name", field:  "userName"  ,width:170,cellClass:["grid-cell-text-center"],pinned: 'left'}] }] ;
      gridOptions.rowData = response.data.userList || [];
      
      var colMethod =    { headerName: "WCM Methods And Tools " , children : []}  ;
      var columns = response.data.methodsAndToolHeaderList || {};
      for (var key in columns) {
        colMethod.children.push({ headerName: columns[key] , field:  key  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
      }
      columnDefs.push(colMethod);
      var colTechTool =    { headerName: "Technical Tools " , children : []};  
      columns = response.data.technicalToolList || {};
      for (var key in columns) {
        colTechTool.children.push({ headerName: columns[key] , field:  key  ,width:120,cellClass:["grid-cell-text-center"], type: 'numberColumn',default: 0});
      }
      columnDefs.push(colTechTool);
      gridOptions.columnDefs = columnDefs;
      $timeout(function() {
        var skill_inventory_reportDiv = document.querySelector('#skill_inventory_report');
        new agGrid.Grid(skill_inventory_reportDiv, gridOptions);
        $rootScope.loaderAction = false;
      }, 100);

    }, function (error) {
        console.error(error);
        $rootScope.loaderAction = false;
    });
  };

  $scope.userSkillReportDataLoad($scope.calendarSelectedYear);
}]);
