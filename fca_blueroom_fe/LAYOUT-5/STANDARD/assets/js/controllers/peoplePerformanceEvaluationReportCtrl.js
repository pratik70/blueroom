app.controller('PeoplePerformanceEvaluationReportCtrl', ['$rootScope', '$scope', 'moment','reportService','factorManagementService',
function ($rootScope, $scope,moment,reportService,factorManagementService) {
  
  $scope.imageHost = $rootScope.baseURL;
  
  $scope.employeeData = [];
  $scope.employeeList = [];
  $scope.selectedEmployees = [];

  $scope.calendarYearList = [];
  $scope.calendarSelectedYear = new Date().getFullYear();
  var startYear = 2018;
  while( startYear < 2100) {
    $scope.calendarYearList.push(startYear);
    startYear++;
  }

  reportService.getAllEmployeeList().then(function (response) {
      if(response && response.data.length > 0 ) {
         $scope.employeeList = response.data;
      }
    }, function (error) {
        console.error(error);
  });

  $scope.getProfileImageUrl = function (id) {
    return $rootScope.baseURL + '/userImage/'+ id;
  }

  $scope.getReportData = function() {
    $scope.employeeData = [];
    if($scope.calendarSelectedYear && $scope.selectedEmployees.length > 0){
      factorManagementService.userSavingsOnProject($scope.calendarSelectedYear).then(function (response) {
        reportService.getPeoplePerformanceEvaluationReport($scope.calendarSelectedYear,$scope.selectedEmployees).then(function (response2) {
          var factorList = response2.data || [];
          var list =   Object.keys( response.data || []).map( function( key ){
            return response.data[key]; 
          });
          /* var result = groupBy( list, function(item) {
            return item.userId;
          });*/
          var result = list;
          $scope.usersData = result;
          if ( factorList.length > 0) {
            factorList.forEach( function(item) {
              $scope.usersData.some( function(item2,index){
                if (item.userId == item2.userId ) {

                  $scope.usersData[index].projectFactor = item.projectFactor;
                  $scope.usersData[index].savingFactor = item.savingFactor;
                  $scope.usersData[index].noOfProjects = (item.projectFactor || 0 ) + ( item2.completeNoProject || 0 );
                  $scope.employeeData.push($scope.usersData[index]);
                }
              });
            });
          } else{
              $scope.selectedEmployees.forEach( function(item) {
              $scope.usersData.some( function(item2,index){
                  if (item == item2.userId ) {
                    $scope.usersData[index].projectFactor = 0;
                    $scope.usersData[index].savingFactor = 1;
                    $scope.usersData[index].noOfProjects = ( 0 ) + ( item2.completeNoProject || 0 );
                    $scope.employeeData.push($scope.usersData[index]);
                  }
              });
            });
          }
         }, function (error) {
            console.error(error);
      });
      }, function (error) {
          console.error(error);
      });
    }
  };

  $scope.getRoundedSaving = function (savingFactor, projectSavings) {
    var saving = (savingFactor || 1) * ( projectSavings ? projectSavings : 0)
    return  parseInt(saving);
  };
 

  // function groupBy( array , f ){
  //   var groups = {};
  //   array.forEach( function( o ) {
  //     var group = JSON.stringify( f(o) );
  //     if (!groups[group]) {
  //       groups[group] = {
  //           "userId" : o.userId,
  //           "name": o.user.name,
  //           "actualSaving": o.projectSavingInPercentage,
  //           "noOfProjects":  1 ,
  //           "projectFactor": 0,
  //           "savingFactor" : 0 ,
  //           "totalProjects" :  1 ,
  //           "totalSaving" : o.projectSavingInPercentage
  //       };
  //     } else {
  //       groups[group].actualSaving += o.projectSavingInPercentage || 0;
  //       groups[group].totalSaving += o.projectSavingInPercentage || 0;
  //       groups[group].noOfProjects += 1;
  //       groups[group].totalProjects += 1;      
  //     }
  //   });
  //   return Object.keys(groups).map( function( group ){
  //     return groups[group]; 
  //   });
  // }
}]);