module.exports = function(grunt) {
	var gtx = require('gruntfile-gtx').wrap(grunt);

    gtx.loadAuto();
    
    var gruntConfig = require('./grunt');
    gruntConfig.package = require('./package.json');

    gtx.config(gruntConfig);

    // We need our bower components in order to develop
    // --> DEVELOP WITH BOWER FOLDER
    gtx.alias('build:layout5', ['compass:layout5', 'clean:layout5', 'clean:bowerfolder', 'copy:bowerfolder', 'copy:layout5', 'string-replace:layout5', 'concat:layout5', 'cssmin:layout5', 'uglify:layout5']);
  
    // --> DEVELOP WITHOUT BOWER FOLDER
    gtx.alias('build:layout5', ['compass:layout5', 'clean:layout5', 'copy:layout5', 'string-replace:layout5', 'concat:layout5', 'cssmin:layout5', 'uglify:layout5']);
   
    // --> DEVELOP ALL THEME
    gtx.alias('build:alltheme', ['compass', 'clean', 'copy', 'string-replace', 'concat', 'cssmin', 'uglify']);

    // Dist bower folder
    gtx.alias('dist:bowerfolder', ['clean:bowerfolder', 'copy:bowerfolder']);

    
	//Build only Css
	gtx.alias('buildcss:layout5', ['compass:layout5']);
    gtx.finalise();
};
