module.exports = {
    layout5: {
        files: [
            { expand: true, src: "**", cwd: 'bower_components/bootstrap/fonts', dest: "dist/layout5/assets/fonts" },
            { expand: true, src: "**", cwd: 'bower_components/font-awesome/fonts', dest: "dist/layout5/assets/fonts" },
            { expand: true, src: "**", cwd: 'bower_components/themify-icons/fonts', dest: "dist/layout5/assets/fonts" },
            { expand: true, src: "**", cwd: 'bower_components/slick-carousel/slick/fonts', dest: "dist/layout5/assets/css/fonts" },
            { expand: true, src: "**", cwd: 'bower_components/flag-icon-css/flags', dest: "dist/layout5/assets/flags" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/templates', dest: "dist/layout5/templates" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/api', dest: "dist/layout5/assets/api" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/i18n', dest: "dist/layout5/assets/i18n" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/images', dest: "dist/layout5/assets/images" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/js/config', dest: "dist/layout5/assets/js/config" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/js/directives', dest: "dist/layout5/assets/js/directives" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/js/controllers', dest: "dist/layout5/assets/js/controllers" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/js/services', dest: "dist/layout5/assets/js/services" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/js/lib', dest: "dist/layout5/assets/js/lib" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/js/filters', dest: "dist/layout5/assets/js/filters" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/views', dest: "dist/layout5/assets/views" },
            { expand: true, src: "**", cwd: 'LAYOUT-5/STANDARD/assets/css/themes', dest: "dist/layout5/assets/css/themes" },
            { src: 'bower_components/slick-carousel/slick/ajax-loader.gif', dest: 'dist/layout5/assets/css/ajax-loader.gif' },
            { src: 'LAYOUT-5/STANDARD/master/_index.min.html', dest: 'dist/layout5/index.html' },
            { src: 'LAYOUT-5/STANDARD/favicon.ico', dest: 'dist/layout5/favicon.ico' },
            { src: 'LAYOUT-5/STANDARD/upload.php', dest: 'dist/layout5/upload.php' }
        ]
    },
    bowerfolder: {
        files: [
            { expand: true, src: "**", cwd: 'bower_components', dest: "dist/bower_components" }
        ]
    }
};
